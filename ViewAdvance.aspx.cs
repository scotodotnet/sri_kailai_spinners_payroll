﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class ViewAdvance : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    
    string str_dept;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin;
  
    string query;
    string fromdate;
    string ToDate;
    string salaryType;
    string Emp_ESI_Code;

    string EmployeeType;
    string EmpNo;

    protected void Page_Load(object sender, EventArgs e)
    {

        //ReportDocument rd = new ReportDocument();
        SqlConnection con = new SqlConnection(constr);
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();

        str_dept = Request.QueryString["Depat"].ToString();
        EmployeeType = Request.QueryString["EmpType"].ToString();
        EmpNo = Request.QueryString["EmpNo"].ToString();


        string Query = "";
        Query = "select ED.ExisistingCode,ED.EmpName,AR.Months,(AP.Amount) as Amt,(AR.Amount) as Paid,(AR.balance) as BalanceAmount, ";
        Query = Query + "AP.ReductionMonth,AP.DueMonth,AP.IncreaseMonth,AP.Completed from Advancerepayment AR ";
        Query = Query + "inner join EmployeeDetails ED on AR.EmpNo=ED.EmpNo ";
        Query = Query + "inner join AdvancePayment AP on AP.EmpNo=AR.EmpNo ";
        Query = Query + "where ED.ExisistingCode='" + EmpNo + "'";


        SqlCommand cmd = new SqlCommand(Query, con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataSet ds1 = new DataSet();
        con.Open();
        sda.Fill(ds1);
        DataTable dt_1 = new DataTable();
        sda.Fill(dt_1);
        con.Close();


        rd.Load(Server.MapPath("crystal/AdvanceHistory.rpt"));
        rd.SetDataSource(ds1.Tables[0]);


        CrystalReportViewer1.ReportSource = rd;
        CrystalReportViewer1.RefreshReport();
        CrystalReportViewer1.DataBind();



    }
}
