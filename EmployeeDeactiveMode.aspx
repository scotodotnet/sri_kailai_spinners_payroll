﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EmployeeDeactiveMode.aspx.cs" Inherits="EmployeeDeactiveMode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Employee Re-Activate</li></h4> 
                    </ol>
                </div>

<div id="main-wrapper" class="container">
<div class="row">
<div class="col-md-12">
<div class="col-md-9">
			<div class="panel panel-white">
		<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Employee Re-Activate</h4>
				</div>
				</div>
<form class="form-horizontal">
<div class="panel-body">

<div class="col-md-12">     
      
				      <div class="form-group row">
					  <asp:Label ID="lnlCategory" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
						<div class="col-sm-3">
                            <asp:DropDownList ID="ddlcategory" class="form-control" runat="server" 
                            AutoPostBack="true" onselectedindexchanged="ddlcategory_SelectedIndexChanged">
                            </asp:DropDownList>                          
                        </div>
					
				    <div class="col-sm-3">
				    <asp:Button ID="btnclick" class="btn btn-success" runat="server" Text="Click" 
				    onclick="btnclick_Click" />
				    </div>
			       </div>
			     </div>

<%--<div class="col-md-12">     
      
				      <div class="form-group row">
					  <asp:Label ID="Label1" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee No"></asp:Label>
						<div class="col-sm-2">
                            <asp:TextBox ID="TextBox1" class="form-control" runat="server"></asp:TextBox>
                                                  
                        </div>
					
					<asp:Label ID="Label2" runat="server" class="col-sm-2 control-label" 
                                                             Text="Existing No"></asp:Label>
					
				    <div class="col-sm-2">
				    <asp:TextBox ID="TextBox2" class="form-control" runat="server"></asp:TextBox>
				    </div>
				    <div class="col-sm-3">
				    <asp:Button ID="Button2" class="btn btn-success" runat="server" Text="Search" />
				    </div>
			       </div>
			   </div>
--%>


</div>
</form>

<form class="form-horizontal">
<div class="panel-body">

<asp:Panel ID="PanelGrid" runat="server" Visible="false" >

<div class="col-md-12">     

<div class="table-responsive">
<asp:Repeater ID="rptrCustomer" runat="server" onitemcommand="Repeater1_ItemCommand">
                                           <HeaderTemplate>
                                                    <table id="tableCustomer" class="display table" style="width: 100%; cellspacing: 0;">
                                                    <thead>
                                                      <tr>
                                                  
                                                      <th>EMPLOYEE ID</th>
                                                      <th>TOKEN NO</th>
                                                      <th>EMPLOYEE NAME</th>
                                                      <th>DEPARTMENT</th>
                                                      <th>DESIGNATION</th>
                                                      <th>ACTIVE</th>
                                                      <th>DEACTIVE DATE</th>
                                                    
                                                      <th>Activate</th>
                                                      <th>DeActivate</th>
                                                      </tr>
                                                   </thead>
                                                 </HeaderTemplate>
                                           <ItemTemplate>
                                                    <tr>
                    
                  
                                   <td>
                                       <%# DataBinder.Eval(Container.DataItem, "EmpNo")%>
                                  </td>
                                   <td>
                                       <%# DataBinder.Eval(Container.DataItem, "ExisistingCode")%>
                                  </td>
                                   
                                   <td>
                                       <%# DataBinder.Eval(Container.DataItem, "EmpName")%>
                                  </td>
                                   
                                   <td>
                                       <%# DataBinder.Eval(Container.DataItem, "DepartmentNm")%>
                                  </td>
                                   
                                   <td>
                                       <%# DataBinder.Eval(Container.DataItem, "Designation")%>
                                  </td>
                                   
                                   <td>
                                       <%# DataBinder.Eval(Container.DataItem, "ActivateMode")%>
                                  </td>
                               
                          <td> 
                          <asp:TextBox ID="txtdeactive"  runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "deactivedate") %>'></asp:TextBox> 
                          </td>
                        
                   <td>  
                      <asp:LinkButton ID="lnkedit1" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("EmpNo") %>' CommandName="Activate"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p>
                   </td>
                    <td>  
                      <asp:LinkButton ID="lnkedit2" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("EmpNo") %>' CommandName="DeActivate"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p>
                   </td>                              
                </tr>
                                               </ItemTemplate>
                                           <FooterTemplate>
              
                                           </table>
                                           </FooterTemplate>
                                            </asp:Repeater>
                                                </div>
</div>

<div class="col-md-12">     
      
				  <div class="form-group row">
				    <div class="col-sm-3">
				    <asp:Button ID="btnActivate" class="btn btn-success" runat="server" Text="Activate" 
				    onclick="btnActivate_Click"/>
				    </div>
				     <div class="col-sm-3">
				    <asp:Button ID="btnDeactive" class="btn btn-success" runat="server" Text="Deactive" 
				     onclick="btnDeactive_Click"/>
				    </div>
			       </div>
			     </div>

</asp:Panel>


</div>
</form>

</div>
</div>

   <!-- Dashboard start -->
 	
 	<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                          
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                           
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
 
    <!-- Dashboard End -->  

</div> <!-- col-md-12 End -->
</div> <!-- row End -->
</div>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

