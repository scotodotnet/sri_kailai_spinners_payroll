﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Basic_Details.aspx.cs" Inherits="Basic_Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate>

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Basic Details</li>
                   </ol>
           </div>
 
 
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Basic Details</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					    
					    <div class="col-md-1">
					    </div>
					   
					    <div class="form-group row">
					    
					      <asp:Label ID="Label1" for="input-Default" class="col-sm-10 control-label" 
                                Text="STAFF AND WORKER BASIC DETAILS" runat="server" 
                                Font-Bold="True" Font-Size="Large"></asp:Label>
                                 
				       </div> 
					
					    <div class="form-group row">
					    
					       <label for="exampleInputName" class="col-sm-2 control-label">Category</label>
						<div class="col-sm-4">
						
						<asp:DropDownList ID="ddlcategory" runat="server" class="form-control"
                         AutoPostBack="True" 
                         onselectedindexchanged="ddlcategory_SelectedIndexChanged"></asp:DropDownList>
                            <%--<asp:TextBox ID="TxtEmpName" class="form-control" runat="server" required></asp:TextBox>--%>
                        </div>
					    
					   
						<label for="exampleInputName" class="col-sm-2 control-label">Employee Type</label>
						<div class="col-sm-4">
						<%-- <asp:TextBox ID="TxtAttnDate" class="form-control" runat="server" required></asp:TextBox>--%>
						<asp:DropDownList ID="txtEmployeeType" runat="server" class="form-control"
                         AutoPostBack="True"
                         onselectedindexchanged="txtEmployeeType_SelectedIndexChanged"></asp:DropDownList>
					
						</div>
				        </div> 
				       
				        <div class="form-group row">
				           <div class="col-sm-3"></div>
					       <label for="exampleInputName" class="col-sm-2 control-label">Basic</label>
						   <div class="col-sm-3">
						    <asp:TextBox ID="txtBasicAndDA" class="form-control" runat="server" Text="0.00" required></asp:TextBox>
                           </div>
                        </div>
					    
					    <div id="Div1" class="form-group row"  runat="server">
					      <div class="col-sm-3"></div>
					      <label for="input-Default" class="col-sm-2 control-label">Washing.Allow</label>
					    	
							<div class="col-sm-3">
							 <asp:TextBox ID="txtWashing_Allow" class="form-control" runat="server" Text="0.00" required></asp:TextBox>
                             
                           </div><span>%</span>
					     </div>
					<div class="form-group row">
				         <div class="col-sm-3"></div>
					      <label for="input-Default" class="col-sm-2 control-label">Conv</label>
						  <div class="col-sm-3">
						  <asp:TextBox ID="txtConv_Allow" runat="server" class="form-control" Text="0.00" required></asp:TextBox>
						  
                         
						</div>
					  </div>
					  
					  
				      <div class="form-group row">
				        <div class="col-sm-3"></div>
					    <label for="exampleInputName" class="col-sm-2 control-label">HRA</label>
				     	<div class="col-sm-3">
				     	    <asp:TextBox ID="txtHRA" class="form-control" runat="server" Text="0.00" required></asp:TextBox>
				     	
				     	
                          
                        </div>
					  </div>
					  
					    <div id="Div2" class="form-group row" >
                         <div class="col-sm-3"></div>
					      <label for="input-Default" class="col-sm-2 control-label">Medical</label>
					    	<div class="col-sm-3">
					    	  <asp:TextBox ID="txtMedi_Allow" class="form-control"   runat="server" Text="0.00" required></asp:TextBox>
                           </div>
					     </div>
						
						
						 <div id="Div3" class="form-group row"  visible="false"  runat="server">
                         <div class="col-sm-3"></div>
					      <label for="input-Default" class="col-sm-2 control-label"  >OT</label>
					    	<div class="col-sm-3">
					    	  <asp:TextBox ID="txtOt" class="form-control" Text="0.00"   runat="server" required></asp:TextBox>
                           </div>
					     </div>
					
				        
				       
				        
				        
                        <div id="Div4" class="form-group row" visible="false"  runat="server">
                         <div class="col-sm-3"></div>
					      <label for="input-Default" class="col-sm-2 control-label"   >Edu.Allow</label>
					    	<div class="col-sm-3">
					    	<asp:TextBox ID="txtEdu_Allow" class="form-control"  runat="server" Text="0.00" required></asp:TextBox>
                           </div>
					     </div>
                        
                        
                      
					     
					     
					    <div id="Div5" class="form-group row" visible="false"  runat="server">
					     <div class="col-sm-3"></div>
					      <label for="input-Default" class="col-sm-2 control-label">RAI</label>
					    	<div class="col-sm-3">
					    
                             <asp:TextBox ID="txtRAI" class="form-control"  runat="server" Text="0.00" required></asp:TextBox>
                           </div>
					     </div>
					     
					     
					     
					
						<!-- Button start -->
						<div class="form-group row">
						</div>
                            <div class="txtcenter">
                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" onclick="btnSave_Click"/>
                                     
                                                       
                   <asp:LinkButton ID="btnClear" class="btn btn-danger" runat="server" onclick="btnClear_Click">Clear</asp:LinkButton>      
                    </div>
                    <!-- Button End -->	
						
						<div class="form-group row">
						</div>
							<!-- Table start -->
						
						        
       
       
					         <!-- Table End -->
						<div class="form-group row">
						</div>
                           
                        
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 <!-- Dashboard start -->
 
 <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
 <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
 
 <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->

 </ContentTemplate>
 </asp:UpdatePanel>

</asp:Content>

