﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SalaryDetails.aspx.cs" Inherits="SalaryDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Salary Details</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
<div class="row">
<div class="col-md-12">
              
<div class="col-md-9">
<div class="panel panel-white">
<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Salary Details</h4>
				</div>
				</div>

<form class="form-horizontal">
    <div class="panel-body"> 
        

        <div class="row">
					    <div class="form-group col-md-12">
					      <div align="center">
				          <label for="exampleInputName">Wages Type</label>
				          </div>
					    <div align="center">
					    
					        <%--<asp:RadioButtonList ID="rbsalary" runat="server" AutoPostBack="true" 
                              RepeatColumns="3">
                              <asp:ListItem Text ="Weekly Wages" Value="1"></asp:ListItem>
                              <asp:ListItem Text="Monthly Wages" Value="2" ></asp:ListItem>                                                                                                                                       
                              <asp:ListItem Text="Bi-Monthly Wages" Value="3"></asp:ListItem>
                            </asp:RadioButtonList>--%>
					    
					    
					    
					     <div class="form-group col-md-2"></div>
					        <div class="form-group col-md-3">
                            <asp:RadioButton ID="RadioButton1" runat="server" Text="WeeklyWages"/>
                            </div>
                            <div class="form-group col-md-3">
                            <asp:RadioButton ID="RadioButton2" runat="server" Text="MonthlyWages" />
                            </div>
                            <div class="form-group col-md-3">
                            <asp:RadioButton ID="RadioButton3" runat="server" Text="Bi-Monthly Wages"/>
                         </div>   
				            
                     </div>
                </div>
				</div>
				
        <div class="col-md-12">     
				        <div class="form-group row">
				          <label for="input-Default" class="col-sm-2 control-label">Category</label>
					      <div class="col-sm-2">
					      <asp:DropDownList ID="ddlCategory" class="form-control" runat="server">
                            </asp:DropDownList>  
					      </div>
					    
					
						<div class="col-md-1">
                          
                        </div>
                        
					   <label for="input-Default" class="col-sm-2 control-label">Department</label>
					   <div class="col-sm-2">
						<asp:DropDownList ID="DropDownList5" class="form-control" runat="server">
                            </asp:DropDownList> 
					  </div>
						
					  <div class="col-sm-3">
                         <asp:Button ID="Button7" class="btn btn-info"  runat="server" Text="Click"/>
                      </div>
						</div>
				        
                       </div>
     
        <div class="col-md-12">     
				        <div class="form-group row">
				            <label for="input-Default" class="col-sm-2 control-label">Existing Number</label>
					      <div class="col-sm-3">
                              <asp:DropDownList ID="DropDownList1" class="form-control" runat="server">
                              </asp:DropDownList>
					      </div>
					    	
						</div>
	 </div>
	 
        <div class="col-md-12">     
				        <div class="form-group row">
				            <label for="input-Default" class="col-sm-2 control-label">Employee No</label>
					      <div class="col-sm-3">
                              <asp:DropDownList ID="DropDownList3" class="form-control" runat="server">
                              </asp:DropDownList>
					      </div>
					    	
						    <div class="col-sm-1"></div>
				            <label for="input-Default" class="col-sm-2 control-label">Employee Name</label>
						    
						      <div class="col-sm-3">                    
						       <asp:DropDownList ID="DropDownList4" class="form-control" runat="server">
                              </asp:DropDownList>
					 </div>
				
						</div>
				        
                       </div>
	 	 
        <div class="col-md-12">     
				        <div class="form-group row">
				            <label for="input-Default" class="col-sm-2 control-label">FromDate</label>
					      <div class="col-sm-3">
                              <asp:TextBox ID="TextBox1" class="form-control" runat="server"></asp:TextBox>  
					      </div>
					    	
						    <div class="col-sm-1"></div>
				            <label for="input-Default" class="col-sm-2 control-label">ToDate</label>
						    
						      <div class="col-sm-3">                    
						      <asp:TextBox ID="TextBox2" class="form-control" runat="server"></asp:TextBox>
					 </div>
				
						</div>
				        
                       </div>
   
        <div class="col-md-12">     
				        <div class="form-group row">
				            <label for="input-Default" class="col-sm-2 control-label">Salary Date</label>
					      <div class="col-sm-3">
                              <asp:TextBox ID="TextBox3" class="form-control" runat="server"></asp:TextBox>  
					      </div>
					    	
						</div>
	 </div>
	 
        <div class="col-md-12">     
				        <div class="form-group row">
				            <label for="input-Default" class="col-sm-2 control-label">Financial Year</label>
					      <div class="col-sm-3">
                              <asp:DropDownList ID="DropDownList2" class="form-control" runat="server">
                              </asp:DropDownList>
					      </div>
					    	
						    <div class="col-sm-1"></div>
				            <label for="input-Default" class="col-sm-2 control-label">Total Working Days</label>
						    
						      <div class="col-sm-3">                    
                                  <asp:TextBox ID="TextBox4" class="form-control" runat="server"></asp:TextBox> 
                              </div>
				
						</div>
				        
                       </div>
	
        <div class="col-md-12">     
				        <div class="form-group row">
				            <label for="input-Default" class="col-sm-2 control-label">NFH Days</label>
					      <div class="col-sm-3">
                              <asp:TextBox ID="TextBox6" class="form-control" runat="server"></asp:TextBox>  
					      </div>
					      
					      <div class="col-sm-1"></div>
					    	<%--<label for="input-Default" class="col-sm-1 control-label">CL</label>--%>
						    <div class="col-sm-2">
						    <asp:TextBox ID="TextBox7" class="form-control" placeholder="CL" runat="server"></asp:TextBox>
						    </div>
						    					    
				            <%--<label for="input-Default" class="col-sm-2 control-label">Week off Days</label>--%>
						     <div class="col-sm-2">                    
                                  <asp:TextBox ID="TextBox5" class="form-control" placeholder="WeekoffDays" runat="server"></asp:TextBox> 
                              </div>
				
						</div>
				        
                    </div>
  
        <div class="col-md-12">     
				        <div class="form-group row">
				            <label for="input-Default" class="col-sm-2 control-label">Employee Working Days</label>
					      <div class="col-sm-3">
                              <asp:TextBox ID="TextBox8" class="form-control" runat="server"></asp:TextBox>  
					      </div>
					    	
						    <div class="col-sm-1"></div>
				            <label for="input-Default" class="col-sm-2 control-label">Absent Days</label>
						    
						      <div class="col-sm-3">                    
						      <asp:TextBox ID="TextBox9" class="form-control" runat="server"></asp:TextBox>
					 </div>
				
						</div>
				        
                       </div>
   
        <div class="col-md-12">     
				        <div class="form-group row">
				            <label for="input-Default" class="col-sm-2 control-label">Home Town Days</label>
					      <div class="col-sm-3">
                              <asp:TextBox ID="TextBox10" class="form-control" runat="server"></asp:TextBox>  
					      </div>
					    </div>
				        
                       </div>
   
        <div class="col-md-12">     
				        <div class="form-group row">
				        <div class="col-sm-2"></div>
					      <div class="col-sm-3">
                              <asp:Label ID="Label21" for="input-Default" class="control-label" 
                                  runat="server" Text="EARNINGS" Font-Bold="True"></asp:Label>
                               <%--<label for="input-Default" class="control-label">EARNINGS</label>--%>  
					      </div>
					    	
						    <div class="col-sm-1"></div>
				            <div class="col-sm-2"></div>
						    <div class="col-sm-3">         
						    <asp:Label ID="Label22" for="input-Default" class="control-label" runat="server" 
                                    Text="DEDUCTIONS" Font-Bold="True"></asp:Label>           
						    <%--<label for="input-Default" class="control-label">DEDUCTIONS</label>--%>
					        </div>
				
						</div>
				        
                       </div>
    
        <div class="col-md-12">     
				        <div class="form-group row">
                            <asp:Label ID="Label3" class="col-sm-2 control-label" runat="server" Text="Basic Salary"></asp:Label>
                          <div class="col-sm-3">
                              <asp:TextBox ID="TextBox11" class="form-control" runat="server"></asp:TextBox>  
					      </div>
					    	
						    <div class="col-sm-1"></div>
						    <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="PF Amount"></asp:Label>
				           <%-- <label for="input-Default" class="col-sm-2 control-label">PF Amount</label>--%>
						    
						      <div class="col-sm-3">                    
						      <asp:TextBox ID="TextBox12" class="form-control" runat="server"></asp:TextBox>
					 </div>
				
						</div>
				        
                       </div>
     
        <div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="Label6" class="col-sm-2 control-label" runat="server" Text="Allowance 2"></asp:Label>
				            <%--<label for="input-Default" class="col-sm-2 control-label">Allowance 2</label>--%>
					      <div class="col-sm-3">
                              <asp:TextBox ID="TextBox15" class="form-control" runat="server"></asp:TextBox>  
					      </div>
					    	
						    <div class="col-sm-1"></div>
						     <asp:Label ID="Label7" class="col-sm-2 control-label" runat="server" Text="Advance Amount"></asp:Label>
				            <%--<label for="input-Default" class="col-sm-2 control-label">Advance Amount</label>--%>
						    
						      <div class="col-sm-3">                    
						      <asp:TextBox ID="TextBox16" class="form-control" runat="server"></asp:TextBox>
					 </div>
				
						</div>
				        
                       </div>
       
        <div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="Label8" class="col-sm-2 control-label" runat="server" Text="Allowance 3"></asp:Label>
				       <%--<label for="input-Default" class="col-sm-2 control-label">Allowance 3</label>--%>
					      <div class="col-sm-3">
                              <asp:TextBox ID="TextBox17" class="form-control" runat="server"></asp:TextBox>  
					      </div>
					    	
						    <div class="col-sm-1"></div>
						    <asp:Label ID="Label9" class="col-sm-2 control-label" runat="server" Text="LOP"></asp:Label>
				            <%--<label for="input-Default" class="col-sm-2 control-label">LOP</label>--%>
						    
						      <div class="col-sm-3">                    
						      <asp:TextBox ID="TextBox18" class="form-control" runat="server"></asp:TextBox>
					 </div>
				
						</div>
				        
                       </div>
        
        <div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="Label10" class="col-sm-2 control-label" runat="server" Text="Allowance 4"></asp:Label>
				            <%--<label for="input-Default" class="col-sm-2 control-label">Allowance 4</label>--%>
					      <div class="col-sm-3">
                              <asp:TextBox ID="TextBox19" class="form-control" runat="server"></asp:TextBox>  
					      </div>
					     <div class="col-sm-1"></div>
					     <asp:Label ID="Label11" class="col-sm-2 control-label" runat="server" Text="Deduction 1"></asp:Label>
				          
				            <%--<label for="input-Default" class="col-sm-2 control-label">Deduction 1</label>--%>
						    
						      <div class="col-sm-3">                    
						      <asp:TextBox ID="TextBox20" class="form-control" runat="server"></asp:TextBox>
					 </div>
				
						</div>
				        
                       </div>
	 
	    <div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="Label12" class="col-sm-2 control-label" runat="server" Text="Allowance 5"></asp:Label>
				          
				            <%--<label for="input-Default" class="col-sm-2 control-label">Allowance 5</label>--%>
					      <div class="col-sm-3">
                              <asp:TextBox ID="TextBox21" class="form-control" runat="server"></asp:TextBox>  
					      </div>
					    	
						    <div class="col-sm-1"></div>
						    <asp:Label ID="Label13" class="col-sm-2 control-label" runat="server" Text="Deduction 2"></asp:Label>
				            <%--<label for="input-Default" class="col-sm-2 control-label">Deduction 2</label>--%>
						    
						      <div class="col-sm-3">                    
						      <asp:TextBox ID="TextBox22" class="form-control" runat="server"></asp:TextBox>
					 </div>
				
						</div>
				        
                       </div>
   
        <div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="Label14" class="col-sm-2 control-label" runat="server" Text="OT Amount"></asp:Label>
				        <%--<label for="input-Default" class="col-sm-2 control-label">OT Amount</label>--%>
					      <div class="col-sm-3">
                              <asp:TextBox ID="TextBox23" class="form-control" runat="server"></asp:TextBox>  
					      </div>
					    	
						    <div class="col-sm-1"></div>
						    <asp:Label ID="Label15" class="col-sm-2 control-label" runat="server" Text="Deduction 3"></asp:Label>
				            <%--<label for="input-Default" class="col-sm-2 control-label">Deduction 3</label>--%>
						    
						      <div class="col-sm-3">                    
						      <asp:TextBox ID="TextBox24" class="form-control" runat="server"></asp:TextBox>
					 </div>
				
						</div>
				        
                       </div>
    
        <div class="col-md-12">     
				        <div class="form-group row">
				           <div class="col-sm-2"></div>
					      <div class="col-sm-3"></div>
					      <div class="col-sm-1"></div>
				            <%--<label for="input-Default" class="col-sm-2 control-label">Deduction 4</label>--%>
						    <asp:Label ID="Label16" class="col-sm-2 control-label" runat="server" Text="Deduction 4"></asp:Label>
				          
						      <div class="col-sm-3">                    
						      <asp:TextBox ID="TextBox26" class="form-control" runat="server"></asp:TextBox>
					 </div>
				
						</div>
				        
                       </div>
     
        <div class="col-md-12">     
				        <div class="form-group row">
				           <div class="col-sm-2"></div>
					      <div class="col-sm-3"></div>
					      <div class="col-sm-1"></div>
				            <%--<label for="input-Default" class="col-sm-2 control-label">Deduction 5</label>--%>
						  <asp:Label ID="Label17" class="col-sm-2 control-label" runat="server" Text="Deduction 5"></asp:Label>
				  	      <div class="col-sm-3">                    
						      <asp:TextBox ID="TextBox25" class="form-control" runat="server"></asp:TextBox>
					 </div>
				
						</div>
				        
                       </div>
      
        <div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="Label18" class="col-sm-2 control-label" runat="server" Text="O/N Amount"></asp:Label>
				        <%--<label for="input-Default" class="col-sm-2 control-label">O/N Amount</label>--%>
					      <div class="col-sm-3">
                              <asp:TextBox ID="TextBox27" class="form-control" runat="server"></asp:TextBox>  
					      </div>
					    	
						    <div class="col-sm-1"></div>
				            <div class="col-sm-2"></div>
						    
						      <div class="col-sm-3"></div>
				
						</div>
				        
                       </div>
       
        <div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="Label19" class="col-sm-2 control-label" runat="server" Text="F/N Amount"></asp:Label>
				        <%--<label for="input-Default" class="col-sm-2 control-label">F/N Amount</label>--%>
					      <div class="col-sm-3">
                              <asp:TextBox ID="TextBox28" class="form-control" runat="server"></asp:TextBox>  
					      </div>
					    	
						    <div class="col-sm-1"></div>
				            <div class="col-sm-2"></div>
						    
						      <div class="col-sm-3"></div>
				
						</div>
				        
                       </div>
        
        <div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="Label20" class="col-sm-2 control-label" runat="server" Text="ThreeSided Amount"></asp:Label>
				        <%--<label for="input-Default" class="col-sm-2 control-label">ThreeSided Amount</label>--%>
					      <div class="col-sm-3">
                              <asp:TextBox ID="TextBox29" class="form-control" runat="server"></asp:TextBox>  
					      </div>
					    	
						    <div class="col-sm-1"></div>
				            <div class="col-sm-2"></div>
						    
						      <div class="col-sm-3"></div>
				
						</div>
				        
                       </div>
         
        <div class="col-md-12">     
				        <div class="form-group row">
				            <div class="col-sm-2">
                                <asp:Button ID="Button1" class="btn btn-success" runat="server" Text="Calculate" />
				            </div>
				            <div class="col-sm-1"></div>
					      <div class="col-sm-3">
                              <asp:Label ID="Label1" runat="server" Text="Gross Earnings RS. 0"></asp:Label>
					      </div>
					    	
						    <div class="col-sm-3">
				            <asp:Label ID="Label2" runat="server" Text="Gross Deductions RS. 0"></asp:Label>
				            </div>
						   <div class="col-sm-2"></div>
				
						</div>
				        
                       </div> 
          
        <div class="col-md-12">     
				        <div class="form-group row">
				            <div class="col-sm-2"></div>
				            <div class="col-sm-1"></div>
					      <div class="col-sm-3"> </div>
                           <div class="col-sm-2">
				            <asp:Label ID="Label4" runat="server" Text="Net Pay RS. 0"></asp:Label>
				            </div>
						   <div class="col-sm-3"></div>
				
						</div>
				        
                       </div>
    
        <div class="col-md-12">     
				<div class="form-group row">
					
			
				 <div class="col-sm-3">  
				     <asp:Button ID="Button4" class="btn btn-success" runat="server" Text="Save" />    
				 </div>
		
				 <div class="col-sm-3">
                     <asp:Button ID="Button5" class="btn btn-success" runat="server" Text="Reset" />
  				 </div>
  				
  				 <div class="col-sm-3">
                     <asp:Button ID="Button6" class="btn btn-danger" runat="server" Text="Cancel" />
  				 </div>
  				 
  				 <div class="col-sm-3">
                     <asp:Button ID="Button3" class="btn btn-success" runat="server" Text="GroupSalary" />                             
                 </div>
  				 				
			    </div>
				       
        </div>
               
         
</div>
</form>

</div>
</div>


              <!-- Dashboard start -->
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
              <!-- Dashboard End --> 

</div>
</div>
</div>
</asp:Content>

