﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using Altius.BusinessAccessLayer.BALDataAccess;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class FrmDeductionDownload : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Name;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        Name = Session["Usernmdisplay"].ToString();
        if (!IsPostBack)
        {
            DropDwonCategory();
        };
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    protected void rbsalary_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnDownload_Click(object sender, EventArgs e)
    {
        try
        {
            if (rbsalary.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Salary Type');", true);
            }
            else if (ddlcategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }
            else if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }

            else if (txtEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else if (txtEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else
            {
                string staff_lab = "";
                string Employee_Type = "";
                if (ddlcategory.SelectedValue == "1")
                {
                    staff_lab = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    staff_lab = "L";
                }
                Employee_Type = txtEmployeeType.SelectedValue.ToString();
                DataTable dt = new DataTable();
                dt = objdata.Sample_Attenance_Download(SessionCcode, SessionLcode, rbsalary.SelectedValue, staff_lab, Employee_Type);
                gvEmp.DataSource = dt;
                gvEmp.DataBind();


                //foreach (GridViewRow gvsalDed in gvEmp.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsalDed.FindControl("lblEmpNo");
                //    Label lbl_Advance = (Label)gvsalDed.FindControl("lblAdvance");
                //    string qry_ot = "";
                //    DataTable Advance_Mst_DT = new DataTable();
                //    DataTable Advance_Pay_DT = new DataTable();
                //    string Advance_Amt = "0.00";
                //    string Balance_Amt = "0";
                //    string Monthly_Paid_Amt = "0";
                //    string Advance_Paid_Amt = "0";
                //    if (rbsalary.SelectedValue == "2")
                //    {
                //        qry_ot = "Select * from AdvancePayment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                //        qry_ot = qry_ot + " And EmpNo='" + lbl_EmpNo.Text.Trim() + "' And Completed='N'";
                //        Advance_Mst_DT = objdata.RptEmployeeMultipleDetails(qry_ot);
                //        if (Advance_Mst_DT.Rows.Count != 0)
                //        {
                //            //get Balance Amt
                //            string constr = ConfigurationManager.AppSettings["ConnectionString"];
                //            SqlConnection cn = new SqlConnection(constr);
                //            cn.Open();
                //            qry_ot = "Select isnull(sum(Amount),0) as Paid_Amt from Advancerepayment where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And AdvanceId='" + Advance_Mst_DT.Rows[0]["ID"] + "'";
                //            SqlCommand cmd_wages = new SqlCommand(qry_ot, cn);
                //            Advance_Paid_Amt = (cmd_wages.ExecuteScalar()).ToString();
                //            Balance_Amt = (Convert.ToDecimal(Advance_Mst_DT.Rows[0]["Amount"].ToString()) - Convert.ToDecimal(Advance_Paid_Amt)).ToString();
                //            Monthly_Paid_Amt = Advance_Mst_DT.Rows[0]["MonthlyDeduction"].ToString();
                //            if (Convert.ToDecimal(Monthly_Paid_Amt.ToString()) >= Convert.ToDecimal(Balance_Amt.ToString()))
                //            {
                //                Advance_Amt = (Math.Round(Convert.ToDecimal(Balance_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                //            }
                //            else
                //            {
                //                Advance_Amt = (Math.Round(Convert.ToDecimal(Monthly_Paid_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                //            }
                //            cn.Close();
                //        }
                //        else
                //        {
                //            Advance_Amt = "0.00";
                //        }


                //    }
                //    else
                //    {
                //        Advance_Amt = "0.00";
                //    }

                //    lbl_Advance.Text = Advance_Amt;

                //}


                if (dt.Rows.Count > 0)
                {
                    string attachment = "attachment;filename=Deduction.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";

                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    gvEmp.RenderControl(htextw);
                    //gvDownload.RenderControl(htextw);
                    //Response.Write("Contract Details");
                    Response.Write(stw.ToString());
                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void gvEmp_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        EmployeeType();
    }
    public void EmployeeType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        txtEmployeeType.DataSource = dtemp;
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }
    protected void btnWagesDownload_Click(object sender, EventArgs e)
    {
        try
        {
            if (rbsalary.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Salary Type');", true);
            }
            else if (ddlcategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }
            else if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }

            else if (txtEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else if (txtEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else
            {
                string staff_lab = "";
                string Employee_Type = "";
                if (ddlcategory.SelectedValue == "1")
                {
                    staff_lab = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    staff_lab = "L";
                }
                Employee_Type = txtEmployeeType.SelectedValue.ToString();
                DataTable dt = new DataTable();
                DataTable SQLServer_Date_DT = new DataTable();
                DataTable Increment_Start_Date_DT = new DataTable();
                string query = "";
                if (Employee_Type == "4")
                {
                    //Hostel Increment Query

                    //Get SQL Server Date
                    query = "Select Convert(Varchar(10),GetDate(),105) as Server_Date,Month(GetDate()) as Month_No,Year(GetDate()) as Year_Int,datename(month,GetDate()) as Month_Name";
                    SQLServer_Date_DT = objdata.RptEmployeeMultipleDetails(query);

                    query = "Select * from MstBasicHostel_Increment_Date where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    Increment_Start_Date_DT = objdata.RptEmployeeMultipleDetails(query);

                    //From_Date_Get
                    string Month_Int = "01";
                    string Year_Int = "2015";
                    string Day_Int = "01";
                    string Date_Check = "";
                    if (SQLServer_Date_DT.Rows[0]["Month_No"].ToString() == "1")
                    {
                        Month_Int = "12";
                        Year_Int = (Convert.ToDecimal(SQLServer_Date_DT.Rows[0]["Year_Int"].ToString()) - Convert.ToDecimal(1)).ToString();
                    }
                    else if (SQLServer_Date_DT.Rows[0]["Month_No"].ToString() == "2")
                    {
                        Month_Int = "2";
                        Year_Int = (Convert.ToDecimal(SQLServer_Date_DT.Rows[0]["Year_Int"].ToString()) - Convert.ToDecimal(0)).ToString();
                    }
                    else
                    {
                        Month_Int = (Convert.ToDecimal(SQLServer_Date_DT.Rows[0]["Month_No"].ToString()) - Convert.ToDecimal(1)).ToString();
                        Year_Int = SQLServer_Date_DT.Rows[0]["Year_Int"].ToString();
                    }
                    Date_Check = Day_Int + "-" + Month_Int + "-" + Year_Int;

                    query = "Select * from (Select ED.EmpNo,ED.BiometricID,ED.ExisistingCode,";
                    query = query + " MD.DepartmentNm,ED.EmpName, SM.Base as OLD_Wages,";

                    query = query + " isnull(((";
                    query = query + " Select Basic_Amt from MstBasicHostel_Increment_Elg where";
                    query = query + " Days_Below <= (Select Sum(AD.Days + AD.ThreeSided) + Sum(Distinct HTW.Total_Worked_Days)";
                    query = query + " from AttenanceDetails AD inner join Hostel_Total_Work_Days HTW on HTW.EmpNo=AD.EmpNo";
                    query = query + " where AD.Lcode='" + SessionLcode + "' And AD.Ccode='" + SessionCcode + "'";
                    query = query + " And AD.FromDate >= convert(datetime,'" + Increment_Start_Date_DT.Rows[0]["Start_Date"].ToString() + "', 105)";
                    query = query + " And AD.FromDate <= convert(datetime,'" + Date_Check + "', 105) And HTW.Lcode='" + SessionLcode + "'";
                    query = query + " And HTW.Ccode='" + SessionCcode + "' And ED.EmpNo=AD.EmpNo And ED.EmpNo=HTW.EmpNo)";
                    query = query + " And Days_Above >= (Select Sum(AD.Days + AD.ThreeSided) + Sum(Distinct HTW.Total_Worked_Days)";
                    query = query + " from AttenanceDetails AD inner join Hostel_Total_Work_Days HTW on HTW.EmpNo=AD.EmpNo";
                    query = query + " where AD.Lcode='" + SessionLcode + "' And AD.Ccode='" + SessionCcode + "'";
                    query = query + " And AD.FromDate >= convert(datetime,'" + Increment_Start_Date_DT.Rows[0]["Start_Date"].ToString() + "', 105)";
                    query = query + " And AD.FromDate <= convert(datetime,'" + Date_Check + "', 105) And HTW.Lcode='" + SessionLcode + "'";
                    query = query + " And HTW.Ccode='" + SessionCcode + "' And ED.EmpNo=AD.EmpNo And ED.EmpNo=HTW.EmpNo)";
                    query = query + " ) + SM.Base),0) as New_Wages,";

                    query = query + " isnull((";
                    query = query + " Select Days_Below from MstBasicHostel_Increment_Elg where";
                    query = query + " Days_Below <= (Select Sum(AD.Days + AD.ThreeSided) + Sum(Distinct HTW.Total_Worked_Days)";
                    query = query + " from AttenanceDetails AD inner join Hostel_Total_Work_Days HTW on HTW.EmpNo=AD.EmpNo";
                    query = query + " where AD.Lcode='" + SessionLcode + "' And AD.Ccode='" + SessionCcode + "'";
                    query = query + " And AD.FromDate >= convert(datetime,'" + Increment_Start_Date_DT.Rows[0]["Start_Date"].ToString() + "', 105)";
                    query = query + " And AD.FromDate <= convert(datetime,'" + Date_Check + "', 105) And HTW.Lcode='" + SessionLcode + "'";
                    query = query + " And HTW.Ccode='" + SessionCcode + "' And ED.EmpNo=AD.EmpNo And ED.EmpNo=HTW.EmpNo)";
                    query = query + " And Days_Above >= (Select Sum(AD.Days + AD.ThreeSided) + Sum(Distinct HTW.Total_Worked_Days)";
                    query = query + " from AttenanceDetails AD inner join Hostel_Total_Work_Days HTW on HTW.EmpNo=AD.EmpNo";
                    query = query + " where AD.Lcode='" + SessionLcode + "' And AD.Ccode='" + SessionCcode + "'";
                    query = query + " And AD.FromDate >= convert(datetime,'" + Increment_Start_Date_DT.Rows[0]["Start_Date"].ToString() + "', 105)";
                    query = query + " And AD.FromDate <= convert(datetime,'" + Date_Check + "', 105) And HTW.Lcode='" + SessionLcode + "'";
                    query = query + " And HTW.Ccode='" + SessionCcode + "' And ED.EmpNo=AD.EmpNo And ED.EmpNo=HTW.EmpNo)";
                    query = query + " ),0) as Days_Below,";

                    query = query + " isnull((";
                    query = query + " Select Days_Above from MstBasicHostel_Increment_Elg where";
                    query = query + " Days_Below <= (Select Sum(AD.Days + AD.ThreeSided) + Sum(Distinct HTW.Total_Worked_Days)";
                    query = query + " from AttenanceDetails AD inner join Hostel_Total_Work_Days HTW on HTW.EmpNo=AD.EmpNo";
                    query = query + " where AD.Lcode='" + SessionLcode + "' And AD.Ccode='" + SessionCcode + "'";
                    query = query + " And AD.FromDate >= convert(datetime,'" + Increment_Start_Date_DT.Rows[0]["Start_Date"].ToString() + "', 105)";
                    query = query + " And AD.FromDate <= convert(datetime,'" + Date_Check + "', 105) And HTW.Lcode='" + SessionLcode + "'";
                    query = query + " And HTW.Ccode='" + SessionCcode + "' And ED.EmpNo=AD.EmpNo And ED.EmpNo=HTW.EmpNo)";
                    query = query + " And Days_Above >= (Select Sum(AD.Days + AD.ThreeSided) + Sum(Distinct HTW.Total_Worked_Days)";
                    query = query + " from AttenanceDetails AD inner join Hostel_Total_Work_Days HTW on HTW.EmpNo=AD.EmpNo";
                    query = query + " where AD.Lcode='" + SessionLcode + "' And AD.Ccode='" + SessionCcode + "'";
                    query = query + " And AD.FromDate >= convert(datetime,'" + Increment_Start_Date_DT.Rows[0]["Start_Date"].ToString() + "', 105)";
                    query = query + " And AD.FromDate <= convert(datetime,'" + Date_Check + "', 105) And HTW.Lcode='" + SessionLcode + "'";
                    query = query + " And HTW.Ccode='" + SessionCcode + "' And ED.EmpNo=AD.EmpNo And ED.EmpNo=HTW.EmpNo)";
                    query = query + " ),0) as Days_Above,";

                    query = query + " isnull((";
                    query = query + " Select Sum(AD.Days + AD.ThreeSided) + Sum(Distinct HTW.Total_Worked_Days) from AttenanceDetails AD";
                    query = query + " inner join Hostel_Total_Work_Days HTW on HTW.EmpNo=AD.EmpNo where AD.Lcode='" + SessionLcode + "'";
                    query = query + " And AD.FromDate >= convert(datetime,'" + Increment_Start_Date_DT.Rows[0]["Start_Date"].ToString() + "', 105)";
                    query = query + " And AD.FromDate <= convert(datetime,'" + Date_Check + "', 105) And HTW.Lcode='" + SessionLcode + "'";
                    query = query + " And AD.Ccode='" + SessionCcode + "' And HTW.Ccode='" + SessionCcode + "'";
                    query = query + " And ED.EmpNo=AD.EmpNo And ED.EmpNo=HTW.EmpNo";
                    query = query + " ),0) As Total_Days";

                    query = query + " from EmployeeDetails ED inner join SalaryMaster SM on ED.EmpNo=SM.EmpNo";
                    query = query + " inner join MstDepartment MD on ED.Department=MD.DepartmentCd";
                    query = query + " where ED.Lcode='" + SessionLcode + "' And ED.Ccode='" + SessionCcode + "'";
                    query = query + " And ED.EmployeeType='" + Employee_Type + "'";
                    query = query + " And SM.Lcode='" + SessionLcode + "' And SM.Ccode='" + SessionCcode + "'";

                    query = query + " ) mytable where OLD_Wages <> New_Wages And New_Wages > 0";
                    query = query + " order by ExisistingCode Asc";
                    dt = objdata.RptEmployeeMultipleDetails(query);


                    //Check Already Increment Updated or Not
                Recheck:
                    DataTable DTTable2 = new DataTable();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        query = "Select * from Wages_Increment_Det where EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "'";
                        query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        query = query + " And Days_Below='" + dt.Rows[i]["Days_Below"].ToString() + "'";
                        query = query + " And Days_Above='" + dt.Rows[i]["Days_Above"].ToString() + "'";
                        DTTable2 = objdata.RptEmployeeMultipleDetails(query);
                        if (DTTable2.Rows.Count != 0)
                        {
                            dt.Rows.RemoveAt(i);
                            dt.AcceptChanges();
                            goto Recheck;
                        }
                    }

                }
                else
                {
                    dt = objdata.Wages_Download(SessionCcode, SessionLcode, rbsalary.SelectedValue, staff_lab, Employee_Type);
                }
                if (Employee_Type == "4")
                {
                    GVHostelWages.DataSource = dt;
                    GVHostelWages.DataBind();
                }
                else
                {
                    GVWages.DataSource = dt;
                    GVWages.DataBind();
                }
                if (dt.Rows.Count > 0)
                {
                    string attachment = "attachment;filename=Wages.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";

                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);

                    if (Employee_Type == "4")
                    {
                        GVHostelWages.RenderControl(htextw);
                    }
                    else
                    {
                        GVWages.RenderControl(htextw);
                    }

                    //gvDownload.RenderControl(htextw);
                    //Response.Write("Contract Details");
                    Response.Write(stw.ToString());
                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
 
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
