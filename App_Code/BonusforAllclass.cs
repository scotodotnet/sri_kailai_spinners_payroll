﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for BonusforAllclass
/// </summary>
public class BonusforAllclass
{
    private string _EmpCode;
    private string _Department;
    private string _Category;
    private string _BonusCalculation;
    private string _Basic;
    private string _HRA;
    private string _Allowance1;
    private string _Allowance2;
    private string _Allowance3;
    private string _Allowance4;
    private string _Precentage;
    private string _PrecentageAmt;
    private string _Noofmonths;
    private string _BonusAmt;
    private string _CreatedBy;
    private string _DailyWages;
    private string _NoofDays;
    private string _LabourBonusAmount;
    private string _CalculationAmt;
    private string _rbwages;
    private string _consolidated;
    private string _deduction1;
    private string _deduction2;
    private string _deduction3;
    private string _deductioncal;
    private string _lbrpercentage;
    private string _lbrpercentageAmt;
    private string _lbrMonths;
    private string _lbrbonus;
    private string _Finance;
    private string _DeductionChkVal;
    private string _BonusType;
    private string _messded;
    private string _Hostel;
    private string _Ccode;
    private string _Lcode;
    private string _FDA;
    private string _VDA;
    private string _AttenanceDays;

    public BonusforAllclass()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string Hostel
    {
        get { return _Hostel; }
        set { _Hostel = value; }
    }
    public string messded 
    {
        get { return _messded; }
        set { _messded = value; }
    }
    public string BonusType
    {
        get { return _BonusType; }
        set { _BonusType = value; }
    }
    public string EmpCode
    {
        get { return _EmpCode; }
        set { _EmpCode = value; }
    }
    public string Department
    {
        get { return _Department; }
        set { _Department = value; }
    }
    public string Category
    {
        get { return _Category; }
        set { _Category = value; }
    }
    public string BonusCalculation
    {
        get { return _BonusCalculation; }
        set { _BonusCalculation = value; }
    }
    public string Basic
    {
        get { return _Basic; }
        set { _Basic = value; }
    }
    public string HRA
    {
        get { return _HRA; }
        set { _HRA = value; }
    }
    public string Allowance1
    {
        get { return _Allowance1; }
        set { _Allowance1 = value; }
    }
    public string Allowance2
    {
        get { return _Allowance2; }
        set { _Allowance2 = value; }
    }
    public string Allowance3
    {
        get { return _Allowance3; }
        set { _Allowance3 = value; }
    }
    public string Allowance4
    {
        get { return _Allowance4; }
        set { _Allowance4 = value; }
    }
    public string Percentage
    {
        get { return _Precentage; }
        set { _Precentage = value; }
    }
    public string PercentageAmt
    {
        get { return _PrecentageAmt; }
        set { _PrecentageAmt = value; }
    }
    public string Noofmonths
    {
        get { return _Noofmonths; }
        set { _Noofmonths = value; }
    }
    public string BonusAmt
    {
        get { return _BonusAmt; }
        set { _BonusAmt = value; }
    }
    public string CreatedBy
    {
        get { return _CreatedBy; }
        set { _CreatedBy = value; }
    }
    public string DailyWages
    {
        get { return _DailyWages; }
        set { _DailyWages = value; }
    }
    public string NoofDays
    {
        get { return _NoofDays; }
        set { _NoofDays = value; }
    }
    public string LabourBonusAmount
    {
        get { return _LabourBonusAmount; }
        set { _LabourBonusAmount = value; }
    }
    public string CalculationAmt
    {
        get { return _CalculationAmt; }
        set { _CalculationAmt = value; }
    }
    public string rbwages
    {
        get { return _rbwages; }
        set { _rbwages = value; }
    }
    public string consolidated
    {
        get { return _consolidated; }
        set { _consolidated = value; }
    }
    public string deduction1
    {
        get { return _deduction1; }
        set { _deduction1 = value; }
    }
    public string deduction2
    {
        get { return _deduction2; }
        set { _deduction2 = value; }
    }
    public string deduction3
    {
        get { return _deduction3; }
        set { _deduction3 = value; }
    }
    public string deductioncal
    {
        get { return _deductioncal; }
        set { _deductioncal = value; }
    }
    public string lbrpercentage
    {
        get { return _lbrpercentage; }
        set { _lbrpercentage = value; }
    }
    public string lbrpercentageAmt
    {
        get { return _lbrpercentageAmt; }
        set { _lbrpercentageAmt = value; }
    }
    public string lbrMonths
    {
        get { return _lbrMonths; }
        set { _lbrMonths = value; }
    }
    public string lbrbonus
    {
        get { return _lbrbonus; }
        set { _lbrbonus = value; }
    }
    public string Finance
    {
        get { return _Finance; }
        set { _Finance = value; }
    }
    public string DeductionChkVal
    {
        get { return _DeductionChkVal; }
        set { _DeductionChkVal = value; }
    }
    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }
    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }
    public string FDA
    {
        get { return _FDA; }
        set { _FDA = value; }
    }
    public string VDA
    {
        get { return _VDA; }
        set { _VDA = value; }
    }
    public string Attenance
    {
        get { return _AttenanceDays; }
        set { _AttenanceDays = value; }
    }
}
