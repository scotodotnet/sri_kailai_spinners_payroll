﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class SchemeUpload : System.Web.UI.Page
{
    //EmployeeClass objEmp = new EmployeeClass();
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string dd;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        if ((SessionAdmin == "2") || (SessionAdmin == "1"))
        {
            Response.Redirect("EmployeeRegistration.aspx");
        }
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        //lblComany.Text = SessionCcode + " - " + SessionLcode;
        //lblusername.Text = Session["Usernmdisplay"].ToString();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    protected void btnupload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (fileUpload.HasFile)
        {
            fileUpload.SaveAs(Server.MapPath("Upload/" + fileUpload.FileName));
            string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + fileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
            OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
            DataTable dts = new DataTable();
            using (sSourceConnection)
            {
                sSourceConnection.Open();
                OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                sSourceConnection.Close();
                using (OleDbCommand cmd = new OleDbCommand())
                {
                    command.CommandText = "Select * FROM [Sheet1$]";
                    sSourceConnection.Open();
                }
                using (OleDbDataReader dr = command.ExecuteReader())
                {

                    if (dr.HasRows)
                    {

                    }

                }
                OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                objDataAdapter.SelectCommand = command;
                DataSet ds = new DataSet();

                objDataAdapter.Fill(ds);
                DataTable dt = new DataTable();
                dt = ds.Tables[0];
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                }
                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    SqlConnection cn = new SqlConnection(constr);
                    string NewID = dt.Rows[j][0].ToString();
                    string OldID = dt.Rows[j][1].ToString();
                    string EmpName = dt.Rows[j][2].ToString();
                    string Scheme = dt.Rows[j][3].ToString();
                    string Balance = dt.Rows[j][4].ToString();
                    if (NewID.Trim() == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter New ID. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag = true;
                    }
                    else if (OldID.Trim() == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Old ID. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag = true;
                    }
                    else if (EmpName.Trim() == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Employee Name. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag = true;
                    }
                    else if (Scheme.Trim() == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Completed Days. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag = true;
                    }
                    else if (Balance.Trim() == "")
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Days. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag = true;
                    }
                    cn.Open();
                    string qry_1 = "Select EmpNo from EmployeeDetails where ExisistingCode='" + NewID + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    SqlCommand cmd_1 = new SqlCommand(qry_1, cn);
                    SqlDataReader sd_1 = cmd_1.ExecuteReader();
                    if (sd_1.HasRows)
                    {
                    }
                    else
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter New ID. The Row Number is " + j + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag = true;
                    }
                }
                if (!ErrFlag)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string EmpNo = "";
                        SqlConnection cn = new SqlConnection(constr);
                        cn.Open();
                        string qry_2 = "Select EmpNo from EmployeeDetails where ExisistingCode='" + dt.Rows[i][0].ToString().Trim() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                        SqlCommand cmd_2 = new SqlCommand(qry_2, cn);
                        EmpNo = Convert.ToString(cmd_2.ExecuteScalar());
                        string qry_up = "Update ContractDetails set BasicDays='" + dt.Rows[i][3] + "',BalanceDays='" + dt.Rows[i][4].ToString().Trim() + "' where EmpNo='" + EmpNo + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                        SqlCommand cmd_upd = new SqlCommand(qry_up, cn);
                        cmd_upd.ExecuteNonQuery();
                        cn.Close();
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Finished Successfully');", true);
            }
        }

    }
}
