﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Drawing;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data;

public partial class EmployeeRegistration : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    BALDataAccess objdata = new BALDataAccess();
    string stafflabor;
    string NonPFGrade;
    DataTable dttaluk = new DataTable();
    static string UserID = "", SesRoleCode = "", sessionAdmin = "";
    string stafflabour_temp;
    bool textSearch = false;
    string SessionCcode;
    string SessionLcode;
    string Hostel;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Panelstafflabor.Visible = false;
            Enable_False();
            District();
            //EmployeeType();
            Qualification();
            Department();
            //LabourType();
            DropdownProbationPeriod();
            DropDwonCategory();
            Dropdowncontract();
            string LeaveconutMsg = objdata.LeavecountMessage();
            //EmpPhoto.ImageUrl = ("~/EmployeeImages/NoImage.gif");
            //educate_RBtn();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert(' " + LeaveconutMsg + " - Staff Apply For Leave ');", true);
            //panelLeave.Visible = true;
            //lblLeave.Text = LeaveconutMsg + " Staffs - " + "Apply For Leave";

        }
    }
    public void EmployeeType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        ddemployeetype.DataSource = dtemp;
        ddemployeetype.DataTextField = "EmpType";
        ddemployeetype.DataValueField = "EmpTypeCd";
        ddemployeetype.DataBind();
    }
    public void District()
    {


        DataTable dt1 = new DataTable();
        dt1 = objdata.DropDownDistrict();
        //ddlPSDistrict.DataSource = dt1;
        //ddlPSDistrict.DataTextField = "DistrictNm";
        //ddlPSDistrict.DataValueField = "DistrictCd";
        //ddlPSDistrict.DataBind();
    }
    public void Qualification()
    {
        DataTable dtQuali = new DataTable();
        dtQuali = objdata.Qualification();
        ddlqualification.DataSource = dtQuali;
        ddlqualification.DataTextField = "QualificationNm";
        ddlqualification.DataValueField = "QualificationCd";
        ddlqualification.DataBind();

    }
    public void Department()
    {
        DataTable dtDip = new DataTable();
        dtDip = objdata.DDdEPARTMENT_LOAD();
        ddldepartment.DataSource = dtDip;
        ddldepartment.DataTextField = "DepartmentNm";
        ddldepartment.DataValueField = "DepartmentCd";
        ddldepartment.DataBind();
    }
    //public void LabourType()
    //{
    //    DataTable dtemp = new DataTable();
    //    dtemp = objdata.LabourDropDown();
    //    ddllabourtype.DataSource = dtemp;
    //    ddllabourtype.DataTextField = "EmpType";
    //    ddllabourtype.DataValueField = "EmpTypeCd";
    //    ddllabourtype.DataBind();
    //}

    public void DropdownProbationPeriod()
    {
        DataTable dtProbation = new DataTable();
        dtProbation = objdata.DropDownProbationPriod();
        //ddlprobationpd.DataSource = dtProbation;
        //ddlprobationpd.DataTextField = "ProbationMonth";
        //ddlprobationpd.DataValueField = "ProbationCd";
        //ddlprobationpd.DataBind();
    }

    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }

    public void Dropdowncontract()
    {
        DataTable dt_empty = new DataTable();
        ddlContract.DataSource = dt_empty;
        ddlContract.DataBind();
        DataTable dt = new DataTable();
        dt = objdata.DropDown_contract();
        ddlContract.DataSource = dt;
        ddlContract.DataTextField = "ContractName";
        ddlContract.DataValueField = "ContractCode";
        ddlContract.DataBind();
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Enable_False();

    }


    protected void rbtngender_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

   


    public void Enable_False()
    {
       
        lblPrsnDet.Visible = false;
        lblempname.Visible = false;
        txtempname.Visible = false;
        lblempname.Visible = false;
        lblempfname.Visible = false;
        txtempfname.Visible = false;
        lblgender.Visible = false;
        rbtngender.Visible = false;
        lblDOB.Visible = false;
        txtdob.Visible = false;
        lblMartial.Visible = false;
        Rbmartial.Visible = false;
        lblinitial.Visible = false;
        Rbfather.Visible = false;
        lblPSadd1.Visible = false;
        txtPSAdd1.Visible = false;
        lblDist.Visible = false;
        txtDistrict.Visible = false;
        lblPSAdd2.Visible = false;
        txtPSAdd2.Visible = false;
        lblTaluk.Visible = false;
        txtTaluk.Visible = false;
        lblState.Visible = false;
        txtpsstate.Visible = false;
        lblPhn.Visible = false;
        txtphone.Visible = false;
        txtphone1.Visible = false;
        lblMob.Visible = false;
        txtMobileCode.Visible = false;
        txtmobile.Visible = false;
        lblppno.Visible = false;
        txtppno.Visible = false;
        lblDirNo.Visible = false;
        txtdrivingno.Visible = false;
        lblEdu.Visible = false;
        //rbtneducateduneducated.Visible = false;
        rbtneducated.Visible = false;
        rbtnuneducated.Visible = false;
        lblqualif.Visible = false;
        ddlqualification.Visible = false;
        lblunive.Visible = false;
        txtuniversity.Visible = false;
        lblCmpDet.Visible = false;
        lblDept.Visible = false;
        ddldepartment.Visible = false;
        lblDesgn.Visible = false;
        txtdesignation.Visible = false;
        lblemptype.Visible = false;
        ddemployeetype.Visible = false;
        lblexisiting.Visible = false;
        txtexistiongno.Visible = false;
        lblMachID.Visible = false;
        txtOld.Visible = false;
        lblContract.Visible = false;
        ddlContract.Visible = false;
        lblnonpfgrade.Visible = false;
        chkboxnonpfgrade.Visible = false;
        lblHostel.Visible = false;
        chkHostel.Visible = false;
        btnsave.Visible = false;
        btnclear.Visible = false;

    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
        EmployeeType();
        if (ddlcategory.SelectedItem.Text == "----Select----")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Any Category');", true);

        }
        else if(ddlcategory.SelectedItem.Text == "Staff")
        {
            lblPrsnDet.Visible = true;
            lblempname.Visible = true;
            txtempname.Visible = true;
            lblempname.Visible = true;
            lblempfname.Visible = true;
            txtempfname.Visible = true;
            lblgender.Visible = true;
            rbtngender.Visible = true;
            lblDOB.Visible = true;
            txtdob.Visible = true;
            lblMartial.Visible = true;
            Rbmartial.Visible = true;
            lblinitial.Visible = true;
            Rbfather.Visible = true;
            lblPSadd1.Visible = true;
            txtPSAdd1.Visible = true;
            lblDist.Visible = true;
            txtDistrict.Visible = true;
            lblPSAdd2.Visible = true;
            txtPSAdd2.Visible = true;
            lblTaluk.Visible = true;
            txtTaluk.Visible = true;
            lblState.Visible = true;
            txtpsstate.Visible = true;
            lblPhn.Visible = true;
            txtphone.Visible = true;
            txtphone1.Visible = true;
            lblMob.Visible = true;
            txtMobileCode.Visible = true;
            txtmobile.Visible = true;
            lblppno.Visible = true;
            txtppno.Visible = true;
            lblDirNo.Visible = true;
            txtdrivingno.Visible = true;
            lblEdu.Visible = true;
            rbtneducated.Visible = false;
            rbtnuneducated.Visible = false;
            lblqualif.Visible = true;
            ddlqualification.Visible = true;
            lblunive.Visible = true;
            txtuniversity.Visible = true;
            lblCmpDet.Visible = true;
            lblDept.Visible = true;
            ddldepartment.Visible = true;
            lblDesgn.Visible = true;
            txtdesignation.Visible = true;
            lblemptype.Visible = true;
            ddemployeetype.Visible = true;
            lblexisiting.Visible = true;
            txtexistiongno.Visible = true;
            lblMachID.Visible = true;
            txtOld.Visible = true;
            lblContract.Visible = false;
            ddlContract.Visible = false;
            lblnonpfgrade.Visible = true;
            chkboxnonpfgrade.Visible = true;
            lblHostel.Visible = true;
            chkHostel.Visible = true;
            btnsave.Visible = true;
            btnclear.Visible = true;
        }
        else if (ddlcategory.SelectedItem.Text == "Labour")
        {
            lblPrsnDet.Visible = true;
            lblempname.Visible = true;
            txtempname.Visible = true;
            lblempname.Visible = true;
            lblempfname.Visible = true;
            txtempfname.Visible = true;
            lblgender.Visible = true;
            rbtngender.Visible = true;
            lblDOB.Visible = true;
            txtdob.Visible = true;
            lblMartial.Visible = true;
            Rbmartial.Visible = true;
            lblinitial.Visible = true;
            Rbfather.Visible = true;
            lblPSadd1.Visible = true;
            txtPSAdd1.Visible = true;
            lblDist.Visible = true;
            txtDistrict.Visible = true;
            lblPSAdd2.Visible = true;
            txtPSAdd2.Visible = true;
            lblTaluk.Visible = true;
            txtTaluk.Visible = true;
            lblState.Visible = true;
            txtpsstate.Visible = true;
            lblPhn.Visible = true;
            txtphone.Visible = true;
            txtphone1.Visible = true;
            lblMob.Visible = true;
            txtMobileCode.Visible = true;
            txtmobile.Visible = true;
            lblppno.Visible = true;
            txtppno.Visible = true;
            lblDirNo.Visible = true;
            txtdrivingno.Visible = true;
            lblEdu.Visible = true;
            //rbtneducateduneducated.Visible = true;
            rbtneducated.Visible = true;
            rbtnuneducated.Visible = true;
            lblqualif.Visible = false;
            ddlqualification.Visible = false;
            lblunive.Visible = false;
            txtuniversity.Visible = false;
            lblCmpDet.Visible = true;
            lblDept.Visible = true;
            ddldepartment.Visible = true;
            lblDesgn.Visible = true;
            txtdesignation.Visible = true;
            lblemptype.Visible = true;
            ddemployeetype.Visible = true;
            lblexisiting.Visible = true;
            txtexistiongno.Visible = true;
            lblMachID.Visible = true;
            txtOld.Visible = true;
            lblContract.Visible = false;
            ddlContract.Visible = false;
            lblnonpfgrade.Visible = true;
            chkboxnonpfgrade.Visible = true;
            lblHostel.Visible = true;
            chkHostel.Visible = true;
            btnsave.Visible = true;
            btnclear.Visible = true;
        }
    }



    //public void educate_RBtn()
    //{
    //    rbtneducateduneducated.SelectedValue = null;
    //}

   //protected void rbtneducateduneducated_SelectedIndexChanged(object sender, EventArgs e)
   //{
   //    if (rbtneducateduneducated.SelectedValue == "1")
   //    {
   //        lblqualif.Visible = true;
   //        ddlqualification.Visible = true;
   //        lblunive.Visible = true;
   //        txtuniversity.Visible = true;


   //    }
   //    else if (rbtneducateduneducated.SelectedValue == "2")
   //    {

   //        lblqualif.Visible = false;
   //        ddlqualification.Visible = false;
   //        lblunive.Visible = false;
   //        txtuniversity.Visible = false;

   //    }
   //}
   protected void btnsave_Click(object sender, EventArgs e)
   {
     
        try
        {
            string EmpNo = "";
            string EmNm = "";
            DateTime MyDateTime;
            MyDateTime = new DateTime();
            //lblError.Text = "";
            EmployeeClass objEmp = new EmployeeClass();

            bool isRegistered = false;
            bool ErrFlag = false;
            bool Updateerr = false;
            bool updateregister = false;
            bool PhoneFlag = false;
            if (txtempname.Text == "" && txtempfname.Text == "" && txtPSAdd1.Text == "" && txtPSAdd2.Text == "")
            {
                //panelError.Visible = true;
                //lblError.Text = "Enter your Name\\n Enter your Father Name\\n Enter your Address";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Name\\nEnter Your Father Name\\nEnter Your PSAddress1\\nEnterYourPSAddress2\\nEnter Your PMAddress1\\nEnter Your PMAddress2');", true);
                //System.Windows.Forms.MessageBox.Show("Enter Your Name\\nEnter Your Father Name\\nEnter Your PSAddress1\\nEnterYourPSAddress2\\nEnter Your PMAddress1\\nEnter Your PMAddress2", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
               
            }
            else if (rbtngender.SelectedValue == "0" || rbtngender.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Gender');", true);
                ErrFlag = true;
            }
            //else if (rbtneducateduneducated.SelectedValue == "0" || rbtneducateduneducated.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select EDUCATION DETAILS');", true);
            //    ErrFlag = true;
            //}
            else if (Rbmartial.SelectedValue == "0" || Rbmartial.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Martial Status');", true);
                ErrFlag = true;
            }
            else if (ddemployeetype.SelectedValue == "0" || ddemployeetype.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Employee Type');", true);
                ErrFlag = true;
            }
            else if (rbtngender.SelectedValue.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Gender');", true);
                ErrFlag = true;
            }
            else if (Rbmartial.SelectedValue.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Martial Status');", true);
                ErrFlag = true;
            }
            else if (Rbfather.SelectedValue.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Inital Type');", true);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                //if (rbtneducated.SelectedValue.Trim() == "")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Education Type');", true);
                //    ErrFlag = true;
                //}
            }
            else if (txtdob.Text == "")
            {
                //panelError.Visible = true;
                //lblError.Text = "Select Your Date of Birth\\nSelect Your PMDistrict\\nSelect Your PSDistrict\\nSelect Your PMTaluk\\nSelect Your PSTaluk";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Your Date of Birth\\nSelect Your PMDistrict\\nSelect Your PSDistrict\\nSelect Your PMTaluk\\nSelect Your PSTaluk');", true);
                //System.Windows.Forms.MessageBox.Show("Select Your Date of Birth\\nSelect Your PMDistrict\\nSelect Your PSDistrict\\nSelect Your PMTaluk\\nSelect Your PSTaluk", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            //else if (txtuniversity.Text == "" && txtdesignation.Text == "")
            //{
            //    //panelError.Visible = true;
            //    //lblError.Text = "Enter Your Mobile No\\nEnter Your BloodGroup\\nEnter Your University Name\\nEnter Your Company Name\\nEnter Your Department\\nEnter Your Designation";
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Mobile No\\nEnter Your BloodGroup\\nEnter Your University Name\\nEnter Your Company Name\\nEnter Your Department\\nEnter Your Designation');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter Your Mobile No\\nEnter Your BloodGroup\\nEnter Your University Name\\nEnter Your Company Name\\nEnter Your Department\\nEnter Your Designation", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            else if (ddldepartment.Text == "0")
            {
                //panelError.Visible = true;
                //lblError.Text = "Select the Department";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department...!');", true);
                //System.Windows.Forms.MessageBox.Show("Select The Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if ((txtexistiongno.Text == "") || (txtexistiongno.Text == "0"))
            {
                //panelError.Visible = true;
                //lblError.Text = "Enter the Existing Code";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing No...!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Existing No", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtphone.Text != "")
            {
                if (txtphone1.Text == "")
                {
                    //panelError.Visible = true;
                    //lblError.Text = "Enter the Phone No Properly";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Phone No properly...!');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Phone No Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }

            else if (txtphone.Text == "" && txtphone1.Text == "")
            {
                //if (txtmobile.Text == "" && txtMobileCode.Text == "")
                //{
                //    //panelError.Visible = true;
                //    //lblError.Text = "Enter the Mobile No or Phone No Properly";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Mobile No or phone No properly...!');", true);
                //    //System.Windows.Forms.MessageBox.Show("Enter The Mobile No or Phone number Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //    ErrFlag = true;
                //}
                if (txtMobileCode.Text != "")
                {
                    if (txtmobile.Text == "")
                    {
                        //panelError.Visible = true;
                        //lblError.Text = "Enter the Mobile No Properly";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Mobile No properly...!');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Mobile No Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag = true;
                    }
                }
            }

            


            if (!ErrFlag)
            {
                //panelError.Visible = false;
                //if (PanelEmployeeSearch.Visible == false)
                //{
                    bool ErrFlag1 = false;
                    if (ddlcategory.SelectedValue == "1")
                    {
                        EmNm = "Emp";
                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {

                        string Name = ddemployeetype.SelectedItem.Text;
                        string Nm = Name.Substring(0, 3);
                        EmNm = Nm;
                    }
                    string ExistingNo = objdata.ExistingNO_Verify(txtexistiongno.Text,SessionCcode,SessionLcode);
                    if (ExistingNo == txtexistiongno.Text)
                    {
                        //panelError.Visible = true;
                        //lblError.Text = "Existing no already Exist...!";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Existing No already Exist...!');", true);
                        //System.Windows.Forms.MessageBox.Show("Existing No Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag1 = true;
                    }
                    //Check with OLD ID
                    string OLD_No_Check = objdata.OLDNO_Verify(txtOld.Text, SessionCcode, SessionLcode);
                    if (OLD_No_Check == txtOld.Text)
                    {
                        //panelError.Visible = true;
                        //lblError.Text = "Existing no already Exist...!";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('OLD ID already Exist...!');", true);
                        //System.Windows.Forms.MessageBox.Show("Existing No Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag1 = true;
                    }
                    if (!ErrFlag1)
                    {
                        string MonthYear = System.DateTime.Now.ToString("MMyyyy");
                        // string dd = System.DateTime.Now.ToString("ddMMyyy");
                        string RegNo = objdata.MaxEmpNo();
                        int Reg = Convert.ToInt32(RegNo);
                        EmpNo = EmNm + MonthYear + RegNo;
                        objEmp.EmpCode = EmpNo;
                        objEmp.MachineCode = EmpNo;
                        objEmp.ExisitingCode = txtexistiongno.Text;
                        objEmp.EmpName = txtempname.Text;
                        objEmp.EmpFname = txtempfname.Text;
                        objEmp.Gender = rbtngender.SelectedValue;
                        objEmp.DOB = txtdob.Text;
                       // MyDateTime = DateTime.ParseExact(objEmp.DOB, "dd-MM-yyyy", culterInfo.DateTimeFormat);
                        objEmp.PSAdd1 = txtPSAdd1.Text;
                        objEmp.PSAdd2 = txtPSAdd2.Text;

                        objEmp.PSTaluk = txtTaluk.Text;
                        objEmp.PSDistrict = txtDistrict.Text;
                        objEmp.PSState = txtpsstate.Text;
                        if (ddlcategory.SelectedValue == "1")
                        {
                            objEmp.UnEducated = "1";
                        }
                        else if (ddlcategory.SelectedValue == "2")
                        {
                            //objEmp.UnEducated = rbtneducated.SelectedValue;//see here
                        }
                        objEmp.SchoolCollege = txtscholl.Text;


                        objEmp.Phone = (txtphone.Text + "-" + txtphone1.Text);
                        objEmp.Mobile = (txtMobileCode.Text + "-" + txtmobile.Text);

                        objEmp.PassportNo = txtppno.Text;
                        objEmp.DrivingLicNo = txtdrivingno.Text;
                        objEmp.Qualification = ddlqualification.SelectedValue;
                        objEmp.University = txtuniversity.Text;

                        objEmp.Department = ddldepartment.SelectedValue;
                        objEmp.Desingation = txtdesignation.Text;

                        objEmp.EmployeeType = ddemployeetype.SelectedValue;
                        objEmp.Ccode = SessionCcode;
                        objEmp.Lcode = SessionLcode;
                        objEmp.OldID = txtOld.Text;
                        //string FileName = Path.GetFileName(fileUpload.PostedFile.FileName);
                        //if (fileUpload.HasFile)
                       // {
                        //    string Exten = Path.GetExtension(fileUpload.PostedFile.FileName);
                        //    if (Exten == ".jpg")
                       //     {
                         //       fileUpload.SaveAs(Server.MapPath("EmployeeImages/" + txtexistiongno.Text.Trim() + Exten));
                                //fileUpload.SaveAs("E:/Images/" + txtexistiongno.Text.Trim() + Exten);
                        //    }
                       // }
                        if (ddemployeetype.SelectedValue == "3")
                        {
                            objEmp.ContractType = ddlContract.SelectedValue;
                        }
                        else
                        {
                            objEmp.ContractType = "0";
                        }

                        string stafflabor = "", RoleCode = "";
                        if (ddlcategory.SelectedValue == "1")
                        {
                            stafflabor = "S";
                        }
                        else if (ddlcategory.SelectedValue == "2")
                        {
                            stafflabor = "L";
                        }
                        if (UserID == "Admin")
                        {
                            RoleCode = "1";

                        }
                        else
                        {
                            RoleCode = "2";
                        }
                        if (chkboxnonpfgrade.Checked == true)
                        {
                            NonPFGrade = "1";
                        }
                        else if (chkboxnonpfgrade.Checked == false)
                        {
                            NonPFGrade = "2";
                        }
                        if (chkHostel.Checked == true)
                        {
                            Hostel = "1";
                        }
                        else if (chkHostel.Checked == false)
                        {
                            Hostel = "0";
                        }
                        objEmp.Hostel = Hostel;
                        objEmp.MaritalStatus = Rbmartial.SelectedValue;
                        objEmp.Initial = Rbfather.SelectedValue;
                        //objEmp.EmpUnitName = txtEmpUnit.Text.ToString();
                        try
                        {
                            //panelError.Visible = false;
                            objdata.InsertEmployeeDetails(objEmp, Reg, txtdob.Text, stafflabor, NonPFGrade, RoleCode);
                            isRegistered = true;

                        }
                        catch (Exception ex)
                        {
                            //panelError.Visible = true;
                            //lblError.Text = "Server Error - Contact Admin";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin....!');", true);
                            //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                    }

                //}
                //else if (PanelEmployeeSearch.Visible == true)////
                //{
                //    string EmpVerify = objdata.EmployeeVerify(txtempNo.Text);

                //    if (txtempNo.Text == "")
                //    {
                //        //panelError.Visible = true;
                //        //lblError.Text = "Enter the Employee No Properly";
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Id properly....');", true);
                //        //System.Windows.Forms.MessageBox.Show("Enter the Employee No Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //        Updateerr = true;
                //    }
                //    else if (txtempNo.Text != EmpVerify)
                //    {
                //        //panelError.Visible = true;
                //        //lblError.Text = "Enter the Employee No Properly";
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Id properly....');", true);
                //        //System.Windows.Forms.MessageBox.Show("Enter the Employee No Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //        Updateerr = true;
                //    }
                //    if (!Updateerr)
                //    {
                //        objEmp.EmpCode = txtempNo.Text;
                //        objEmp.MachineCode = txtempNo.Text;
                //        objEmp.ExisitingCode = txtexistiongno.Text;
                //        objEmp.EmpName = txtempname.Text;
                //        objEmp.EmpFname = txtempfname.Text;
                //        objEmp.Gender = rbtngender.SelectedValue;
                //        objEmp.DOB = txtdob.Text;
                //        //MyDateTime = DateTime.ParseExact(objEmp.DOB, "dd-MM-yyyy", null);
                //        objEmp.PSAdd1 = txtPSAdd1.Text;
                //        objEmp.PSAdd2 = txtPSAdd2.Text;
                //        objEmp.PSTaluk = txtTaluk.Text;
                //        objEmp.PSDistrict = txtDistrict.Text;
                //        objEmp.PSState = txtpsstate.Text;
                //        objEmp.OldID = txtOld.Text;
                //        if (ddlcategory.SelectedValue == "1")
                //        {
                //            objEmp.UnEducated = "1";
                //        }
                //        else if (ddlcategory.SelectedValue == "2")
                //        {
                //            objEmp.UnEducated = rbtneducateduneducated.SelectedValue;
                //        }
                        
                //        objEmp.SchoolCollege = txtscholl.Text;
                //        objEmp.Phone = (txtphone.Text + "-" + txtPhone1.Text);
                //        objEmp.Mobile = (txtMobileCode.Text + "-" + txtmobile.Text);
                //        objEmp.PassportNo = txtppno.Text;
                //        objEmp.DrivingLicNo = txtdrivingno.Text;
                //        objEmp.Qualification = ddlqualification.SelectedValue;
                //        objEmp.University = txtuniversity.Text;
                //        objEmp.Department = ddldepartment.SelectedValue;
                //        objEmp.Desingation = txtdesignation.Text;
                //        //objEmp.UnEduDept = txtunedudept.Text;
                //        //objEmp.UnEduDesig = txtunedudesg.Text;
                //        //     objEmp.Shift = rbtnshift.SelectedItem.Text;
                //        objEmp.EmployeeType = ddemployeetype.SelectedValue;
                //        objEmp.Ccode = SessionCcode;
                //        objEmp.Lcode = SessionLcode;
                //        objEmp.Hostel = Hostel;
                //            if (fileUpload.HasFile)
                //            {
                //                string Exten = Path.GetExtension(fileUpload.PostedFile.FileName);
                //                if (Exten == ".jpg")
                //                {
                //                    fileUpload.SaveAs(Server.MapPath("EmployeeImages/" + txtexistiongno.Text.Trim() + Exten));
                //                    //fileUpload.SaveAs("E:/Images/" + txtexistiongno.Text.Trim() + Exten);
                //                }
                //            }
                //        if (ddlcategory.SelectedValue == "1")
                //        {
                //            stafflabor = "S";
                //        }
                //        else if (ddlcategory.SelectedValue == "2")
                //        {
                //            stafflabor = "L";
                //        }
                //        // = rbtnlaburstaff.SelectedItem.Text;
                //        //  string str = "1";
                //        if (chkboxnonpfgrade.Checked == true)
                //        {
                //            NonPFGrade = "1";
                //        }
                //        else if (chkboxnonpfgrade.Checked == false)
                //        {
                //            NonPFGrade = "2";
                //        }
                //        if (UserID == "Admin")
                //        {

                //        }
                //        objEmp.MaritalStatus = Rbmartial.SelectedValue;
                //        objEmp.Initial = Rbfather.SelectedValue;
                //        //objEmp.EmpUnitName = txtEmpUnit.Text.ToString();
                //        try
                //        {
                //            panelError.Visible = false;
                //            objdata.UpdateEmployeeRegistration(objEmp, txtempNo.Text, stafflabor, txtdob.Text, NonPFGrade);
                //            updateregister = true;

                //        }
                //        catch (Exception ex)
                //        {
                //            //panelError.Visible = true;
                //            //lblError.Text = "Server Error - Contact Admin";
                //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin....!');", true);
                //            //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //            ErrFlag = true;
                //        }

                //    }
                //}//
            }


            if (isRegistered)
            {
                if (SesRoleCode == "1")
                {
                    //panelsuccess.Visible = true;
                    //lbloutput.Text = "Saved Successfully";
                    //btncancel.Enabled = false;
                    //btnsearch.Enabled = false;
                    //btnsave.Enabled = false;
                    //btnedit.Enabled = false;
                    //btnreset.Enabled = false;
                    //panelError.Visible = false;
                    //btnok.Focus();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your Registration No : " + EmpNo + "');", true);
                    //System.Windows.Forms.MessageBox.Show("Your Registration No : " + EmpNo, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    Clear();
                    //txtempname.Focus();
                }
                else if (SesRoleCode == "2")
                {
                    //lblError.Text = "Employee Registration Successfully";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Registration Successfully...');", true);
                    //System.Windows.Forms.MessageBox.Show("Employee Registered Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    Clear();
                    ////panelsuccess.Visible = true;
                    ////lbloutput.Text = "Saved Successfully";
                    //btncancel.Enabled = false;
                    //btnsearch.Enabled = false;
                    //btnsave.Enabled = false;
                    //btnedit.Enabled = false;
                    //btnreset.Enabled = false;
                    //panelError.Visible = false;
                    //btnok.Focus();

                }

            }
            if (updateregister)
            {
                //lblError.Text = "Modification done Successfully";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Modification done Successfully...');", true);
                //System.Windows.Forms.MessageBox.Show("Modification Done Successfully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                Clear();
                //panelsuccess.Visible = true;
                //lbloutput.Text = "Updated Successfully";
                //btnok.Visible = true;
                //btncancel.Enabled = false;
                //btnsearch.Enabled = false;
                //btnsave.Enabled = false;
                //btnedit.Enabled = false;
                //btnreset.Enabled = false;
                //panelError.Visible = false;
                //btnok.Focus();

            }
        }

        catch (Exception ex)
        {
            //panelError.Visible = true;
            //lblError.Text = "Contact Admin";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Contact Admin....!');", true);
            //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }

    
   }


   public void Clear()
   {
       //panelError.Visible = false;
       //btnok.Visible = false;
       //btncancel.Enabled = true;
       //lbloutput.Text = "";
       //btnsearch.Enabled = true;
       btnsave.Enabled = true;
       //btnedit.Enabled = true;
       //btnreset.Enabled = true;
       //panelsuccess.Visible = false;
       //panelError.Visible = false;
       txtexistiongno.Text = "";
       txtempname.Text = "";
       txtempfname.Text = "";
       rbtngender.SelectedValue = "1";
       txtdob.Text = "";
       txtPSAdd1.Text = "";
       txtPSAdd2.Text = "";
       //ddlPSDistrict.SelectedValue = "0";
       //DataTable dt = new DataTable();
       //ddlPSTaluk.DataSource = dt;
       //ddlPSTaluk.DataBind();
       txtphone1.Text = "";
       txtMobileCode.Text = "+91";
       //txteducation.Text = "";
       txtscholl.Text = "";
       txtphone.Text = "";
       txtmobile.Text = "";
       txtDistrict.Text = "";
       txtTaluk.Text = "";
       txtppno.Text = "";
       txtdrivingno.Text = "";
       ddlqualification.SelectedValue = "0";
       txtuniversity.Text = "";

       ddldepartment.SelectedValue = "0";
       txtdesignation.Text = "";
       ddemployeetype.SelectedValue = "0";
       //EmpPhoto.ImageUrl = ("~/EmployeeImages/NoImage.gif");
       txtpsstate.Text = "TamilNadu";
       chkboxnonpfgrade.Checked = false;
       //lblError.Text = "";
       //panelcontract.Visible = false;
       Rbmartial.SelectedIndex = -1;
       Rbfather.SelectedIndex = -1;
       //rbtncontratctype.SelectedIndex = -1;
       rbtnuneducated.Checked = false;
       rbtneducated.Checked = false;
       //rbtneducateduneducated.SelectedIndex = -1;
       rbtngender.SelectedIndex = -1;
       txtOld.Text = "";
       chkHostel.Checked = false;
   }

   protected void rbtneducated_CheckedChanged(object sender, EventArgs e)
   {
       //rbtnuneducated.Checked = false;
       //lblqualif.Visible = true;
       //ddlqualification.Visible = true;
       //lblunive.Visible = true;
       //txtuniversity.Visible = true;
   }

   protected void rbtnuneducated_CheckedChanged(object sender, EventArgs e)
   {
       //rbtneducated.Checked = false;
       //lblqualif.Visible = false;
       //ddlqualification.Visible = false;
       //lblunive.Visible = false;
       //txtuniversity.Visible = false;
   }
}
