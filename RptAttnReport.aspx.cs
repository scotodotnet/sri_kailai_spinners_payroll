﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class RptAttnReport : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    Int32 Staff_Strength_Tot;
    decimal Staff_Salary_Tot;
    Int32 Labour_Strength_Tot;
    decimal Labour_Salary_Tot;
    Int32 Total_Strength_Tot;
    decimal Total_Salary_Tot;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            DropDwonCategory();

        }
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();


        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        //EmployeeType();
        EmployeeType_Load();
    }

    public void EmployeeType_Load()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        txtEmployeeType.DataSource = dtemp;
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }

    protected void btnAttnReport_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;



            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "1")
                    {
                        Stafflabour = "S";
                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {
                        Stafflabour = "L";
                    }


                    ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + "" + "&Months=" + "" + "&yr=" + "2014" + "&fromdate=" + "" + "&ToDate=" + "" + "&Salary=" + "" + "&CashBank=" + "" + "&ESICode=" + "" + "&EmpType=" + txtEmployeeType.SelectedValue.ToString() + "&PayslipType=" + "" + "&PFTypePost=" + "" + "&Left_Emp=" + "" + "&Leftdate=" + "" + "&EligbleWDays=" + "" + "&AttnDaysType=" + rdbAttnDaysType.SelectedValue.ToString() + "&Report_Type=Attn_REPORT", "_blank", "");

                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
