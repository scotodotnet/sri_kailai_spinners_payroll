﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PFDownload.aspx.cs" Inherits="PFDownload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>

          <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">PF DOWNLOAD</li></h4> 
                    </ol>
        </div>
          
          
<div id="main-wrapper" class="container">
<div class="row">

<div class="col-md-12">
               
<div class="col-md-9">
			  <div class="panel panel-white">
			
			
			    <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">PF DOWNLOADS</h4>
				</div>
			   </div>
				<form class="form-horizontal">
				  
				  <div align="center">
				        <div class="form-group>
					       <asp:Label ID="Label1" for="input-Default" class="col-sm-12 control-label" 
                                Text="PF DOWNLOAD DETAILS" runat="server" Font-Bold="True" Font-Underline="True" Font-Size=Large></asp:Label>
				        
				        </div>
				   </div>
				   
				 <div class="panel-body">
                        
                  <div class="row">
					    <div class="form-group col-md-12">
					      
				   <div class="form-group row">
					       <asp:Label ID="lblFinance" runat="server" class="col-sm-2 control-label" 
                                                             Text="Financial Year"></asp:Label>
					   	<div class="col-sm-3">
				           <asp:DropDownList ID="ddlfinance" runat="server" AutoPostBack="true" class="form-control">
                           </asp:DropDownList>
                                                  
                        </div>
					
					     <div class="col-sm-1"></div>
				       <asp:Label ID="lblMonth" runat="server" Text="Months" class="col-sm-2 control-label"></asp:Label>
				      			     
				        <div class="col-sm-3">
						       <asp:DropDownList ID="ddlmonths" runat="server" AutoPostBack="true" class="form-control">
                              </asp:DropDownList>
                          </div>
			       </div>
			       
				   <div class="form-group row">
					       <asp:Label ID="Label2" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
					   	<div class="col-sm-3">
				           <asp:DropDownList ID="ddlcategory" runat="server" AutoPostBack="true" class="form-control" onselectedindexchanged="ddlcategory_SelectedIndexChanged">
                           </asp:DropDownList>
                                                  
                        </div>
									    
			       </div>
				   
				   <div class="form-group row">
					    <div class="col-sm-3"></div>
					   	<div class="col-sm-3">
				           <asp:Button ID="btnDownload" runat="server" Text="DownLoad" class="btn btn-success" OnClick="btnDownload_Click"/>
                                 
                                                  
                        </div>
									    
			       </div>
			     </div>
			     </div>
			     
			     </div> <!-- panel-body End -->
			    </form> <!-- form End -->
			  </div> <!-- panel-white End -->
			  </div> <!-- col-md-9 End -->



       <asp:Panel ID="panel1" runat="server" Visible="false">
 <table>
                                                    <tr>
                                                        <td colspan="5">
                                                            <asp:GridView ID="gvDownload" runat="server" AutoGenerateColumns="false" >
                                                                <Columns>
                                                                <asp:TemplateField>
                                                                        <HeaderTemplate>EmployeeName</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblpfNo" runat="server" Text='<%# Eval("EmpName") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>pf no</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblpfNo" runat="server" Text='<%# Eval("PFnumber") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Wages</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                          <asp:Label ID="lblEages" runat="server" Text='<%# Eval("PfSalary") %>' ></asp:Label> 
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>epf_ee</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="pf_ee" runat="server" Text='<%# Eval("ProvidentFund") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>EE Refund</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblrefund" runat="server" Text="0" ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>NCP Days</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblNcp" runat="server" Text='<%# Eval("LOPDays") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>DOL</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldol" runat="server" Text="" ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>RFL</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblRFl" runat="server" Text="" ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Wage Arr</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblwageArr" runat="server" Text="0" ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>EE Arr</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEEArr" runat="server" Text="0" ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ER Arr</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblERArr" runat="server" Text="0"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>EPS ARR</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblepsArr" runat="server" Text="0" ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    </table>
                                                    </asp:Panel>


<!-- Dashboard start -->
 	
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
     <!-- Dashboard End --> 
     
     </div>  <!-- col-md-12 End -->
     </div>  <!-- row End -->
     </div>  <!-- main End -->
     </ContentTemplate>
     
     
       <Triggers>
          <asp:PostBackTrigger ControlID="btnDownload"/>
      </Triggers>
     </asp:UpdatePanel>
     
</asp:Content>

