﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;
using System.Collections;

public partial class SalaryCalculation : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    SalaryClass objSal = new SalaryClass();
    string SessionCcode = "0";
    string SessionLcode = "0";
    static string EmployeeDays = "0";
    static string Work = "0";
    static string NFh = "0";
    static string lopdays = "0";
    static string WeekOff = "0";
    static string Home = "0";
    static string halfNight = "0";
    static string FullNight = "0";
    static string ThreeSided = "0";
    static string HalfNight_Amt = "0";
    static string FullNight_Amt = "0";
    static string ThreeSided_Amt = "0";
    static string SpinningAmt = "0";
    static string DayIncentive = "0";
    static string cl = "0";
    static string OTDays = "0";
    static string basic = "0";
    static string All1 = "0";
    static string All2 = "0";
    static string All3 = "0";
    static string Al4 = "0";
    static string All5 = "0";
    static string Ded1 = "0";
    static string ded2 = "0";
    static string ded3 = "0";
    static string ded4 = "0";
    static string ded5 = "0";
    static string Fine = "0";
    static string lop = "0";
    static string Advance = "0";
    static string pfAmt = "0";
    static string ESI = "0";
    static string stamp = "0";
    static string Union = "0";
    static string TotalEarnings = "0";
    static string TotalDeductions = "0";
    static string NetPay = "0";
    static string RoundOffNetPay = "0";
    static string Words = "";
    static string PFS = "0.00";
    static string ExistNo = "";
    static string PF_salary;
    string TempDate;
    DateTime TransDate;
    DateTime MyDate;
    static int d = 0;
    static decimal AdvAmt;
    static string ID;
    static bool isUK = false;
    string SMonth;
    string Year;
    int dd;
    string MyMonth;
    static decimal val;
    string WagesType;
    static decimal basicsalary = 0;
    static string Basic_For_SM = "0";
    static string OTHoursNew_Str = "0";
    static string OTHoursAmtNew_Str = "0";
    static string query2 = "0";

    static string Fixed_Work_Dayss = "0";
    static string WH_Work_Days = "0";
    static string DedOthers1 = "0";
    static string DedOthers2 = "0";
    static string Mess = "0";

    static string EmployeerPFOne = "0.00";
    static string EmployeerPFTwo = "0.00";
    static string EmployeerESI = "0.00";

    string Error_Query = "Error";

    DataTable AutoDataTable = new DataTable();
    string Wages;
    Boolean ErrFlag = false;
    string todate;
    string fromdate;
    string SSQL;
    DataTable Datacells = new DataTable();
    static int CurrentYear;
    DataTable dsEmployee = new DataTable();
    string Encry_MachineID;
    DateTime dtime;
    DateTime dtime1;
    DateTime date1;
    DateTime date2 = new DateTime();
    string Spin_Machine_ID_Str;
    string[] Att_Date_Check;
    string Att_Already_Date_Check;
    string Att_Year_Check;
    string Month_Name_Change;
    string halfPresent;
    int aintI = 1;
    int aintK = 1;
    int aintCol;
    string Time_IN_Str;
    string Time_Out_Str;
    string Total_Time_get = "";
    Double time_Check_dbl = 0;
    string Machine_ID_Str = "";
    string OT_Week_OFF_Machine_No = null;
    DateTime NewDate1;
    DataTable mLocalDS = new DataTable();
    string Date_Value_Str = "";
    string Date_Value_Str1;
    double Final_Count;
    DateTime InTime_Check;
    DateTime InTime_Check1;
    DateTime InToTime_Check;
    TimeSpan InTime_TimeSpan;
    string From_Time_Str;
    string To_Time_Str = "";
    DataTable DS_Time = new DataTable();
    DataTable DS_InTime = new DataTable();
    string Final_InTime = "";
    string Final_OutTime = "";
    string Final_Shift = "";
    DataTable Shift_DS_Change = new DataTable();
    bool Shift_Check_blb = false;
    string Shift_Start_Time_Change = null;
    string Shift_End_Time_Change = null;
    string Employee_Time_Change = "";
    DateTime ShiftdateStartIN_Change = default(DateTime);
    DateTime ShiftdateEndIN_Change = default(DateTime);
    DateTime EmpdateIN_Change = default(DateTime);
    TimeSpan ts4 = new TimeSpan();
    decimal Month_WH_Count = 0;
    string[] Time_Minus_Value_Check;
    bool isPresent = false;
    double Present_WH_Count;
    double Present_Count;
    double NFH_Days_Present_Count;
    int Appsent_Count;
    int shiftCount = 0;
    string Token_No_Get;
    double EHours_Count;
    Decimal Total_Days_Count;
    double NFH_Days_Count;
    decimal Fixed_Work_Days;
    static string Gift;
    


    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    public string Right_Val(string Value, int Length)
    {
        // Recreate a RIGHT function for string manipulation
        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        if (SessionAdmin == "2")
        {
            Response.Redirect("EmployeeRegistration.aspx");

        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();

        con = new SqlConnection(constr);
        //lblComany.Text = SessionCcode + " - " + SessionLcode;
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            DropDwonCategory();
            Months_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            txtTokenNoSearch.Enabled = false;
        }
        //lblusername.Text = Session["Usernmdisplay"].ToString();
    }

    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    public void EmployeeType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        txtEmployeeType.DataSource = dtemp;
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }

    public string NumerictoNumber(int Number, bool isUK)
    {
        if (Number == 0)
            return "Zero";

        string and = isUK ? "and " : "";
        if (Number == -2147483648)
            return "Minus Two Billion One Hundred " + and +
        "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
        "Six Hundred " + and + "Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t = 0;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (Number < 0)
        {
            sb.Append("Minus");
            Number = -Number;
        }

        string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
        num[0] = Number % 1000;
        num[1] = Number / 1000;
        num[2] = Number / 1000000;
        num[1] = num[1] - 1000 * num[2];  // thousands
        num[2] = num[2] - 100 * num[3];//laks
        num[3] = Number / 10000000;     // billions
        num[2] = num[2] - 1000 * num[3];  // millions

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10;
            t = num[i] / 10;
            h = num[i] / 100;
            t = t - 10 * h;

            if (h > 0)
                sb.Append(words0[h] + " Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i < first)
                    sb.Append(and);

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);

            }

            if (i != 0)
                sb.Append(words3[i - 1]);

        }

        return sb.ToString().TrimEnd();



    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        EmployeeType_Load();
    }
    public void EmployeeType_Load()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        txtEmployeeType.DataSource = dtemp;
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }

    protected void rbsalary_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (rbsalary.SelectedValue == "1")
        //{
        //    PanelMonthly.Visible = true;
        //    panelWeekly.Visible = true;
        //    panelBimonth.Visible = false;

        //}
        //else if (rbsalary.SelectedValue == "2")
        //{
        //    PanelMonthly.Visible = true;
        //    panelWeekly.Visible = false;
        //    panelBimonth.Visible = false;
        //}
        //else if (rbsalary.SelectedValue == "3")
        //{
        //    PanelMonthly.Visible = true;
        //    panelWeekly.Visible = false;
        //    panelBimonth.Visible = true;
        //}
    }
    protected void txtFrom_TextChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        txtTo.Text = null;
        int val_Month = Convert.ToDateTime(txtFrom.Text).Month;
        string Mont = "";
        switch (val_Month)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }
        if (Mont != ddlMonths.SelectedValue)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Date Properly...');", true);
            ErrFlag = true;
            txtFrom.Text = null;
        }
    }
    protected void txtTo_TextChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                DateTime dfrom = Convert.ToDateTime(txtFrom.Text);
                DateTime dtto = Convert.ToDateTime(txtTo.Text);
                if (dfrom >= dtto)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Date Properly...');", true);
                    ErrFlag = true;
                    txtTo.Text = null;
                }

            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void ChkTokenNoSalaryprocess_CheckedChanged(object sender, EventArgs e)
    {
        if (ChkTokenNoSalaryprocess.Checked == true)
        {
            txtTokenNoSearch.Enabled = true;

        }
        else
        {
            txtTokenNoSearch.Enabled = false;
            txtTokenNoSearch.Text = "";
        }
    }
    protected void ChkCivilIncentive_CheckedChanged(object sender, EventArgs e)
    {
        if (ChkCivilIncentive.Checked == true)
        {
            PanelCivilIncenDate.Visible = true;
        }
        else
        {
            PanelCivilIncenDate.Visible = false;
        }
    }
    protected void BtnSalaryAttendance_Click(object sender, EventArgs e)
    {
        lbldisplay.Text = "Loading...";

        if (ddlMonths.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (txtFrom.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (txtTo.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddlcategory.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category.');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (txtEmployeeType.SelectedItem.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly.');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }

        try
        {

            if (ErrFlag != true)
            {

                DataColumn auto = new DataColumn();
                auto.AutoIncrement = true;
                auto.AutoIncrementSeed = 1;

                Wages = txtEmployeeType.SelectedItem.Text;

                AutoDataTable.Columns.Add("MachineID");
                AutoDataTable.Columns.Add("Token No");
                AutoDataTable.Columns.Add("EmpName");
                AutoDataTable.Columns.Add("8 Hours");
                AutoDataTable.Columns.Add("N/FH");
                AutoDataTable.Columns.Add("OT Days");
                AutoDataTable.Columns.Add("SPG Allow");
                AutoDataTable.Columns.Add("Canteen Days Minus");
                AutoDataTable.Columns.Add("OT Hours");
                AutoDataTable.Columns.Add("W.H");
                AutoDataTable.Columns.Add("Fixed W.Days");
                AutoDataTable.Columns.Add("NFH W.Days");
                AutoDataTable.Columns.Add("Total Month Days");
                AutoDataTable.Columns.Add("MachineID_Encrypt");
                AutoDataTable.Columns.Add("NFH Worked Days");
                AutoDataTable.Columns.Add("NFH D.W Statutory");
                AutoDataTable.Columns.Add("DeptName");

                fromdate = txtFrom.Text;
                todate = txtTo.Text;
                //NewDate1 = Convert.ToDateTime(fromdate);
                //string Query_Fromdate = String.Format(Convert.ToString(NewDate1), "yyyy/MM/dd");
                //NewDate1 = Convert.ToDateTime(todate);
                //string Query_Todate = String.Format(Convert.ToString(NewDate1), "yyyy/MM/dd");
                
                //Spin_Wages = Request.QueryString["Wages"].ToString();
                // Wages = txtEmployeeType.SelectedItem.Text;


                DataTable dtIPaddress = new DataTable();
                //dtIPaddress = objdata.BelowFourHours(SessionCcode.ToString(), SessionLcode.ToString());


                //if (dtIPaddress.Rows.Count > 0)
                //{
                //    for (int i = 0; i < dtIPaddress.Rows.Count; i++)
                //    {
                //        if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                //        {
                //            mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                //        }
                //        else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                //        {
                //            mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                //        }
                //    }
                //}

                Payroll_Att_Fill();
                Write_payroll_attenance();


                if (Datacells.Rows.Count > 0)
                {


                    string SSQL;

                    for (int i = 0; i < Datacells.Rows.Count; i++)
                    {

                        //Check the Old Attendance for the Date 
                        Error_Query = "";
                        Error_Query = "delete from AttenanceDetails where Months='" + ddlMonths.SelectedItem.Text + "' and FromDate =convert(datetime,'" + fromdate + "', 105)" +
                             " and ToDate=convert(datetime,'" + todate + "', 105) and Ccode='" + SessionCcode + "'   " +
                             " and Lcode='" + SessionLcode + "' and ExistingCode='" + Datacells.Rows[i]["Token No"].ToString() + "'";
                        objdata.ReturnMultipleValue(Error_Query);


                        DateTime Fdate = Convert.ToDateTime(fromdate.ToString());
                        DateTime Tdate = Convert.ToDateTime(todate.ToString());
                        DateTime CreatedDate = Convert.ToDateTime(txtsalaryday.Text);
                        SSQL = "";
                        DataTable dept = new DataTable();
                        string deptname = AutoDataTable.Rows[i]["DeptName"].ToString();
                        SSQL = "select DepartmentCd from [Poongothai_Epay]..MstDepartment where DepartmentNm = '" + deptname.ToString() + "'";
                        dept = objdata.ReturnMultipleValue(SSQL);

                        SSQL = "insert into [Poongothai_Epay]..AttenanceDetails(DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,";
                        SSQL = SSQL + "CreatedDate,Years,TotalDays,Ccode,Lcode,NFh,WorkingDays,CL,AbsentDays,weekoff,";
                        SSQL = SSQL + "FromDate,ToDate,Modeval,home,halfNight,FullNight,ThreeSided,OTHoursNew,Fixed_Work_Days,";
                        SSQL = SSQL + "WH_Work_Days,NFH_Work_Days,NFH_Work_Days_Incentive,NFH_Work_Days_Statutory)";
                        SSQL = SSQL + "values(";

                        if (dept.Rows.Count > 0)
                        {
                            SSQL = SSQL + "'" + dept.Rows[0]["DepartmentCd"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0',";
                        }

                        SSQL = SSQL + "'" + Datacells.Rows[i]["MachineID"].ToString() + "',";
                        SSQL = SSQL + "'" + Datacells.Rows[i]["Token No"].ToString() + "',";
                        SSQL = SSQL + "'" + Datacells.Rows[i]["Days"].ToString() + "',";
                        SSQL = SSQL + "'" + ddlMonths.SelectedItem.Text + "',";
                        SSQL = SSQL + "'" + Datacells.Rows[i]["Year"].ToString() + "',";
                        SSQL = SSQL + "'" + CreatedDate.ToString("MM/dd/yyyy") + "',";
                        SSQL = SSQL + "'" + Datacells.Rows[i]["Year"].ToString() + "',";
                        SSQL = SSQL + "'" + Datacells.Rows[i]["Total Month Days"].ToString() + "',";
                        SSQL = SSQL + "'" + SessionCcode + "',";
                        SSQL = SSQL + "'" + SessionLcode + "',";
                        SSQL = SSQL + "'0',";
                        SSQL = SSQL + "'" + Datacells.Rows[i]["W.H"].ToString() + "',";
                        SSQL = SSQL + "'0','0',";
                        SSQL = SSQL + "'" + Datacells.Rows[i]["W.H"].ToString() + "',";
                        SSQL = SSQL + "'" + Fdate.ToString("MM/dd/yyyy") + "',";
                        SSQL = SSQL + "'" + Tdate.ToString("MM/dd/yyyy") + "',";
                        SSQL = SSQL + "'0','0.0',";
                        SSQL = SSQL + "'0.0','0.0','0.0',";
                        SSQL = SSQL + "'" + Datacells.Rows[i]["OT Hours"].ToString() + "',";
                        SSQL = SSQL + "'" + Datacells.Rows[i]["Fixed W.Days"].ToString() + "',";
                        SSQL = SSQL + "'" + Datacells.Rows[i]["W.H"].ToString() + "',";
                        SSQL = SSQL + "'0.0','0.0','0.0')";
                        lbldisplay.Text = Error_Query;
                        //string query1 = "insert into [Krishna_Epay]..Query_Check(Query_Check) values('" + SSQL + "')";
                        objdata.ReturnMultipleValue(SSQL);
                    }

                }
                if (ErrFlag == false)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Data in attendance Details successfully');", true);
                    lbldisplay.Text = "Completed..";

                }
            }
        }
        catch (Exception ex)
        {
            lbldisplay.Text = Error_Query + " Error : " + ex.Message.ToString();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }

    }



    public void Payroll_Att_Fill()
    {
        if (fromdate != "" && todate != "")
        {

            //SSQL = "";
            //SSQL = "select MachineID,MachineID_Encrypt";
            //SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
            //SSQL = SSQL + ",ExistingCode";
            //SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode.ToString() + "'";
            //SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "' And IsActive='Yes' and Wages='" + Wages + "' order by MachineID asc";


            SSQL = "";
            SSQL = "select MachineID,MachineID_Encrypt";
            SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + ",ExistingCode,DeptName";
            SSQL = SSQL + " from [Poongothai]..Employee_Mst Where Compcode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And Wages='" + txtEmployeeType.SelectedItem.Text + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "' And IsActive='Yes' and CatName='" + ddlcategory.SelectedItem.Text + "' order by MachineID asc";


            dsEmployee = objdata.ReturnMultipleValue(SSQL);
            int i1 = 0;
            if (dsEmployee.Rows.Count > 0)
            {
                //AutoDataTable.Rows.Count = 0;
                //AutoDataTable.Columns.Count = 0;
                //int rcount = dsEmployee.Rows.Count + 3;
                //int ccount = dsEmployee.Columns.Count + 1;
                //int ccounted = dsEmployee.Columns.Count;

                //AutoDataTable.Rows.Add();
                DataColumn dc = new DataColumn();

                for (int j = 0; j < dsEmployee.Rows.Count; j++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();





                    AutoDataTable.Rows[i1][1] = dsEmployee.Rows[j]["MachineID"].ToString();

                    string EmpID = dsEmployee.Rows[j]["MachineID"].ToString();
                    DataTable dtdEmpDet = new DataTable();

                    if (EmpID == "")
                    {

                    }

                    SSQL = "select EmpNo from EmployeeDetails where ExisistingCode= '" + EmpID + "' ";
                    SSQL = SSQL + "and Ccode='" + SessionCcode + "' ";
                    SSQL = SSQL + "and Lcode='" + SessionLcode + "' ";

                    dtdEmpDet = objdata.ReturnMultipleValue(SSQL);

                    if (dtdEmpDet.Rows.Count > 0)
                    {
                        AutoDataTable.Rows[i1][0] = dtdEmpDet.Rows[0]["EmpNo"].ToString();
                    }
                    //Datacells.Rows[i1][0] = dsEmployee.Rows[j]["MachineID"].ToString();
                    AutoDataTable.Rows[i1][13] = dsEmployee.Rows[j]["MachineID_Encrypt"].ToString();
                    //Datacells.Rows[i1][13] = dsEmployee.Rows[j]["MachineID_Encrypt"].ToString();
                    AutoDataTable.Rows[i1][2] = dsEmployee.Rows[j]["FirstName"].ToString();
                    //Datacells.Rows[i1][2] = dsEmployee.Rows[j]["FirstName"].ToString();
                    Encry_MachineID = dsEmployee.Rows[j]["MachineID_Encrypt"].ToString();

                    AutoDataTable.Rows[i1]["DeptName"] = dsEmployee.Rows[j]["DeptName"].ToString();
                    i1++;
                }

            }

        }
    }
    public void Write_payroll_attenance()
    {
        try
        {

            DataColumn auto = new DataColumn();
            auto.AutoIncrement = true;
            auto.AutoIncrementSeed = 1;

         

            Datacells.Columns.Add("MachineID");
            Datacells.Columns.Add("Token No");
            Datacells.Columns.Add("EmpName");
            Datacells.Columns.Add("Days");
            Datacells.Columns.Add("8 Hours");

            Datacells.Columns.Add("N/FH");
            Datacells.Columns.Add("OT Days");
            Datacells.Columns.Add("SPG Allow");
            Datacells.Columns.Add("Canteen Days Minus");
            Datacells.Columns.Add("OT Hours");

            Datacells.Columns.Add("W.H");
            Datacells.Columns.Add("Fixed W.Days");
            Datacells.Columns.Add("NFH W.Days");
            Datacells.Columns.Add("Total Month Days");
            Datacells.Columns.Add("NFH Worked Days");
            Datacells.Columns.Add("NFH D.W Statutory");
            Datacells.Columns.Add("Year");

            //Datacells.Columns.Add("MachineID_Encrypt");
            //Datacells.Columns.Add("Total");

            int aintCol = 4;
            int Month_Name_Change = 1;
            TimeSpan ts_get;


            //string ds1 = string.Format("{dd/MM/yyyy}", Txtfrom.Text);
            date1 = Convert.ToDateTime(fromdate);
            // string dd =string.Format("{dd/MM/yyyy}",TxtTo.Text);
            date2 = Convert.ToDateTime(todate);
            int dayCount = (int)((date2 - date1).TotalDays);
            int daysAdded = 0;
            Att_Already_Date_Check = "";

            AutoDataTable.Columns.Add("Days");

            while (dayCount >= 0)
            {
                DateTime d1 = date1.AddDays(daysAdded);
                string date_str = Convert.ToString(d1);

                Att_Date_Check = date_str.Split('/');
                if (aintCol == 3)
                {
                    Att_Already_Date_Check = Att_Date_Check[1];
                    Att_Year_Check = Att_Date_Check[2];

                    Month_Name_Change = Convert.ToUInt16(Att_Already_Date_Check);
                    aintCol = aintCol + 1;
                }
                else
                {
                }

                dayCount = dayCount - 1;
                daysAdded = daysAdded + 1;
            }
            aintI = 0;
            aintK = 1;

            for (int intRow = 0; intRow < AutoDataTable.Rows.Count; intRow++)
            {
                aintK = 1;

                Datacells.NewRow();
                Datacells.Rows.Add();
                TimeSpan TempTimeSpan;
                string Total_TIme_work = "00:00";
                ArrayList OT_Array_Value = new ArrayList();
                Int32 Total_Calculate = 0;

                //for (int j = 0; j < dsEmployee.Rows.Count; j++)
                //{
                //    AutoDataTable.NewRow();
                //    AutoDataTable.Rows.Add();
                //    AutoDataTable.Rows[i1][0] = dsEmployee.Rows[j]["MachineID"].ToString();
                //    AutoDataTable.Rows[i1][2] = dsEmployee.Rows[j]["FirstName"].ToString();
                //    i1++;
                //}

                for (aintCol = 0; aintCol <= 1; aintCol++)
                {
                    aintK += 1;
                }

                string Emp_WH_Day;
                DataTable DS_WH = new DataTable();
                string DOJ_Date_Str;
                SSQL = "Select * from [Poongothai]..Employee_MST where MachineID='" + AutoDataTable.Rows[intRow][1].ToString() + "' And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                DS_WH = objdata.ReturnMultipleValue(SSQL);

                if (DS_WH.Rows.Count != 0)
                {
                    Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                    DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();

                }
                else
                {
                    Emp_WH_Day = "";
                    DOJ_Date_Str = "";
                }

                int colIndex = aintK;

                //decimal Present_Count = 0;
                decimal Month_WH_Count = 0;
                //decimal Present_WH_Count = 0;
                //Int32 Appsent_Count = 0;
                //decimal Final_Count = 0;
                decimal Total_Days_Count = 0;
                decimal Month_Mid_Join_Total_Days_Count = 0;

                string Already_Date_Check = null;
                string Year_Check = null;
                Int32 Days_Insert_RowVal = 0;

                //decimal FullNight_Shift_Count = 0;
                // Int32 NFH_Days_Count = 0;
                //decimal NFH_Days_Present_Count = 0;
                Already_Date_Check = "";
                Year_Check = "";
                TimeSpan Total_Hr = new TimeSpan();
                double OT_Time = 0;

                for (aintCol = 0; aintCol < daysAdded; aintCol++)
                {
                    string MID ="";
                    if (Spin_Machine_ID_Str == "062016120")
                    {
                        Spin_Machine_ID_Str = "062016120";
                    }
                    time_Check_dbl = 0;

                    Spin_Machine_ID_Str = AutoDataTable.Rows[intRow]["MachineID"].ToString();
                    MID=AutoDataTable.Rows[intRow]["Token No"].ToString();
                    if (MID == "8122")
                    {
                        MID = "8122";
                    }

                    // Machine_ID_Str = Datacells.Rows[aintI]["MachineID"].ToString();
                    Machine_ID_Str = AutoDataTable.Rows[intRow]["MachineID_Encrypt"].ToString();
                    OT_Week_OFF_Machine_No = AutoDataTable.Rows[intRow]["MachineID"].ToString();

                   

                    NewDate1 = Convert.ToDateTime(fromdate);

                    dtime = NewDate1.AddDays(aintCol);
                    dtime1 = NewDate1.AddDays(aintCol).AddDays(1);
                    Date_Value_Str = String.Format(Convert.ToString(dtime), "yyyy/MM/dd");
                    Date_Value_Str1 = String.Format(Convert.ToString(dtime1), "yyyy/MM/dd");

                    DataTable mLocalDS1 = new DataTable();

                    //ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                    //ss = ss + " Where LT.Compcode='" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    //ss = ss + "And LT.TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    //ss = ss + "And LT.TimeIN <='" + dtime1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    //ss = ss + "AND em.ShiftType='" + ddlshifttype.SelectedItem.Text + "' And EM.Compcode='" + SessionCcode.ToString() + "'";
                    //ss = ss + "And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsActive='yes' Group By LT.MachineID Order By Min(LT.TimeIN)";
                                                                                
                    SSQL = "Select TimeIN from [Poongothai]..LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);

                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Time_IN_Str = "";
                    }

                    SSQL = "Select TimeOUT from [Poongothai]..LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT ASC";
                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);

                    if (mLocalDS1.Rows.Count <= 0)
                    {
                        Time_Out_Str = "";
                    }


                    if (mLocalDS.Rows.Count != 0)
                    {

                        InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"]);
                        InToTime_Check = InTime_Check.AddHours(2);
                        string s1 = InTime_Check.ToString("HH:mm:ss");
                        string s2 = InToTime_Check.ToString("HH:mm:ss");
                        //string InTime_Check_str = String.Format("HH:mm:ss", s1);
                        //string InToTime_Check_str = String.Format("HH:mm:ss", s2);
                        InTime_TimeSpan = TimeSpan.Parse(s1);
                        // From_Time_Str = Convert.ToString(InTime_TimeSpan.Hour) + ":" + Convert.ToString(InTime_TimeSpan.Minute);
                        From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                        TimeSpan InTime_TimeSpan1 = TimeSpan.Parse(s2);
                        To_Time_Str = InTime_TimeSpan1.Hours + ":" + InTime_TimeSpan1.Minutes;

                        SSQL = "Select TimeOUT from [Poongothai]..LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + From_Time_Str + "' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";
                        DS_Time = objdata.ReturnMultipleValue(SSQL);

                        //if (DS_Time.Rows.Count != 0)
                        //{
                        SSQL = "Select TimeIN from [Poongothai]..LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                        DS_InTime = objdata.ReturnMultipleValue(SSQL);

                        //if (DS_InTime.Rows.Count != 0)
                        //{
                        Final_InTime = mLocalDS.Rows[0][0].ToString();
                        SSQL = "Select * from [Poongothai]..Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                        Shift_DS_Change = objdata.ReturnMultipleValue(SSQL);
                        Shift_Check_blb = false;

                        for (int K = 0; K < Shift_DS_Change.Rows.Count; K++)
                        {
                            DateTime DataValueStr = Convert.ToDateTime(Date_Value_Str.ToString());
                            DateTime DataValueStr1 = Convert.ToDateTime(Date_Value_Str1.ToString());
                          
                            Shift_Start_Time_Change = DataValueStr.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
                            if (Shift_DS_Change.Rows[K]["EndIN_Days"].ToString() == "1")
                            {
                                Shift_End_Time_Change = DataValueStr1.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                            }
                            else
                            {
                                Shift_End_Time_Change = DataValueStr.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                            }

                            ShiftdateStartIN_Change = Convert.ToDateTime(Shift_Start_Time_Change);
                            ShiftdateEndIN_Change = Convert.ToDateTime(Shift_End_Time_Change);
                            EmpdateIN_Change = Convert.ToDateTime(Final_InTime);
                            if (EmpdateIN_Change >= ShiftdateStartIN_Change & EmpdateIN_Change <= ShiftdateEndIN_Change)
                            {
                                Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                                Shift_Check_blb = true;
                                break;
                            }
                        }
                        if (Shift_Check_blb == true)
                        {
                            SSQL = "Select TimeIN from [Poongothai]..LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                            mLocalDS = objdata.ReturnMultipleValue(SSQL);

                            if (Final_Shift == "SHIFT2")
                            {
                                SSQL = "Select TimeOUT from [Poongothai]..LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
                            }
                            else if (Final_Shift == "SHIFT1")
                            {
                                SSQL = "Select TimeIN from [Poongothai]..LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "00:00' And TimeIN <='" + dtime.ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeIN ASC";

                                mLocalDS = objdata.ReturnMultipleValue(SSQL);



                                SSQL = "Select TimeOUT from [Poongothai]..LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + dtime.ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeOUT Asc";
                                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                            }
                            else
                            {
                                SSQL = "Select TimeOUT from [Poongothai]..LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "17:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                            }


                        }
                        else
                        {
                            SSQL = "Select TimeIN from [Poongothai]..LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

                            mLocalDS = objdata.ReturnMultipleValue(SSQL);

                            SSQL = "Select TimeOUT from [Poongothai]..LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

                            mLocalDS1 = objdata.ReturnMultipleValue(SSQL);

                        }

                    }

                    string Emp_Total_Work_Time_1 = "00:00";

                    if (mLocalDS.Rows.Count > 1)
                    {
                        for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();

                            if (mLocalDS1.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_Out_Str = "";
                            }


                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                            }
                            if (Time_IN_Str == "" || Time_Out_Str == "")
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {

                                date1 = System.Convert.ToDateTime(Time_IN_Str);
                                date2 = System.Convert.ToDateTime(Time_Out_Str);

                                TimeSpan ts = new TimeSpan();
                                ts = date2.Subtract(date1);
                                Total_Time_get = Convert.ToString(ts.Hours);
                                ts4 = ts4.Add(ts);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;



                                string tsfour = Convert.ToString(ts4.Minutes);
                                string tsfour1 = Convert.ToString(ts4.Hours);



                                if (Left_Val(tsfour, 1) == "-" | Left_Val(tsfour1, 1) == "-" | Emp_Total_Work_Time_1 == "0:0")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }

                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = ts.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
                                    Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                }
                                else
                                {
                                    time_Check_dbl = Convert.ToInt16(time_Check_dbl) + Convert.ToInt16(Total_Time_get.ToString());
                                }

                            }
                        }
                    }
                    else
                    {
                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout <= mLocalDS1.Rows.Count - 1; tout++)
                        {
                            if (mLocalDS1.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";
                            }
                            else
                            {
                                Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                            }
                        }

                        if (Time_IN_Str == "" || Time_Out_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {


                            date1 = System.Convert.ToDateTime(Time_IN_Str);
                            date2 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts;
                            ts = date2.Subtract(date1);
                            Total_Time_get = ts.Hours.ToString();
                            ts4 = ts4.Add(ts);

                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = ts.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
                                Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";

                                }
                            }
                            else
                            {
                                time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
                            }
                        }
                    }





                    string Emp_Total_Work_Time;
                    string Final_OT_Work_Time;
                    string Final_OT_Work_Time_Val;
                    //String Emp_Total_Work_Time;
                    string Employee_Week_Name;
                    string Assign_Week_Name;
                    //mLocalDS == null;

                    Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                    SSQL = "Select WeekOff from [Poongothai]..Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "'";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Assign_Week_Name = "";
                    }
                    else
                    {
                        Assign_Week_Name = "";
                    }
                    Boolean NFH_Day_Check = false;
                    DateTime NFH_Date;
                    DateTime DOJ_Date_Format;
                    DateTime Date_Value = new DateTime();

                    Date_Value = Convert.ToDateTime(Date_Value_Str.ToString());
                    string qry_nfh = "Select NFHDate from [Poongothai]..NFH_Mst where NFHDate='" + Date_Value.ToString("yyyy/MM/dd") + "'";

                    // string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                    mLocalDS = objdata.ReturnMultipleValue(qry_nfh);


                    if (Employee_Week_Name.ToString() == Assign_Week_Name.ToString())
                    {

                    }
                    else
                    {
                        double Calculate_Attd_Work_Time = 0;
                        double Calculate_Attd_Work_Time_half = 0;
                        SSQL = "Select * from [Poongothai]..Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and Calculate_Work_Hours <> '' And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And IsActive='Yes'";
                        mLocalDS = objdata.ReturnMultipleValue(SSQL);

                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Calculate_Attd_Work_Time = 7;


                        }
                        else
                        {

                            //Calculate_Attd_Work_Time = (!string.IsNullOrEmpty(mLocalDS.Rows[0]["Calculate_Work_Hours"]) ? mLocalDS.Rows[0]["Calculate_Work_Hours"] : 0);
                            if (Calculate_Attd_Work_Time == 0 || Calculate_Attd_Work_Time == null)
                            {
                                Calculate_Attd_Work_Time = 7;
                            }
                            else
                            {
                                Calculate_Attd_Work_Time = 7;
                            }


                        }
                        Calculate_Attd_Work_Time_half = 4.0;
                        halfPresent = "0";

                        if (time_Check_dbl >= Calculate_Attd_Work_Time_half & time_Check_dbl < Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "1";
                        }

                        else if (time_Check_dbl >= Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "0";
                        }
                        else
                        {
                            isPresent = false;
                            halfPresent = "0";
                        }

                    }

                    //OT Calculation start
                    Int32 Final_OT_Time_Check = 0;
                    double Work_Time = 0;
                    Final_OT_Time_Check = 9;


                    if (time_Check_dbl >= Final_OT_Time_Check)
                    {
                        
                        isPresent = true;
                        string Time_Minus = "08:00";
                        if (Work_Time == 0)
                        {
                            Time_Minus = "08:00";
                        }
                        else
                        {
                            Time_Minus = Work_Time + ":00";
                        }
                        DateTime date_TotalTime = Convert.ToDateTime(Emp_Total_Work_Time_1);
                        DateTime date_TotalTimeMinus = Convert.ToDateTime(Time_Minus);
                        TimeSpan TSminus;
                        TSminus = date_TotalTimeMinus.Subtract(date_TotalTime);
                        TSminus = date_TotalTimeMinus.Subtract(date_TotalTime);
                        if (Left_Val(Convert.ToString(TSminus.Hours), 1) == "-")
                        {
                            string TSminus_str1 = Convert.ToString(TSminus.Hours);
                            int l1 = TSminus_str1.Length;
                            Final_OT_Work_Time = Right_Val(Convert.ToString(TSminus.Hours), l1 - 1);
                            Final_OT_Work_Time_Val = Final_OT_Work_Time;
                        }
                        else
                        {
                            Final_OT_Work_Time = Convert.ToString(TSminus.Hours);
                            Final_OT_Work_Time_Val = Final_OT_Work_Time;
                        }
                        if (Left_Val(Convert.ToString(TSminus.Minutes), 1) == "-")
                        {
                            string TSminus_str2 = Convert.ToString(TSminus.Minutes);
                            int l2 = TSminus_str2.Length;
                            Final_OT_Work_Time = Final_OT_Work_Time + ":" + Right_Val(Convert.ToString(TSminus.Minutes), l2 - 1);
                            Final_OT_Work_Time_Val = Final_OT_Work_Time;
                        }
                        else
                        {
                            Final_OT_Work_Time = Final_OT_Work_Time + ":" + TSminus.Minutes;
                            Final_OT_Work_Time_Val = Final_OT_Work_Time;
                        }
                        ts_get = Convert.ToDateTime(string.Format("{0:hh:mm}", Final_OT_Work_Time_Val)).TimeOfDay;

                        TimeSpan tt = ts_get;

                        TempTimeSpan = tt;
                        Total_TIme_work = tt.Hours + ":" + tt.Minutes;
                        if (isPresent == true)
                        {
                            OT_Array_Value.Add(Final_OT_Work_Time);
                            Total_Calculate = Total_Calculate + 1;

                         

                                TimeSpan temp1 = Convert.ToDateTime(string.Format("{0:hh:mm}", Final_OT_Work_Time)).TimeOfDay;
                                Total_Hr = Total_Hr.Add(temp1);
                                OT_Time = OT_Time + (time_Check_dbl - 8);
                           
             
                        }
                        else
                        {
                            OT_Array_Value.Add("0");
                        }

                        //Final_OT_Work_Time = Trim(TSminus.Hours) & ":" & Trim(TSminus.Minutes)
                    }








                    //OT Calculation Stop
                 



                    string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                    if (Emp_WH_Day == Attn_Date_Day)
                    {

                        Month_WH_Count = Month_WH_Count + 1;
                        if (isPresent == true)
                        {

                            if (halfPresent == "1")
                            {
                                Present_WH_Count = Convert.ToDouble(Present_WH_Count + 0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_WH_Count = Present_WH_Count + 1;
                            }

                        }

                    }

                    if (isPresent == true)
                    {

                        if (halfPresent == "1")
                        {
                            Present_Count = Convert.ToDouble(Present_Count) + 0.5;
                            if (NFH_Day_Check == true)
                            {
                                NFH_Days_Present_Count = Convert.ToDouble(NFH_Days_Present_Count) + 0.5;
                            }
                        }
                        else if (halfPresent == "0")
                        {
                            Present_Count = Present_Count + 1;
                            if (NFH_Day_Check == true)
                            {
                                NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                            }
                        }


                    }
                    else
                    {
                        Appsent_Count = Appsent_Count + 1;
                    }


                    colIndex += shiftCount;
                    aintK += 1;
                    Total_Days_Count = Total_Days_Count + 1;

                    //System.DateTime Report_Date = default(System.DateTime);
                    //System.DateTime DOJ_Date_Format_Check = default(System.DateTime);
                    //if (Wages == "STAFF salary" | Wages == "Watch & Ward")
                    //{
                    //    if (!string.IsNullOrEmpty(DOJ_Date_Str))
                    //    {
                    //        Report_Date = Convert.ToDateTime(Date_Value_Str.ToString());
                    //        DOJ_Date_Format_Check = Convert.ToDateTime(DOJ_Date_Str.ToString());
                    //        if (DOJ_Date_Format_Check.Date <= Report_Date.Date)
                    //        {
                    //            Month_Mid_Join_Total_Days_Count = Month_Mid_Join_Total_Days_Count + 1;
                    //        }
                    //        else
                    //        {
                    //        }
                    //    }
                    //    else
                    //    {
                    //        Month_Mid_Join_Total_Days_Count = Month_Mid_Join_Total_Days_Count + 1;
                    //    }
                    //    //Month_Mid_Join_Total_Days_Count = Month_Mid_Join_Total_Days_Count + 1;
                    //    Total_Days_Count = Total_Days_Count + 1;
                    //}
                    //else
                    //{
                    //    Month_Mid_Join_Total_Days_Count = Month_Mid_Join_Total_Days_Count + 1;
                    //    Total_Days_Count = Total_Days_Count + 1;
                    //}
                }


                Final_Count = Present_Count;
                //if (NFH_Days_Count == 0)
                //{
                //    NFH_Days_Present_Count = 0;
                //}
                string Token_No;
                string Machine_ID_Str_Excel;
                DataTable Token_DS = new DataTable();



                Machine_ID_Str_Excel = Datacells.Rows[aintI][0].ToString();

                SSQL = "Select * from [Poongothai]..Employee_Mst where MachineID='" + Datacells.Rows[aintI][1].ToString() + "' and Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
                Token_DS = objdata.ReturnMultipleValue(SSQL);
                if (Token_DS.Rows.Count != 0)
                {
                    Token_No_Get = Token_DS.Rows[0]["ExistingCode"].ToString();
                    Int32 Fixed_Work_Days = 0;
                }

                if (Wages == "STAFF")
                {
                    Fixed_Work_Days = Total_Days_Count - Month_WH_Count;
                    Final_Count = Final_Count - Present_WH_Count;
                    Final_Count = Final_Count - NFH_Days_Present_Count;
                    Present_WH_Count = Present_WH_Count + NFH_Days_Present_Count;
                    Present_WH_Count = Present_WH_Count + NFH_Days_Present_Count;
                }

                Fixed_Work_Days = Total_Days_Count - Month_WH_Count;
                Final_Count = Final_Count - Present_WH_Count;
                Final_Count = Final_Count - NFH_Days_Present_Count;
                Present_WH_Count = Present_WH_Count + NFH_Days_Present_Count;
                Present_WH_Count = Present_WH_Count + NFH_Days_Present_Count;



                string NFH_Year_Str = "0";
                string NFH_Month_Str = "";
                string NFH_Rule_OLD = "";
                int NFH_Display_Days = 0;
                string NFH_Double_Wages_Rule = "";
                DataTable mDataSet = new DataTable();
                NFH_Year_Str = Convert.ToString(date1.Year);
                NFH_Month_Str = Convert.ToString(date1.Month);
                //SSQL = "Select * from NFH_Rule_Det where NFHYear='" + NFH_Year_Str + "' And Months='" + NFH_Month_Str + "'";
                if (Final_Count != 0)
                {

                    Datacells.Rows[aintI][0] = AutoDataTable.Rows[intRow]["MachineID"].ToString();
                    Datacells.Rows[aintI][2] = AutoDataTable.Rows[intRow]["EmpName"].ToString();
                    //Datacells.Rows[aintI][13] = AutoDataTable.Rows[intRow]["MachineID_Encrypt"].ToString();
                    Datacells.Rows[aintI][1] = AutoDataTable.Rows[intRow]["Token No"].ToString();
                    Datacells.Rows[aintI]["NFH Worked Days"] = "0";
                    Datacells.Rows[aintI]["NFH D.W Statutory"] = "0";

                    //Datacells.Rows[aintI][1] = Machine_ID_Str_Excel;
                    Datacells.Rows[aintI][3] = Final_Count;
                    Datacells.Rows[aintI][4] = EHours_Count;
                    Datacells.Rows[aintI][5] = NFH_Days_Count;
                    Datacells.Rows[aintI][6] = "0";
                    Datacells.Rows[aintI][7] = "0";
                    Datacells.Rows[aintI][9] = Present_WH_Count;
                    Datacells.Rows[aintI][10] = Month_WH_Count;
                    Datacells.Rows[aintI][11] = Fixed_Work_Days;
                    Datacells.Rows[aintI][9] = OT_Time;
                    Datacells.Rows[aintI]["Year"] = NFH_Year_Str;

                    if (Wages == "STAFF salary" || Wages == "Watch & Ward")
                    {

                        Datacells.Rows[aintI][8] = Total_Hr;
                        // Datacells.Rows[aintI][12] = Present_WH_Count;
                        Datacells.Rows[aintI][12] = NFH_Days_Present_Count;
                        Datacells.Rows[aintI][13] = Total_Days_Count;
                    }
                    else
                    {
                        Datacells.Rows[aintI][8] = 0;
                        Datacells.Rows[aintI][12] = 0;
                        Datacells.Rows[aintI][13] = 0;
                    }

                    //  Datacells.Rows[aintI][14] = NFH_Days_Present_Count;
                    // Datacells.Rows[aintI][15] = 
                }
                else
                {
                    Datacells.Rows[aintI][0] = AutoDataTable.Rows[intRow]["MachineID"].ToString();
                    Datacells.Rows[aintI][2] = AutoDataTable.Rows[intRow]["EmpName"].ToString();
                    // Datacells.Rows[aintI][13] = AutoDataTable.Rows[intRow]["MachineID_Encrypt"].ToString();
                    Datacells.Rows[aintI][1] = AutoDataTable.Rows[intRow]["Token No"].ToString();
                    Datacells.Rows[aintI]["NFH Worked Days"] = "0";
                    Datacells.Rows[aintI]["NFH D.W Statutory"] = "0";


                    //Datacells.Rows[aintI][1] = Machine_ID_Str_Excel;
                    //Datacells.Rows[aintI][2] = Token_No_Get;
                    Datacells.Rows[aintI][3] = Final_Count;
                    Datacells.Rows[aintI][4] = EHours_Count;
                    Datacells.Rows[aintI][5] = NFH_Days_Count;
                    Datacells.Rows[aintI][6] = "0";
                    Datacells.Rows[aintI][7] = "0";
                    Datacells.Rows[aintI][9] = Present_WH_Count;
                    Datacells.Rows[aintI][10] = Month_WH_Count;
                    Datacells.Rows[aintI][11] = Fixed_Work_Days;

                    if (Wages == "STAFF salary" || Wages == "Watch & Ward")
                    {
                        //Datacells.Rows[aintI][8] = 0;
                        //Datacells.Rows[aintI][12] = Present_WH_Count;
                        //Datacells.Rows[aintI][13] = Fixed_Work_Days;
                        Datacells.Rows[aintI][8] = "0";
                        //Datacells.Rows[aintI][12] = Present_WH_Count;
                        Datacells.Rows[aintI][12] = NFH_Days_Present_Count;
                        Datacells.Rows[aintI][13] = Total_Days_Count;
                    }
                    else
                    {

                        Datacells.Rows[aintI][8] = Present_WH_Count;
                        Datacells.Rows[aintI][12] = 0;
                        Datacells.Rows[aintI][13] = 0;
                    }

                    //Datacells.Rows[aintI][14] = NFH_Days_Present_Count;
                    //  Datacells.Rows[aintI][15] = Total_Days_Count;


                }

                aintI = aintI + 1;
                Total_Days_Count = 0;
                Final_Count = 0;
                Appsent_Count = 0;
                Present_Count = 0;
                Fixed_Work_Days = 0;
                Month_WH_Count = 0;
                Present_WH_Count = 0;
                NFH_Days_Count = 0;
                NFH_Days_Present_Count = 0;
                EHours_Count = 0;

            }


        }

        catch (Exception e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
        }

        //UploadDataTableToExcel(Datacells);
    }



    public void Write_payroll_attenance1()
    {
        try
        {

            DataColumn auto = new DataColumn();
            auto.AutoIncrement = true;
            auto.AutoIncrementSeed = 1;



            Datacells.Columns.Add("MachineID");
            Datacells.Columns.Add("Token No");
            Datacells.Columns.Add("EmpName");
            Datacells.Columns.Add("Days");
            Datacells.Columns.Add("8 Hours");

            Datacells.Columns.Add("N/FH");
            Datacells.Columns.Add("OT Days");
            Datacells.Columns.Add("SPG Allow");
            Datacells.Columns.Add("Canteen Days Minus");
            Datacells.Columns.Add("OT Hours");

            Datacells.Columns.Add("W.H");
            Datacells.Columns.Add("Fixed W.Days");
            Datacells.Columns.Add("NFH W.Days");
            Datacells.Columns.Add("Total Month Days");
            Datacells.Columns.Add("NFH Worked Days");
            Datacells.Columns.Add("NFH D.W Statutory");
            Datacells.Columns.Add("Year");

            //Datacells.Columns.Add("MachineID_Encrypt");
            //Datacells.Columns.Add("Total");

            int aintCol = 4;
            int Month_Name_Change = 1;
            TimeSpan ts_get;


            //string ds1 = string.Format("{dd/MM/yyyy}", Txtfrom.Text);
            date1 = Convert.ToDateTime(fromdate);
            // string dd =string.Format("{dd/MM/yyyy}",TxtTo.Text);
            date2 = Convert.ToDateTime(todate);
            int dayCount = (int)((date2 - date1).TotalDays);
            int daysAdded = 0;
            Att_Already_Date_Check = "";

            AutoDataTable.Columns.Add("Days");

            while (dayCount >= 0)
            {
                DateTime d1 = date1.AddDays(daysAdded);
                string date_str = Convert.ToString(d1);

                Att_Date_Check = date_str.Split('/');
                if (aintCol == 3)
                {
                    Att_Already_Date_Check = Att_Date_Check[1];
                    Att_Year_Check = Att_Date_Check[2];

                    Month_Name_Change = Convert.ToUInt16(Att_Already_Date_Check);
                    aintCol = aintCol + 1;
                }
                else
                {
                }

                dayCount = dayCount - 1;
                daysAdded = daysAdded + 1;
            }
            aintI = 0;
            aintK = 1;

            for (int intRow = 0; intRow < AutoDataTable.Rows.Count; intRow++)
            {
                aintK = 1;

                Datacells.NewRow();
                Datacells.Rows.Add();
                TimeSpan TempTimeSpan;
                string Total_TIme_work = "00:00";
                ArrayList OT_Array_Value = new ArrayList();
                Int32 Total_Calculate = 0;

                //for (int j = 0; j < dsEmployee.Rows.Count; j++)
                //{
                //    AutoDataTable.NewRow();
                //    AutoDataTable.Rows.Add();
                //    AutoDataTable.Rows[i1][0] = dsEmployee.Rows[j]["MachineID"].ToString();
                //    AutoDataTable.Rows[i1][2] = dsEmployee.Rows[j]["FirstName"].ToString();
                //    i1++;
                //}

                for (aintCol = 0; aintCol <= 1; aintCol++)
                {
                    aintK += 1;
                }

                string Emp_WH_Day;
                DataTable DS_WH = new DataTable();
                string DOJ_Date_Str;
                SSQL = "Select * from [Poongothai]..Employee_MST where MachineID='" + AutoDataTable.Rows[intRow][1].ToString() + "' And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                DS_WH = objdata.ReturnMultipleValue(SSQL);

                if (DS_WH.Rows.Count != 0)
                {
                    Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                    DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();

                }
                else
                {
                    Emp_WH_Day = "";
                    DOJ_Date_Str = "";
                }

                int colIndex = aintK;

                //decimal Present_Count = 0;
                decimal Month_WH_Count = 0;
                //decimal Present_WH_Count = 0;
                //Int32 Appsent_Count = 0;
                //decimal Final_Count = 0;
                decimal Total_Days_Count = 0;
                decimal Month_Mid_Join_Total_Days_Count = 0;

                string Already_Date_Check = null;
                string Year_Check = null;
                Int32 Days_Insert_RowVal = 0;

                //decimal FullNight_Shift_Count = 0;
                // Int32 NFH_Days_Count = 0;
                //decimal NFH_Days_Present_Count = 0;
                Already_Date_Check = "";
                Year_Check = "";
                TimeSpan Total_Hr = new TimeSpan();
                double OT_Time = 0;

                for (aintCol = 0; aintCol < daysAdded; aintCol++)
                {
                    if (Spin_Machine_ID_Str == "062016120")
                    {
                        Spin_Machine_ID_Str = "062016120";
                    }
                    time_Check_dbl = 0;

                    Spin_Machine_ID_Str = AutoDataTable.Rows[intRow]["MachineID"].ToString();

                    // Machine_ID_Str = Datacells.Rows[aintI]["MachineID"].ToString();
                    Machine_ID_Str = AutoDataTable.Rows[intRow]["MachineID_Encrypt"].ToString();
                    OT_Week_OFF_Machine_No = AutoDataTable.Rows[intRow]["MachineID"].ToString();



                    NewDate1 = Convert.ToDateTime(fromdate);

                    dtime = NewDate1.AddDays(aintCol);
                    dtime1 = NewDate1.AddDays(aintCol).AddDays(1);
                    Date_Value_Str = String.Format(Convert.ToString(dtime), "yyyy/MM/dd");
                    Date_Value_Str1 = String.Format(Convert.ToString(dtime1), "yyyy/MM/dd");

                    DataTable mLocalDS1 = new DataTable();

                    //ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
                    //ss = ss + " Where LT.Compcode='" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    //ss = ss + "And LT.TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    //ss = ss + "And LT.TimeIN <='" + dtime1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    //ss = ss + "AND em.ShiftType='" + ddlshifttype.SelectedItem.Text + "' And EM.Compcode='" + SessionCcode.ToString() + "'";
                    //ss = ss + "And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsActive='yes' Group By LT.MachineID Order By Min(LT.TimeIN)";





                    SSQL = "Select TimeIN from [Poongothai]..LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);


                //    if (mLocalDS.Rows.Count <= 0)
                //    {
                //        Time_IN_Str = "";
                //    }

                //    SSQL = "Select TimeOUT from [Poongothai]..LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                //    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                //    SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT ASC";
                //    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);

                //    if (mLocalDS1.Rows.Count <= 0)
                //    {
                //        Time_Out_Str = "";
                //    }


                //    if (mLocalDS.Rows.Count != 0)
                //    {

                //        InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"]);

                //        InToTime_Check = InTime_Check.AddHours(2);

                //        string s1 = InTime_Check.ToString("HH:mm:ss");
                //        string s2 = InToTime_Check.ToString("HH:mm:ss");

                //        //string InTime_Check_str = String.Format("HH:mm:ss", s1);
                //        //string InToTime_Check_str = String.Format("HH:mm:ss", s2);

                //        InTime_TimeSpan = TimeSpan.Parse(s1);
                //        // From_Time_Str = Convert.ToString(InTime_TimeSpan.Hour) + ":" + Convert.ToString(InTime_TimeSpan.Minute);
                //        From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                //        TimeSpan InTime_TimeSpan1 = TimeSpan.Parse(s2);
                //        To_Time_Str = InTime_TimeSpan1.Hours + ":" + InTime_TimeSpan1.Minutes;

                //        SSQL = "Select TimeOUT from [Poongothai]..LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                //        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //        SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + From_Time_Str + "' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";
                //        DS_Time = objdata.ReturnMultipleValue(SSQL);

                //        //if (DS_Time.Rows.Count != 0)
                //        //{
                //        SSQL = "Select TimeIN from [Poongothai]..LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                //        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //        SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                //        DS_InTime = objdata.ReturnMultipleValue(SSQL);

                //        //if (DS_InTime.Rows.Count != 0)
                //        //{
                //        Final_InTime = mLocalDS.Rows[0][0].ToString();
                //        SSQL = "Select * from [Poongothai]..Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                //        Shift_DS_Change = objdata.ReturnMultipleValue(SSQL);
                //        Shift_Check_blb = false;

                //        for (int K = 0; K < Shift_DS_Change.Rows.Count; K++)
                //        {
                //            DateTime DataValueStr = Convert.ToDateTime(Date_Value_Str.ToString());
                //            DateTime DataValueStr1 = Convert.ToDateTime(Date_Value_Str1.ToString());


                //            Shift_Start_Time_Change = DataValueStr.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();

                //            if (Shift_DS_Change.Rows[K]["EndIN_Days"].ToString() == "1")
                //            {
                //                Shift_End_Time_Change = DataValueStr1.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                //            }
                //            else
                //            {
                //                Shift_End_Time_Change = DataValueStr.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                //            }




                //            ShiftdateStartIN_Change = Convert.ToDateTime(Shift_Start_Time_Change);
                //            ShiftdateEndIN_Change = Convert.ToDateTime(Shift_End_Time_Change);
                //            EmpdateIN_Change = Convert.ToDateTime(Final_InTime);

                //            if (EmpdateIN_Change >= ShiftdateStartIN_Change & EmpdateIN_Change <= ShiftdateEndIN_Change)
                //            {
                //                Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                //                Shift_Check_blb = true;
                //                break;
                //            }
                //        }
                //        if (Shift_Check_blb == true)
                //        {
                //            SSQL = "Select TimeIN from [Poongothai]..LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                //            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //            SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                //            mLocalDS = objdata.ReturnMultipleValue(SSQL);

                //            if (Final_Shift == "SHIFT2")
                //            {
                //                SSQL = "Select TimeOUT from [Poongothai]..LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                //                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //                SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
                //            }
                //            else if (Final_Shift == "SHIFT1")
                //            {
                //                SSQL = "Select TimeIN from [Poongothai]..LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                //                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //                SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "00:00' And TimeIN <='" + dtime.ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeIN ASC";

                //                mLocalDS = objdata.ReturnMultipleValue(SSQL);



                //                SSQL = "Select TimeOUT from [Poongothai]..LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                //                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //                SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + dtime.ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeOUT Asc";
                //                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                //            }
                //            else
                //            {
                //                SSQL = "Select TimeOUT from [Poongothai]..LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                //                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //                SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "17:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                //                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
                //            }


                //        }
                //        else
                //        {
                //            SSQL = "Select TimeIN from [Poongothai]..LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                //            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //            SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

                //            mLocalDS = objdata.ReturnMultipleValue(SSQL);

                //            SSQL = "Select TimeOUT from [Poongothai]..LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                //            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //            SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

                //            mLocalDS1 = objdata.ReturnMultipleValue(SSQL);

                //        }

                //    }

                //    string Emp_Total_Work_Time_1 = "00:00";

                //    if (mLocalDS.Rows.Count > 1)
                //    {
                //        for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                //        {
                //            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();

                //            if (mLocalDS1.Rows.Count > tin)
                //            {
                //                Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                //            }
                //            else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                //            {
                //                Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                //            }
                //            else
                //            {
                //                Time_Out_Str = "";
                //            }


                //            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                //            if (mLocalDS.Rows.Count <= 0)
                //            {
                //                Time_IN_Str = "";
                //            }
                //            else
                //            {
                //                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                //            }
                //            if (Time_IN_Str == "" || Time_Out_Str == "")
                //            {
                //                time_Check_dbl = time_Check_dbl;
                //            }
                //            else
                //            {

                //                date1 = System.Convert.ToDateTime(Time_IN_Str);
                //                date2 = System.Convert.ToDateTime(Time_Out_Str);

                //                TimeSpan ts = new TimeSpan();
                //                ts = date2.Subtract(date1);
                //                Total_Time_get = Convert.ToString(ts.Hours);
                //                ts4 = ts4.Add(ts);
                //                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;



                //                string tsfour = Convert.ToString(ts4.Minutes);
                //                string tsfour1 = Convert.ToString(ts4.Hours);



                //                if (Left_Val(tsfour, 1) == "-" | Left_Val(tsfour1, 1) == "-" | Emp_Total_Work_Time_1 == "0:0")
                //                {
                //                    Emp_Total_Work_Time_1 = "00:00";
                //                }

                //                if (Left_Val(Total_Time_get, 1) == "-")
                //                {
                //                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                //                    ts = date2.Subtract(date1);
                //                    ts = date2.Subtract(date1);
                //                    Total_Time_get = ts.Hours.ToString();
                //                    time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
                //                    Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
                //                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                //                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                //                    {
                //                        Emp_Total_Work_Time_1 = "00:00";
                //                    }
                //                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                //                    {
                //                        Emp_Total_Work_Time_1 = "00:00";
                //                    }
                //                }
                //                else
                //                {
                //                    time_Check_dbl = Convert.ToInt16(time_Check_dbl) + Convert.ToInt16(Total_Time_get.ToString());
                //                }

                //            }
                //        }
                //    }
                //    else
                //    {
                //        TimeSpan ts4 = new TimeSpan();
                //        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                //        if (mLocalDS.Rows.Count <= 0)
                //        {
                //            Time_IN_Str = "";
                //        }
                //        else
                //        {
                //            Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                //        }
                //        for (int tout = 0; tout <= mLocalDS1.Rows.Count - 1; tout++)
                //        {
                //            if (mLocalDS1.Rows.Count <= 0)
                //            {
                //                Time_Out_Str = "";
                //            }
                //            else
                //            {
                //                Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                //            }
                //        }

                //        if (Time_IN_Str == "" || Time_Out_Str == "")
                //        {
                //            time_Check_dbl = 0;
                //        }
                //        else
                //        {


                //            date1 = System.Convert.ToDateTime(Time_IN_Str);
                //            date2 = System.Convert.ToDateTime(Time_Out_Str);
                //            TimeSpan ts;
                //            ts = date2.Subtract(date1);
                //            Total_Time_get = ts.Hours.ToString();
                //            ts4 = ts4.Add(ts);

                //            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                //            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                //            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                //            {
                //                Emp_Total_Work_Time_1 = "00:00";
                //            }
                //            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                //            {
                //                Emp_Total_Work_Time_1 = "00:00";
                //            }

                //            if (Left_Val(Total_Time_get, 1) == "-")
                //            {
                //                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                //                ts = date2.Subtract(date1);
                //                Total_Time_get = ts.Hours.ToString();
                //                time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
                //                Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
                //                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                //                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                //                {
                //                    Emp_Total_Work_Time_1 = "00:00";
                //                }
                //                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                //                {
                //                    Emp_Total_Work_Time_1 = "00:00";

                //                }
                //            }
                //            else
                //            {
                //                time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
                //            }
                //        }
                //    }





                //    string Emp_Total_Work_Time;
                //    string Final_OT_Work_Time;
                //    string Final_OT_Work_Time_Val;
                //    //String Emp_Total_Work_Time;
                //    string Employee_Week_Name;
                //    string Assign_Week_Name;
                //    //mLocalDS == null;

                //    Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                //    SSQL = "Select WeekOff from [Poongothai]..Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "'";
                //    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                //    if (mLocalDS.Rows.Count <= 0)
                //    {
                //        Assign_Week_Name = "";
                //    }
                //    else
                //    {
                //        Assign_Week_Name = "";
                //    }
                //    Boolean NFH_Day_Check = false;
                //    DateTime NFH_Date;
                //    DateTime DOJ_Date_Format;
                //    DateTime Date_Value = new DateTime();

                //    Date_Value = Convert.ToDateTime(Date_Value_Str.ToString());
                //    string qry_nfh = "Select NFHDate from [Poongothai]..NFH_Mst where NFHDate='" + Date_Value.ToString("yyyy/MM/dd") + "'";

                //    // string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                //    mLocalDS = objdata.ReturnMultipleValue(qry_nfh);


                //    if (Employee_Week_Name.ToString() == Assign_Week_Name.ToString())
                //    {

                //    }
                //    else
                //    {
                //        double Calculate_Attd_Work_Time = 0;
                //        double Calculate_Attd_Work_Time_half = 0;
                //        SSQL = "Select * from [Poongothai]..Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and Calculate_Work_Hours <> '' And CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And IsActive='Yes'";
                //        mLocalDS = objdata.ReturnMultipleValue(SSQL);

                //        if (mLocalDS.Rows.Count <= 0)
                //        {
                //            Calculate_Attd_Work_Time = 7;


                //        }
                //        else
                //        {

                //            //Calculate_Attd_Work_Time = (!string.IsNullOrEmpty(mLocalDS.Rows[0]["Calculate_Work_Hours"]) ? mLocalDS.Rows[0]["Calculate_Work_Hours"] : 0);
                //            if (Calculate_Attd_Work_Time == 0 || Calculate_Attd_Work_Time == null)
                //            {
                //                Calculate_Attd_Work_Time = 7;
                //            }
                //            else
                //            {
                //                Calculate_Attd_Work_Time = 7;
                //            }


                //        }
                //        Calculate_Attd_Work_Time_half = 4.0;
                //        halfPresent = "0";

                //        if (time_Check_dbl >= Calculate_Attd_Work_Time_half & time_Check_dbl < Calculate_Attd_Work_Time)
                //        {
                //            isPresent = true;
                //            halfPresent = "1";
                //        }

                //        else if (time_Check_dbl >= Calculate_Attd_Work_Time)
                //        {
                //            isPresent = true;
                //            halfPresent = "0";
                //        }
                //        else
                //        {
                //            isPresent = false;
                //            halfPresent = "0";
                //        }

                //    }

                //    //OT Calculation start
                //    Int32 Final_OT_Time_Check = 0;
                //    double Work_Time = 0;
                //    Final_OT_Time_Check = 9;


                //    if (time_Check_dbl >= Final_OT_Time_Check)
                //    {

                //        isPresent = true;
                //        string Time_Minus = "08:00";
                //        if (Work_Time == 0)
                //        {
                //            Time_Minus = "08:00";
                //        }
                //        else
                //        {
                //            Time_Minus = Work_Time + ":00";
                //        }
                //        DateTime date_TotalTime = Convert.ToDateTime(Emp_Total_Work_Time_1);
                //        DateTime date_TotalTimeMinus = Convert.ToDateTime(Time_Minus);
                //        TimeSpan TSminus;
                //        TSminus = date_TotalTimeMinus.Subtract(date_TotalTime);
                //        TSminus = date_TotalTimeMinus.Subtract(date_TotalTime);
                //        if (Left_Val(Convert.ToString(TSminus.Hours), 1) == "-")
                //        {
                //            string TSminus_str1 = Convert.ToString(TSminus.Hours);
                //            int l1 = TSminus_str1.Length;
                //            Final_OT_Work_Time = Right_Val(Convert.ToString(TSminus.Hours), l1 - 1);
                //            Final_OT_Work_Time_Val = Final_OT_Work_Time;
                //        }
                //        else
                //        {
                //            Final_OT_Work_Time = Convert.ToString(TSminus.Hours);
                //            Final_OT_Work_Time_Val = Final_OT_Work_Time;
                //        }
                //        if (Left_Val(Convert.ToString(TSminus.Minutes), 1) == "-")
                //        {
                //            string TSminus_str2 = Convert.ToString(TSminus.Minutes);
                //            int l2 = TSminus_str2.Length;
                //            Final_OT_Work_Time = Final_OT_Work_Time + ":" + Right_Val(Convert.ToString(TSminus.Minutes), l2 - 1);
                //            Final_OT_Work_Time_Val = Final_OT_Work_Time;
                //        }
                //        else
                //        {
                //            Final_OT_Work_Time = Final_OT_Work_Time + ":" + TSminus.Minutes;
                //            Final_OT_Work_Time_Val = Final_OT_Work_Time;
                //        }
                //        ts_get = Convert.ToDateTime(string.Format("{0:hh:mm}", Final_OT_Work_Time_Val)).TimeOfDay;

                //        TimeSpan tt = ts_get;

                //        TempTimeSpan = tt;
                //        Total_TIme_work = tt.Hours + ":" + tt.Minutes;
                //        if (isPresent == true)
                //        {
                //            OT_Array_Value.Add(Final_OT_Work_Time);
                //            Total_Calculate = Total_Calculate + 1;



                //            TimeSpan temp1 = Convert.ToDateTime(string.Format("{0:hh:mm}", Final_OT_Work_Time)).TimeOfDay;
                //            Total_Hr = Total_Hr.Add(temp1);
                //            OT_Time = OT_Time + (time_Check_dbl - 8);


                //        }
                //        else
                //        {
                //            OT_Array_Value.Add("0");
                //        }

                //        //Final_OT_Work_Time = Trim(TSminus.Hours) & ":" & Trim(TSminus.Minutes)
                //    }








                //    //OT Calculation Stop




                //    string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                //    if (Emp_WH_Day == Attn_Date_Day)
                //    {

                //        Month_WH_Count = Month_WH_Count + 1;
                //        if (isPresent == true)
                //        {

                //            if (halfPresent == "1")
                //            {
                //                Present_WH_Count = Convert.ToDouble(Present_WH_Count + 0.5);
                //            }
                //            else if (halfPresent == "0")
                //            {
                //                Present_WH_Count = Present_WH_Count + 1;
                //            }

                //        }

                //    }

                //    if (isPresent == true)
                //    {

                //        if (halfPresent == "1")
                //        {
                //            Present_Count = Convert.ToDouble(Present_Count) + 0.5;
                //            if (NFH_Day_Check == true)
                //            {
                //                NFH_Days_Present_Count = Convert.ToDouble(NFH_Days_Present_Count) + 0.5;
                //            }
                //        }
                //        else if (halfPresent == "0")
                //        {
                //            Present_Count = Present_Count + 1;
                //            if (NFH_Day_Check == true)
                //            {
                //                NFH_Days_Present_Count = NFH_Days_Present_Count + 1;
                //            }
                //        }


                //    }
                //    else
                //    {
                //        Appsent_Count = Appsent_Count + 1;
                //    }


                //    colIndex += shiftCount;
                //    aintK += 1;
                //    Total_Days_Count = Total_Days_Count + 1;

                //    //System.DateTime Report_Date = default(System.DateTime);
                //    //System.DateTime DOJ_Date_Format_Check = default(System.DateTime);
                //    //if (Wages == "STAFF salary" | Wages == "Watch & Ward")
                //    //{
                //    //    if (!string.IsNullOrEmpty(DOJ_Date_Str))
                //    //    {
                //    //        Report_Date = Convert.ToDateTime(Date_Value_Str.ToString());
                //    //        DOJ_Date_Format_Check = Convert.ToDateTime(DOJ_Date_Str.ToString());
                //    //        if (DOJ_Date_Format_Check.Date <= Report_Date.Date)
                //    //        {
                //    //            Month_Mid_Join_Total_Days_Count = Month_Mid_Join_Total_Days_Count + 1;
                //    //        }
                //    //        else
                //    //        {
                //    //        }
                //    //    }
                //    //    else
                //    //    {
                //    //        Month_Mid_Join_Total_Days_Count = Month_Mid_Join_Total_Days_Count + 1;
                //    //    }
                //    //    //Month_Mid_Join_Total_Days_Count = Month_Mid_Join_Total_Days_Count + 1;
                //    //    Total_Days_Count = Total_Days_Count + 1;
                //    //}
                //    //else
                //    //{
                //    //    Month_Mid_Join_Total_Days_Count = Month_Mid_Join_Total_Days_Count + 1;
                //    //    Total_Days_Count = Total_Days_Count + 1;
                //    //}
                //}


                //Final_Count = Present_Count;
                ////if (NFH_Days_Count == 0)
                ////{
                ////    NFH_Days_Present_Count = 0;
                ////}
                //string Token_No;
                //string Machine_ID_Str_Excel;
                //DataTable Token_DS = new DataTable();



                //Machine_ID_Str_Excel = Datacells.Rows[aintI][0].ToString();

                //SSQL = "Select * from [Poongothai]..Employee_Mst where MachineID='" + Datacells.Rows[aintI][1].ToString() + "' and Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
                //Token_DS = objdata.ReturnMultipleValue(SSQL);
                //if (Token_DS.Rows.Count != 0)
                //{
                //    Token_No_Get = Token_DS.Rows[0]["ExistingCode"].ToString();
                //    Int32 Fixed_Work_Days = 0;
                //}

                //if (Wages == "STAFF")
                //{
                //    Fixed_Work_Days = Total_Days_Count - Month_WH_Count;
                //    Final_Count = Final_Count - Present_WH_Count;
                //    Final_Count = Final_Count - NFH_Days_Present_Count;
                //    Present_WH_Count = Present_WH_Count + NFH_Days_Present_Count;
                //    Present_WH_Count = Present_WH_Count + NFH_Days_Present_Count;
                //}

                //Fixed_Work_Days = Total_Days_Count - Month_WH_Count;
                //Final_Count = Final_Count - Present_WH_Count;
                //Final_Count = Final_Count - NFH_Days_Present_Count;
                //Present_WH_Count = Present_WH_Count + NFH_Days_Present_Count;
                //Present_WH_Count = Present_WH_Count + NFH_Days_Present_Count;



                //string NFH_Year_Str = "0";
                //string NFH_Month_Str = "";
                //string NFH_Rule_OLD = "";
                //int NFH_Display_Days = 0;
                //string NFH_Double_Wages_Rule = "";
                //DataTable mDataSet = new DataTable();
                //NFH_Year_Str = Convert.ToString(date1.Year);
                //NFH_Month_Str = Convert.ToString(date1.Month);
                ////SSQL = "Select * from NFH_Rule_Det where NFHYear='" + NFH_Year_Str + "' And Months='" + NFH_Month_Str + "'";
                //if (Final_Count != 0)
                //{

                //    Datacells.Rows[aintI][0] = AutoDataTable.Rows[intRow]["MachineID"].ToString();
                //    Datacells.Rows[aintI][2] = AutoDataTable.Rows[intRow]["EmpName"].ToString();
                //    //Datacells.Rows[aintI][13] = AutoDataTable.Rows[intRow]["MachineID_Encrypt"].ToString();
                //    Datacells.Rows[aintI][1] = AutoDataTable.Rows[intRow]["Token No"].ToString();
                //    Datacells.Rows[aintI]["NFH Worked Days"] = "0";
                //    Datacells.Rows[aintI]["NFH D.W Statutory"] = "0";

                //    //Datacells.Rows[aintI][1] = Machine_ID_Str_Excel;
                //    Datacells.Rows[aintI][3] = Final_Count;
                //    Datacells.Rows[aintI][4] = EHours_Count;
                //    Datacells.Rows[aintI][5] = NFH_Days_Count;
                //    Datacells.Rows[aintI][6] = "0";
                //    Datacells.Rows[aintI][7] = "0";
                //    Datacells.Rows[aintI][9] = Present_WH_Count;
                //    Datacells.Rows[aintI][10] = Month_WH_Count;
                //    Datacells.Rows[aintI][11] = Fixed_Work_Days;
                //    Datacells.Rows[aintI][9] = OT_Time;
                //    Datacells.Rows[aintI]["Year"] = NFH_Year_Str;

                //    if (Wages == "STAFF salary" || Wages == "Watch & Ward")
                //    {

                //        Datacells.Rows[aintI][8] = Total_Hr;
                //        // Datacells.Rows[aintI][12] = Present_WH_Count;
                //        Datacells.Rows[aintI][12] = NFH_Days_Present_Count;
                //        Datacells.Rows[aintI][13] = Total_Days_Count;
                //    }
                //    else
                //    {
                //        Datacells.Rows[aintI][8] = 0;
                //        Datacells.Rows[aintI][12] = 0;
                //        Datacells.Rows[aintI][13] = 0;
                //    }

                //    //  Datacells.Rows[aintI][14] = NFH_Days_Present_Count;
                //    // Datacells.Rows[aintI][15] = 
                //}
                //else
                //{
                //    Datacells.Rows[aintI][0] = AutoDataTable.Rows[intRow]["MachineID"].ToString();
                //    Datacells.Rows[aintI][2] = AutoDataTable.Rows[intRow]["EmpName"].ToString();
                //    // Datacells.Rows[aintI][13] = AutoDataTable.Rows[intRow]["MachineID_Encrypt"].ToString();
                //    Datacells.Rows[aintI][1] = AutoDataTable.Rows[intRow]["Token No"].ToString();
                //    Datacells.Rows[aintI]["NFH Worked Days"] = "0";
                //    Datacells.Rows[aintI]["NFH D.W Statutory"] = "0";


                //    //Datacells.Rows[aintI][1] = Machine_ID_Str_Excel;
                //    //Datacells.Rows[aintI][2] = Token_No_Get;
                //    Datacells.Rows[aintI][3] = Final_Count;
                //    Datacells.Rows[aintI][4] = EHours_Count;
                //    Datacells.Rows[aintI][5] = NFH_Days_Count;
                //    Datacells.Rows[aintI][6] = "0";
                //    Datacells.Rows[aintI][7] = "0";
                //    Datacells.Rows[aintI][9] = Present_WH_Count;
                //    Datacells.Rows[aintI][10] = Month_WH_Count;
                //    Datacells.Rows[aintI][11] = Fixed_Work_Days;

                //    if (Wages == "STAFF salary" || Wages == "Watch & Ward")
                //    {
                //        //Datacells.Rows[aintI][8] = 0;
                //        //Datacells.Rows[aintI][12] = Present_WH_Count;
                //        //Datacells.Rows[aintI][13] = Fixed_Work_Days;
                //        Datacells.Rows[aintI][8] = "0";
                //        //Datacells.Rows[aintI][12] = Present_WH_Count;
                //        Datacells.Rows[aintI][12] = NFH_Days_Present_Count;
                //        Datacells.Rows[aintI][13] = Total_Days_Count;
                //    }
                //    else
                //    {

                //        Datacells.Rows[aintI][8] = Present_WH_Count;
                //        Datacells.Rows[aintI][12] = 0;
                //        Datacells.Rows[aintI][13] = 0;
                //    }

                //    //Datacells.Rows[aintI][14] = NFH_Days_Present_Count;
                //    //  Datacells.Rows[aintI][15] = Total_Days_Count;


                }

                aintI = aintI + 1;
                Total_Days_Count = 0;
                Final_Count = 0;
                Appsent_Count = 0;
                Present_Count = 0;
                Fixed_Work_Days = 0;
                Month_WH_Count = 0;
                Present_WH_Count = 0;
                NFH_Days_Count = 0;
                NFH_Days_Present_Count = 0;
                EHours_Count = 0;

            }


        }

        catch (Exception e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
        }

        //UploadDataTableToExcel(Datacells);
    }

    //AutoMate Salary Calculation
    protected void btnSalaryCalculate_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        bool SaveFlag = false;
        string BasicPFDays = "26";
        

        //Basic Spilit
        string BasicAndDA = "0.00";
        
        string BasicHRA = "0.00";
        string ConvAllow = "0.00";
        string BasicAndDA_Val = "0.00";
        string ConvAllow_Val = "0.00";
        string BasicHRA_Val = "0.00";

        string EduAllow = "0.00";
        string MediAllow = "0.00";
        string BasicRAI = "0.00";
        string WashingAllow = "0.00";

        string Leaver_Credit_Status = "0";
        string Leave_Credit_Year = "0";
        string Leave_Credit_Days_Month = "0";
        string Leave_Credit_Add_Minus = "0";
        string Leave_Credit_Type = "";
        string HRA = "0";
        string FDA = "0";
        string VDA = "0";


        DataTable Advance_Mst_DT = new DataTable();
        DataTable Advance_Pay_DT = new DataTable();
        string Advance_Amt_Check = "0";
        string Advance_Paid_Count = "0";
        string HomeDays = "0";
        decimal TotalDays = 0;
        string Emp_Token_No_Search_Sal_Process = "";
        
        basicsalary = 0;
        try
        {
            if (ddlMonths.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (rbsalary.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Wges Type.');", true);
                ErrFlag = true;
            }
            //else if (Convert.ToInt32(txtdays.Text) > 31)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}


            decimal Month_Total_days = 0;
            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
            {
                Month_Total_days = 31;
            }
            else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
            {
                Month_Total_days = 30;
            }

            else if (ddlMonths.SelectedValue == "February")
            {
                int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                if ((yrs % 4) == 0)
                {
                    Month_Total_days = 29;
                }
                else
                {
                    Month_Total_days = 28;
                     TotalDays = Month_Total_days;
                }
            }

            TempDate = Convert.ToDateTime(txtFrom.Text).AddMonths(0).ToShortDateString();
            string MonthName = TempDate;
            DateTime mtn;
            mtn = Convert.ToDateTime(TempDate);
            d = mtn.Month;
            int mons = mtn.Month;
            int yrs1 = mtn.Month;
            #region MonthName
            string DB_Mont = "";
            switch (d)
            {
                case 1:
                    DB_Mont = "January";
                    break;

                case 2:
                    DB_Mont = "February";
                    break;
                case 3:
                    DB_Mont = "March";
                    break;
                case 4:
                    DB_Mont = "April";
                    break;
                case 5:
                    DB_Mont = "May";
                    break;
                case 6:
                    DB_Mont = "June";
                    break;
                case 7:
                    DB_Mont = "July";
                    break;
                case 8:
                    DB_Mont = "August";
                    break;
                case 9:
                    DB_Mont = "September";
                    break;
                case 10:
                    DB_Mont = "October";
                    break;
                case 11:
                    DB_Mont = "November";
                    break;
                case 12:
                    DB_Mont = "December";
                    break;
                default:
                    break;
            }
            #endregion

            if (ChkCivilIncentive.Checked == true)
            {
                if (txtIstWeekDate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the First Week Date Properly.');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (txtLastWeekDate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Last Week Date Properly.');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }

            //Check Token No Correct or Not
            if (ChkTokenNoSalaryprocess.Checked == true)
            {
                Emp_Token_No_Search_Sal_Process = txtTokenNoSearch.Text;
                DataTable Token_Sch_DT = new DataTable();
                query2 = "Select * from EmployeeDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                query2 = query2 + " And EmployeeType='" + txtEmployeeType.SelectedValue + "' And ExisistingCode='" + Emp_Token_No_Search_Sal_Process + "'";
                Token_Sch_DT = objdata.RptEmployeeMultipleDetails(query2);
                if (Token_Sch_DT.Rows.Count == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You have to Enter Token No Proberly...');", true);
                    ErrFlag = true;
                }
            }

            if (!ErrFlag)
            {
                DateTime dfrom = Convert.ToDateTime(txtFrom.Text);
                DateTime dtto = Convert.ToDateTime(txtTo.Text);
                DateTime MyDate1 = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                DateTime MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                Leave_Credit_Year = MyDate1.Year.ToString();

                if (dtto < dfrom)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                    txtFrom.Text = null;
                    txtTo.Text = null;
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    DataTable dt = new DataTable();
                    string Staff_Labour_Cat = "";
                    if (ddlcategory.SelectedValue == "1")
                    {
                        Staff_Labour_Cat = "S";
                    }
                    else
                    {
                        Staff_Labour_Cat = "L";
                    }
                    //Get Employee Details
                    query2 = "Select MstDpt.DepartmentNm,AD.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,'0' as All3,'0' as All4,";
                    query2 = query2 + " '0' as All5,'0' as Ded3,'0' as Ded4,'0' as Ded5,'0' as Advance,'0' as DedOthers1,'0' as DedOthers2,EmpDet.BiometricID,'0' as LOPDays";
                    query2 = query2 + " from EmployeeDetails EmpDet inner Join AttenanceDetails AD on EmpDet.EmpNo=AD.EmpNo";
                    query2 = query2 + " inner Join SalaryMaster SM on EmpDet.EmpNo=SM.EmpNo";
                    query2 = query2 + " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department where";
                    query2 = query2 + " AD.Months='" + ddlMonths.SelectedValue + "' AND";
                    query2 = query2 + " AD.FinancialYear='" + ddlfinance.SelectedValue + "' and EmpDet.StafforLabor='" + Staff_Labour_Cat + "'";
                    query2 = query2 + " And EmpDet.EmployeeType='" + txtEmployeeType.SelectedValue + "' And EmpDet.Ccode='" + SessionCcode + "'";
                    query2 = query2 + " And EmpDet.Lcode='" + SessionLcode + "' And AD.FromDate = convert(datetime,'" + MyDate1 + "', 105)";
                    query2 = query2 + " And AD.ToDate = Convert(Datetime,'" + MyDate2 + "', 105)";

                    //Check Token No
                    if (ChkTokenNoSalaryprocess.Checked == true)
                    {
                        query2 = query2 + " And EmpDet.ExisistingCode='" + txtTokenNoSearch.Text.ToString() + "'";
                    }
                    query2 = query2 + " Order by EmpDet.ExisistingCode Asc";
                    dt = objdata.RptEmployeeMultipleDetails(query2);


                    if (dt.Rows.Count != 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //Get Allowence And Deduction
                            DataTable Allo_Ded_DT = new DataTable();
                            query2 = "Select * from eAlert_Deduction_Det where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                            query2 = query2 + " And Month='" + ddlMonths.SelectedValue + "' And FinancialYear='" + ddlfinance.SelectedValue + "'";
                            query2 = query2 + " And MachineID='" + dt.Rows[i]["BiometricID"].ToString() + "'";
                            query2 = query2 + " And FromDate = convert(datetime,'" + MyDate1 + "', 105)";
                            query2 = query2 + " And ToDate = Convert(Datetime,'" + MyDate2 + "', 105)";
                            Allo_Ded_DT = objdata.RptEmployeeMultipleDetails(query2);
                            if (Allo_Ded_DT.Rows.Count != 0)
                            {
                                dt.Rows[i]["All3"] = Allo_Ded_DT.Rows[0]["allowances3"].ToString();
                                dt.Rows[i]["All4"] = Allo_Ded_DT.Rows[0]["allowances4"].ToString();
                                dt.Rows[i]["All5"] = Allo_Ded_DT.Rows[0]["allowances5"].ToString();
                                dt.Rows[i]["Ded3"] = Allo_Ded_DT.Rows[0]["Deduction3"].ToString();
                                dt.Rows[i]["Ded4"] = Allo_Ded_DT.Rows[0]["Deduction4"].ToString();
                                dt.Rows[i]["Ded5"] = Allo_Ded_DT.Rows[0]["Deduction5"].ToString();
                                dt.Rows[i]["Advance"] = Allo_Ded_DT.Rows[0]["Advance"].ToString();
                                dt.Rows[i]["DedOthers1"] = Allo_Ded_DT.Rows[0]["DedOthers1"].ToString();
                                dt.Rows[i]["DedOthers2"] = Allo_Ded_DT.Rows[0]["DedOthers2"].ToString();
                                dt.Rows[i]["LOPDays"] = Allo_Ded_DT.Rows[0]["LOPDays"].ToString();
                            }

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];



                        if (!ErrFlag)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                string Updateval = "1";
                                bool PassFlag = false;
                                bool ErrVerify = false;
                                string EmpNo = "";
                                string EmpType = "";
                                string SalarythroughType = "";
                                string Get_Wagestype = "";
                                basic = "0.00";
                                All1 = "0.00";
                                All2 = "0.00";
                                All3 = "0.00";
                                All5 = "0.00";
                                Al4 = "0.00";
                                Ded1 = "0.00";
                                ded2 = "0.00";
                                ded3 = "0.00";
                                ded4 = "0.00";
                                ded5 = "0.00";
                                SqlConnection cn = new SqlConnection(constr);
                                cn.Open();
                                string qry_dpt1 = "Select DepartmentCd from MstDepartment where DepartmentNm = '" + dt.Rows[i][0].ToString() + "'";
                                SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                                DepartmentCode = Convert.ToString(cmd_dpt1.ExecuteScalar());
                                string qry_emp = "Select EmpNo from SalaryDetails where EmpNo='" + dt.Rows[i][1].ToString() + "' and " +
                                                 "Month='" + ddlMonths.Text + "' and FinancialYear='" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                 " and Process_Mode='1' and convert(datetime,FromDate,105) = convert(datetime,'" + txtFrom.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";
                                string qry_EmpType = "Select ED.EmployeeType from EmployeeDetails ED inner Join MstEmployeeType ME on ME.EmpTypeCd = ED.EmployeeType where ED.EmpNo='" + dt.Rows[i][1].ToString() + "' AND ED.Ccode='" + SessionCcode + "' and ED.Lcode = '" + SessionLcode + "'";
                                SqlCommand cmd_EmpType = new SqlCommand(qry_EmpType, cn);
                                EmpType = (cmd_EmpType.ExecuteScalar()).ToString();


                                //Get Employee Wagestype and Salarythrough
                                string wages_query = "";
                                wages_query = "Select Wagestype from Officialprofile where EmpNo='" + dt.Rows[i][1].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                SqlCommand cmd_wages = new SqlCommand(wages_query, cn);
                                Get_Wagestype = (cmd_wages.ExecuteScalar()).ToString();
                                wages_query = "Select SalaryThrough from Officialprofile where EmpNo='" + dt.Rows[i][1].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                SqlCommand cmd_cash_bank = new SqlCommand(wages_query, cn);
                                SalarythroughType = (cmd_cash_bank.ExecuteScalar()).ToString();



                                // " and Process_Mode='1' and convert(datetime,TODate,105) >= convert(datetime,'" + txtFrom.Text + "',105)";

                                SqlCommand cmd_verify = new SqlCommand(qry_emp, cn);
                                Name_Upload = Convert.ToString(cmd_verify.ExecuteScalar());
                                //DateTime dfrom = Convert.ToDateTime(txtFrom.Text);
                                //DateTime dtto = Convert.ToDateTime(txtTo.Text);
                                //DateTime MyDate1 = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                //DateTime MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                                TempDate = Convert.ToDateTime(txtFrom.Text).AddMonths(0).ToShortDateString();
                                EmpNo = dt.Rows[i][1].ToString();
                                All3 = dt.Rows[i][4].ToString();
                                Al4 = dt.Rows[i][5].ToString();
                                All5 = dt.Rows[i][6].ToString();
                                ded3 = dt.Rows[i][7].ToString();
                                ded4 = dt.Rows[i][8].ToString();
                                ded5 = dt.Rows[i][9].ToString();
                                Advance = dt.Rows[i][10].ToString();
                                ExistNo = dt.Rows[i][2].ToString();

                                //Deduction Others
                                DedOthers1 = dt.Rows[i][11].ToString();
                                DedOthers2 = dt.Rows[i][12].ToString();


                                //Advance Amt Check And Update Start
                                DataTable Advance_Check_DT = new DataTable();
                                //query2 = "Select * from eAlert_Deduction_Det where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                //query2 = query2 + " And Month='" + ddlMonths.SelectedValue + "' And FinancialYear='" + ddlfinance.SelectedValue + "'";
                                //query2 = query2 + " And MachineID='" + dt.Rows[i]["BiometricID"].ToString() + "'";
                                //query2 = query2 + " And FromDate = convert(datetime,'" + MyDate1 + "', 105)";
                                //query2 = query2 + " And ToDate = Convert(Datetime,'" + MyDate2 + "', 105)";
                                //Advance_Check_DT = objdata.RptEmployeeMultipleDetails(query2);
                                Advance_Amt_Check = "0";
                                //if (Advance_Check_DT.Rows.Count != 0)
                                //{
                                //    if (Advance_Check_DT.Rows[0]["Advance_Check"].ToString() == "1")
                                //    {
                                //        Advance = dt.Rows[i][10].ToString();
                                //        Advance_Amt_Check = "1";
                                //    }
                                //    else { Advance_Amt_Check = "0"; }
                                //}

                                if (Convert.ToDecimal(Advance.ToString()) > 0)
                                {
                                    Advance_Amt_Check = "1";
                                }
                                else { Advance_Amt_Check = "0"; }

                                string Advance_Paid_Amt = "0";
                                string Balance_Amt = "0";
                                string Monthly_Paid_Amt = "0";
                                DataTable Update_Advance = new DataTable();

                                //Check Already Paid Or Not
                                query2 = "Select * from Advancerepayment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                query2 = query2 + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And TransDate=convert(datetime,'" + MyDate1 + "', 105)";
                                Advance_Pay_DT = objdata.RptEmployeeMultipleDetails(query2);
                                if (Advance_Pay_DT.Rows.Count != 0)
                                {
                                    string Advance_ID_Get = "0";
                                    Advance_ID_Get = Advance_Pay_DT.Rows[0]["AdvanceId"].ToString();
                                    //Delete OLD Amount Paid...
                                    query2 = "Delete from Advancerepayment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                    query2 = query2 + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And TransDate=convert(datetime,'" + MyDate1 + "', 105)";
                                    Update_Advance = objdata.RptEmployeeMultipleDetails(query2);
                                    //Update Advance Status
                                    query2 = "Update AdvancePayment set Completed='N' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                    query2 = query2 + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And ID='" + Advance_ID_Get + "'";
                                    Update_Advance = objdata.RptEmployeeMultipleDetails(query2);
                                }

                                if (Advance_Amt_Check.ToString() == "0")
                                {
                                    query2 = "Select * from AdvancePayment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                    query2 = query2 + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And Completed='N'";
                                    Advance_Mst_DT = objdata.RptEmployeeMultipleDetails(query2);
                                    if (Advance_Mst_DT.Rows.Count != 0)
                                    {


                                        //get Balance Amt
                                        if (Advance_Amt_Check.ToString() == "0")
                                        {

                                            query2 = "Select isnull(sum(Amount),0) as Paid_Amt from Advancerepayment where EmpNo='" + dt.Rows[i][1].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And AdvanceId='" + Advance_Mst_DT.Rows[0]["ID"] + "'";
                                            cmd_wages = new SqlCommand(query2, cn);
                                            Advance_Paid_Amt = (cmd_wages.ExecuteScalar()).ToString();
                                            Balance_Amt = (Convert.ToDecimal(Advance_Mst_DT.Rows[0]["Amount"].ToString()) - Convert.ToDecimal(Advance_Paid_Amt)).ToString();
                                            Monthly_Paid_Amt = Advance_Mst_DT.Rows[0]["MonthlyDeduction"].ToString();
                                            if (Convert.ToDecimal(Monthly_Paid_Amt.ToString()) >= Convert.ToDecimal(Balance_Amt.ToString()))
                                            {
                                                Advance = Balance_Amt;
                                            }
                                            else
                                            {
                                                Advance = Monthly_Paid_Amt;
                                            }
                                        }
                                        if (Convert.ToDecimal(Advance.ToString()) > 0)
                                        {
                                            //Paid Count Get
                                            query2 = "Select isnull(count(Amount),0) as Paid_Amt from Advancerepayment where EmpNo='" + EmpNo + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And AdvanceId='" + Advance_Mst_DT.Rows[0]["ID"] + "'";
                                            cmd_wages = new SqlCommand(query2, cn);
                                            Advance_Paid_Count = (cmd_wages.ExecuteScalar()).ToString();
                                            Advance_Paid_Count = (Convert.ToDecimal(Advance_Paid_Count.ToString()) + Convert.ToDecimal(1)).ToString();

                                            //Update Advance
                                            string Advance_Status = "N";
                                            Balance_Amt = (Convert.ToDecimal(Balance_Amt.ToString()) - Convert.ToDecimal(Advance.ToString())).ToString();
                                            Balance_Amt = (Math.Round(Convert.ToDecimal(Balance_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                                            if (Convert.ToDecimal(Balance_Amt.ToString()) <= 0)
                                            {
                                                Advance_Status = "Y";
                                            }
                                            else { Advance_Status = "N"; }
                                            query2 = "Update AdvancePayment set BalanceAmount='" + Balance_Amt + "',Completed='" + Advance_Status + "',IncreaseMonth='" + Advance_Paid_Count + "',ReductionMonth='0' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                            query2 = query2 + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And ID='" + Advance_Mst_DT.Rows[0]["ID"] + "'";
                                            Update_Advance = objdata.RptEmployeeMultipleDetails(query2);
                                            //Insert Paid Amount
                                            query2 = "Insert Into Advancerepayment(EmpNo,TransDate,Months,Amount,AdvanceId,Ccode,Lcode) values ('" + dt.Rows[i]["EmpNo"].ToString() + "',";
                                            query2 = query2 + "convert(datetime, '" + MyDate1 + "', 105),'" + ddlMonths.SelectedValue + "','" + Advance + "','" + Advance_Mst_DT.Rows[0]["ID"] + "',";
                                            query2 = query2 + "'" + SessionCcode + "','" + SessionLcode + "')";
                                            Update_Advance = objdata.RptEmployeeMultipleDetails(query2);
                                        }

                                    }
                                }


                                //Advance Amt Check And Update End


                                string Month = TempDate;
                                DateTime m;
                                m = Convert.ToDateTime(TempDate);
                                d = m.Month;
                                int mon = m.Month;
                                int yr = m.Year;
                                #region Month
                                string Mont = "";
                                switch (d)
                                {
                                    case 1:
                                        Mont = "January";
                                        break;

                                    case 2:
                                        Mont = "February";
                                        break;
                                    case 3:
                                        Mont = "March";
                                        break;
                                    case 4:
                                        Mont = "April";
                                        break;
                                    case 5:
                                        Mont = "May";
                                        break;
                                    case 6:
                                        Mont = "June";
                                        break;
                                    case 7:
                                        Mont = "July";
                                        break;
                                    case 8:
                                        Mont = "August";
                                        break;
                                    case 9:
                                        Mont = "September";
                                        break;
                                    case 10:
                                        Mont = "October";
                                        break;
                                    case 11:
                                        Mont = "November";
                                        break;
                                    case 12:
                                        Mont = "December";
                                        break;
                                    default:
                                        break;
                                }
                                #endregion

                                SMonth = Convert.ToString(mon);
                                Year = Convert.ToString(yr);
                                dd = DateTime.DaysInMonth(yr, mon);
                                MyMonth = Mont;
                                DataTable dtAttenance = new DataTable();

                                OTHoursNew_Str = "0";
                                OTHoursAmtNew_Str = "0";
                                lop = "0.00";
                                dtAttenance = objdata.Salary_attenanceDetails(EmpNo, Mont, ddlfinance.SelectedValue, DepartmentCode, SessionCcode, SessionLcode, MyDate1, MyDate2);
                                if (dtAttenance.Rows.Count <= 0)
                                {
                                    //i = i + 2;
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data Found " + i + "');", true);
                                    //ErrVerify = true;
                                    EmployeeDays = "0";
                                    Work = "0";
                                    NFh = "0";
                                    cl = "0";
                                    lopdays = "0";
                                    WeekOff = "0";
                                    Home = "0";
                                    halfNight = "0";
                                    FullNight = "0";
                                    ThreeSided = "0";
                                    OTHoursNew_Str = "0";
                                    OTHoursAmtNew_Str = "0";
                                    Fixed_Work_Dayss = "0";
                                    WH_Work_Days = "0";
                                }
                                else
                                {
                                    if (dtAttenance.Rows.Count > 0)
                                    {

                                        EmployeeDays = dtAttenance.Rows[0]["Days"].ToString();
                                        Work = dtAttenance.Rows[0]["TotalDays"].ToString();
                                        NFh = dtAttenance.Rows[0]["NFh"].ToString();
                                        cl = dtAttenance.Rows[0]["CL"].ToString();
                                        lopdays = dtAttenance.Rows[0]["AbsentDays"].ToString();
                                        WeekOff = dtAttenance.Rows[0]["weekoff"].ToString();
                                        Home = dtAttenance.Rows[0]["home"].ToString();
                                        halfNight = dtAttenance.Rows[0]["halfNight"].ToString();
                                        FullNight = dtAttenance.Rows[0]["FullNight"].ToString();
                                        ThreeSided = dtAttenance.Rows[0]["ThreeSided"].ToString();
                                        OTHoursNew_Str = dtAttenance.Rows[0]["OTHoursNew"].ToString();
                                        Fixed_Work_Dayss = dtAttenance.Rows[0]["Fixed_Work_Days"].ToString();
                                        WH_Work_Days = dtAttenance.Rows[0]["WH_Work_Days"].ToString();
                                        //txtlossofpay.Text = (Convert.ToDecimal(txtwork.Text) - (Convert.ToDecimal(txtempdays.Text) + (Convert.ToDecimal(txtNFh.Text) + (Convert.ToDecimal(txtcl.Text))))).ToString();
                                        DataTable dt_ot = new DataTable();
                                        dt_ot = objdata.Attenance_ot(EmpNo, Mont, ddlfinance.SelectedValue, DepartmentCode, SessionCcode, SessionLcode, MyDate1, MyDate2);
                                        if (dt_ot.Rows.Count > 0)
                                        {
                                            OTDays = dt_ot.Rows[0]["netAmount"].ToString();
                                        }
                                        else
                                        {
                                            OTDays = "0";
                                        }
                                    }
                                }

                                string Salarymonth = objdata.SalaryMonthFromLeave(EmpNo, ddlfinance.SelectedValue, SessionCcode, SessionLcode, Mont, MyDate1, MyDate2);

                                string MonthEEE = Salarymonth.Replace(" ", "");

                                int result = string.Compare(Salarymonth, Mont, true);
                                //int i = DateTime.ParseExact(Salarymonth, "MMMM", CultureInfo.CurrentCulture).Month;
                                string PayDay = objdata.SalaryPayDay();
                                // txtattendancewithpay.Text = PayDay;

                                if (Name_Upload == "")
                                {
                                    if (Mont == MonthEEE)
                                    {
                                        Updateval = "1";
                                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This " + Mont + " Month Salary All Ready Processed...!');", true);
                                        //ErrVerify = true;
                                    }
                                    else
                                    {
                                        Updateval = "1";
                                    }

                                }
                                else
                                {
                                    //Updateval = "0"; 
                                }
                                //if (Mont == MonthEEE)
                                //{
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This " + Mont + " Month Salary All Ready Processed...!');", true);
                                //    ErrVerify = true;
                                //}
                                //else
                                {
                                    Leaver_Credit_Status = "0";
                                    if (ddlcategory.SelectedValue == "1") //Staff Salary Calculation
                                    {
                                        AdvAmt = 0;
                                        BasicRAI = "0.00";
                                        ID = "";
                                        basicsalary = 0;
                                        Basic_For_SM = "0";
                                        DataTable dt1 = new DataTable();
                                        DataTable dt_Bonus = new DataTable(); 
                                        dt1 = objdata.NewSlary_Load(SessionCcode, SessionLcode, EmpNo);

                                        DataTable dteligible = new DataTable();
                                        DataTable dtpf = new DataTable();
                                        dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                                        dteligible = objdata.EligibleESI_PF(SessionCcode, SessionLcode, EmpNo);
                                        DataTable dtBasicPercent = new DataTable();
                                        dtBasicPercent = objdata.BsaicDetails_Load(SessionCcode, SessionLcode, ddlcategory.SelectedValue.ToString(), EmpType.ToString());
                                       //Bonus Percentage 
                                        dt_Bonus = objdata.ESI_PF_Load(SessionCcode, SessionLcode);
                                        BasicRAI = dt_Bonus.Rows[0]["EmployeerPFTwo"].ToString();
                                        if (dt1.Rows.Count > 0)
                                        {

                                            string basic1 = "0.00";
                                            string query = "";
                                            BasicAndDA = "0.00";
                                            BasicHRA = "0.00";
                                            ConvAllow = "0.00";
                                            EduAllow = "0.00";
                                            MediAllow = "0.00";
                                            WashingAllow = "0.00";
                                            stamp = "0";
                                            HalfNight_Amt = "0.00";
                                            FullNight_Amt = "0.00";
                                            DayIncentive = "0.00";
                                            SpinningAmt = "0.00";
                                            ThreeSided_Amt = "0.00";
                                            OTHoursAmtNew_Str = "0.00";
                                            Basic_For_SM = "0";
                                            Basic_For_SM = dt1.Rows[0]["Base"].ToString();



                                            if (EmpNo == "EMP062016102")
                                            {
                                                EmpNo = "EMP062016102";
                                            }
                                           

                                          
                                             All1 = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString())).ToString();
                                             All1 = (Math.Round(Convert.ToDecimal(All1), 2, MidpointRounding.AwayFromZero)).ToString();
                                             basic = (Math.Round(Convert.ToDecimal(basic), 2, MidpointRounding.AwayFromZero)).ToString();
                                            //txtSAll1.Text = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();

                                            //txtSAll2.Text = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                            All2 = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString())).ToString();
                                            All2 = (Math.Round(Convert.ToDecimal(All2), 2, MidpointRounding.AwayFromZero)).ToString();
                                            //txtSded1.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                            Ded1 = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString())).ToString();
                                            Ded1 = (Math.Round(Convert.ToDecimal(Ded1), 2, MidpointRounding.AwayFromZero)).ToString();
                                            //txtSded2.Text = (Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString()) * Convert.ToDecimal(txtempdays.Text)).ToString();
                                            ded2 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString())).ToString();
                                            ded2 = (Math.Round(Convert.ToDecimal(ded2), 2, MidpointRounding.AwayFromZero)).ToString();
                                            
                                            Basic_For_SM = "0";
                                            Basic_For_SM = dt1.Rows[0]["Base"].ToString();
                                            //basicsalary = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) - Convert.ToDecimal(lop));

                                            DataTable da_Base = new DataTable();
                                            //BASIC 
                                            SSQL = "select * from MstBasicDet where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and EmployeeType='" + EmpType + "'";
                                            da_Base = objdata.RptEmployeeMultipleDetails(SSQL);
                                            if (da_Base.Rows.Count != 0)
                                            {

                                                BasicAndDA = da_Base.Rows[0]["BasicDA"].ToString();
                                                BasicHRA = da_Base.Rows[0]["HRA"].ToString();
                                                ConvAllow = da_Base.Rows[0]["ConvAllow"].ToString();

                                                //Basic percentage
                                                BasicAndDA = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * Convert.ToDecimal(BasicAndDA) / 100).ToString();
                                                BasicAndDA = (Math.Round(Convert.ToDecimal(BasicAndDA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                //HR percentage
                                                BasicHRA = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * Convert.ToDecimal(BasicHRA) / 100).ToString();
                                                BasicHRA = (Math.Round(Convert.ToDecimal(BasicHRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                                //ConvAllow Percentage 
                                                ConvAllow = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) * Convert.ToDecimal(ConvAllow) / 100).ToString();
                                                ConvAllow = (Math.Round(Convert.ToDecimal(ConvAllow), 0, MidpointRounding.AwayFromZero)).ToString();
                                            }
                                            else
                                            {
                                                BasicAndDA = "0.0";
                                                BasicHRA = "0.0";
                                                ConvAllow = "0.0";
                                            }




                                            if (EmpType == "1")
                                            {

                                                if (Convert.ToDecimal(EmployeeDays) >= Convert.ToDecimal(26))
                                                {
                                                    //Basic Calculation 
                                                    BasicAndDA_Val = ((Convert.ToDecimal(BasicAndDA) / Convert.ToDecimal(26)) * Convert.ToDecimal(26)).ToString();
                                                    BasicAndDA_Val = (Math.Round(Convert.ToDecimal(BasicAndDA_Val), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    //HRA Calculation
                                                    BasicHRA_Val = ((Convert.ToDecimal(BasicHRA) / Convert.ToDecimal(26)) * Convert.ToDecimal(26)).ToString();
                                                    BasicHRA_Val = (Math.Round(Convert.ToDecimal(BasicHRA_Val), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    //ConvAllow Calculation
                                                    ConvAllow_Val = ((Convert.ToDecimal(ConvAllow) / Convert.ToDecimal(26)) * Convert.ToDecimal(26)).ToString();
                                                    ConvAllow_Val = (Math.Round(Convert.ToDecimal(ConvAllow_Val), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    basic = (Convert.ToDecimal(BasicAndDA_Val) + Convert.ToDecimal(BasicHRA_Val) + Convert.ToDecimal(ConvAllow_Val)).ToString();
                                                    //OT Calculation 
                                                    OTHoursAmtNew_Str = ((Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) / Convert.ToDecimal(26)) * Convert.ToDecimal(ThreeSided)).ToString();
                                                    OTHoursAmtNew_Str = (Math.Round(Convert.ToDecimal(OTHoursAmtNew_Str), 0, MidpointRounding.AwayFromZero)).ToString();

                                                }
                                                else
                                                {
                                                    //Basic Calculation 
                                                    BasicAndDA_Val = ((Convert.ToDecimal(BasicAndDA) / Convert.ToDecimal(26)) * Convert.ToDecimal(EmployeeDays)).ToString();
                                                    BasicAndDA_Val = (Math.Round(Convert.ToDecimal(BasicAndDA_Val), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    //HRA Calculation
                                                    BasicHRA_Val = ((Convert.ToDecimal(BasicHRA) / Convert.ToDecimal(26)) * Convert.ToDecimal(EmployeeDays)).ToString();
                                                    BasicHRA_Val = (Math.Round(Convert.ToDecimal(BasicHRA_Val), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    //ConvAllow Calculation
                                                    ConvAllow_Val = ((Convert.ToDecimal(ConvAllow) / Convert.ToDecimal(26)) * Convert.ToDecimal(EmployeeDays)).ToString();
                                                    ConvAllow_Val = (Math.Round(Convert.ToDecimal(ConvAllow_Val), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    basic = (Convert.ToDecimal(BasicAndDA_Val) + Convert.ToDecimal(BasicHRA_Val) + Convert.ToDecimal(ConvAllow_Val)).ToString();
                                                    //OT Calculation 
                                                    OTHoursAmtNew_Str = ((Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) / Convert.ToDecimal(26)) * Convert.ToDecimal(ThreeSided)).ToString();
                                                    OTHoursAmtNew_Str = (Math.Round(Convert.ToDecimal(OTHoursAmtNew_Str), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }
                                            }


                                            if (EmpType == "2")
                                            {

                                                if (Convert.ToDecimal(EmployeeDays) >= Convert.ToDecimal(26))
                                                {
                                                    //Basic Calculation 
                                                    BasicAndDA_Val = ((Convert.ToDecimal(BasicAndDA) / Convert.ToDecimal(26)) * Convert.ToDecimal(26)).ToString();
                                                    BasicAndDA_Val = (Math.Round(Convert.ToDecimal(BasicAndDA_Val), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    //HRA Calculation
                                                    BasicHRA_Val = ((Convert.ToDecimal(BasicHRA) / Convert.ToDecimal(26)) * Convert.ToDecimal(26)).ToString();
                                                    BasicHRA_Val = (Math.Round(Convert.ToDecimal(BasicHRA_Val), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    //ConvAllow Calculation
                                                    ConvAllow_Val = ((Convert.ToDecimal(ConvAllow) / Convert.ToDecimal(26)) * Convert.ToDecimal(26)).ToString();
                                                    ConvAllow_Val = (Math.Round(Convert.ToDecimal(ConvAllow_Val), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    basic = (Convert.ToDecimal(BasicAndDA_Val) + Convert.ToDecimal(BasicHRA_Val) + Convert.ToDecimal(ConvAllow_Val)).ToString();

                                                    //OT Calculation 
                                                    OTHoursAmtNew_Str = ((Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) / Convert.ToDecimal(26)) * Convert.ToDecimal(ThreeSided)).ToString();
                                                    OTHoursAmtNew_Str = (Math.Round(Convert.ToDecimal(OTHoursAmtNew_Str), 0, MidpointRounding.AwayFromZero)).ToString();


                                                }
                                                else
                                                {
                                                    //Basic Calculation 
                                                    BasicAndDA_Val = ((Convert.ToDecimal(BasicAndDA) / Convert.ToDecimal(26)) * Convert.ToDecimal(EmployeeDays)).ToString();
                                                    BasicAndDA_Val = (Math.Round(Convert.ToDecimal(BasicAndDA_Val), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    //HRA Calculation
                                                    BasicHRA_Val = ((Convert.ToDecimal(BasicHRA) / Convert.ToDecimal(26)) * Convert.ToDecimal(EmployeeDays)).ToString();
                                                    BasicHRA_Val = (Math.Round(Convert.ToDecimal(BasicHRA_Val), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    //ConvAllow Calculation
                                                    ConvAllow_Val = ((Convert.ToDecimal(ConvAllow) / Convert.ToDecimal(26)) * Convert.ToDecimal(EmployeeDays)).ToString();
                                                    ConvAllow_Val = (Math.Round(Convert.ToDecimal(ConvAllow_Val), 0, MidpointRounding.AwayFromZero)).ToString();
                                                    basic = (Convert.ToDecimal(BasicAndDA_Val) + Convert.ToDecimal(BasicHRA_Val) + Convert.ToDecimal(ConvAllow_Val)).ToString();
                                                    //OT Calculation 
                                                    OTHoursAmtNew_Str = ((Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()) / Convert.ToDecimal(26)) * Convert.ToDecimal(ThreeSided)).ToString();
                                                    OTHoursAmtNew_Str = (Math.Round(Convert.ToDecimal(OTHoursAmtNew_Str), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }
                                            }

                                            //OT Calculation 

                                            


                                                
                                            string Basic_Spilit_Tot = "0.00";
                                            TotalEarnings = (Convert.ToDecimal(basic) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(OTDays) + Convert.ToDecimal(All5) + Convert.ToDecimal(OTHoursAmtNew_Str)).ToString();

                                            TotalEarnings = (Math.Round(Convert.ToDecimal(TotalEarnings), 2, MidpointRounding.ToEven)).ToString();

                                            TotalDeductions = (Convert.ToDecimal(PFS) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Advance) + Convert.ToDecimal(stamp) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(DedOthers1) + Convert.ToDecimal(DedOthers2)).ToString();
                                            TotalDeductions = (Math.Round(Convert.ToDecimal(TotalDeductions), 2, MidpointRounding.ToEven)).ToString();
                                            NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();
                                            NetPay = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();
                                            if (Convert.ToDecimal(NetPay) < 0)
                                            {
                                                NetPay = "0";
                                            }
                                            double d1 = Convert.ToDouble(NetPay.ToString());
                                            int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                                            RoundOffNetPay = rounded1.ToString();

                                            Words = NumerictoNumber(Convert.ToInt32(RoundOffNetPay), isUK).ToString() + " " + "Only";
                                        }
                                    }
                                    else //Labour Salary Calculation
                                    {

                                        AdvAmt = 0;
                                        ID = "";
                                        basicsalary = 0;
                                        Basic_For_SM = "0";
                                        DataTable dt1 = new DataTable();
                                        dt1 = objdata.NewSlary_Load(SessionCcode, SessionLcode, EmpNo);
                                        DataTable dt_con = new DataTable();
                                        DataTable dt_slab = new DataTable();
                                        string basic1 = "0.00";
                                        Basic_For_SM = "0";
                                        OTDays = "0";
                                        Basic_For_SM = dt1.Rows[0]["Base"].ToString();
                                        basic1 = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
                                        if (EmpType == "2")
                                        {

                                            DayIncentive = "0";
                                            Basic_For_SM = "0";
                                            WashingAllow = "0.00";

                                            //basic = (Convert.ToDecimal(basic1) + Convert.ToDecimal(DayIncentive)).ToString();
                                            basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(EmployeeDays))).ToString();
                                            basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();
                                            HomeDays = EmployeeDays;
                                        }
                                        
                                        //OT Hour Calculation

                                        string Ot_Per = "";

                                        Ot_Per = (Convert.ToDecimal(basic1) / (Convert.ToDecimal(8))).ToString();
                                        Ot_Per = (Convert.ToDecimal(Ot_Per) * (Convert.ToDecimal(OTHoursNew_Str))).ToString();
                                        OTDays = (Math.Round(Convert.ToDecimal(Ot_Per), 0, MidpointRounding.AwayFromZero)).ToString();


                                        ////Incentive
                                        DataTable da_Incentive = new DataTable();
                                        SSQL = "select WorkerDays,WorkerAmt  from WorkerIncentive_mst	where WorkerDays='" + EmployeeDays + "' ";
                                        da_Incentive = objdata.RptEmployeeMultipleDetails(SSQL);
                                        if (da_Incentive.Rows.Count != 0)
                                        {
                                            DayIncentive = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(da_Incentive.Rows[0]["WorkerAmt"].ToString())).ToString();
                                        }
                                        else
                                        {
                                            DayIncentive = "0";
                                        }


                                        
                                        //saba End
                                        DataTable dteligible = new DataTable();
                                        DataTable dtpf = new DataTable();
                                        dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                                        dteligible = objdata.EligibleESI_PF(SessionCcode, SessionLcode, EmpNo);
             
                                        if (dt1.Rows.Count > 0)
                                        {


                                            //string basic1 = "0.00";                                               

                                            basic = (Math.Round(Convert.ToDecimal(basic), 2, MidpointRounding.AwayFromZero)).ToString();
                                            All1 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString())).ToString();
                                            All1 = (Math.Round(Convert.ToDecimal(All1), 2, MidpointRounding.AwayFromZero)).ToString();
                                            All2 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString())).ToString();
                                            All2 = (Math.Round(Convert.ToDecimal(All2), 2, MidpointRounding.AwayFromZero)).ToString();
                                            Ded1 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString())).ToString();
                                            Ded1 = (Math.Round(Convert.ToDecimal(Ded1), 2, MidpointRounding.AwayFromZero)).ToString();
                                            ded2 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString())).ToString();
                                            ded2 = (Math.Round(Convert.ToDecimal(ded2), 2, MidpointRounding.AwayFromZero)).ToString();
                                            lop = "0.00";
                                            PFS = "0";
                                            HalfNight_Amt = "0.00";
                                            FullNight_Amt = "0.00";
                                            DayIncentive = "0.00";
                                            SpinningAmt = "0.00";
                                            ESI = "0";
                                           
                                            stamp = "0";
                                                                                   
                                            TotalEarnings = (Convert.ToDecimal(basic) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(OTDays) + Convert.ToDecimal(All5) + Convert.ToDecimal(DayIncentive) + Convert.ToDecimal(OTHoursAmtNew_Str)).ToString();
                                            //TotalEarnings = (Convert.ToDecimal(basic) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(OTDays) + Convert.ToDecimal(All5) + Convert.ToDecimal(DayIncentive)).ToString();
                                            TotalEarnings = (Math.Round(Convert.ToDecimal(TotalEarnings), 2, MidpointRounding.ToEven)).ToString();
                                            TotalDeductions = (Convert.ToDecimal(PFS) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Advance) + Convert.ToDecimal(stamp) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(DedOthers1) + Convert.ToDecimal(DedOthers2)).ToString();
                                            TotalDeductions = (Math.Round(Convert.ToDecimal(TotalDeductions), 2, MidpointRounding.ToEven)).ToString();
                                            NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();
                                            NetPay = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();
                                            if (Convert.ToDecimal(NetPay) < 0)
                                            {
                                                NetPay = "0";
                                            }
                                            double d1 = Convert.ToDouble(NetPay.ToString());
                                            int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                                            RoundOffNetPay = rounded1.ToString();

                                            Words = NumerictoNumber(Convert.ToInt32(RoundOffNetPay), isUK).ToString() + " " + "Only";

                                        }
                                    }
                                }

                                if (!ErrVerify)
                                {
                                    string pfEligible = objdata.PF_Eligible_Salary(EmpNo, SessionCcode, SessionLcode);
                                    DateTime MyDate = new DateTime();
                                    objSal.TotalWorkingDays = Work;
                                    objSal.NFh = NFh;
                                    objSal.Lcode = SessionLcode;
                                    objSal.Ccode = SessionCcode;
                                    objSal.ApplyLeaveDays = lopdays;
                                    objSal.GrossEarnings = TotalEarnings;
                                    objSal.TotalDeduction = TotalDeductions;
                                    objSal.NetPay = NetPay;
                                    objSal.RoundOffNetPay = RoundOffNetPay;
                                    objSal.Words = Words;
                                    objSal.AvailableDays = EmployeeDays;
                                    objSal.SalaryDate = txtsalaryday.Text;
                                    objSal.work = EmployeeDays;
                                    objSal.weekoff = WeekOff;
                                    objSal.CL = cl;
                                    objSal.FixedBase = basic;
                                    objSal.FixedFDA = "0.00";
                                    objSal.FixedFRA = "0.00";
                                    objSal.HomeTown = "0.00";
                                    objSal.HalfNightAmt = HalfNight_Amt;
                                    objSal.FullNightAmt = FullNight_Amt;
                                    objSal.ThreesidedAmt = ThreeSided_Amt;
                                    objSal.SpinningAmt = SpinningAmt;
                                    objSal.DayIncentive = DayIncentive;
                                    MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);
                                    //if (EmpType == "1")
                                    //{
                                    objSal.BasicAndDA = BasicAndDA;
                                    objSal.HRA = BasicHRA_Val;
                                    objSal.FDA = ConvAllow_Val;
                                    objSal.VDA = BasicAndDA_Val;
                                    objSal.OT = "0";
                                    objSal.Allowances1 = All1;
                                    objSal.Allowances2 = All2;
                                    objSal.Allowances3 = All3;
                                    objSal.Allowances4 = Al4;
                                    objSal.Allowances5 = All5;
                                    objSal.Deduction1 = Ded1;
                                    objSal.Deduction2 = ded2;
                                    objSal.Deduction3 = ded3;
                                    objSal.Deduction4 = ded4;
                                    objSal.Deduction5 = ded5;
                                    objSal.ProvidentFund = PFS;
                                    objSal.ESI = ESI;
                                    objSal.Stamp = "0";
                                    objSal.Advance = Advance;
                                    objSal.LossofPay = lop;
                                    objSal.OT = OTDays;
                                    objSal.LOPDays = lopdays;

                                    //Basic Spilit Det
                                    objSal.BasicDA = BasicAndDA;
                                    objSal.Basic_HRA = BasicHRA;
                                    objSal.Conv_Allow = ConvAllow;
                                    objSal.Edu_Allow = EduAllow;
                                    objSal.Medi_Allow = MediAllow;
                                    objSal.Basic_RAI = BasicRAI;
                                    objSal.Washing_Allow = WashingAllow;

                                    if (pfEligible == "1")
                                    {
                                        objSal.PfSalary = PFS;
                                        objSal.Emp_PF = PFS;
                                    }
                                    else
                                    {
                                        objSal.PfSalary = "0.00";
                                        objSal.Emp_PF = "0.00";
                                    }

                                    //Sub-Staff OT Hours Set
                                    objSal.OTHoursAmt = OTHoursAmtNew_Str;
                                    objSal.OTHoursNew = OTHoursNew_Str;
                                    objSal.Leave_Credit_Check = Leaver_Credit_Status;

                                    objSal.Fixed_Work_Days = Fixed_Work_Dayss;
                                    objSal.WH_Work_Days = WH_Work_Days;

                                    //Deduction Others
                                    objSal.Ded_Others1 = DedOthers1;
                                    objSal.Ded_Others2 = DedOthers2;

                                    //Employeer PF And ESI
                                    objSal.EmployeerPFone = EmployeerPFOne;
                                    objSal.EmployeerPFTwo = EmployeerPFTwo;
                                    objSal.EmployeerESI = EmployeerESI;
                                    objSal.Gift = Gift;
                                    objSal.Fine = Fine;
                                    //objSal.Leave_Credit_Days = Leave_Credit_Days_Month;
                                    //objSal.Leave_Credit_Add = Leave_Credit_Add_Minus;
                                    //objSal.Leave_Credit_Type = Leave_Credit_Type;

                                    //PassFlag = true;

                                    //Check Salary Conformation
                                    DataTable Sal_Confm_DT = new DataTable();
                                    string Sal_Conform_qry = "Select * from SalaryDetails where EmpNo='" + EmpNo + "' and " +
                                                 "Month='" + ddlMonths.Text + "' and FinancialYear='" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                 " and convert(datetime,FromDate,105) = convert(datetime,'" + txtFrom.Text + "',105)" +
                                                 " and convert(datetime, ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105) And Salary_Conform='1'";
                                    Sal_Confm_DT = objdata.RptEmployeeMultipleDetails(Sal_Conform_qry);
                                    if (Sal_Confm_DT.Rows.Count == 0)
                                    {
                                        PassFlag = true;
                                    }
                                    else
                                    {
                                        PassFlag = false;
                                    }

                                }
                                if (PassFlag == true)
                                {
                                    if (Updateval == "0")
                                    {
                                        string Del2 = "Delete from SalaryDetails where EmpNo='" + EmpNo + "' and " +
                                                 "Month='" + ddlMonths.Text + "' and FinancialYear='" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                 " and convert(datetime,FromDate,105) = convert(datetime,'" + txtFrom.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";
                                        //cn.Open();
                                        SqlCommand cmd_verify3 = new SqlCommand(Del2, cn);
                                        cmd_verify3.ExecuteNonQuery();
                                    }
                                    else if (Updateval == "1")
                                    {
                                        string Del = "Delete from SalaryDetails where EmpNo='" + EmpNo + "' and " +
                                                 "Month='" + ddlMonths.Text + "' and FinancialYear='" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                 " and convert(datetime,FromDate,105) = convert(datetime,'" + txtFrom.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";
                                        //cn.Open();
                                        SqlCommand cmd_verify1 = new SqlCommand(Del, cn);
                                        cmd_verify1.ExecuteNonQuery();
                                        MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);

                                        TempDate = Convert.ToDateTime(txtsalaryday.Text).AddMonths(0).ToShortDateString();
                                        //TransDate = DateTime.ParseExact(TempDate, "dd/MM/yyyy",null);

                                        TransDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);



                            
                                       

                                        //Get Basic Salary for Salary Master
                                        objSal.BasicforSM = Basic_For_SM;
                                        objdata.SalaryDetails(objSal, EmpNo, ExistNo, MyDate, MyMonth, Year, ddlfinance.SelectedValue, TransDate, MyDate1, MyDate2, SalarythroughType, Get_Wagestype);


                                        string Balanceday = objdata.Contract_GetEmpDet(EmpNo, SessionCcode, SessionLcode);
                                        if (Balanceday.Trim() != "")
                                        {
                                            if (Convert.ToDecimal(Balanceday) > 0)
                                            {
                                                DataTable dt_det = new DataTable();
                                                dt_det = objdata.Contract_getPlanDetails(EmpNo, SessionCcode, SessionLcode);
                                                if (dt_det.Rows.Count > 0)
                                                {
                                                    if (dt_det.Rows[0]["ContractType"].ToString() == "1")
                                                    {
                                                        val = (Convert.ToDecimal(EmployeeDays));
                                                        Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                        //objdata.Contract_Reducing(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode);
                                                    }
                                                    else if (dt_det.Rows[0]["ContractType"].ToString() == "2")
                                                    {
                                                        val = (Convert.ToDecimal(EmployeeDays));
                                                        if (val >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                                                        {
                                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                                                            Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                            //objdata.Contract_Reducing_Month(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode, Mon);
                                                        }
                                                        else
                                                        {
                                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                                                            //Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                            //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val, SessionCcode, SessionLcode, Mon);
                                                            // objdata.Not_Contract_Reducing_Month(EmpNo, val.ToString(), SessionCcode, SessionLcode);
                                                        }
                                                    }
                                                    else if (dt_det.Rows[0]["ContractType"].ToString() == "3")
                                                    {
                                                        val = (Convert.ToDecimal(EmployeeDays));
                                                        Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                        //objdata.Contract_Reducing(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode);
                                                        SaveFlag = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Finished Success Fully');", true);
                                cn.Close();

                            }

                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }

    protected void  btnUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        bool SaveFlag = false;
        string BasicPFDays = "26";

        //Basic Spilit
        string BasicAndDA = "0.00";
        string BasicHRA = "0.00";
        string ConvAllow = "0.00";
        string EduAllow = "0.00";
        string MediAllow = "0.00";
        string BasicRAI = "0.00";
        string WashingAllow = "0.00";
        string HomeDays = "0";
        string HRA = "0";

        string BasicAndDA_Val = "0.00";
        string ConvAllow_Val = "0.00";
        string BasicHRA_Val = "0.00";
        string basic1 = "0.00";
        decimal TotalDays = 0;


        try
        {
            if (ddlMonths.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (rbsalary.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Wges Type.');", true);
                ErrFlag = true;
            }
            //else if (Convert.ToInt32(txtdays.Text) > 31)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}


            decimal Month_Total_days = 0;
            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
            {
                Month_Total_days = 31;
            }
            else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
            {
                Month_Total_days = 30;
            }

            else if (ddlMonths.SelectedValue == "February")
            {
                int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                if ((yrs % 4) == 0)
                {
                    Month_Total_days = 29;
                }
                else
                {
                    Month_Total_days = 28;
                    TotalDays = Month_Total_days;
                }
            }

            TempDate = Convert.ToDateTime(txtFrom.Text).AddMonths(0).ToShortDateString();
            string MonthName = TempDate;
            DateTime mtn;
            mtn = Convert.ToDateTime(TempDate);
            d = mtn.Month;
            int mons = mtn.Month;
            int yrs1 = mtn.Month;
            #region MonthName
            string DB_Mont = "";
            switch (d)
            {
                case 1:
                    DB_Mont = "January";
                    break;

                case 2:
                    DB_Mont = "February";
                    break;
                case 3:
                    DB_Mont = "March";
                    break;
                case 4:
                    DB_Mont = "April";
                    break;
                case 5:
                    DB_Mont = "May";
                    break;
                case 6:
                    DB_Mont = "June";
                    break;
                case 7:
                    DB_Mont = "July";
                    break;
                case 8:
                    DB_Mont = "August";
                    break;
                case 9:
                    DB_Mont = "September";
                    break;
                case 10:
                    DB_Mont = "October";
                    break;
                case 11:
                    DB_Mont = "November";
                    break;
                case 12:
                    DB_Mont = "December";
                    break;
                default:
                    break;
            }
            #endregion

            if (ChkCivilIncentive.Checked == true)
            {
                if (txtIstWeekDate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the First Week Date Properly.');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                if (txtLastWeekDate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Last Week Date Properly.');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
            }

            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

            }
            if (!ErrFlag)
            {
                DateTime dfrom = Convert.ToDateTime(txtFrom.Text);
                DateTime dtto = Convert.ToDateTime(txtTo.Text);
                DateTime MyDate1 = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                DateTime MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                
                if (dtto < dfrom)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                    txtFrom.Text = null;
                    txtTo.Text = null;
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();
                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            SqlConnection cn = new SqlConnection(constr);
                            string Department = dt.Rows[j][0].ToString();
                            string MachineID = "";//dt.Rows[j][1].ToString();
                            string EmpNo = dt.Rows[j][1].ToString();
                            string ExistingCode = dt.Rows[j][2].ToString();
                            string FirstName = dt.Rows[j][3].ToString();
                            string SAll3 = dt.Rows[j][4].ToString();
                            string SAll4 = dt.Rows[j][5].ToString();
                            string SAll5 = dt.Rows[j][6].ToString();
                            string Sded3 = dt.Rows[j][7].ToString();
                            string Sded4 = dt.Rows[j][8].ToString();
                            string Sded5 = dt.Rows[j][9].ToString();
                            string SAdvance = dt.Rows[j][10].ToString();
                            string SFine = dt.Rows[j][13].ToString();

                            string Full_OT = "0";
                            string Full_OT_Inc = "0";
                            string Full_OT_Amt = "0";

                            string Emp_query = "";
                            DataTable Emp_Check_DT = new DataTable();
                            //Check Employee Table
                            Emp_query = "Select * from EmployeeDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and EmpNo='" + EmpNo + "'";
                            Emp_Check_DT = objdata.RptEmployeeMultipleDetails(Emp_query);
                            if (Emp_Check_DT.Rows.Count == 0)
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check the Employee Details in Employee Details Page. The Row Number is " + j + "');", true);
                                ErrFlag = true;
                            }
                            //Check Official Profile Table
                            Emp_query = "Select * from OfficialProfile where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and EmpNo='" + EmpNo + "'";
                            Emp_Check_DT = objdata.RptEmployeeMultipleDetails(Emp_query);
                            if (Emp_Check_DT.Rows.Count == 0)
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check the Employee Details in Profile Page. The Row Number is " + j + "');", true);
                                ErrFlag = true;
                            }
                            //Check Salary Master Table
                            Emp_query = "Select * from SalaryMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and EmpNo='" + EmpNo + "'";
                            Emp_Check_DT = objdata.RptEmployeeMultipleDetails(Emp_query);
                            if (Emp_Check_DT.Rows.Count == 0)
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check the Employee Wages in Salary Master Page. The Row Number is " + j + "');", true);
                                ErrFlag = true;
                            }
                            //Check AttenanceDetails Table 
                            Emp_query = "Select * from AttenanceDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and EmpNo='" + EmpNo + "' and Months='" + DB_Mont + "' and FinancialYear='" + ddlfinance.SelectedValue + "'";
                            Emp_Check_DT = objdata.RptEmployeeMultipleDetails(Emp_query);
                            if (Emp_Check_DT.Rows.Count == 0)
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Attenance Not Upload This Employee Check Attenance Upload Excel File. The Row Number is " + j + "');", true);
                                ErrFlag = true;
                            }
                            //Check PF Master
                            Emp_query = "Select * from MstESIPF where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                            Emp_Check_DT = objdata.RptEmployeeMultipleDetails(Emp_query);
                            if (Emp_Check_DT.Rows.Count == 0)
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You have enter PF And ESI Percentage in PF Master Page. The Row Number is " + j + "');", true);
                                ErrFlag = true;
                            }
                            //Check Basic Details

                            if (Department == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Department. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Department. The Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            //else if (MachineID == "")
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Machine ID. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            else if (EmpNo == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Number. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (ExistingCode == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing Code. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Existing Code. The Roe Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (FirstName == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Name. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (SAll3 == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 3s. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (SAll4 == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 4. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (SAll5 == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Gift. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Sded3 == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 3. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Sded4 == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 4. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Sded5 == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 5. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (SAdvance == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Advance. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (SFine == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Fine. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Open();
                            string qry_dpt = "Select DepartmentNm from MstDepartment where DepartmentNm = '" + Department + "'";
                            SqlCommand cmd_dpt = new SqlCommand(qry_dpt, cn);
                            SqlDataReader sdr_dpt = cmd_dpt.ExecuteReader();
                            if (sdr_dpt.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            cn.Open();
                            string qry_dpt1 = "Select ED.Department from EmployeeDetails ED inner Join MstDepartment MSt on Mst.DepartmentCd=ED.Department where DepartmentNm = '" + Department + "' and ED.EmpNo='" + EmpNo + "'";
                            SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                            SqlDataReader sdr_dpt1 = cmd_dpt1.ExecuteReader();
                            if (sdr_dpt1.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            cn.Open();
                            string Qry_SalaryType = "Select EmpNo from Officialprofile where WagesType ='" + rbsalary.SelectedValue + "'";
                            SqlCommand cmd_wages = new SqlCommand(Qry_SalaryType, cn);
                            SqlDataReader sdr_wages = cmd_wages.ExecuteReader();
                            if (sdr_wages.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This " + EmpNo + " is Wages Type is incorrect.');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            cn.Open();
                            string qry_empNo = "Select EmpNo from EmployeeDetails where EmpNo= '" + EmpNo + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                            SqlCommand cmd_Emp = new SqlCommand(qry_empNo, cn);
                            SqlDataReader sdr_Emp = cmd_Emp.ExecuteReader();
                            if (sdr_Emp.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Details Properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Details Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();

                        }
                        if (!ErrFlag)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                string Updateval = "1";
                                bool PassFlag = false;
                                bool ErrVerify = false;
                                string EmpNo = "";
                                string EmpType = "";
                                string SalarythroughType = "";
                                string Get_Wagestype = "";
                                string Incentive_Days_Default = "4";
                                string Incentive_Days = "0";
                                basic = "0.00";
                                All1 = "0.00";
                                All2 = "0.00";
                                All3 = "0.00";
                                All5 = "0.00";
                                Al4 = "0.00";
                                Ded1 = "0.00";
                                ded2 = "0.00";
                                ded3 = "0.00";
                                ded4 = "0.00";
                                ded5 = "0.00";
                                Gift = "0.00";
                                Fine = "0.00";
                                SqlConnection cn = new SqlConnection(constr);
                                cn.Open();
                                string qry_dpt1 = "Select DepartmentCd from MstDepartment where DepartmentNm = '" + dt.Rows[i][0].ToString() + "'";
                                SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                                DepartmentCode = Convert.ToString(cmd_dpt1.ExecuteScalar());
                                string qry_emp = "Select EmpNo from SalaryDetails where EmpNo='" + dt.Rows[i][1].ToString() + "' and " +
                                                 "Month='" + ddlMonths.Text + "' and FinancialYear='" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                 " and Process_Mode='1' and convert(datetime,FromDate,105) = convert(datetime,'" + txtFrom.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";
                                string qry_EmpType = "Select ED.EmployeeType from EmployeeDetails ED inner Join MstEmployeeType ME on ME.EmpTypeCd = ED.EmployeeType where ED.EmpNo='" + dt.Rows[i][1].ToString() + "' AND ED.Ccode='" + SessionCcode + "' and ED.Lcode = '" + SessionLcode + "'";
                                SqlCommand cmd_EmpType = new SqlCommand(qry_EmpType, cn);
                                EmpType = (cmd_EmpType.ExecuteScalar()).ToString();


                                //Get Employee Wagestype and Salarythrough
                                string wages_query = "";
                                wages_query = "Select Wagestype from Officialprofile where EmpNo='" + dt.Rows[i][1].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                SqlCommand cmd_wages = new SqlCommand(wages_query, cn);
                                Get_Wagestype = (cmd_wages.ExecuteScalar()).ToString();
                                wages_query = "Select SalaryThrough from Officialprofile where EmpNo='" + dt.Rows[i][1].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                SqlCommand cmd_cash_bank = new SqlCommand(wages_query, cn);
                                SalarythroughType = (cmd_cash_bank.ExecuteScalar()).ToString();



                                // " and Process_Mode='1' and convert(datetime,TODate,105) >= convert(datetime,'" + txtFrom.Text + "',105)";

                                SqlCommand cmd_verify = new SqlCommand(qry_emp, cn);
                                Name_Upload = Convert.ToString(cmd_verify.ExecuteScalar());
                                //DateTime dfrom = Convert.ToDateTime(txtFrom.Text);
                                //DateTime dtto = Convert.ToDateTime(txtTo.Text);
                                //DateTime MyDate1 = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                //DateTime MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                                TempDate = Convert.ToDateTime(txtFrom.Text).AddMonths(0).ToShortDateString();
                                EmpNo = dt.Rows[i][1].ToString();
                                All3 = dt.Rows[i][4].ToString();
                                Al4 = dt.Rows[i][5].ToString();
                                All5 = dt.Rows[i][6].ToString();
                                ded3 = dt.Rows[i][7].ToString();
                                ded4 = dt.Rows[i][8].ToString();
                                ded5 = dt.Rows[i][9].ToString();
                                Advance = dt.Rows[i][10].ToString();
                                Fine=dt.Rows[i][13].ToString();
                                ExistNo = dt.Rows[i][2].ToString();

                                //Deduction Others
                                DedOthers1 = dt.Rows[i][11].ToString();
                                //DedOthers2 = dt.Rows[i][12].ToString();
                                Mess = dt.Rows[i][12].ToString();
                                Gift = dt.Rows[i][14].ToString();

                                string Month = TempDate;
                                DateTime m;
                                m = Convert.ToDateTime(TempDate);
                                d = m.Month;
                                int mon = m.Month;
                                int yr = m.Year;
                                #region Month
                                string Mont = "";
                                switch (d)
                                {
                                    case 1:
                                        Mont = "January";
                                        break;

                                    case 2:
                                        Mont = "February";
                                        break;
                                    case 3:
                                        Mont = "March";
                                        break;
                                    case 4:
                                        Mont = "April";
                                        break;
                                    case 5:
                                        Mont = "May";
                                        break;
                                    case 6:
                                        Mont = "June";
                                        break;
                                    case 7:
                                        Mont = "July";
                                        break;
                                    case 8:
                                        Mont = "August";
                                        break;
                                    case 9:
                                        Mont = "September";
                                        break;
                                    case 10:
                                        Mont = "October";
                                        break;
                                    case 11:
                                        Mont = "November";
                                        break;
                                    case 12:
                                        Mont = "December";
                                        break;
                                    default:
                                        break;
                                }
                                #endregion

                                SMonth = Convert.ToString(mon);
                                Year = Convert.ToString(yr);
                                dd = DateTime.DaysInMonth(yr, mon);
                                MyMonth = Mont;
                                DataTable dtAttenance = new DataTable();

                                OTHoursNew_Str = "0";
                                OTHoursAmtNew_Str = "0";
                                lop = "0.00";



                                dtAttenance = objdata.Salary_attenanceDetails(EmpNo, Mont, ddlfinance.SelectedValue, DepartmentCode, SessionCcode, SessionLcode, MyDate1, MyDate2);
                                if (dtAttenance.Rows.Count <= 0)
                                {
                                    //i = i + 2;
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data Found " + i + "');", true);
                                    //ErrVerify = true;
                                    EmployeeDays = "0";
                                    Work = "0";
                                    NFh = "0";
                                    cl = "0";
                                    lopdays = "0";
                                    WeekOff = "0";
                                    Home = "0";
                                    halfNight = "0";
                                    FullNight = "0";
                                    ThreeSided = "0";
                                    OTHoursNew_Str = "0";
                                    OTHoursAmtNew_Str = "0";
                                    Fixed_Work_Dayss = "0";
                                    WH_Work_Days = "0";
                                }
                                else
                                {
                                    if (dtAttenance.Rows.Count > 0)
                                    {
                                        EmployeeDays = dtAttenance.Rows[0]["Days"].ToString();
                                        Work = dtAttenance.Rows[0]["TotalDays"].ToString();
                                        NFh = dtAttenance.Rows[0]["NFh"].ToString();
                                        cl = dtAttenance.Rows[0]["CL"].ToString();
                                        lopdays = dtAttenance.Rows[0]["AbsentDays"].ToString();
                                        WeekOff = dtAttenance.Rows[0]["weekoff"].ToString();
                                        Home = dtAttenance.Rows[0]["home"].ToString();
                                        halfNight = dtAttenance.Rows[0]["halfNight"].ToString();
                                        FullNight = dtAttenance.Rows[0]["FullNight"].ToString();
                                        ThreeSided = dtAttenance.Rows[0]["ThreeSided"].ToString();
                                        OTHoursNew_Str = dtAttenance.Rows[0]["OTHoursNew"].ToString();
                                        Fixed_Work_Dayss = dtAttenance.Rows[0]["Fixed_Work_Days"].ToString();
                                        WH_Work_Days = dtAttenance.Rows[0]["WH_Work_Days"].ToString();
                                        //txtlossofpay.Text = (Convert.ToDecimal(txtwork.Text) - (Convert.ToDecimal(txtempdays.Text) + (Convert.ToDecimal(txtNFh.Text) + (Convert.ToDecimal(txtcl.Text))))).ToString();
                                        DataTable dt_ot = new DataTable();
                                        dt_ot = objdata.Attenance_ot(EmpNo, Mont, ddlfinance.SelectedValue, DepartmentCode, SessionCcode, SessionLcode, MyDate1, MyDate2);
                                        if (dt_ot.Rows.Count > 0)
                                        {
                                            OTDays = dt_ot.Rows[0]["netAmount"].ToString();
                                        }
                                        else
                                        {
                                            OTDays = "0";
                                        }
                                    }
                                }

                                string Salarymonth = objdata.SalaryMonthFromLeave(EmpNo, ddlfinance.SelectedValue, SessionCcode, SessionLcode, Mont, MyDate1, MyDate2);

                                string MonthEEE = Salarymonth.Replace(" ", "");

                                int result = string.Compare(Salarymonth, Mont, true);
                                //int i = DateTime.ParseExact(Salarymonth, "MMMM", CultureInfo.CurrentCulture).Month;
                                string PayDay = objdata.SalaryPayDay();
                                // txtattendancewithpay.Text = PayDay;

                                if (Name_Upload == "")
                                {
                                    if (Mont == MonthEEE)
                                    {
                                        Updateval = "1";
                                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This " + Mont + " Month Salary All Ready Processed...!');", true);
                                        //ErrVerify = true;
                                    }
                                    else
                                    {
                                        Updateval = "1";
                                    }

                                }
                                else
                                {
                                    //Updateval = "0"; 
                                }
                                //if (Mont == MonthEEE)
                                //{
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This " + Mont + " Month Salary All Ready Processed...!');", true);
                                //    ErrVerify = true;
                                //}
                                //else
                                {

                                    basicsalary = 0;
                                    Basic_For_SM = "0";

                                    if (ddlcategory.SelectedValue == "1") //Staff Salary Calculation
                                    {
                                        AdvAmt = 0;
                                        BasicRAI = "0.00";
                                        ID = "";
                                        basicsalary = 0;
                                        Basic_For_SM = "0";
                                        DataTable dt1 = new DataTable();
                                        DataTable dt_Bonus = new DataTable();
                                        dt1 = objdata.NewSlary_Load(SessionCcode, SessionLcode, EmpNo);

                                        DataTable Da_PF = new DataTable();
                                        DataTable da_Incentive = new DataTable();
                                        string PF_Labour_Salary = "";
                                        string pf_per = "";
                                        string ESI_Per = "";
                                        string Pf_Salary = "";

                                        DataTable dteligible = new DataTable();
                                        DataTable dtpf = new DataTable();
                                        DataTable da_Salary = new DataTable();
                                        dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                                        dteligible = objdata.EligibleESI_PF(SessionCcode, SessionLcode, EmpNo);

                                        Pf_Salary = (Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString())).ToString();
                                        pf_per = (Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())).ToString();
                                        ESI_Per = (Convert.ToDecimal(dtpf.Rows[0]["ESI_Per"].ToString())).ToString();

                                        if (dt1.Rows.Count > 0)
                                        {

                                            basic1 = "0.00";
                                            string query = "";
                                            BasicAndDA = "0.00";
                                            BasicHRA = "0.00";
                                            ConvAllow = "0.00";
                                            EduAllow = "0.00";
                                            MediAllow = "0.00";
                                            WashingAllow = "0.00";
                                            stamp = "0";
                                            HalfNight_Amt = "0.00";
                                            FullNight_Amt = "0.00";
                                            DayIncentive = "0.00";
                                            SpinningAmt = "0.00";
                                            ThreeSided_Amt = "0.00";
                                            OTHoursAmtNew_Str = "0.00";
                                            Basic_For_SM = "0";
                                            Basic_For_SM = dt1.Rows[0]["Base"].ToString();
                                            if (EmpNo == "EMP062016102")
                                            {
                                                EmpNo = "EMP062016102";
                                            }
                                            basic = (Math.Round(Convert.ToDecimal(Basic_For_SM), 0, MidpointRounding.AwayFromZero)).ToString();

                                            BasicAndDA = (Convert.ToDecimal(basic) * Convert.ToDecimal(EmployeeDays)).ToString();
                                            BasicAndDA = (Math.Round(Convert.ToDecimal(BasicAndDA), 0, MidpointRounding.AwayFromZero)).ToString();

                                            //Hrs Salary
                                            BasicHRA = ((Convert.ToDecimal(basic) / Convert.ToDecimal(8)) * Convert.ToDecimal(cl)).ToString();
                                            BasicHRA = (Math.Round(Convert.ToDecimal(BasicHRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                            //OT Hours
                                            ThreeSided = OTHoursNew_Str;
                                            OTHoursNew_Str = "";
                                            OTHoursAmtNew_Str = ((Convert.ToDecimal(Basic_For_SM) / Convert.ToDecimal(8)) * Convert.ToDecimal(ThreeSided)).ToString();
                                            OTHoursAmtNew_Str = (Math.Round(Convert.ToDecimal(OTHoursAmtNew_Str), 0, MidpointRounding.AwayFromZero)).ToString();

                                            //Incentive
                                            SSQL = "select WorkerDays,WorkerAmt  from WorkerIncentive_mst	where  WorkerDays<='" + EmployeeDays + "' ";
                                            da_Incentive = objdata.RptEmployeeMultipleDetails(SSQL);
                                            if (da_Incentive.Rows.Count != 0)
                                            {
                                                DayIncentive = "300";
                                            }
                                            else
                                            {
                                                DayIncentive = "0";
                                            }

                                            All1 = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString())).ToString();
                                            All1 = (Math.Round(Convert.ToDecimal(All1), 2, MidpointRounding.AwayFromZero)).ToString();
                                            All2 = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString())).ToString();
                                            All2 = (Math.Round(Convert.ToDecimal(All2), 2, MidpointRounding.AwayFromZero)).ToString();
                                            Ded1 = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString())).ToString();
                                            Ded1 = (Math.Round(Convert.ToDecimal(Ded1), 2, MidpointRounding.AwayFromZero)).ToString();
                                            ded2 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString())).ToString();
                                            ded2 = (Math.Round(Convert.ToDecimal(ded2), 2, MidpointRounding.AwayFromZero)).ToString();
                                            ded3 = (Math.Round(Convert.ToDecimal(ded3), 2, MidpointRounding.AwayFromZero)).ToString();
                                            ded4 = (Math.Round(Convert.ToDecimal(ded4), 2, MidpointRounding.AwayFromZero)).ToString();
                                            ded5 = (Math.Round(Convert.ToDecimal(ded5), 2, MidpointRounding.AwayFromZero)).ToString();
                                            Fine = (Math.Round(Convert.ToDecimal(Fine), 2, MidpointRounding.AwayFromZero)).ToString();

                                            DedOthers1 = (Math.Round(Convert.ToDecimal(DedOthers1), 2, MidpointRounding.AwayFromZero)).ToString();
                                            DedOthers2 = (Math.Round(Convert.ToDecimal(DedOthers2), 2, MidpointRounding.AwayFromZero)).ToString();
                                            Mess = (Math.Round(Convert.ToDecimal(Mess), 2, MidpointRounding.AwayFromZero)).ToString();
                                            if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
                                            {
                                                string Oneday_PfSal = "";
                                                string Val_Sal1 = "";
                                                string Base_New = "";
                                                if ((Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(0.00)) || (Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(0.0)))
                                                {
                                                    Base_New = (Convert.ToDecimal(BasicAndDA)).ToString();
                                                    pfAmt = ((Convert.ToDecimal(Base_New) * Convert.ToDecimal(pf_per)) / 100).ToString();
                                                    PFS = (Math.Round(Convert.ToDecimal(pfAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }

                                                else
                                                {
                                                    PFS = "0";
                                                }
                                            }
                                            else
                                            {
                                                PFS = "0";
                                            }

                                            if ((Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(0.00)) || (Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(0.0)))
                                            {
                                                if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
                                                {
                                                    string Base_New = "";
                                                    Base_New = (Convert.ToDecimal(BasicAndDA) / (Convert.ToDecimal(ESI_Per)) / 100).ToString();
                                                    ESI = (Math.Round(Convert.ToDecimal(Base_New), 0, MidpointRounding.AwayFromZero)).ToString();

                                                }
                                                else
                                                {
                                                    ESI = "0";
                                                }
                                            }
                                            else
                                            {
                                                ESI = "0";
                                            }



                                            TotalEarnings = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(BasicHRA) + Convert.ToDecimal(DayIncentive) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(All5) + Convert.ToDecimal(OTHoursAmtNew_Str)).ToString();
                                            TotalEarnings = (Math.Round(Convert.ToDecimal(TotalEarnings), 2, MidpointRounding.ToEven)).ToString();

                                            TotalDeductions = (Convert.ToDecimal(Advance) + Convert.ToDecimal(PFS) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(DedOthers1) + Convert.ToDecimal(DedOthers2) + Convert.ToDecimal(Fine)).ToString();
                                            //TotalDeductions = (Convert.ToDecimal(Advance) + Convert.ToDecimal(PFS) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(DedOthers1) + Convert.ToDecimal(DedOthers2)).ToString();
                                            TotalDeductions = (Math.Round(Convert.ToDecimal(TotalDeductions), 2, MidpointRounding.ToEven)).ToString();
                                            NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();

                                            //Change start

                                            //NetPay = (Convert.ToDecimal(NetPay) - Convert.ToDecimal(Fine)).ToString();
                                            NetPay = (Convert.ToDecimal(NetPay) + Convert.ToDecimal(Gift)).ToString();
                                            //Change end
                                            NetPay = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();
                                            if (Convert.ToDecimal(NetPay) < 0)
                                            {
                                                NetPay = "0";
                                            }
                                            double d1 = Convert.ToDouble(NetPay.ToString());
                                            int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                                            RoundOffNetPay = rounded1.ToString();

                                            Words = NumerictoNumber(Convert.ToInt32(RoundOffNetPay), isUK).ToString() + " " + "Only";
                                        }


                                    }

                                    else //Labour Salary Calculation
                                    {
                                        Basic_For_SM = "0";
                                        BasicAndDA = "0.00";
                                        BasicHRA = "0.00";
                                        ConvAllow = "0.00"; FullNight_Amt = "0.00";
                                        EduAllow = "0.00"; HalfNight_Amt = "0.00";
                                        MediAllow = "0.00"; ESI = "0";
                                        BasicRAI = "0.00";  PFS = "0";
                                        SpinningAmt = "0.00"; ThreeSided_Amt = "0.00";
                                        stamp = "0";
                                        AdvAmt = 0;
                                        ID = "";
                                        basicsalary = 0;
                                        Basic_For_SM = "0";
                                        DataTable dt1 = new DataTable();
                                        dt1 = objdata.NewSlary_Load(SessionCcode, SessionLcode, EmpNo);
                                        DataTable dt_con = new DataTable();
                                        DataTable dt_slab = new DataTable();
                                        DataTable Da_PF = new DataTable();
                                        DataTable da_Incentive = new DataTable();
                                       
                                        string pf_per = "";
                                        string ESI_Per = "";
                                        string Pf_Salary = "";

                                        DataTable dteligible = new DataTable();
                                        DataTable dtpf = new DataTable();
                                        DataTable da_Salary = new DataTable();
                                        dtpf = objdata.Load_pf_details(SessionCcode, SessionLcode);
                                        dteligible = objdata.EligibleESI_PF(SessionCcode, SessionLcode, EmpNo);

                                        Pf_Salary = (Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString())).ToString();
                                        pf_per = (Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())).ToString();
                                        ESI_Per = (Convert.ToDecimal(dtpf.Rows[0]["ESI_Per"].ToString())).ToString();

                                        basic = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString())).ToString();
                                        basicsalary = (Convert.ToDecimal(dt1.Rows[0]["Base"].ToString()));


                                        DataTable dtIncentive = new DataTable();
                                        string Eligible_Incentive = "";
                                        SSQL = "select Incentive_Eligible from [SKS_Spay]..Employee_Mst  where MachineID ='" + ExistNo + "'";
                                        dtIncentive = objdata.RptEmployeeMultipleDetails(SSQL);
                                        if (dtIncentive.Rows.Count != 0)
                                        {
                                            Eligible_Incentive = dtIncentive.Rows[0]["Incentive_Eligible"].ToString();
                                        }
                                        if (ExistNo == "1273")
                                        {
                                            ExistNo = "1273";
                                        }
                                        if (Eligible_Incentive != "2")
                                        {
                                            DayIncentive = "0";
                                        }
                                        else
                                        {
                                            double Full_OT_Days = 0.0;
                                            double Full_OT_DaysElegible = 0.0;

                                            Full_OT_DaysElegible = Convert.ToDouble(EmployeeDays) + Convert.ToDouble(ThreeSided);
                                            Full_OT_Days = (Full_OT_DaysElegible < Convert.ToDouble(Fixed_Work_Dayss)) ? Convert.ToDouble(Full_OT_DaysElegible) : Convert.ToDouble(Fixed_Work_Dayss);
                                            DayIncentive = "0";
                                            SSQL = "select WorkerDays,WorkerAmt from WorkerIncentive_mst where WorkerDays='" + Full_OT_Days + "' and  MonthDays='" + Fixed_Work_Dayss + "'";
                                            da_Incentive = objdata.RptEmployeeMultipleDetails(this.SSQL);
                                            if (da_Incentive.Rows.Count != 0)
                                            {
                                                DayIncentive = Convert.ToDecimal(da_Incentive.Rows[0]["WorkerAmt"].ToString()).ToString();
                                            }
                                            else
                                            {
                                                Full_OT_Days = 0.0;
                                                Full_OT_DaysElegible = 0.0;
                                            }
                                        }
                                        if (EmpType == "2") //Labour
                                        {

                                            DayIncentive = "0";
                                            Basic_For_SM = "0";
                                            WashingAllow = "0.00";

                                            basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                            BasicAndDA = (Convert.ToDecimal(basic) * Convert.ToDecimal(EmployeeDays)).ToString();
                                            BasicAndDA = (Math.Round(Convert.ToDecimal(BasicAndDA), 0, MidpointRounding.AwayFromZero)).ToString();

                                            //Hrs Salary
                                            BasicHRA = ((Convert.ToDecimal(basic) / Convert.ToDecimal(8)) * Convert.ToDecimal(cl)).ToString();
                                            BasicHRA = (Math.Round(Convert.ToDecimal(BasicHRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                            ////OT Hours
                                            //ThreeSided = OTHoursNew_Str;
                                            //OTHoursNew_Str = "";
                                            //OTHoursAmtNew_Str = ((Convert.ToDecimal(basic) / Convert.ToDecimal(8)) * Convert.ToDecimal(ThreeSided)).ToString();
                                            //OTHoursAmtNew_Str = (Math.Round(Convert.ToDecimal(OTHoursAmtNew_Str), 0, MidpointRounding.AwayFromZero)).ToString();

                                            OTHoursAmtNew_Str = ((Convert.ToDecimal(basic) / Convert.ToDecimal(8)) * Convert.ToDecimal(OTHoursNew_Str)).ToString();
                                            OTHoursAmtNew_Str = Math.Round(Convert.ToDecimal(OTHoursAmtNew_Str),0,MidpointRounding.AwayFromZero).ToString();
                                            ThreeSided_Amt = (Convert.ToDecimal(8) * Convert.ToDecimal(ThreeSided)).ToString();
                                            ThreeSided_Amt = Math.Round(Convert.ToDecimal(ThreeSided_Amt), 0, MidpointRounding.AwayFromZero).ToString();
                                            SpinningAmt = (Convert.ToDecimal(basic) * Convert.ToDecimal(ThreeSided)).ToString();
                                            SpinningAmt = Math.Round(Convert.ToDecimal(SpinningAmt), 0, MidpointRounding.AwayFromZero).ToString();

                                        }
                                        if (EmpType == "3")//Weekly
                                        {
                                            
                                            Basic_For_SM = "0";
                                            //Basic
                                            basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                            //Basic_salary
                                            BasicAndDA = (Convert.ToDecimal(basic) * Convert.ToDecimal(EmployeeDays)).ToString();
                                            BasicAndDA = (Math.Round(Convert.ToDecimal(BasicAndDA), 0, MidpointRounding.AwayFromZero)).ToString();

                                            //Hrs Salary
                                            BasicHRA = ((Convert.ToDecimal(basic) / Convert.ToDecimal(8)) * Convert.ToDecimal(cl)).ToString();
                                            BasicHRA = (Math.Round(Convert.ToDecimal(BasicHRA), 0, MidpointRounding.AwayFromZero)).ToString();

                                            //OT Hours
                                            ThreeSided = OTHoursNew_Str;
                                            OTHoursNew_Str = "";
                                            OTHoursAmtNew_Str = ((Convert.ToDecimal(basic) / Convert.ToDecimal(8)) * Convert.ToDecimal(ThreeSided)).ToString();
                                            OTHoursAmtNew_Str = (Math.Round(Convert.ToDecimal(OTHoursAmtNew_Str), 0, MidpointRounding.AwayFromZero)).ToString();

                                        }


                                        ////Incentive
                                        //decimal  Ot_days_val = 0;
                                        //if (Convert.ToDecimal(ThreeSided) > 8)
                                        //{
                                        //    Ot_days_val = (Convert.ToDecimal(ThreeSided) / 8);
                                        //}

                                        //DayIncentive = "0";
                                        //SSQL = "select WorkerDays,WorkerAmt  from WorkerIncentive_mst	where  WorkerDays<='" + EmployeeDays + "' ";
                                        //da_Incentive = objdata.RptEmployeeMultipleDetails(SSQL);
                                        //if (da_Incentive.Rows.Count != 0)
                                        //{
                                        //    string DayIncentiveVal = da_Incentive.Rows[0]["WorkerAmt"].ToString();
                                        //    DayIncentive = (Convert.ToDecimal(DayIncentiveVal) * Convert.ToDecimal(EmployeeDays)).ToString();
                                        //}
                                        //else
                                        //{
                                        //    DayIncentive = "0";
                                        //}


                                        //PF/ESI

                                        if (dteligible.Rows[0]["EligiblePF"].ToString() == "1")
                                        {
                                            //string Oneday_PfSal = "";
                                            //string Val_Sal1 = "";
                                            //string Base_New = "";
                                            //if ((Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(0.00)) || (Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(0.0)))
                                            //{
                                            //    Base_New = (Convert.ToDecimal(165) * Convert.ToDecimal(EmployeeDays)).ToString();
                                            //    pfAmt = ((Convert.ToDecimal(Base_New) * Convert.ToDecimal(pf_per)) / 100).ToString();
                                            //    PFS = (Math.Round(Convert.ToDecimal(pfAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                            //}

                                            //else
                                            //{
                                            //    PFS = "0";
                                            //}

                                            DataTable dtBasic_Dect = new DataTable();
                                            int WashAmt = 0;
                                            SSQL = "";
                                            SSQL = "select BasicDA,WashingAllow from MstBasicDet where EmployeeType='" + EmpType + "' ";
                                            dtBasic_Dect = objdata.RptEmployeeMultipleDetails(SSQL);

                                            if (dtBasic_Dect.Rows.Count > 0)
                                            {
                                                WashAmt = Convert.ToInt32(dtBasic_Dect.Rows[0]["WashingAllow"]);
                                                WashingAllow  = Math.Round(Convert.ToDecimal((Convert.ToDecimal(basic) / Convert.ToDecimal(WashAmt)).ToString()), 0, MidpointRounding.AwayFromZero).ToString();
                                                Mess = Math.Round(Convert.ToDecimal((Convert.ToDecimal(basic) - (Convert.ToDecimal(Pf_Salary) + Convert.ToDecimal(WashingAllow))).ToString()), 0, MidpointRounding.AwayFromZero).ToString();
                                            }

                                        }
                                        else if ((Convert.ToDecimal(EmployeeDays) <= Convert.ToDecimal((double)0.0)) && (Convert.ToDecimal(EmployeeDays) <= Convert.ToDecimal((double)0.0)))
                                        {
                                            PFS = "0";
                                        }
                                        else if (Convert.ToDecimal(EmployeeDays) >= Convert.ToDecimal(0x1a))
                                        {
                                            pfAmt = ((Convert.ToDecimal((Convert.ToDecimal(Pf_Salary) * Convert.ToDecimal(26)).ToString()) * Convert.ToDecimal(pf_per)) / 100).ToString();
                                            PFS = Math.Round(Convert.ToDecimal(pfAmt), 0, MidpointRounding.AwayFromZero).ToString();
                                        }
                                        else
                                        {
                                            PFS = "0";
                                            WashingAllow = "0";
                                            Mess = "0";
                                        }

                                        if ((Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(0.00)) || (Convert.ToDecimal(EmployeeDays) > Convert.ToDecimal(0.0)))
                                        {
                                            //if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
                                            //{

                                            //    string Base_New = (Convert.ToDecimal(165) * Convert.ToDecimal(EmployeeDays)).ToString();
                                            //    Base_New = (Convert.ToDecimal(Base_New) / (Convert.ToDecimal(ESI_Per)) / 100).ToString();
                                            //    ESI = (Math.Round(Convert.ToDecimal(Base_New), 0, MidpointRounding.AwayFromZero)).ToString();

                                            //}
                                            //else
                                            //{
                                            //    ESI = "0";
                                            //}
                                            if (dteligible.Rows[0]["ElgibleESI"].ToString() == "1")
                                            {
                                                if (Convert.ToDecimal(EmployeeDays) >= Convert.ToDecimal(0x1a))
                                                {
                                                    ESI = ((Convert.ToDecimal(((Convert.ToDecimal(Pf_Salary) + Convert.ToDecimal(Mess)) * Convert.ToDecimal(0x1a)).ToString()) * Convert.ToDecimal(ESI_Per)) / 100).ToString();
                                                    ESI = Math.Round(Convert.ToDecimal(ESI), 0, MidpointRounding.AwayFromZero).ToString();
                                                }
                                                else
                                                {
                                                    ESI = ((Convert.ToDecimal(((Convert.ToDecimal(Pf_Salary) + Convert.ToDecimal(Mess)) * Convert.ToDecimal(EmployeeDays)).ToString()) * Convert.ToDecimal(ESI_Per)) / 100).ToString();
                                                    ESI = Math.Round(Convert.ToDecimal(ESI), 0, MidpointRounding.AwayFromZero).ToString();
                                                }
                                            }
                                            else
                                            {
                                                ESI = "0";
                                            }
                                        }
                                        else
                                        {
                                            ESI = "0";
                                        }

                                        All1 = (Convert.ToDecimal(dt1.Rows[0]["Alllowance1amt"].ToString())).ToString();
                                        All1 = (Math.Round(Convert.ToDecimal(All1), 2, MidpointRounding.AwayFromZero)).ToString();
                                        All2 = (Convert.ToDecimal(dt1.Rows[0]["Allowance2amt"].ToString())).ToString();
                                        All2 = (Math.Round(Convert.ToDecimal(All2), 2, MidpointRounding.AwayFromZero)).ToString();
                                        Ded1 = (Convert.ToDecimal(dt1.Rows[0]["Deduction1"].ToString())).ToString();
                                        Ded1 = (Math.Round(Convert.ToDecimal(Ded1), 2, MidpointRounding.AwayFromZero)).ToString();
                                        ded2 = ((Convert.ToDecimal(EmployeeDays)) * Convert.ToDecimal(dt1.Rows[0]["Deduction2"].ToString())).ToString();
                                        ded2 = (Math.Round(Convert.ToDecimal(ded2), 2, MidpointRounding.AwayFromZero)).ToString();
                                        ded3 = (Math.Round(Convert.ToDecimal(ded3), 2, MidpointRounding.AwayFromZero)).ToString();
                                        ded4 = (Math.Round(Convert.ToDecimal(ded4), 2, MidpointRounding.AwayFromZero)).ToString();
                                        ded5 = (Math.Round(Convert.ToDecimal(ded5), 2, MidpointRounding.AwayFromZero)).ToString();
                                        Fine = (Math.Round(Convert.ToDecimal(Fine), 2, MidpointRounding.AwayFromZero)).ToString();
                                        Mess = Math.Round(Convert.ToDecimal(Mess), 2, MidpointRounding.AwayFromZero).ToString();
                                        DedOthers1 = (Math.Round(Convert.ToDecimal(DedOthers1), 2, MidpointRounding.AwayFromZero)).ToString();
                                        DedOthers2 = (Math.Round(Convert.ToDecimal(DedOthers2), 2, MidpointRounding.AwayFromZero)).ToString();

                                        TotalEarnings = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(BasicHRA) + Convert.ToDecimal(DayIncentive) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(All5) + Convert.ToDecimal(OTHoursAmtNew_Str)).ToString();
                                    //    TotalEarnings = (Convert.ToDecimal(BasicAndDA) + Convert.ToDecimal(BasicHRA) + Convert.ToDecimal(DayIncentive) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) +  Convert.ToDecimal(OTHoursAmtNew_Str)).ToString();
                                        TotalEarnings = (Math.Round(Convert.ToDecimal(TotalEarnings), 2, MidpointRounding.ToEven)).ToString();
                                        TotalDeductions = (Convert.ToDecimal(Advance) + Convert.ToDecimal(PFS) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(DedOthers1) + Convert.ToDecimal(DedOthers2) + Convert.ToDecimal(Fine)).ToString();
                                       // TotalDeductions = (Convert.ToDecimal(Advance) + Convert.ToDecimal(PFS) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(DedOthers1) + Convert.ToDecimal(DedOthers2)).ToString();
                                        TotalDeductions = (Math.Round(Convert.ToDecimal(TotalDeductions), 2, MidpointRounding.ToEven)).ToString();
                                        NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();



                                        //Change start

                                        //NetPay = (Convert.ToDecimal(NetPay) - Convert.ToDecimal(Fine)).ToString();
                                        NetPay = (Convert.ToDecimal(NetPay) + Convert.ToDecimal(Gift)).ToString();
                                        //Change end
                                        NetPay = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();
                                        if (Convert.ToDecimal(NetPay) < 0)
                                        {
                                            NetPay = "0";
                                        }
                                        double d1 = Convert.ToDouble(NetPay.ToString());
                                        int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                                        RoundOffNetPay = rounded1.ToString();


                                        Words = NumerictoNumber(Convert.ToInt32(RoundOffNetPay), isUK).ToString() + " " + "Only";


                                    }//labour calculation 
                                }

                                if (!ErrVerify)
                                {
                                    string pfEligible = objdata.PF_Eligible_Salary(EmpNo, SessionCcode, SessionLcode);
                                    DateTime MyDate = new DateTime();
                                    objSal.TotalWorkingDays = Work;
                                    objSal.NFh = NFh;
                                    objSal.Lcode = SessionLcode;
                                    objSal.Ccode = SessionCcode;
                                    objSal.ApplyLeaveDays = lopdays;
                                    objSal.GrossEarnings = TotalEarnings;
                                    objSal.TotalDeduction = TotalDeductions;
                                    objSal.NetPay = NetPay;
                                    objSal.RoundOffNetPay = RoundOffNetPay;
                                    objSal.Words = Words;
                                    objSal.AvailableDays = EmployeeDays;
                                    objSal.SalaryDate = txtsalaryday.Text;
                                    objSal.work = EmployeeDays;
                                    objSal.weekoff = WeekOff;
                                    objSal.CL = cl;
                                    objSal.FixedBase = basic;
                                    objSal.FixedFDA = "0.00";
                                    objSal.FixedFRA = "0.00";
                                    objSal.HomeTown = HomeDays;
                                    objSal.HalfNightAmt = HalfNight_Amt;
                                    objSal.FullNightAmt = FullNight_Amt;
                                    objSal.ThreesidedAmt = ThreeSided_Amt;
                                    objSal.SpinningAmt = SpinningAmt;
                                    objSal.DayIncentive = DayIncentive;
                                    MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    //if (EmpType == "1")
                                    //{
                                    objSal.BasicAndDA = BasicAndDA;
                                    objSal.HRA = BasicHRA_Val;
                                    objSal.FDA = ConvAllow_Val;
                                    objSal.VDA = BasicAndDA_Val;
                                    //objSal.OT = "0";
                                    objSal.Allowances1 = All1;
                                    objSal.Allowances2 = All2;
                                    objSal.Allowances3 = All3;
                                    objSal.Allowances4 = Al4;
                                    objSal.Allowances5 = All5;
                                  
                                    objSal.Deduction1 = Ded1;
                                    objSal.Deduction2 = ded2;
                                    objSal.Deduction3 = ded3;
                                    objSal.Deduction4 = ded4;
                                    objSal.Deduction5 = ded5;
                                    objSal.Fine = Fine;
                                    objSal.ProvidentFund = PFS;
                                    objSal.ESI = ESI;
                                    objSal.Stamp = basic1;
                                    objSal.Advance = Advance;
                                    objSal.LossofPay = lop;
                                    objSal.OT = OTDays;
                                    objSal.LOPDays = lopdays;

                                    //Basic Spilit Det
                                    objSal.BasicDA = BasicAndDA;
                                    objSal.Basic_HRA = BasicHRA;
                                    objSal.Conv_Allow = ConvAllow;
                                    objSal.Edu_Allow = EduAllow;
                                    objSal.Medi_Allow = MediAllow;
                                    objSal.Basic_RAI = BasicRAI;
                                    objSal.Washing_Allow = WashingAllow;
                                    objSal.MessDeduction = Mess;

                                    if (pfEligible == "1")
                                    {
                                        objSal.PfSalary = PFS;
                                        objSal.Emp_PF = PFS;
                                    }
                                    else
                                    {
                                        objSal.PfSalary = "0.00";
                                        objSal.Emp_PF = "0.00";
                                    }

                                    //Sub-Staff OT Hours Set
                                    objSal.OTHoursAmt = OTHoursAmtNew_Str;
                                    objSal.OTHoursNew = OTHoursNew_Str;
                                    objSal.Leave_Credit_Check = "0";

                                    objSal.Fixed_Work_Days = Fixed_Work_Dayss;
                                    objSal.WH_Work_Days = WH_Work_Days;

                                    //Deduction Others
                                    objSal.Ded_Others1 = DedOthers1;
                                    objSal.Ded_Others2 = DedOthers2;

                                    //Employeer PF And ESI
                                    objSal.EmployeerPFone = EmployeerPFOne;
                                    objSal.EmployeerPFTwo = EmployeerPFTwo;
                                    objSal.EmployeerESI = EmployeerESI;

                                    objSal.Leave_Credit_Days = "0";
                                    objSal.Leave_Credit_Add = "0";
                                    objSal.Leave_Credit_Type = "0";

                                    objSal.Gift = Gift;
                                   

                                    //PassFlag = true;

                                    //Check Salary Conformation
                                    DataTable Sal_Confm_DT = new DataTable();
                                    string Sal_Conform_qry = "Select * from SalaryDetails where EmpNo='" + EmpNo + "' and " +
                                                 "Month='" + ddlMonths.Text + "' and FinancialYear='" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                 " and convert(datetime,FromDate,105) = convert(datetime,'" + txtFrom.Text + "',105)" +
                                                 " and convert(datetime, ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105) And Salary_Conform='1'";
                                    Sal_Confm_DT = objdata.RptEmployeeMultipleDetails(Sal_Conform_qry);
                                    if (Sal_Confm_DT.Rows.Count == 0)
                                    {
                                        PassFlag = true;
                                    }
                                    else
                                    {
                                        PassFlag = false;
                                    }
                                }
                                if (PassFlag == true)
                                {
                                    if (Updateval == "0")
                                    {
                                        string Del2 = "Delete from SalaryDetails where EmpNo='" + EmpNo + "' and " +
                                                 "Month='" + ddlMonths.Text + "' and FinancialYear='" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                 " and convert(datetime,FromDate,105) = convert(datetime,'" + txtFrom.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";
                                        //cn.Open();
                                        SqlCommand cmd_verify3 = new SqlCommand(Del2, cn);
                                        cmd_verify3.ExecuteNonQuery();
                                    }
                                    else if (Updateval == "1")
                                    {
                                        string Del = "Delete from SalaryDetails where EmpNo='" + EmpNo + "' and " +
                                                 "Month='" + ddlMonths.Text + "' and FinancialYear='" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'" +
                                                 " and convert(datetime,FromDate,105) = convert(datetime,'" + txtFrom.Text + "',105) and convert(datetime, ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";
                                        //cn.Open();
                                        SqlCommand cmd_verify1 = new SqlCommand(Del, cn);
                                        cmd_verify1.ExecuteNonQuery();
                                        MyDate = DateTime.ParseExact(objSal.SalaryDate, "dd-MM-yyyy", null);

                                        TempDate = Convert.ToDateTime(txtsalaryday.Text).AddMonths(0).ToShortDateString();
                                        TransDate = DateTime.ParseExact(TempDate, "dd/MM/yyyy", null);

                                        //Get Basic Salary for Salary Master
                                        objSal.BasicforSM = Basic_For_SM;
                                        objdata.SalaryDetails(objSal, EmpNo, ExistNo, MyDate, MyMonth, Year, ddlfinance.SelectedValue, TransDate, MyDate1, MyDate2, SalarythroughType, Get_Wagestype);


                                        string Balanceday = objdata.Contract_GetEmpDet(EmpNo, SessionCcode, SessionLcode);
                                        if (Balanceday.Trim() != "")
                                        {
                                            if (Convert.ToDecimal(Balanceday) > 0)
                                            {
                                                DataTable dt_det = new DataTable();
                                                dt_det = objdata.Contract_getPlanDetails(EmpNo, SessionCcode, SessionLcode);
                                                if (dt_det.Rows.Count > 0)
                                                {
                                                    if (dt_det.Rows[0]["ContractType"].ToString() == "1")
                                                    {
                                                        val = (Convert.ToDecimal(EmployeeDays));
                                                        Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                        //objdata.Contract_Reducing(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode);
                                                    }
                                                    else if (dt_det.Rows[0]["ContractType"].ToString() == "2")
                                                    {
                                                        val = (Convert.ToDecimal(EmployeeDays));
                                                        if (val >= Convert.ToDecimal(dt_det.Rows[0]["FixedDays"].ToString()))
                                                        {
                                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                                                            Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                            //objdata.Contract_Reducing_Month(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode, Mon);
                                                        }
                                                        else
                                                        {
                                                            string Mon = (Convert.ToInt32(dt_det.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                                                            //Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                            //objdata.Contract_Reducing(Balanceday, ddlempCodeStaff.SelectedValue, val, SessionCcode, SessionLcode, Mon);
                                                            // objdata.Not_Contract_Reducing_Month(EmpNo, val.ToString(), SessionCcode, SessionLcode);
                                                        }
                                                    }
                                                    else if (dt_det.Rows[0]["ContractType"].ToString() == "3")
                                                    {
                                                        val = (Convert.ToDecimal(EmployeeDays));
                                                        Balanceday = (Convert.ToDecimal(Balanceday) - val).ToString();
                                                        //objdata.Contract_Reducing(Balanceday, EmpNo, val.ToString(), SessionCcode, SessionLcode);
                                                        SaveFlag = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Finished Success Fully');", true);
                                cn.Close();
                                sSourceConnection.Close();
                            }

                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }

    protected void btnUpload_Click1(object sender, EventArgs e)
    {
        bool flag = false;
        string str = "0.00";
        string str2 = "0.00";
        string str3 = "0.00";
        string str4 = "0.00";
        string str5 = "0.00";
        string str6 = "0.00";
        string str7 = "0.00";
        string str8 = "0.00";
        string str9 = "0.00";
        string str10 = "0.00";
        string str11 = "0.00";

        string Fine = "0.00";
        string Gift = "0.00";
        //try
        //{
            if (this.ddlMonths.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Select the Month.');", true);
                flag = true;
            }
            else if (this.txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                flag = true;
            }
            else if (this.txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                flag = true;
            }
            else if (this.ddlcategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Select the Category.');", true);
                flag = true;
            }
            else if (this.ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Select the Category Properly.');", true);
                flag = true;
            }
            if (this.rbsalary.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Select the Wges Type.');", true);
                flag = true;
            }
            decimal num = 0M;
            if ((this.ddlMonths.SelectedValue == "January") || ((this.ddlMonths.SelectedValue == "March") || ((this.ddlMonths.SelectedValue == "May") || ((this.ddlMonths.SelectedValue == "July") || ((this.ddlMonths.SelectedValue == "August") || ((this.ddlMonths.SelectedValue == "October") || (this.ddlMonths.SelectedValue == "December")))))))
            {
                num = 31M;
            }
            else if ((this.ddlMonths.SelectedValue == "April") || ((this.ddlMonths.SelectedValue == "June") || ((this.ddlMonths.SelectedValue == "September") || (this.ddlMonths.SelectedValue == "November"))))
            {
                num = 30M;
            }
            else if (this.ddlMonths.SelectedValue == "February")
            {
                num = (((Convert.ToInt32(this.ddlfinance.SelectedValue) + 1) % 4) != 0) ? 28M : 29M;
            }
            DateTime time7 = Convert.ToDateTime(this.txtFrom.Text).AddMonths(0);
            this.TempDate = time7.ToShortDateString();
            DateTime time = Convert.ToDateTime(this.TempDate);
            SalaryCalculation.d = time.Month;
            int month = time.Month;
            int num17 = time.Month;
            string str12 = "";
            int d = SalaryCalculation.d;
            switch (d)
            {
                case 1:
                    str12 = "January";
                    break;

                case 2:
                    str12 = "February";
                    break;

                case 3:
                    str12 = "March";
                    break;

                case 4:
                    str12 = "April";
                    break;

                case 5:
                    str12 = "May";
                    break;

                case 6:
                    str12 = "June";
                    break;

                case 7:
                    str12 = "July";
                    break;

                case 8:
                    str12 = "August";
                    break;

                case 9:
                    str12 = "September";
                    break;

                case 10:
                    str12 = "October";
                    break;

                case 11:
                    str12 = "November";
                    break;

                case 12:
                    str12 = "December";
                    break;

                default:
                    break;
            }
            if (this.ChkCivilIncentive.Checked)
            {
                if (this.txtIstWeekDate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the First Week Date Properly.');", true);
                    flag = true;
                }
                if (this.txtLastWeekDate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Last Week Date Properly.');", true);
                    flag = true;
                }
            }
            if (this.FileUpload.HasFile)
            {
                this.FileUpload.SaveAs(base.Server.MapPath("Upload/" + this.FileUpload.FileName));
            }
            if (!flag)
            {
                DateTime dfrom = DateTime.ParseExact(this.txtFrom.Text, "dd-MM-yyyy", null);
                DateTime dto = DateTime.ParseExact(this.txtTo.Text, "dd-MM-yyyy", null);
                if (Convert.ToDateTime(this.txtTo.Text) < Convert.ToDateTime(this.txtFrom.Text))
                {
                    ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the From Date...');", true);
                    this.txtFrom.Text = null;
                    this.txtTo.Text = null;
                    flag = true;
                }
                if (!flag)
                {
                    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + base.Server.MapPath("Upload/" + this.FileUpload.FileName) + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"");
                    DataTable table1 = new DataTable();
                    using (connection)
                    {
                        connection.Open();
                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", connection);
                        connection.Close();
                        using (connection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            connection.Open();
                        }
                        using (OleDbDataReader reader = command.ExecuteReader())
                        {
                            bool hasRows = reader.HasRows;
                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable table = new DataTable();
                        table = ds.Tables[0];
                        for (int i = 0; i < table.Columns.Count; i++)
                        {
                            table.Columns[i].ColumnName = table.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                        int num3 = 0;
                        while (true)
                        {
                            if (num3 >= table.Columns.Count)
                            {
                                string str13 = ConfigurationManager.AppSettings["ConnectionString"];
                                int num4 = 0;
                                while (true)
                                {
                                    string[] strArray;
                                    if (num4 >= table.Rows.Count)
                                    {
                                        if (!flag)
                                        {
                                            for (int i = 0; i < table.Rows.Count; i++)
                                            {
                                                string str30 = "1";
                                                bool flag2 = false;
                                                bool flag3 = false;
                                                string empNo = "";
                                                string str32 = "";
                                                string str33 = "";
                                                string wagesType = "";
                                                basic = "0.00";
                                                All1 = "0.00";
                                                All2 = "0.00";
                                                All3 = "0.00";
                                                All5 = "0.00";
                                                Al4 = "0.00";
                                                Ded1 = "0.00";
                                                ded2 = "0.00";
                                                ded3 = "0.00";
                                                ded4 = "0.00";
                                                ded5 = "0.00";
                                                SqlConnection connection3 = new SqlConnection(str13);
                                                connection3.Open();
                                                string str35 = "Select DepartmentCd from MstDepartment where DepartmentNm = '" + table.Rows[i][0].ToString() + "'";
                                                this.DepartmentCode = Convert.ToString(new SqlCommand(str35, connection3).ExecuteScalar());
                                                strArray = new string[] { "Select EmpNo from SalaryDetails where EmpNo='", table.Rows[i][1].ToString(), "' and Month='", this.ddlMonths.Text, "' and FinancialYear='", this.ddlfinance.SelectedValue, "' and Ccode='", this.SessionCcode, "' and Lcode='", this.SessionLcode, "' and Process_Mode='1' and convert(datetime,FromDate,105) = convert(datetime,'", this.txtFrom.Text, "',105) and convert(datetime, ToDate, 105) = convert(datetime, '", this.txtTo.Text, "', 105)" };
                                                //strArray[9] = this.SessionLcode;
                                                //strArray[10] = "' and Process_Mode='1' and convert(datetime,FromDate,105) = convert(datetime,'";
                                                //strArray[11] = this.txtFrom.Text;
                                                //strArray[12] = "',105) and convert(datetime, ToDate, 105) = convert(datetime, '";
                                                //strArray[13] = this.txtTo.Text;
                                                //strArray[14] = "', 105)";
                                                string str36 = string.Concat(strArray);
                                                strArray = new string[] { "Select ED.EmployeeType from EmployeeDetails ED inner Join MstEmployeeType ME on ME.EmpTypeCd = ED.EmployeeType where ED.EmpNo='", table.Rows[i][1].ToString(), "' AND ED.Ccode='", this.SessionCcode, "' and ED.Lcode = '", this.SessionLcode, "'" };
                                                string str37 = string.Concat(strArray);
                                                SqlCommand command8 = new SqlCommand(str37, connection3);
                                                str32 = command8.ExecuteScalar().ToString();
                                                strArray = new string[] { "Select Wagestype from Officialprofile where EmpNo='", table.Rows[i][1].ToString(), "' and Ccode='", this.SessionCcode, "' and Lcode='", this.SessionLcode, "'" };
                                                SqlCommand command9 = new SqlCommand(string.Concat(strArray), connection3);
                                                wagesType = command9.ExecuteScalar().ToString();
                                                strArray = new string[] { "Select SalaryThrough from Officialprofile where EmpNo='", table.Rows[i][1].ToString(), "' and Ccode='", this.SessionCcode, "' and Lcode='", this.SessionLcode, "'" };
                                                SqlCommand command10 = new SqlCommand(string.Concat(strArray), connection3);
                                                str33 = command10.ExecuteScalar().ToString();
                                                this.Name_Upload = Convert.ToString(new SqlCommand(str36, connection3).ExecuteScalar());
                                                this.TempDate = Convert.ToDateTime(this.txtFrom.Text).AddMonths(0).ToShortDateString();
                                                empNo = table.Rows[i][1].ToString();
                                                All3 = table.Rows[i][4].ToString();
                                                Al4 = table.Rows[i][5].ToString();
                                                All5 = table.Rows[i][6].ToString();
                                                ded3 = table.Rows[i][7].ToString();
                                                ded4 = table.Rows[i][8].ToString();
                                                ded5 = table.Rows[i][9].ToString();
                                                Advance = table.Rows[i][10].ToString();
                                                ExistNo = table.Rows[i][2].ToString();
                                                DedOthers1 = table.Rows[i][11].ToString();
                                                Mess = table.Rows[i][12].ToString();
                                                str11 = table.Rows[i][13].ToString();

                                                Fine = table.Rows[i][14].ToString();
                                                Gift = table.Rows[i][15].ToString();

                                                DateTime time6 = Convert.ToDateTime(this.TempDate);
                                                SalaryCalculation.d = time6.Month;
                                                int num6 = time6.Month;
                                                int year = time6.Year;
                                                string months = "";
                                                d = SalaryCalculation.d;
                                                switch (d)
                                                {
                                                    case 1:
                                                        months = "January";
                                                        break;

                                                    case 2:
                                                        months = "February";
                                                        break;

                                                    case 3:
                                                        months = "March";
                                                        break;

                                                    case 4:
                                                        months = "April";
                                                        break;

                                                    case 5:
                                                        months = "May";
                                                        break;

                                                    case 6:
                                                        months = "June";
                                                        break;

                                                    case 7:
                                                        months = "July";
                                                        break;

                                                    case 8:
                                                        months = "August";
                                                        break;

                                                    case 9:
                                                        months = "September";
                                                        break;

                                                    case 10:
                                                        months = "October";
                                                        break;

                                                    case 11:
                                                        months = "November";
                                                        break;

                                                    case 12:
                                                        months = "December";
                                                        break;

                                                    default:
                                                        break;
                                                }
                                                this.SMonth = Convert.ToString(num6);
                                                this.Year = Convert.ToString(year);
                                                this.dd = DateTime.DaysInMonth(year, num6);
                                                this.MyMonth = months;
                                                DataTable table3 = new DataTable();
                                                OTHoursNew_Str = "0";
                                                OTHoursAmtNew_Str = "0";
                                                lop = "0.00";
                                                table3 = this.objdata.Salary_attenanceDetails(empNo, months, this.ddlfinance.SelectedValue, this.DepartmentCode, this.SessionCcode, this.SessionLcode, dfrom, dto);
                                                if (table3.Rows.Count <= 0)
                                                {
                                                    EmployeeDays = "0";
                                                    Work = "0";
                                                    NFh = "0";
                                                    cl = "0";
                                                    lopdays = "0";
                                                    WeekOff = "0";
                                                    Home = "0";
                                                    halfNight = "0";
                                                    FullNight = "0";
                                                    ThreeSided = "0";
                                                    OTHoursNew_Str = "0";
                                                    OTHoursAmtNew_Str = "0";
                                                    Fixed_Work_Dayss = "0";
                                                    WH_Work_Days = "0";
                                                }
                                                else if (table3.Rows.Count > 0)
                                                {
                                                    EmployeeDays = table3.Rows[0]["Days"].ToString();
                                                    Work = table3.Rows[0]["TotalDays"].ToString();
                                                    NFh = table3.Rows[0]["NFh"].ToString();
                                                    cl = table3.Rows[0]["CL"].ToString();
                                                    lopdays = table3.Rows[0]["AbsentDays"].ToString();
                                                    WeekOff = table3.Rows[0]["weekoff"].ToString();
                                                    Home = table3.Rows[0]["home"].ToString();
                                                    halfNight = table3.Rows[0]["halfNight"].ToString();
                                                    FullNight = table3.Rows[0]["FullNight"].ToString();
                                                    ThreeSided = table3.Rows[0]["ThreeSided"].ToString();
                                                    OTHoursNew_Str = table3.Rows[0]["OTHoursNew"].ToString();
                                                    Fixed_Work_Dayss = table3.Rows[0]["Fixed_Work_Days"].ToString();
                                                    WH_Work_Days = table3.Rows[0]["WH_Work_Days"].ToString();
                                                    DataTable table4 = new DataTable();
                                                    table4 = this.objdata.Attenance_ot(empNo, months, this.ddlfinance.SelectedValue, this.DepartmentCode, this.SessionCcode, this.SessionLcode, dfrom, dto);
                                                    OTDays = (table4.Rows.Count <= 0) ? "0" : table4.Rows[0]["netAmount"].ToString();
                                                }
                                                string strA = this.objdata.SalaryMonthFromLeave(empNo, this.ddlfinance.SelectedValue, this.SessionCcode, this.SessionLcode, months, dfrom, dto);
                                                string str41 = strA.Replace(" ", "");
                                                string.Compare(strA, months, true);
                                                this.objdata.SalaryPayDay();
                                                if (this.Name_Upload == "")
                                                {
                                                    str30 = (months != str41) ? "1" : "1";
                                                }
                                                if (empNo == "EMP0320181273")
                                                {
                                                    empNo = "EMP0320181273";
                                                }
                                                basicsalary = 0M;
                                                Basic_For_SM = "0";
                                                if (this.ddlcategory.SelectedValue == "1")
                                                {
                                                    AdvAmt = 0M;
                                                    str6 = "0.00";
                                                    ID = "";
                                                    basicsalary = 0M;
                                                    Basic_For_SM = "0";
                                                    DataTable table5 = new DataTable();
                                                    DataTable table17 = new DataTable();
                                                    table5 = this.objdata.NewSlary_Load(this.SessionCcode, this.SessionLcode, empNo);

                                                    DataTable table18 = new DataTable();
                                                    DataTable table6 = new DataTable();
                                                    string str42 = "";
                                                    string str43 = "";
                                                    DataTable table7 = new DataTable();
                                                    DataTable table8 = new DataTable();
                                                    DataTable table19 = new DataTable();
                                                    table8 = this.objdata.Load_pf_details(this.SessionCcode, this.SessionLcode);
                                                    table7 = this.objdata.EligibleESI_PF(this.SessionCcode, this.SessionLcode, empNo);
                                                    Convert.ToDecimal(table8.Rows[0]["StaffSalary"].ToString()).ToString();
                                                    str42 = Convert.ToDecimal(table8.Rows[0]["PF_per"].ToString()).ToString();
                                                    str43 = Convert.ToDecimal(table8.Rows[0]["ESI_Per"].ToString()).ToString();
                                                    if (table5.Rows.Count > 0)
                                                    {
                                                        str10 = "0.00";
                                                        str = "0.00";
                                                        str2 = "0.00";
                                                        str3 = "0.00";
                                                        str4 = "0.00";
                                                        str5 = "0.00";
                                                        str7 = "0.00";
                                                        stamp = "0";
                                                        HalfNight_Amt = "0.00";
                                                        FullNight_Amt = "0.00";
                                                        DayIncentive = "0.00";
                                                        SpinningAmt = "0.00";
                                                        ThreeSided_Amt = "0.00";
                                                        OTHoursAmtNew_Str = "0.00";
                                                        Basic_For_SM = "0";
                                                        Basic_For_SM = table5.Rows[0]["Base"].ToString();
                                                        if (empNo == "EMP062016102")
                                                        {
                                                            empNo = "EMP062016102";
                                                        }
                                                        basic = Math.Round(Convert.ToDecimal(Basic_For_SM), 0, MidpointRounding.AwayFromZero).ToString();
                                                        str = Math.Round(Convert.ToDecimal((Convert.ToDecimal(basic) * Convert.ToDecimal(EmployeeDays)).ToString()), 0, MidpointRounding.AwayFromZero).ToString();
                                                        str2 = Math.Round(Convert.ToDecimal(((Convert.ToDecimal(basic) / Convert.ToDecimal(8)) * Convert.ToDecimal(cl)).ToString()), 0, MidpointRounding.AwayFromZero).ToString();
                                                        ThreeSided = OTHoursNew_Str;
                                                        OTHoursNew_Str = "";
                                                        OTHoursAmtNew_Str = ((Convert.ToDecimal(Basic_For_SM) / Convert.ToDecimal(8)) * Convert.ToDecimal(ThreeSided)).ToString();
                                                        OTHoursAmtNew_Str = Math.Round(Convert.ToDecimal(OTHoursAmtNew_Str), 0, MidpointRounding.AwayFromZero).ToString();
                                                        this.SSQL = "select WorkerDays,WorkerAmt  from WorkerIncentive_mst\twhere  WorkerDays<='" + EmployeeDays + "' ";
                                                        DayIncentive = (this.objdata.RptEmployeeMultipleDetails(this.SSQL).Rows.Count == 0) ? "0" : "300";
                                                        All1 = Convert.ToDecimal(table5.Rows[0]["Alllowance1amt"].ToString()).ToString();
                                                        All1 = Math.Round(Convert.ToDecimal(All1), 2, MidpointRounding.AwayFromZero).ToString();
                                                        All2 = Convert.ToDecimal(table5.Rows[0]["Allowance2amt"].ToString()).ToString();
                                                        All2 = Math.Round(Convert.ToDecimal(All2), 2, MidpointRounding.AwayFromZero).ToString();
                                                        Ded1 = Convert.ToDecimal(table5.Rows[0]["Deduction1"].ToString()).ToString();
                                                        Ded1 = Math.Round(Convert.ToDecimal(Ded1), 2, MidpointRounding.AwayFromZero).ToString();
                                                        ded2 = (Convert.ToDecimal(EmployeeDays) * Convert.ToDecimal(table5.Rows[0]["Deduction2"].ToString())).ToString();
                                                        ded2 = Math.Round(Convert.ToDecimal(ded2), 2, MidpointRounding.AwayFromZero).ToString();
                                                        ded3 = Math.Round(Convert.ToDecimal(ded3), 2, MidpointRounding.AwayFromZero).ToString();
                                                        ded4 = Math.Round(Convert.ToDecimal(ded4), 2, MidpointRounding.AwayFromZero).ToString();
                                                        ded5 = Math.Round(Convert.ToDecimal(ded5), 2, MidpointRounding.AwayFromZero).ToString();
                                                        DedOthers1 = Math.Round(Convert.ToDecimal(DedOthers1), 2, MidpointRounding.AwayFromZero).ToString();
                                                        DedOthers2 = Math.Round(Convert.ToDecimal(DedOthers2), 2, MidpointRounding.AwayFromZero).ToString();
                                                        Mess = Math.Round(Convert.ToDecimal(Mess), 2, MidpointRounding.AwayFromZero).ToString();
                                                        str11 = Math.Round(Convert.ToDecimal(str11), 2, MidpointRounding.AwayFromZero).ToString();
                                                        if (table7.Rows[0]["EligiblePF"].ToString() != "1")
                                                        {
                                                            PFS = "0";
                                                        }
                                                        else if ((Convert.ToDecimal(EmployeeDays) <= Convert.ToDecimal((double)0.0)) && (Convert.ToDecimal(EmployeeDays) <= Convert.ToDecimal((double)0.0)))
                                                        {
                                                            PFS = "0";
                                                        }
                                                        else
                                                        {
                                                            pfAmt = ((Convert.ToDecimal(Convert.ToDecimal(str).ToString()) * Convert.ToDecimal(str42)) / 100M).ToString();
                                                            PFS = Math.Round(Convert.ToDecimal(pfAmt), 0, MidpointRounding.AwayFromZero).ToString();
                                                        }
                                                        if ((Convert.ToDecimal(EmployeeDays) <= Convert.ToDecimal((double)0.0)) && (Convert.ToDecimal(EmployeeDays) <= Convert.ToDecimal((double)0.0)))
                                                        {
                                                            ESI = "0";
                                                        }
                                                        else if (table7.Rows[0]["ElgibleESI"].ToString() != "1")
                                                        {
                                                            ESI = "0";
                                                        }
                                                        else
                                                        {
                                                            ESI = Math.Round(Convert.ToDecimal(((Convert.ToDecimal(str) / Convert.ToDecimal(str43)) / 100M).ToString()), 0, MidpointRounding.AwayFromZero).ToString();
                                                        }
                                                        TotalEarnings = ((((((((Convert.ToDecimal(str) + Convert.ToDecimal(str2)) + Convert.ToDecimal(DayIncentive)) + Convert.ToDecimal(All1)) + Convert.ToDecimal(All2)) + Convert.ToDecimal(All3)) + Convert.ToDecimal(Al4)) + Convert.ToDecimal(All5)) + Convert.ToDecimal(OTHoursAmtNew_Str)).ToString();
                                                        TotalEarnings = Math.Round(Convert.ToDecimal(TotalEarnings), 2, 0).ToString();
                                                        TotalDeductions = (((((((((Convert.ToDecimal(Advance) + Convert.ToDecimal(PFS)) + Convert.ToDecimal(ESI)) + Convert.ToDecimal(Ded1)) + Convert.ToDecimal(ded2)) + Convert.ToDecimal(ded3)) + Convert.ToDecimal(ded4)) + Convert.ToDecimal(ded5)) + Convert.ToDecimal(DedOthers1)) + Convert.ToDecimal(DedOthers2) + Convert.ToDecimal(Fine)).ToString();
                                                        TotalDeductions = Math.Round(Convert.ToDecimal(TotalDeductions), 2, 0).ToString();
                                                        NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();

                                                        NetPay = (Convert.ToDecimal(NetPay) + Convert.ToDecimal(Gift)).ToString();
                                                        NetPay = Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero).ToString();
                                                        if (Convert.ToDecimal(NetPay) < 0M)
                                                        {
                                                            NetPay = "0";
                                                        }
                                                        RoundOffNetPay = (((int)((Convert.ToDouble(NetPay.ToString()) + 5.0) / 10.0)) * 10).ToString();
                                                        Words = this.NumerictoNumber(Convert.ToInt32(RoundOffNetPay), isUK).ToString() + " Only";
                                                    }
                                                }
                                                else
                                                {
                                                    if (empNo == "EMP0320181271")
                                                    {
                                                        empNo = "EMP0320181271";
                                                    }
                                                    Basic_For_SM = "0";
                                                    str = "0.00";
                                                    str2 = "0.00";
                                                    str3 = "0.00";
                                                    FullNight_Amt = "0.00";
                                                    str4 = "0.00";
                                                    HalfNight_Amt = "0.00";
                                                    str5 = "0.00";
                                                    ESI = "0";
                                                    str6 = "0.00";
                                                    PFS = "0";
                                                    SpinningAmt = "0.00";
                                                    ThreeSided_Amt = "0.00";
                                                    stamp = "0";
                                                    AdvAmt = 0M;
                                                    ID = "";
                                                    basicsalary = 0M;
                                                    str7 = "0.00";
                                                    Basic_For_SM = "0";
                                                    DataTable table9 = new DataTable();
                                                    table9 = this.objdata.NewSlary_Load(this.SessionCcode, this.SessionLcode, empNo);
                                                    DataTable table20 = new DataTable();
                                                    DataTable table21 = new DataTable();
                                                    DataTable table22 = new DataTable();
                                                    DataTable table10 = new DataTable();

                                                    string str46 = "";
                                                    string str47 = "";
                                                    string str48 = "";

                                                    DataTable table11 = new DataTable();
                                                    DataTable table12 = new DataTable();
                                                    DataTable table23 = new DataTable();
                                                    table12 = this.objdata.Load_pf_details(this.SessionCcode, this.SessionLcode);
                                                    table11 = this.objdata.EligibleESI_PF(this.SessionCcode, this.SessionLcode, empNo);

                                                    str48 = Convert.ToDecimal(table12.Rows[0]["StaffSalary"].ToString()).ToString();
                                                    str46 = Convert.ToDecimal(table12.Rows[0]["PF_per"].ToString()).ToString();
                                                    str47 = Convert.ToDecimal(table12.Rows[0]["ESI_Per"].ToString()).ToString();

                                                    basic = Convert.ToDecimal(table9.Rows[0]["Base"].ToString()).ToString();
                                                    basicsalary = Convert.ToDecimal(table9.Rows[0]["Base"].ToString());

                                                    DataTable table13 = new DataTable();
                                                    string str49 = "";
                                                    this.SSQL = "select Incentive_Eligible from [SKS_Spay]..Employee_Mst  where MachineID ='" + ExistNo + "' ";
                                                    table13 = this.objdata.RptEmployeeMultipleDetails(this.SSQL);
                                                    if (table13.Rows.Count != 0)
                                                    {
                                                        str49 = table13.Rows[0]["Incentive_Eligible"].ToString();
                                                    }
                                                    if (ExistNo == "1273")
                                                    {
                                                        ExistNo = "1273";
                                                    }
                                                    if (str49 != "2")
                                                    {
                                                        DayIncentive = "0";
                                                    }
                                                    else
                                                    {
                                                        double num10 = 0.0;
                                                        double num11 = 0.0;
                                                        num11 = Convert.ToDouble(EmployeeDays) + Convert.ToDouble(ThreeSided);
                                                        num10 = (num11 < Convert.ToDouble(Fixed_Work_Dayss)) ? Convert.ToDouble(num11) : Convert.ToDouble(Fixed_Work_Dayss);
                                                        DayIncentive = "0";
                                                        object[] objArray = new object[] { "select WorkerDays,WorkerAmt from WorkerIncentive_mst where WorkerDays='", num10, "' and  MonthDays='", Fixed_Work_Dayss, "' " };
                                                        this.SSQL = string.Concat(objArray);
                                                        table10 = this.objdata.RptEmployeeMultipleDetails(this.SSQL);
                                                        if (table10.Rows.Count != 0)
                                                        {
                                                            DayIncentive = Convert.ToDecimal(table10.Rows[0]["WorkerAmt"].ToString()).ToString();
                                                        }
                                                        else
                                                        {
                                                            num10 = 0.0;
                                                            num11 = 0.0;
                                                            num11 = Convert.ToDouble(EmployeeDays) + Convert.ToDouble(ThreeSided);
                                                            num10 = (num11 > Convert.ToDouble(this.Fixed_Work_Days)) ? Convert.ToDouble(num11) : Convert.ToDouble(this.Fixed_Work_Days);
                                                            objArray = new object[] { "select WorkerDays,WorkerAmt from WorkerIncentive_mst where WorkerDays='", num10, "' and  MonthDays='", Fixed_Work_Dayss, "' " };
                                                            this.SSQL = string.Concat(objArray);
                                                            table10 = this.objdata.RptEmployeeMultipleDetails(this.SSQL);
                                                            DayIncentive = (table10.Rows.Count == 0) ? "0" : Convert.ToDecimal(table10.Rows[0]["WorkerAmt"].ToString()).ToString();
                                                        }
                                                    }
                                                    if (str32 == "2")
                                                    {
                                                        Basic_For_SM = "0";
                                                        str7 = "0.00";
                                                        ThreeSided_Amt = "0.00";
                                                        basic = (Convert.ToDecimal(basicsalary) + Convert.ToDecimal(DayIncentive)).ToString();
                                                        basic = Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero).ToString();
                                                        str = Math.Round(Convert.ToDecimal((Convert.ToDecimal(basic) * Convert.ToDecimal(EmployeeDays)).ToString()), 0, MidpointRounding.AwayFromZero).ToString();
                                                        str2 = Math.Round(Convert.ToDecimal(((Convert.ToDecimal(basic) / Convert.ToDecimal(8)) * Convert.ToDecimal(cl)).ToString()), 0, MidpointRounding.AwayFromZero).ToString();
                                                        OTHoursAmtNew_Str = ((Convert.ToDecimal(basic) / Convert.ToDecimal(8)) * Convert.ToDecimal(OTHoursNew_Str)).ToString();
                                                        OTHoursAmtNew_Str = Math.Round(Convert.ToDecimal(OTHoursAmtNew_Str), 0, MidpointRounding.AwayFromZero).ToString();
                                                        ThreeSided_Amt = (Convert.ToDecimal(8) * Convert.ToDecimal(ThreeSided)).ToString();
                                                        ThreeSided_Amt = Math.Round(Convert.ToDecimal(ThreeSided_Amt), 0, MidpointRounding.AwayFromZero).ToString();
                                                        SpinningAmt = (Convert.ToDecimal(basic) * Convert.ToDecimal(ThreeSided)).ToString();
                                                        SpinningAmt = Math.Round(Convert.ToDecimal(SpinningAmt), 0, MidpointRounding.AwayFromZero).ToString();
                                                    }
                                                    if (table11.Rows[0]["EligiblePF"].ToString() != "1")
                                                    {
                                                        str7 = "0.0";
                                                        str6 = "0.0";
                                                    }
                                                    else
                                                    {
                                                        DataTable table14 = new DataTable();
                                                        int num12 = 0;
                                                        this.SSQL = "select BasicDA,WashingAllow from MstBasicDet where EmployeeType='" + str32 + "'  ";
                                                        table14 = this.objdata.RptEmployeeMultipleDetails(this.SSQL);
                                                        if (table14.Rows.Count != 0)
                                                        {
                                                            num12 = Convert.ToInt32(table14.Rows[0]["WashingAllow"]);
                                                            str7 = Math.Round(Convert.ToDecimal((Convert.ToDecimal(basic) / Convert.ToDecimal(num12)).ToString()), 0, MidpointRounding.AwayFromZero).ToString();
                                                            str6 = Math.Round(Convert.ToDecimal((Convert.ToDecimal(basic) - (Convert.ToDecimal(str48) + Convert.ToDecimal(str7))).ToString()), 0, MidpointRounding.AwayFromZero).ToString();
                                                        }
                                                    }
                                                    if (table11.Rows[0]["EligiblePF"].ToString() != "1")
                                                    {
                                                        PFS = "0";
                                                    }
                                                    else if ((Convert.ToDecimal(EmployeeDays) <= Convert.ToDecimal((double)0.0)) && (Convert.ToDecimal(EmployeeDays) <= Convert.ToDecimal((double)0.0)))
                                                    {
                                                        PFS = "0";
                                                    }
                                                    else if (Convert.ToDecimal(EmployeeDays) >= Convert.ToDecimal(0x1a))
                                                    {
                                                        pfAmt = ((Convert.ToDecimal((Convert.ToDecimal(str48) * Convert.ToDecimal(0x1a)).ToString()) * Convert.ToDecimal(str46)) / 100M).ToString();
                                                        PFS = Math.Round(Convert.ToDecimal(pfAmt), 0, MidpointRounding.AwayFromZero).ToString();
                                                    }
                                                    else
                                                    {
                                                        pfAmt = ((Convert.ToDecimal((Convert.ToDecimal(str48) * Convert.ToDecimal(EmployeeDays)).ToString()) * Convert.ToDecimal(str46)) / 100M).ToString();
                                                        PFS = Math.Round(Convert.ToDecimal(pfAmt), 0, MidpointRounding.AwayFromZero).ToString();
                                                    }
                                                    if ((Convert.ToDecimal(EmployeeDays) <= Convert.ToDecimal((double)0.0)) && (Convert.ToDecimal(EmployeeDays) <= Convert.ToDecimal((double)0.0)))
                                                    {
                                                        ESI = "0";
                                                    }
                                                    else if (table11.Rows[0]["ElgibleESI"].ToString() != "1")
                                                    {
                                                        ESI = "0";
                                                    }
                                                    else if (Convert.ToDecimal(EmployeeDays) >= Convert.ToDecimal(0x1a))
                                                    {
                                                        ESI = ((Convert.ToDecimal(((Convert.ToDecimal(str48) + Convert.ToDecimal(str6)) * Convert.ToDecimal(0x1a)).ToString()) * Convert.ToDecimal(str47)) / 100M).ToString();
                                                        ESI = Math.Round(Convert.ToDecimal(ESI), 0, MidpointRounding.AwayFromZero).ToString();
                                                    }
                                                    else
                                                    {
                                                        ESI = ((Convert.ToDecimal(((Convert.ToDecimal(str48) + Convert.ToDecimal(str6)) * Convert.ToDecimal(EmployeeDays)).ToString()) * Convert.ToDecimal(str47)) / 100M).ToString();
                                                        ESI = Math.Round(Convert.ToDecimal(ESI), 0, MidpointRounding.AwayFromZero).ToString();
                                                    }
                                                    All1 = Convert.ToDecimal(table9.Rows[0]["Alllowance1amt"].ToString()).ToString();
                                                    All1 = Math.Round(Convert.ToDecimal(All1), 2, MidpointRounding.AwayFromZero).ToString();
                                                    All2 = Convert.ToDecimal(table9.Rows[0]["Allowance2amt"].ToString()).ToString();
                                                    All2 = Math.Round(Convert.ToDecimal(All2), 2, MidpointRounding.AwayFromZero).ToString();
                                                    Ded1 = Convert.ToDecimal(table9.Rows[0]["Deduction1"].ToString()).ToString();
                                                    Ded1 = Math.Round(Convert.ToDecimal(Ded1), 2, MidpointRounding.AwayFromZero).ToString();
                                                    ded2 = (Convert.ToDecimal(EmployeeDays) * Convert.ToDecimal(table9.Rows[0]["Deduction2"].ToString())).ToString();
                                                    ded2 = Math.Round(Convert.ToDecimal(ded2), 2, MidpointRounding.AwayFromZero).ToString();
                                                    ded3 = Math.Round(Convert.ToDecimal(ded3), 2, MidpointRounding.AwayFromZero).ToString();
                                                    ded4 = Math.Round(Convert.ToDecimal(ded4), 2, MidpointRounding.AwayFromZero).ToString();
                                                    ded5 = Math.Round(Convert.ToDecimal(ded5), 2, MidpointRounding.AwayFromZero).ToString();
                                                    DedOthers1 = Math.Round(Convert.ToDecimal(DedOthers1), 2, MidpointRounding.AwayFromZero).ToString();
                                                    DedOthers2 = Math.Round(Convert.ToDecimal(DedOthers2), 2, MidpointRounding.AwayFromZero).ToString();
                                                    Mess = Math.Round(Convert.ToDecimal(Mess), 2, MidpointRounding.AwayFromZero).ToString();
                                                    str11 = Math.Round(Convert.ToDecimal(str11), 2, MidpointRounding.AwayFromZero).ToString();
                                                    TotalEarnings = (((((((((Convert.ToDecimal(str) + Convert.ToDecimal(str2)) + Convert.ToDecimal(All1)) + Convert.ToDecimal(All2)) + Convert.ToDecimal(All3)) + Convert.ToDecimal(Al4)) + Convert.ToDecimal(All5)) + Convert.ToDecimal(OTHoursAmtNew_Str)) + Convert.ToDecimal(SpinningAmt)) + Convert.ToDecimal(ThreeSided_Amt)).ToString();
                                                    TotalEarnings = Math.Round(Convert.ToDecimal(TotalEarnings), 2, 0).ToString();
                                                    TotalDeductions = (((((((((((Convert.ToDecimal(Advance) + Convert.ToDecimal(PFS)) + Convert.ToDecimal(ESI)) + Convert.ToDecimal(Ded1)) + Convert.ToDecimal(ded2)) + Convert.ToDecimal(ded3)) + Convert.ToDecimal(ded4)) + Convert.ToDecimal(ded5)) + Convert.ToDecimal(DedOthers1)) + Convert.ToDecimal(DedOthers2)) + Convert.ToDecimal(Mess)) + Convert.ToDecimal(str11) + Convert.ToDecimal(Fine)).ToString();
                                                    TotalDeductions = Math.Round(Convert.ToDecimal(TotalDeductions), 2, 0).ToString();
                                                    NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();

                                                    NetPay = (Convert.ToDecimal(NetPay) + Convert.ToDecimal(Gift)).ToString();
                                                    NetPay = Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero).ToString();
                                                    if (Convert.ToDecimal(NetPay) < 0M)
                                                    {
                                                        NetPay = "0";
                                                    }
                                                    RoundOffNetPay = (((int)((Convert.ToDouble(NetPay.ToString()) + 5.0) / 10.0)) * 10).ToString();
                                                    Words = this.NumerictoNumber(Convert.ToInt32(RoundOffNetPay), isUK).ToString() + " Only";
                                                }
                                                if (!flag3)
                                                {
                                                    string str55 = this.objdata.PF_Eligible_Salary(empNo, this.SessionCcode, this.SessionLcode);
                                                    this.objSal.TotalWorkingDays = Work;
                                                    this.objSal.NFh = NFh;
                                                    this.objSal.Lcode = this.SessionLcode;
                                                    this.objSal.Ccode = this.SessionCcode;
                                                    this.objSal.ApplyLeaveDays = lopdays;
                                                    this.objSal.GrossEarnings = TotalEarnings;
                                                    this.objSal.TotalDeduction = TotalDeductions;
                                                    this.objSal.NetPay = NetPay;
                                                    this.objSal.RoundOffNetPay = RoundOffNetPay;
                                                    this.objSal.Words = Words;
                                                    this.objSal.AvailableDays = EmployeeDays;
                                                    this.objSal.SalaryDate = this.txtsalaryday.Text;
                                                    this.objSal.work = EmployeeDays;
                                                    this.objSal.weekoff = WeekOff;
                                                    this.objSal.CL = cl;
                                                    this.objSal.FixedBase = basic;
                                                    this.objSal.FixedFDA = "0.00";
                                                    this.objSal.FixedFRA = "0.00";
                                                    this.objSal.HomeTown = ThreeSided;
                                                    this.objSal.HalfNightAmt = HalfNight_Amt;
                                                    this.objSal.FullNightAmt = FullNight_Amt;
                                                    this.objSal.ThreesidedAmt = ThreeSided_Amt;
                                                    this.objSal.SpinningAmt = SpinningAmt;
                                                    this.objSal.DayIncentive = DayIncentive;
                                                    DateTime.ParseExact(this.objSal.SalaryDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                                    this.objSal.BasicAndDA = str;
                                                    this.objSal.HRA = str11;
                                                    this.objSal.FDA = str9;
                                                    this.objSal.VDA = str8;
                                                    this.objSal.Allowances1 = All1;
                                                    this.objSal.Allowances2 = All2;
                                                    this.objSal.Allowances3 = All3;
                                                    this.objSal.Allowances4 = Al4;
                                                    this.objSal.Allowances5 = All5;
                                                    this.objSal.Deduction1 = Ded1;
                                                    this.objSal.Deduction2 = ded2;
                                                    this.objSal.Deduction3 = ded3;
                                                    this.objSal.Deduction4 = ded4;
                                                    this.objSal.Deduction5 = ded5;
                                                    this.objSal.ProvidentFund = PFS;
                                                    this.objSal.ESI = ESI;
                                                    this.objSal.Stamp = str10;
                                                    this.objSal.Advance = Advance;
                                                    this.objSal.LossofPay = lop;
                                                    this.objSal.OT = OTDays;
                                                    this.objSal.LOPDays = lopdays;
                                                    this.objSal.BasicDA = str;
                                                    this.objSal.Basic_HRA = str2;
                                                    this.objSal.Conv_Allow = str3;
                                                    this.objSal.Edu_Allow = str4;
                                                    this.objSal.Medi_Allow = str5;
                                                    this.objSal.Basic_RAI = str6;
                                                    this.objSal.Washing_Allow = str7;
                                                    this.objSal.MessDeduction = Mess;
                                                    if (str55 == "1")
                                                    {
                                                        this.objSal.PfSalary = PFS;
                                                        this.objSal.Emp_PF = PFS;
                                                    }
                                                    else
                                                    {
                                                        this.objSal.PfSalary = "0.00";
                                                        this.objSal.Emp_PF = "0.00";
                                                    }
                                                    this.objSal.OTHoursAmt = OTHoursAmtNew_Str;
                                                    this.objSal.OTHoursNew = OTHoursNew_Str;
                                                    this.objSal.Leave_Credit_Check = "0";
                                                    this.objSal.Fixed_Work_Days = Fixed_Work_Dayss;
                                                    this.objSal.WH_Work_Days = WH_Work_Days;
                                                    this.objSal.Ded_Others1 = DedOthers1;
                                                    this.objSal.Ded_Others2 = DedOthers2;
                                                    this.objSal.EmployeerPFone = EmployeerPFOne;
                                                    this.objSal.EmployeerPFTwo = EmployeerPFTwo;
                                                    this.objSal.EmployeerESI = EmployeerESI;
                                                    this.objSal.Leave_Credit_Days = "0";
                                                    this.objSal.Leave_Credit_Add = "0";
                                                    this.objSal.Leave_Credit_Type = "0";

                                                    //Fine and Gift by Selva
                                                    this.objSal.Fine = Fine;
                                                    this.objSal.Gift = Gift;

                                                    DataTable table15 = new DataTable();
                                                    strArray = new string[] { "Select * from SalaryDetails where EmpNo='", empNo, "' and Month='", this.ddlMonths.Text, "' and FinancialYear='", this.ddlfinance.SelectedValue, "' and Ccode='", this.SessionCcode, "' and Lcode='", this.SessionLcode, "' and convert(datetime,FromDate,105) = convert(datetime,'", this.txtFrom.Text, "',105) and convert(datetime, ToDate, 105) = convert(datetime, '", this.txtTo.Text, "', 105) And Salary_Conform='1'" };
                                                    //strArray[9] = this.SessionLcode;
                                                    //strArray[10] = "' and convert(datetime,FromDate,105) = convert(datetime,'";
                                                    //strArray[11] = this.txtFrom.Text;
                                                    //strArray[12] = "',105) and convert(datetime, ToDate, 105) = convert(datetime, '";
                                                    //strArray[13] = this.txtTo.Text;
                                                    //strArray[14] = "', 105) And Salary_Conform='1'";
                                                    string str56 = string.Concat(strArray);
                                                    flag2 = this.objdata.RptEmployeeMultipleDetails(str56).Rows.Count == 0;
                                                }
                                                if (flag2)
                                                {
                                                    if (str30 == "0")
                                                    {
                                                        strArray = new string[] { "Delete from SalaryDetails where EmpNo='", empNo, "' and Month='", this.ddlMonths.Text, "' and FinancialYear='", this.ddlfinance.SelectedValue, "' and Ccode='", this.SessionCcode, "' and Lcode='", this.SessionLcode, "' and convert(datetime,FromDate,105) = convert(datetime,'", this.txtFrom.Text, "',105) and convert(datetime, ToDate, 105) = convert(datetime, '", this.txtTo.Text, "', 105)" };
                                                        //strArray[9] = this.SessionLcode;
                                                        //strArray[10] = "' and convert(datetime,FromDate,105) = convert(datetime,'";
                                                        //strArray[11] = this.txtFrom.Text;
                                                        //strArray[12] = "',105) and convert(datetime, ToDate, 105) = convert(datetime, '";
                                                        //strArray[13] = this.txtTo.Text;
                                                        //strArray[14] = "', 105)";
                                                        new SqlCommand(string.Concat(strArray), connection3).ExecuteNonQuery();
                                                    }
                                                    else if (str30 == "1")
                                                    {
                                                        strArray = new string[] { "Delete from SalaryDetails where EmpNo='", empNo, "' and Month='", this.ddlMonths.Text, "' and FinancialYear='", this.ddlfinance.SelectedValue, "' and Ccode='", this.SessionCcode, "' and Lcode='", this.SessionLcode, "' and convert(datetime,FromDate,105) = convert(datetime,'", this.txtFrom.Text, "',105) and convert(datetime, ToDate, 105) = convert(datetime, '", this.txtTo.Text, "', 105)" };
                                                        //strArray[9] = this.SessionLcode;
                                                        //strArray[10] = "' and convert(datetime,FromDate,105) = convert(datetime,'";
                                                        //strArray[11] = this.txtFrom.Text;
                                                        //strArray[12] = "',105) and convert(datetime, ToDate, 105) = convert(datetime, '";
                                                        //strArray[13] = this.txtTo.Text;
                                                        //strArray[14] = "', 105)";
                                                        new SqlCommand(string.Concat(strArray), connection3).ExecuteNonQuery();
                                                        this.MyDate = DateTime.ParseExact(this.objSal.SalaryDate, "dd-MM-yyyy", null);
                                                        this.TempDate = Convert.ToDateTime(this.MyDate).AddMonths(0).ToShortDateString();
                                                        this.TransDate = DateTime.ParseExact(this.TempDate, "dd/MM/yyyy", null);
                                                        this.objSal.BasicforSM = Basic_For_SM;
                                                        this.objdata.SalaryDetails(this.objSal, empNo, ExistNo, this.MyDate, this.MyMonth, this.Year, this.ddlfinance.SelectedValue, this.TransDate, dfrom, dto, str33, wagesType);
                                                        string str59 = this.objdata.Contract_GetEmpDet(empNo, this.SessionCcode, this.SessionLcode);
                                                        if ((str59.Trim() != "") && (Convert.ToDecimal(str59) > 0M))
                                                        {
                                                            DataTable table16 = new DataTable();
                                                            table16 = this.objdata.Contract_getPlanDetails(empNo, this.SessionCcode, this.SessionLcode);
                                                            if (table16.Rows.Count > 0)
                                                            {
                                                                if (table16.Rows[0]["ContractType"].ToString() == "1")
                                                                {
                                                                    val = Convert.ToDecimal(EmployeeDays);
                                                                    str59 = (Convert.ToDecimal(str59) - val).ToString();
                                                                }
                                                                else if (table16.Rows[0]["ContractType"].ToString() != "2")
                                                                {
                                                                    if (table16.Rows[0]["ContractType"].ToString() == "3")
                                                                    {
                                                                        val = Convert.ToDecimal(EmployeeDays);
                                                                        str59 = (Convert.ToDecimal(str59) - val).ToString();
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    val = Convert.ToDecimal(EmployeeDays);
                                                                    if (val < Convert.ToDecimal(table16.Rows[0]["FixedDays"].ToString()))
                                                                    {
                                                                        (Convert.ToInt32(table16.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                                                                    }
                                                                    else
                                                                    {
                                                                        (Convert.ToInt32(table16.Rows[0]["DecMonth"].ToString()) - 1).ToString();
                                                                        str59 = (Convert.ToDecimal(str59) - val).ToString();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Finished Success Fully');", true);
                                                connection3.Close();
                                                connection.Close();
                                            }
                                        }
                                        break;
                                    }
                                    SqlConnection connection2 = new SqlConnection(str13);
                                    string str14 = table.Rows[num4][0].ToString();
                                    string str15 = table.Rows[num4][1].ToString();
                                    string str16 = table.Rows[num4][2].ToString();
                                    string str17 = table.Rows[num4][3].ToString();
                                    string str18 = table.Rows[num4][4].ToString();
                                    string str19 = table.Rows[num4][5].ToString();
                                    string str20 = table.Rows[num4][6].ToString();
                                    string str21 = table.Rows[num4][7].ToString();
                                    string str22 = table.Rows[num4][8].ToString();
                                    string str23 = table.Rows[num4][9].ToString();
                                    string str24 = table.Rows[num4][10].ToString();
                                    string str25 = "";
                                    DataTable table2 = new DataTable();
                                    strArray = new string[] { "Select * from EmployeeDetails where Ccode='", this.SessionCcode, "' And Lcode='", this.SessionLcode, "' and EmpNo='", str15, "'" };
                                    str25 = string.Concat(strArray);
                                    if (this.objdata.RptEmployeeMultipleDetails(str25).Rows.Count == 0)
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Check the Employee Details in Employee Details Page. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    strArray = new string[] { "Select * from OfficialProfile where Ccode='", this.SessionCcode, "' And Lcode='", this.SessionLcode, "' and EmpNo='", str15, "'" };
                                    str25 = string.Concat(strArray);
                                    if (this.objdata.RptEmployeeMultipleDetails(str25).Rows.Count == 0)
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Check the Employee Details in Profile Page. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    strArray = new string[] { "Select * from SalaryMaster where Ccode='", this.SessionCcode, "' And Lcode='", this.SessionLcode, "' and EmpNo='", str15, "'" };
                                    str25 = string.Concat(strArray);
                                    if (this.objdata.RptEmployeeMultipleDetails(str25).Rows.Count == 0)
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Check the Employee Wages in Salary Master Page. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    strArray = new string[] { "Select * from AttenanceDetails where Ccode='", this.SessionCcode, "' And Lcode='", this.SessionLcode, "' and EmpNo='", str15, "' and Months='", str12, "' and FinancialYear='", this.ddlfinance.SelectedValue,"'" };
                                  
                                    str25 = string.Concat(strArray);
                                    if (this.objdata.RptEmployeeMultipleDetails(str25).Rows.Count == 0)
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Attenance Not Upload This Employee Check Attenance Upload Excel File. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    strArray = new string[] { "Select * from MstESIPF where Ccode='", this.SessionCcode, "' And Lcode='", this.SessionLcode, "'" };
                                    str25 = string.Concat(strArray);
                                    if (this.objdata.RptEmployeeMultipleDetails(str25).Rows.Count == 0)
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('You have enter PF And ESI Percentage in PF Master Page. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    if (str14 == "")
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Department. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    else if (str15 == "")
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Employee Number. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    else if (str16 == "")
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Existing Code. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    else if (str17 == "")
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Name. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    else if (str18 == "")
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Allowance 3s. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    else if (str19 == "")
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Allowance 4. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    else if (str20 == "")
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Allowance 5. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    else if (str21 == "")
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Deduction 3. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    else if (str22 == "")
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Deduction 4. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    else if (str23 == "")
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Deduction 5. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    else if (str24 == "")
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Advance. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    connection2.Open();
                                    if (!new SqlCommand("Select DepartmentNm from MstDepartment where DepartmentNm = '" + str14 + "'", connection2).ExecuteReader().HasRows)
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    connection2.Close();
                                    connection2.Open();
                                    strArray = new string[] { "Select ED.Department from EmployeeDetails ED inner Join MstDepartment MSt on Mst.DepartmentCd=ED.Department where DepartmentNm = '", str14, "' and ED.EmpNo='", str15, "'" };
                                    if (!new SqlCommand(string.Concat(strArray), connection2).ExecuteReader().HasRows)
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    connection2.Close();
                                    connection2.Open();
                                    if (!new SqlCommand("Select EmpNo from Officialprofile where WagesType ='" + this.rbsalary.SelectedValue + "'", connection2).ExecuteReader().HasRows)
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('This " + str15 + " is Wages Type is incorrect.');", true);
                                        flag = true;
                                    }
                                    connection2.Close();
                                    connection2.Open();
                                    strArray = new string[] { "Select EmpNo from EmployeeDetails where EmpNo= '", str15, "' and Ccode='", this.SessionCcode, "' and Lcode='", this.SessionLcode, "'" };
                                    if (!new SqlCommand(string.Concat(strArray), connection2).ExecuteReader().HasRows)
                                    {
                                        num4 += 2;
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Employee Details Properly. The Row Number is " + num4 + "');", true);
                                        flag = true;
                                    }
                                    connection2.Close();
                                    num4++;
                                }
                                break;
                            }
                            table.Columns[num3].ColumnName = table.Columns[num3].ColumnName.Replace(" ", string.Empty).ToString();
                            num3++;
                        }
                    }
                }
            }
        //}
        //catch (Exception ex)
        //{
        //    ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
        //}
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
