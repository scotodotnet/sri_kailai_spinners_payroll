﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Drawing;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data;

public partial class EmployeeRegistration2 : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    BALDataAccess objdata = new BALDataAccess();
    string stafflabor;
    string NonPFGrade;
    DataTable dttaluk = new DataTable();
    static string UserID = "", SesRoleCode = "", sessionAdmin = "";
    string stafflabour_temp;
    bool textSearch = false;
    string SessionCcode;
    string SessionLcode;
    string Hostel;
    string MachineNo;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        UserID = Session["UserId"].ToString();
        SesRoleCode = Session["RoleCode"].ToString();
        sessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        //MachineNo = Session["MachineNo"].ToString();
      
        if (sessionAdmin == "1")
        {
            lblnonpfgrade.Visible = true;
            chkboxnonpfgrade.Visible = true;
        }
        else
        {
            lblnonpfgrade.Visible = false;
            chkboxnonpfgrade.Visible = false;
        }
        
            
       
        if (!IsPostBack)
        {

           


            Enable_False();
            District();
            //EmployeeType();
            Qualification();
            Department();
            //LabourType();
            DropdownProbationPeriod();
            DropDwonCategory();
            Dropdowncontract();
            string LeaveconutMsg = objdata.LeavecountMessage();
            //EmpPhoto.ImageUrl = ("~/EmployeeImages/NoImage.gif");
            //educate_RBtn();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert(' " + LeaveconutMsg + " - Staff Apply For Leave ');", true);
            //panelLeave.Visible = true;
            //lblLeave.Text = LeaveconutMsg + " Staffs - " + "Apply For Leave";

            if (MachineNo != "")
            {
                txtEmpNo.Text = MachineNo;
                DisplayEmployeeDetails();
                btnsave.Text = "Update";
            }
            else
            {
                btnsave.Text = "Save";
            }


        }
    }

    public void DropdownProbationPeriod()
    {
        DataTable dtProbation = new DataTable();
        dtProbation = objdata.DropDownProbationPriod();
        //ddlprobationpd.DataSource = dtProbation;
        //ddlprobationpd.DataTextField = "ProbationMonth";
        //ddlprobationpd.DataValueField = "ProbationCd";
        //ddlprobationpd.DataBind();
    }

    public void District()
    {


        DataTable dt1 = new DataTable();
        dt1 = objdata.DropDownDistrict();
        //ddlPSDistrict.DataSource = dt1;
        //ddlPSDistrict.DataTextField = "DistrictNm";
        //ddlPSDistrict.DataValueField = "DistrictCd";
        //ddlPSDistrict.DataBind();
    }

    public void Qualification()
    {
        DataTable dtQuali = new DataTable();
        dtQuali = objdata.Qualification();
        ddlqualification.DataSource = dtQuali;
        ddlqualification.DataTextField = "QualificationNm";
        ddlqualification.DataValueField = "QualificationCd";
        ddlqualification.DataBind();

    }

    public void Department()
    {
        DataTable dtDip = new DataTable();
        dtDip = objdata.DDdEPARTMENT_LOAD();
        ddldepartment.DataSource = dtDip;
        ddldepartment.DataTextField = "DepartmentNm";
        ddldepartment.DataValueField = "DepartmentCd";
        ddldepartment.DataBind();
    }

    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }

    public void Dropdowncontract()
    {
        DataTable dt_empty = new DataTable();
        ddlContract.DataSource = dt_empty;
        ddlContract.DataBind();
        DataTable dt = new DataTable();
        dt = objdata.DropDown_contract();
        ddlContract.DataSource = dt;
        ddlContract.DataTextField = "ContractName";
        ddlContract.DataValueField = "ContractCode";
        ddlContract.DataBind();
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Enable_False();
      
    }
    protected void rbtngender_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    public void EmployeeType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        ddemployeetype.DataSource = dtemp;
        ddemployeetype.DataTextField = "EmpType";
        ddemployeetype.DataValueField = "EmpTypeCd";
        ddemployeetype.DataBind();
    }
    protected void rbtneducateduneducated_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtneducateduneducated.SelectedValue == "1")
        {
            lblqualif.Visible = true;
            ddlqualification.Visible = true;
            lblunive.Visible = true;
            txtuniversity.Visible = true;


        }
        else if (rbtneducateduneducated.SelectedValue == "2")
        {
            lblqualif.Visible = false;
            ddlqualification.Visible = false;
            lblunive.Visible = false;
            txtuniversity.Visible = false;

        }
    }

    public void Enable_False()
    {

        lblPrsnDet.Visible = false;
        lblempname.Visible = false;
        txtempname.Visible = false;
        lblempname.Visible = false;
        lblempfname.Visible = false;
        txtempfname.Visible = false;
        lblgender.Visible = false;
        rbtngender.Visible = false;
        lblDOB.Visible = false;
        txtdob.Visible = false;
        lblMartial.Visible = false;
        Rbmartial.Visible = false;
        lblinitial.Visible = false;
        Rbfather.Visible = false;
        lblPSadd1.Visible = false;
        txtPSAdd1.Visible = false;
        lblDist.Visible = false;
        txtDistrict.Visible = false;
        lblPSAdd2.Visible = false;
        txtPSAdd2.Visible = false;
        lblTaluk.Visible = false;
        txtTaluk.Visible = false;
        lblState.Visible = false;
        txtpsstate.Visible = false;
        lblPhn.Visible = false;
        txtphone.Visible = false;
        txtphone1.Visible = false;
        lblMob.Visible = false;
        txtMobileCode.Visible = false;
        txtmobile.Visible = false;
        lblppno.Visible = false;
        txtppno.Visible = false;
        lblDirNo.Visible = false;
        txtdrivingno.Visible = false;
        lblEdu.Visible = false;
        rbtneducateduneducated.Visible = false;
        lblqualif.Visible = false;
        ddlqualification.Visible = false;
        lblunive.Visible = false;
        txtuniversity.Visible = false;
        lblCmpDet.Visible = false;
        lblDept.Visible = false;
        ddldepartment.Visible = false;
        lblDesgn.Visible = false;
        txtdesignation.Visible = false;
        lblemptype.Visible = false;
        ddemployeetype.Visible = false;
        lblexisiting.Visible = false;
        txtexistiongno.Visible = false;
        lblMachID.Visible = false;
        txtOld.Visible = false;
        lblContract.Visible = false;
        ddlContract.Visible = false;
        lblnonpfgrade.Visible = false;
        chkboxnonpfgrade.Visible = false;
        lblHostel.Visible = false;
        chkHostel.Visible = false;
        btnsave.Visible = false;
        btnclear.Visible = false;

    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
        EmployeeType();

        if (ddlcategory.SelectedItem.Text == "----Select----")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Any Category');", true);

        }
        else if (ddlcategory.SelectedItem.Text == "Staff")
        {
            lblEmpNo.Visible = false;
            txtEmpNo.Visible = false;
            lblPrsnDet.Visible = true;
            lblempname.Visible = true;
            txtempname.Visible = true;
            lblempname.Visible = true;
            lblempfname.Visible = true;
            txtempfname.Visible = true;
            lblgender.Visible = true;
            rbtngender.Visible = true;
            lblDOB.Visible = true;
            txtdob.Visible = true;
            lblMartial.Visible = true;
            Rbmartial.Visible = true;
            lblinitial.Visible = true;
            Rbfather.Visible = true;
            lblPSadd1.Visible = true;
            txtPSAdd1.Visible = true;
            lblDist.Visible = true;
            txtDistrict.Visible = true;
            lblPSAdd2.Visible = true;
            txtPSAdd2.Visible = true;
            lblTaluk.Visible = true;
            txtTaluk.Visible = true;
            lblState.Visible = true;
            txtpsstate.Visible = true;
            lblPhn.Visible = true;
            txtphone.Visible = true;
            txtphone1.Visible = true;
            lblMob.Visible = true;
            txtMobileCode.Visible = true;
            txtmobile.Visible = true;
            lblppno.Visible = true;
            txtppno.Visible = true;
            lblDirNo.Visible = true;
            txtdrivingno.Visible = true;
            lblEdu.Visible = true;
            rbtneducateduneducated.Visible = false;
            //rbtneducated.Visible = false;
            //rbtnuneducated.Visible = false;
            //RadioButton1.Visible = false;
            //RadioButton2.Visible = false;
            lblqualif.Visible = true;
            ddlqualification.Visible = true;
            lblunive.Visible = true;
            txtuniversity.Visible = true;
            lblCmpDet.Visible = true;
            lblDept.Visible = true;
            ddldepartment.Visible = true;
            lblDesgn.Visible = true;
            txtdesignation.Visible = true;
            lblemptype.Visible = true;
            ddemployeetype.Visible = true;
            lblexisiting.Visible = true;
            txtexistiongno.Visible = true;
            lblMachID.Visible = true;
            txtOld.Visible = true;
            lblContract.Visible = false;
            ddlContract.Visible = false;
            lblnonpfgrade.Visible = true;
            chkboxnonpfgrade.Visible = true;
            lblHostel.Visible = true;
            chkHostel.Visible = true;
            btnsave.Visible = true;
            btnclear.Visible = true;
        }
        else if (ddlcategory.SelectedItem.Text == "Labour")
        {
            lblEmpNo.Visible = false;
            txtEmpNo.Visible = false;
            lblPrsnDet.Visible = true;
            lblempname.Visible = true;
            txtempname.Visible = true;
            lblempname.Visible = true;
            lblempfname.Visible = true;
            txtempfname.Visible = true;
            lblgender.Visible = true;
            rbtngender.Visible = true;
            lblDOB.Visible = true;
            txtdob.Visible = true;
            lblMartial.Visible = true;
            Rbmartial.Visible = true;
            lblinitial.Visible = true;
            Rbfather.Visible = true;
            lblPSadd1.Visible = true;
            txtPSAdd1.Visible = true;
            lblDist.Visible = true;
            txtDistrict.Visible = true;
            lblPSAdd2.Visible = true;
            txtPSAdd2.Visible = true;
            lblTaluk.Visible = true;
            txtTaluk.Visible = true;
            lblState.Visible = true;
            txtpsstate.Visible = true;
            lblPhn.Visible = true;
            txtphone.Visible = true;
            txtphone1.Visible = true;
            lblMob.Visible = true;
            txtMobileCode.Visible = true;
            txtmobile.Visible = true;
            lblppno.Visible = true;
            txtppno.Visible = true;
            lblDirNo.Visible = true;
            txtdrivingno.Visible = true;
            lblEdu.Visible = true;
            rbtneducateduneducated.Visible = true;
            //rbtneducated.Visible = true;
            //rbtnuneducated.Visible = true;
            //RadioButton1.Visible = true;
            //RadioButton2.Visible = true;
            lblqualif.Visible = false;
            ddlqualification.Visible = false;
            lblunive.Visible = false;
            txtuniversity.Visible = false;
            lblCmpDet.Visible = true;
            lblDept.Visible = true;
            ddldepartment.Visible = true;
            lblDesgn.Visible = true;
            txtdesignation.Visible = true;
            lblemptype.Visible = true;
            ddemployeetype.Visible = true;
            lblexisiting.Visible = true;
            txtexistiongno.Visible = true;
            lblMachID.Visible = true;
            txtOld.Visible = true;
            lblContract.Visible = false;
            ddlContract.Visible = false;
            lblnonpfgrade.Visible = true;
            chkboxnonpfgrade.Visible = true;
            lblHostel.Visible = true;
            chkHostel.Visible = true;
            btnsave.Visible = true;
            btnclear.Visible = true;
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {


        try
        {
            string StafforLavour = "";
            string EmployeeType = "";
            string Dept = "";
            string Qulification = "";
            string ExistingCode = "";
            string Gender = "";

            DataTable dt = new DataTable();
            DataTable dtDept = new DataTable();
            DataTable dtQulification=new DataTable();
            DataTable dtGender = new DataTable();
            DataTable dtInsert = new DataTable();
            DataTable dtDel = new DataTable();
            string Query = "";

            Query = "Select * from [SKS_Spay]..Employee_Mst where  Wages='LABOUR' order by EmpNo asc";
                dt = objdata.RptEmployeeMultipleDetails(Query);

            for (int i = 0; i <= dt.Rows.Count-1; i++)
            {
                string NumberVal = dt.Rows[i]["EmpNo"].ToString().Trim();
                if (NumberVal == "6035")
                {
                    NumberVal = "6035";
                }
               
                //GetDept 
                string MonthYear = System.DateTime.Now.ToString("MMyyyy");
                string GetDate=System.DateTime.Now.ToString("dd/MM/yyyy");
                ExistingCode = "EMP" + MonthYear + dt.Rows[i]["EmpNo"].ToString().Trim();

                Query = "Select DepartmentCd from [SKS_Epay]..MstDepartment where DepartmentNm='" + dt.Rows[i]["DeptName"].ToString().ToUpper().Trim() + "'";
                dtDept = objdata.RptEmployeeMultipleDetails(Query);

                if (dtDept.Rows.Count > 0) { Dept = dtDept.Rows[0]["DepartmentCd"].ToString(); } else { Dept = "0"; }

                Query = "select QualificationCd from [SKS_Epay]..MstQualification where QualificationNm='" + dt.Rows[i]["Qualification"].ToString().ToUpper().Trim() + "'";
                dtQulification = objdata.RptEmployeeMultipleDetails(Query);
                if (dtQulification.Rows.Count > 0) { Qulification = dtQulification.Rows[0]["QualificationCd"].ToString(); } else { Qulification = "0"; }

                if (dt.Rows[i]["Gender"].ToString().ToUpper().Trim() == "FEMALE")
                { Gender = "2"; }
                else if (dt.Rows[i]["Gender"].ToString().ToUpper().Trim() == "MALE")
                { Gender = "1"; }

                //Delete
                Query = "Delete from [SKS_Epay]..EmployeeDetails where ExisistingCode='" + dt.Rows[i]["EmpNo"].ToString().Trim() + "'";
                dtDel = objdata.RptEmployeeMultipleDetails(Query);


                //Insert Table 
                Query = "Insert into [SKS_Epay]..EmployeeDetails(EmpNo,MachineNo,ExisistingCode,EmpName,FatherName,Gender,DOB,PSAdd1,PSAdd2,PSDistrict,PSTaluk,PSState, ";
                Query = Query + "Phone,Mobile,Qualification,Department,Designation,EmployeeType,CreatedDate,IsValid,StafforLabor,IsLeave,ActivateMode,Status,RoleCode,Ccode,Lcode,MartialStatus,Initial,ContractType,OldID,BiometricID) Values( ";
                Query = Query + "'" + ExistingCode + "','" + ExistingCode + "','" + dt.Rows[i]["EmpNo"].ToString().Trim() + "','" + dt.Rows[i]["FirstName"].ToString().Trim() + "','" + dt.Rows[i]["LastName"].ToString().Trim() + "','" + Gender + "',convert(datetime,'" + dt.Rows[i]["BirthDate"].ToString().Trim() + "',103),'" + dt.Rows[i]["Address1"].ToString().Trim() + "','" + dt.Rows[i]["Address1"].ToString().Trim() + "','Tirupur','Kunnathur','TamilNadu', ";
                Query = Query + "'-','" + dt.Rows[i]["EmployeeMobile"].ToString().Trim() + "','" + Qulification + "','" + Dept + "','" + dt.Rows[i]["Designation"].ToString().Trim() + "','2',convert(datetime,'" + GetDate + "',103),'Y','S','N','Y','R','1','SKS','UNIT I','U','" + dt.Rows[i]["MiddleInitial"].ToString().Trim() + "','0','" + dt.Rows[i]["EmpNo"].ToString().Trim() + "','" + dt.Rows[i]["EmpNo"].ToString().Trim() + "')";
                dtInsert = objdata.RptEmployeeMultipleDetails(Query);


            }

            //string EmpNo = "";
            //string EmNm = "";
            //DateTime MyDateTime;
            //MyDateTime = new DateTime();
            ////lblError.Text = "";
            //EmployeeClass objEmp = new EmployeeClass();

            //bool isRegistered = false;
            //bool ErrFlag = false;
            //bool Updateerr = false;
            //bool updateregister = false;
            //bool PhoneFlag = false;
            //if (txtempname.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Name');", true);
            //    ErrFlag = true;
            //}
            //else if (txtempfname.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your Father Name');", true);
            //    ErrFlag = true;
            //}
            //else if (txtPSAdd1.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Your PSAddress1');", true);
            //    ErrFlag = true;
            //}
            //else if (txtPSAdd2.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('EnterYourPSAddress2ess2');", true);
            //    ErrFlag = true;
            //}
            //else if (rbtngender.SelectedValue == "0" || rbtngender.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Gender');", true);
            //    ErrFlag = true;
            //}
            //else if (rbtneducateduneducated.SelectedValue == "0" || rbtneducateduneducated.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select EDUCATION DETAILS');", true);
            //    ErrFlag = true;
            //}
            //else if (Rbmartial.SelectedValue == "0" || Rbmartial.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Martial Status');", true);
            //    ErrFlag = true;
            //}
            //else if (ddemployeetype.SelectedValue == "0" || ddemployeetype.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Employee Type');", true);
            //    ErrFlag = true;
            //}
            //else if (Rbmartial.SelectedValue.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Martial Status');", true);
            //    ErrFlag = true;
            //}
            //else if (Rbfather.SelectedValue.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Inital Type');", true);
            //    ErrFlag = true;
            //}
            //else if (ddlcategory.SelectedValue == "2")
            //{
            //    if (rbtneducateduneducated.SelectedValue.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Education Type');", true);
            //        ErrFlag = true;
            //    }
            //}
            //else if (txtdob.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Your Date of Birth\\nSelect Your PMDistrict\\nSelect Your PSDistrict\\nSelect Your PMTaluk\\nSelect Your PSTaluk');", true);
            //    ErrFlag = true;
            //}
            //else if (ddldepartment.Text == "0")
            //{

            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department...!');", true);
            //    ErrFlag = true;
            //}
            //else if ((txtexistiongno.Text == "") || (txtexistiongno.Text == "0"))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing No...!');", true);
            //    ErrFlag = true;
            //}
            //else if (txtphone.Text != "")
            //{
            //    if (txtphone1.Text == "")
            //    {
            //        //panelError.Visible = true;
            //        //lblError.Text = "Enter the Phone No Properly";
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Phone No properly...!');", true);
            //        //System.Windows.Forms.MessageBox.Show("Enter the Phone No Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //        ErrFlag = true;
            //    }
            //}

            //else if (txtMobileCode.Text != "")
            //{

            //    if (txtmobile.Text == "")
            //    {

            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Mobile No properly...!');", true);
            //        //System.Windows.Forms.MessageBox.Show("Enter the Mobile No Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //        ErrFlag = true;
            //    }

            //}
            //else if (txtOld.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the MachineID...!');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Mobile No Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}



            //if (btnsave.Text == "Save")
            
            //{
            //    if (!ErrFlag)
            //    {

            //        bool ErrFlag1 = false;
            //        if (ddlcategory.SelectedValue == "1")
            //        {
            //            EmNm = "Emp";
            //        }
            //        else if (ddlcategory.SelectedValue == "2")
            //        {

            //            string Name = ddemployeetype.SelectedItem.Text;
            //            string Nm = Name.Substring(0, 3);
            //            EmNm = Nm;
            //        }
            //        string ExistingNo = objdata.ExistingNO_Verify(txtexistiongno.Text, SessionCcode, SessionLcode);
            //        if (ExistingNo == txtexistiongno.Text)
            //        {
            //            //panelError.Visible = true;
            //            //lblError.Text = "Existing no already Exist...!";
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Existing No already Exist...!');", true);
            //            //System.Windows.Forms.MessageBox.Show("Existing No Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //            ErrFlag1 = true;
            //        }
            //        //Check with OLD ID
            //        string OLD_No_Check = objdata.OLDNO_Verify(txtOld.Text, SessionCcode, SessionLcode);
            //        if (OLD_No_Check == txtOld.Text)
            //        {
            //            //panelError.Visible = true;
            //            //lblError.Text = "Existing no already Exist...!";
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('OLD ID already Exist...!');", true);
            //            //System.Windows.Forms.MessageBox.Show("Existing No Already Exist", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //            ErrFlag1 = true;
            //        }
            //        if (!ErrFlag1)
            //        {
            //            string MonthYear = System.DateTime.Now.ToString("MMyyyy");
            //            // string dd = System.DateTime.Now.ToString("ddMMyyy");
            //            string RegNo = objdata.MaxEmpNo();
            //            int Reg = Convert.ToInt32(RegNo);
            //            EmpNo = EmNm + MonthYear + RegNo;
            //            objEmp.EmpCode = EmpNo;
            //            objEmp.MachineCode = EmpNo;
            //            objEmp.ExisitingCode = txtexistiongno.Text;
            //            objEmp.EmpName = txtempname.Text;
            //            objEmp.EmpFname = txtempfname.Text;
            //            objEmp.Gender = rbtngender.SelectedValue;
            //            objEmp.DOB = txtdob.Text;
            //            // MyDateTime = DateTime.ParseExact(objEmp.DOB, "dd-MM-yyyy", culterInfo.DateTimeFormat);
            //            objEmp.PSAdd1 = txtPSAdd1.Text;
            //            objEmp.PSAdd2 = txtPSAdd2.Text;

            //            objEmp.PSTaluk = txtTaluk.Text;
            //            objEmp.PSDistrict = txtDistrict.Text;
            //            objEmp.PSState = txtpsstate.Text;
            //            if (ddlcategory.SelectedValue == "1")
            //            {
            //                objEmp.UnEducated = "1";
            //            }
            //            else if (ddlcategory.SelectedValue == "2")
            //            {
            //                objEmp.UnEducated = rbtneducateduneducated.SelectedValue;
            //            }
            //            objEmp.SchoolCollege = txtscholl.Text;


            //            objEmp.Phone = (txtphone.Text + "-" + txtphone1.Text);
            //            objEmp.Mobile = (txtMobileCode.Text + "-" + txtmobile.Text);

            //            objEmp.PassportNo = txtppno.Text;
            //            objEmp.DrivingLicNo = txtdrivingno.Text;
            //            objEmp.Qualification = ddlqualification.SelectedValue;
            //            objEmp.University = txtuniversity.Text;

            //            objEmp.Department = ddldepartment.SelectedValue;
            //            objEmp.Desingation = txtdesignation.Text;

            //            objEmp.EmployeeType = ddemployeetype.SelectedValue;
            //            objEmp.Ccode = SessionCcode;
            //            objEmp.Lcode = SessionLcode;
            //            objEmp.OldID = txtOld.Text;
            //            string FileName = Path.GetFileName(fileUpload.PostedFile.FileName);
            //            if (fileUpload.HasFile)
            //            {
            //                string Exten = Path.GetExtension(fileUpload.PostedFile.FileName);
            //                if (Exten == ".jpg")
            //                {
            //                    fileUpload.SaveAs(Server.MapPath("EmployeeImages/" + txtexistiongno.Text.Trim() + Exten));
            //                    fileUpload.SaveAs("D:/Images/" + txtexistiongno.Text.Trim() + Exten);
            //                }
            //            }
            //            if (ddemployeetype.SelectedValue == "3")
            //            {
            //                objEmp.ContractType = ddlContract.SelectedValue;
            //            }
            //            else
            //            {
            //                objEmp.ContractType = "0";
            //            }

            //            string stafflabor = "", RoleCode = "";
            //            if (ddlcategory.SelectedValue == "1")
            //            {
            //                stafflabor = "S";
            //            }
            //            else if (ddlcategory.SelectedValue == "2")
            //            {
            //                stafflabor = "L";
            //            }
            //            if (UserID == "Admin")
            //            {
            //                RoleCode = "1";

            //            }
            //            else
            //            {
            //                RoleCode = "2";
            //            }
            //            if (chkboxnonpfgrade.Checked == true)
            //            {
            //                NonPFGrade = "1";
            //            }
            //            else if (chkboxnonpfgrade.Checked == false)
            //            {
            //                NonPFGrade = "2";
            //            }
            //            if (chkHostel.Checked == true)
            //            {
            //                Hostel = "1";
            //            }
            //            else if (chkHostel.Checked == false)
            //            {
            //                Hostel = "0";
            //            }
            //            objEmp.Hostel = Hostel;
            //            objEmp.MaritalStatus = Rbmartial.SelectedValue;
            //            objEmp.Initial = Rbfather.SelectedValue;
            //            //objEmp.EmpUnitName = txtEmpUnit.Text.ToString();
            //            try
            //            {
            //                //panelError.Visible = false;
            //                objdata.InsertEmployeeDetails(objEmp, Reg, txtdob.Text, stafflabor, NonPFGrade, RoleCode);
            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Save Successfully....!');", true);
            //                Response.Redirect("EmployeeView.aspx");

            //            }
            //            catch (Exception ex)
            //            {

            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin....!');", true);
            //                //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //                ErrFlag = true;
            //            }
            //        }


            //    }


            //}

            //else if (btnsave.Text == "Update")
            //{
            //    string EmpVerify = objdata.EmployeeVerify(txtEmpNo.Text);

            //    if (txtEmpNo.Text == "")
            //    {
            //        //panelError.Visible = true;
            //        //lblError.Text = "Enter the Employee No Properly";
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Id properly....');", true);
            //        //System.Windows.Forms.MessageBox.Show("Enter the Employee No Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //        Updateerr = true;
            //    }
            //    else if (txtEmpNo.Text != EmpVerify)
            //    {
            //        //panelError.Visible = true;
            //        //lblError.Text = "Enter the Employee No Properly";
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Id properly....');", true);
            //        //System.Windows.Forms.MessageBox.Show("Enter the Employee No Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //        Updateerr = true;
            //    }
            //    if (!Updateerr)
            //    {
            //        objEmp.EmpCode = txtEmpNo.Text;
            //        objEmp.MachineCode = txtEmpNo.Text;
            //        objEmp.ExisitingCode = txtexistiongno.Text;
            //        objEmp.EmpName = txtempname.Text;
            //        objEmp.EmpFname = txtempfname.Text;
            //        objEmp.Gender = rbtngender.SelectedValue;
            //        objEmp.DOB = txtdob.Text;
            //        //MyDateTime = DateTime.ParseExact(objEmp.DOB, "dd-MM-yyyy", null);
            //        objEmp.PSAdd1 = txtPSAdd1.Text;
            //        objEmp.PSAdd2 = txtPSAdd2.Text;
            //        objEmp.PSTaluk = txtTaluk.Text;
            //        objEmp.PSDistrict = txtDistrict.Text;
            //        objEmp.PSState = txtpsstate.Text;
            //        objEmp.OldID = txtOld.Text;
            //        if (ddlcategory.SelectedValue == "1")
            //        {
            //            objEmp.UnEducated = "1";
            //        }
            //        else if (ddlcategory.SelectedValue == "2")
            //        {
            //            objEmp.UnEducated = rbtneducateduneducated.SelectedValue;
            //        }

            //        objEmp.SchoolCollege = txtscholl.Text;
            //        objEmp.Phone = (txtphone.Text + "-" + txtphone1.Text);
            //        objEmp.Mobile = (txtMobileCode.Text + "-" + txtmobile.Text);
            //        objEmp.PassportNo = txtppno.Text;
            //        objEmp.DrivingLicNo = txtdrivingno.Text;
            //        objEmp.Qualification = ddlqualification.SelectedValue;
            //        objEmp.University = txtuniversity.Text;
            //        objEmp.Department = ddldepartment.SelectedValue;
            //        objEmp.Desingation = txtdesignation.Text;
            //        objEmp.EmployeeType = ddemployeetype.SelectedValue;
            //        objEmp.Ccode = SessionCcode;
            //        objEmp.Lcode = SessionLcode;
            //        objEmp.Hostel = Hostel;
            //        if (fileUpload.HasFile)
            //        {
            //            string Exten = Path.GetExtension(fileUpload.PostedFile.FileName);
            //            if (Exten == ".jpg")
            //            {
            //                fileUpload.SaveAs(Server.MapPath("EmployeeImages/" + txtexistiongno.Text.Trim() + Exten));
                           
            //            }
            //        }
            //        if (ddlcategory.SelectedValue == "1")
            //        {
            //            stafflabor = "S";
            //        }
            //        else if (ddlcategory.SelectedValue == "2")
            //        {
            //            stafflabor = "L";
            //        }
                   
            //        if (chkboxnonpfgrade.Checked == true)
            //        {
            //            NonPFGrade = "1";
            //        }
            //        else if (chkboxnonpfgrade.Checked == false)
            //        {
            //            NonPFGrade = "2";
            //        }
            //        if (UserID == "Admin")
            //        {

            //        }
            //        objEmp.MaritalStatus = Rbmartial.SelectedValue;
            //        objEmp.Initial = Rbfather.SelectedValue;
            //        //objEmp.EmpUnitName = txtEmpUnit.Text.ToString();
            //        try
            //        {

            //            objdata.UpdateEmployeeRegistration(objEmp, txtEmpNo.Text, stafflabor, txtdob.Text, NonPFGrade);
            //            btnsave.Text = "Save";
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully....!');", true);
            //            Response.Redirect("EmployeeView.aspx");

                        
            //        }
            //        catch (Exception ex)
            //        {
            //            //panelError.Visible = true;
            //            //lblError.Text = "Server Error - Contact Admin";
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin....!');", true);
            //            //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //            ErrFlag = true;
            //        }

            //    }

            //}



        }

        catch (Exception ex)
        {
            //panelError.Visible = true;
            //lblError.Text = "Contact Admin";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Contact Admin....!');", true);
            //System.Windows.Forms.MessageBox.Show("Contact Admin", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }

        
    }


    public void Clear()
    {
        txtdesignation.Text = "";
        txtDistrict.Text = "";
        txtdob.Text = "";
        txtdrivingno.Text = "";
        txtempfname.Text = "";
        txtempname.Text = "";
        txtexistiongno.Text = "";
        txtmobile.Text = "";
        txtMobileCode.Text = "";
        txtOld.Text = "";
        txtphone.Text = "";
        txtphone1.Text = "";
        txtppno.Text = "";
        txtPSAdd1.Text = "";
        txtPSAdd2.Text = "";
        txtpsstate.Text = "";
        txtscholl.Text = "";
        txtTaluk.Text = "";
        txtuniversity.Text = "";
        ddemployeetype.SelectedValue = "0";
        ddlcategory.SelectedValue = "0";
        ddlContract.SelectedValue = "0";
        ddldepartment.SelectedValue = "0";
        ddlqualification.SelectedValue = "0";
        Rbmartial.SelectedIndex = -1;
        Rbfather.SelectedIndex = -1;
        rbtngender.SelectedIndex = -1;
        //RadioButton1.Checked = false;
        //RadioButton2.Checked = false;
        chkHostel.Checked = false;
        chkboxnonpfgrade.Checked = false;
      
    }

   
    protected void btnclear_Click(object sender, EventArgs e)
    {
        Clear();
    }

   

    protected void ddemployeetype_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if (ddemployeetype.SelectedValue == "3")
        {
            lblContract.Visible = true;
            ddlContract.Visible = true;
        }
        else
        {
            lblContract.Visible = false;
            ddlContract.Visible = false;
        }
    }

    public void DisplayEmployeeDetails()
    {
        DataTable dtd = new DataTable();
        dtd = objdata.CheckAndReturn(txtEmpNo.Text, SessionCcode, SessionLcode);

        if (dtd.Rows.Count > 0)
        {
            string category = dtd.Rows[0]["StafforLabor"].ToString();
            if (category.ToString() == "S")
            {
                ddlcategory.SelectedIndex = 1;

            }
            else if (category.ToString() == "L")
           {
                ddlcategory.SelectedIndex = 2;

          }
            Visible_true();


            txtempname.Text = dtd.Rows[0]["EmpName"].ToString();
            txtempfname.Text = dtd.Rows[0]["FatherName"].ToString();
            if (dtd.Rows[0]["Gender"].ToString() == "1")
            {
                rbtngender.SelectedValue = "1";
            }
            else if (dtd.Rows[0]["Gender"].ToString() == "2")
            {
                rbtngender.SelectedValue = "2";
            }
            DateTime dob=Convert.ToDateTime(dtd.Rows[0]["DOB"].ToString());
            txtdob.Text=dob.ToString("dd-MM-yyyy");
            if(dtd.Rows[0]["MartialStatus"].ToString()=="U")
            {
                Rbmartial.SelectedValue = "U";
            }
            else if (dtd.Rows[0]["MartialStatus"].ToString() == "M")
            {
                Rbmartial.SelectedValue = "M";
            }
            txtPSAdd1.Text = dtd.Rows[0]["PSAdd1"].ToString();
            txtDistrict.Text = dtd.Rows[0]["PSDistrict"].ToString();
            txtPSAdd2.Text = dtd.Rows[0]["PSAdd2"].ToString();
            txtTaluk.Text = dtd.Rows[0]["PSTaluk"].ToString();
            txtpsstate.Text = dtd.Rows[0]["PSState"].ToString();
            string[] phone_split = dtd.Rows[0]["Phone"].ToString().Split('-');
            txtphone.Text = phone_split[0].ToString();
            txtphone1.Text = phone_split[1].ToString();
            //txtphone.Text = dted.Rows[0]["Phone"].ToString();
            string[] Mobile_split = dtd.Rows[0]["Mobile"].ToString().Split('-');
            txtMobileCode.Text = Mobile_split[0].ToString();
            txtmobile.Text = Mobile_split[1].ToString();
            txtppno.Text = dtd.Rows[0]["PassportNo"].ToString();
            txtdrivingno.Text = dtd.Rows[0]["DrivingLicenceNo"].ToString();
            ddlqualification.SelectedValue = dtd.Rows[0]["Qualification"].ToString();
            txtscholl.Text = dtd.Rows[0]["SchoolCollege"].ToString();
            txtuniversity.Text = dtd.Rows[0]["University"].ToString();
            ddldepartment.SelectedValue = dtd.Rows[0]["Department"].ToString();
            txtdesignation.Text = dtd.Rows[0]["Designation"].ToString();
            //txtunedudept.Text = dted.Rows[0]["UnEduDept"].ToString();
            //txtunedudesg.Text = dted.Rows[0]["UnEduDesig"].ToString();
            ddemployeetype.SelectedValue = dtd.Rows[0]["EmployeeType"].ToString();
            txtexistiongno.Text = dtd.Rows[0]["ExisistingCode"].ToString();
            txtOld.Text = dtd.Rows[0]["OldID"].ToString();
            if (dtd.Rows[0]["NonPFGrade"].ToString() == "1")
            {
                chkboxnonpfgrade.Checked = true;
            }
            else
            {
                chkboxnonpfgrade.Checked = false;
            }
            if (dtd.Rows[0]["Hostel"].ToString() == "1")
            {
                chkHostel.Checked = true;
            }
            else
            {
                chkHostel.Checked = false;
            }


        }

        

    }

    public void Visible_true()
    {
        
        string s = ddlcategory.SelectedValue;

        EmployeeType();

        if (s=="1")
        {
            lblEmpNo.Visible = true;
            txtEmpNo.Visible = true;
            lblPrsnDet.Visible = true;
            lblempname.Visible = true;
            txtempname.Visible = true;
            lblempname.Visible = true;
            lblempfname.Visible = true;
            txtempfname.Visible = true;
            lblgender.Visible = true;
            rbtngender.Visible = true;
            lblDOB.Visible = true;
            txtdob.Visible = true;
            lblMartial.Visible = true;
            Rbmartial.Visible = true;
            lblinitial.Visible = true;
            Rbfather.Visible = true;
            lblPSadd1.Visible = true;
            txtPSAdd1.Visible = true;
            lblDist.Visible = true;
            txtDistrict.Visible = true;
            lblPSAdd2.Visible = true;
            txtPSAdd2.Visible = true;
            lblTaluk.Visible = true;
            txtTaluk.Visible = true;
            lblState.Visible = true;
            txtpsstate.Visible = true;
            lblPhn.Visible = true;
            txtphone.Visible = true;
            txtphone1.Visible = true;
            lblMob.Visible = true;
            txtMobileCode.Visible = true;
            txtmobile.Visible = true;
            lblppno.Visible = true;
            txtppno.Visible = true;
            lblDirNo.Visible = true;
            txtdrivingno.Visible = true;
            lblEdu.Visible = true;
            rbtneducateduneducated.Visible = false;
            //rbtneducated.Visible = false;
            //rbtnuneducated.Visible = false;
            //RadioButton1.Visible = false;
            //RadioButton2.Visible = false;
            lblqualif.Visible = true;
            ddlqualification.Visible = true;
            lblunive.Visible = true;
            txtuniversity.Visible = true;
            lblCmpDet.Visible = true;
            lblDept.Visible = true;
            ddldepartment.Visible = true;
            lblDesgn.Visible = true;
            txtdesignation.Visible = true;
            lblemptype.Visible = true;
            ddemployeetype.Visible = true;
            lblexisiting.Visible = true;
            txtexistiongno.Visible = true;
            lblMachID.Visible = true;
            txtOld.Visible = true;
            lblContract.Visible = false;
            ddlContract.Visible = false;
            lblnonpfgrade.Visible = true;
            chkboxnonpfgrade.Visible = true;
            lblHostel.Visible = true;
            chkHostel.Visible = true;
            btnsave.Visible = true;
            btnclear.Visible = true;
        }
        else if (s == "2")
        {
            lblEmpNo.Visible = true;
            txtEmpNo.Visible = true;
            lblPrsnDet.Visible = true;
            lblempname.Visible = true;
            txtempname.Visible = true;
            lblempname.Visible = true;
            lblempfname.Visible = true;
            txtempfname.Visible = true;
            lblgender.Visible = true;
            rbtngender.Visible = true;
            lblDOB.Visible = true;
            txtdob.Visible = true;
            lblMartial.Visible = true;
            Rbmartial.Visible = true;
            lblinitial.Visible = true;
            Rbfather.Visible = true;
            lblPSadd1.Visible = true;
            txtPSAdd1.Visible = true;
            lblDist.Visible = true;
            txtDistrict.Visible = true;
            lblPSAdd2.Visible = true;
            txtPSAdd2.Visible = true;
            lblTaluk.Visible = true;
            txtTaluk.Visible = true;
            lblState.Visible = true;
            txtpsstate.Visible = true;
            lblPhn.Visible = true;
            txtphone.Visible = true;
            txtphone1.Visible = true;
            lblMob.Visible = true;
            txtMobileCode.Visible = true;
            txtmobile.Visible = true;
            lblppno.Visible = true;
            txtppno.Visible = true;
            lblDirNo.Visible = true;
            txtdrivingno.Visible = true;
            lblEdu.Visible = true;
            rbtneducateduneducated.Visible = true;
            //rbtneducated.Visible = true;
            //rbtnuneducated.Visible = true;
            //RadioButton1.Visible = true;
            //RadioButton2.Visible = true;
            lblqualif.Visible = false;
            ddlqualification.Visible = false;
            lblunive.Visible = false;
            txtuniversity.Visible = false;
            lblCmpDet.Visible = true;
            lblDept.Visible = true;
            ddldepartment.Visible = true;
            lblDesgn.Visible = true;
            txtdesignation.Visible = true;
            lblemptype.Visible = true;
            ddemployeetype.Visible = true;
            lblexisiting.Visible = true;
            txtexistiongno.Visible = true;
            lblMachID.Visible = true;
            txtOld.Visible = true;
            lblContract.Visible = false;
            ddlContract.Visible = false;
            lblnonpfgrade.Visible = true;
            chkboxnonpfgrade.Visible = true;
            lblHostel.Visible = true;
            chkHostel.Visible = true;
            btnsave.Visible = true;
            btnclear.Visible = true;
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
