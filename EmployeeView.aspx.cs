﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Drawing;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data;

public partial class EmployeeView : System.Web.UI.Page
{
    static string UserID = "", SesRoleCode = "", sessionAdmin = "";
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    string SSQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        UserID = Session["UserId"].ToString();
        SesRoleCode = Session["RoleCode"].ToString();
        sessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();

        if (!IsPostBack)
        {
            CreateEmployeeDisplay();


        }
    }

    public void CreateEmployeeDisplay()
    {
        DataTable dtdDisplay = new DataTable();

        dtdDisplay = objdata.EmployeeDisplay(SessionCcode, SessionLcode);
        if (dtdDisplay.Rows.Count > 0)
        {
            rptrEmployee.DataSource = dtdDisplay;
            rptrEmployee.DataBind();
        }
    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
           
            case ("Edit"):
                EditRepeaterData(id);
                break;
        }
    }

    

    private void EditRepeaterData(string id)
    {
        Session["MachineNo"] = id;
        Response.Redirect("EmployeeRegistration2.aspx");
        //ResponseHelper.Redirect("new_employee.aspx?MachineID=" + id, "_blank", ""); 
    }
    protected void btnNEWEMPentry_Click(object sender, EventArgs e)
    {
        string id = "";
        Session["MachineNo"] = id;
        Response.Redirect("EmployeeRegistration2.aspx");
        CreateEmployeeDisplay();
    }


}
