﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RptAdvance.aspx.cs" Inherits="RptAdvance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
    
             $(document).ready(function () {
             $('#tableEmployee').dataTable();
        });
        </script>
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Salary Advance History</li></h4> 
                    </ol>
              </div>

<div id="main-wrapper" class="container">
<div class="row">
<div class="col-md-12">
              
 <div class="col-md-9">
<div class="panel panel-white">
<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title"> Advance History</h4>
				</div>
				</div>
<form class="form-horizontal">
<div class="panel-body"> 

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="Label7" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
				     
					   <div class="col-sm-3">
					      <asp:DropDownList ID="ddlcategory" class="form-control" runat="server" 
					      AutoPostBack="True" onselectedindexchanged="ddlcategory_SelectedIndexChanged">
                            </asp:DropDownList>  
					   </div>
				       <div class="col-md-1"></div>
				       <asp:Label ID="Label1" runat="server" class="col-sm-2 control-label" 
                                                             Text="Department"></asp:Label>
                	  <div class="col-sm-3">
						<asp:DropDownList ID="ddldepartment" class="form-control" runat="server" 
						AutoPostBack="true" onselectedindexchanged="ddldepartment_SelectedIndexChanged">
                            </asp:DropDownList> 
					  </div>
						
					  </div>
				        
</div>
 
<div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="lblEmployee_Type" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee Type"></asp:Label>
				         <div class="col-sm-3">
                              <asp:DropDownList ID="txtEmployee_Type" class="form-control" runat="server">
                              </asp:DropDownList>
					      </div>
					    	
						</div>
	 </div>
	 
<div class="col-md-12">     
<div class="form-group row">
 <asp:Label ID="lblEmp" runat="server" class="col-sm-2 control-label" 
                                                             Text="EmployeeNo"></asp:Label>

   <div class="col-sm-2">
      <asp:DropDownList ID="ddlEmpNo" class="form-control" runat="server" AutoPostBack="true" onselectedindexchanged="ddlEmpNo_SelectedIndexChanged" >
      </asp:DropDownList>  
     </div>
    <div class="col-md-1">    
    </div>
<asp:Label ID="lblExist" runat="server" class="col-sm-2 control-label" 
                                                             Text="Existing No"></asp:Label>
                    
  <div class="col-sm-2">
		<asp:TextBox ID="txtExist" runat="server" class="form-control" ontextchanged="txtExist_TextChanged"></asp:TextBox>
     </div>
						
  <div class="col-sm-3">
          <asp:Button ID="btnSearch" class="btn btn-info"  runat="server" Text="Search" onclick="btnSearch_Click"/>
     </div>
	</div>
		        
 </div> 
 
<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblEmpName" runat="server" class="col-sm-2 control-label" 
                                                             Text="EmployeeName"></asp:Label>

   <div class="col-sm-3">
      <asp:DropDownList ID="ddlEmpName" class="form-control" runat="server" AutoPostBack="true" 
      onselectedindexchanged="ddlEmpName_SelectedIndexChanged" >
      </asp:DropDownList>  
     </div>
  
   <asp:Label ID="lblDepartment" runat="server" class="col-sm-2 control-label" 
                                                             Text="Department"></asp:Label>
                    
    <div class="col-sm-3">
        <asp:Label ID="lblDepartment1" runat="server" Text=""></asp:Label>
     </div>
		
   
	</div>
		        
 </div> 

<div class="col-md-12">     
 <div class="form-group row">
  <asp:Label ID="lbldesignation" runat="server" class="col-sm-2 control-label" 
                                                             Text="Designation"></asp:Label>
    <div class="col-sm-3">
    <asp:Label ID="lbldesignation1" runat="server" Text=""></asp:Label>  
     </div>
  
   <asp:Label ID="lbldop" runat="server" class="col-sm-2 control-label" 
                                                             Text="Date of Join"></asp:Label>                    
   
    <div class="col-sm-3">
        <asp:Label ID="lbldop1" runat="server" Text=""></asp:Label>
     </div>
		
   
	</div>
		        
 </div> 
 
<asp:Panel ID="panelAdvance" runat="server" Visible="false" >

 <div class="col-md-12">     
 
 <div class="row">
		        
	<asp:Repeater ID="rptrEmployee" runat="server" onitemcommand="Repeater1_ItemCommand">
                    <HeaderTemplate>
                       <table id="tableEmployee" class="display table">
                    <thead>
                        <tr>
                       
                        <th>Advance Id</th>
                        <th>Availed Date</th>
                        <th>Advance Amount</th>
                        <th>Settled Date</th>
                        <th>Duration</th>
                        <th>Balance Amount</th>
                        <th>Completed</th>
                        <th>View</th>
                                               
                       </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                
                    <td>
                   <%# Eval("ID") %>
                    </td>
                    <td>
                    <%# Eval("CreateDate") %>
                    </td>
                    <td><%# Eval("Amount") %></td>
                    <td><%# Eval("Modifieddate") %></td>
                    <td><%# Eval("ReductionMonth") %></td>
                    <td><%# Eval("BalanceAmount")%></td>
                    <td><%# Eval("Completed") %> </td>
                     <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("ID") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p>
                     </td>
                    
               </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>	        
		        
		        
</div>		        
 </div>
 
 </asp:Panel>
 
 
<div class="col-md-12">     
				<div class="form-group row">
				
				<div class="col-sm-2"></div>	
			     
				 <div class="col-sm-3">  
				     <asp:Button ID="btnAll_Emp_Advance" class="btn btn-success" runat="server" Text="AllEmpAdvn" onclick="btnAll_Emp_Advance_Click"/>    
				 </div>
		
  				
  				 <div class="col-sm-3">
                     <asp:Button ID="btnclr" class="btn btn-success" runat="server" Text="Clear" onclick="btnclr_Click"/>
  				 </div>
  				 
  				 				 				
			    </div>
				       
        </div>
 

</div>
</form>

</div>
</div>
 
 <!-- Dashboard start -->
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                           
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                           
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
    
 <!-- Dashboard End -->


</div>
</div>
</div>
 
 </ContentTemplate>
 <Triggers>
 <asp:PostBackTrigger ControlID="btnSearch"  />
 <asp:PostBackTrigger ControlID="btnAll_Emp_Advance"  />
 </Triggers>
 </asp:UpdatePanel>
</asp:Content>

