﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Payroll Management System Login</title>
    
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <link href="assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
        <link href="assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>	
        <link href="assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>	
        <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>	
        
        <!-- Theme Styles -->
        <link href="assets/css/modern.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
        
        <script src="assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    
</head>
 <body class="page-login login-alt" onload="GoBack();">
        <main class="page-content">
            <div class="page-inner">
                <div id="main-wrapper">
                    <div class="row">
                        <div class="col-md-6 center">
                            <div class="login-box panel panel-white">
                                <div class="panel-body">
                                    <div class="row" align="center">
                                     

    <form id="form1" class="m-t-md" runat="server">
  <cc1:toolkitscriptmanager runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
                                           ID="ScriptManager1" EnablePartialRendering="true">
                                         </cc1:toolkitscriptmanager>
                                          <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate>
                                          <%--  <div class="panel panel-white">--%>
			                               <%--<div class="panel panel-primary" height="100%">
                                           <div class="panel-heading clearfix" height="100%">
					                          
					                          <asp:Label ID="Label1" runat="server" Text="SMART PAY" BorderStyle="None"  
                                                   Font-Bold="True" Font-Size="XX-Large" ForeColor="White" Font-Italic="True"></asp:Label> 
					                           
					                           
				                           </div>
				                           </div>--%>
				                           <%--</div>--%>
                                            <div class="col-md-6 m-b-md" align="center">
                                             <img src="./new_images/icons/user-login.png" alt="" />
                                            </div>
                                
                                           
                                                 <div class="col-md-6" align="center">
                                                     <div class="form-group" align="center">
                                                        <asp:DropDownList ID="ddlCode" runat="server" class="form-control" AutopostBack="true" 
                                                             onselectedindexchanged="ddlCode_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                   </div>
                                                   
                                                     <div class="form-group" align="center">
                                                        <asp:DropDownList ID="ddlLocation" runat="server" class="form-control" 
                                                         AutoPostBack="True"></asp:DropDownList>
                                                    </div>
                                                   
                                                     <div class="form-group" align="center">
                                                       <asp:TextBox ID="txtusername" runat="server" class="form-control"  placeholder="User Name" required></asp:TextBox>
                                                       </div>
                                                   
                                                     <div class="form-group" align="center">
                                                       <asp:TextBox ID="txtpassword" runat="server" class="form-control" 
                                                             placeholder="Password" TextMode="Password" required
                                                       ></asp:TextBox>
                                                     </div>
                                                                                                
                                              
                                                       <asp:Button ID="btnsave" runat="server" Text="LOGIN" 
                                                          class="btn btn-success" Height="30%" Weight="30%" onclick="btnsave_Click" 
                                                         ></asp:Button>
                                                    
                                      
                                        </div>
                                        </center>
                                       </ContentTemplate>
                                      </asp:UpdatePanel>
                                      </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
            </div><!-- Page Inner -->
        </main><!-- Page Content -->
	
 <!-- Javascripts -->
        <script src="assets/plugins/jquery/jquery-2.1.3.min.js"></script>
        <script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="assets/plugins/pace-master/pace.min.js"></script>
        <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="assets/plugins/classie/classie.js"></script>
        <script src="assets/plugins/waves/waves.min.js"></script>
        <script src="assets/js/modern.min.js"></script>

        
       
</body>

</html>

