﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;


public partial class User_Master : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    static string lblprobation_Common;
    string SessionCcode;
    string SessionLcode;
    string username;

    protected void Page_Load(object sender, EventArgs e)
    {
         if (SessionAdmin == "2")
        {
            Response.Redirect("EmployeeRegistration.aspx");
        }
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
       
        if (!IsPostBack)
        {
            display();
        }
    }
    public void display()
    {
        DataTable dt = new DataTable();
        dt = objdata.user_gridLoad(SessionCcode, SessionLcode);
        rptrCustomer.DataSource = dt;
        rptrCustomer.DataBind();
        //gvuser.DataSource = dt;
        //gvuser.DataBind();
    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
     {
         username = Convert.ToString(e.CommandArgument);
         switch (e.CommandName)
         {
             case ("Delete"):
                 DeleteRepeaterData(username);
                 break;
             case ("Edit"):
                 EditRepeaterData(username);
                 break;
         }
     }
    public void DeleteRepeaterData(string username)
     {
         DataTable dtdDelete = new DataTable();
         string qry = "Delete from MstUsers where UserName = '" + username + "' and CompCode='" + SessionCcode + "' and LocationCode='" + SessionLcode + "'";
         objdata.ReturnMultipleValue(qry);
         ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Deleted Successfully');", true);
         display();
         Clear();
     }
    public void EditRepeaterData(string username)
     {
          Clear();
         //Label lbledit = (Label)gvuser.Rows[e.NewEditIndex].FindControl("lbluser");
         DataTable dt = new DataTable();
         dt = objdata.user_edit(username.ToString(), SessionCcode , SessionLcode);
         if (dt.Rows.Count > 0)
         {
             txtusercode.Text = dt.Rows[0]["UserCode"].ToString();
             txtusercode.Enabled = false;
             txtpassword.Text = dt.Rows[0]["Password"].ToString();
             txtconfirmpasswd.Text = dt.Rows[0]["Password"].ToString();
             txtusernm.Text = dt.Rows[0]["UserName"].ToString();
             txtmobile.Text = dt.Rows[0]["Mobile"].ToString();
             txtdeaprtment.Text = dt.Rows[0]["Department"].ToString();
             txtdesignation.Text = dt.Rows[0]["Designation"].ToString();
             if (dt.Rows[0]["IsAdmin"].ToString().Trim() == "1")
             {
                 rbtnisadmin.SelectedValue = "1";
             }
             else if (dt.Rows[0]["IsAdmin"].ToString().Trim() == "2")
             {
                 rbtnisadmin.SelectedValue = "2";
             }

             btnSave.Text = "Update";

         }

     }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            UserRegistrationClass objReg = new UserRegistrationClass();

            bool ErrrFlg = false;
            bool isRegistred = false;
            if (txtusercode.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Your User Code');", true);
                ErrrFlg = true;
            }
            else if (txtusernm.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Your Usere Name');", true);
                ErrrFlg = true;
            }
            else if (txtpassword.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Password');", true);
                ErrrFlg = true;
            }
            else if (txtpassword.Text != txtconfirmpasswd.Text)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Password and conformpassword Correctly');", true);
                ErrrFlg = true;
            }

            else if (txtusercode.Enabled == true)
            {
                string user = objdata.user_verify(txtusercode.Text.Trim(), SessionCcode, SessionLcode);
                if (user == txtusercode.Text)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Code already Exist');", true);
                    ErrrFlg = true;
                }
            }
            if (!ErrrFlg)
            {
                objReg.UserCode = txtusercode.Text;
                objReg.UserName = txtusernm.Text;
                objReg.Password = s_hex_md5(txtpassword.Text);
                objReg.IsAdmin = rbtnisadmin.SelectedValue;
                objReg.Mobile = txtmobile.Text;
                objReg.Department = txtdeaprtment.Text;
                objReg.Designation = txtdesignation.Text;
                objReg.Ccode = SessionCcode;
                objReg.Lcode = SessionLcode;
                isRegistred = true;
            }
            try
            {
                if (isRegistred)
                {
                    if (btnSave.Text == "Update")
                    {
                        objdata.UserRegistration_update(objReg);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Updated Successfully...!');", true);
                        Clear();
                        display();
                        btnSave.Text = "Save";
                    }
                    else
                    {
                        objdata.UserRegistration(objReg);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Registered Successfully...!');", true);
                        Clear();
                        display();
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Server Error Contact Admin...!');", true);
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Correct Value');", true);
        }

        }


    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    public void Clear()
    {
        txtusercode.Text = "";
        txtusernm.Text = "";
        txtmobile.Text = "";
        txtdeaprtment.Text = "";
        txtdesignation.Text = "";
        txtusercode.Enabled = true;
        btnSave.Text = "Save";
        rbtnisadmin.SelectedValue = null; 
        txtpassword.Text = "";
        txtconfirmpasswd.Text = "";
        
    }
    public static String s_hex_md5(String originalPassword)
    {
        UTF8Encoding encoder = new UTF8Encoding();
        MD5 md5 = new MD5CryptoServiceProvider();

        Byte[] hashedbytes = md5.ComputeHash(encoder.GetBytes(originalPassword));
        return BitConverter.ToString(hashedbytes).Replace("-", "").ToLower();
    }
}

