﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Upload_OT.aspx.cs" Inherits="Upload_OT" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">OverTime Upload</li></h4> 
                    </ol>
</div>

<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
    <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">OverTime Upload</h4>
				</div>
				</div>
				
				
		<asp:UpdatePanel ID="Panel11" runat="server">
		   <ContentTemplate>
		      <form class="form-horizontal">
			
			<div class="panel-body">
			

                           
				    <div class="form-group row">
					<asp:Label ID="lblFinance" runat="server" class="col-sm-2 control-label" 
                                                             Text="Financial Year"></asp:Label>
					<div class="col-sm-3">
                    <asp:DropDownList ID="ddlfinance" class="form-control" runat="server">
                    </asp:DropDownList>                          
                    </div>
					
				    <div class="col-sm-1"></div>
				       				
				    <asp:Label ID="lblMonths" runat="server" class="col-sm-2 control-label" 
                                                             Text="Month for OT"></asp:Label>    
				  
					<div class="col-sm-3">
                    <asp:DropDownList ID="ddlMonths" class="form-control" runat="server">
                    </asp:DropDownList>   
					</div>
			       </div>
					    
							        				        
				    <div class="form-group row">
					<asp:Label ID="lblfrom" runat="server" class="col-sm-2 control-label" 
                                                             Text="From Date"></asp:Label>    
				
					<div class="col-sm-3">
                          <asp:TextBox ID="txtfrom" class="form-control" runat="server"></asp:TextBox>
                         <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtfrom" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
                         <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR38" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtfrom" ValidChars="0123456789/- "></cc1:FilteredTextBoxExtender>
                    </div>
					    
					<div class="col-md-1"></div>
					<asp:Label ID="lblToDate" runat="server" class="col-sm-2 control-label" 
                                                             Text="To Date"></asp:Label>
					<div class="col-sm-3">
						 <asp:TextBox ID="txtTo" class="form-control" runat="server"></asp:TextBox>
						 <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
                         <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR39" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtTo" ValidChars="0123456789/- "></cc1:FilteredTextBoxExtender>
                    </div>
				    </div>
				        
				    <div class="form-group row">
					    
					     
						<div class="col-sm-4">
                            <asp:FileUpload ID="FileUpload" class="btn btn-default btn-rounded" runat="server" />
                        </div>
					    
					   	<div class="col-md-1"></div>
						
						<div class="col-sm-3">
                            <asp:Button ID="btnUpload" class="btn btn-success" runat="server" Text="Upload" onclick="btnUpload_Click" />
						</div>
				        </div>
				        
			     </div>
            
            </form>
		   </ContentTemplate>
		   <Triggers>
		      <asp:PostBackTrigger ControlID="btnUpload" />
		   </Triggers>
		</asp:UpdatePanel>		
			
			
			</div>
			</div>
		  
		    
		
 	  <!-- Dashboard start -->
 	
 	<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
 
    <!-- Dashboard End -->
 
  
          </div> 
 </div><!-- col 12 end -->
      
  </div><!-- row end -->

</asp:Content>

