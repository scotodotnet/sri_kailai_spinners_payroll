﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class RptResign : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    bool searchFlag = false;
    string SessionAdmin;
    string exportvalue = "";
    bool textvalue = false;
    string SessionCcode;
    string SessionLcode;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            Months();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                ddlFinance1.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;

            }
        }
    }
    public void Months()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }
    protected void ddlreport_SelectedIndexChanged(object sender, EventArgs e)
    {
        panelyearwise.Visible = false;
        panelmont.Visible = false;
        panelemployee.Visible = false;
        exportvalue = "";
        DataTable dtempty = new DataTable();
        gvreport.DataSource = dtempty;
        gvreport.DataBind();
        if (ddlreport.SelectedValue == "1")
        {
            panelyearwise.Visible = true;
        }
        else if (ddlreport.SelectedValue == "2")
        {
            panelmont.Visible = true;
        }
        else if (ddlreport.SelectedValue == "3")
        {
            panelemployee.Visible = true;
        }
    }
    protected void btnyr_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string fromdate = ("01-04-" + ddlfinance.SelectedValue).ToString();
        string Todate = ("31-03-" + (Convert.ToInt32(ddlfinance.SelectedValue) + 1)).ToString();
        if (SessionAdmin == "1")
        {
            dt = objdata.rpt_resign_yr(fromdate, Todate, SessionCcode, SessionLcode);
        }
        else
        {
            dt = objdata.rpt_resign_yr_user(fromdate, Todate, SessionCcode, SessionLcode);
        }
        if (dt.Rows.Count > 0)
        {
            gvreport.DataSource = dt;
            gvreport.DataBind();
            exportvalue = "yr";
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data Found');", true);
            //System.Windows.Forms.MessageBox.Show("No Data Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void ddlfinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        gvreport.DataSource = dtempty;
        gvreport.DataBind();
    }
    protected void ddlFinance1_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        gvreport.DataSource = dtempty;
        gvreport.DataBind();
        ddlMonths.SelectedValue = "0";
    }
    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        gvreport.DataSource = dtempty;
        gvreport.DataBind();
    }
    protected void btnmonths_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable dtempty = new DataTable();
        gvreport.DataSource = dtempty;
        gvreport.DataBind();
        if (ddlfinance.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Financial year');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Financial year", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddlMonths.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Months');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Months", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            string fromdate = ("01-04-" + ddlFinance1.SelectedValue).ToString();
            string Todate = ("31-03-" + (Convert.ToInt32(ddlFinance1.SelectedValue) + 1)).ToString();
            //dt = objdata.rpt_resign_yr(fromdate, Todate);
            if (SessionAdmin == "1")
            {
                dt = objdata.rpt_resign_month(fromdate, Todate, ddlMonths.SelectedValue, SessionCcode, SessionLcode);
            }
            else
            {
                dt = objdata.rpt_resign_month_user(fromdate, Todate, ddlMonths.SelectedValue, SessionCcode, SessionLcode);
            }
            if (dt.Rows.Count > 0)
            {
                gvreport.DataSource = dt;
                gvreport.DataBind();
                exportvalue = "month";
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data Found');", true);
                //System.Windows.Forms.MessageBox.Show("No Data Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }
    }
    protected void txtEmpno_TextChanged(object sender, EventArgs e)
    {
        if (textvalue == false)
        {
            DataTable dtempty = new DataTable();
            gvreport.DataSource = dtempty;
            gvreport.DataBind();
            txtexist.Text = "";
        }
    }
    protected void txtexist_TextChanged(object sender, EventArgs e)
    {
        if (textvalue == false)
        {
            DataTable dtempty = new DataTable();
            gvreport.DataSource = dtempty;
            gvreport.DataBind();
            txtEmpno.Text = "";
        }
    }
    protected void btnemployee_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if ((txtEmpno.Text.Trim() == null) && (txtexist.Text.Trim() == null))
        {
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            if (txtEmpno.Text != "")
            {
                if (SessionAdmin == "1")
                {
                    dt = objdata.rpt_resign_empNo(txtEmpno.Text, SessionCcode, SessionLcode);
                }
                else
                {
                    dt = objdata.rpt_resign_empNo_user(txtEmpno.Text, SessionCcode, SessionLcode);
                }
            }
            else if (txtexist.Text != "")
            {
                if (SessionAdmin == "1")
                {
                    dt = objdata.rpt_resign_exist(txtexist.Text, SessionCcode, SessionLcode);
                }
                else
                {
                    dt = objdata.rpt_resign_exist_user(txtexist.Text, SessionCcode, SessionLcode);
                }
            }
            if (dt.Rows.Count > 0)
            {
                textvalue = true;
                gvreport.DataSource = dt;
                gvreport.DataBind();
                exportvalue = "employee";
                txtexist.Text = dt.Rows[0]["ExistingCode"].ToString();
                txtEmpno.Text = dt.Rows[0]["EmpNo"].ToString();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data Found');", true);
                //System.Windows.Forms.MessageBox.Show("No Data Found", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
            textvalue = false;

        }

    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlexport.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Data');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Data", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
        if (!ErrFlag)
        {
            if (gvreport.Rows.Count > 0)
            {
                if (ddlexport.SelectedValue == "1")
                {
                    string attachment = "attachment; filename=RptResign.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";

                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    gvreport.RenderControl(htextw);
                    Response.Write("Resign Details");
                    Response.Write(stw.ToString());

                    Response.End();
                }
                else if (ddlexport.SelectedValue == "2")
                {

                    string attachment = "attachment; filename=RptResign.pdf";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/pdf";
                    StringWriter stw = new StringWriter();

                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    gvreport.RenderControl(htextw);

                    Document document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);


                    PdfWriter.GetInstance(document, Response.OutputStream);
                    document.Open();
                    StringReader str = new StringReader(stw.ToString());
                    HTMLWorker htmlworker = new HTMLWorker(document);
                    htmlworker.Parse(str);
                    document.Close();
                    Response.Write(document);
                    Response.End();
                }
                else if (ddlexport.SelectedValue == "3")
                {

                    string attachment = "attachment; filename=RptResign.doc";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);

                    gvreport.RenderControl(htextw);
                    Response.Write("Resign Details");
                    Response.Write(stw.ToString());
                    Response.End();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Data to Export');", true);
                //System.Windows.Forms.MessageBox.Show("No Data to Export", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }

}
