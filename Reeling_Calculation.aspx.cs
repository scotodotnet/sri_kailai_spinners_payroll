﻿using AjaxControlToolkit;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System;
using System.Data;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class Reeling_Calculation : System.Web.UI.Page
{
 
    private decimal callarge;
    private decimal calmedium;
    private decimal calSmall;
    private decimal calxl;
    private decimal calxxl;
 
    private static string ESI = "0";
  
    private decimal finallarge;
    private decimal finalmedium;
    private decimal finalsmall;
    private decimal finalxl;
    private decimal finalxxl;
  
    private decimal Large;
  
    private decimal medium;
    private static string NetPay = "0";
    private Altius.BusinessAccessLayer.BALDataAccess.BALDataAccess objdata = new Altius.BusinessAccessLayer.BALDataAccess.BALDataAccess();
    private static string pfAmt = "0";
    private static string PFS = "0.00";
    private string SessionAdmin;
    private string SessionCcode;
    private string SessionLcode;
    private decimal Small;
    private string SSQL = "";
    private decimal total;
    private static string TotalDeductions = "0";
    private static string TotalEarnings = "0";
   
    private decimal XL;
    private decimal XXL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Session["UserId"] == null)
        {
            base.Response.Redirect("Default.aspx");
            base.Response.Write("Your session expired");
        }
        this.SessionAdmin = this.Session["Isadmin"].ToString();
        this.SessionCcode = this.Session["Ccode"].ToString();
        this.SessionLcode = this.Session["Lcode"].ToString();
        this.txtDept.Text = "REELING";
        if (!base.IsPostBack)
        {
            this.Months_load();
            this.DropDown_TokenNumber();
            int getFinancialYear = Payroll.Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                string text = getFinancialYear.ToString() + "-" + Convert.ToString((int)(getFinancialYear + 1));
                this.ddlFinance.Items.Add(new ListItem(text, getFinancialYear.ToString()));
                getFinancialYear--;
            }
            getFinancialYear = Payroll.Utility.GetFinancialYear;
            for (int j = 0; j < 10; j++)
            {
                getFinancialYear--;
            }
        }
    }
    public void Months_load()
    {
        DataTable table = new DataTable();
        table = this.objdata.months_load();
        this.ddlMonths.DataSource = table;
        this.ddlMonths.DataTextField = "Months";
        this.ddlMonths.DataValueField = "Months";
        this.ddlMonths.DataBind();
    }
    public void DropDown_TokenNumber()
    {
        DataTable table = new DataTable();
        this.SSQL = "Select EmpDet.ExisistingCode  as ExisistingCode from  EmployeeDetails EmpDet inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department where  MstDpt.DepartmentNm='REELING'  ";
        table = this.objdata.RptEmployeeMultipleDetails(this.SSQL);
        this.ddlempno.DataSource = table;
        DataRow row = table.NewRow();
        row["ExisistingCode"] = "-----Select----";
        row["ExisistingCode"] = "-----Select----";
        table.Rows.InsertAt(row, 0);
        this.ddlempno.DataTextField = "ExisistingCode";
        this.ddlempno.DataValueField = "ExisistingCode";
        this.ddlempno.DataBind();
    }
    protected void ddlempno_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable table = new DataTable();
        this.SSQL = "Select MachineNo,EmpName from EmployeeDetails where ExisistingCode='" + this.ddlempno.SelectedItem.Text + "'";
        table = this.objdata.RptEmployeeMultipleDetails(this.SSQL);
        this.txtexistingcode.Text = table.Rows[0]["MachineNo"].ToString();
        this.txtname.Text = table.Rows[0]["EmpName"].ToString();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        new DataTable();
        DataTable table = new DataTable();
        int num = 0;
        if (this.ddlMonths.SelectedValue == "January")
        {
            num = Convert.ToInt32(this.ddlFinance.SelectedValue) + 1;
        }
        else if (this.ddlMonths.SelectedValue == "February")
        {
            num = Convert.ToInt32(this.ddlFinance.SelectedValue) + 1;
        }
        else if (this.ddlMonths.SelectedValue == "March")
        {
            num = Convert.ToInt32(this.ddlFinance.SelectedValue) + 1;
        }
        else
        {
            num = Convert.ToInt32(this.ddlFinance.SelectedValue);
        }
        if (this.ddlempno.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "Window", "alert('Select EmpNo');", true);
        }
        if (this.ddlMonths.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "Window", "alert('Select  Month');", true);
        }
        if (this.txtFromDate.Text == "")
        {
            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "Window", "alert('Enter The From Date');", true);
        }
        if (this.txtToDate.Text == "")
        {
            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "Window", "alert('Enter To Date');", true);
        }
        if ((((!(this.ddlMonths.SelectedValue == "January") && !(this.ddlMonths.SelectedValue == "March")) && (!(this.ddlMonths.SelectedValue == "May") && !(this.ddlMonths.SelectedValue == "July"))) && ((!(this.ddlMonths.SelectedValue == "August") && !(this.ddlMonths.SelectedValue == "October")) && (!(this.ddlMonths.SelectedValue == "December") && !(this.ddlMonths.SelectedValue == "April")))) && ((!(this.ddlMonths.SelectedValue == "June") && !(this.ddlMonths.SelectedValue == "September")) && (!(this.ddlMonths.SelectedValue == "November") && (this.ddlMonths.SelectedValue == "February"))))
        {
            int num2 = Convert.ToInt32(this.ddlFinance.SelectedValue) + 1;
            int num1 = num2 % 4;
        }
        DataTable table2 = new DataTable();
        this.SSQL = "select * from Reeling_Mst where compcode='" + this.SessionCcode + "' and Loccode='" + this.SessionLcode + "'";
        table2 = this.objdata.RptEmployeeMultipleDetails(this.SSQL);
        if (table2.Rows.Count != 0)
        {
            this.Small = Convert.ToDecimal(table2.Rows[0]["Small"].ToString());
            this.medium = Convert.ToDecimal(table2.Rows[0]["Medium"].ToString());
            this.Large = Convert.ToDecimal(table2.Rows[0]["Large"].ToString());
            this.XL = Convert.ToDecimal(table2.Rows[0]["XL"].ToString());
            this.XXL = Convert.ToDecimal(table2.Rows[0]["XXL"].ToString());
        }
        if (this.txtSmall.Text == "")
        {
            this.txtSmall.Text = "0.0";
        }
        if (this.txtMedium.Text == "")
        {
            this.txtMedium.Text = "0.0";
        }
        if (this.txtLarge.Text == "")
        {
            this.txtLarge.Text = "0.0";
        }
        if (this.txtXL.Text == "")
        {
            this.txtXL.Text = "0.0";
        }
        if (this.txtXXL.Text == "")
        {
            this.txtXXL.Text = "0.0";
        }
        string str = "";
        DataTable table3 = new DataTable();
        DataTable table4 = new DataTable();
        string str2 = "";
        string str3 = "";
        table3 = this.objdata.Load_pf_details(this.SessionCcode, this.SessionLcode);
        str = Convert.ToDecimal(table3.Rows[0]["StaffSalary"].ToString()).ToString();
        str2 = Convert.ToDecimal(table3.Rows[0]["PF_per"].ToString()).ToString();
        str3 = Convert.ToDecimal(table3.Rows[0]["ESI_Per"].ToString()).ToString();
        table4 = this.objdata.EligibleESI_PF(this.SessionCcode, this.SessionLcode, this.txtexistingcode.Text);
        if (table4.Rows[0]["EligiblePF"].ToString() == "1")
        {
            decimal num6 = Convert.ToDecimal(str) * Convert.ToDecimal(this.txtWdays.Text);
            pfAmt = ((Convert.ToDecimal(num6.ToString()) * Convert.ToDecimal(str2)) / 100M).ToString();
            PFS = Math.Round(Convert.ToDecimal(pfAmt), 0, MidpointRounding.AwayFromZero).ToString();
        }
        else
        {
            PFS = "0";
        }
        if (table4.Rows[0]["ElgibleESI"].ToString() == "1")
        {
            decimal num9 = Convert.ToDecimal(str) * Convert.ToDecimal(this.txtWdays.Text);
            decimal num10 = (Convert.ToDecimal(num9.ToString()) * Convert.ToDecimal(str3)) / 100M;
            ESI = Math.Round(Convert.ToDecimal(num10.ToString()), 0, MidpointRounding.AwayFromZero).ToString();
        }
        else
        {
            ESI = "0";
        }
        this.finalsmall = Convert.ToDecimal(this.txtSmall.Text) * Convert.ToDecimal(this.Small);
        this.finalmedium = Convert.ToDecimal(this.txtMedium.Text) * Convert.ToDecimal(this.medium);
        this.finallarge = Convert.ToDecimal(this.txtLarge.Text) * Convert.ToDecimal(this.Large);
        this.finalxl = Convert.ToDecimal(this.txtXL.Text) * Convert.ToDecimal(this.XL);
        this.finalxxl = Convert.ToDecimal(this.txtXXL.Text) * Convert.ToDecimal(this.XXL);
        TotalEarnings = ((((Convert.ToDecimal(this.finalsmall) + Convert.ToDecimal(this.finalmedium)) + Convert.ToDecimal(this.finallarge)) + Convert.ToDecimal(this.finalxl)) + Convert.ToDecimal(this.finalxxl)).ToString();
        TotalDeductions = ((Convert.ToDecimal(PFS) + Convert.ToDecimal(ESI)) + Convert.ToDecimal(this.txtAdvance.Text)).ToString();
        NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();
        this.SSQL = "select * from EmployeeDetails where MachineNo='" + this.txtexistingcode.Text + "' and ExisistingCode='" + this.ddlempno.SelectedItem.Text + "' ";
        this.objdata.RptEmployeeMultipleDetails(this.SSQL);
        this.SSQL = "select * from SalaryDetails where MachineNo='" + this.txtexistingcode.Text + "' and ExisistingCode='" + this.ddlempno.SelectedItem.Text + "' and  ";
        this.SSQL = this.SSQL + " FromDate=convert(datetime,'" + this.txtFromDate.Text + "',103) and ToDate=convert(datetime,'" + this.txtToDate.Text + "',103)";
        if (this.objdata.RptEmployeeMultipleDetails(this.SSQL).Rows.Count > 0)
        {
            this.SSQL = "Delete SalaryDetails where MachineNo='" + this.txtexistingcode.Text + "' and ExisistingCode='" + this.ddlempno.SelectedItem.Text + "' and ";
            this.SSQL = this.SSQL + " FromDate=  convert(datetime,'" + this.txtFromDate.Text + "',103) and ToDate=convert(datetime,'" + this.txtToDate.Text + "',103) ";
            table = this.objdata.RptEmployeeMultipleDetails(this.SSQL);
        }
        this.SSQL = "Insert into SalaryDetails(EmpNo,MachineNo,ExisistingCode,allowances1,allowances2,allowances3,allowances4,allowances5,Deduction1,Deduction2,Deduction3,Deduction4,Deduction5,Month,FinancialYear,Year,FromDate,Todate,WagesType, ";
        this.SSQL = string.Concat(new object[] { this.SSQL, " Netpay,ProvidentFund,ESI,GrossEarnings,TotalDeductions,WorkedDays,Advance) Values('", this.txtexistingcode.Text, "','", this.txtexistingcode.Text, "','", this.ddlempno.SelectedItem.Text, "','", this.finalsmall, "', " });
        this.SSQL = string.Concat(new object[] { this.SSQL, " '", this.finalmedium, "','", this.finallarge, "','", this.finalxl, "','", this.finalxxl, "','", this.txtSmall.Text, "','", this.txtMedium.Text, "'," });
        this.SSQL = this.SSQL + " '" + this.txtLarge.Text + "','" + this.txtXL.Text + "','" + this.txtXXL.Text + "',";
        this.SSQL = string.Concat(new object[] { this.SSQL, "'", this.ddlMonths.SelectedItem.Text, "','", this.ddlFinance.SelectedValue, "','", num, "', " });
        this.SSQL = this.SSQL + " convert(datetime,'" + this.txtFromDate.Text + "',103),convert(datetime,'" + this.txtToDate.Text + "',103),'2','" + NetPay + "','" + PFS + "','" + ESI + "','" + TotalEarnings + "','" + TotalDeductions + "','" + this.txtWdays.Text + "','" + this.txtAdvance.Text + "') ";
        table = this.objdata.RptEmployeeMultipleDetails(this.SSQL);
        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Reeling Calculation Saved Successfully');", true);
        this.clear();
    }

    public void clear()
    {
        this.ddlempno.SelectedItem.Text = "-----Select----";
        this.ddlMonths.SelectedValue = "-----Select----";
        this.txtexistingcode.Text = "";
        this.txtname.Text = "";
        this.txtFromDate.Text = "";
        this.txtToDate.Text = "";
        this.txtSmall.Text = "";
        this.txtMedium.Text = "";
        this.txtXL.Text = "";
        this.txtXXL.Text = "";
        this.txtLarge.Text = "";
    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
    }

    protected void txtToDate_TextChanged(object sender, EventArgs e)
    {
        try
        {
            bool flag = false;
            if (this.txtFromDate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the From Date...');", true);
                flag = true;
            }
            if (!flag)
            {
                DateTime time = Convert.ToDateTime(this.txtFromDate.Text);
                DateTime time2 = Convert.ToDateTime(this.txtToDate.Text);
                if (time >= time2)
                {
                    ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Date Properly...');", true);
                    flag = true;
                    this.txtToDate.Text = null;
                }
                if (!flag)
                {
                    TimeSpan span = (TimeSpan)(time2 - time);
                    this.txtdays.Text = span.Days.ToString();
                }
            }
        }
        catch (Exception)
        {
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        base.Response.Redirect("AttenanceDownload.aspx");
    }

    protected void btnclr_Click(object sender, EventArgs e)
    {
        this.clear();
    }

    protected void btncontract_Click(object sender, EventArgs e)
    {
        base.Response.Redirect("ContractRPT.aspx");
    }

    protected void btnEmp_Click(object sender, EventArgs e)
    {
        base.Response.Redirect("RptEmpDownload.aspx");
    }

    protected void btnLeave_Click(object sender, EventArgs e)
    {
        base.Response.Redirect("RptLeaveSample.aspx");
    }

    protected void btnOT_Click(object sender, EventArgs e)
    {
        base.Response.Redirect("OTDownload.aspx");
    }

    protected void btnSal_Click(object sender, EventArgs e)
    {
        base.Response.Redirect("FrmDeductionDownload.aspx");
    }
}
