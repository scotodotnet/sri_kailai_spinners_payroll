﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;

public partial class AdministrationRights : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    BALDataAccess objdata = new BALDataAccess();
    UserCreationClass objUsercreation = new UserCreationClass();
    DataTable dtdDisplay = new DataTable();
    string utype;
    static int ss;
    string usert = "1";
    string SSQL;
    string Dept;

    string DashBoard;
    string masterpage;
    string UserCreation;
    string AdministrationRight;
    string EmployeeType;
    string PFandESImaster;
    string BankDetails;
    string DepartmentDetails;
    string IncentiveMaster;
    string BasicDetails;
    string OTPaymentDetails;
    string Employee;
    string EmployeeRegistration;
    string Profiles;
    string SalaryMaster;
    string SalaryDetails;
    string Advance;
    string Settlement;
    string EmployeeReactive;
    string Reports;
    string BonusProcess;
    string Upload;
    string AttendanceUpload;
    string EmployeeUpload;
    string OTUpload;
    string NewWagesUpload;
    string SalaryMasterUpload;
    string SchemeUpload;
    string ProfileUpload;
    string ESIandPf;
    string PFDownload;
   



    protected void Page_Load(object sender, EventArgs e)
    {
        //SessionUserType = Session["UserType"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        //SessionCompanyName = Session["CompanyName"].ToString();
        //SessionLocationName = Session["LocationName"].ToString();
        if (!IsPostBack)
        {
            //Dropdown_Company();
            //Dropdown_Location();
            Dropdown_UserName();
           
        }
    }


    //public void Dropdown_Company()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.Dropdown_Company();
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        ddlCompany.Items.Add(dt.Rows[i]["Cname"].ToString());
    //    }
    //}



    //public void Dropdown_Location()
    //{
    //    DataTable dt = new DataTable();
    //    dt = objdata.dropdown_ParticularLocation(SessionCcode, SessionLcode);
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        ddlLocation.Items.Add(dt.Rows[i]["LocName"].ToString());
    //    }
    // }

      public void Dropdown_UserName()
    {
        DataTable dt = new DataTable();
        dt = objdata.dropdown_UserName(SessionCcode, SessionLcode);
        ddlUserName.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["UserName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlUserName.Items.Add(dt.Rows[i]["UserName"].ToString());
        }
    }


      protected void btnSave_Click(object sender, EventArgs e)
      {
          //SSQL = "insert into Rights()values()";

          
          

          if (ChkDashBoard.Checked == true)
          {
              DashBoard = "1";             
              Dept = Dept + "1,";
          }
          else
          {
              DashBoard = "0";   
              Dept = Dept + "0,";
          }

          if (ChkMasterPage.Checked == true)
          {
              masterpage = "1";
              Dept = Dept + "1,";
          }
          else
          {
              masterpage = "0";
              Dept = Dept + "0,";
          }


          if (ChkUserCreation.Checked == true)
          {
              UserCreation = "1";
              Dept = Dept + "1,";
          }
          else
          {
              UserCreation = "0";
              Dept = Dept + "0,";
          }


          if (ChkAdministrationRights.Checked == true)
          {
              AdministrationRight = "1";
              Dept = Dept + "1,";

          }
          else
          {
              AdministrationRight = "0";
              Dept = Dept + "0,";
          }



          if (ChkEmployeeType.Checked == true)
          {
              EmployeeType = "1";
              Dept = Dept + "1,";
          }
          else
          {
              EmployeeType = "0";
              Dept = Dept + "0,";
          }



          if (ChkPFandESIMaster.Checked == true)
          {


              PFandESImaster = "1";
              Dept = Dept + "1,";
          }
          else
          {
              Dept = Dept + "0,";
              PFandESImaster = "0";
          }


          if (ChkBankDetails.Checked == true)
          {


              BankDetails = "1";
              Dept = Dept + "1,";
          }
          else
          {
              BankDetails = "0";
              Dept = Dept + "0,";
          }



          if (ChkDepartmentDetails.Checked == true)

              
          {
              DepartmentDetails = "1";
              Dept = Dept + "1,";
          }
          else
          {
              DepartmentDetails = "0";
              Dept = Dept + "0,";
          }


          if (ChkIncentiveMaster.Checked == true)
          {

              IncentiveMaster = "1";
              Dept = Dept + "1,";
          }
          else
          {
              IncentiveMaster = "0";
              Dept = Dept + "0,";
          }

          


          if (ChkBasicDetails.Checked == true)
          {
              BasicDetails = "1";
              Dept = Dept + "1,";
          }
          else
          {
              BasicDetails = "0";
              Dept = Dept + "0,";
          }

          
          
          if (ChkOTPaymentDetails.Checked == true)
          {
              OTPaymentDetails = "1";
              Dept = Dept + "1,";
          }
          else
          {
              OTPaymentDetails = "0";
              Dept = Dept + "0,";
          }


          

          if (ChkEmployee.Checked == true)
          {
              Employee = "1";
              Dept = Dept + "1,";
          }
          else
          {
              Employee = "0";
              Dept = Dept + "0,";
          }

          


          if (ChkEmployeeRegistration.Checked == true)
          {

              EmployeeRegistration = "1";
              Dept = Dept + "1,";
          }
          else
          {
              EmployeeRegistration = "0";
              Dept = Dept + "0,";
          }


          


          if (ChkProfile.Checked == true)
          {
               Profiles = "1"; 
               Dept = Dept + "1,";
          }
          else
          {
              Profiles = "0"; 
              Dept = Dept + "0,";
          }




          if (ChkSalaryMaster.Checked == true)
          {

              SalaryMaster = "1";
              Dept = Dept + "1,";
          }
          else
          {
              SalaryMaster = "0";
              Dept = Dept + "0,";
          }




          if (ChkSalaryDetails.Checked == true)
          {


              SalaryDetails = "1";
                Dept = Dept + "1,";
          }
          else
          {
              SalaryDetails = "0";
              Dept = Dept + "0,";
          }


          if (ChkAdvance.Checked == true)
          {
              Advance = "1";
              Dept = Dept + "1,";
          }
          else
          {
              Dept = Dept + "0,";
              Advance = "0";
          }




          if (ChkSettelment.Checked == true)
          {


              Settlement = "1";
              Dept = Dept + "1,";
          }
          else
          {
              Settlement = "0";
              Dept = Dept + "0,";
          }


          if (ChkEmployeeReactive.Checked == true)
          {

              EmployeeReactive = "1";
              Dept = Dept + "1,";
          }
          else
          {
              EmployeeReactive = "0";
              Dept = Dept + "0,";
          }



          if (ChkReports.Checked == true)
          {

              Reports = "1";
              Dept = Dept + "1,";
          }
          else
          {
              Reports = "0";
              Dept = Dept + "0,";
          }


          if (ChkBonusProcess.Checked == true)
          {

              BonusProcess = "1";
              Dept = Dept + "1,";
          }
          else
          {
              BonusProcess = "0";
              Dept = Dept + "0,";
          }



          if (ChkUpload.Checked == true)
          {
              Upload = "1";
              Dept = Dept + "1,";
          }
          else
          {
              Upload = "0";
              Dept = Dept + "0,";
          }
          //if (ChkPermissionDetails.Checked == true)
          //{
          //    PermissionDetail = "1";
          //    Dept = Dept + "1,";
          //}
          //else
          //{
          //    PermissionDetail = "0";
          //    Dept = Dept + "0,";
          //}




          if (ChkAttendanceUpload.Checked == true)
          {
              AttendanceUpload = "1";
              Dept = Dept + "1,";
          }
          else
          {
              AttendanceUpload = "0";
              Dept = Dept + "0,";
          }


          if (ChkEmployeeUpload.Checked == true)
          {
              EmployeeUpload = "1";
              Dept = Dept + "1,";
          }
          else
          {
              EmployeeUpload = "0";
              Dept = Dept + "0,";
          }


          if (ChkOTUpload.Checked == true)
          {
              OTUpload = "1";
              Dept = Dept + "1,";
          }
          else
          {
              OTUpload = "0";
              Dept = Dept + "0,";
          }

          if (ChkNewWagesUpload.Checked == true)
          {
              NewWagesUpload = "1";
              Dept = Dept + "1,";
          }
          else
          {
              NewWagesUpload = "0";
              Dept = Dept + "0,";
          }


          if (ChkSalaryMasterUpload.Checked == true)
          {
              SalaryMasterUpload = "1";
              Dept = Dept + "1,";
          }
          else
          {
              SalaryMasterUpload = "0";
              Dept = Dept + "0,";
          }


          if (ChkSchemeUpload.Checked == true)
          {
              SchemeUpload = "1";
              Dept = Dept + "1,";
          }
          else
          {
              SchemeUpload = "0";
              Dept = Dept + "0,";
          }


          if (ChkProfileUpload.Checked == true)
          {
              ProfileUpload = "1";
              Dept = Dept + "1,";
          }
          else
          {
              ProfileUpload = "0";
              Dept = Dept + "0";
          }

          if (ChkESIandPF.Checked == true)
          {
              ESIandPf = "1";
              Dept = Dept + "1,";
          }
          else
          {
              ESIandPf = "0";
              Dept = Dept + "0";
          }

          if (ChkPFDownload.Checked == true)
          {
              PFDownload = "1";
              Dept = Dept + "1,";
          }
          else
          {
              PFDownload = "0";
              Dept = Dept + "0";
          }

          bool ErrFlag = false;



          if (Dept.ToString() == "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0")
          {
              ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Choose Your Rights');", true);
              ErrFlag = true;
          }

          else if(ddlUserName.SelectedItem.Text == "- select -")
          {
              ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select UserName');", true);
              ErrFlag = true;
          }
          else
          {
              DataTable dtd = new DataTable();
              dtd = objdata.CheckAdminRights(SessionCcode, SessionLcode, ddlUserName.SelectedItem.Text);
              if (dtd.Rows.Count > 0)
              {
                  ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Already Exist');", true);
                  ErrFlag = true;
                  Clear();
              }
              else
              {
                  //EmployeeDetails,LeaveMaster,PermissionMaster,GraceTime

                  SSQL = "insert into Rights(CompCode,LocCode,Username,DashBoard,masterpage,UserCreation, ";
                  SSQL = SSQL + "AdminRights,EmployeeType,PFandESImaster,BankDetails, ";
                  SSQL = SSQL + "Department,";
                  SSQL = SSQL + "IncentiveMaster,BasicDetails,OvertimePayment,PermissionMaster,EmployeeRegistration,Profile,"; // permission master == employee
                  SSQL = SSQL + "SalaryMaster,SalaryDetails,Advance,Settelment,EmployeeReactive,Reports,";
                  SSQL = SSQL + "BonusProcess,Upload,AttendanceUpload,EmployeeUpload,OTUpload,";
                  SSQL = SSQL + "NewWagesUpload,SalaryMasterUpload,SchemeUpload,ProfileUpload,ESIandPFupload,PFDownload)";
                  SSQL = SSQL + "values('" + SessionCcode.ToString() + "','" + SessionLcode.ToString() + "','" + ddlUserName.SelectedItem.Text + "',";
                  SSQL = SSQL + "'" + DashBoard + "','" + masterpage + "','" + UserCreation + "','" + AdministrationRight + "',";
                  SSQL = SSQL + "'" + EmployeeType + "','" + PFandESImaster + "','" + BankDetails + "','" + DepartmentDetails + "',";
                  SSQL = SSQL + "'" + IncentiveMaster + "','" + BasicDetails + "','" + OTPaymentDetails + "','" + Employee + "','"+ EmployeeRegistration +"',";
                  SSQL = SSQL + "'" + Profiles + "','" + SalaryMaster + "','" + SalaryDetails + "','" + Advance + "','" + Settlement + "',";
                  SSQL = SSQL + "'" + EmployeeReactive + "','" + Reports + "','" + BonusProcess + "','" + Upload + "','" + AttendanceUpload + "',";
                  SSQL = SSQL + "'" + EmployeeUpload + "','" + OTUpload + "','" + NewWagesUpload + "','" + SalaryMasterUpload + "','" + SchemeUpload + "',";
                  SSQL = SSQL + "'" + ProfileUpload + "','"+ ESIandPf +"','" + PFDownload +"')";

                  objdata.ReturnMultipleValue(SSQL);

                  //objUsercreation.Ccode = SessionCcode.ToString();
                  //objUsercreation.Lcode = SessionLcode.ToString();
                  //objUsercreation.UserCode = ddlUserName.SelectedItem.Text;
                  //objUsercreation.Data = Dept.ToString();
                  //objUsercreation.AttendanceLogClear = Attn_Log_Clear.ToString();
                  //objUsercreation.ManualattendanceCheck = Man_Attn_Perm.ToString();
                  //objUsercreation.ManAttnUpload = Man_Attn_Upload_Perm.ToString();
                  //objUsercreation.EmpApproval = Employee_Approval_Perm.ToString();
                  //objUsercreation.EmpStatus = Employee_Status_Perm.ToString();
                  //objUsercreation.Emp_Mst_Data_Perm = Emp_Mst_Data_Perm.ToString();
                  //objdata.AdministrationRegistration(objUsercreation);
                  ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Save sucessFully');", true);

              }
          }
      }

      protected void ddlUserName_SelectedIndexChanged(object sender, EventArgs e)
      {
          Clear();

          DataTable dtd = new DataTable();
          SSQL = "select * from Rights where Username='" + ddlUserName.SelectedItem.Text + "' and CompCode = '" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
          dtd = objdata.ReturnMultipleValue(SSQL);
          if (dtd.Rows.Count > 0)
          {
              DashBoard = dtd.Rows[0]["DashBoard"].ToString();
              if (DashBoard.ToString() == "1")
              {
                  ChkDashBoard.Checked = true;
              }


              masterpage = dtd.Rows[0]["masterpage"].ToString();
              if (masterpage.ToString() == "1")
              {
                  ChkMasterPage.Checked = true;
              }



              UserCreation = dtd.Rows[0]["UserCreation"].ToString();
              if (UserCreation.ToString() == "1")
              {
                  ChkUserCreation.Checked = true;
              }



              AdministrationRight = dtd.Rows[0]["AdminRights"].ToString();

              if (AdministrationRight.ToString() == "1")
              {
                  ChkAdministrationRights.Checked = true;
              }



              EmployeeType = dtd.Rows[0]["EmployeeType"].ToString();

              if (EmployeeType.ToString() == "1")
              {
                  ChkEmployeeType.Checked = true;
              }

              UserCreation = dtd.Rows[0]["UserCreation"].ToString();

              if (UserCreation.ToString() == "1")
              {
                  ChkUserCreation.Checked = true;
              }



              PFandESImaster = dtd.Rows[0]["PFandESImaster"].ToString();

              if (PFandESImaster.ToString() == "1")
              {
                  ChkPFandESIMaster.Checked = true;
              }



              BankDetails = dtd.Rows[0]["BankDetails"].ToString();

              if (BankDetails.ToString() == "1")
              {
                  ChkBankDetails.Checked = true;
              }



              DepartmentDetails = dtd.Rows[0]["Department"].ToString();

              if (DepartmentDetails.ToString() == "1")
              {
                  ChkDepartmentDetails.Checked = true;
              }


              IncentiveMaster = dtd.Rows[0]["IncentiveMaster"].ToString();

              if (IncentiveMaster.ToString() == "1")
              {
                  ChkIncentiveMaster.Checked = true;
              }



              BasicDetails = dtd.Rows[0]["BasicDetails"].ToString();

              if (BasicDetails.ToString() == "1")
              {
                  ChkBasicDetails.Checked = true;
              }


              OTPaymentDetails = dtd.Rows[0]["OvertimePayment"].ToString();

              if (OTPaymentDetails.ToString() == "1")
              {
                  ChkOTPaymentDetails.Checked = true;
              }

              Employee = dtd.Rows[0]["PermissionMaster"].ToString();

              if (Employee.ToString() == "1")
              {
                  ChkEmployee.Checked = true;
              }



              EmployeeRegistration = dtd.Rows[0]["EmployeeRegistration"].ToString();

              if (EmployeeRegistration.ToString() == "1")
              {
                  ChkEmployeeRegistration.Checked = true;
              }



              Profiles = dtd.Rows[0]["Profile"].ToString();

              if (Profiles.ToString() == "1")
              {
                  ChkProfile.Checked = true;
              }

              SalaryMaster = dtd.Rows[0]["SalaryMaster"].ToString();

              if (SalaryMaster.ToString() == "1")
              {
                  ChkSalaryMaster.Checked = true;
              }

              SalaryDetails = dtd.Rows[0]["SalaryDetails"].ToString();

              if (SalaryDetails.ToString() == "1")
              {
                  ChkSalaryDetails.Checked = true;
              }


              Advance = dtd.Rows[0]["Advance"].ToString();

              if (Advance.ToString() == "1")
              {
                  ChkAdvance.Checked = true;
              }



              Settlement = dtd.Rows[0]["Settelment"].ToString();

              if (Settlement.ToString() == "1")
              {
                  ChkSettelment.Checked = true;
              }



              EmployeeReactive = dtd.Rows[0]["EmployeeReactive"].ToString();

              if (EmployeeReactive.ToString() == "1")
              {
                  ChkEmployeeReactive.Checked = true;
              }



              Reports = dtd.Rows[0]["Reports"].ToString();

              if (Reports.ToString() == "1")
              {
                  ChkReports.Checked = true;
              }



              BonusProcess = dtd.Rows[0]["BonusProcess"].ToString();

              if (BonusProcess.ToString() == "1")
              {
                  ChkBonusProcess.Checked = true;
              }

              Upload = dtd.Rows[0]["Upload"].ToString();

              if (Upload.ToString() == "1")
              {
                  ChkUpload.Checked = true;
              }


              AttendanceUpload = dtd.Rows[0]["AttendanceUpload"].ToString();

              if (AttendanceUpload.ToString() == "1")
              {
                  ChkAttendanceUpload.Checked = true;
              }



              EmployeeUpload = dtd.Rows[0]["EmployeeUpload"].ToString();

              if (EmployeeUpload.ToString() == "1")
              {
                  ChkEmployeeUpload.Checked = true;
              }



              OTUpload = dtd.Rows[0]["OTUpload"].ToString();

              if (OTUpload.ToString() == "1")
              {
                  ChkOTUpload.Checked = true;
              }


              NewWagesUpload = dtd.Rows[0]["NewWagesUpload"].ToString();

              if (NewWagesUpload.ToString() == "1")
              {
                  ChkNewWagesUpload.Checked = true;
              }

              SalaryMasterUpload = dtd.Rows[0]["SalaryMasterUpload"].ToString();

              if (SalaryMasterUpload.ToString() == "1")
              {
                  ChkSalaryMasterUpload.Checked = true;
              }


              SchemeUpload = dtd.Rows[0]["SchemeUpload"].ToString();

              if (SchemeUpload.ToString() == "1")
              {
                  ChkSchemeUpload.Checked = true;
              }



              ProfileUpload = dtd.Rows[0]["ProfileUpload"].ToString();

              if (ProfileUpload.ToString() == "1")
              {
                  ChkProfileUpload.Checked = true;
              }


              ESIandPf = dtd.Rows[0]["ESIandPFupload"].ToString();

              if (ESIandPf.ToString() == "1")
              {
                  ChkESIandPF.Checked = true;
              }

              PFDownload = dtd.Rows[0]["PFDownload"].ToString();

              if (PFDownload.ToString() == "1")
              {
                  ChkPFDownload.Checked = true;
              }





              //string ar = dtdAdmin.Rows[0]["SchemeUpload"].ToString();
              //string[] arr = ar.Split(',');

              //if (arr[0].ToString() == "1")
              //{
              //    ChkBoxAdminRights.Checked = true;
              //}
              //if (arr[1].ToString() == "1")
              //{
              //    ChkBoxUserCreation.Checked = true;
              //}
              //if (arr[2].ToString() == "1")
              //{
              //    ChkBoxNewEmployee.Checked = true;
              //}
              //if (arr[3].ToString() == "1")
              //{
              //    ChkBoxEmployeeDetails.Checked = true;
              //}
              //if (arr[4].ToString() == "1")
              //{
              //    ChkBoxDownloadClear.Checked = true;
              //}
              //if (arr[5].ToString() == "1")
              //{
              //    ChkBoxEmployeeApproval.Checked = true;
              //}
              //if (arr[6].ToString() == "1")
              //{
              //    ChkBoxEmployeeStatus.Checked = true;
              //}
              //if (arr[7].ToString() == "1")
              //{
              //    ChkBoxManualAttendance.Checked = true;
              //}
              //if (arr[8].ToString() == "1")
              //{
              //    ChkBoxdeductionAndOT.Checked = true;
              //}
              //if (arr[9].ToString() == "1")
              //{
              //    ChkBoxDepartmentIncentive.Checked = true;
              //}
              //if (arr[10].ToString() == "1")
              //{
              //    ChkBoxSalaryCoverPhoto.Checked = true;
              //}
              //if (arr[11].ToString() == "1")
              //{
              //    ChkBoxSalaryDisbursement.Checked = true;
              //}
              //if (arr[12].ToString() == "1")
              //{
              //    ChkBoxTimeDelete.Checked = true;
              //}
              //if (arr[13].ToString() == "1")
              //{
              //    ChkBoxLeaveDetails.Checked = true;
              //}
              //if (arr[14].ToString() == "1")
              //{
              //    ChkBoxReport.Checked = true;
              //}
          }
      }

      protected void btnCancel_Click(object sender, EventArgs e)
      {
          Clear();
      }

      public void Clear()
      {
          ChkAdministrationRights.Checked = false;
          ChkAdvance.Checked = false;
          ChkBankDetails.Checked = false;
          ChkBasicDetails.Checked = false;
          ChkBonusProcess.Checked = false;
          ChkDashBoard.Checked = false;
          ChkDepartmentDetails.Checked = false;
          ChkEmployee.Checked = false;
          ChkEmployeeReactive.Checked = false;
          ChkEmployeeRegistration.Checked = false;
          ChkEmployeeType.Checked = false;
          ChkEmployeeUpload.Checked = false;
          ChkESIandPF.Checked = false;
          ChkIncentiveMaster.Checked = false;
          ChkMasterPage.Checked = false;
          ChkNewWagesUpload.Checked = false;
          ChkOTPaymentDetails.Checked = false;
          ChkOTUpload.Checked = false;
          ChkPFandESIMaster.Checked = false;
          ChkPFDownload.Checked = false;
          ChkProfile.Checked = false;
          ChkProfileUpload.Checked = false;
          ChkReports.Checked = false;
          ChkSalaryDetails.Checked = false;
          ChkSalaryMaster.Checked = false;
          ChkSalaryMasterUpload.Checked = false;
          ChkSchemeUpload.Checked = false;
          ChkSettelment.Checked = false;
          ChkUpload.Checked = false;
          ChkUserCreation.Checked = false;
          ChkAttendanceUpload.Checked = false;





          //ddlUserName.SelectedIndex = 0;
          ////ChkBoxAdminRights.Checked = false;
          //ChkAdministrationRights.Checked = false;
          //ChkDashBoard.Checked = false;
          //ChkDeduction.Checked = false;
          //ChkDepartmentDetails.Checked = false;
          //ChkDesginationDetails.Checked = false;

          //ChkDownloadClear.Checked = false;
          //ChkEarlyOutMaster.Checked = false;
          //ChkEmployee.Checked = false;
          //ChkEmployeeApproval.Checked = false;
          //ChkEmployeeDetails.Checked = false;

          //ChkEmployeeStatus.Checked = false;
          //ChkEmployeeTypeDetails.Checked = false;
          //ChkGraceTime.Checked = false;
          //ChkIncentive.Checked = false;
          //ChkLateINMaster.Checked = false;

          //ChkLeaveDetails.Checked = false;
          //ChkLeaveManagement.Checked = false;
          //ChkLeaveMaster.Checked = false;
          //ChkManualAttendance.Checked = false;
          //ChkManualShift.Checked = false;

          //ChkManualShiftDetails.Checked = false;
          //ChkMasterPage.Checked = false;
          //ChkPermissionDetails.Checked = false;
          //ChkPermissionMaster.Checked = false;
          //ChkReports.Checked = false;

          //ChkTimeDelete.Checked = false;
          //ChkUploadDownload.Checked = false;
          //ChkUserCreation.Checked = false;
          //ChkWagesTypeDetails.Checked = false;
      }
}
