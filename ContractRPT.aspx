﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ContractRPT.aspx.cs" Inherits="ContractRPT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Contract Report</li></h4> 
                    </ol>
             </div>

<div id="main-wrapper" class="container">
<div class="row">
<div class="col-md-12">
              
<div class="col-md-9">
<div class="panel panel-white">
<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Contract Report</h4>
				</div>
				</div>
<form class="form-horizontal">
<div class="panel-body">
<div class="col-md-12">     
<div class="form-group row">
				       <asp:Label ID="lblContractType" runat="server" class="col-sm-2 control-label" 
                                                             Text="Contract Type"></asp:Label>
				   
					   <div class="col-sm-5">
					     <asp:RadioButtonList ID="rbcontract" runat="server" RepeatColumns="3" 
                                                        Font-Bold="true" AutoPostBack="true" 
                                                        onselectedindexchanged="rbcontract_SelectedIndexChanged" >
                         <asp:ListItem Text="Years" Value="1" ></asp:ListItem>
                         <asp:ListItem Text="Months" Value="2"></asp:ListItem>
                         <asp:ListItem Text="Days" Value="3"></asp:ListItem>
                         </asp:RadioButtonList> 
					   </div>
				       <div class="col-md-1"></div>
				       
					   </div>
				        
                       </div>
<div class="col-md-12">     
<div class="form-group row">
	<div class="col-sm-3"></div>
	<div class="col-sm-3">
    <asp:Button ID="Button1" class="btn btn-success" runat="server" Text="Export" onclick="Button1_Click"/>
	</div>
	<div class="col-sm-3">
	<asp:Button ID="btnclear" class="btn btn-success" runat="server" Text="Completed Report" onclick="btnclear_Click"/>
	</div>
	</div>
	</div>
	
<div class="col-md-12">
	<div class="form-group row">
	<table>
	<tbody>
	<tr>
	<td>
	<asp:Panel ID="panelyear" runat="server" Visible="false" >
                                                <asp:GridView ID="gvReport" runat="server" AutoGenerateColumns="false" AllowPaging="false" >
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Si. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Employee No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Employee Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Token No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblToken" runat="server" Text='<%# Eval("ExisistingCode") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Contract Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblConName" runat="server" Text='<%# Eval("ContractName") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Planned Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblplanned" runat="server" Text='<%# Eval("PlannedDays") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Remaining Days</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBalance" runat="server" Text='<%# Eval("BalanceDays") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>StartingDate</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStart" runat="server" Text='<%# Eval("StartingDate") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                </asp:Panel>
  </td>
  </tr>
  <tr>
	<td>
	<asp:Panel ID="panelMonths" runat="server" Visible="false" >
                                                <asp:GridView ID="gvContract1" runat="server" AutoGenerateColumns="false" AllowPaging="false" >
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Si. No</HeaderTemplate>
                                                            <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Employee No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmpNo1" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Employee Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmpName1" runat="server" Text='<%# Eval("EmpName") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Token No</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblToken1" runat="server" Text='<%# Eval("ExisistingCode") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Contract Name</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblConName1" runat="server" Text='<%# Eval("ContractName") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Planned Months</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblplanned1" runat="server" Text='<%# Eval("Months") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>Remaining Months</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBalance1" runat="server" Text='<%# Eval("DecMonth") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>StartingDate</HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStart1" runat="server" Text='<%# Eval("StartingDate") %>' ></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                </asp:Panel>
	</td>
	</tr>
	</tbody>
	
	</table>
	
	
	</div>
	</div>
	
	</div>
	</form>
	</div>
	</div>
	
	<!-- Dashboard start -->
 	
 	<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
 
    <!-- Dashboard End --> 
    
    </div>  <!-- col-md-12 End --> 
    </div>  <!-- row End -->
    </div>
    </ContentTemplate>
    
    <Triggers>
      <asp:PostBackTrigger ControlID="Button1"  />
 </Triggers>
     <Triggers>
      <asp:PostBackTrigger ControlID="btnclear"  />
 </Triggers>
    
    </asp:UpdatePanel>
			
</asp:Content>

