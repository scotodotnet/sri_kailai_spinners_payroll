﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class ViewReport : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string str_cate;
    string str_dept;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate;
    string ToDate;
    string salaryType;
    string Emp_ESI_Code;

    string EmployeeType;
    string PayslipType;

    string PFTypeGet;

    string Left_Employee = "0";
    string Left_Date = "";
    string Get_Report_Type = "0";
    string Salary_CashOrBank;
    protected void Page_Load(object sender, EventArgs e)
    {
        //ReportDocument rd = new ReportDocument();
        SqlConnection con = new SqlConnection(constr);
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        str_cate = Request.QueryString["Cate"].ToString();
        //str_dept = Request.QueryString["Depat"].ToString();
        str_month = Request.QueryString["Months"].ToString();
        str_yr = Request.QueryString["yr"].ToString();
        //fromdate = Request.QueryString["fromdate"].ToString();
        //ToDate = Request.QueryString["ToDate"].ToString();
        salaryType = Request.QueryString["Salary"].ToString();
        EmployeeType = Request.QueryString["EmpType"].ToString();
       // PayslipType = Request.QueryString["PayslipType"].ToString();
        int YR = 0;
        string report_head = "";
        if (str_month == "January")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "February")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "March")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(str_yr);
        }
        if (salaryType == "2")
        {
            report_head = "PAYSLIP FOR THE MONTH OF " + str_month.ToUpper() + " " + YR.ToString();
        }
        else
        {
            report_head = "PAYSLIP FOR THE MONTH OF " + fromdate.ToString() + " - " + ToDate.ToString();
        }


        salaryType = Request.QueryString["Salary"].ToString();
        DataTable dt = new DataTable();
        dt = objdata.Company_retrive(SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {
            CmpName = dt.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        }
       // Get_Report_Type = Request.QueryString["Report_Type"].ToString();
             


                //query = "select ED.EmpNo,ED.ExisistingCode,ED.EmpName,ED.FatherName,SD.Month,convert(varchar,SD.FromDate,103) as FromDate,";
                //query = query + " left(DATENAME(month,DATEADD(mm,1,SD.FromDate)),3) + '-' + right(Year(DATEADD(mm,1,SD.FromDate)),2) as Month_Str,";
                //query = query + " cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,cast(SD.BasicHRA as decimal(18,2)) as BasicHRA,";
                //query = query + " cast(SD.ConvAllow as decimal(18,2)) as ConvAllow,cast(SD.EduAllow as decimal(18,2)) as EduAllow,";
                //query = query + " cast(SD.MediAllow as decimal(18,2)) as MediAllow,cast(SD.BasicRAI as decimal(18,2)) as BasicRAI,";
                //query = query + " cast(SD.WashingAllow as decimal(18,2)) as WashingAllow,SD.ProvidentFund,SD.EmployeerPFone,";
                //query = query + " SD.EmployeerPFTwo,OP.PFnumber,ED.EmployeeType";
                //query = query + " from EmployeeDetails ED inner join SalaryDetails SD on ED.EmpNo=SD.EmpNo";
                //query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "'";
                //query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                //query = query + " And SD.fromdate >= convert(datetime,'" + fromdate + "', 105) And SD.fromdate <= convert(datetime,'" + ToDate + "', 105)";
                //query = query + " And ED.EmployeeType='" + EmployeeType + "' And OP.EligiblePF='1'";
                //query = query + " Order by ED.ExisistingCode,SD.FromDate Asc";

        query = "select SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName) as Name,EmpDet.Designation,EmpDet.Department,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.ProvidentFund as PF,SalDet.ESI," +
                               " SalDet.Totalworkingdays,SalDet.NFh,SalDet.Advance,SalDet.Stamp,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.WorkedDays,SalDet.Month," +
                               " SalDet.Year,SalDet.SalaryDate,SalDet.allowances1,(SalDet.GrossEarnings) as GrossAmt," +
                               " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Weekoff,SalDet.Deduction2," +
                               " SalDet.Fixed_Work_Days,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                               " SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew,SalDet.OverTime,MstDpt.DepartmentNm," +
                               " SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
                               " SalDet.DayIncentive,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                               " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo" +
                               "  inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where" +
                               "  SalDet.Month='" + str_month + "'   AND SalDet.FinancialYear='" + YR.ToString() + "' and AD.Months='" + str_month + "' AND AD.FinancialYear='" + YR.ToString() + "' and EmpDet.StafforLabor='" + str_cate + "' and " +
                               " EmpDet.EmployeeType='" + EmployeeType + "' and EmpDet.Ccode='" + SessionCcode + "' and OP.WagesType='2' and EmpDet.Lcode='" + SessionLcode + "' group by  " +
                               " SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.BasicAndDANew,  SalDet.NFh,SalDet.Advance,SalDet.Stamp,SalDet.WorkedDays,SalDet.Month," +
                               " SalDet.Year,SalDet.SalaryDate,SalDet.allowances1,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.BasicHRA," +
                               " SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.ProvidentFund ,SalDet.ESI," +
                               " SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew, SalDet.OverTime,MstDpt.DepartmentNm,SalDet.WorkedDays,SM.Base,SalDet.DedOthers1,SalDet.DedOthers2," +
                               " SalDet.DayIncentive,EmpDet.Initial,OP.Dateofjoining,SalDet.Fixed_Work_Days order by EmpDet.ExisistingCode asc";





                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds1 = new DataSet();
                con.Open();
                sda.Fill(ds1);
                DataTable dt_1 = new DataTable();
                sda.Fill(dt_1);
                con.Close();

                if (ds1.Tables[0].Rows.Count != 0)
                {

                    //rd.Load(Server.MapPath("Payslip/permanent_Staff.rpt"));
                    //rd.SetDataSource(ds1.Tables[0]);

                    //rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                    //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                    ////rd.DataDefinition.FormulaFields["From_Date_Str"].Text = "'" + fromdate + "'";
                    ////rd.DataDefinition.FormulaFields["To_Date_Str"].Text = "'" + ToDate + "'";

                    //rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "" + "'";
                    //rd.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    //CrystalReportViewer1.ReportSource = rd;
                    ////CrystalReportViewer1.RefreshReport();
                    //CrystalReportViewer1.DataBind();


                    
                    rd.Load(Server.MapPath("Payslip/permanent_Staff.rpt"));
                    rd.DataDefinition.FormulaFields["PayMonth"].Text = "'" + str_month + "'";
                    rd.DataDefinition.FormulaFields["year"].Text = "'" + YR + "'";
                    rd.Database.Tables[0].SetDataSource(ds1.Tables[0]);
                    CrystalReportViewer1.ReportSource = rd;
                   // rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                   
                  


                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
                }
            
            
       
    }
    
}