﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PaySlipGener.aspx.cs" Inherits="PaySlipGener" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>

          <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">PAY SLIP REPORT</li>
                       <h4>
                       </h4>
                        <h4>
                       </h4>
                        <h4>
                       </h4>
                        </h4> 
                    </ol>
              </div>
    <div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
    <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">PaySlip Report</h4>
				</div>
				</div>
				<form class="form-horizontal">
				   <div align="center">
				        <div class="form-group>
					       <asp:Label ID="Label1" for="input-Default" class="col-sm-12 control-label" 
                                Text="WAGES TYPE" runat="server" Font-Bold="True" Font-Underline="True" Font-Size=Large></asp:Label>
				        
				        </div>
				   </div>
				
				<div class="panel-body">
                        
                <div class="row">
					    <div class="form-group col-md-12">
					      
					    <div align="center">
					    
					     <div class="form-group col-md-2"></div>
					     <div class="form-group col-md-9">
					         <asp:RadioButtonList ID="rbsalary" runat="server" AutoPostBack="true" 
                              RepeatColumns="3" onselectedindexchanged="rbsalary_SelectedIndexChanged">
                              <asp:ListItem Text ="Weekly Wages" Value="1"></asp:ListItem>
                              <asp:ListItem Text="Monthly Wages" Value="2" ></asp:ListItem>                                                                                                                                       
                              <asp:ListItem Text="Bi-Monthly Wages" Value="3"></asp:ListItem>
                            </asp:RadioButtonList>
                            
					        <%--<div class="form-group col-md-3">
                            <asp:RadioButton ID="RadioButton1" runat="server" Text="WeeklyWages"/>
                            </div>
                            <div class="form-group col-md-3">
                            <asp:RadioButton ID="RadioButton2" runat="server" Text="MonthlyWages" />
                            </div>
                            <div class="form-group col-md-3">
                            <asp:RadioButton ID="RadioButton3" runat="server" Text="Bi-Monthly Wages"/>
                         </div>   --%>
                         </div>
				            </div>
                     </div>
                </div>
                      <asp:Panel ID="pnlMonth" runat="server" Visible="false" >
                           <div class="form-group row">
					  <asp:Label ID="Label2" runat="server" class="col-sm-2 control-label" Text="From Date"></asp:Label>
						<div class="col-sm-3">
                        <asp:TextBox ID="txtFromDate" runat="server"  class="form-control"></asp:TextBox>
						<cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromDate" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true">
                        </cc1:CalendarExtender>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR1" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtFromDate" ValidChars="0123456789/- ">
                        </cc1:FilteredTextBoxExtender>     
                        </div>
					
					 <div class="col-sm-1"></div>
				    <asp:Label ID="Label4" runat="server" class="col-sm-2 control-label"  Text="To Date" ></asp:Label>		    
				    <div class="col-sm-3">  
                        <asp:TextBox ID="txtToDate" runat="server"  class="form-control"></asp:TextBox>
                        
                           <cc1:CalendarExtender ID="Caleder_txtdob" runat="server" TargetControlID="txtToDate" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true">
                        </cc1:CalendarExtender>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR2" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtToDate" ValidChars="0123456789/- ">
                        </cc1:FilteredTextBoxExtender>
					  </div>
			       </div>
                       </asp:Panel>    
				      <div class="form-group row">
					  <asp:Label ID="Label3" runat="server" class="col-sm-2 control-label" Text="Category"></asp:Label>
						<div class="col-sm-3">
						    <asp:DropDownList ID="ddlcategory" runat="server" AutoPostBack="true" class="form-control"
                            OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged">
                           </asp:DropDownList>            
                        </div>
					 <div class="col-sm-1"></div>
				    <asp:Label ID="lblEmployeeType" runat="server" class="col-sm-2 control-label"  Text="Employee Type" ></asp:Label>		    
				    <div class="col-sm-3">  
                    <asp:DropDownList ID="ddlEmployeeType" runat="server"  class="form-control">
                   </asp:DropDownList>
					  </div>
			       </div>
			       
					  <%--<div class="form-group row">
					   <asp:Label ID="lblDeparmtent" runat="server" class="col-sm-2 control-label"  Text="Department" ></asp:Label>
						<div class="col-sm-3">
                           <asp:DropDownList ID="ddldept" runat="server" AutoPostBack="true" class="form-control" OnSelectedIndexChanged="ddldept_SelectedIndexChanged">
                           </asp:DropDownList>
                        </div>
					     <div class="col-sm-1"></div>
					   <asp:Label ID="lblSalaryThrough" runat="server" class="col-sm-2 control-label"  Text="Salary Through"></asp:Label>
					   	<div class="col-sm-3">
						 <asp:RadioButtonList ID="RdbCashBank" runat="server" RepeatColumns="3" 
                           TabIndex="3" Width="180">
                           <asp:ListItem Selected="true" Text="ALL" Value="0"></asp:ListItem>
                           <asp:ListItem Selected="False" Text="Cash" Value="1"></asp:ListItem>
                           <asp:ListItem Selected="False" Text="Bank" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
						</div>
				        </div>--%>
				        
				      <div class="form-group row">
					        <asp:Label ID="lblMonth" runat="server" class="col-sm-2 control-label"  Text="Month"></asp:Label>
						<div class="col-sm-3">
                           <asp:DropDownList ID="ddlMonths" runat="server" class="form-control">
                           </asp:DropDownList>
                        </div>
                         <div class="col-sm-1"></div>
                          <asp:Label ID="lblfinance" runat="server" class="col-sm-2 control-label"  Text="Financial Year" ></asp:Label>
                   
                          <div class="col-sm-3">
                           <asp:DropDownList ID="ddlFinance" runat="server" AutoPostBack="true" class="form-control">
                           </asp:DropDownList>
                        </div>
                        </div>
				    <%--  <div class="form-group row">
				       <div class="col-sm-2"></div>
				        <div class="col-sm-2">
                           <asp:Button ID="btnSearch" class="btn btn-success" runat="server" Text="PaySlip" OnClick="btnSearch_Click"/>
				       </div>
				        <div class="col-sm-1"></div>
				          <div class="col-sm-1"></div>
				         </div>
				         <div class="form-group row">
				       <div class="col-sm-2"></div>
				        <div class="col-sm-2">
                           <asp:Button ID="btnSalary" class="btn btn-success" runat="server" Text="Salary" 
                                onclick="btnSalary_Click"/>
				       </div>
				        <div class="col-sm-1"></div>
				          <div class="col-sm-1"></div>
				          
				       <asp:Button ID="btnStaffPayslip" class="btn btn-success" runat="server" 
                                 Text="Staff_Payslip" onclick="btnStaffPayslip_Click"/>
				         
				         </div>--%>
				         
				         
				           <div class="form-group row">
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-2">
                                                <asp:Button ID="btnSearch" class="btn btn-success" runat="server" Text="PaySlip" OnClick="btnSearch_Click" />
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-1"></div>



                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-2">
                                                <asp:Button ID="btnSalary" class="btn btn-success" runat="server" Visible="false" Text="Salary"
                                                    OnClick="btnSalary_Click" />
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-1"></div>

                                            <asp:Button ID="btnStaffPayslip" class="btn btn-success" runat="server"
                                                Text="Staff_Payslip" OnClick="btnStaffPayslip_Click" Visible="false" />

                                        </div>
				         
			        </div>    
			</form>
			
			</div>
			</div>
	  
		    <div>
                                    <table class="full">
                                        <thead>
                                         <!--  BANK SALARY Details 
                                         
                                         
                                         View End  -->
                                            <tr id="BankSalary" runat="server" visible="false"> 
                                                <td colspan="4">
                                                    <asp:GridView ID="StaffGridView" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>S. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Token No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>PF No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("PFNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NAME</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>DSG</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAccountNo" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>W-Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>One-Day</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Stamp") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>TotalWages</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Fbasic") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>Basic</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>HRA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("BasicHRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ConvAllow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("ConvAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                                   
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>Medical</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("MediAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                               <asp:TemplateField>
                                                                <HeaderTemplate>GrossEarnings</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>PF</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>ESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                               <asp:TemplateField>
                                                                <HeaderTemplate>EmployeerPF</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("EmployeerPFone") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                               <asp:TemplateField>
                                                                <HeaderTemplate>EmployeerESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("EmployeerESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             
                                                           <asp:TemplateField>
                                                                <HeaderTemplate>Fine</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFine" runat="server" Text='<%# Eval("Fine") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                       
                                                          <asp:TemplateField>
                                                                <HeaderTemplate>Total-Ded</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField> <asp:TemplateField>
                                                                <HeaderTemplate>Gift</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGift" runat="server" Text='<%# Eval("Gift") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>     
                                                            
                                                            
                                                             <asp:TemplateField>
                                                           <HeaderTemplate>Net-Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRemarks" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr> <!--  BANK SALARY Details GridView End  -->
                                            
                                            <tr id="LadiesSalary" runat="server" visible="false">
                                              <td colspan="4">
                                                   <asp:GridView ID="LadiesGridView" runat="server" AutoGenerateColumns="false">
                                                     <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>S. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Token No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NAME</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>DSG</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAccountNo" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>Base</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                         
                                                            
                                                               <asp:TemplateField>
                                                                <HeaderTemplate>W-Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                                  <asp:TemplateField>
                                                                <HeaderTemplate>BasicAndDA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("BasicAndDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OTHrs</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("OTHoursNew") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>OTAmt</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                               <asp:TemplateField>
                                                                <HeaderTemplate>allowances1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                               <asp:TemplateField>
                                                                <HeaderTemplate>allowances2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>GrossEarnings</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>Deduction1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDeduction3" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>TOTAL DEDUCTION</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NET SALARY</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRemarks" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                   </asp:GridView>
                                              </td>
                                            </tr>
                                            
                                            <tr id="TravelSalary" runat="server" visible="false">
                                               <td colspan="4">
                                                  <asp:GridView ID="TravelGridView" runat="server" AutoGenerateColumns="false">
                                                     <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>S. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField Visible="false">
                                                                <HeaderTemplate>EmpNo</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Token No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NAME</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>DEPARTMENT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAccountNo" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                                                                        
                                                               <asp:TemplateField>
                                                                <HeaderTemplate>NOD</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                                  <asp:TemplateField>
                                                                <HeaderTemplate>BASIC WAGES</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBase" runat="server" Text='<%# Eval("BasicAndDANew") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                               <asp:TemplateField>
                                                              <HeaderTemplate>SPG INCEN</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSpg" runat="server" Text='<%# Eval("ConvAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                                 <asp:TemplateField>
                                                                <HeaderTemplate>MAIS INCEN</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMaisInc" runat="server" Text='<%# Eval("BasicHRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                          
                                                            
                                                                  
                                                                 <asp:TemplateField>
                                                                <HeaderTemplate>WDAY INCEN</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWdayInce" runat="server" Text='<%# Eval("DayIncentive") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                                  <asp:TemplateField>
                                                                <HeaderTemplate>NET WAGES</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNetWages" runat="server" Text='<%# Eval("Basic_SM") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                                  <asp:TemplateField>
                                                                <HeaderTemplate>TOTAL WAGES</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotalWages" runat="server" Text='<%# Eval("BasicAndDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOThours" runat="server" Text=''></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                      
                                                            
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>OT AMT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOTAmt" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>GRAND WAGES</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGWages" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>MESS</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMess" runat="server" Text='<%# Eval("WashingAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                               
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NET SALARY</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRemarks" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                  </asp:GridView>
                                               </td>
                                            </tr>
                                            
                                            <tr id="Grid_Labour111" runat="server" visible="true" >
                                            <td colspan="4">
                                            <asp:GridView ID="Grid_Labour" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>S. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Token No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>PF No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("PFNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NAME</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>DaySalary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Fbasic") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>W-Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Basic</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>HRA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("BasicRAI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>WashingAllow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("WashingAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>W.hrs</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("CL") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>W.hrs_Sal</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("BasicHRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT-Hrs</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("OTHoursNew") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT_Hrs_Amt</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("OTHoursAmtNew") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Full_OT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Threesided") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Full_OT_Inc</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("ThreesidedAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Full_OT_Amt</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("SpinningAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                                                                                         
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>GrossEarnings</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                               <asp:TemplateField>
                                                                <HeaderTemplate>PF</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>ESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                                                                                                                                       
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                         
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Fancy</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Uni</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Main</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                                                                                                                                    
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>EB</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("DedOthers1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Mess</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Messdeduction") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Atm</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("HRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                                                                                       
                                                          <asp:TemplateField>
                                                                <HeaderTemplate>Fine</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFine" runat="server" Text='<%# Eval("Fine") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                       
                                                          <asp:TemplateField>
                                                                <HeaderTemplate>Total-Ded</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField> <asp:TemplateField>
                                                                <HeaderTemplate>Gift</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGift" runat="server" Text='<%# Eval("Gift") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net-Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRemarks" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                            
                                            
                                            </td>                                       
                                            </tr>
                                            
                                            
                                            <!--  Labour Details GridView Start  -->
                                            <tr id="Grid_Labour11" runat="server" visible="true" >
                                            <td colspan="4">
                                            <asp:GridView ID="Grid_Labour1" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>S. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Token No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <%-- <asp:TemplateField>
                                                                <HeaderTemplate>PF No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("PFNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NAME</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>DaySalary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Fbasic") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>W-Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Basic</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT-Hrs</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("OTHoursNew") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT_Amt</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("OTHoursAmtNew") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                                    
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                                                 
                                                                 <asp:TemplateField>
                                                                <HeaderTemplate>Incentive</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWdayInce" runat="server" Text='<%# Eval("DayIncentive") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>GrossEarnings</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                               <asp:TemplateField>
                                                                <HeaderTemplate>PF</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>ESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>Deduction1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                         
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Fancy</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Uni</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Main</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                                                                                                                                    
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Canteen</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("DedOthers1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                                                                                                                              
                                                                                               
                                                         <asp:TemplateField>
                                                                <HeaderTemplate>Fine</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFine" runat="server" Text='<%# Eval("Fine") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                       
                                                          <asp:TemplateField>
                                                                <HeaderTemplate>Total-Ded</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField> <asp:TemplateField>
                                                                <HeaderTemplate>Gift</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGift" runat="server" Text='<%# Eval("Gift") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>     


                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net-Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRemarks" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                            
                                            
                                            </td>                                       
                                            </tr>
                                            <!--  Labour Details GridView End  -->
                                            
                                            
                                            <tr id="Tr1" runat="server" visible="false"> 
                                                <td colspan="4">
                                                    <asp:GridView ID="GridPayslip" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>S. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Token No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField>
                                                                <HeaderTemplate>PF No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("PFNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NAME</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>DaySalary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Fbasic") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>W-Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>Basic</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>W-Hrs</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("CL") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>W-Hrs_Wages</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("BasicHRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>DayIncentive</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("DayIncentive") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>allowances</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>allowances3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                                   
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>GrossEarnings</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHome" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                                <asp:TemplateField>
                                                                <HeaderTemplate>Deduction1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>PF</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                         
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Rent</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Gas</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            
                                                             <asp:TemplateField>
                                                                <HeaderTemplate>EB</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                                                                                                                                    
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Canteen</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorked" runat="server" Text='<%# Eval("DedOthers1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                                                 <asp:TemplateField>
                                                                <HeaderTemplate>Fine</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFine" runat="server" Text='<%# Eval("Fine") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                       
                                                          <asp:TemplateField>
                                                                <HeaderTemplate>Total-Ded</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField> <asp:TemplateField>
                                                                <HeaderTemplate>Gift</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGift" runat="server" Text='<%# Eval("Gift") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net-Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRemarks" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr> 
                                            
                                            
                                             
                                             
                                        </thead>
                                        </table>
                                        </div>
		
 	<!-- Dashboard start -->
 	
 	<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                       
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                             
                                          
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>

 
    <!-- Dashboard End --> 
 
  
          </div> 
 </div><!-- col 12 end -->
      
  </div><!-- row end -->






            </ContentTemplate>
            <Triggers>
             <asp:PostBackTrigger ControlID="btnSearch" />
             <asp:PostBackTrigger ControlID="btnSalary" />
             
            </Triggers>
        </asp:UpdatePanel>
</asp:Content>

