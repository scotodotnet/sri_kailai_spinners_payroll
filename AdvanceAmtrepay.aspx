﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdvanceAmtrepay.aspx.cs" Inherits="AdvanceAmtrepay" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>



<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Advance Repayment</li></h4> 
                    </ol>
             </div>

<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
<div class="col-md-9">
<div class="panel panel-white">
<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Advance Repayment</h4>
				</div>
				</div>
<form class="form-horizontal">
    <div class="panel-body"> 

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="Label7" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
				   
					   <div class="col-sm-3">
					      <asp:DropDownList ID="ddlcategory" class="form-control" runat="server" AutoPostBack="True" 
					      onselectedindexchanged="ddlcategory_SelectedIndexChanged">
                            </asp:DropDownList>  
					   </div>
				       <div class="col-md-1"></div>
				       <asp:Label ID="lbldepartment" runat="server" class="col-sm-2 control-label" 
                                                             Text="Department"></asp:Label>
                	  <div class="col-sm-3">
						<asp:DropDownList ID="ddldepartment" class="form-control" runat="server" AutoPostBack="True" 
						onselectedindexchanged="ddldepartment_SelectedIndexChanged">
                        </asp:DropDownList> 
					  </div>
						
					   </div>
				        
                       </div>
 
<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblempNo" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee No"></asp:Label>
				
					   <div class="col-sm-3">
					      <asp:DropDownList ID="ddlEmpNo" class="form-control" runat="server"  AutoPostBack="True" 
					                             onselectedindexchanged="ddlEmpNo_SelectedIndexChanged">
                            </asp:DropDownList>  
					   </div>
				       <div class="col-md-1"></div>
				         <asp:Label ID="lblempName" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee Name"></asp:Label>
                
					   <div class="col-sm-3">
						<asp:DropDownList ID="ddlEmpName" class="form-control" runat="server"  AutoPostBack="True" 
						             onselectedindexchanged="ddlEmpName_SelectedIndexChanged">
                        </asp:DropDownList> 
					  </div>
						
					   </div>
				        
                       </div>
 
<div class="col-md-12">     
				       <div class="form-group row">
				        <asp:Label ID="lblexist" runat="server" class="col-sm-2 control-label" 
                                                             Text="Existing No"></asp:Label>
				 
					   <div class="col-sm-3">
                           <asp:TextBox ID="txtExist" class="form-control" runat="server"></asp:TextBox>	
                       </div>
				       <div class="col-md-1"></div>
                	   <div class="col-sm-2 control-label">
                           <asp:Button ID="btnsearch" class="btn btn-success" runat="server" Text="Search" onclick="btnsearch_Click"/>
                	   </div>
					   					
					   </div>
				        
                       </div>
 
<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblDesignation" runat="server" class="col-sm-2 control-label" 
                                                             Text="Designation"></asp:Label>
 
   <div class="col-sm-3">
    <asp:Label ID="lbldesignation1" runat="server" Text=""></asp:Label>  
     </div>
  
     <div class="col-sm-1"></div>
     <asp:Label ID="lblbalanceAmt" runat="server" class="col-sm-2 control-label" 
                                                             Text="Balance Amount"></asp:Label>
     <div class="col-sm-3">
        <asp:Label ID="lblBalanceAmt1" runat="server" Text=""></asp:Label>
     </div>
		
   
	</div>
		        
 </div> 
 
<div class="col-md-12">     
 <div class="form-group row">
  <asp:Label ID="lblAmt" runat="server" class="col-sm-2 control-label" 
                                                             Text="Amount"></asp:Label>
 
   <div class="col-sm-3">
       <asp:TextBox ID="txtamt" class="form-control" runat="server" ontextchanged="txtamt_TextChanged"></asp:TextBox>
  </div>
    <div class="col-sm-1"></div> 
    <asp:Label ID="lbldate" runat="server" class="col-sm-2 control-label" 
                                                             Text="Date"></asp:Label>  
   
    <div class="col-sm-3">
        <asp:TextBox ID="txtdate" class="form-control" runat="server"></asp:TextBox>
        <cc1:CalendarExtender ID="Caleder_txtdob" runat="server" TargetControlID="txtdate"
        Format="dd-MM-yyyy" CssClass="orange">
        </cc1:CalendarExtender>
        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server"
          FilterMode="ValidChars" FilterType="Custom,Numbers"
          TargetControlID="txtdate" ValidChars="0123456789/-">
        </cc1:FilteredTextBoxExtender>
     </div>
		
   
	</div>
		        
 </div> 
 
<div class="col-md-12">     
				<div class="form-group row">
				
				<div class="col-sm-3"></div>	
			     
				
				 <div class="col-sm-3">
                     <asp:Button ID="btnsave" class="btn btn-success" runat="server" Text="Save" onclick="btnsave_Click"/>
  				 </div>
  				
  				 <div class="col-sm-3">
                     <asp:Button ID="btnClear" class="btn btn-success" runat="server" Text="Clear" onclick="btnClear_Click"/>
  				 </div>
  				 
  				 				 				
			    </div>
				       
        </div> 

</div>
</form>
</div>
</div>

<!-- Dashboard start -->
 	
 <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    
 <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
  
 
    <!-- Dashboard End --> 


</div>
</div>
</div> 

</ContentTemplate>
 </asp:UpdatePanel>

</asp:Content>

