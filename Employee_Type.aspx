﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Employee_Type.aspx.cs" Inherits="Employee_Type" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tableEmployeeType').dataTable();               
            }
        }); 
    };
    </script>
      
             
     <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
        $('#tableEmployeeType').dataTable();
        });
    </script>





 <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate>

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Employee Type</li>
                   </ol>
              </div>
 
 
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
             
           <div class="col-md-9">
			<div class="panel panel-white">
			    <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Employee Type</h4>
				</div>
				</div>
				<form class="form-horizontal">
				 <div class="panel-body">
                            
					
                        <form class="form-horizontal">
                          <div class="panel-body">
                             <div class="col-md-20">
                                <div class="row">
        
                           
				       <div class="form-group row">
				    	
					   <label for="input-Default" class="col-sm-2 control-label">Category</label>
						<div class="col-sm-4">
                             <asp:RadioButtonList ID="rbtstaffloaber" runat="server" RepeatColumns="2" >
                             <asp:ListItem Text="Staff" Value="1"></asp:ListItem>
	                         <asp:ListItem Text="Labour" Value="2"></asp:ListItem>                                            
	                         </asp:RadioButtonList>                               
                        </div>
					
				         <label for="input-Default" class="col-sm-2 control-label">Employee Type</label>
						<div class="col-sm-3">
						 <asp:TextBox ID="txtemptyp" class="form-control" runat="server"></asp:TextBox>
                         
						</div>
				    
					 
			       </div>
					    
			      
                     <div class="form-group row">     
						<!-- Button start -->
						
                   <div class="txtcenter">
                         <asp:Button ID="btnsave" class="btn btn-success"  runat="server" Text="Save" onclick="btnsave_Click"/>
                                                       
                         <asp:LinkButton ID="btncancel" class="btn btn-danger" runat="server" onclick="btncancel_Click">Cancel</asp:LinkButton>      
                    </div>
                    <!-- Button End -->	
						
					</div>
						
						  <!-- Table start -->
					
				      <div class="panel-body">
					
					
						<div class="table-responsive">
    
                              <asp:Repeater ID="rptrEmployeeType" runat="server" onitemcommand="Repeater1_ItemCommand">
                            <HeaderTemplate>
                
                            <table id="tableEmployeeType" class="display table" style="width: 100%;">
                          <thead>
                           <tr>
                                                 
                                                    <%--<th>Employee Type Code</th>--%>
                                                    <th>Employee Type</th>
                                                    <th>Category</th> 
                                                    <th>Edit</th> 
                                                    <th>Delete</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                                      
                    <%--<td>
                        <%# DataBinder.Eval(Container.DataItem, "EmpTypeCd")%>
                    </td>--%>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "EmpType")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "EmpCategory")%>
                    </td>
                    <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("EmpType")+","+ Eval("EmpCategory")+","+ Eval("EmpTypeCd") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                     </td>
                    <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("EmpType")+","+ Eval("EmpCategory")+","+ Eval("EmpTypeCd") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
       
                     </div>
            
                       
                 				
						<!--Table end -->
			
	
			    </div>
                           
                           </div>
                          </div>
				        </form>
			
			         </div>
			      </form>
			
			
			</div><!-- panel white end -->
		   
		    
		      <div class="col-md-2"></div>
		    
                
      
 </div> <!-- col-9 end -->
 <!-- Dashboard start -->

<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                             
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>

 <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 
 </div>
 
 </ContentTemplate>
 </asp:UpdatePanel>

 
</asp:Content>

