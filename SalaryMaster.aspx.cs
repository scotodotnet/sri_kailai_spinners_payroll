﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class SalaryMaster : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string staffLabour;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string UserID = "", SesRoleCode = "";
    static string EmpType;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        UserID = Session["UserId"].ToString();
        SesRoleCode = Session["RoleCode"].ToString();
        //lblComany.Text = SessionCcode + " - " + SessionLcode;
        if (!IsPostBack)
        {
            ////Check Rights Code Start
            //string query = "";
            //DataTable dt = new DataTable();
            //if (UserID.ToUpper().ToString() != "Altius".ToUpper().ToString())
            //{
            //    query = "Select * from MstUser_Rights where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Username='" + UserID + "'";
            //    dt = objdata.RptEmployeeMultipleDetails(query);
            //    if (dt.Rows.Count != 0)
            //    {
            //        if (dt.Rows[0]["SalaryMaster"].ToString() == "1")
            //        {
            //            //Skip
            //        }
            //        else
            //        {
            //            Response.Redirect("EmployeeRegistration2.aspx");
            //        }
            //    }
            //    else
            //    {
            //        Response.Redirect("EmployeeRegistration2.aspx");
            //    }
            //}
            ////Check Rights Code End

            //Department();
            //FoodDetails();
            category();
            clr();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddFinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            txtTokenNo.Enabled = false;
        }
        //lblusername.Text = Session["Usernmdisplay"].ToString();
        
    }

    public void category()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public void clr()
    {
        ddlcategory.SelectedIndex = -1;
        ddlDepartment.SelectedIndex = -1;
        ChkAllDept.Checked = false;
        txtexistingNo.Text = "";
        ddlEmpName.SelectedIndex = -1;
        ddlempNo.SelectedIndex = -1;
        ddToken.SelectedIndex = -1;
        rbsalary.SelectedIndex = -1;
        txtSBasic.Text = "0";
        txtSall1.Text = "0";
        txtSall2.Text = "0";
        txtSded1.Text = "0";
        txtSded2.Text = "0";
        PanelStaff.Visible = false;
       
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
       
        string staffLabour_temp;
        DataTable dtempty = new DataTable();
        ddlDepartment.DataSource = dtempty;
        ddlDepartment.DataBind();
        ddlEmpName.DataSource = dtempty;
        ddlEmpName.DataBind();
        ddlempNo.DataSource = dtempty;
        ddlempNo.DataBind();
        ddToken.DataSource = dtempty;
        ddToken.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            staffLabour_temp = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            staffLabour_temp = "L";
        }
        else
        {
            staffLabour_temp = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(staffLabour_temp, SessionCcode, SessionLcode);
        if (dtDip.Rows.Count > 1)
        {
            ddlDepartment.DataSource = dtDip;
            ddlDepartment.DataTextField = "DepartmentNm";
            ddlDepartment.DataValueField = "DepartmentCd";
            ddlDepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddlDepartment.DataSource = dt;
            ddlDepartment.DataBind();
        }
        //PanelSpecial.Visible = false;
        //Panelall.Visible = false;
        PanelStaff.Visible = false;
        //PanelDaily.Visible = false;

    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        //PanelSpecial.Visible = false;
        //Panelall.Visible = false;
        PanelStaff.Visible = false;
        //PanelDaily.Visible = false;
        DataTable dtempty = new DataTable();
        ddlEmpName.DataSource = dtempty;
        ddlEmpName.DataBind();
        ddlempNo.DataSource = dtempty;
        ddlempNo.DataBind();
        ddToken.DataSource = dtempty;
        ddToken.DataBind();
    }
    protected void btnclick_Click(object sender, EventArgs e)
    {
        //PanelSpecial.Visible = false;
        //Panelall.Visible = false;
        PanelStaff.Visible = false;
        //PanelDaily.Visible = false;
        bool ErrFlag = false;
        if (ddlDepartment.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department...!');", true);
            ErrFlag = true;
        }
        else if (ddlDepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department...!');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "1")
            {

                //panelError.Visible = true;
                if (ddlcategory.SelectedValue == "1")
                {
                    staffLabour = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    staffLabour = "L";
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category...!');", true);
                    //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //lblError.Text = "Select the Category";
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {


                    DataTable dtempcode = new DataTable();
                    if (SessionAdmin == "1")
                    {
                        dtempcode = objdata.EmployeeLoadSalaryMaster(staffLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
                    }
                    else
                    {
                        dtempcode = objdata.EmployeeLoadSalaryMaster_user(staffLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
                    }

                    ddlempNo.DataSource = dtempcode;
                    DataRow dr = dtempcode.NewRow();
                    dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
                    dr["EmpName"] = ddlDepartment.SelectedItem.Text;
                    dr["ExisistingCode"] = ddlDepartment.SelectedItem.Text;
                    dtempcode.Rows.InsertAt(dr, 0);


                    ddlempNo.DataTextField = "EmpNo";

                    ddlempNo.DataValueField = "EmpNo";
                    ddlempNo.DataBind();
                    ddlEmpName.DataSource = dtempcode;
                    ddlEmpName.DataTextField = "EmpName";
                    ddlEmpName.DataValueField = "EmpNo";
                    ddlEmpName.DataBind();

                    ddToken.DataSource = dtempcode;
                    ddToken.DataTextField = "ExisistingCode";
                    ddToken.DataValueField = "ExisistingCode";
                    ddToken.DataBind();
                    //PanelStaff.Visible = true;
                }
                //clr();
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                staffLabour = "L";

                DataTable dtempcode = new DataTable();
                dtempcode = objdata.EmployeeLoadSalaryMaster(staffLabour, ddlDepartment.SelectedValue, SessionCcode, SessionLcode);
                //EmpType = dtempcode.Rows[0]["EmployeeType"].ToString();

                ddlempNo.DataSource = dtempcode;

                DataRow dr = dtempcode.NewRow();
                dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
                dr["EmpName"] = ddlDepartment.SelectedItem.Text;
                dr["ExisistingCode"] = ddlDepartment.SelectedItem.Text;
                dtempcode.Rows.InsertAt(dr, 0);

                ddlempNo.DataTextField = "EmpNo";
                ddlempNo.DataValueField = "EmpNo";
                ddlempNo.DataBind();
                ddlEmpName.DataSource = dtempcode;
                ddlEmpName.DataTextField = "EmpName";
                ddlEmpName.DataValueField = "EmpNo";
                ddlEmpName.DataBind();

                ddToken.DataSource = dtempcode;
                ddToken.DataTextField = "ExisistingCode";
                ddToken.DataValueField = "ExisistingCode";
                ddToken.DataBind();

            }
        }
    }

    protected void ddFinance_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnexitingSearch_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            clr();
            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Category Staff or Labour');", true);

                ErrFlag = true;
            }
            else if (ddlDepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Department');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select Department";
                ErrFlag = true;
            }
            //else if (txtexistingNo.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Existing Number');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Existing Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    //lblError.Text = "Enter the Existing Number";
            //    ErrFlag = true;
            //}
            if (!ErrFlag)
            {
                DataTable dtempcode = new DataTable();
                string Cate = "";
                if (ddlcategory.SelectedValue == "1")
                {
                    Cate = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    Cate = "L";
                }

                dtempcode = objdata.Salary_ExistNo(ddlDepartment.SelectedValue, ddToken.SelectedValue, Cate, SessionCcode, SessionLcode, SessionAdmin);
                EmpType = dtempcode.Rows[0]["EmployeeType"].ToString();

                if (EmpType == "1")
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = false;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "2")
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = true;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "3")
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = true;
                    //Panelall.Visible = false;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "4")
                {
                    //PanelSpecial.Visible = true;
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = false;
                }
                else
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = true;
                    //PanelSpecial.Visible = false;
                }
                if (dtempcode.Rows.Count > 0)
                {
                    ddlempNo.SelectedValue = dtempcode.Rows[0]["EmpNo"].ToString();
                    ddlEmpName.DataSource = dtempcode;
                    ddlEmpName.DataTextField = "EmpName";
                    ddlEmpName.DataValueField = "EmpNo";
                    ddlEmpName.DataBind();
                    if (dtempcode.Rows[0]["Wagestype"].ToString() == "1")
                    {
                        rbsalary.SelectedValue = "1";
                    }
                    else if (dtempcode.Rows[0]["Wagestype"].ToString() == "2")
                    {
                        rbsalary.SelectedValue = "2";
                    }
                    else if (dtempcode.Rows[0]["Wagestype"].ToString() == "3")
                    {
                        rbsalary.SelectedValue = "3";
                    }
                    string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                    if (empCodeverify == ddlempNo.SelectedValue)
                    {
                        DataTable dtempLoad = new DataTable();
                        dtempLoad = objdata.SalaryMasterEmpLoad(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                        if (dtempLoad.Rows.Count > 0)
                        {
                            txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                            txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //Profilelbl.Text = dtempLoad.Rows[0]["ProfileType"].ToString();
                            //if (EmpType == "1")
                            //{
                            //    txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}
                            //else if (EmpType == "2")
                            //{
                            //    txtbase.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtFDA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                            //    txthra.Text = dtempLoad.Rows[0]["HRA"].ToString();
                            //    txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                            //    //txttotal.Text = dtempLoad.Rows[0]["Total"].ToString();
                            //    txtUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                            //    txAllowance1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtAllowance2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtDeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtDeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}
                            //else if (EmpType == "3")
                            //{
                            //    txtWBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtWAll1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtWAll2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtWded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtWded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}
                            //else if (EmpType == "4")
                            //{
                            //    txtOBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtOUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                            //    txtOAll1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtOAll2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtOded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtOded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}
                            //else
                            //{
                            //    txtbase.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtFDA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                            //    txthra.Text = dtempLoad.Rows[0]["HRA"].ToString();
                            //    txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                            //    //txttotal.Text = dtempLoad.Rows[0]["Total"].ToString();
                            //    txtUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                            //    txAllowance1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtAllowance2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtDeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtDeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}

                        }
                        else
                        {
                            
                            txtSBasic.Text = "0";
                            txtSall1.Text = "0";
                            txtSall2.Text = "0";
                            txtSded1.Text = "0";
                            txtSded2.Text = "0";
                           

                        }

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Data Not Found');", true);
                    clr();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlempNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (ddlempNo.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dtempName = new DataTable();
                dtempName = objdata.BonusEmployeeName(ddlempNo.SelectedValue, SessionCcode, SessionLcode);

                //DataRow dr = dtempName.NewRow();
                //dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
                //dr["EmpName"] = ddlDepartment.SelectedItem.Text;
                //dtempName.Rows.InsertAt(dr, 0);
                if (dtempName.Rows.Count > 0)
                {
                    ddToken.SelectedValue = dtempName.Rows[0]["ExisistingCode"].ToString();
                    ddlEmpName.SelectedValue = dtempName.Rows[0]["EmpNo"].ToString();
                    //ddlEmpName.DataSource = dtempName;
                    //ddlEmpName.DataTextField = "EmpName";
                    //ddlEmpName.DataValueField = "EmpNo";
                    //ddlEmpName.DataBind();
                    //ddToken.DataSource = dtempName;
                    //ddToken.DataTextField = "ExisistingCode";
                    //ddToken.DataValueField = "ExisistingCode";
                    //ddToken.DataBind();


                    txtexistingNo.Text = dtempName.Rows[0]["ExisistingCode"].ToString();
                    //clr();
                    string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                    DataTable dtempLoad = new DataTable();
                    dtempLoad = objdata.SalaryMasterEmpLoad(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                    EmpType = dtempName.Rows[0]["EmployeeType"].ToString();
                    if (dtempName.Rows[0]["Wagestype"].ToString() == "1")
                    {
                        rbsalary.SelectedValue = "1";
                    }
                    else if (dtempName.Rows[0]["Wagestype"].ToString() == "2")
                    {
                        rbsalary.SelectedValue = "2";
                    }
                    else if (dtempName.Rows[0]["Wagestype"].ToString() == "3")
                    {
                        rbsalary.SelectedValue = "3";
                    }
                    if (EmpType == "1")
                    {
                        PanelStaff.Visible = true;
                        //PanelDaily.Visible = false;
                        //Panelall.Visible = false;
                        //PanelSpecial.Visible = false;
                    }
                    else if (EmpType == "2")
                    {
                        PanelStaff.Visible = true;
                        //PanelDaily.Visible = false;
                        //Panelall.Visible = true;
                        //PanelSpecial.Visible = false;
                    }
                    else if (EmpType == "3")
                    {
                        PanelStaff.Visible = true;
                        //PanelDaily.Visible = true;
                        //Panelall.Visible = false;
                        //PanelSpecial.Visible = false;
                    }
                    else if (EmpType == "4")
                    {
                        //PanelSpecial.Visible = true;
                        PanelStaff.Visible = true;
                        //PanelDaily.Visible = false;
                        //Panelall.Visible = false;
                    }
                    else
                    {
                        PanelStaff.Visible = true;
                        //PanelDaily.Visible = false;
                        //Panelall.Visible = true;
                        //PanelSpecial.Visible = false;
                    }
                    if (empCodeverify == ddlempNo.SelectedValue)
                    {


                        if (dtempLoad.Rows.Count > 0)
                        {
                            txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                            txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            txtPF.Text = dtempLoad.Rows[0]["PFS"].ToString();
                            txtexistingNo.Text = dtempLoad.Rows[0]["ExisistingCode"].ToString();
                            //if (EmpType == "1")
                            //{
                            //    txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}
                            //else if (EmpType == "2")
                            //{
                            //    txtbase.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtFDA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                            //    txthra.Text = dtempLoad.Rows[0]["HRA"].ToString();
                            //    txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                            //    //txttotal.Text = dtempLoad.Rows[0]["Total"].ToString();
                            //    txtUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                            //    txAllowance1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtAllowance2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtDeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtDeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}
                            //else if (EmpType == "3")
                            //{
                            //    txtWBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtWAll1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtWAll2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtWded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtWded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}
                            //else if (EmpType == "4")
                            //{
                            //    txtOBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtOUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                            //    txtOAll1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtOAll2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtOded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtOded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}
                            //else
                            //{
                            //    txtbase.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtFDA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                            //    txthra.Text = dtempLoad.Rows[0]["HRA"].ToString();
                            //    txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                            //    //txttotal.Text = dtempLoad.Rows[0]["Total"].ToString();
                            //    txtUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                            //    txAllowance1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtAllowance2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtDeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtDeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}
                        }
                        else
                        {
                            //txtbase.Text = "0";
                            //txtFDA.Text = "0";
                            //txthra.Text = "0";
                            //txtVDA.Text = "0";
                            //txttotal.Text = "0";
                            //txtUnion.Text = "0";
                            //txAllowance1.Text = "0";
                            //txtAllowance2.Text = "0";
                            //txtDeduction1.Text = "0";
                            //txtDeduction2.Text = "0";
                            //txtOBasic.Text = "0";
                            //txtOUnion.Text = "0";
                            //txtOAll1.Text = "0";
                            //txtOAll2.Text = "0";
                            //txtOded1.Text = "0";
                            //txtOded2.Text = "0";
                        }
                    }
                }
                else
                {
                    //txtbase.Text = "0";
                    //txtFDA.Text = "0";
                    //txthra.Text = "0";
                    //txtVDA.Text = "0";
                    //txttotal.Text = "0";
                    //txtUnion.Text = "0";
                    //txAllowance1.Text = "0";
                    //txtAllowance2.Text = "0";
                    //txtDeduction1.Text = "0";
                    //txtDeduction2.Text = "0";
                    //txtOBasic.Text = "0";
                    //txtOUnion.Text = "0";
                    //txtOAll1.Text = "0";
                    //txtOAll2.Text = "0";
                    //txtOded1.Text = "0";
                    //txtOded2.Text = "0";
                }
            }

        }
        catch (Exception ex)
        {
        }


    }
    protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (ddlEmpName.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dtemp2 = new DataTable();
                dtemp2 = objdata.BonusEmployeeName(ddlEmpName.SelectedValue, SessionCcode, SessionLcode);

                ddToken.SelectedValue = dtemp2.Rows[0]["ExisistingCode"].ToString();
                ddlempNo.SelectedValue = dtemp2.Rows[0]["EmpNo"].ToString();

                //ddlempNo.DataSource = dtemp2;
                //ddlempNo.DataTextField = "EmpNo";
                //ddlempNo.DataValueField = "EmpNo";
                //ddlempNo.DataBind();
                txtexistingNo.Text = dtemp2.Rows[0]["ExisistingCode"].ToString();
                //ddToken.DataSource = dtemp2;
                //ddToken.DataTextField = "ExisistingCode";
                //ddToken.DataValueField = "ExisistingCode";
                //ddToken.DataBind();
                //clr();
                string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                DataTable dtempLoad = new DataTable();
                dtempLoad = objdata.SalaryMasterEmpLoad(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                EmpType = dtemp2.Rows[0]["EmployeeType"].ToString();
                if (dtemp2.Rows[0]["Wagestype"].ToString() == "1")
                {
                    rbsalary.SelectedValue = "1";
                }
                else if (dtemp2.Rows[0]["Wagestype"].ToString() == "2")
                {
                    rbsalary.SelectedValue = "2";
                }
                else if (dtemp2.Rows[0]["Wagestype"].ToString() == "3")
                {
                    rbsalary.SelectedValue = "3";
                }
                if (EmpType == "1")
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = false;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "2")
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = true;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "3")
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = true;
                    //Panelall.Visible = false;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "4")
                {
                    //PanelSpecial.Visible = true;
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = false;
                }
                else
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = true;
                    //PanelSpecial.Visible = false;
                }
                if (empCodeverify == ddlempNo.SelectedValue)
                {

                    if (dtempLoad.Rows.Count > 0)
                    {
                        txtexistingNo.Text = dtempLoad.Rows[0]["ExisistingCode"].ToString();
                        txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                        txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        txtPF.Text = dtempLoad.Rows[0]["PFS"].ToString();
                        //Profilelbl.Text = dtempLoad.Rows[0]["ProfileType"].ToString();
                        //if (EmpType == "1")
                        //{
                        //    txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                        //else if (EmpType == "2")
                        //{
                        //    txtbase.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtFDA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                        //    txthra.Text = dtempLoad.Rows[0]["HRA"].ToString();
                        //    txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                        //    //txttotal.Text = dtempLoad.Rows[0]["Total"].ToString();
                        //    txtUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                        //    txAllowance1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtAllowance2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtDeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtDeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                        //else if (EmpType == "3")
                        //{
                        //    txtWBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtWAll1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtWAll2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtWded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtWded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                        //else if (EmpType == "4")
                        //{
                        //    txtOBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtOUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                        //    txtOAll1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtOAll2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtOded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtOded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                        //else
                        //{
                        //    txtbase.Text = dtempLoad.Rows[0]["Base"].ToString();
                        //    txtFDA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                        //    txthra.Text = dtempLoad.Rows[0]["HRA"].ToString();
                        //    txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                        //    //txttotal.Text = dtempLoad.Rows[0]["Total"].ToString();
                        //    txtUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                        //    txAllowance1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                        //    txtAllowance2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                        //    txtDeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                        //    txtDeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                        //}
                    }
                    else
                    {
                        //txtbase.Text = "0";
                        //txtFDA.Text = "0";
                        //txthra.Text = "0";
                        //txtVDA.Text = "0";
                        //txttotal.Text = "0";
                        //txtUnion.Text = "0";
                        //txAllowance1.Text = "0";
                        //txtAllowance2.Text = "0";
                        //txtDeduction1.Text = "0";
                        //txtDeduction2.Text = "0";
                        //txtOBasic.Text = "0";
                        //txtOUnion.Text = "0";
                        //txtOAll1.Text = "0";
                        //txtOAll2.Text = "0";
                        //txtOded1.Text = "0";
                        //txtOded2.Text = "0";
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
              DataTable dt = new DataTable();
            DataTable Da = new DataTable(); 
            DataTable dtSalary = new DataTable();
            string Query = "";

            Query = "Select * from [SKS_Epay]..EmployeeDetails where EmployeeType='1'";
            dt = objdata.RptEmployeeMultipleDetails(Query);

            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                string SSQL = "";
                SSQL = "Select BaseSalary from [SKS_Spay]..Employee_Mst where MachineID='" + dt.Rows[i]["ExisistingCode"].ToString().Trim() + "'";
                Da = objdata.RptEmployeeMultipleDetails(SSQL);


                Query = "Insert into [SKS_Epay]..SalaryMaster(EmpNo,Alllowance1amt,Allowance2amt,CreatedDate,ModifiedDate,Deduction1,Deduction2,Ccode,Lcode,Base,FDA,VDA,HRA,Total,Unioncharges,PFS) Values( ";
                Query = Query + "'" + dt.Rows[i]["EmpNo"].ToString().Trim() + "','0.00','0.00',convert(datetime,'07/06/2016',103),convert(datetime,'07/06/2016',103),'0.00','0.00','SKS','UNIT I','" + Da.Rows[0]["BaseSalary"].ToString().Trim() + "','0.00','0.00','0.00','0.00','0.00','0.00')";
                dtSalary = objdata.RptEmployeeMultipleDetails(Query);
            }

            //bool ErrFlag = false;
            //bool Update = false;
            //bool Insert = false;
            ////if (EmpType == "2")
            ////{
            ////    if (txtbase.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtFDA.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the FDA....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txthra.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the HRA....!');", true);
            ////        ErrFlag = true;
            ////    }

            ////    else if (txtVDA.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtUnion.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Union Charges....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (Convert.ToDecimal(txtbase.Text.Trim()) == 0)
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txAllowance1.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the  Allowance 1 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtAllowance2.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 2 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtDeduction1.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 1 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtDeduction2.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 2 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    if (!ErrFlag)
            ////    {
            ////        //string temp = (Convert.ToDecimal(txtbase.Text.Trim()) + Convert.ToDecimal(txtFDA.Text.Trim()) + Convert.ToDecimal(txtVDA.Text.Trim()) + Convert.ToDecimal(txthra.Text.Trim())).ToString();
            ////        //temp = (Math.Round(Convert.ToDecimal(temp), 4, MidpointRounding.ToEven)).ToString();
            ////        //if (Convert.ToDecimal(temp) != Convert.ToDecimal(txttotal.Text))
            ////        //{
            ////        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Cick the Calculate....!');", true);
            ////        //    ErrFlag = true;
            ////        //}
            ////        if (!ErrFlag)
            ////        {
            ////            SalaryMasterClass objsal = new SalaryMasterClass();

            ////            objsal.EmpNo = ddlempNo.SelectedValue;
            ////            objsal.Ccode = SessionCcode;
            ////            objsal.Lcode = SessionLcode;
            ////            objsal.Base = txtbase.Text;
            ////            objsal.HRA = txthra.Text;
            ////            objsal.FDA = txtFDA.Text;
            ////            objsal.VDA = txtVDA.Text;
            ////            objsal.union = txtUnion.Text;
            ////            //objsal.total = txttotal.Text;
            ////            objsal.Allowance1Amt = txAllowance1.Text;
            ////            objsal.Allowance2Amt = txtAllowance2.Text;
            ////            objsal.deduction1 = txtDeduction1.Text;
            ////            objsal.deduction2 = txtDeduction2.Text;
            ////            objsal.EmpType = EmpType;
            ////            string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
            ////            if (empCodeverify == ddlempNo.SelectedValue)
            ////            {
            ////                objdata.SalaryMaster_Update(objsal);
            ////                Update = true;
            ////            }
            ////            else
            ////            {
            ////                objdata.SalaryMaster_Insert(objsal);
            ////                Insert = true;
            ////            }
            ////            if (Insert == true)
            ////            {
            ////                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);

            ////            }
            ////            if (Update == true)
            ////            {
            ////                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully....!');", true);

            ////            }
            ////        }
            ////    }
            ////}
            ////if (EmpType == "1")
            //{
            //    if (txtSBasic.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary....!');", true);
            //        ErrFlag = true;
            //    }
            //    //else if (Convert.ToDecimal(txtSBasic.Text) == 0)
            //    //{
            //    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Properly....!');", true);
            //    //    ErrFlag = true;
            //    //}
            //    else if (txtSall1.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 1 Amount....!');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtSall2.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 2 Amount....!');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtSded1.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 1 Amount....!');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtSded2.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 2 Amount....!');", true);
            //        ErrFlag = true;
            //    }
            //    else if (txtPF.Text.Trim() == "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Salary....!');", true);
            //        ErrFlag = true;
            //    }
            //    if (!ErrFlag)
            //    {
            //        SalaryMasterClass objsal = new SalaryMasterClass();
            //        objsal.EmpNo = ddlempNo.SelectedValue;
            //        objsal.Ccode = SessionCcode;
            //        objsal.Lcode = SessionLcode;
            //        objsal.Base = txtSBasic.Text;
            //        objsal.HRA = "0";
            //        objsal.FDA = "0";
            //        objsal.VDA = "0";
            //        objsal.union = "0";
            //        objsal.total = "0";
            //        objsal.PFsalary = txtPF.Text;
            //        objsal.Allowance1Amt = txtSall1.Text;
            //        objsal.Allowance2Amt = txtSall2.Text;
            //        objsal.deduction1 = txtSded1.Text;
            //        objsal.deduction2 = txtSded2.Text;
            //        objsal.EmpType = EmpType;
            //        objsal.Finance = ddFinance.SelectedValue;
            //        string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
            //        if (empCodeverify == ddlempNo.SelectedValue)
            //        {
            //            objdata.SalaryMaster_Update(objsal);
            //            Update = true;
            //        }
            //        else
            //        {
            //            objdata.SalaryMaster_Insert(objsal);
            //            Insert = true;
            //        }
            //        if (Insert == true)
            //        {
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);
            //            clr();
            //        }
            //        if (Update == true)
            //        {
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully....!');", true);

            //        }
            //    }
            //}
            ////else if (EmpType == "3")
            ////{
            ////    if (txtWBasic.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (Convert.ToDecimal(txtWBasic.Text) == 0)
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Properly....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtWAll1.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 1 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtWAll2.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 2 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtWded1.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 1 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtWded2.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 2 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    if (!ErrFlag)
            ////    {
            ////        SalaryMasterClass objsal = new SalaryMasterClass();
            ////        objsal.EmpNo = ddlempNo.SelectedValue;
            ////        objsal.Ccode = SessionCcode;
            ////        objsal.Lcode = SessionLcode;
            ////        objsal.Base = txtWBasic.Text;
            ////        objsal.HRA = "0";
            ////        objsal.FDA = "0";
            ////        objsal.VDA = "0";
            ////        objsal.union = "0";
            ////        objsal.total = "0";
            ////        objsal.Allowance1Amt = txtWAll1.Text;
            ////        objsal.Allowance2Amt = txtWAll2.Text;
            ////        objsal.deduction1 = txtWded1.Text;
            ////        objsal.deduction2 = txtWded2.Text;
            ////        objsal.EmpType = EmpType;
            ////        string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
            ////        if (empCodeverify == ddlempNo.SelectedValue)
            ////        {
            ////            objdata.SalaryMaster_Update(objsal);
            ////            Update = true;
            ////        }
            ////        else
            ////        {
            ////            objdata.SalaryMaster_Insert(objsal);
            ////            Insert = true;
            ////        }
            ////        if (Insert == true)
            ////        {
            ////            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);

            ////        }
            ////        if (Update == true)
            ////        {
            ////            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully....!');", true);

            ////        }
            ////    }
            ////}
            ////else if (EmpType == "4")
            ////{
            ////    if (txtOBasic.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (Convert.ToDecimal(txtOBasic.Text) == 0)
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary Properly....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtOAll1.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 1 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtOAll2.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 2 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtOded1.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 1 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtOded2.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 2 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtOUnion.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Union Charges....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    if (!ErrFlag)
            ////    {
            ////        SalaryMasterClass objsal = new SalaryMasterClass();
            ////        objsal.EmpNo = ddlempNo.SelectedValue;
            ////        objsal.Ccode = SessionCcode;
            ////        objsal.Lcode = SessionLcode;
            ////        objsal.Base = txtOBasic.Text;
            ////        objsal.HRA = "0";
            ////        objsal.FDA = "0";
            ////        objsal.VDA = "0";
            ////        objsal.union = txtOUnion.Text;
            ////        objsal.total = "0";
            ////        objsal.Allowance1Amt = txtOAll1.Text;
            ////        objsal.Allowance2Amt = txtOAll2.Text;
            ////        objsal.deduction1 = txtOded1.Text;
            ////        objsal.deduction2 = txtOded2.Text;
            ////        objsal.EmpType = EmpType;
            ////        string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
            ////        if (empCodeverify == ddlempNo.SelectedValue)
            ////        {
            ////            objdata.SalaryMaster_Update(objsal);
            ////            Update = true;
            ////        }
            ////        else
            ////        {
            ////            objdata.SalaryMaster_Insert(objsal);
            ////            Insert = true;
            ////        }
            ////        if (Insert == true)
            ////        {
            ////            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);

            ////        }
            ////        if (Update == true)
            ////        {
            ////            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully....!');", true);

            ////        }
            ////    }
            ////}
            ////else
            ////{
            ////    if (txtbase.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtFDA.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the FDA....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txthra.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the HRA....!');", true);
            ////        ErrFlag = true;
            ////    }

            ////    else if (txtVDA.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtUnion.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Union Charges....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (Convert.ToDecimal(txtbase.Text.Trim()) == 0)
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Basic Salary....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txAllowance1.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the  Allowance 1 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtAllowance2.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Allowance 2 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtDeduction1.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 1 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    else if (txtDeduction2.Text.Trim() == "")
            ////    {
            ////        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Deduction 2 Amount....!');", true);
            ////        ErrFlag = true;
            ////    }
            ////    if (!ErrFlag)
            ////    {
            ////        //string temp = (Convert.ToDecimal(txtbase.Text.Trim()) + Convert.ToDecimal(txtFDA.Text.Trim()) + Convert.ToDecimal(txtVDA.Text.Trim()) + Convert.ToDecimal(txthra.Text.Trim())).ToString();
            ////        //temp = (Math.Round(Convert.ToDecimal(temp), 4, MidpointRounding.ToEven)).ToString();
            ////        //if (Convert.ToDecimal(temp) != Convert.ToDecimal(txttotal.Text))
            ////        //{
            ////        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Cick the Calculate....!');", true);
            ////        //    ErrFlag = true;
            ////        //}
            ////        if (!ErrFlag)
            ////        {
            ////            SalaryMasterClass objsal = new SalaryMasterClass();

            ////            objsal.EmpNo = ddlempNo.SelectedValue;
            ////            objsal.Ccode = SessionCcode;
            ////            objsal.Lcode = SessionLcode;
            ////            objsal.Base = txtbase.Text;
            ////            objsal.HRA = txthra.Text;
            ////            objsal.FDA = txtFDA.Text;
            ////            objsal.VDA = txtVDA.Text;
            ////            objsal.union = txtUnion.Text;
            ////            //objsal.total = txttotal.Text;
            ////            objsal.Allowance1Amt = txAllowance1.Text;
            ////            objsal.Allowance2Amt = txtAllowance2.Text;
            ////            objsal.deduction1 = txtDeduction1.Text;
            ////            objsal.deduction2 = txtDeduction2.Text;
            ////            objsal.EmpType = EmpType;
            ////            string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
            ////            if (empCodeverify == ddlempNo.SelectedValue)
            ////            {
            ////                objdata.SalaryMaster_Update(objsal);
            ////                Update = true;
            ////            }
            ////            else
            ////            {
            ////                objdata.SalaryMaster_Insert(objsal);
            ////                Insert = true;
            ////            }
            ////            if (Insert == true)
            ////            {
            ////                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);

            ////            }
            ////            if (Update == true)
            ////            {
            ////                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully....!');", true);

            ////            }
            ////        }
            ////    }
            ////}
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnreset_Click(object sender, EventArgs e)
    {
        Response.Redirect("SalaryMaster.aspx");
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        clr();
    }

    protected void ChkAllDept_CheckedChanged(object sender, EventArgs e)
    {
        if (ChkAllDept.Checked == true)
        {
            txtTokenNo.Enabled = true;

        }
        else
        {
            txtTokenNo.Enabled = false;
            txtTokenNo.Text = "";
        }
    }

    protected void btnTokenNoSearch_Click(object sender, EventArgs e)
    {
        //PanelSpecial.Visible = false;
        //Panelall.Visible = false;
        PanelStaff.Visible = false;
        //PanelDaily.Visible = false;
        bool ErrFlag = false;
        if (txtTokenNo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Token No...!');", true);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            //panelError.Visible = true;
            if (ddlcategory.SelectedValue == "1")
            {
                staffLabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                staffLabour = "L";
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category...!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select the Category";
                ErrFlag = true;
            }
            if (!ErrFlag)
            {


                DataTable dtempcode = new DataTable();
                dtempcode = objdata.EmployeeLoadSalaryMasterAllDept(staffLabour, SessionCcode, SessionLcode, txtTokenNo.Text.ToString());

                if (dtempcode.Rows.Count > 0)
                {

                    //Department Add And Select
                    ddlcategory_SelectedIndexChanged(sender, e);
                    ddlDepartment.SelectedValue = dtempcode.Rows[0]["Department"].ToString();

                    // Department Token No Add
                    btnclick_Click(sender, e);

                    //ddlempNo.DataSource = dtempcode;
                    //DataRow dr = dtempcode.NewRow();
                    //dr["EmpNo"] = ddlDepartment.SelectedItem.Text;
                    //dr["EmpName"] = ddlDepartment.SelectedItem.Text;
                    //dr["ExisistingCode"] = ddlDepartment.SelectedItem.Text;
                    //dtempcode.Rows.InsertAt(dr, 0);


                    //ddlempNo.DataTextField = "EmpNo";

                    //ddlempNo.DataValueField = "EmpNo";
                    //ddlempNo.DataBind();
                    //ddlEmpName.DataSource = dtempcode;
                    //ddlEmpName.DataTextField = "EmpName";
                    //ddlEmpName.DataValueField = "EmpNo";
                    //ddlEmpName.DataBind();

                    //ddToken.DataSource = dtempcode;
                    //ddToken.DataTextField = "ExisistingCode";
                    //ddToken.DataValueField = "ExisistingCode";
                    //ddToken.DataBind();

                    //Select Token No And EmpNo And Emp Name
                    ddToken.SelectedValue = dtempcode.Rows[0]["ExisistingCode"].ToString();
                    ddlEmpName.SelectedValue = dtempcode.Rows[0]["EmpNo"].ToString();
                    ddlempNo.SelectedValue = dtempcode.Rows[0]["EmpNo"].ToString();
                    ddToken_SelectedIndexChanged(sender, e);
                    //PanelStaff.Visible = true;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Token No Not Found...!');", true);
                    //System.Windows.Forms.MessageBox.Show("Select the Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //lblError.Text = "Select the Category";
                    ErrFlag = true;
                }
            }
            //clr();
        }
    }

    protected void ddToken_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            
            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Category Staff or Labour');", true);

                ErrFlag = true;
            }
            else if (ddlDepartment.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Department');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Department", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //lblError.Text = "Select Department";
                ErrFlag = true;
            }
            //else if (txtexistingNo.Text == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Existing Number');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Existing Number", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    //lblError.Text = "Enter the Existing Number";
            //    ErrFlag = true;
            //}
            if (!ErrFlag)
            {
                DataTable dtempcode = new DataTable();
                string Cate = "";
                if (ddlcategory.SelectedValue == "1")
                {
                    Cate = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    Cate = "L";
                }

                dtempcode = objdata.Salary_ExistNo(ddlDepartment.SelectedValue, ddToken.SelectedValue, Cate, SessionCcode, SessionLcode, SessionAdmin);
                EmpType = dtempcode.Rows[0]["EmployeeType"].ToString();

                if (EmpType == "1")
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = false;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "2")
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = true;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "3")
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = true;
                    //Panelall.Visible = false;
                    //PanelSpecial.Visible = false;
                }
                else if (EmpType == "4")
                {
                    //PanelSpecial.Visible = true;
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = false;
                }
                else
                {
                    PanelStaff.Visible = true;
                    //PanelDaily.Visible = false;
                    //Panelall.Visible = true;
                    //PanelSpecial.Visible = false;
                }
                if (dtempcode.Rows.Count > 0)
                {
                    ddlempNo.SelectedValue = dtempcode.Rows[0]["EmpNo"].ToString();
                    ddlEmpName.SelectedValue = dtempcode.Rows[0]["EmpNo"].ToString();
                    //ddlEmpName.DataSource = dtempcode;
                    //ddlEmpName.DataTextField = "EmpName";
                    //ddlEmpName.DataValueField = "EmpNo";
                    //ddlEmpName.DataBind();

                    if (dtempcode.Rows[0]["Wagestype"].ToString() == "1")
                    {
                        rbsalary.SelectedValue = "1";
                    }
                    else if (dtempcode.Rows[0]["Wagestype"].ToString() == "2")
                    {
                        rbsalary.SelectedValue = "2";
                    }
                    else if (dtempcode.Rows[0]["Wagestype"].ToString() == "3")
                    {
                        rbsalary.SelectedValue = "3";
                    }
                    string empCodeverify = objdata.SalaryMasterVerify(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                    if (empCodeverify == ddlempNo.SelectedValue)
                    {
                        DataTable dtempLoad = new DataTable();
                        dtempLoad = objdata.SalaryMasterEmpLoad(ddlempNo.SelectedValue, SessionCcode, SessionLcode);
                        if (dtempLoad.Rows.Count > 0)
                        {
                            txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                            txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            txtPF.Text = dtempLoad.Rows[0]["PFS"].ToString();
                            //Profilelbl.Text = dtempLoad.Rows[0]["ProfileType"].ToString();
                            //if (EmpType == "1")
                            //{
                            //    txtSBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtSall1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtSall2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtSded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtSded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}
                            //else if (EmpType == "2")
                            //{
                            //    txtbase.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtFDA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                            //    txthra.Text = dtempLoad.Rows[0]["HRA"].ToString();
                            //    txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                            //    //txttotal.Text = dtempLoad.Rows[0]["Total"].ToString();
                            //    txtUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                            //    txAllowance1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtAllowance2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtDeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtDeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}
                            //else if (EmpType == "3")
                            //{
                            //    txtWBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtWAll1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtWAll2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtWded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtWded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}
                            //else if (EmpType == "4")
                            //{
                            //    txtOBasic.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtOUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                            //    txtOAll1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtOAll2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtOded1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtOded2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}
                            //else
                            //{
                            //    txtbase.Text = dtempLoad.Rows[0]["Base"].ToString();
                            //    txtFDA.Text = dtempLoad.Rows[0]["FDA"].ToString();
                            //    txthra.Text = dtempLoad.Rows[0]["HRA"].ToString();
                            //    txtVDA.Text = dtempLoad.Rows[0]["VDA"].ToString();
                            //    //txttotal.Text = dtempLoad.Rows[0]["Total"].ToString();
                            //    txtUnion.Text = dtempLoad.Rows[0]["Unioncharges"].ToString();
                            //    txAllowance1.Text = dtempLoad.Rows[0]["Alllowance1amt"].ToString();
                            //    txtAllowance2.Text = dtempLoad.Rows[0]["Allowance2amt"].ToString();
                            //    txtDeduction1.Text = dtempLoad.Rows[0]["Deduction1"].ToString();
                            //    txtDeduction2.Text = dtempLoad.Rows[0]["Deduction2"].ToString();
                            //}

                        }
                        else
                        {
                            
                            txtSBasic.Text = "0";
                            txtSall1.Text = "0";
                            txtSall2.Text = "0";
                            txtSded1.Text = "0";
                            txtSded2.Text = "0";
                           

                        }

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Data Not Found');", true);
                    clr();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
