﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Settlement.aspx.cs" Inherits="Settlement" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Final Settlement</li></h4> 
                    </ol>
            </div>
                
<div id="main-wrapper" class="container">
<div class="row">
<div class="col-md-12">
              
<div class="col-md-9">
<div class="panel panel-white">
<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Final Settlement</h4>
				</div>
				</div>
<form class="form-horizontal">
<div class="panel-body"> 
               
<div class="col-md-12">     
				        <div class="form-group row">
				       <asp:Label ID="lblEmployeeType" runat="server" class="col-sm-2 control-label" 
                                                        Text="Category"></asp:Label>
				 
					      <div class="col-sm-2">
					      <asp:DropDownList ID="ddlcategory" class="form-control" runat="server" AutoPostBack="true"
					       onselectedindexchanged="DropDownList1_SelectedIndexChanged">
                            </asp:DropDownList>  
					      </div>
					    
					
						<div class="col-md-1">
                          
                        </div>
                     <asp:Label ID="lblDepartment" runat="server" class="col-sm-2 control-label" 
                                                        Text="Department"></asp:Label>   
					
					   <div class="col-sm-2">
						<asp:DropDownList ID="ddldepartment" class="form-control" runat="server" AutoPostBack="true"
						 onselectedindexchanged="ddldepartment_SelectedIndexChanged">
                            </asp:DropDownList> 
					  </div>
						
					  <div class="col-sm-3">
                         <asp:Button ID="btnclick" class="btn btn-info"  runat="server" Text="Click" onclick="btnclick_Click"/>
                      </div>
						</div>
				        
                       </div>
 
 
<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblEmpNo" runat="server" class="col-sm-2 control-label" 
                                                        Text="Employee No"></asp:Label>  
				     
					   <div class="col-sm-3">
					      <asp:DropDownList ID="ddlEmpNo" class="form-control" runat="server" AutoPostBack="true" onselectedindexchanged="ddlEmpNo_SelectedIndexChanged">
                            </asp:DropDownList>  
					   </div>
				       <div class="col-md-1"></div>
				       <asp:Label ID="lblEmpName" runat="server" class="col-sm-2 control-label" 
                                                        Text="Employee Name"></asp:Label>
                	 
					   <div class="col-sm-3">
						<asp:DropDownList ID="ddlEmpName" class="form-control" runat="server" AutoPostBack="true" 
						onselectedindexchanged="ddlEmpName_SelectedIndexChanged">
                        </asp:DropDownList> 
					  </div>
						
					   </div>
				        
                       </div>
 
<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblExist" runat="server" class="col-sm-2 control-label" 
                                                    Text="Existing No"></asp:Label>
				     
					   <div class="col-sm-3">
                           <asp:TextBox ID="txtExist" class="form-control" ontextchanged="txtExist_TextChanged" runat="server"></asp:TextBox>	
                       </div>
				       <div class="col-md-1"></div>
                	   <div class="col-sm-2 control-label">
                           <asp:Button ID="btnSearch" class="btn btn-success" runat="server" onclick="btnSearch_Click" Text="Search" />
                	   </div>
					   					
					   </div>
				        
                       </div>
 
<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblDesignation" runat="server" class="col-sm-2 control-label" 
                                                        Text="Designation"></asp:Label>
 
   <div class="col-sm-3">
    <asp:Label ID="lblDesignation1" runat="server" Text=""></asp:Label>  
     </div>
  
     <div class="col-sm-1"></div>
      <asp:Label ID="lbldoj" runat="server" class="col-sm-2 control-label" 
                                                        Text="Date of Joining"></asp:Label>                  
  
    <div class="col-sm-3">
        <asp:Label ID="lbldoj1" runat="server" Text=""></asp:Label>
     </div>
		
   
	</div>
		        
 </div> 
  
<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblExperience" runat="server" class="col-sm-2 control-label" 
                                                        Text="Experience"></asp:Label> 
 
   <div class="col-sm-3">
    <asp:Label ID="lblExperience1" runat="server" Text=""></asp:Label>  
     </div>
  
     <div class="col-sm-1"></div>
     <asp:Label ID="lblresign" runat="server" class="col-sm-2 control-label" 
                                                        Text="Date of Resign"></asp:Label>                   
   
    <div class="col-sm-3">
        <asp:TextBox ID="txtresign" class="form-control" runat="server"></asp:TextBox>
       <cc1:CalendarExtender ID="Caleder_txtdob" runat="server" TargetControlID="txtresign"
        Format="dd-MM-yyyy" CssClass="orange">
        </cc1:CalendarExtender>
        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server"
          FilterMode="ValidChars" FilterType="Custom,Numbers"
          TargetControlID="txtresign" ValidChars="0123456789/-">
        </cc1:FilteredTextBoxExtender>
     </div>
		
   
	</div>
		        
 </div> 

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblAdvance" runat="server" class="col-sm-2 control-label" 
                                                        Text="Advance Amount"></asp:Label>
				      
					   <div class="col-sm-3">
                           <asp:TextBox ID="txtAdvance" class="form-control" runat="server" Text="0"></asp:TextBox>	
                       </div>
				       				   					
					   </div>
				        
                       </div>
 
 <div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblESI" runat="server" class="col-sm-2 control-label" 
                                                        Text="Eligible for ESI"></asp:Label>
				  
					   <div class="col-sm-3">
                           <asp:RadioButtonList ID="rbesi" runat="server" AutoPostBack="true" 
                                    Font-Bold="true" RepeatColumns="2" Enabled="false">
                             <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                             <asp:ListItem Text="No" Value="2"></asp:ListItem>
                           </asp:RadioButtonList>
                       </div>
				       				   					
					   </div>
				        
                       </div>

<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblpf" runat="server" class="col-sm-2 control-label" 
                                                        Text="PF Account No"></asp:Label>
  
   <div class="col-sm-3">
       <asp:TextBox ID="txtpfaccount" class="form-control" runat="server"></asp:TextBox>
     </div>
  
     <div class="col-sm-1"></div> 
     <asp:Label ID="lblESIAcc" runat="server" class="col-sm-2 control-label" 
                                                        Text="ESIC Account No"></asp:Label>                 
   
    <div class="col-sm-3">
        <asp:TextBox ID="txtESICacc" class="form-control" runat="server"></asp:TextBox>
     </div>
		
   
	</div>
		        
 </div>
 
 <div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblpf1" runat="server" class="col-sm-2 control-label" 
                                                        Text="PF Amount"></asp:Label>
  
   <div class="col-sm-3">
       <asp:TextBox ID="txtPF" class="form-control" Text="0" runat="server"></asp:TextBox>
     </div>
  
     <div class="col-sm-1"></div> 
     <asp:Label ID="lblESI1" runat="server" class="col-sm-2 control-label" 
                                                        Text="ESI Amount"></asp:Label>                 
 
    <div class="col-sm-3">
        <asp:TextBox ID="txtESI" class="form-control" Text="0" runat="server" Enabled="false"></asp:TextBox>
     </div>
		
   
	</div>
		        
 </div>
 
 <div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblReason" runat="server" class="col-sm-2 control-label" 
                                                        Text="Reason"></asp:Label>   
  
   <div class="col-sm-6">
       <asp:TextBox ID="txtReason" class="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
     </div>
  	</div>
		        
 </div>

<div class="col-md-12">     
				<div class="form-group row">
				
				<div class="col-sm-3"></div>	
			     
				
				 <div class="col-sm-3">
                     <asp:Button ID="btnSave" class="btn btn-success" runat="server" Text="Save" onclick="btnSave_Click" />
  				 </div>
  				
  				 <div class="col-sm-3">
                     <asp:Button ID="btncancel" class="btn btn-success" runat="server" Text="Clear" onclick="btncancel_Click"/>
  				 </div>
  				 
  				 				 				
			    </div>
				       
        </div> 

</div>
</form>
</div>
</div>

<!-- Dashboard start -->
 	
 	<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
 
    <!-- Dashboard End --> 


</div>
</div>
</div>

</ContentTemplate>

 <Triggers>
      <asp:PostBackTrigger ControlID="btnSave"  />
      <asp:PostBackTrigger ControlID="btncancel"  />                                             
      </Triggers>

</asp:UpdatePanel>



</asp:Content>

