﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AttenanceUpload.aspx.cs" Inherits="AttenanceUpload" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Attendance Upload</li></h4> 
                    </ol>
</div>

<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
    <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Attendance Upload</h4>
				</div>
				</div>
				
	<asp:UpdatePanel ID="PanelAttend" runat="server">
	   <ContentTemplate>
	     <form class="form-horizontal">
			
			
			
		                 <div align="center">
					       <asp:Label ID="Label1" for="input-Default" class="col-sm-12 control-label" 
                                Text="WAGES TYPE" runat="server" Font-Bold="True" Font-Underline="True"></asp:Label>
				        
				   </div>
				
				<div class="panel-body">
                        
                <div class="row">
					    <div class="form-group col-md-12">
					      
					    <div align="center">
					    
					        
					    
					    
					    
					         <div class="form-group col-md-2"></div>
					         <div class="form-group col-md-9">
					             <asp:RadioButtonList ID="rbsalary" runat="server" Font-Bold="true"
                                  RepeatColumns="3" >
                                  <asp:ListItem Text="Weekly Wages" Value="1"></asp:ListItem>
                                  <asp:ListItem Text="Monthly Wages" Value="2" ></asp:ListItem>                                                                                                                                       
                                  <asp:ListItem Text="Bi-Monthly Wages" Value="3"></asp:ListItem>
                                </asp:RadioButtonList>
                                
    					       
                             </div>
				        </div>
                     </div>
                </div>

                           
				         <div class="form-group row">
					  <asp:Label ID="Label3" runat="server" class="col-sm-2 control-label" 
                                                             Text="Financial Year"></asp:Label>
						<div class="col-sm-3">
                            <asp:DropDownList ID="ddlfinance" class="form-control" runat="server">
                            </asp:DropDownList>                          
                        </div>
					
				    <div class="col-sm-1"></div>
				       				    
				     <label for="input-Default" class="col-sm-2 control-label">Month</label>
					 <div class="col-sm-3">
                    <asp:DropDownList ID="ddlMonths" class="form-control" runat="server">
                            </asp:DropDownList>   
					  </div>
			       </div>
					    
					     <div class="form-group row">
					    
					    
					     <label for="input-Default" class="col-sm-2 control-label">From Date</label>
						<div class="col-sm-3">
                        <asp:TextBox ID="txtFrom" class="form-control" runat="server" OnTextChanged="txtFrom_TextChanged" AutoPostBack="true"></asp:TextBox>
                          
                        <cc1:CalendarExtender ID="Caleder_txtdob" runat="server" TargetControlID="txtFrom" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true">
                        </cc1:CalendarExtender>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR2" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtFrom" ValidChars="0123456789/- ">
                        </cc1:FilteredTextBoxExtender>
                       </div>
					    <div class="col-md-1"></div>
					   	<label for="input-Default" class="col-sm-2 control-label">To Date</label>
						<div class="col-sm-3">
						 <asp:TextBox ID="txtTo" class="form-control" runat="server" OnTextChanged="txtTo_TextChanged" AutoPostBack="true"></asp:TextBox>
						 
						 <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTo" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true">
						 </cc1:CalendarExtender>
                         <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR3" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtTo" ValidChars="0123456789/- ">
                         </cc1:FilteredTextBoxExtender>
						 
						 
						</div>
						
				        </div> 
				        
				        
				        <div class="form-group row">
					    
					   
					       <label for="input-Default" class="col-sm-2 control-label">No.of Working Days</label>
						<div class="col-sm-3">
                            <asp:TextBox ID="txtdays" Enabled="false"  class="form-control" runat="server"></asp:TextBox>
                        </div>
					    
						
						<div class="col-md-1"></div>
							<div class="col-sm-4">
                            <asp:FileUpload ID="FileUpload" class="btn btn-default btn-rounded" runat="server" />
                        </div>
						
				        </div>
				        
				        <div class="form-group row">
					    
					     
						<div class="col-sm-3">
						<%--<label for="input-Default"   class="col-sm-2 control-label">NFH Days</label>--%>
						  <asp:TextBox ID="txtNfh" Visible="false"   class="form-control" Text="0" runat="server"></asp:TextBox>
						</div>
					     
					     
					
					    
					   	<div class="col-md-1"></div>
						
						<div class="col-sm-3">
                            <asp:Button ID="btnUpload" class="btn btn-success" runat="server" Text="Upload" 
                                onclick="btnUpload_Click" />
						</div>
				        </div>
				        
			   </div>

            
            </form>
	   </ContentTemplate>
	   <Triggers>
	      <asp:PostBackTrigger ControlID="btnUpload" />
	   </Triggers>
	   
	</asp:UpdatePanel>			
			
			
			</div>
			</div>
		  
		    
		
 	  <!-- Dashboard start -->
 	
 	<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                            
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                            
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
 
    <!-- Dashboard End -->
 
  
          </div> 
 </div><!-- col 12 end -->
      
  </div><!-- row end -->

</asp:Content>

