﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RptDepartmentSalaryAbstract.aspx.cs" Inherits="RptDepartmentSalaryAbstract" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Employee Details</li></h4> 
                    </ol>
              </div>

<div id="main-wrapper" class="container">
<div class="row">
<div class="col-md-12">
              
 <div class="col-md-9">
<div class="panel panel-white">

<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">PF / ESI DETAILS</h4>
				</div>
				</div>
	
<form class="form-horizontal">

<div align="center">
<asp:Label ID="lblWagesType" for="input-Default" class="col-sm-12 control-label" 
 Text="Report Type" runat="server" Font-Bold="True" Font-Underline="True"></asp:Label>
</div>

<div class="panel-body"> 

<div class="row">
<div class="form-group col-md-12">
					      
<div align="center">
<div class="form-group col-md-2"></div>
<div class="form-group col-md-10">
<asp:RadioButtonList ID="rbsalary" runat="server" TabIndex="5" AutoPostBack="true" 
    RepeatColumns="4" onselectedindexchanged="rbsalary_SelectedIndexChanged">
 <asp:ListItem Text ="ESI" Value="1" Selected="True"></asp:ListItem>
 <asp:ListItem Text="ESI ONLINE" Value="2" ></asp:ListItem>                                                                                                                                       
 <asp:ListItem Text="PF" Value="3"></asp:ListItem>
 <asp:ListItem Text="PF ONLINE" Value="3"></asp:ListItem>
</asp:RadioButtonList>
</div>
</div>
</div>
</div>

<div class="col-md-12">     
<div class="form-group row">
				      <asp:Label ID="lblFrom_Month" runat="server" class="col-sm-2 control-label" 
                                                             Text="Month"></asp:Label>
					  <div class="col-sm-3">
                      <asp:DropDownList ID="txtFrom_Month" class="form-control" runat="server" AutoPostBack="true" onselectedindexchanged="txtFrom_Month_SelectedIndexChanged">
                      </asp:DropDownList>                   
					  </div>
				      <div class="col-md-1"></div>
				      <asp:Label ID="lblFinancialYear" runat="server" class="col-sm-2 control-label" 
                                                             Text="Financial Year"></asp:Label>
				      <div class="col-sm-3">
				      <asp:DropDownList ID="txtFinancial_Year" class="form-control" runat="server" AutoPostBack="true" onselectedindexchanged="txtFinancial_Year_SelectedIndexChanged" >
                      </asp:DropDownList> 
				      </div>
						
</div>
				        
</div>

<div class="col-md-12">     
				      <div class="form-group row">
				      <div class="col-sm-5"></div>
				      <div class="col-md-2">
                          <asp:Button ID="btnexport" class="btn btn-success" runat="server" Text="Report" onclick="btnexport_Click" />
				      </div>
				      
						
</div>
				        
</div>

<div class="col-md-12">     
				      <div class="form-group row">
				      <asp:Label ID="lblLeftDate" runat="server" class="col-sm-2 control-label" 
                                                             Text="Left Date"></asp:Label>
					  <div class="col-sm-3">
                     <asp:TextBox ID="txtLeftDate" runat="server" class="form-control" AutoPostBack="True" ontextchanged="txtLeftDate_TextChanged" ></asp:TextBox>                 
                     <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtLeftDate" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
                     <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR1" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtLeftDate" ValidChars="0123456789/- "></cc1:FilteredTextBoxExtender>
                                                            
					  </div>
				      <div class="col-md-1"></div>
				      <div class="col-sm-3">
				      <asp:CheckBox ID="ChkLeft" runat="server" Text="Left Employee" />
				      </div>
						
</div>
				        
</div>

<div class="col-md-12">
 <div class="form-group row">
<div align="center">
<asp:Label ID="Label1" for="input-Default" class="col-sm-12 control-label" 
 Text="PF / ESI ADDITION DELETION DETAILS" runat="server" Font-Bold="True" Font-Underline="True"></asp:Label>
</div>
</div>
</div>

<div class="col-md-12">     
				      <div class="form-group row">
				      <asp:Label ID="lblcategory" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
					  <div class="col-sm-3">
                      <asp:DropDownList ID="ddlcategory" class="form-control" runat="server" AutoPostBack="true"
                       OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged" >
                      </asp:DropDownList>                   
					  </div>
				      <div class="col-md-1"></div>
				      <asp:Label ID="lblEmployeeType" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee Type"></asp:Label>
				      <div class="col-sm-3">
				      <asp:DropDownList ID="txtEmployeeType" class="form-control" runat="server" AutoPostBack="true" >
                      </asp:DropDownList> 
				      </div>
						
</div>
				        
</div>

<div class="col-md-12">     
				      <div class="form-group row">
				      <asp:Label ID="Label2" runat="server" class="col-sm-2 control-label" 
                                                             Text="Month"></asp:Label>
					  <div class="col-sm-3">
                      <asp:DropDownList ID="txtPFMonth" class="form-control" runat="server" AutoPostBack="true"
                       onselectedindexchanged="txtPFMonth_SelectedIndexChanged" >
                      </asp:DropDownList>                   
					  </div>
				      <div class="col-md-1"></div>
				      <asp:Label ID="Label3" runat="server" class="col-sm-2 control-label" 
                                                             Text="Year"></asp:Label>
				      <div class="col-sm-3">
				      <asp:DropDownList ID="txtYear" class="form-control" runat="server" AutoPostBack="true" 
				      onselectedindexchanged="txtYear_SelectedIndexChanged">
                      </asp:DropDownList> 
				      </div>
						
</div>
				        
</div>

<div class="col-md-12">     
				      <div class="form-group row">
				      <asp:Label ID="lbl3AFromDate" runat="server" class="col-sm-2 control-label" 
                                                             Text="From Date"></asp:Label>
					  <div class="col-sm-3">
                      <asp:TextBox ID="txt3AFromDate" class="form-control" runat="server" ontextchanged="txt3AFromDate_TextChanged"></asp:TextBox>
                      <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txt3AFromDate" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
                      <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR2" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txt3AFromDate" ValidChars="0123456789/- "></cc1:FilteredTextBoxExtender>
                                                                          
					  </div>
				      <div class="col-md-1"></div>
				      <asp:Label ID="lbl3AToDate" runat="server" class="col-sm-2 control-label" 
                                                             Text="To Date"></asp:Label>
				      <div class="col-sm-3">
				      <asp:TextBox ID="txt3AToDate" class="form-control" runat="server" ontextchanged="txt3AToDate_TextChanged"></asp:TextBox>
                      <cc1:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="txt3AToDate" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
                      <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR3" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txt3AToDate" ValidChars="0123456789/- "></cc1:FilteredTextBoxExtender>
                           
				      </div>
						
</div>
				        
</div>

<div class="col-md-12">     
				      <div class="form-group row">
				      <div class="col-sm-3">
                          <asp:Button ID="btnPFForm3A" class="btn btn-success" runat="server" Text="Form 3A" onclick="btnPFForm3A_Click" />
				      </div> 
                                                           
					  <div class="col-sm-3">
                       <asp:Button ID="btnPFAddLeft" class="btn btn-success" runat="server" Text="PFAddDelete" onclick="btnPFAddLeft_Click" />
				      </div> 
				      <div class="col-md-3">
				     <asp:Button ID="btnESIAddLeft" class="btn btn-success" runat="server" Text="ESIAddDelete" onclick="btnESIAddLeft_Click" />
				      </div>
				     
				   <div class="col-sm-3">
				   <asp:Button ID="btnEmpAddLeft" class="btn btn-success" runat="server" Text="EmpAddDelete" onclick="btnEmpAddLeft_Click" />
				   </div>
						
</div>
				        
</div>

<div class="col-md-12">
<div class="form-group row">
<table>
<tbody>
<tr id="PFAddition" runat="server" visible="false">
                                                        <td colspan="4">
                                                            <%--PF Addition--%>
                                                            <asp:GridView ID="GVPFAddition" runat="server" AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Token No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblTokenNo" runat="server" Text= '<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>F.Name/Husband Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFName" runat="server" Text='<%# Eval("FatherName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>DOB</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PF DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFDOJ" runat="server" Text='<%# Eval("PFDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PF No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("PFnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESI NO</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESINo" runat="server" Text='<%# Eval("ESICnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                    
                                                                </Columns>
                                                            </asp:GridView>
                                                            <%--PF Deletion--%>
                                                            <asp:GridView ID="GVPFDeletion" runat="server" AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Token No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblTokenNo" runat="server" Text= '<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PF No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("PFnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESI NO</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESINo" runat="server" Text='<%# Eval("ESICnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                     
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PF DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFDOJ" runat="server" Text='<%# Eval("PFDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Left Date</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblLEFT_Date" runat="server" Text='<%# Eval("LEFT_Date") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                                                                                       
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    
<tr id="ESIAddition" runat="server" visible="false">
                                                        <td colspan="4">
                                                            <%--ESI Addition--%>
                                                            <asp:GridView ID="GVESIAddition" runat="server" AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Token No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblTokenNo" runat="server" Text= '<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>F.Name/Husband Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFName" runat="server" Text='<%# Eval("FatherName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>DOB</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESI DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFDOJ" runat="server" Text='<%# Eval("ESIDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PF No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("PFnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESI NO</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESINo" runat="server" Text='<%# Eval("ESICnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                            <%--ESI Deletion--%>
                                                            <asp:GridView ID="GVESIDeletion" runat="server" AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Token No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblTokenNo" runat="server" Text= '<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PF No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("PFnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESI NO</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESINo" runat="server" Text='<%# Eval("ESICnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                     
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESI DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFDOJ" runat="server" Text='<%# Eval("ESIDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Left Date</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblLEFT_Date" runat="server" Text='<%# Eval("LEFT_Date") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                                                                                       
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                
<tr id="EmpAddition" runat="server" visible="false">
                                                        <td colspan="4">
                                                            <%--Employee Addition--%>
                                                            <asp:GridView ID="GVEmpAddLeft" runat="server" AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Token No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblTokenNo" runat="server" Text= '<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>F.Name/Husband Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFName" runat="server" Text='<%# Eval("FatherName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Designation</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDOJ" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>PF DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFDOJ" runat="server" Text='<%# Eval("PFDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>PF No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("PFnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>ESI DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESIDOJ" runat="server" Text='<%# Eval("ESIDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>ESI No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESINo" runat="server" Text='<%# Eval("ESICnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Wages</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                            <%--Employee Deletion--%>
                                                            <asp:GridView ID="GVEmpDeletion" runat="server" AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Token No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblTokenNo" runat="server" Text= '<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>F.Name/Husband Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFName" runat="server" Text='<%# Eval("FatherName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Designation</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDOJ" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>PF DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFDOJ" runat="server" Text='<%# Eval("PFDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>PF No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("PFnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>ESI DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESIDOJ" runat="server" Text='<%# Eval("ESIDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>ESI No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESINo" runat="server" Text='<%# Eval("ESICnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Wages</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>Left Date</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblLeft_Date" runat="server" Text='<%# Eval("Left_Date") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>

</tbody>
</table>
</div>
</div>

<asp:Panel ID="Result_Panel" Visible="false" runat="server">
<div class="col-md-12">
<div class="form-group row">
<table class="full">
				                                <tbody>
				                                    <tr>
                                                        <td colspan="4">
                                                            <asp:GridView ID="griddept" runat="server" AutoGenerateColumns="false" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Staff Strength</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblStaff_Strength" runat="server" Text= '<%# Eval("Staff_Strength") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Staff Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblStaff_Salary" runat="server" Text='<%# Eval("Staff_Salary") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Labour Strength</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblLabour_Strength" runat="server" Text='<%# Eval("Labour_Strength") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Labour Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblLabour_Salary" runat="server" Text='<%# Eval("Labour_Salary") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Total Strength</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTotal_Strength" runat="server" Text='<%# Eval("Total_Strength") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Total Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTotal_Salary" runat="server" Text='<%# Eval("Total_Salary") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
				                                </tbody>
				                            </table>
</div>
</div>
</asp:Panel>


</div>
</form>
</div>
</div>


<!-- Dashboard start -->
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>  
 <!-- Dashboard End -->

</div>  <!-- col-md-12 End -->
</div>  <!-- row End -->
</div>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

