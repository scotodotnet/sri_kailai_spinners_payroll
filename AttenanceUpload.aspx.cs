﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.OleDb;
using System.Data.SqlClient;
using Payroll;




public partial class AttenanceUpload : System.Web.UI.Page
{

    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    AttenanceClass objatt = new AttenanceClass();
    string SessionCcode;
    string SessionLcode;
    string WagesType;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        if (SessionAdmin == "2")
        {
            Response.Redirect("EmployeeRegistration.aspx");

        }
        //lblComany.Text = SessionCcode + " - " + SessionLcode;
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        if (!IsPostBack)
        {
            Months_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        //lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    protected void btnUpload_Click1(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        try
        {

            if (ddlMonths.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if ((txtdays.Text.Trim() == "") || (txtdays.Text.Trim() == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Working days in a Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (Convert.ToInt32(txtdays.Text) == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtNfh.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To NFh Days Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (rbsalary.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Wges Type.');", true);
                ErrFlag = true;
            }
            //else if (Convert.ToInt32(txtdays.Text) > 31)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

            }
            if (!ErrFlag)
            {
                decimal total = (Convert.ToDecimal(txtdays.Text.Trim()));
                int total_check = 0;
                decimal days = 0;
                if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
                {
                    days = 31;
                }
                else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
                {
                    days = 30;
                }

                else if (ddlMonths.SelectedValue == "February")
                {
                    int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                    if ((yrs % 4) == 0)
                    {
                        days = 29;
                    }
                    else
                    {
                        days = 28;
                    }
                }
                if (total > days)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();
                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            SqlConnection cn = new SqlConnection(constr);

                            //Get Employee Master Details

                            DataTable dt_Emp_Mst = new DataTable();
                            string MachineID = dt.Rows[j][0].ToString();
                            string Query_Emp_mst = "";
                            Query_Emp_mst = "Select ED.EmpName,ED.MachineNo,ED.EmpNo,ED.ExisistingCode,MD.DepartmentNm, OP.Wagestype,";
                            Query_Emp_mst = Query_Emp_mst + " ED.BiometricID from EmployeeDetails ED inner Join MstDepartment MD on MD.DepartmentCd=ED.Department";
                            Query_Emp_mst = Query_Emp_mst + " inner Join OfficialProfile OP on OP.EmpNo=ED.EmpNo where ED.Ccode='" + SessionCcode + "'";
                            Query_Emp_mst = Query_Emp_mst + " and ED.Lcode='" + SessionLcode + "' and ED.ActivateMode='Y' and OP.Wagestype = '" + rbsalary.SelectedValue.ToString() + "'";
                            Query_Emp_mst = Query_Emp_mst + " and ED.BiometricID='" + MachineID + "'";
                            dt_Emp_Mst = objdata.RptEmployeeMultipleDetails(Query_Emp_mst);

                            if (dt_Emp_Mst.Rows.Count == 0)
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Token No Not Found in Employee Master. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }

                            string Department = dt_Emp_Mst.Rows[0]["DepartmentNm"].ToString();
                            string EmpNo = dt_Emp_Mst.Rows[0]["EmpNo"].ToString();

                            string ExistingCode = dt.Rows[j][1].ToString();
                            string FirstName = dt.Rows[j][2].ToString();
                            string WorkingDays = dt.Rows[j][3].ToString();
                            string CL = dt.Rows[j][4].ToString();
                            string AbsentDays = dt.Rows[j][5].ToString();
                            string OT_Days = dt.Rows[j][6].ToString();
                            string Full = dt.Rows[j][7].ToString();
                            string Home = dt.Rows[j][8].ToString();
                            string weekoff = "0";//dt.Rows[j][9].ToString();


                            //if (Department == "")
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Department. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Department. The Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            if (MachineID == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Machine ID. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            //else if (EmpNo == "")
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Number. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            else if (ExistingCode == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing Code. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Existing Code. The Roe Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (FirstName == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Name. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (WorkingDays == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Working Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            //else if (Home == "")
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Working Days. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            else if (CL == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the CL Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (AbsentDays == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the N/FH Days properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (OT_Days == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the OT Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Full == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the F/N Shift Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Home == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Canteen Days Minus. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            //else if ((Convert.ToDecimal(Half)) > Convert.ToDecimal(WorkingDays))
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the O/N Days Properly. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            //else if ((Convert.ToDecimal(Full)) > Convert.ToDecimal(WorkingDays))
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the O/N Days Properly. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            //else if ((Convert.ToDecimal(ThreeSided)) > Convert.ToDecimal(WorkingDays))
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ThreeSided Days Properly. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}

                            else if ((Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text)) > 31)
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }


                            //else if (weekoff == "")
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Weekoff Days Properly');", true);
                            //    ErrFlag = true;
                            //}
                            cn.Open();
                            string qry_dpt = "Select DepartmentNm from MstDepartment where DepartmentNm = '" + Department + "'";
                            SqlCommand cmd_dpt = new SqlCommand(qry_dpt, cn);
                            SqlDataReader sdr_dpt = cmd_dpt.ExecuteReader();
                            if (sdr_dpt.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            cn.Open();
                            string qry_dpt1 = "Select ED.Department from EmployeeDetails ED inner Join MstDepartment MSt on Mst.DepartmentCd=ED.Department where DepartmentNm = '" + Department + "' and ED.EmpNo='" + EmpNo + "'";
                            SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                            SqlDataReader sdr_dpt1 = cmd_dpt1.ExecuteReader();
                            if (sdr_dpt1.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            cn.Open();
                            string Qry_SalaryType = "Select EmpNo from Officialprofile where WagesType ='" + rbsalary.SelectedValue + "'";
                            SqlCommand cmd_wages = new SqlCommand(Qry_SalaryType, cn);
                            SqlDataReader sdr_wages = cmd_wages.ExecuteReader();
                            if (sdr_wages.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This " + EmpNo + " is Wages Type is incorrect.');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            cn.Open();
                            string qry_empNo = "Select EmpNo from EmployeeDetails where EmpNo= '" + EmpNo + "' and ExisistingCode = '" + ExistingCode + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                            SqlCommand cmd_Emp = new SqlCommand(qry_empNo, cn);
                            SqlDataReader sdr_Emp = cmd_Emp.ExecuteReader();
                            if (sdr_Emp.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Details Properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Details Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            total = (Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text.Trim())  + Convert.ToDecimal(AbsentDays));
                            days = 0;
                            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
                            {
                                days = 31;
                            }
                            else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
                            {
                                days = 30;
                            }
                            else if (ddlMonths.SelectedValue == "February")
                            {
                                int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                                if ((yrs % 4) == 0)
                                {
                                    days = 29;
                                }
                                else
                                {
                                    days = 28;
                                }
                            }
                            total_check = Convert.ToInt32(total);
                            if (total_check > Convert.ToInt32(days))
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                                ErrFlag = true;
                            }
                            //total_check = Convert.ToInt32(total);
                            //if (total_check < Convert.ToInt32(txtdays.Text))
                            //{
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                            //    ErrFlag = true;
                            //}
                            decimal Att_sum = (Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text.Trim()) + Convert.ToDecimal(AbsentDays));
                            int Att_Sum_Check = 0;
                            Att_Sum_Check = Convert.ToInt32(Att_sum);
                            if (Convert.ToInt32(days) < Att_Sum_Check)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Attenance Details Properly ...');", true);
                                ErrFlag = true;
                            }
                        }
                        if (!ErrFlag)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                //Get Employee Master Details
                                DataTable dt_Emp_Mst = new DataTable();
                                string MachineID = dt.Rows[i][0].ToString();
                                string Query_Emp_mst = "";
                                Query_Emp_mst = "Select ED.EmpName,ED.MachineNo,ED.EmpNo,ED.ExisistingCode,MD.DepartmentNm, OP.Wagestype,";
                                Query_Emp_mst = Query_Emp_mst + " ED.BiometricID from EmployeeDetails ED inner Join MstDepartment MD on MD.DepartmentCd=ED.Department";
                                Query_Emp_mst = Query_Emp_mst + " inner Join OfficialProfile OP on OP.EmpNo=ED.EmpNo where ED.Ccode='" + SessionCcode + "'";
                                Query_Emp_mst = Query_Emp_mst + " and ED.Lcode='" + SessionLcode + "' and ED.ActivateMode='Y' and OP.Wagestype = '" + rbsalary.SelectedValue.ToString() + "'";
                                Query_Emp_mst = Query_Emp_mst + " and ED.BiometricID='" + MachineID + "'";
                                dt_Emp_Mst = objdata.RptEmployeeMultipleDetails(Query_Emp_mst);

                                SqlConnection cn = new SqlConnection(constr);
                                cn.Open();
                                string qry_dpt1 = "Select DepartmentCd from MstDepartment where DepartmentNm = '" + dt_Emp_Mst.Rows[0]["DepartmentNm"].ToString() + "'";
                                SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                                DepartmentCode = Convert.ToString(cmd_dpt1.ExecuteScalar());
                                string qry_emp = "Select AT.EmpNo from AttenanceDetails AT inner JOin SalaryDetails SD on SD.EmpNo=AT.EmpNo where AT.EmpNo='" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and " +
                                                 "AT.Months='" + ddlMonths.Text + "' and AT.FinancialYear='" + ddlfinance.SelectedValue + "' and AT.Ccode='" + SessionCcode + "' and AT.Lcode='" + SessionLcode + "' and SD.Month='" + ddlMonths.Text + "'" +
                                                 " and SD.FinancialYear='" + ddlfinance.SelectedValue + "' and SD.Process_Mode='1' and SD.Ccode='" + SessionCcode + "' and SD.Lcode='" + SessionLcode + "' and convert(datetime,AT.TODate,105) >= convert(datetime,'" + txtFrom.Text + "',105)";
                                //string qry_emp = "Select EmpNo from AttenanceDetails where EmpNo = '" + dt.Rows[i][2].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                SqlCommand cmd_verify = new SqlCommand(qry_emp, cn);
                                Name_Upload = Convert.ToString(cmd_verify.ExecuteScalar());
                                string qry_emp1 = "Select EmpNo from AttenanceDetails where EmpNo = '" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(datetime,TODate,105) >= convert(datetime,'" + txtFrom.Text + "',105)";
                                SqlCommand cmd_verify1 = new SqlCommand(qry_emp1, cn);
                                Name_Upload1 = Convert.ToString(cmd_verify1.ExecuteScalar());
                                string qry_Type = "Select Wagestype from Officialprofile where EmpNo = '" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                SqlCommand cmd_verify2 = new SqlCommand(qry_Type, cn);
                                WagesType = Convert.ToString(cmd_verify2.ExecuteScalar());



                                objatt.department = DepartmentCode;
                                objatt.EmpNo = dt_Emp_Mst.Rows[0]["EmpNo"].ToString();
                                objatt.Exist = dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString();
                                objatt.EmpName = dt_Emp_Mst.Rows[0]["EmpName"].ToString();
                                objatt.days = dt.Rows[i][3].ToString();

                                objatt.Months = ddlMonths.Text;

                                if (dt.Rows[i][13].ToString().Trim() == "")
                                {
                                    objatt.TotalDays = (Convert.ToInt32(txtdays.Text)).ToString();
                                }
                                else
                                {
                                    objatt.TotalDays = dt.Rows[i][13].ToString();
                                }

                                //objatt.TotalDays = (Convert.ToInt32(txtdays.Text)).ToString();


                                objatt.finance = ddlfinance.SelectedValue;
                                objatt.Ccode = SessionCcode;
                                objatt.Lcode = SessionLcode;
                                objatt.nfh = txtNfh.Text.Trim();
                                objatt.Absent = "0";

                                if (dt.Rows[i][5].ToString().Trim() == "")      //   N/FH
                                {
                                    objatt.nfh = "0";
                                }
                                else
                                {
                                    objatt.nfh = dt.Rows[i][5].ToString();
                                }

                                if (dt.Rows[i][4].ToString().Trim() == "")
                                {
                                    objatt.cl = "0";
                                }

                                else
                                {
                                    objatt.cl = dt.Rows[i][4].ToString();
                                }
                                //if (dt.Rows[i][5].ToString().Trim() == "")
                                //{
                                //    objatt.Absent = "0";
                                //}

                                //else
                                //{
                                //    objatt.Absent = dt.Rows[i][5].ToString();
                                //}
                                if (dt.Rows[i][6].ToString().Trim() == "") //OT DAYS
                                {
                                    objatt.ThreeSided = "0";
                                }

                                else
                                {
                                    objatt.ThreeSided = dt.Rows[i][6].ToString();
                                }
                                if (dt.Rows[i][7].ToString().Trim() == "") { objatt.Full = "0"; } else { objatt.Full = dt.Rows[i][7].ToString(); }
                                if (dt.Rows[i][8].ToString().Trim() == "") { objatt.HomeDays = "0"; } else { objatt.HomeDays = dt.Rows[i][8].ToString(); }
                                //if (dt.Rows[i][9].ToString().Trim() == "") { objatt.weekoff = "0"; } else { objatt.weekoff = dt.Rows[i][9].ToString(); }
                                objatt.weekoff = "0";
                                if (dt.Rows[i][9].ToString().Trim() == "") { objatt.OTHoursNew = "0"; } else { objatt.OTHoursNew = dt.Rows[i][9].ToString(); }

                                //objatt.HomeDays = dt.Rows[i][7].ToString();
                                //objatt.Absent = dt.Rows[i][7].ToString();
                                //objatt.weekoff = dt.Rows[i][8].ToString();
                                //objatt.half = "0";
                                //objatt.Full = "0";
                                //objatt.ThreeSided = "0";



                                //if (WagesType == "1")
                                //{
                                //    objatt.half = dt.Rows[i][10].ToString();
                                //    objatt.Full = dt.Rows[i][11].ToString();
                                //    objatt.ThreeSided = "0";
                                //}
                                //else if (WagesType == "3")
                                //{
                                //    objatt.half = "0";
                                //    objatt.Full = "0";
                                //    objatt.ThreeSided = dt.Rows[i][12].ToString();
                                //}
                                //else
                                //{
                                //    objatt.half = "0";
                                //    objatt.Full = "0";
                                //    objatt.ThreeSided = "0";
                                //}

                                objatt.workingdays = txtdays.Text;

                                if (dt.Rows[i][10].ToString().Trim() == "") { objatt.WH_Work_Days = "0"; } else { objatt.WH_Work_Days = dt.Rows[i][10].ToString(); }
                                if (dt.Rows[i][11].ToString().Trim() == "") { objatt.Fixed_Work_Days = "0"; } else { objatt.Fixed_Work_Days = dt.Rows[i][11].ToString(); }
                                if (dt.Rows[i][12].ToString().Trim() == "") { objatt.NFH_Work_Days = "0"; } else { objatt.NFH_Work_Days = dt.Rows[i][12].ToString(); }

                                if (dt.Rows[i][14].ToString().Trim() == "") { objatt.NFH_Work_Days_Manual = "0"; } else { objatt.NFH_Work_Days_Manual = dt.Rows[i][14].ToString(); }
                                if (dt.Rows[i][15].ToString().Trim() == "") { objatt.NFH_Work_Days_Statutory = "0"; } else { objatt.NFH_Work_Days_Statutory = dt.Rows[i][15].ToString(); }

                                cn.Close();
                                if (Name_Upload == dt.Rows[i][2].ToString())
                                {
                                    //string Del = "Delete from AttenanceDetails where EmpNo = '" + dt.Rows[i][2].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                    //cn.Open();
                                    //SqlCommand cmd_del = new SqlCommand(Del, cn);
                                    ////cn.Open();
                                    //cmd_del.ExecuteNonQuery();
                                    //cn.Close();
                                }
                                else if (Name_Upload1 == dt_Emp_Mst.Rows[0]["EmpNo"].ToString())
                                {
                                    string Del = "Delete from AttenanceDetails where EmpNo = '" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(Datetime,FromDate,105) = Convert(Datetime,'" + txtFrom.Text.Trim() + "', 105)";
                                    cn.Open();
                                    SqlCommand cmd_del = new SqlCommand(Del, cn);
                                    cmd_del.ExecuteNonQuery();
                                    cn.Close();
                                    DateTime MyDate = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                    DateTime MyDate1 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                                    objdata.Attenance_insert(objatt, MyDate, MyDate1, rbsalary.SelectedValue);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                }
                                else
                                {
                                    DateTime MyDate = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                    DateTime MyDate1 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                                    objdata.Attenance_insert(objatt, MyDate, MyDate1, rbsalary.SelectedValue);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                }
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                sSourceConnection.Close();
                            }

                            //System.Windows.Forms.MessageBox.Show("Uploaded SuccessFully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

                        }
                        if (ErrFlag == true)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your File Not Upload...');", true);
                            //System.Windows.Forms.MessageBox.Show("Your File Not Uploaded", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool flag = false;
        try
        {
            if (this.ddlMonths.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Select the Month.');", true);
                flag = true;
            }
            else if ((this.txtdays.Text.Trim() == "") || (this.txtdays.Text.Trim() == null))
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Days.');", true);
                flag = true;
            }
            else if (Convert.ToInt32(this.txtdays.Text) == 0)
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the days Properly.');", true);
                flag = true;
            }
            else if (this.txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                flag = true;
            }
            else if (this.txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                flag = true;
            }
            else if (this.txtNfh.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the To NFh Days Properly.');", true);
                flag = true;
            }
            if (this.rbsalary.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Select the Wges Type.');", true);
                flag = true;
            }
            if (this.FileUpload.HasFile)
            {
                this.FileUpload.SaveAs(base.Server.MapPath("Upload/" + this.FileUpload.FileName));
            }
            if (!flag)
            {
                decimal num = Convert.ToDecimal(this.txtdays.Text.Trim());
                decimal num3 = 0M;
                if ((((this.ddlMonths.SelectedValue == "January") || (this.ddlMonths.SelectedValue == "March")) || ((this.ddlMonths.SelectedValue == "May") || (this.ddlMonths.SelectedValue == "July"))) || (((this.ddlMonths.SelectedValue == "August") || (this.ddlMonths.SelectedValue == "October")) || (this.ddlMonths.SelectedValue == "December")))
                {
                    num3 = 31M;
                }
                else if (((this.ddlMonths.SelectedValue == "April") || (this.ddlMonths.SelectedValue == "June")) || ((this.ddlMonths.SelectedValue == "September") || (this.ddlMonths.SelectedValue == "November")))
                {
                    num3 = 30M;
                }
                else if (this.ddlMonths.SelectedValue == "February")
                {
                    int num4 = Convert.ToInt32(this.ddlfinance.SelectedValue) + 1;
                    if ((num4 % 4) == 0)
                    {
                        num3 = 29M;
                    }
                    else
                    {
                        num3 = 28M;
                    }
                }
                if (num > num3)
                {
                    ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the days properly.');", true);
                    flag = true;
                }
                if (!flag)
                {
                    OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + base.Server.MapPath("Upload/" + this.FileUpload.FileName) + ";Extended Properties=Excel 8.0;");
                    new DataTable();
                    using (connection)
                    {
                        connection.Open();
                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", connection);
                        connection.Close();
                        using (connection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            connection.Open();
                        }
                        using (OleDbDataReader reader = command.ExecuteReader())
                        {
                            bool hasRows = reader.HasRows;
                        }
                        OleDbDataAdapter adapter = new OleDbDataAdapter
                        {
                            SelectCommand = command
                        };
                        DataSet dataSet = new DataSet();
                        adapter.Fill(dataSet);
                        DataTable table = new DataTable();
                        table = dataSet.Tables[0];
                        for (int i = 0; i < table.Columns.Count; i++)
                        {
                            table.Columns[i].ColumnName = table.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();
                        }
                        string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        for (int j = 0; j < table.Rows.Count; j++)
                        {
                            SqlConnection connection2 = new SqlConnection(connectionString);
                            DataTable table2 = new DataTable();
                            string str3 = table.Rows[j][0].ToString();
                            string str4 = "";
                            str4 = "Select ED.EmpName,ED.MachineNo,ED.EmpNo,ED.ExisistingCode,MD.DepartmentNm, OP.Wagestype,";
                            str4 = (str4 + " ED.BiometricID from EmployeeDetails ED inner Join MstDepartment MD on MD.DepartmentCd=ED.Department") + " inner Join OfficialProfile OP on OP.EmpNo=ED.EmpNo where ED.Ccode='" + this.SessionCcode + "'";
                            str4 = (str4 + " and ED.Lcode='" + this.SessionLcode + "' and ED.ActivateMode='Y' and OP.Wagestype = '" + this.rbsalary.SelectedValue.ToString() + "'") + " and ED.BiometricID='" + str3 + "'";
                            table2 = this.objdata.RptEmployeeMultipleDetails(str4);
                            if (table2.Rows.Count == 0)
                            {
                                j += 2;
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Token No Not Found in Employee Master. The Row Number is " + j + "');", true);
                                flag = true;
                            }
                            string str5 = table2.Rows[0]["DepartmentNm"].ToString();
                            string str6 = table2.Rows[0]["EmpNo"].ToString();
                            string str7 = table.Rows[j][1].ToString();
                            string str8 = table.Rows[j][2].ToString();
                            string str9 = table.Rows[j][3].ToString();
                            string str10 = table.Rows[j][4].ToString();
                            string str11 = table.Rows[j][5].ToString();
                            string str12 = table.Rows[j][6].ToString();
                            string str13 = table.Rows[j][7].ToString();
                            string str14 = table.Rows[j][8].ToString();
                            if (str3 == "")
                            {
                                j += 2;
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Machine ID. The Row Number is " + j + "');", true);
                                flag = true;
                            }
                            else if (str7 == "")
                            {
                                j += 2;
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Existing Code. The Row Number is " + j + "');", true);
                                flag = true;
                            }
                            else if (str8 == "")
                            {
                                j += 2;
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Name. The Row Number is " + j + "');", true);
                                flag = true;
                            }
                            else if (str9 == "")
                            {
                                j += 2;
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Working Days. The Row Number is " + j + "');", true);
                                flag = true;
                            }
                            else if (str10 == "")
                            {
                                j += 2;
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the CL Days. The Row Number is " + j + "');", true);
                                flag = true;
                            }
                            else if (str11 == "")
                            {
                                j += 2;
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the N/FH Days properly. The Row Number is " + j + "');", true);
                                flag = true;
                            }
                            else if (str12 == "")
                            {
                                j += 2;
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the OT Days. The Row Number is " + j + "');", true);
                                flag = true;
                            }
                            else if (str13 == "")
                            {
                                j += 2;
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the F/N Shift Days. The Row Number is " + j + "');", true);
                                flag = true;
                            }
                            else if (str14 == "")
                            {
                                j += 2;
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Canteen Days Minus. The Row Number is " + j + "');", true);
                                flag = true;
                            }
                            else if ((Convert.ToDecimal(str9) + Convert.ToDecimal(this.txtNfh.Text)) > 31M)
                            {
                                j += 2;
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Days properly. The Row Number is " + j + "');", true);
                                flag = true;
                            }
                            connection2.Open();
                            SqlCommand command3 = new SqlCommand("Select DepartmentNm from MstDepartment where DepartmentNm = '" + str5 + "'", connection2);
                            if (!command3.ExecuteReader().HasRows)
                            {
                                j += 2;
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                flag = true;
                            }
                            connection2.Close();
                            connection2.Open();
                            SqlCommand command4 = new SqlCommand("Select ED.Department from EmployeeDetails ED inner Join MstDepartment MSt on Mst.DepartmentCd=ED.Department where DepartmentNm = '" + str5 + "' and ED.EmpNo='" + str6 + "'", connection2);
                            if (!command4.ExecuteReader().HasRows)
                            {
                                j += 2;
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                flag = true;
                            }
                            connection2.Close();
                            connection2.Open();
                            SqlCommand command5 = new SqlCommand("Select EmpNo from Officialprofile where WagesType ='" + this.rbsalary.SelectedValue + "'", connection2);
                            if (!command5.ExecuteReader().HasRows)
                            {
                                j += 2;
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('This " + str6 + " is Wages Type is incorrect.');", true);
                                flag = true;
                            }
                            connection2.Close();
                            connection2.Open();
                            SqlCommand command6 = new SqlCommand("Select EmpNo from EmployeeDetails where EmpNo= '" + str6 + "' and ExisistingCode = '" + str7 + "' and Ccode='" + this.SessionCcode + "' and Lcode='" + this.SessionLcode + "'", connection2);
                            if (!command6.ExecuteReader().HasRows)
                            {
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Employee Details Properly. The Row Number is " + (j + 2) + "');", true);
                                flag = true;
                            }
                            connection2.Close();
                            num = (Convert.ToDecimal(str9) + Convert.ToDecimal(this.txtNfh.Text.Trim())) + Convert.ToDecimal(str11);
                            num3 = 0M;
                            if ((((this.ddlMonths.SelectedValue == "January") || (this.ddlMonths.SelectedValue == "March")) || ((this.ddlMonths.SelectedValue == "May") || (this.ddlMonths.SelectedValue == "July"))) || (((this.ddlMonths.SelectedValue == "August") || (this.ddlMonths.SelectedValue == "October")) || (this.ddlMonths.SelectedValue == "December")))
                            {
                                num3 = 31M;
                            }
                            else if (((this.ddlMonths.SelectedValue == "April") || (this.ddlMonths.SelectedValue == "June")) || ((this.ddlMonths.SelectedValue == "September") || (this.ddlMonths.SelectedValue == "November")))
                            {
                                num3 = 30M;
                            }
                            else if (this.ddlMonths.SelectedValue == "February")
                            {
                                int num7 = Convert.ToInt32(this.ddlfinance.SelectedValue) + 1;
                                if ((num7 % 4) == 0)
                                {
                                    num3 = 29M;
                                }
                                else
                                {
                                    num3 = 28M;
                                }
                            }
                            if (Convert.ToInt32(num) > Convert.ToInt32(num3))
                            {
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the days properly.');", true);
                                flag = true;
                            }
                            decimal num8 = (Convert.ToDecimal(str9) + Convert.ToDecimal(this.txtNfh.Text.Trim())) + Convert.ToDecimal(str11);
                            int num9 = 0;
                            num9 = Convert.ToInt32(num8);
                            if (Convert.ToInt32(num3) < num9)
                            {
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Attenance Details Properly ...');", true);
                                flag = true;
                            }
                        }
                        if (!flag)
                        {
                            for (int k = 0; k < table.Rows.Count; k++)
                            {
                                DataTable table3 = new DataTable();
                                string str19 = table.Rows[k][0].ToString();
                                string str20 = "";
                                str20 = "Select ED.EmpName,ED.MachineNo,ED.EmpNo,ED.ExisistingCode,MD.DepartmentNm, OP.Wagestype,";
                                str20 = (str20 + " ED.BiometricID from EmployeeDetails ED inner Join MstDepartment MD on MD.DepartmentCd=ED.Department") + " inner Join OfficialProfile OP on OP.EmpNo=ED.EmpNo where ED.Ccode='" + this.SessionCcode + "'";
                                str20 = (str20 + " and ED.Lcode='" + this.SessionLcode + "' and ED.ActivateMode='Y' and OP.Wagestype = '" + this.rbsalary.SelectedValue.ToString() + "'") + " and ED.BiometricID='" + str19 + "'";
                                table3 = this.objdata.RptEmployeeMultipleDetails(str20);
                                SqlConnection connection3 = new SqlConnection(connectionString);
                                connection3.Open();
                                this.DepartmentCode = Convert.ToString(new SqlCommand("Select DepartmentCd from MstDepartment where DepartmentNm = '" + table3.Rows[0]["DepartmentNm"].ToString() + "'", connection3).ExecuteScalar());
                                this.Name_Upload = Convert.ToString(new SqlCommand("Select AT.EmpNo from AttenanceDetails AT inner JOin SalaryDetails SD on SD.EmpNo=AT.EmpNo where AT.EmpNo='" + table3.Rows[0]["EmpNo"].ToString() + "' and AT.Months='" + this.ddlMonths.Text + "' and AT.FinancialYear='" + this.ddlfinance.SelectedValue + "' and AT.Ccode='" + this.SessionCcode + "' and AT.Lcode='" + this.SessionLcode + "' and SD.Month='" + this.ddlMonths.Text + "' and SD.FinancialYear='" + this.ddlfinance.SelectedValue + "' and SD.Process_Mode='1' and SD.Ccode='" + this.SessionCcode + "' and SD.Lcode='" + this.SessionLcode + "' and convert(datetime,AT.TODate,105) >= convert(datetime,'" + this.txtFrom.Text + "',105)", connection3).ExecuteScalar());
                                this.Name_Upload1 = Convert.ToString(new SqlCommand("Select EmpNo from AttenanceDetails where EmpNo = '" + table3.Rows[0]["EmpNo"].ToString() + "' and Months = '" + this.ddlMonths.Text + "' and FinancialYear = '" + this.ddlfinance.SelectedValue + "' and Ccode='" + this.SessionCcode + "' and Lcode='" + this.SessionLcode + "' and convert(datetime,TODate,105) >= convert(datetime,'" + this.txtFrom.Text + "',105)", connection3).ExecuteScalar());
                                this.WagesType = Convert.ToString(new SqlCommand("Select Wagestype from Officialprofile where EmpNo = '" + table3.Rows[0]["EmpNo"].ToString() + "' and Ccode='" + this.SessionCcode + "' and Lcode='" + this.SessionLcode + "'", connection3).ExecuteScalar());
                                this.objatt.department = this.DepartmentCode;
                                this.objatt.EmpNo = table3.Rows[0]["EmpNo"].ToString();
                                this.objatt.Exist = table3.Rows[0]["ExisistingCode"].ToString();
                                this.objatt.EmpName = table3.Rows[0]["EmpName"].ToString();
                                this.objatt.days = table.Rows[k][3].ToString();
                                this.objatt.Months = this.ddlMonths.Text;
                                if (table.Rows[k][13].ToString().Trim() == "")
                                {
                                    this.objatt.TotalDays = Convert.ToInt32(this.txtdays.Text).ToString();
                                }
                                else
                                {
                                    this.objatt.TotalDays = table.Rows[k][13].ToString();
                                }
                                this.objatt.finance = this.ddlfinance.SelectedValue;
                                this.objatt.Ccode = this.SessionCcode;
                                this.objatt.Lcode = this.SessionLcode;
                                this.objatt.nfh = this.txtNfh.Text.Trim();
                                this.objatt.Absent = "0";
                                if (table.Rows[k][5].ToString().Trim() == "")
                                {
                                    this.objatt.nfh = "0";
                                }
                                else
                                {
                                    this.objatt.nfh = table.Rows[k][5].ToString();
                                }
                                if (table.Rows[k][4].ToString().Trim() == "")
                                {
                                    this.objatt.cl = "0";
                                }
                                else
                                {
                                    this.objatt.cl = table.Rows[k][4].ToString();
                                }
                                if (table.Rows[k][6].ToString().Trim() == "")
                                {
                                    this.objatt.ThreeSided = "0";
                                }
                                else
                                {
                                    this.objatt.ThreeSided = table.Rows[k][6].ToString();
                                }
                                if (table.Rows[k][7].ToString().Trim() == "")
                                {
                                    this.objatt.Full = "0";
                                }
                                else
                                {
                                    this.objatt.Full = table.Rows[k][7].ToString();
                                }
                                if (table.Rows[k][8].ToString().Trim() == "")
                                {
                                    this.objatt.HomeDays = "0";
                                }
                                else
                                {
                                    this.objatt.HomeDays = table.Rows[k][8].ToString();
                                }
                                this.objatt.weekoff = "0";
                                if (table.Rows[k][9].ToString().Trim() == "")
                                {
                                    this.objatt.OTHoursNew = "0";
                                }
                                else
                                {
                                    this.objatt.OTHoursNew = table.Rows[k][9].ToString();
                                }
                                this.objatt.workingdays = this.txtdays.Text;
                                if (table.Rows[k][10].ToString().Trim() == "")
                                {
                                    this.objatt.WH_Work_Days = "0";
                                }
                                else
                                {
                                    this.objatt.WH_Work_Days = table.Rows[k][10].ToString();
                                }
                                if (table.Rows[k][11].ToString().Trim() == "")
                                {
                                    this.objatt.Fixed_Work_Days = "0";
                                }
                                else
                                {
                                    this.objatt.Fixed_Work_Days = table.Rows[k][11].ToString();
                                }
                                if (table.Rows[k][12].ToString().Trim() == "")
                                {
                                    this.objatt.NFH_Work_Days = "0";
                                }
                                else
                                {
                                    this.objatt.NFH_Work_Days = table.Rows[k][12].ToString();
                                }
                                if (table.Rows[k][14].ToString().Trim() == "")
                                {
                                    this.objatt.NFH_Work_Days_Manual = "0";
                                }
                                else
                                {
                                    this.objatt.NFH_Work_Days_Manual = table.Rows[k][14].ToString();
                                }
                                if (table.Rows[k][15].ToString().Trim() == "")
                                {
                                    this.objatt.NFH_Work_Days_Statutory = "0";
                                }
                                else
                                {
                                    this.objatt.NFH_Work_Days_Statutory = table.Rows[k][15].ToString();
                                }
                                connection3.Close();
                                if (this.Name_Upload != table.Rows[k][2].ToString())
                                {
                                    if (this.Name_Upload1 == table3.Rows[0]["EmpNo"].ToString())
                                    {
                                        string cmdText = "Delete from AttenanceDetails where EmpNo = '" + table3.Rows[0]["EmpNo"].ToString() + "' and Months = '" + this.ddlMonths.Text + "' and FinancialYear = '" + this.ddlfinance.SelectedValue + "' and Ccode='" + this.SessionCcode + "' and Lcode='" + this.SessionLcode + "' and convert(Datetime,FromDate,105) = Convert(Datetime,'" + this.txtFrom.Text.Trim() + "', 105)";
                                        connection3.Open();
                                        new SqlCommand(cmdText, connection3).ExecuteNonQuery();
                                        connection3.Close();
                                        DateTime fromDate = DateTime.ParseExact(this.txtFrom.Text, "dd-MM-yyyy", null);
                                        DateTime todate = DateTime.ParseExact(this.txtTo.Text, "dd-MM-yyyy", null);
                                        this.objdata.Attenance_insert(this.objatt, fromDate, todate, this.rbsalary.SelectedValue);
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                    }
                                    else
                                    {
                                        DateTime time3 = DateTime.ParseExact(this.txtFrom.Text, "dd-MM-yyyy", null);
                                        DateTime time4 = DateTime.ParseExact(this.txtTo.Text, "dd-MM-yyyy", null);
                                        this.objdata.Attenance_insert(this.objatt, time3, time4, this.rbsalary.SelectedValue);
                                        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                    }
                                }
                                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                connection.Close();
                            }
                        }
                        if (flag)
                        {
                            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Your File Not Upload...');", true);
                        }
                    }
                }
            }
        }
        catch (Exception)
        {
            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
        }
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    
   
   
    protected void txtFrom_TextChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        txtTo.Text = null;
        int val_Month = Convert.ToDateTime(txtFrom.Text).Month;
        string Mont = "";
        switch (val_Month)
        {
            case 1:
                Mont = "January";
                break;

            case 2:
                Mont = "February";
                break;
            case 3:
                Mont = "March";
                break;
            case 4:
                Mont = "April";
                break;
            case 5:
                Mont = "May";
                break;
            case 6:
                Mont = "June";
                break;
            case 7:
                Mont = "July";
                break;
            case 8:
                Mont = "August";
                break;
            case 9:
                Mont = "September";
                break;
            case 10:
                Mont = "October";
                break;
            case 11:
                Mont = "November";
                break;
            case 12:
                Mont = "December";
                break;
            default:
                break;
        }
        if (Mont != ddlMonths.SelectedValue)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Date Properly...');", true);
            ErrFlag = true;
            txtFrom.Text = null;
        }
    }
    protected void txtTo_TextChanged(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date...');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                DateTime dfrom = Convert.ToDateTime(txtFrom.Text);
                DateTime dtto = Convert.ToDateTime(txtTo.Text);
                if (dfrom >= dtto)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Date Properly...');", true);
                    ErrFlag = true;
                    txtTo.Text = null;
                }
                if (!ErrFlag)
                {
                    decimal NoDays = (dtto - dfrom).Days;
                    NoDays = NoDays + 1;
                    txtdays.Text = NoDays.ToString();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
