﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AttenanceDownload.aspx.cs" Inherits="AttenanceDownload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Attendance Download</li></h4> 
                    </ol>
             </div>
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
    <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Attendance Download</h4>
				</div>
				</div>
				<form class="form-horizontal">
				   <div align="center">
					       <asp:Label ID="Label1" for="input-Default" class="col-sm-12 control-label" 
                                Text="WAGES TYPE" runat="server" Font-Bold="True" Font-Underline="True"></asp:Label>
				        
				   </div>
				
				<div class="panel-body">
                        
            <div class="row">
					    <div class="form-group col-md-12">
					      
					    <div align="center">
					    
					        
					    
					    
					    
					     <div class="form-group col-md-2"></div>
					     <div class="form-group col-md-9">
					         <asp:RadioButtonList ID="rbsalary" runat="server" AutoPostBack="true" 
                              RepeatColumns="3" onselectedindexchanged="rbsalary_SelectedIndexChanged">
                              <asp:ListItem Text ="Weekly Wages" Value="1"></asp:ListItem>
                              <asp:ListItem Text="Monthly Wages" Value="2" ></asp:ListItem>                                                                                                                                       
                              <asp:ListItem Text="Bi-Monthly Wages" Value="3"></asp:ListItem>
                            </asp:RadioButtonList>
                            
					       
                         </div>
				            </div>
                     </div>
                </div>

                           
			<div class="form-group row">
					  <asp:Label ID="Label3" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
						<div class="col-sm-3">
                            <asp:DropDownList ID="ddlcategory" class="form-control" runat="server" AutoPostBack="True"  
                                                                        onselectedindexchanged="ddlcategory_SelectedIndexChanged">
                            </asp:DropDownList>                          
                        </div>
					
				    <div class="col-sm-1"></div>
				       				    
				     <label for="input-Default" class="col-sm-2 control-label">Employee Type</label>
					 <div class="col-sm-3">
                    <asp:DropDownList ID="txtEmployeeType" class="form-control" runat="server">
                            </asp:DropDownList>   
					  </div>
			       </div>
			
			<asp:Panel ID="Panelload" runat="server" Visible="false" >

			<div class="form-group row">
			<table class="full">
			<tbody>
			<tr>
                                                                <td colspan="5">
                                                                    <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="false" >
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>MachineID</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblMachineID" runat="server" Text='<%# Eval("BiometricID") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Token No</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblTokenNo" runat="server" Text='<%# Eval("ExisistingCode") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>FirstName</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("EmpName") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Days</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="Days" runat="server" Text="" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>H.Allowed</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="CL" runat="server" Text="0" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>N/FH</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblleave" runat="server" Text="0" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>OT Days</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblOTDays" runat="server" Text="0" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                       <asp:TemplateField>
                                                                            <HeaderTemplate>SPG Allow</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblfull" runat="server" Text="0" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Canteen Days Minus</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCanteenDaysMinus" runat="server" Text="0" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>OT Hours</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblRESTDAYS" runat="server" Text="0" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>W.H</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblWH_Work_Days" runat="server" Text="0" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Fixed W.Days</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFixed_Work_Days" runat="server" Text="0" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>NFH Double Wages Days</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblNFH_Double_Wages_Days" runat="server" Text="0" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Total Month Days</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblTotal_Month_Days" runat="server" Text="0" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>NFH Worked Days</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblNFH_Work_Days" runat="server" Text="0" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>NFH D.W Statutory</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblNFH_DW_Statutory" runat="server" Text="0" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                </td>
                                                            </tr>
			</tbody>
			</table>
			</div>
			</asp:Panel>
					    
				<div class="form-group row">
					 <div align="center">
                         <asp:Button ID="btnDownload" runat="server" class="btn btn-success" Text="Download" onclick="btnDownload_Click"/>
					 </div>
				        
			   </div>
              </div>   
			
			
		                        
			</form>
			
			</div>
			</div>
	  
		    
		
 	<!-- Dashboard start -->
 	
 	<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                           
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                           
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
 
    <!-- Dashboard End --> 
 
  
  </div> <!-- col 12 end -->
 </div><!-- row end -->
      
  </div>
</ContentTemplate>
<Triggers>
      <asp:PostBackTrigger ControlID="btnDownload"  />
 </Triggers>


</asp:UpdatePanel>
</asp:Content>

