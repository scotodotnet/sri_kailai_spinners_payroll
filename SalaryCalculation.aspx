﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SalaryCalculation.aspx.cs" Inherits="SalaryCalculation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Salary Calculation</li></h4> 
                    </ol>
                </div>
                <div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Salary Calculation</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
					
               
                
                <div class="row">
					    <div class="form-group col-md-12">
					      <div align="center">
					      <asp:Label ID="lblWagesType" runat="server" class="control-label" 
                                                        Font-Bold="true" Text="Wages Type"></asp:Label>
				       
				          </div>
					    <div align="center">
					    <div class="form-group col-md-2"></div>
					       <div class="form-group col-md-9">
					        <asp:RadioButtonList ID="rbsalary" runat="server" AutoPostBack="true" 
                             RepeatColumns="3" onselectedindexchanged="rbsalary_SelectedIndexChanged">
                              <asp:ListItem Text ="Weekly Wages" Value="1"></asp:ListItem>
                              <asp:ListItem Text="Monthly Wages" Value="2" ></asp:ListItem>                                                                                                                                       
                              <asp:ListItem Text="Bi-Monthly Wages" Value="3"></asp:ListItem>
                            </asp:RadioButtonList>
					  </div>
					   
                           
				            </div>
                     </div>
          
          
           <div class="col-md-12">     
				        <div class="form-group row">
				         <asp:Label ID="lblFinance" runat="server" class="col-sm-2 control-label" 
                                                        Text="Financial Year"></asp:Label>
				       
				         <div class="col-sm-3">
                              <asp:DropDownList ID="ddlfinance" class="form-control" runat="server">
                              </asp:DropDownList>
					      </div>
					    	
						    <div class="col-sm-1"></div>
						     <asp:Label ID="lblMonths" runat="server" class="col-sm-2 control-label" 
                                                         Text="Month for Attendance"></asp:Label>
				        			    
						      <div class="col-sm-3">                    
						       <asp:DropDownList ID="ddlMonths" class="form-control" runat="server">
                              </asp:DropDownList>
					 </div>
				
						</div>
				        
                       </div>
           
           
           <div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="lblcategory" runat="server" class="col-sm-2 control-label" 
                                                        Text="Category"></asp:Label>
				            <div class="col-sm-3">
                              <asp:DropDownList ID="ddlcategory" class="form-control" runat="server"
                              onselectedindexchanged="ddlcategory_SelectedIndexChanged" AutoPostBack="true">
                              </asp:DropDownList>
					      </div>
					    	
						    <div class="col-sm-1"></div>
						<asp:Label ID="lbltransferDate" runat="server" class="col-sm-2 control-label" 
                                                        Text="Transfer Date"></asp:Label>
				           
						    
						      <div class="col-sm-3">                    
                                  <asp:TextBox ID="txtsalaryday" class="form-control" runat="server"></asp:TextBox>
                                   <cc1:CalendarExtender ID="CalendarExtender13" runat="server" TargetControlID="txtsalaryday"
                                        Format="dd-MM-yyyy" CssClass="CalendarCSS">
                                  </cc1:CalendarExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR12" runat="server"
                              FilterMode="ValidChars" FilterType="Custom,Numbers"
                              TargetControlID="txtsalaryday" ValidChars="0123456789/-">
                            </cc1:FilteredTextBoxExtender>
					 </div>
				
						</div>
				        
                       </div>
            
           <div class="col-md-12">     
				        <div class="form-group row">
				          <asp:Label ID="lblfromdate" runat="server" class="col-sm-2 control-label" 
                                                        Text="From Date"></asp:Label>
                                 <div class="col-sm-3">                    
                                  <asp:TextBox ID="txtFrom" class="form-control" runat="server"></asp:TextBox>
                                   <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFrom"
                                        Format="dd-MM-yyyy" CssClass="CalendarCSS">
                                  </cc1:CalendarExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR1" runat="server"
                              FilterMode="ValidChars" FilterType="Custom,Numbers"
                              TargetControlID="txtFrom" ValidChars="0123456789/-">
                            </cc1:FilteredTextBoxExtender>
					 </div>
                             
					    	
						    <div class="col-sm-1"></div>
						     <asp:Label ID="lblTo" runat="server" class="col-sm-2 control-label" 
                                                        Text="To Date"></asp:Label>
				          					    
						      <div class="col-sm-3">                    
                                  <asp:TextBox ID="txtTo" class="form-control" runat="server"
                                  AutoPostBack="True" ontextchanged="txtTo_TextChanged"></asp:TextBox>
                                  <cc1:CalendarExtender ID="CalendarExtender15" runat="server" TargetControlID="txtTo"
                                        Format="dd-MM-yyyy" CssClass="CalendarCSS">
                                  </cc1:CalendarExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR14" runat="server"
                              FilterMode="ValidChars" FilterType="Custom,Numbers"
                              TargetControlID="txtTo" ValidChars="0123456789/-">
                            </cc1:FilteredTextBoxExtender>
					 </div>
				
						</div>
				        
                       </div>
             
           <div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="lblEmployeeType" runat="server" class="col-sm-2 control-label" 
                                                        Text="Employee Type"></asp:Label>
				      
					      <div class="col-sm-3">
                             <asp:DropDownList ID="txtEmployeeType" class="form-control" runat="server">
                              </asp:DropDownList>
					      </div>
					    	<div class="col-sm-1"></div> 
					    	<div class="col-sm-3"> 
					    	  <asp:FileUpload ID="FileUpload" class="btn btn-default btn-rounded" runat="server" />
					    	
					    	</div>
					    	
					    	
						    <div class="col-sm-1"></div>
						    <div class="col-sm-2">
				           <asp:CheckBox ID="ChkTokenNoSalaryprocess" runat="server" Text="Token No" Font-Bold="true"
                                 AutoPostBack="true" oncheckedchanged="ChkTokenNoSalaryprocess_CheckedChanged" Visible="false"/>
						    </div>
						      <div class="col-sm-3">                    
                                  <asp:TextBox ID="txtTokenNoSearch" class="form-control" runat="server" Visible="false" ></asp:TextBox>
					 </div>
				
						</div>
				        
                       </div>
          
          <div class="col-md-12">     
				        <div class="form-group row">
				            
					      <div class="col-sm-2">
                            <asp:CheckBox ID="ChkCivilIncentive" runat="server" Text="Civil Incentive"
                             AutoPostBack="True"  Visible="false" oncheckedchanged="ChkCivilIncentive_CheckedChanged"/>
					      </div>
					    	
						    <div class="col-sm-1"></div>
					
				
						</div>
				        
                     </div>
              
          <asp:Panel ID="PanelCivilIncenDate" runat="server" Visible="false">
          <div class="col-md-12">     
				        <div class="form-group row">
				            <label for="input-Default" class="col-sm-2 control-label">Civil From Date</label>
					      <div class="col-sm-3">
                             <asp:TextBox ID="txtIstWeekDate" class="form-control" runat="server"></asp:TextBox>
                             <cc1:CalendarExtender ID="CalendarExtender16" runat="server" TargetControlID="txtIstWeekDate"
                                        Format="dd-MM-yyyy" CssClass="orange">
                                  </cc1:CalendarExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR15" runat="server"
                              FilterMode="ValidChars" FilterType="Custom,Numbers"
                              TargetControlID="txtIstWeekDate" ValidChars="0123456789/-">
                            </cc1:FilteredTextBoxExtender>
					      </div>
					    	
						    <div class="col-sm-1"></div>
						   <label for="input-Default" class="col-sm-2 control-label">Civil To Date</label>
						      <div class="col-sm-3">                    
                                  <asp:TextBox ID="txtLastWeekDate" class="form-control" runat="server"></asp:TextBox>
                                  <cc1:CalendarExtender ID="CalendarExtender17" runat="server" TargetControlID="txtLastWeekDate"
                                        Format="dd-MM-yyyy" CssClass="orange">
                                  </cc1:CalendarExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR16" runat="server"
                              FilterMode="ValidChars" FilterType="Custom,Numbers"
                              TargetControlID="txtLastWeekDate" ValidChars="0123456789/-">
                            </cc1:FilteredTextBoxExtender>
					 </div>
				
						</div>
				        
                       </div>
          
          </asp:Panel>    
          
           <div class="col-md-12">
          <div class="form-group row">
					<div class="col-sm-1"></div>
					<div class="col-sm-3"></div>
						<div class="col-sm-2">
                            <asp:Button ID="btnUpload" class="btn btn-success" onclick="btnUpload_Click1" runat="server" Text="Upload" />                             
                         <asp:Label ID="lbldisplay" runat="server" class="col-sm-2 control-label" 
                                        Text=""></asp:Label>
                        </div>
					<div class="col-sm-1"></div>
					
					 <div class="col-sm-2">
                                    <asp:Button ID="BtnSalaryAttendance" runat="server" class="btn btn-success" 
                                        onclick="BtnSalaryAttendance_Click" Text="Attendance" Visible="false"   />
                                    </div>
					
				    <div class="col-sm-2">  
				     <asp:Button ID="btnSalaryCalculate" class="btn btn-success" Visible="false" onclick="btnSalaryCalculate_Click" runat="server" Text="Salary Calculate" 
                            />    
				    </div>
				
  				
			       </div>
                </div>

</div>
</div>
</form>
</div>
</div>

<!-- Dashboard start -->
 	
 <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
 <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
<!-- Dashboard End --> 



</div>
</div>
</div>
</ContentTemplate>
 <Triggers>
      <asp:PostBackTrigger ControlID="btnUpload"  />
 </Triggers>
</asp:UpdatePanel>
</asp:Content>

