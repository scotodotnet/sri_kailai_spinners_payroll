﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using System.Globalization;
using Payroll.Configuration;
using Payroll.Data;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;

public partial class BonusProcess : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        //string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
       
        if (SessionAdmin == "2")
        {
            Response.Redirect("EmployeeRegistration.aspx");
        }
        if (!IsPostBack)
        {
            DropDwonCategory();
        }
    }

    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();

        ddlcategory_Rpt.DataSource = dtcate;
        ddlcategory_Rpt.DataTextField = "CategoryName";
        ddlcategory_Rpt.DataValueField = "CategoryCd";
        ddlcategory_Rpt.DataBind();

        int currentYear = Utility.GetCurrentYearOnly;
        for (int i = 0; i < 10; i++)
        {
            ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
            txtBonusYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
            currentYear = currentYear - 1;
        }
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        EmployeeType();
    }

    protected void ddlcategory_Rpt_SelectedIndexChanged(object sender, EventArgs e)
    {
        EmployeeType_Rpt_Add();
    }

    public void EmployeeType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        txtEmployeeType.DataSource = dtemp;
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }
    public void EmployeeType_Rpt_Add()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory_Rpt.SelectedValue);
        txtEmployeeType_Rpt.DataSource = dtemp;
        txtEmployeeType_Rpt.DataTextField = "EmpType";
        txtEmployeeType_Rpt.DataValueField = "EmpTypeCd";
        txtEmployeeType_Rpt.DataBind();
    }

    protected void txtEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
    protected void btnBonusProcess_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool SaveFlag = false;
            string bonus_Period_From = "";
            string bonus_Period_To = "";

            string Gross_Sal_Add = "0";
            string Percent_Gross_Sal = "0";
            string Percent_Bonus_Amount = "0";
            string Round_Bonus_Amt = "0";
            string Round_Near_10_Bonus = "0";
            string Get_Bonus_Percent = "0";
            string BasicSalary = "0.00";
            if (ddlcategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            //Bonus Process Start
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                bonus_Period_From = "01-10-" + (Convert.ToDecimal(ddlFinance.SelectedValue.ToString()) - Convert.ToDecimal(1)).ToString();
                bonus_Period_To = "01-09-" + ddlFinance.SelectedValue.ToString();
                DateTime Bonus_From_Date = DateTime.ParseExact(bonus_Period_From, "dd-MM-yyyy", null);
                DateTime Bonus_To_Date = DateTime.ParseExact(bonus_Period_To, "dd-MM-yyyy", null);

                string query2 = "";
                //Get Bonus Process Employee Details
                if (SessionLcode == "UNIT I" || SessionLcode == "UNIT IV")
                {
                    query2 = "Select ED.EmpNo,ED.BiometricID,ED.ExisistingCode,ED.EmpName,ED.FatherName,ED.Department,MD.DepartmentNm,";
                    query2 = query2 + " ED.Designation,convert(varchar,OP.Dateofjoining,105) as Dateofjoining,";
                    //Experiance get
                    query2 = query2 + " (case when ED.ActivateMode = 'N' then (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, ED.deactivedate)/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, ED.deactivedate) % 12))";
                    query2 = query2 + " else (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000') % 12)) end";
                    query2 = query2 + " ) as Experiance,";

                    //query2 = query2 + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000') % 12)) AS Experiance,";


                    query2 = query2 + " Sum(SD.WDays) as W_Days,SUM(SD.OTDays) as OT_Days,SUM(SD.NFh) as NFH_Days,";
                    query2 = query2 + " SUM(";
                    query2 = query2 + " cast(SD.BasicAndDANew as decimal(18,2)) + cast(SD.BasicHRA as decimal(18,2)) + ";
                    query2 = query2 + " cast(SD.ConvAllow as decimal(18,2)) + cast(SD.EduAllow as decimal(18,2)) + ";
                    query2 = query2 + " cast(SD.MediAllow as decimal(18,2)) + cast(SD.BasicRAI as decimal(18,2)) + ";
                    query2 = query2 + " cast(SD.WashingAllow as decimal(18,2)) + SD.ThreesidedAmt + SD.DayIncentive";
                    query2 = query2 + " ) as Gross_Sal,ED.ActivateMode,SM.Base";
                    query2 = query2 + " from EmployeeDetails ED";
                    query2 = query2 + " inner join SalaryDetails_Bonus SD on ED.ExisistingCode=SD.ExisistingCode And SD.Lcode=ED.Lcode";
                    query2 = query2 + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo And OP.Lcode=ED.Lcode";
                    query2 = query2 + " inner join SalaryMaster SM on ED.EmpNo=SM.EmpNo And SM.Lcode=ED.Lcode";
                    query2 = query2 + " inner join MstDepartment MD on ED.Department=MD.DepartmentCd";
                    query2 = query2 + " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "' And ED.EmployeeType='" + txtEmployeeType.Text + "'";
                    query2 = query2 + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                    query2 = query2 + " And SD.FromDate >=convert(datetime,'" + Bonus_From_Date + "', 105) And SD.FromDate <=convert(datetime,'" + Bonus_To_Date + "', 105)";

                    query2 = query2 + " group by ED.EmpNo,ED.BiometricID,ED.ExisistingCode,ED.EmpName,ED.FatherName,";
                    query2 = query2 + " ED.Department,MD.DepartmentNm,ED.Designation,OP.Dateofjoining,ED.ActivateMode,SM.Base,ED.Deactivedate";
                    query2 = query2 + " Order by ED.ExisistingCode Asc";
                }
                else
                {
                    query2 = "Select ED.EmpNo,ED.BiometricID,ED.ExisistingCode,ED.EmpName,ED.FatherName,ED.Department,MD.DepartmentNm,";
                    query2 = query2 + " ED.Designation,convert(varchar,OP.Dateofjoining,105) as Dateofjoining,";

                    //Experiance get
                    query2 = query2 + " (case when ED.ActivateMode = 'N' then (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, ED.deactivedate)/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, ED.deactivedate) % 12))";
                    query2 = query2 + " else (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000') % 12)) end";
                    query2 = query2 + " ) as Experiance,";

                    //query2 = query2 + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000') % 12)) AS Experiance,";
                    if (txtEmployeeType.Text == "1" || txtEmployeeType.Text == "6")
                    {
                        query2 = query2 + " Sum(AD.Days) as W_Days,SUM(cast(AD.WH_Work_Days as decimal(18,2))) as OT_Days,SUM(AD.NFh) as NFH_Days,";
                    }
                    else
                    {
                        query2 = query2 + " Sum(AD.Days) as W_Days,SUM(AD.ThreeSided) as OT_Days,SUM(AD.NFh) as NFH_Days,";
                    }

                    query2 = query2 + " SUM(";
                    query2 = query2 + " cast(SD.BasicAndDANew as decimal(18,2)) + cast(SD.BasicHRA as decimal(18,2)) + ";
                    query2 = query2 + " cast(SD.ConvAllow as decimal(18,2)) + cast(SD.EduAllow as decimal(18,2)) + ";
                    query2 = query2 + " cast(SD.MediAllow as decimal(18,2)) + cast(SD.BasicRAI as decimal(18,2)) + ";
                    query2 = query2 + " cast(SD.WashingAllow as decimal(18,2)) + SD.ThreesidedAmt + SD.DayIncentive";
                    query2 = query2 + " ) as Gross_Sal,ED.ActivateMode,SM.Base";
                    query2 = query2 + " from EmployeeDetails ED";
                    query2 = query2 + " inner join SalaryDetails SD on ED.EmpNo=SD.EmpNo And SD.Lcode=ED.Lcode";
                    query2 = query2 + " inner join AttenanceDetails AD on ED.EmpNo=AD.EmpNo And AD.Lcode=ED.Lcode And SD.Lcode=AD.Lcode And SD.FromDate=AD.FromDate";
                    query2 = query2 + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo And OP.Lcode=ED.Lcode";
                    query2 = query2 + " inner join SalaryMaster SM on ED.EmpNo=SM.EmpNo And SM.Lcode=ED.Lcode";
                    query2 = query2 + " inner join MstDepartment MD on ED.Department=MD.DepartmentCd";
                    query2 = query2 + " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "' And ED.EmployeeType='" + txtEmployeeType.Text + "'";
                    query2 = query2 + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                    query2 = query2 + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                    query2 = query2 + " And SD.FromDate >=convert(datetime,'" + Bonus_From_Date + "', 105) And SD.FromDate <=convert(datetime,'" + Bonus_To_Date + "', 105)";
                    query2 = query2 + " And AD.FromDate >=convert(datetime,'" + Bonus_From_Date + "', 105) And AD.FromDate <=convert(datetime,'" + Bonus_To_Date + "', 105)";

                    query2 = query2 + " group by ED.EmpNo,ED.BiometricID,ED.ExisistingCode,ED.EmpName,ED.FatherName,";
                    query2 = query2 + " ED.Department,MD.DepartmentNm,ED.Designation,OP.Dateofjoining,ED.ActivateMode,SM.Base,ED.Deactivedate";
                    query2 = query2 + " Order by ED.ExisistingCode Asc";
                }
                dt = objdata.RptEmployeeMultipleDetails(query2);

                if (dt.Rows.Count != 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataTable Bonus_Fixed_DT = new DataTable();
                        DataTable Last_Year_Bonus_DT = new DataTable();
                        DataTable Insert_DT = new DataTable();
                        BasicSalary = dt.Rows[i]["Base"].ToString();
                        string Hostel_Stage = "0.0";
                        BasicSalary = (Math.Round(Convert.ToDecimal(BasicSalary), 2, MidpointRounding.ToEven)).ToString();

                        //STAFF And Regular Bonus Process Start
                        if (txtEmployeeType.Text.ToString() == "1" || txtEmployeeType.Text.ToString() == "2" || txtEmployeeType.Text.ToString() == "3" || txtEmployeeType.Text.ToString() == "6")
                        {
                            bool exgratia_check = false;
                            string Eight_Percent_Bonus_Val = "0.00";
                            string Eight_Percent_Bonus_Val_1 = "0.00";
                            string signlist_bonus_Amt = "0.00";
                            string signlist_exgratia_Amt = "0.00";
                            string Min_Amount_Check = "0.00";

                            query2 = "Select * from MstBonusMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmployeeType='" + txtEmployeeType.Text + "'";
                            Bonus_Fixed_DT = objdata.RptEmployeeMultipleDetails(query2);
                            if (Bonus_Fixed_DT.Rows.Count != 0)
                            {
                                string LY_GS = "0";
                                string LY_Percent_GS = "0";
                                string LY_Bonus_Percent = "0";
                                string LY_Final_Bonus = "0";
                                string LY_Total_Days = "0";
                                Min_Amount_Check = Bonus_Fixed_DT.Rows[0]["Min_Amount"].ToString();

                                query2 = "Select * from Bonus_Details_Last_Year where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ExisistingCode='" + dt.Rows[i]["ExisistingCode"].ToString() + "'";
                                Last_Year_Bonus_DT = objdata.RptEmployeeMultipleDetails(query2);
                                if (Last_Year_Bonus_DT.Rows.Count != 0)
                                {
                                    LY_GS = Last_Year_Bonus_DT.Rows[0]["Gross_Amount"].ToString();
                                    LY_Percent_GS = Last_Year_Bonus_DT.Rows[0]["Percent_Gross_Amount"].ToString();
                                    LY_Bonus_Percent = Last_Year_Bonus_DT.Rows[0]["Bonus_Perncet"].ToString();
                                    LY_Final_Bonus = Last_Year_Bonus_DT.Rows[0]["Final_Bonus_Amt"].ToString();
                                    LY_Total_Days = Last_Year_Bonus_DT.Rows[0]["Total_Days"].ToString();
                                }
                                else
                                {
                                    LY_GS = "0.00";
                                    LY_Percent_GS = "0.00";
                                    LY_Bonus_Percent = "0.00";
                                    LY_Final_Bonus = "0.00";
                                    LY_Total_Days = "0.00";
                                }

                                //Get Voucher Amt
                                string Voucher_Amt = "0.00";
                                string Voucher_Days = "0.00";
                                DataTable Vo_DT = new DataTable();
                                query2 = "Select * from SalaryDetails_Voucher where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ExisistingCode='" + dt.Rows[i]["ExisistingCode"] + "' And Bonus_Year='" + ddlFinance.SelectedValue.ToString() + "'";
                                Vo_DT = objdata.RptEmployeeMultipleDetails(query2);
                                if (Vo_DT.Rows.Count != 0)
                                {
                                    Voucher_Amt = Vo_DT.Rows[0]["Voucher_Amt"].ToString();
                                    Voucher_Days = Vo_DT.Rows[0]["Total_Days"].ToString();
                                }
                                else
                                {
                                    Voucher_Amt = "0.00";
                                    Voucher_Days = "0.00";
                                }

                                //Gross Salary Percent Calculate
                                Percent_Gross_Sal = (Convert.ToDecimal(dt.Rows[i]["Gross_Sal"].ToString()) + Convert.ToDecimal(Voucher_Amt)).ToString();
                                Percent_Gross_Sal = ((Convert.ToDecimal(Percent_Gross_Sal) * Convert.ToDecimal(Bonus_Fixed_DT.Rows[0]["Gross_Sal_Percent"].ToString())) / 100).ToString();
                                Percent_Gross_Sal = (Math.Round(Convert.ToDecimal(Percent_Gross_Sal), 2, MidpointRounding.ToEven)).ToString();

                                //Bonus Percent Calculate;
                                string Total_work_Days = "0";
                                Total_work_Days = (Convert.ToDecimal(dt.Rows[i]["W_Days"].ToString()) + Convert.ToDecimal(dt.Rows[i]["NFH_Days"].ToString()) + Convert.ToDecimal(dt.Rows[i]["OT_Days"].ToString())).ToString();
                                Eight_Percent_Bonus_Val_1 = Bonus_Fixed_DT.Rows[0]["BelowDaysPercent"].ToString();
                                if (Convert.ToDecimal(dt.Rows[i]["Experiance"].ToString()) >= Convert.ToDecimal(1))
                                {
                                    Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["AboveOneYear"].ToString();
                                    exgratia_check = true;
                                }
                                else
                                {
                                    if (Convert.ToDecimal(Bonus_Fixed_DT.Rows[0]["BelowOneYear"].ToString()) <= Convert.ToDecimal(Total_work_Days))
                                    {
                                        Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["AboveDaysPercent"].ToString();
                                        exgratia_check = true;
                                    }
                                    else
                                    {
                                        Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["BelowDaysPercent"].ToString();
                                    }

                                }
                                Percent_Bonus_Amount = ((Convert.ToDecimal(Percent_Gross_Sal) * Convert.ToDecimal(Get_Bonus_Percent)) / 100).ToString();
                                Percent_Bonus_Amount = (Math.Round(Convert.ToDecimal(Percent_Bonus_Amount), 2, MidpointRounding.ToEven)).ToString();

                                //Final Bonus Amount
                                string[] Round_Bonus_Amt_Splt = Percent_Bonus_Amount.Split('.');
                                //Round_Bonus_Amt = (Math.Round(Convert.ToDecimal(Percent_Bonus_Amount), 0, MidpointRounding.AwayFromZero)).ToString();
                                Round_Bonus_Amt = Round_Bonus_Amt_Splt[0].ToString();
                                if (Convert.ToDecimal(Round_Bonus_Amt) < 0)
                                {
                                    Round_Bonus_Amt = "0.00";
                                }
                                double d1 = Convert.ToDouble(Round_Bonus_Amt.ToString());
                                int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                                Round_Near_10_Bonus = rounded1.ToString();


                                if (Convert.ToDecimal(Min_Amount_Check) >= Convert.ToDecimal(Round_Near_10_Bonus))
                                {
                                    Round_Near_10_Bonus = Min_Amount_Check;
                                    signlist_bonus_Amt = Round_Near_10_Bonus;
                                    signlist_exgratia_Amt = "0.00";
                                }
                                else
                                {
                                    //Round_Near_10_Bonus = Round_Near_10_Bonus;
                                    if (exgratia_check == true)
                                    {
                                        Eight_Percent_Bonus_Val = ((Convert.ToDecimal(Percent_Gross_Sal) * Convert.ToDecimal(Eight_Percent_Bonus_Val_1)) / 100).ToString();
                                        Eight_Percent_Bonus_Val = (Math.Round(Convert.ToDecimal(Eight_Percent_Bonus_Val), 0, MidpointRounding.AwayFromZero)).ToString();
                                        signlist_bonus_Amt = Eight_Percent_Bonus_Val;
                                        signlist_exgratia_Amt = (Convert.ToDecimal(Round_Near_10_Bonus) - Convert.ToDecimal(signlist_bonus_Amt)).ToString();
                                    }
                                    else
                                    {
                                        signlist_bonus_Amt = Round_Near_10_Bonus;
                                        signlist_exgratia_Amt = "0.00";
                                    }
                                }





                                //Insert Bonus Table Start
                                query2 = "Select * from Bonus_Details where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And Bonus_Year='" + ddlFinance.SelectedValue.ToString() + "'";
                                Insert_DT = objdata.RptEmployeeMultipleDetails(query2);
                                if (Insert_DT.Rows.Count != 0)
                                {
                                    query2 = "Delete from Bonus_Details where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And Bonus_Year='" + ddlFinance.SelectedValue.ToString() + "'";
                                    Insert_DT = objdata.RptEmployeeMultipleDetails(query2);
                                }
                                Hostel_Stage = "0.0";
                                query2 = "Insert Into Bonus_Details(Ccode,Lcode,EmpNo,MachineID,ExisistingCode,EmpName,FatherName,Department,";
                                query2 = query2 + " DepartmentName,Designation,DOJ,Experiance,Worked_Days,NFH_Days,OT_Days,Gross_Sal,Gross_Sal_Fixed_Per,Percent_Gross_Sal,";
                                query2 = query2 + " Bonus_Percent,Bonus_Amt,Round_Bonus,Final_Bonus_Amt,LY_Gross_Sal,LY_Percent_Gross_Sal,LY_Bonus_Percent,LY_Final_Bonus_Amt,";
                                query2 = query2 + " LY_Total_Days,Bonus_Year,ActivateMode,BaseSalary,Employee_Type,signlist_bonus_Amt,signlist_exgratia_Amt,Hostel_Stage,Voucher_Days,Voucher_Amt) Values('" + SessionCcode + "','" + SessionLcode + "','" + dt.Rows[i]["EmpNo"] + "',";
                                query2 = query2 + " '" + dt.Rows[i]["BiometricID"] + "','" + dt.Rows[i]["ExisistingCode"] + "','" + dt.Rows[i]["EmpName"] + "','" + dt.Rows[i]["FatherName"] + "',";
                                query2 = query2 + " '" + dt.Rows[i]["Department"] + "','" + dt.Rows[i]["DepartmentNm"] + "','" + dt.Rows[i]["Designation"] + "','" + dt.Rows[i]["Dateofjoining"] + "','" + dt.Rows[i]["Experiance"] + "',";
                                query2 = query2 + " '" + dt.Rows[i]["W_Days"] + "','" + dt.Rows[i]["NFH_Days"] + "','" + dt.Rows[i]["OT_Days"] + "','" + dt.Rows[i]["Gross_Sal"] + "','" + Bonus_Fixed_DT.Rows[0]["Gross_Sal_Percent"] + "',";
                                query2 = query2 + " '" + Percent_Gross_Sal + "','" + Get_Bonus_Percent + "','" + Percent_Bonus_Amount + "','" + Round_Bonus_Amt + "','" + Round_Near_10_Bonus + "',";
                                query2 = query2 + " '" + LY_GS + "','" + LY_Percent_GS + "','" + LY_Bonus_Percent + "','" + LY_Final_Bonus + "','" + LY_Total_Days + "',";
                                query2 = query2 + " '" + ddlFinance.SelectedValue.ToString() + "','" + dt.Rows[i]["ActivateMode"] + "','" + dt.Rows[i]["Base"] + "',";
                                query2 = query2 + " '" + txtEmployeeType.SelectedValue + "','" + signlist_bonus_Amt + "','" + signlist_exgratia_Amt + "','" + Hostel_Stage + "','" + Voucher_Days + "','" + Voucher_Amt + "')";
                                Insert_DT = objdata.RptEmployeeMultipleDetails(query2);
                                //Insert Bonus Table End
                            }

                        }
                        //STAFF And Regular Bonus Process End


                        else if (txtEmployeeType.SelectedValue.ToString() == "4")  // Hostel Bonus Process Start
                        {
                            string signlist_bonus_Amt = "0.00";
                            string signlist_exgratia_Amt = "0.00";
                            Hostel_Stage = "0.0";
                            string Min_Amount_Check = "0.00";
                            query2 = "Select * from MstBonusMaster_Hostel where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                            Bonus_Fixed_DT = objdata.RptEmployeeMultipleDetails(query2);
                            if (Bonus_Fixed_DT.Rows.Count != 0)
                            {
                                Min_Amount_Check = Bonus_Fixed_DT.Rows[0]["Min_Amount"].ToString();

                                string LY_GS = "0";
                                string LY_Percent_GS = "0";
                                string LY_Bonus_Percent = "0";
                                string LY_Final_Bonus = "0";
                                string LY_Total_Days = "0";

                                string Total_work_Days = "0";
                                Total_work_Days = (Convert.ToDecimal(dt.Rows[i]["W_Days"].ToString()) + Convert.ToDecimal(dt.Rows[i]["NFH_Days"].ToString()) + Convert.ToDecimal(dt.Rows[i]["OT_Days"].ToString())).ToString();

                                //Get Bonus Value;
                                query2 = "Select * from MstBasicDetHostel where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TotalAmt='" + BasicSalary + "'";
                                DataTable Query_DT = new DataTable();
                                Query_DT = objdata.RptEmployeeMultipleDetails(query2);
                                if (Query_DT.Rows.Count != 0)
                                {
                                    Hostel_Stage = Query_DT.Rows[0]["Stage"].ToString();
                                    if (Query_DT.Rows[0]["Basic_Year"].ToString() == "0")
                                    {
                                        if (Convert.ToDecimal(Bonus_Fixed_DT.Rows[0]["BelowOneYear"].ToString()) <= Convert.ToDecimal(Total_work_Days))
                                        {
                                            Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["AboveDaysPercent"].ToString();
                                        }
                                        else
                                        {
                                            Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["BelowDaysPercent"].ToString();
                                        }
                                        //Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["BelowOneYear"].ToString();
                                    }
                                    else if (Convert.ToDecimal(Query_DT.Rows[0]["Basic_Year"].ToString()) >= Convert.ToDecimal(1) && Convert.ToDecimal(Query_DT.Rows[0]["Basic_Year"].ToString()) < Convert.ToDecimal(2))
                                    {
                                        Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["AboveOneBelowTwo"].ToString();
                                    }
                                    else if (Convert.ToDecimal(Query_DT.Rows[0]["Basic_Year"].ToString()) >= Convert.ToDecimal(2) && Convert.ToDecimal(Query_DT.Rows[0]["Basic_Year"].ToString()) < Convert.ToDecimal(3))
                                    {
                                        Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["AboveTwoBelowThree"].ToString();
                                    }
                                    else if (Convert.ToDecimal(Query_DT.Rows[0]["Basic_Year"].ToString()) >= Convert.ToDecimal(3))
                                    {
                                        Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["AboveThree"].ToString();
                                    }
                                }
                                else
                                {
                                    Get_Bonus_Percent = "0";
                                }


                                //Get Voucher Amt
                                string Voucher_Amt = "0.00";
                                string Voucher_Days = "0.00";
                                DataTable Vo_DT = new DataTable();
                                query2 = "Select * from SalaryDetails_Voucher where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ExisistingCode='" + dt.Rows[i]["ExisistingCode"] + "' And Bonus_Year='" + ddlFinance.SelectedValue.ToString() + "'";
                                Vo_DT = objdata.RptEmployeeMultipleDetails(query2);
                                if (Vo_DT.Rows.Count != 0)
                                {
                                    Voucher_Amt = Vo_DT.Rows[0]["Voucher_Amt"].ToString();
                                    Voucher_Days = Vo_DT.Rows[0]["Total_Days"].ToString();
                                }
                                else
                                {
                                    Voucher_Amt = "0.00";
                                    Voucher_Days = "0.00";
                                }
                                Percent_Bonus_Amount = (Convert.ToDecimal(Total_work_Days) + Convert.ToDecimal(Voucher_Days)).ToString();
                                Percent_Bonus_Amount = (Convert.ToDecimal(Percent_Bonus_Amount) * Convert.ToDecimal(Get_Bonus_Percent)).ToString();
                                Percent_Bonus_Amount = (Math.Round(Convert.ToDecimal(Percent_Bonus_Amount), 2, MidpointRounding.ToEven)).ToString();

                                //Final Bonus Amount
                                string[] Round_Bonus_Amt_Splt = Percent_Bonus_Amount.Split('.');
                                //Round_Bonus_Amt = (Math.Round(Convert.ToDecimal(Percent_Bonus_Amount), 0, MidpointRounding.AwayFromZero)).ToString();
                                Round_Bonus_Amt = Round_Bonus_Amt_Splt[0].ToString();

                                if (Convert.ToDecimal(Round_Bonus_Amt) < 0)
                                {
                                    Round_Bonus_Amt = "0.00";
                                }
                                double d1 = Convert.ToDouble(Round_Bonus_Amt.ToString());
                                int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                                Round_Near_10_Bonus = rounded1.ToString();

                                if (Convert.ToDecimal(Min_Amount_Check) >= Convert.ToDecimal(Round_Near_10_Bonus))
                                {
                                    Round_Near_10_Bonus = Min_Amount_Check;
                                }
                                else
                                {
                                    Round_Near_10_Bonus = Round_Near_10_Bonus;
                                }

                                signlist_bonus_Amt = "0.00";
                                signlist_exgratia_Amt = Round_Near_10_Bonus;

                                //Insert Bonus Table Start
                                query2 = "Select * from Bonus_Details where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And Bonus_Year='" + ddlFinance.SelectedValue.ToString() + "'";
                                Insert_DT = objdata.RptEmployeeMultipleDetails(query2);
                                if (Insert_DT.Rows.Count != 0)
                                {
                                    query2 = "Delete from Bonus_Details where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And Bonus_Year='" + ddlFinance.SelectedValue.ToString() + "'";
                                    Insert_DT = objdata.RptEmployeeMultipleDetails(query2);
                                }
                                query2 = "Insert Into Bonus_Details(Ccode,Lcode,EmpNo,MachineID,ExisistingCode,EmpName,FatherName,Department,";
                                query2 = query2 + " DepartmentName,Designation,DOJ,Experiance,Worked_Days,NFH_Days,OT_Days,Gross_Sal,Gross_Sal_Fixed_Per,Percent_Gross_Sal,";
                                query2 = query2 + " Bonus_Percent,Bonus_Amt,Round_Bonus,Final_Bonus_Amt,LY_Gross_Sal,LY_Percent_Gross_Sal,LY_Bonus_Percent,LY_Final_Bonus_Amt,";
                                query2 = query2 + " LY_Total_Days,Bonus_Year,ActivateMode,BaseSalary,Employee_Type,signlist_bonus_Amt,signlist_exgratia_Amt,Hostel_Stage,Voucher_Days,Voucher_Amt) Values('" + SessionCcode + "','" + SessionLcode + "','" + dt.Rows[i]["EmpNo"] + "',";
                                query2 = query2 + " '" + dt.Rows[i]["BiometricID"] + "','" + dt.Rows[i]["ExisistingCode"] + "','" + dt.Rows[i]["EmpName"] + "','" + dt.Rows[i]["FatherName"] + "',";
                                query2 = query2 + " '" + dt.Rows[i]["Department"] + "','" + dt.Rows[i]["DepartmentNm"] + "','" + dt.Rows[i]["Designation"] + "','" + dt.Rows[i]["Dateofjoining"] + "','" + dt.Rows[i]["Experiance"] + "',";
                                query2 = query2 + " '" + dt.Rows[i]["W_Days"] + "','" + dt.Rows[i]["NFH_Days"] + "','" + dt.Rows[i]["OT_Days"] + "','" + dt.Rows[i]["Gross_Sal"] + "','0.00',";
                                query2 = query2 + " '0.00','" + Get_Bonus_Percent + "','" + Percent_Bonus_Amount + "','" + Round_Bonus_Amt + "','" + Round_Near_10_Bonus + "',";
                                query2 = query2 + " '" + LY_GS + "','" + LY_Percent_GS + "','" + LY_Bonus_Percent + "','" + LY_Final_Bonus + "','" + LY_Total_Days + "',";
                                query2 = query2 + " '" + ddlFinance.SelectedValue.ToString() + "','" + dt.Rows[i]["ActivateMode"] + "','" + dt.Rows[i]["Base"] + "',";
                                query2 = query2 + " '" + txtEmployeeType.SelectedValue + "','" + signlist_bonus_Amt + "','" + signlist_exgratia_Amt + "','" + Hostel_Stage + "','" + Voucher_Days + "','" + Voucher_Amt + "')";
                                Insert_DT = objdata.RptEmployeeMultipleDetails(query2);
                                //Insert Bonus Table End

                            }

                        }  // Hostel Bonus Process End


                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Finished Success Fully');", true);
                }
            }
            //Bonus Process End

        }

        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Bonus Process ERROR...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }


    protected void btnBonusView_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            int YR = 0;
            string Stafflabour = "";

            if (ddlcategory_Rpt.SelectedValue == "0" || ddlcategory_Rpt.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType_Rpt.SelectedValue == "") || (txtEmployeeType_Rpt.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory_Rpt.SelectedValue == "1")
                    {
                        Stafflabour = "S";
                    }
                    else if (ddlcategory_Rpt.SelectedValue == "2")
                    {
                        Stafflabour = "L";
                    }


                    string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();
                    string SalaryThrough = RdbCashBank.SelectedValue.ToString();
                    string Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                    string Str_ChkLeft = RdbLeftType.SelectedValue.ToString();
                    string bonus_Period_From = "";
                    string bonus_Period_To = "";
                    string Report_Type_Call = "";

                    if (rdbPayslipFormat.SelectedValue == "1")
                    {
                        Report_Type_Call = "Bonus_Individual";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "2")
                    {
                        Report_Type_Call = "Bonus_Recociliation";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "3")
                    {
                        Report_Type_Call = "Bonus_Checklist";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "4")
                    {
                        Report_Type_Call = "Bonus_Signlist";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "5")
                    {
                        Report_Type_Call = "Bonus_Blank_Signlist";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "6")
                    {
                        Report_Type_Call = "Bonus_Cover";
                    }

                    bonus_Period_From = "01-10-" + (Convert.ToDecimal(ddlFinance.SelectedValue.ToString()) - Convert.ToDecimal(1)).ToString();
                    bonus_Period_To = "01-09-" + ddlFinance.SelectedValue.ToString();

                    ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + "" + "&Months=" + "" + "&yr=" + txtBonusYear.SelectedValue + "&fromdate=" + bonus_Period_From + "&ToDate=" + bonus_Period_To + "&Salary=" + "" + "&CashBank=" + RdbCashBank.SelectedValue + "&ESICode=" + "" + "&EmpType=" + txtEmployeeType_Rpt.SelectedValue.ToString() + "&PayslipType=" + rdbPayslipFormat.SelectedValue.ToString() + "&PFTypePost=" + RdbPFNonPF.SelectedValue.ToString() + "&Left_Emp=" + RdbLeftType.SelectedValue.ToString() + "&Leftdate=" + "" + "&Report_Type=" + Report_Type_Call, "_blank", "");

                }
            }
        }
        catch (Exception ex)
        {
        }
    }


    protected void btnBonusExcelView_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            int YR = 0;
            string Stafflabour = "";

            if (ddlcategory_Rpt.SelectedValue == "0" || ddlcategory_Rpt.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType_Rpt.SelectedValue == "") || (txtEmployeeType_Rpt.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                string query = "";
                DataTable dt = new DataTable();
                dt = objdata.Company_retrive(SessionCcode, SessionLcode);
                string CmpName = "";
                string Cmpaddress = "";
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                if (!ErrFlag)
                {
                    if (ddlcategory_Rpt.SelectedValue == "1")
                    {
                        Stafflabour = "S";
                    }
                    else if (ddlcategory_Rpt.SelectedValue == "2")
                    {
                        Stafflabour = "L";
                    }


                    string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();
                    string SalaryThrough = RdbCashBank.SelectedValue.ToString();
                    string Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                    string Str_ChkLeft = RdbLeftType.SelectedValue.ToString();
                    string bonus_Period_From = "";
                    string bonus_Period_To = "";
                    string Report_Type_Call = "";

                    if (rdbPayslipFormat.SelectedValue == "1")
                    {
                        Report_Type_Call = "Bonus_Individual";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "2")
                    {
                        Report_Type_Call = "Bonus_Recociliation";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "3")
                    {
                        Report_Type_Call = "Bonus_Checklist";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "4")
                    {
                        //Report_Type_Call = "Bonus_Signlist";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "5")
                    {
                        //Report_Type_Call = "Bonus_Blank_Signlist";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "6")
                    {
                        //Report_Type_Call = "Bonus_Cover";
                    }

                    bonus_Period_From = "01-10-" + (Convert.ToDecimal(ddlFinance.SelectedValue.ToString()) - Convert.ToDecimal(1)).ToString();
                    bonus_Period_To = "01-09-" + ddlFinance.SelectedValue.ToString();

                    if (Report_Type_Call == "Bonus_Recociliation") //Bonus_Recociliation
                    {
                        query = "Select ExisistingCode as Token_No,EmpName as Name,Dateofjoining as DOJ,";
                        query = query + " sum(case when Months = 'Oct' then WDays else 0 end) as O_Day,sum(case when Months = 'Oct' then WithInc_Amt else 0 end) as O_Earned,";
                        query = query + " sum(case when Months = 'Nov' then WDays else 0 end) as N_Day,sum(case when Months = 'Nov' then WithInc_Amt else 0 end) as N_Earned,";
                        query = query + " sum(case when Months = 'Dec' then WDays else 0 end) as D_Day,sum(case when Months = 'Dec' then WithInc_Amt else 0 end) as D_Earned,";
                        query = query + " sum(case when Months = 'Jan' then WDays else 0 end) as J_Day,sum(case when Months = 'Jan' then WithInc_Amt else 0 end) as J_Earned,";
                        query = query + " sum(case when Months = 'Feb' then WDays else 0 end) as F_Day,sum(case when Months = 'Feb' then WithInc_Amt else 0 end) as F_Earned,";
                        query = query + " sum(case when Months = 'Mar' then WDays else 0 end) as Ma_Day,sum(case when Months = 'Mar' then WithInc_Amt else 0 end) as Ma_Earned,";
                        query = query + " sum(case when Months = 'Apr' then WDays else 0 end) as A_Day,sum(case when Months = 'Apr' then WithInc_Amt else 0 end) as A_Earned,";
                        query = query + " sum(case when Months = 'May' then WDays else 0 end) as M_Day,sum(case when Months = 'May' then WithInc_Amt else 0 end) as M_Earned,";
                        query = query + " sum(case when Months = 'Jun' then WDays else 0 end) as Jun_Day,sum(case when Months = 'Jun' then WithInc_Amt else 0 end) as Jun_Earned,";
                        query = query + " sum(case when Months = 'Jul' then WDays else 0 end) as Jul_Day,sum(case when Months = 'Jul' then WithInc_Amt else 0 end) as Jul_Earned,";
                        query = query + " sum(case when Months = 'Aug' then WDays else 0 end) as Au_Day,sum(case when Months = 'Aug' then WithInc_Amt else 0 end) as Au_Earned,";
                        query = query + " sum(case when Months = 'Sep' then WDays else 0 end) as S_Day,sum(case when Months = 'Sep' then WithInc_Amt else 0 end) as S_Earned,";

                        query = query + " sum(WDays) as T_Days,sum(WithInc_Amt) as T_Earned,";

                        query = query + " SUM(Incentive_Amt) as Incentive_Amt,(SUM(WithInc_Amt) - SUM(Incentive_Amt)) as Bonus_Earned from (";
                        if (SessionLcode == "UNIT I" || SessionLcode == "UNIT IV")
                        {
                            query = query + " Select ED.ExisistingCode,ED.EmpName,convert(varchar,OP.Dateofjoining,105) as Dateofjoining,sum(SD.WDays + SD.OTDays + SD.NFH) as WDays,";
                            query = query + " SUM(cast(SD.BasicAndDANew as decimal(18,2)) + cast(SD.BasicHRA as decimal(18,2)) + cast(SD.ConvAllow as decimal(18,2)) + cast(SD.EduAllow as decimal(18,2)) +";
                            query = query + " cast(SD.MediAllow as decimal(18,2)) + cast(SD.BasicRAI as decimal(18,2)) + cast(SD.WashingAllow as decimal(18,2)) + SD.ThreesidedAmt + SD.DayIncentive + SD.allowances1) as WithInc_Amt,";
                            query = query + " SUM(SD.allowances1) as Incentive_Amt,left(DATENAME(MM, SD.FromDate),3) as Months";
                            query = query + " from EmployeeDetails ED inner join SalaryDetails_Bonus SD on ED.ExisistingCode=SD.ExisistingCode And SD.Lcode=ED.Lcode";
                            query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo";
                            query = query + " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "' And ED.EmployeeType='" + txtEmployeeType_Rpt.SelectedValue.ToString() + "'";
                            query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                            query = query + " And SD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And SD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                        }
                        else
                        {
                            query = query + " Select ED.ExisistingCode,ED.EmpName,convert(varchar,OP.Dateofjoining,105) as Dateofjoining,";
                            if (txtEmployeeType_Rpt.SelectedValue == "1" || txtEmployeeType_Rpt.SelectedValue == "6")
                            {
                                query = query + " sum(AD.Days + cast(AD.WH_Work_Days as decimal(18,2)) + AD.NFh) as WDays,";
                            }
                            else
                            {
                                query = query + " sum(AD.Days + AD.ThreeSided + AD.NFh) as WDays,";
                            }
                            query = query + " SUM(cast(SD.BasicAndDANew as decimal(18,2)) + cast(SD.BasicHRA as decimal(18,2)) + cast(SD.ConvAllow as decimal(18,2)) + cast(SD.EduAllow as decimal(18,2)) +";
                            query = query + " cast(SD.MediAllow as decimal(18,2)) + cast(SD.BasicRAI as decimal(18,2)) + cast(SD.WashingAllow as decimal(18,2)) + SD.ThreesidedAmt + SD.DayIncentive + SD.allowances1) as WithInc_Amt,";
                            query = query + " SUM(SD.allowances1) as Incentive_Amt,left(DATENAME(MM, SD.FromDate),3) as Months";
                            query = query + " from EmployeeDetails ED inner join SalaryDetails SD on ED.EmpNo=SD.EmpNo And SD.Lcode=ED.Lcode";
                            query = query + " inner join AttenanceDetails AD on ED.EmpNo=AD.EmpNo And AD.Lcode=ED.Lcode and SD.Lcode=AD.Lcode And AD.FromDate=SD.FromDate";
                            query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo";
                            query = query + " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "' And ED.EmployeeType='" + txtEmployeeType_Rpt.SelectedValue + "'";
                            query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                            query = query + " And SD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And SD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                            query = query + " And AD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And AD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                        }


                        //Activate Employee Check
                        if (RdbLeftType.SelectedValue != "1")
                        {
                            if (RdbLeftType.SelectedValue == "2")
                            {
                                query = query + " and ED.ActivateMode='Y'";
                            }
                            if (RdbLeftType.SelectedValue == "3")
                            {
                                query = query + " and ED.ActivateMode='N'";
                            }
                        }

                        //PF Check
                        if (RdbPFNonPF.SelectedValue.ToString() != "0")
                        {
                            if (RdbPFNonPF.SelectedValue.ToString() == "1")
                            {
                                //PF Employee
                                query = query + " and (OP.EligiblePF='1')";
                            }
                            else
                            {
                                //Non PF Employee
                                query = query + " and (OP.EligiblePF='2')";
                            }
                        }

                        //Salary Through Bank Or Cash Check
                        if (RdbCashBank.SelectedValue.ToString() != "0")
                        {
                            if (RdbCashBank.SelectedValue.ToString() == "1")
                            {
                                //Cash Employee
                                query = query + " and (OP.Salarythrough='1')";
                            }
                            else
                            {
                                //Bank Employee
                                query = query + " and (OP.Salarythrough='2')";
                            }
                        }


                        query = query + " group by ED.ExisistingCode,ED.EmpName,OP.Dateofjoining,DATENAME(MM, SD.FromDate)";
                        query = query + " ) as P";
                        query = query + " Group by ExisistingCode,EmpName,Dateofjoining";
                        query = query + " Order by ExisistingCode Asc";
                        DataTable dt_1 = new DataTable();
                        dt_1 = objdata.RptEmployeeMultipleDetails(query);
                        GridExcelView.DataSource = dt_1;
                        GridExcelView.DataBind();

                        string attachment = "attachment;filename=Bonus_Recociliation.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);
                        GridExcelView.RenderControl(htextw);

                        Response.Write("<table>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='31' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
                        Response.Write(CmpName + " - " + SessionLcode);
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='31' style='font-size:10.0pt;font-weight:bold;text-decoration:underline;''>");

                        if (txtEmployeeType_Rpt.SelectedValue == "1" || txtEmployeeType_Rpt.SelectedValue == "6")
                        {
                            Response.Write("STAFF RECOCILIATION DETAILS - FROM : 2014 TO : 2015");
                        }
                        else if (txtEmployeeType_Rpt.SelectedValue == "3")
                        {
                            Response.Write("WORKERS RECOCILIATION DETAILS - FROM : 2014 TO : 2015");
                        }
                        else if (txtEmployeeType_Rpt.SelectedValue == "4")
                        {
                            Response.Write("WORKERS CASUAL / RECOCILIATION DETAILS - FROM : 2014 TO : 2015");
                        }
                        else
                        {
                            Response.Write("STAFF RECOCILIATION DETAILS - FROM : 2014 TO : 2015");
                        }

                        //Response.Write("Recociliation Report");


                        Response.Write("</td>");
                        Response.Write("</tr><tr><td></td></tr>");
                        Response.Write("</table>");

                        //Heading Fixed
                        Response.Write("<table border='1'>");
                        Response.Write("<tr>");
                        Response.Write("<td></td><td></td><td></td>");
                        Response.Write("<td colspan='2' align='Center'>Oct 14</td><td colspan='2' align='Center'>Nov 14</td><td colspan='2' align='Center'>Dec 14</td>");
                        Response.Write("<td colspan='2' align='Center'>Jan 15</td><td colspan='2' align='Center'>Feb 15</td><td colspan='2' align='Center'>Mar 15</td>");
                        Response.Write("<td colspan='2' align='Center'>Apr 15</td><td colspan='2' align='Center'>May 15</td><td colspan='2' align='Center'>Jun 15</td>");
                        Response.Write("<td colspan='2' align='Center'>Jul 15</td><td colspan='2' align='Center'>Aug 15</td><td colspan='2' align='Center'>Sep 15</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");


                        //Grid Write Excel
                        Response.Write(stw.ToString());

                        //Total Add
                        string Total_Row_Count = (Convert.ToDecimal(5) + Convert.ToDecimal(GridExcelView.Rows.Count)).ToString();
                        Response.Write("<table border='1'>");
                        Response.Write("<tr>");
                        Response.Write("<td colspan='3' align='right'>TOTAL</td>");
                        Response.Write("<td>=sum(D6:D" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(E6:E" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(F6:F" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(G6:G" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(H6:H" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(I6:I" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(J6:J" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(K6:K" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(L6:L" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(M6:M" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(N6:N" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(O6:O" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(P6:P" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(Q6:Q" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(R6:R" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(S6:S" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(T6:T" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(U6:U" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(V6:V" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(W6:W" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(X6:X" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(Y6:Y" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(Z6:Z" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AA6:AA" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AB6:AB" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AC6:AC" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AD6:AD" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AE6:AE" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");

                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);


                    }

                    if (Report_Type_Call == "Bonus_Checklist") //Bonus_Checklist
                    {
                        if (txtEmployeeType_Rpt.SelectedValue == "4")
                        {
                            query = "Select ED.ExisistingCode as Token_No,ED.EmpName as Name,convert(varchar,OP.Dateofjoining,105) as DOJ,MD.DepartmentNm as Dept_Name,ED.Designation,";
                            //Experiance Get
                            query = query + " (case when ED.ActivateMode = 'N' then (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, ED.deactivedate)/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, ED.deactivedate) % 12))";
                            query = query + " else (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000') % 12)) end";
                            query = query + " ) as [Year & Month],";

                            //query = query + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000') % 12)) AS Experiance,";

                            query = query + " BD.Worked_Days,BD.NFH_Days as NFH,BD.OT_Days,isnull(BD.Voucher_Days,0) as Voucher_Days,(BD.Worked_Days+BD.NFH_Days+BD.OT_Days+isnull(BD.Voucher_Days,0)) as [Total Days],";
                            query = query + " SM.Base as [Perday Sal],BD.Hostel_Stage as [Stage], BD.Bonus_Percent as [Amount / Day],BD.Bonus_Amt as [Bonus Amt],";
                            query = query + " BD.Final_Bonus_Amt as [Near By Rs.10]";
                            query = query + " from EmployeeDetails ED inner join Bonus_Details BD on ED.EmpNo=BD.EmpNo And ED.Lcode=BD.Lcode";
                            query = query + " inner join Officialprofile OP on OP.EmpNo=ED.EmpNo inner join SalaryMaster SM on SM.EmpNo=ED.EmpNo";
                            query = query + " inner join MstDepartment MD on MD.DepartmentCd=ED.Department";
                            query = query + " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "' And ED.EmployeeType='" + txtEmployeeType_Rpt.SelectedValue + "'";
                            query = query + " And BD.Ccode='" + SessionCcode + "' And BD.Lcode='" + SessionLcode + "' And BD.Bonus_Year='" + txtBonusYear.SelectedValue + "'";

                            //Activate Employee Check
                            if (RdbLeftType.SelectedValue != "1")
                            {
                                if (RdbLeftType.SelectedValue == "2")
                                {
                                    query = query + " and ED.ActivateMode='Y'";
                                }
                                if (RdbLeftType.SelectedValue == "3")
                                {
                                    query = query + " and ED.ActivateMode='N'";
                                }
                            }

                            //PF Check
                            if (RdbPFNonPF.SelectedValue.ToString() != "0")
                            {
                                if (RdbPFNonPF.SelectedValue.ToString() == "1")
                                {
                                    //PF Employee
                                    query = query + " and (OP.EligiblePF='1')";
                                }
                                else
                                {
                                    //Non PF Employee
                                    query = query + " and (OP.EligiblePF='2')";
                                }
                            }

                            //Salary Through Bank Or Cash Check
                            if (RdbCashBank.SelectedValue.ToString() != "0")
                            {
                                if (RdbCashBank.SelectedValue.ToString() == "1")
                                {
                                    //Cash Employee
                                    query = query + " and (OP.Salarythrough='1')";
                                }
                                else
                                {
                                    //Bank Employee
                                    query = query + " and (OP.Salarythrough='2')";
                                }
                            }
                            query = query + " Order by ED.ExisistingCode Asc";
                        }
                        else
                        {
                            query = "Select ED.ExisistingCode as Token_No,ED.EmpName as Name,MD.DepartmentNm as Dept_Name,ED.Designation,convert(varchar,OP.Dateofjoining,105) as DOJ,";
                            query = query + " SUM(BD.Worked_Days + BD.OT_Days + BD.NFH_Days + isnull(BD.Voucher_Days,0)) as Total_Days,BD.Gross_Sal as [Gross Salary],";
                            query = query + " isnull(BD.Voucher_Amt,0) as Voucher_Amt,(BD.Gross_Sal+isnull(BD.Voucher_Amt,0)) as [Total Gross Salary],BD.Percent_Gross_Sal as [70% Gross Sal],BD.Bonus_Percent as [Bonus %],BD.Bonus_Amt,";
                            query = query + " BD.Final_Bonus_Amt as [Near By Rs.10],LY_Final_Bonus_Amt as [LY. Bonus Amt],LY_Gross_Sal as [LY. Earning],LY_Bonus_Percent as [LY. %],LY_Total_Days as [LY. Days],";
                            query = query + " (BD.Final_Bonus_Amt - LY_Final_Bonus_Amt) as [CY - LY]";
                            query = query + " from EmployeeDetails ED inner join Bonus_Details BD on BD.EmpNo=ED.EmpNo And BD.Lcode=ED.Lcode";
                            query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo inner join SalaryMaster SM on SM.EmpNo=ED.EmpNo";
                            query = query + " inner join MstDepartment MD on MD.DepartmentCd=ED.Department ";
                            query = query + " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "' And ED.EmployeeType='" + txtEmployeeType_Rpt.SelectedValue + "'";
                            query = query + " And BD.Ccode='" + SessionCcode + "' And BD.Lcode='" + SessionLcode + "' And BD.Bonus_Year='" + txtBonusYear.SelectedValue + "'";

                            //Activate Employee Check
                            if (RdbLeftType.SelectedValue != "1")
                            {
                                if (RdbLeftType.SelectedValue == "2")
                                {
                                    query = query + " and ED.ActivateMode='Y'";
                                }
                                if (RdbLeftType.SelectedValue == "3")
                                {
                                    query = query + " and ED.ActivateMode='N'";
                                }
                            }

                            //PF Check
                            if (RdbPFNonPF.SelectedValue.ToString() != "0")
                            {
                                if (RdbPFNonPF.SelectedValue.ToString() == "1")
                                {
                                    //PF Employee
                                    query = query + " and (OP.EligiblePF='1')";
                                }
                                else
                                {
                                    //Non PF Employee
                                    query = query + " and (OP.EligiblePF='2')";
                                }
                            }

                            //Salary Through Bank Or Cash Check
                            if (RdbCashBank.SelectedValue.ToString() != "0")
                            {
                                if (RdbCashBank.SelectedValue.ToString() == "1")
                                {
                                    //Cash Employee
                                    query = query + " and (OP.Salarythrough='1')";
                                }
                                else
                                {
                                    //Bank Employee
                                    query = query + " and (OP.Salarythrough='2')";
                                }
                            }

                            query = query + " group by ED.ExisistingCode,ED.EmpName,MD.DepartmentNm,ED.Designation,OP.Dateofjoining,BD.Gross_Sal,BD.Voucher_Amt,BD.Percent_Gross_Sal,BD.Bonus_Percent,BD.Bonus_Amt,";
                            query = query + " BD.Round_Bonus,BD.Final_Bonus_Amt,LY_Final_Bonus_Amt,LY_Gross_Sal,LY_Bonus_Percent,LY_Total_Days";
                            query = query + " Order by ED.ExisistingCode Asc";
                        }

                        DataTable dt_1 = new DataTable();
                        dt_1 = objdata.RptEmployeeMultipleDetails(query);
                        GridExcelView.DataSource = dt_1;
                        GridExcelView.DataBind();

                        string attachment = "attachment;filename=Bonus_Checklist.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);
                        GridExcelView.RenderControl(htextw);

                        if (txtEmployeeType_Rpt.SelectedValue == "4")
                        {
                            Response.Write("<table>");
                            Response.Write("<tr align='Center'>");
                            Response.Write("<td colspan='16' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
                            Response.Write(CmpName + " - " + SessionLcode);
                            Response.Write("</td>");
                            Response.Write("</tr>");
                            Response.Write("<tr align='Center'>");
                            Response.Write("<td colspan='16' style='font-size:10.0pt;font-weight:bold;text-decoration:underline;''>");
                        }
                        else
                        {
                            Response.Write("<table>");
                            Response.Write("<tr align='Center'>");
                            Response.Write("<td colspan='18' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
                            Response.Write(CmpName + " - " + SessionLcode);
                            Response.Write("</td>");
                            Response.Write("</tr>");
                            Response.Write("<tr align='Center'>");
                            Response.Write("<td colspan='18' style='font-size:10.0pt;font-weight:bold;text-decoration:underline;''>");
                        }

                        if (txtEmployeeType_Rpt.SelectedValue == "1" || txtEmployeeType_Rpt.SelectedValue == "6")
                        {
                            Response.Write("STAFF BONUS CUMULATIVE - FOR THE PERIOD OCTOBER 2014 TO SEPTEMBER 2015");
                        }
                        else if (txtEmployeeType_Rpt.SelectedValue == "3")
                        {
                            Response.Write("WORKERS BONUS CUMULATIVE - FOR THE PERIOD OCTOBER 2014 TO SEPTEMBER 2015");
                        }
                        else if (txtEmployeeType_Rpt.SelectedValue == "4")
                        {
                            Response.Write("WORKERS CASUAL / BONUS CUMULATIVE - FOR THE PERIOD OCTOBER 2014 TO SEPTEMBER 2015");
                        }
                        else
                        {
                            Response.Write("BONUS CUMULATIVE - FOR THE PERIOD OCTOBER 2014 TO SEPTEMBER 2015");
                        }

                        Response.Write("</td>");
                        Response.Write("</tr><tr><td></td></tr>");
                        Response.Write("</table>");

                        Response.Write(stw.ToString());


                        //Total Add
                        string Total_Row_Count = (Convert.ToDecimal(4) + Convert.ToDecimal(GridExcelView.Rows.Count)).ToString();
                        if (txtEmployeeType_Rpt.SelectedValue == "4")
                        {
                            Response.Write("<table border='1'>");
                            Response.Write("<tr>");
                            Response.Write("<td colspan='5' align='right'>TOTAL</td>");
                            Response.Write("<td>=sum(F5:F" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(G5:G" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(H5:H" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(I5:I" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(J5:J" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(K5:K" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(L5:L" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td></td>");
                            Response.Write("<td>=sum(N5:N" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(O5:O" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(P5:P" + Total_Row_Count.ToString() + ")</td>");
                        }
                        else
                        {
                            Response.Write("<table border='1'>");
                            Response.Write("<tr>");
                            Response.Write("<td colspan='5' align='right'>TOTAL</td>");
                            Response.Write("<td>=sum(F5:F" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(G5:G" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(H5:H" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(I5:I" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(J5:J" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(K5:K" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(L5:L" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(M5:M" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(N5:N" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(O5:O" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(P5:P" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(Q5:Q" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(R5:R" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("</tr>");
                            Response.Write("</table>");
                        }
                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                    }

                    if (Report_Type_Call == "Bonus_Individual") //Bonus_Individual
                    {
                        if (SessionLcode == "UNIT I" || SessionLcode == "UNIT IV")
                        {
                            query = "Select ED.ExisistingCode,ED.EmpName,convert(varchar,OP.Dateofjoining,105) as Dateofjoining,MD.DepartmentNm,ED.Designation,";

                            //Experiance Get
                            query = query + " (case when ED.ActivateMode = 'N' then (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, ED.deactivedate)/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, ED.deactivedate) % 12))";
                            query = query + " else (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000') % 12)) end";
                            query = query + " ) as Experiance,";

                            //query = query + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000') % 12)) AS Experiance,";

                            query = query + " (left(DATENAME(MM, SD.FromDate),3) + '-' + right(Year(SD.FromDate),2)) as Months,SD.WDays,SD.NFh,SD.OTDays,cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,";
                            query = query + " cast(SD.BasicHRA as decimal(18,2)) as BasicHRA,cast(SD.ConvAllow as decimal(18,2)) as ConvAllow,cast(SD.EduAllow as decimal(18,2)) as EduAllow,cast(SD.MediAllow as decimal(18,2)) as MediAllow,";
                            query = query + " cast(SD.BasicRAI as decimal(18,2)) as BasicRAI,cast(SD.WashingAllow as decimal(18,2)) as WashingAllow,SD.ThreesidedAmt,SD.DayIncentive,SD.allowances1,SM.Base";
                            query = query + " from EmployeeDetails ED inner join SalaryDetails_Bonus SD on ED.ExisistingCode=SD.ExisistingCode And ED.Lcode=SD.Lcode";
                            query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo inner join SalaryMaster SM on SM.EmpNo=ED.EmpNo";
                            query = query + " inner join MstDepartment MD on MD.DepartmentCd=ED.Department";
                            query = query + " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "' And ED.EmployeeType='" + txtEmployeeType_Rpt.SelectedValue.ToString() + "'";
                            query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                            //query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                            query = query + " And SD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And SD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                        }
                        else
                        {
                            query = "Select ED.ExisistingCode,ED.EmpName,convert(varchar,OP.Dateofjoining,105) as Dateofjoining,MD.DepartmentNm,ED.Designation,";

                            //Experiance Get
                            query = query + " (case when ED.ActivateMode = 'N' then (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, ED.deactivedate)/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, ED.deactivedate) % 12))";
                            query = query + " else (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000') % 12)) end";
                            query = query + " ) as Experiance,";

                            //query = query + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2015-09-30 00:00:00.000') % 12)) AS Experiance,";
                            if (txtEmployeeType_Rpt.SelectedValue == "1" || txtEmployeeType_Rpt.SelectedValue == "6")
                            {
                                query = query + " (left(DATENAME(MM, SD.FromDate),3) + '-' + right(Year(SD.FromDate),2)) as Months,AD.Days as WDays,AD.NFh,cast(AD.WH_Work_Days as decimal(18,2)) as OTDays,cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,";
                            }
                            else
                            {
                                query = query + " (left(DATENAME(MM, SD.FromDate),3) + '-' + right(Year(SD.FromDate),2)) as Months,AD.Days as WDays,AD.NFh,AD.ThreeSided as OTDays,cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,";
                            }
                            query = query + " cast(SD.BasicHRA as decimal(18,2)) as BasicHRA,cast(SD.ConvAllow as decimal(18,2)) as ConvAllow,cast(SD.EduAllow as decimal(18,2)) as EduAllow,cast(SD.MediAllow as decimal(18,2)) as MediAllow,";
                            query = query + " cast(SD.BasicRAI as decimal(18,2)) as BasicRAI,cast(SD.WashingAllow as decimal(18,2)) as WashingAllow,SD.ThreesidedAmt,SD.DayIncentive,SD.allowances1,SM.Base";
                            query = query + " from EmployeeDetails ED inner join SalaryDetails SD on ED.EmpNo=SD.EmpNo And ED.Lcode=SD.Lcode";
                            query = query + " inner join AttenanceDetails AD on AD.EmpNo=ED.EmpNo And SD.Lcode=AD.Lcode And SD.FromDate=AD.FromDate And ED.Lcode=AD.Lcode";
                            query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo inner join SalaryMaster SM on SM.EmpNo=ED.EmpNo";
                            query = query + " inner join MstDepartment MD on MD.DepartmentCd=ED.Department";
                            query = query + " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "' And ED.EmployeeType='" + txtEmployeeType_Rpt.SelectedValue + "'";
                            query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                            query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                            query = query + " And SD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And SD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                            query = query + " And AD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And AD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                        }
                        //Activate Employee Check
                        if (RdbLeftType.SelectedValue != "1")
                        {
                            if (RdbLeftType.SelectedValue == "2")
                            {
                                query = query + " and ED.ActivateMode='Y'";
                            }
                            if (RdbLeftType.SelectedValue == "3")
                            {
                                query = query + " and ED.ActivateMode='N'";
                            }
                        }

                        //PF Check
                        if (RdbPFNonPF.SelectedValue.ToString() != "0")
                        {
                            if (RdbPFNonPF.SelectedValue.ToString() == "1")
                            {
                                //PF Employee
                                query = query + " and (OP.EligiblePF='1')";
                            }
                            else
                            {
                                //Non PF Employee
                                query = query + " and (OP.EligiblePF='2')";
                            }
                        }

                        //Salary Through Bank Or Cash Check
                        if (RdbCashBank.SelectedValue.ToString() != "0")
                        {
                            if (RdbCashBank.SelectedValue.ToString() == "1")
                            {
                                //Cash Employee
                                query = query + " and (OP.Salarythrough='1')";
                            }
                            else
                            {
                                //Bank Employee
                                query = query + " and (OP.Salarythrough='2')";
                            }
                        }
                        query = query + " Order by ED.ExisistingCode,SD.Fromdate Asc";
                        DataTable dt_1 = new DataTable();
                        dt_1 = objdata.RptEmployeeMultipleDetails(query);
                        GridExcelView.DataSource = dt_1;
                        GridExcelView.DataBind();


                        string attachment = "attachment;filename=Bonus_Individual.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);
                        GridExcelView.RenderControl(htextw);

                        Response.Write("<table>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='17' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
                        Response.Write(CmpName + " - " + SessionLcode);
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='17' style='font-size:10.0pt;font-weight:bold;text-decoration:underline;''>");


                        if (txtEmployeeType_Rpt.SelectedValue == "1" || txtEmployeeType_Rpt.SelectedValue == "6")
                        {
                            Response.Write("STAFF BONUS PERIOD FROM OCTOBER 2014 TO SEPTEMBER 2015");
                        }
                        else if (txtEmployeeType_Rpt.SelectedValue == "3")
                        {
                            Response.Write("WORKERS BONUS PERIOD FROM OCTOBER 2014 TO SEPTEMBER 2015");
                        }
                        else if (txtEmployeeType_Rpt.SelectedValue == "4")
                        {
                            Response.Write("WORKERS CASUAL / BONUS PERIOD FROM OCTOBER 2014 TO SEPTEMBER 2015");
                        }
                        else
                        {
                            Response.Write("BONUS PERIOD FROM OCTOBER 2014 TO SEPTEMBER 2015");
                        }

                        Response.Write("</td>");
                        Response.Write("</tr><tr><td></td></tr>");
                        Response.Write("</table>");

                        string OLD_Token_No_Check = "";
                        string Current_Token_No_Check = "";
                        string First_Row_Count = "6";
                        string Last_Row_Count = "0";
                        int SI_Count = 0;
                        for (int k = 0; k < dt_1.Rows.Count; k++)
                        {
                            Current_Token_No_Check = dt_1.Rows[k]["ExisistingCode"].ToString();
                            if (k == 0)
                            {
                                OLD_Token_No_Check = dt_1.Rows[k]["ExisistingCode"].ToString();
                                Response.Write("<table border='1' style='font-weight:bold;'>");
                                Response.Write("<tr>");
                                Response.Write("<td colspan='4'>Name : " + dt_1.Rows[k]["EmpName"] + "</td>");
                                Response.Write("<td colspan='2'>Token No : " + dt_1.Rows[k]["ExisistingCode"] + "</td>");
                                Response.Write("<td colspan='3'>Dept. Name : " + dt_1.Rows[k]["DepartmentNm"] + "</td>");
                                Response.Write("<td colspan='3'>Designation : " + dt_1.Rows[k]["Designation"] + "</td>");
                                Response.Write("<td colspan='2'>DOJ : " + dt_1.Rows[k]["Dateofjoining"] + "</td>");
                                Response.Write("<td colspan='2'>Service : " + dt_1.Rows[k]["Experiance"] + "</td>");
                                Response.Write("<td>" + (k + 1) + "</td>");
                                Response.Write("</tr>");
                                Response.Write("</table>");

                                Response.Write("<table border='1' style='font-weight:bold;'>");
                                Response.Write("<tr>");
                                Response.Write("<td>Month</td><td>Days</td>");
                                Response.Write("<td>NFH</td><td>OT Days</td>");
                                Response.Write("<td>Total Days</td><td>Basic Amount</td>");
                                Response.Write("<td>HRA</td><td>Conv. Allow</td>");
                                Response.Write("<td>Edu. Allow</td><td>Medi. Allow</td>");
                                Response.Write("<td>RAI</td><td>Washing Allow</td>");
                                Response.Write("<td>OT Amount</td><td>Attn. Inc</td>");
                                Response.Write("<td>Earned Amount</td><td>SPG. Inc</td>");
                                Response.Write("<td>Total Amount</td>");
                                Response.Write("</tr>");
                                Response.Write("</table>");
                                Response.Write("</tr>");

                                Response.Write("<table border='1'>");
                                Response.Write("<tr>");
                                Response.Write("<td>" + dt_1.Rows[k]["Months"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["WDays"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["NFh"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["OTDays"] + "</td>");
                                string Total_Days_Cal = (Convert.ToDecimal(dt_1.Rows[k]["WDays"]) + Convert.ToDecimal(dt_1.Rows[k]["NFh"]) + Convert.ToDecimal(dt_1.Rows[k]["OTDays"])).ToString();
                                Response.Write("<td>" + Total_Days_Cal + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["BasicAndDANew"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["ThreesidedAmt"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["DayIncentive"] + "</td>");

                                string Earned_Amt_Cal = "";
                                Earned_Amt_Cal = (Convert.ToDecimal(dt_1.Rows[k]["BasicAndDANew"]) + Convert.ToDecimal(dt_1.Rows[k]["BasicHRA"]) + Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"])).ToString();
                                Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["EduAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["MediAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["BasicRAI"])).ToString();
                                Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["WashingAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["ThreesidedAmt"]) + Convert.ToDecimal(dt_1.Rows[k]["DayIncentive"])).ToString();

                                Response.Write("<td>" + Earned_Amt_Cal + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["allowances1"] + "</td>");

                                string Total_Amt_Cal = "";
                                Total_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["allowances1"])).ToString();
                                Response.Write("<td>" + Total_Amt_Cal + "</td>");

                                Response.Write("</tr>");
                                Response.Write("</table>");
                                SI_Count = SI_Count + 1;
                            }

                            else
                            {
                                if (OLD_Token_No_Check.ToUpper().ToString() == Current_Token_No_Check.ToUpper().ToString())
                                {
                                    Response.Write("<table border='1'>");
                                    Response.Write("<tr>");
                                    Response.Write("<td>" + dt_1.Rows[k]["Months"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["WDays"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["NFh"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["OTDays"] + "</td>");
                                    string Total_Days_Cal = (Convert.ToDecimal(dt_1.Rows[k]["WDays"]) + Convert.ToDecimal(dt_1.Rows[k]["NFh"]) + Convert.ToDecimal(dt_1.Rows[k]["OTDays"])).ToString();
                                    Response.Write("<td>" + Total_Days_Cal + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["BasicAndDANew"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["ThreesidedAmt"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["DayIncentive"] + "</td>");

                                    string Earned_Amt_Cal = "";
                                    Earned_Amt_Cal = (Convert.ToDecimal(dt_1.Rows[k]["BasicAndDANew"]) + Convert.ToDecimal(dt_1.Rows[k]["BasicHRA"]) + Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"])).ToString();
                                    Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["EduAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["MediAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["BasicRAI"])).ToString();
                                    Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["WashingAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["ThreesidedAmt"]) + Convert.ToDecimal(dt_1.Rows[k]["DayIncentive"])).ToString();

                                    Response.Write("<td>" + Earned_Amt_Cal + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["allowances1"] + "</td>");

                                    string Total_Amt_Cal = "";
                                    Total_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["allowances1"])).ToString();
                                    Response.Write("<td>" + Total_Amt_Cal + "</td>");

                                    Response.Write("</tr>");
                                    Response.Write("</table>");
                                    SI_Count = SI_Count + 1;
                                }
                                else
                                {
                                    OLD_Token_No_Check = dt_1.Rows[k]["ExisistingCode"].ToString();
                                    //Total Add
                                    Last_Row_Count = (Convert.ToDecimal(SI_Count) + Convert.ToDecimal(First_Row_Count)).ToString();
                                    Last_Row_Count = (Convert.ToDecimal(Last_Row_Count) - Convert.ToDecimal(1)).ToString();

                                    Response.Write("<table border='1' style='font-weight:bold;'>");
                                    Response.Write("<tr>");
                                    Response.Write("<td>Total</td>");
                                    Response.Write("<td>=sum(B" + First_Row_Count.ToString() + ":B" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(C" + First_Row_Count.ToString() + ":C" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(D" + First_Row_Count.ToString() + ":D" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(E" + First_Row_Count.ToString() + ":E" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(F" + First_Row_Count.ToString() + ":F" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(G" + First_Row_Count.ToString() + ":G" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(H" + First_Row_Count.ToString() + ":H" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(I" + First_Row_Count.ToString() + ":I" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(J" + First_Row_Count.ToString() + ":J" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(K" + First_Row_Count.ToString() + ":K" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(L" + First_Row_Count.ToString() + ":L" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(M" + First_Row_Count.ToString() + ":M" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(N" + First_Row_Count.ToString() + ":N" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(O" + First_Row_Count.ToString() + ":O" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(P" + First_Row_Count.ToString() + ":P" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(Q" + First_Row_Count.ToString() + ":Q" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("</tr>");
                                    Response.Write("</table>");
                                    First_Row_Count = (Convert.ToDecimal(Last_Row_Count) + Convert.ToDecimal(5)).ToString();
                                    SI_Count = 1;
                                    //Total Add End

                                    Response.Write("<table><tr><td></tr></table>");

                                    Response.Write("<table border='1' style='font-weight:bold;'>");
                                    Response.Write("<tr>");
                                    Response.Write("<td colspan='4'>Name : " + dt_1.Rows[k]["EmpName"] + "</td>");
                                    Response.Write("<td colspan='2'>Token No : " + dt_1.Rows[k]["ExisistingCode"] + "</td>");
                                    Response.Write("<td colspan='3'>Dept. Name : " + dt_1.Rows[k]["DepartmentNm"] + "</td>");
                                    Response.Write("<td colspan='3'>Designation : " + dt_1.Rows[k]["Designation"] + "</td>");
                                    Response.Write("<td colspan='2'>DOJ : " + dt_1.Rows[k]["Dateofjoining"] + "</td>");
                                    Response.Write("<td colspan='2'>Service : " + dt_1.Rows[k]["Experiance"] + "</td>");
                                    Response.Write("<td>" + (k + 1) + "</td>");
                                    Response.Write("</tr>");
                                    Response.Write("</table>");

                                    Response.Write("<table border='1' style='font-weight:bold;'>");
                                    Response.Write("<tr>");
                                    Response.Write("<td>Month</td><td>Days</td>");
                                    Response.Write("<td>NFH</td><td>OT Days</td>");
                                    Response.Write("<td>Total Days</td><td>Basic Amount</td>");
                                    Response.Write("<td>HRA</td><td>Conv. Allow</td>");
                                    Response.Write("<td>Edu. Allow</td><td>Medi. Allow</td>");
                                    Response.Write("<td>RAI</td><td>Washing Allow</td>");
                                    Response.Write("<td>OT Amount</td><td>Attn. Inc</td>");
                                    Response.Write("<td>Earned Amount</td><td>SPG. Inc</td>");
                                    Response.Write("<td>Total Amount</td>");
                                    Response.Write("</tr>");
                                    Response.Write("</table>");
                                    Response.Write("</tr>");

                                    Response.Write("<table border='1'>");
                                    Response.Write("<tr>");
                                    Response.Write("<td>" + dt_1.Rows[k]["Months"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["WDays"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["NFh"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["OTDays"] + "</td>");
                                    string Total_Days_Cal = (Convert.ToDecimal(dt_1.Rows[k]["WDays"]) + Convert.ToDecimal(dt_1.Rows[k]["NFh"]) + Convert.ToDecimal(dt_1.Rows[k]["OTDays"])).ToString();
                                    Response.Write("<td>" + Total_Days_Cal + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["BasicAndDANew"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["ThreesidedAmt"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["DayIncentive"] + "</td>");

                                    string Earned_Amt_Cal = "";
                                    Earned_Amt_Cal = (Convert.ToDecimal(dt_1.Rows[k]["BasicAndDANew"]) + Convert.ToDecimal(dt_1.Rows[k]["BasicHRA"]) + Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"])).ToString();
                                    Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["EduAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["MediAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["BasicRAI"])).ToString();
                                    Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["WashingAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["ThreesidedAmt"]) + Convert.ToDecimal(dt_1.Rows[k]["DayIncentive"])).ToString();

                                    Response.Write("<td>" + Earned_Amt_Cal + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["allowances1"] + "</td>");

                                    string Total_Amt_Cal = "";
                                    Total_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["allowances1"])).ToString();
                                    Response.Write("<td>" + Total_Amt_Cal + "</td>");

                                    Response.Write("</tr>");
                                    Response.Write("</table>");
                                }
                            }
                        }

                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);

                    }

                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
