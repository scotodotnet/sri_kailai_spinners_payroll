﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RptAttnReport.aspx.cs" Inherits="RptAttnReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Employee Details</li></h4> 
                    </ol>
               </div>
<div id="main-wrapper" class="container">
<div class="row">
<div class="col-md-12">
              
 <div class="col-md-9">
<div class="panel panel-white">
<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">ATTENDANCE REPORT DETAILS</h4>
				</div>
				</div>
<form class="form-horizontal">
<div class="panel-body">
<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblcategory" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
				     
					   <div class="col-sm-3">
					      <asp:DropDownList ID="ddlcategory" class="form-control" runat="server" 
					      AutoPostBack="True" onselectedindexchanged="ddlcategory_SelectedIndexChanged">
                            </asp:DropDownList>  
					   </div>
				       <div class="col-md-1"></div>
				       <asp:Label ID="lblEmployeeType" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee Type"></asp:Label>
                	  <div class="col-sm-3">
						<asp:DropDownList ID="txtEmployeeType" class="form-control" runat="server" 
						AutoPostBack="true">
                            </asp:DropDownList> 
					  </div>
						
					  </div>
				        
</div>

<div class="col-md-12">     
				       <div class="form-group row">
				       <div class="col-sm-4">
				       <asp:RadioButtonList ID="rdbAttnDaysType" runat="server" RepeatColumns="2" 
                               TabIndex="2" Width="340">
                       <asp:ListItem Selected="true" Text="Over All Attendance" Value="0"></asp:ListItem>
                       <asp:ListItem Selected="False" Text="Statutory Attendance" Value="1"></asp:ListItem>
                       </asp:RadioButtonList>
				       </div>
				      <div class="col-md-1"></div>
				      <div class="col-sm-3">
                          <asp:Button ID="btnAttnReport" class="btn btn-success" runat="server" Text="Attendance View" onclick="btnAttnReport_Click" />
					  </div>
						
					  </div>
				        
</div>

</div>
</form>
</div>
</div>

<!-- Dashboard start -->
 
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
 
 <!-- Dashboard End -->

</div>
</div>
</div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

