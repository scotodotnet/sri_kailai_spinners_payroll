﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Globalization;

public partial class RptDepartmentSalaryAbstract : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    BALDataAccess objdata = new BALDataAccess();

    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Stafflabour;
    DateTime MyDate;
    DateTime MyDate1;
    DateTime MyDate2;

    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(constr);
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            DropDwonCategory();
            alldropdownadd();
        }
       
    }

    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }

    public void alldropdownadd()
    {

        //Financial Year Add
        int currentYear = Utility.GetFinancialYear;
        txtFinancial_Year.Items.Add("----Select----");
        txtFrom_Month.Items.Add("----Select----");
        txtPFMonth.Items.Add("----Select----");
        for (int i = 0; i <= 11; i++)
        {
            txtFinancial_Year.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
            currentYear = currentYear - 1;
        }
        txtFinancial_Year.SelectedIndex = 1;
        //Single Year Add
        txtYear.Items.Add("----Select----");
        currentYear = Utility.GetCurrentYearOnly;
        for (int i = 0; i < 10; i++)
        {
            txtYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
            //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
            currentYear = currentYear - 1;
        }
        txtYear.SelectedIndex = 1;


        //Month Add
        System.Globalization.DateTimeFormatInfo mfi = new
        System.Globalization.DateTimeFormatInfo();
        string strMonthName = "";
        for (int i = 3; i <= 11; i++)
        {
            //strMonthName = mfi.GetAbbreviatedMonthName(i + 1).ToString();
            strMonthName = mfi.GetMonthName(i + 1).ToString();
            txtFrom_Month.Items.Add(strMonthName);
            txtPFMonth.Items.Add(strMonthName);
        }
        for (int i = 0; i <= 2; i++)
        {
            strMonthName = mfi.GetMonthName(i + 1).ToString();
            txtFrom_Month.Items.Add(strMonthName);
            txtPFMonth.Items.Add(strMonthName);
        }
    }

    protected void txtFrom_Month_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void txtPFMonth_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void txtFinancial_Year_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void txtYear_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlrptReport_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnexport_Click(object sender, EventArgs e)
    {
        //All Header Details Get
        string CmpName = "";
        string Cmpaddress = "";
        string From_Month_DB = "";
        string Department_ID = "";
        string NetPay_Count = "";
        bool ErrFlag = false;
        string query = "";
        DataTable NetPay_DT = new DataTable();

        if (txtFrom_Month.SelectedValue == "" || txtFrom_Month.SelectedValue == "----Select----")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month Properly');", true);
            ErrFlag = true;
        }

        int YR = 0;
        if (!ErrFlag)
        {
            if (txtFrom_Month.SelectedValue == "January")
            {
                YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
                YR = YR + 1;
            }
            else if (txtFrom_Month.SelectedValue == "February")
            {
                YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
                YR = YR + 1;
            }
            else if (txtFrom_Month.SelectedValue == "March")
            {
                YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
            }

            if (txtFrom_Month.SelectedValue != "0") { From_Month_DB = txtFrom_Month.SelectedItem.Text.ToString(); }

            int Fin_Year_DB = 0;
            string[] Fin_Year_Split = txtFinancial_Year.SelectedItem.Text.Split('-');
            Fin_Year_DB = Convert.ToInt32(Fin_Year_Split[0].ToString());

            string Str_ChkLeft = "";
            if (ChkLeft.Checked == true)
            {
                Str_ChkLeft = "1";
            }
            else { Str_ChkLeft = "0"; }

            ResponseHelper.Redirect("RptPFESIView.aspx?Months=" + From_Month_DB + "&yr=" + Fin_Year_DB + "&ReportType=" + rbsalary.SelectedValue + "&Leftdate=" + txtLeftDate.Text + "&Left_Emp=" + Str_ChkLeft.ToString(), "_blank", "");



            ////OLD Report Command
            //query = "Select Distinct EmpDet.Department,MstDpt.DepartmentNm,SalDet.WagesType from EmployeeDetails EmpDet" + 
            //        " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department" +
            //        " inner Join SalaryDetails SalDet on SalDet.EmpNo=EmpDet.EmpNo where EmpDet.Ccode='" + SessionCcode + "'" +
            //        " and EmpDet.Lcode='" + SessionLcode + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "'" + 
            //        " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "'" +
            //        " Order by MstDpt.DepartmentNm Asc";

            //SqlCommand cmd = new SqlCommand(query, con);
            //DataTable Department_Dt = new DataTable();
            //SqlDataAdapter sda = new SqlDataAdapter(cmd);
            //con.Open();
            //sda.Fill(Department_Dt);
            //con.Close();

            ////Excel Write
            //string attachment = "attachment; filename=SalaryAbstractDetails.xls";
            //Response.ClearContent();
            //Response.AddHeader("content-disposition", attachment);
            //Response.ContentType = "application/ms-excel";

            //DataTable dt = new DataTable();
            //dt = objdata.Company_retrive(SessionCcode, SessionLcode);
            //if (dt.Rows.Count > 0)
            //{
            //    CmpName = dt.Rows[0]["Cname"].ToString();
            //    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
            //}

            //StringWriter stw = new StringWriter();
            //HtmlTextWriter htextw = new HtmlTextWriter(stw);
            ////griddept.RenderControl(htextw);

            //Response.Write("<table>");
            //Response.Write("<tr align='Center'>");
            //Response.Write("<td colspan='16' style='font-size:16.0pt;font-weight:bold;text-decoration:underline;''>");
            //Response.Write("MURUGAN TEXTILES");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            //Response.Write("<tr align='Center'>");
            //Response.Write("<td colspan='16' style='font-size:13.0pt;font-weight:bold;text-decoration:underline;''>");
            //Response.Write("" + SessionLcode + "");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            //Response.Write("<tr align='Center'>");
            //Response.Write("<td colspan='16' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
            //Response.Write("" + Cmpaddress + "");
            //Response.Write("</td>");
            //Response.Write("</tr>");

            //if (rbsalary.SelectedValue == "2")
            //{
            //    Response.Write("<tr align='center'>");
            //    Response.Write("<td colspan='16' style='font-size:11.0pt;font-weight:bold;text-decoration:underline;''>");
            //    Response.Write("SALARY ABSTRACT THE MONTH OF " + txtFrom_Month.SelectedValue.ToUpper().ToString() + " - " + YR);
            //    Response.Write("</td>");
            //    Response.Write("</tr>");
            //}
            //else
            //{
            //    Response.Write("<tr align='center'>");
            //    Response.Write("<td colspan='16' style='font-size:11.0pt;font-weight:bold;text-decoration:underline;''>");
            //    Response.Write("SALARY ABSTRACT THE MONTH OF " + txtfrom.Text + " - " + txtTo.Text);
            //    Response.Write("</td>");
            //    Response.Write("</tr>");
            //}
            //Response.Write("</table>");

            ////Report Column Heading Add
            //Response.Write("<table border='1' style='font-weight:bold;'><tr align='center' style='vertical-align: middle;'>");
            //Response.Write("<td rowspan='3'>S.NO</td><td rowspan='3'>DEPARTMENT</td><td colspan='4'>STAFFS</td>");

            //Response.Write("<td colspan='7'>LABOUR</td><td rowspan='3'>BANK <br/>TOTAL <br/>AMOUNT</td><td rowspan='3'>CASH <br/>TOTAL <br/>AMOUNT</td><td rowspan='3'>GRAND <br/>TOTAL <br/>AMOUNT</td>");
            ////Response.Write("<td colspan='8'>LABOUR</td><td rowspan='3'>BANK <br/>TOTAL <br/>AMOUNT</td><td rowspan='3'>CASH <br/>TOTAL <br/>AMOUNT</td><td rowspan='3'>GRAND <br/>TOTAL <br/>AMOUNT</td>");

            //Response.Write("</tr><tr align='center' style='vertical-align: middle;'>");
            //Response.Write("<td colspan='2'>GENERAL</td><td colspan='2'>REGULAR</td><td colspan='2'>WORKERS</td><td colspan='2'>CONTRACT</td>");
            //Response.Write("<td rowspan='2'>LEADER COMMISION <br/>& INSENTIVE</td><td rowspan='2'>LABOUR <br/>INSENTIVE</td><td rowspan='2'>HOSTEL GIRLS <br/>INSENTIVE</td>");

            ////Response.Write("<td rowspan='2'>OT Amount</td>");

            //Response.Write("</tr><tr align='center' style='vertical-align: middle;'>");
            //Response.Write("<td>BANK</td><td>CASH</td><td>BANK</td><td>CASH</td><td>BANK</td><td>CASH</td><td>BANK</td><td>CASH</td>");
            //Response.Write("</tr></table>");


            //Int32 Grand_Tot_Start;
            //Int32 Grand_Tot_End;
            //Grand_Tot_Start = 7;
            //for (int i = 0; i < Department_Dt.Rows.Count; i++)
            //{
            //    NetPay_Count = "";
            //    Response.Write("<table border='1'><tr>");
            //    Response.Write("<td>" + (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString() + "</td>");
            //    Response.Write("<td align='center'>" + Department_Dt.Rows[i]["DepartmentNm"].ToString() + "</td>");
            //    Department_ID = Department_Dt.Rows[i]["Department"].ToString();

            //    //General Staff Bank Sum Total
            //    query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and OP.EmpNo=SalDet.EmpNo" +
            //    " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.StafforLabor='S'" +
            //    " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.Salarythrough='2'" +
            //    " and EmpDet.Department='" + Department_ID + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.EmployeeType='1'";
            //    NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
            //    if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
            //    Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

            //    //General Staff CASH Sum Total
            //    query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and OP.EmpNo=SalDet.EmpNo" +
            //    " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.StafforLabor='S'" +
            //    " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.Salarythrough='1'" +
            //    " and EmpDet.Department='" + Department_ID + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.EmployeeType='1'";
            //    NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
            //    if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
            //    Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

            //    //REGULAR Staff Bank Sum Total
            //    query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and OP.EmpNo=SalDet.EmpNo" +
            //    " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.StafforLabor='S'" +
            //    " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.Salarythrough='2'" +
            //    " and EmpDet.Department='" + Department_ID + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.EmployeeType='2'";
            //    NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
            //    if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
            //    Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

            //    //REGULAR Staff CASH Sum Total
            //    query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and OP.EmpNo=SalDet.EmpNo" +
            //    " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.StafforLabor='S'" +
            //    " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.Salarythrough='1'" +
            //    " and EmpDet.Department='" + Department_ID + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.EmployeeType='2'";
            //    NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
            //    if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
            //    Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

            //    //WORKER LABOUR Bank Sum Total
            //    query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and OP.EmpNo=SalDet.EmpNo" +
            //    " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.StafforLabor='L'" +
            //    " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.Salarythrough='2'" +
            //    " and EmpDet.Department='" + Department_ID + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.EmployeeType='4'";
            //    NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
            //    if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
            //    Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";



            //    //WORKER LABOUR CASH Sum Total
            //    query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and OP.EmpNo=SalDet.EmpNo" +
            //    " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.StafforLabor='L'" +
            //    " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.Salarythrough='1'" +
            //    " and EmpDet.Department='" + Department_ID + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.EmployeeType='4'";
            //    NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
            //    if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
            //    Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

            //    //CONTRACT LABOUR Bank Sum Total
            //    query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and OP.EmpNo=SalDet.EmpNo" +
            //    " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.StafforLabor='L'" +
            //    " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.Salarythrough='2'" +
            //    " and EmpDet.Department='" + Department_ID + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.EmployeeType='3'";
            //    NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
            //    if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
            //    Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

            //    //CONTRACT LABOUR CASH Sum Total
            //    query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and OP.EmpNo=SalDet.EmpNo" +
            //    " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.StafforLabor='L'" +
            //    " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.Salarythrough='1'" +
            //    " and EmpDet.Department='" + Department_ID + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.EmployeeType='3'";
            //    NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
            //    if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
            //    Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

            //    //LEADER COMMISION & INSENTIVE Amount
            //    query = "Select round(sum(TL.TLCommission) + sum(TL.TLIncentive),0) as NetPay  from TeamLeader_Commission TL,EmployeeDetails ED,SalaryDetails SalDet where ED.EmpNo=TL.TLEmpNo and SalDet.EmpNo=TL.TLEmpNo" +
            //    " and TL.Months='" + From_Month_DB + "' and TL.FinancialYear='" + Fin_Year_DB + "' and TL.Ccode='" + SessionCcode + "' and TL.Lcode='" + SessionLcode + "' and ED.Department='" + Department_ID + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "'";
            //    NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
            //    if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
            //    Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

            //    //Workers labour Incentive Amount
            //    query = "Select round(sum(WI.Amount),0) as NetPay from EmployeeDetails ED,Worker_Incentive WI,SalaryDetails SalDet where WI.EmpNo=ED.EmpNo and SalDet.EmpNo=WI.EmpNo" +
            //    " and ED.Ccode='" + SessionCcode + "' and ED.Lcode='" + SessionLcode + "' and WI.Ccode='" + SessionCcode + "' and WI.Lcode='" + SessionLcode + "'" +
            //    " and WI.Months='" + From_Month_DB + "' and WI.Fyear='" + Fin_Year_DB + "' and ED.Department='" + Department_ID + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "'";
            //    NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
            //    if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
            //    Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

            //    //Hostel labour Incentive Amount
            //    query = "Select SUM(Amt) AS NetPay from EmployeeDetails ED,HostelIncentive_data HI,SalaryDetails SalDet where HI.EmpNo=ED.EmpNo and SalDet.EmpNo=HI.EmpNo and HI.Ccode='" + SessionCcode + "'" +
            //    " and HI.Lcode='" + SessionLcode + "' and HI.Months='" + From_Month_DB + "' and HI.Finance='" + Fin_Year_DB + "' and ED.Ccode='" + SessionCcode + "'" +
            //    " and ED.Lcode='" + SessionLcode + "' and ED.Department='" + Department_ID + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "'";
            //    NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
            //    if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
            //    Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

            //    ////OT Amount Calculate
            //    //query = "Select SUM(SalDet.OverTime) AS OverTime from SalaryDetails SalDet,EmployeeDetails ED where SalDet.EmpNo=ED.EmpNo and SalDet.Ccode='" + SessionCcode + "'" +
            //    //" and SalDet.Lcode='" + SessionLcode + "' and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and ED.Ccode='" + SessionCcode + "'" +
            //    //" and ED.Lcode='" + SessionLcode + "' and ED.Department='" + Department_ID + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "'";
            //    //NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
            //    //if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
            //    //Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

            //    //Bank Total Amount
            //    query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,MstDepartment MstDpt,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and MstDpt.DepartmentCd = EmpDet.Department and OP.EmpNo=SalDet.EmpNo" +
            //    " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.Salarythrough='2' and EmpDet.Department='" + Department_ID + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "'";
            //    NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
            //    if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
            //    Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

            //    //Cash Total Amount
            //    query = "Select sum(SalDet.NetPay) as NetPay from SalaryDetails SalDet,EmployeeDetails EmpDet,MstDepartment MstDpt,OfficialProfile OP where EmpDet.EmpNo=SalDet.EmpNo and MstDpt.DepartmentCd = EmpDet.Department and OP.EmpNo=SalDet.EmpNo" +
            //    " and SalDet.Month='" + From_Month_DB + "' and SalDet.FinancialYear='" + Fin_Year_DB + "' and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.Salarythrough='1' and EmpDet.Department='" + Department_ID + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "'";
            //    NetPay_DT = objdata.RptEmployeeMultipleDetails(query);
            //    if (NetPay_DT.Rows[0][0].ToString() != "" && NetPay_DT.Rows[0][0].ToString() != "0.00") { NetPay_Count = NetPay_DT.Rows[0][0].ToString(); }
            //    Response.Write("<td>" + NetPay_Count + "</td>"); NetPay_Count = "";

            //    //Grand Total Amount
            //    Grand_Tot_Start++;
            //    Response.Write("<td>=sum(K" + Grand_Tot_Start.ToString() + ":O" + Grand_Tot_Start.ToString() + ")</td>");
            //    //Response.Write("<td>=sum(C" + Grand_Tot_Start.ToString() + ":N" + Grand_Tot_Start.ToString() + ")</td>");

            //    Response.Write("</tr></table>");

            //}
            ////Final Total Amount
            //if (Department_Dt.Rows.Count.ToString() != "0")
            //{
            //    Grand_Tot_End = Grand_Tot_Start;
            //    Response.Write("<table border='1'><tr>");
            //    Response.Write("<td colspan='2' align='right'>TOTAL AMOUNT</td>");

            //    Response.Write("<td>=sum(C8:C" + Grand_Tot_End.ToString() + ")</td>");
            //    Response.Write("<td>=sum(D8:D" + Grand_Tot_End.ToString() + ")</td>");
            //    Response.Write("<td>=sum(E8:E" + Grand_Tot_End.ToString() + ")</td>");
            //    Response.Write("<td>=sum(F8:F" + Grand_Tot_End.ToString() + ")</td>");
            //    Response.Write("<td>=sum(G8:G" + Grand_Tot_End.ToString() + ")</td>");
            //    Response.Write("<td>=sum(H8:H" + Grand_Tot_End.ToString() + ")</td>");
            //    Response.Write("<td>=sum(I8:I" + Grand_Tot_End.ToString() + ")</td>");
            //    Response.Write("<td>=sum(J8:J" + Grand_Tot_End.ToString() + ")</td>");
            //    Response.Write("<td>=sum(K8:K" + Grand_Tot_End.ToString() + ")</td>");
            //    Response.Write("<td>=sum(L8:L" + Grand_Tot_End.ToString() + ")</td>");
            //    Response.Write("<td>=sum(M8:M" + Grand_Tot_End.ToString() + ")</td>");
            //    Response.Write("<td>=sum(N8:N" + Grand_Tot_End.ToString() + ")</td>");
            //    Response.Write("<td>=sum(O8:O" + Grand_Tot_End.ToString() + ")</td>");
            //    Response.Write("<td>=sum(P8:P" + Grand_Tot_End.ToString() + ")</td>");

            //    //Response.Write("<td>=sum(Q8:Q" + Grand_Tot_End.ToString() + ")</td>");

            //    Response.Write("</tr></table>");
            //}
            //Response.End();
            //Response.Clear();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
        }
    }

    protected void rbsalary_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (rbsalary.SelectedValue == "2")
        //{
        //    pnlMonth.Visible = false;
        //}
        //else if (rbsalary.SelectedValue == "1")
        //{
        //    pnlMonth.Visible = true;
        //}
        //else if (rbsalary.SelectedValue == "3")
        //{
        //    pnlMonth.Visible = true;
        //}
        //else
        //{
        //    pnlMonth.Visible = false;
        //}
    }
    protected void txtfrom_TextChanged(object sender, EventArgs e)
    {

    }
    protected void txtTo_TextChanged(object sender, EventArgs e)
    {

    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        //EmployeeType();
        EmployeeType_Load();
    }

    public void EmployeeType_Load()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        txtEmployeeType.DataSource = dtemp;
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }

    protected void btnPFAddLeft_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            string query = "";

            //if (ddlcategory.SelectedValue == "0")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
            //    ErrFlag = true;
            //}
            //else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
            //    ErrFlag = true;
            //}

            if ((txtPFMonth.SelectedValue == "0") || (txtPFMonth.SelectedValue == "----Select----"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Months');", true);
                ErrFlag = true;
            }
            else if ((txtYear.SelectedValue == "0") || (txtYear.SelectedValue == "----Select----"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Year Months');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "1")
                    {
                        Stafflabour = "S";
                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {
                        Stafflabour = "L";
                    }



                    //Result Start
                    string Salarymonth = txtPFMonth.SelectedValue.ToString();
                    int monthInDigit = DateTime.ParseExact(Salarymonth, "MMMM", CultureInfo.CurrentCulture).Month;

                    string fromdate_str = "";
                    string Month_str = "";
                    string ToDate_str = "";
                    int Month_Last_Day_Int = DateTime.DaysInMonth(Convert.ToInt32(txtYear.SelectedValue.ToString()), monthInDigit);
                    string Month_Last_Day = Month_Last_Day_Int.ToString();
                    if (monthInDigit.ToString().Length == 2)
                    {
                        Month_str = monthInDigit.ToString();
                    }
                    else
                    {
                        Month_str = "0" + monthInDigit.ToString();
                    }
                    fromdate_str = "01." + Month_str + "." + txtYear.SelectedValue.ToString();
                    ToDate_str = Month_Last_Day + "." + Month_str + "." + txtYear.SelectedValue.ToString();

                    //PF Addition for Selected the Month
                    query = "Select ED.EmpNo,ED.ExisistingCode,ED.EmpName,ED.FatherName,MD.DepartmentNm,convert(varchar,ED.DOB,103) as DOB, " +
                    " convert(varchar,OP.PFDOJ,103) as PFDOJ,OP.PFnumber,OP.ESICnumber from EmployeeDetails ED " +
                    " inner Join OfficialProfile OP on OP.EmpNo=ED.EmpNo " +
                    " inner Join MstDepartment MD on MD.DepartmentCd=ED.Department " +
                    " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "'" +
                    " And OP.EligiblePF='1'" +
                    " And Month(OP.PFDOJ)='" + monthInDigit + "' And Year(OP.PFDOJ)='" + txtYear.SelectedValue.ToString() + "'";

                    if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                    {
                    }
                    else
                    {
                        query = query + " and ED.EmployeeType='" + txtEmployeeType.SelectedValue.ToString() + "'";
                    }
                    query = query + " Order by ED.ExisistingCode Asc";

                    SqlCommand cmd = new SqlCommand(query, con);
                    DataTable dt_1 = new DataTable();
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    //DataSet ds1 = new DataSet();
                    con.Open();
                    sda.Fill(dt_1);
                    con.Close();
                    GVPFAddition.DataSource = dt_1;
                    GVPFAddition.DataBind();

                    //PF Deletion for Selected the Month
                    query = "Select ED.EmpNo,ED.ExisistingCode,ED.EmpName,ED.FatherName,MD.DepartmentNm,convert(varchar,ED.DOB,103) as DOB, " +
                    " convert(varchar,OP.PFDOJ,103) as PFDOJ,OP.PFnumber,OP.ESICnumber,convert(varchar,ED.deactivedate,103) as LEFT_Date from EmployeeDetails ED " +
                    " inner Join OfficialProfile OP on OP.EmpNo=ED.EmpNo " +
                    " inner Join MstDepartment MD on MD.DepartmentCd=ED.Department " +
                    " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "'" +
                    " And OP.EligiblePF='1'" +
                    " And Month(ED.deactivedate)='" + monthInDigit + "' And Year(ED.deactivedate)='" + txtYear.SelectedValue.ToString() + "'";

                    if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                    {
                    }
                    else
                    {
                        query = query + " and ED.EmployeeType='" + txtEmployeeType.SelectedValue.ToString() + "'";
                    }
                    query = query + " Order by ED.deactivedate,ED.ExisistingCode Asc";

                    cmd = new SqlCommand(query, con);
                    sda = new SqlDataAdapter(cmd);
                    DataTable dt_PF_Deletion = new DataTable();
                    con.Open();
                    sda.Fill(dt_PF_Deletion);
                    con.Close();
                    GVPFDeletion.DataSource = dt_PF_Deletion;
                    GVPFDeletion.DataBind();

                    if (dt_PF_Deletion.Rows.Count == 0 && dt_1.Rows.Count == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Record Found...');", true);
                        ErrFlag = true;
                    }
                    if (!ErrFlag)
                    {

                        //Company Details Find
                        string attachment = "attachment;filename=PFAddition_Deletion.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        DataTable dt = new DataTable();
                        dt = objdata.Company_retrive(SessionCcode, SessionLcode);
                        if (dt.Rows.Count > 0)
                        {
                            CmpName = dt.Rows[0]["Cname"].ToString();
                            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                        }

                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);
                        GVPFAddition.RenderControl(htextw);

                        Response.Write("<table>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='10'>");
                        Response.Write("" + CmpName + "");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='10'>");
                        Response.Write("" + SessionLcode + "");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='10'>");
                        Response.Write("" + Cmpaddress + "");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        string Salary_Head = "PF ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str;

                        if (txtEmployeeType.SelectedValue.ToString() == "1") { Salary_Head = "STAFF PF ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "2") { Salary_Head = "SUB-STAFF PF ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "3") { Salary_Head = "REGULAR PF ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "4") { Salary_Head = "HOSTEL PF ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "5") { Salary_Head = "CIVIL PF ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "6") { Salary_Head = "Watch & Ward PF ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                        {
                            Salary_Head = "PF ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str;
                        }

                        Response.Write("<tr align='Center'><td colspan='10'>");
                        Response.Write(Salary_Head);
                        Response.Write("</td></tr>");
                        Response.Write("</table>");
                        Response.Write("<table>");
                        Response.Write("<tr align='Left'><td></td><td colspan='2'style='font-weight:bold;text-decoration:underline;'>PF Addition</td></tr>");
                        Response.Write("<tr><td colspan='3'></td></tr>");
                        Response.Write("</table>");

                        //gvSalary.RenderControl(htextw);
                        //Response.Write("Contract Details");

                        //PF Addition
                        Response.Write("<table>");
                        Response.Write("<tr><td></td>");
                        Response.Write("<td>" + stw.ToString() + "</td>");
                        Response.Write("</tr></table>");
                        Response.Write("<table>");
                        Response.Write("<tr><td></td></tr>");
                        Response.Write("</table>");

                        //PF Deletion
                        StringWriter stw_PFDeletion = new StringWriter();
                        HtmlTextWriter htextw_PFDeletion = new HtmlTextWriter(stw_PFDeletion);
                        GVPFDeletion.RenderControl(htextw_PFDeletion);

                        Response.Write("<table>");
                        Response.Write("<tr align='Left'><td></td><td colspan='2'style='font-weight:bold;text-decoration:underline;'>PF Deletion</td></tr>");
                        Response.Write("<tr><td colspan='3'></td></tr>");
                        Response.Write("</table>");

                        Response.Write("<table>");
                        Response.Write("<tr><td></td>");
                        Response.Write("<td>" + stw_PFDeletion.ToString() + "</td>");
                        Response.Write("</tr></table>");

                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);

                    }






                    //Result End

                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnESIAddLeft_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            string query = "";

            //if (ddlcategory.SelectedValue == "0")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
            //    ErrFlag = true;
            //}
            //else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
            //    ErrFlag = true;
            //}

            if ((txtPFMonth.SelectedValue == "0") || (txtPFMonth.SelectedValue == "----Select----"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Months');", true);
                ErrFlag = true;
            }
            else if ((txtYear.SelectedValue == "0") || (txtYear.SelectedValue == "----Select----"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Year Months');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "1")
                    {
                        Stafflabour = "S";
                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {
                        Stafflabour = "L";
                    }

                    //Result Start
                    string Salarymonth = txtPFMonth.SelectedValue.ToString();
                    int monthInDigit = DateTime.ParseExact(Salarymonth, "MMMM", CultureInfo.CurrentCulture).Month;

                    string fromdate_str = "";
                    string Month_str = "";
                    string ToDate_str = "";
                    int Month_Last_Day_Int = DateTime.DaysInMonth(Convert.ToInt32(txtYear.SelectedValue.ToString()), monthInDigit);
                    string Month_Last_Day = Month_Last_Day_Int.ToString();
                    if (monthInDigit.ToString().Length == 2)
                    {
                        Month_str = monthInDigit.ToString();
                    }
                    else
                    {
                        Month_str = "0" + monthInDigit.ToString();
                    }
                    fromdate_str = "01." + Month_str + "." + txtYear.SelectedValue.ToString();
                    ToDate_str = Month_Last_Day + "." + Month_str + "." + txtYear.SelectedValue.ToString();

                    //ESI Addition for Selected the Month
                    query = "Select ED.EmpNo,ED.ExisistingCode,ED.EmpName,ED.FatherName,MD.DepartmentNm,convert(varchar,ED.DOB,103) as DOB, " +
                    " convert(varchar,OP.ESIDOJ,103) as ESIDOJ,OP.PFnumber,OP.ESICnumber from EmployeeDetails ED " +
                    " inner Join OfficialProfile OP on OP.EmpNo=ED.EmpNo " +
                    " inner Join MstDepartment MD on MD.DepartmentCd=ED.Department " +
                    " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "'" +
                    " And OP.EligiblePF='1'" +
                    " And Month(OP.ESIDOJ)='" + monthInDigit + "' And Year(OP.ESIDOJ)='" + txtYear.SelectedValue.ToString() + "'";

                    if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                    {
                    }
                    else
                    {
                        query = query + " and ED.EmployeeType='" + txtEmployeeType.SelectedValue.ToString() + "'";
                    }
                    query = query + " Order by ED.ExisistingCode Asc";

                    SqlCommand cmd = new SqlCommand(query, con);
                    DataTable dt_1 = new DataTable();
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    //DataSet ds1 = new DataSet();
                    con.Open();
                    sda.Fill(dt_1);
                    con.Close();
                    GVESIAddition.DataSource = dt_1;
                    GVESIAddition.DataBind();

                    //ESI Deletion for Selected the Month
                    query = "Select ED.EmpNo,ED.ExisistingCode,ED.EmpName,ED.FatherName,MD.DepartmentNm,convert(varchar,ED.DOB,103) as DOB, " +
                    " convert(varchar,OP.ESIDOJ,103) as ESIDOJ,OP.PFnumber,OP.ESICnumber,convert(varchar,ED.deactivedate,103) as LEFT_Date from EmployeeDetails ED " +
                    " inner Join OfficialProfile OP on OP.EmpNo=ED.EmpNo " +
                    " inner Join MstDepartment MD on MD.DepartmentCd=ED.Department " +
                    " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "'" +
                    " And OP.EligiblePF='1'" +
                    " And Month(ED.deactivedate)='" + monthInDigit + "' And Year(ED.deactivedate)='" + txtYear.SelectedValue.ToString() + "'";
                    if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                    {
                    }
                    else
                    {
                        query = query + " and ED.EmployeeType='" + txtEmployeeType.SelectedValue.ToString() + "'";
                    }
                    query = query + " Order by ED.deactivedate,ED.ExisistingCode Asc";

                    cmd = new SqlCommand(query, con);
                    sda = new SqlDataAdapter(cmd);
                    DataTable dt_ESI_Deletion = new DataTable();
                    con.Open();
                    sda.Fill(dt_ESI_Deletion);
                    con.Close();
                    GVESIDeletion.DataSource = dt_ESI_Deletion;
                    GVESIDeletion.DataBind();

                    if (dt_ESI_Deletion.Rows.Count == 0 && dt_1.Rows.Count == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Record Found...');", true);
                        ErrFlag = true;
                    }
                    if (!ErrFlag)
                    {
                        //Company Details Find
                        string attachment = "attachment;filename=ESIAddition_Deletion.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        DataTable dt = new DataTable();
                        dt = objdata.Company_retrive(SessionCcode, SessionLcode);
                        if (dt.Rows.Count > 0)
                        {
                            CmpName = dt.Rows[0]["Cname"].ToString();
                            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                        }

                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);
                        GVESIAddition.RenderControl(htextw);

                        Response.Write("<table>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='10'>");
                        Response.Write("" + CmpName + "");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='10'>");
                        Response.Write("" + SessionLcode + "");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='10'>");
                        Response.Write("" + Cmpaddress + "");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        string Salary_Head = "ESI ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str;

                        if (txtEmployeeType.SelectedValue.ToString() == "1") { Salary_Head = "STAFF ESI ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "2") { Salary_Head = "SUB-STAFF ESI ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "3") { Salary_Head = "REGULAR ESI ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "4") { Salary_Head = "HOSTEL ESI ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "5") { Salary_Head = "CIVIL ESI ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "6") { Salary_Head = "Watch & Ward ESI ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                        {
                            Salary_Head = "ESI ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str;
                        }

                        Response.Write("<tr align='Center'><td colspan='10'>");
                        Response.Write(Salary_Head);
                        Response.Write("</td></tr>");
                        Response.Write("</table>");
                        Response.Write("<table>");
                        Response.Write("<tr align='Left'><td></td><td colspan='2'style='font-weight:bold;text-decoration:underline;'>ESI Addition</td></tr>");
                        Response.Write("<tr><td colspan='3'></td></tr>");
                        Response.Write("</table>");

                        //gvSalary.RenderControl(htextw);
                        //Response.Write("Contract Details");

                        //ESI Addition
                        Response.Write("<table>");
                        Response.Write("<tr><td></td>");
                        Response.Write("<td>" + stw.ToString() + "</td>");
                        Response.Write("</tr></table>");
                        Response.Write("<table>");
                        Response.Write("<tr><td></td></tr>");
                        Response.Write("</table>");

                        //ESI Deletion
                        StringWriter stw_ESIDeletion = new StringWriter();
                        HtmlTextWriter htextw_ESIDeletion = new HtmlTextWriter(stw_ESIDeletion);
                        GVESIDeletion.RenderControl(htextw_ESIDeletion);

                        Response.Write("<table>");
                        Response.Write("<tr align='Left'><td></td><td colspan='2'style='font-weight:bold;text-decoration:underline;'>ESI Deletion</td></tr>");
                        Response.Write("<tr><td colspan='3'></td></tr>");
                        Response.Write("</table>");

                        Response.Write("<table>");
                        Response.Write("<tr><td></td>");
                        Response.Write("<td>" + stw_ESIDeletion.ToString() + "</td>");
                        Response.Write("</tr></table>");

                        Response.End();
                        Response.Clear();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Success Fully');", true);

                    }






                    //Result End

                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnEmpAddLeft_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            string query = "";

            //if (ddlcategory.SelectedValue == "0")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
            //    ErrFlag = true;
            //}
            //else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
            //    ErrFlag = true;
            //}

            if ((txtPFMonth.SelectedValue == "0") || (txtPFMonth.SelectedValue == "----Select----"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Months');", true);
                ErrFlag = true;
            }
            else if ((txtYear.SelectedValue == "0") || (txtYear.SelectedValue == "----Select----"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Year Months');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "1")
                    {
                        Stafflabour = "S";
                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {
                        Stafflabour = "L";
                    }

                    //Result Start
                    string Salarymonth = txtPFMonth.SelectedValue.ToString();
                    int monthInDigit = DateTime.ParseExact(Salarymonth, "MMMM", CultureInfo.CurrentCulture).Month;
                    string fromdate_str = "";
                    string Month_str = "";
                    string ToDate_str = "";
                    int Month_Last_Day_Int = DateTime.DaysInMonth(Convert.ToInt32(txtYear.SelectedValue.ToString()), monthInDigit);
                    string Month_Last_Day = Month_Last_Day_Int.ToString();
                    if (monthInDigit.ToString().Length == 2)
                    {
                        Month_str = monthInDigit.ToString();
                    }
                    else
                    {
                        Month_str = "0" + monthInDigit.ToString();
                    }
                    fromdate_str = "01." + Month_str + "." + txtYear.SelectedValue.ToString();
                    ToDate_str = Month_Last_Day + "." + Month_str + "." + txtYear.SelectedValue.ToString();

                    //Employee Addition
                    query = "Select ED.EmpNo,ED.ExisistingCode,ED.EmpName,ED.FatherName,MD.DepartmentNm,ED.Designation," +
                    " convert(varchar,OP.Dateofjoining,103) as DOJ," +
                    " Case isnull(OP.EligiblePF,2) When 1 then convert(varchar,OP.PFDOJ,103) Else '' End as PFDOJ," +
                    " Case isnull(OP.EligiblePF,2) When 1 then OP.PFnumber Else '' End as PFnumber," +
                    " Case isnull(OP.ElgibleESI,2) When 1 then convert(varchar,OP.ESIDOJ,103) Else '' End as ESIDOJ," +
                    " Case isnull(OP.ElgibleESI,2) When 1 then OP.ESICnumber Else '' End as ESICnumber,cast(SM.Base as decimal(18,2)) as Base" +
                    " from EmployeeDetails ED inner Join OfficialProfile OP on OP.EmpNo=ED.EmpNo " +
                    " inner Join SalaryMaster SM on SM.EmpNo=ED.EmpNo inner Join MstDepartment MD on MD.DepartmentCd=ED.Department" +
                    " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "'" +
                    " And Month(OP.Dateofjoining)='" + monthInDigit + "' And Year(OP.Dateofjoining)='" + txtYear.SelectedValue.ToString() + "'";

                    if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                    {
                        //Skip
                    }
                    else
                    {
                        query = query + " And ED.EmployeeType='" + txtEmployeeType.SelectedValue.ToString() + "'";
                    }
                    query = query + "Order by ED.ExisistingCode Asc";

                    SqlCommand cmd = new SqlCommand(query, con);
                    DataTable dt_1 = new DataTable();
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    //DataSet ds1 = new DataSet();
                    con.Open();
                    sda.Fill(dt_1);
                    con.Close();
                    GVEmpAddLeft.DataSource = dt_1;
                    GVEmpAddLeft.DataBind();

                    //Employee Deletion
                    query = "Select ED.EmpNo,ED.ExisistingCode,ED.EmpName,ED.FatherName,MD.DepartmentNm,ED.Designation," +
                    " convert(varchar,OP.Dateofjoining,103) as DOJ," +
                    " Case isnull(OP.EligiblePF,2) When 1 then convert(varchar,OP.PFDOJ,103) Else '' End as PFDOJ," +
                    " Case isnull(OP.EligiblePF,2) When 1 then OP.PFnumber Else '' End as PFnumber," +
                    " Case isnull(OP.ElgibleESI,2) When 1 then convert(varchar,OP.ESIDOJ,103) Else '' End as ESIDOJ," +
                    " Case isnull(OP.ElgibleESI,2) When 1 then OP.ESICnumber Else '' End as ESICnumber,cast(SM.Base as decimal(18,2)) as Base," +
                    " convert(varchar,ED.deactivedate,103) as Left_Date" +
                    " from EmployeeDetails ED inner Join OfficialProfile OP on OP.EmpNo=ED.EmpNo " +
                    " inner Join SalaryMaster SM on SM.EmpNo=ED.EmpNo inner Join MstDepartment MD on MD.DepartmentCd=ED.Department" +
                    " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "'" +
                    " And Month(ED.deactivedate)='" + monthInDigit + "' And Year(ED.deactivedate)='" + txtYear.SelectedValue.ToString() + "'";

                    if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                    {
                        //Skip
                    }
                    else
                    {
                        query = query + " And ED.EmployeeType='" + txtEmployeeType.SelectedValue.ToString() + "'";
                    }
                    query = query + "Order by ED.deactivedate,ED.ExisistingCode Asc";
                    cmd = new SqlCommand(query, con);
                    DataTable dt_EmpDeletion = new DataTable();
                    sda = new SqlDataAdapter(cmd);
                    con.Open();
                    sda.Fill(dt_EmpDeletion);
                    con.Close();
                    GVEmpDeletion.DataSource = dt_EmpDeletion;
                    GVEmpDeletion.DataBind();

                    if (dt_EmpDeletion.Rows.Count == 0 && dt_1.Rows.Count == 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Record Found...');", true);
                        ErrFlag = true;
                    }
                    if (!ErrFlag)
                    {
                        //Company Details Find
                        string attachment = "attachment;filename=Employee_Addition_Deletion.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        DataTable dt = new DataTable();
                        dt = objdata.Company_retrive(SessionCcode, SessionLcode);
                        if (dt.Rows.Count > 0)
                        {
                            CmpName = dt.Rows[0]["Cname"].ToString();
                            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                        }

                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);
                        GVEmpAddLeft.RenderControl(htextw);

                        Response.Write("<table>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='13'>");
                        Response.Write("" + CmpName + "");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='13'>");
                        Response.Write("" + SessionLcode + "");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='13'>");
                        Response.Write("" + Cmpaddress + "");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        string Salary_Head = "ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str;

                        if (txtEmployeeType.SelectedValue.ToString() == "1") { Salary_Head = "STAFF ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "2") { Salary_Head = "SUB-STAFF ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "3") { Salary_Head = "REGULAR ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "4") { Salary_Head = "HOSTEL ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "5") { Salary_Head = "CIVIL ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }
                        if (txtEmployeeType.SelectedValue.ToString() == "6") { Salary_Head = "Watch & Ward ADDITION AND DELETION REPORT FOR THE PERIOD FROM " + fromdate_str + " TO " + ToDate_str; }

                        Response.Write("<tr align='Center'><td colspan='13'>");
                        Response.Write(Salary_Head);
                        Response.Write("</td></tr>");
                        Response.Write("<tr align='Center'><td colspan='13'></td></tr>");
                        Response.Write("</table>");

                        Response.Write("<table>");
                        Response.Write("<tr align='Left'><td></td><td colspan='4'style='font-weight:bold;text-decoration:underline;'>New Employee Enrollment For The Month Of " + txtPFMonth.SelectedValue.ToString() + " - " + txtYear.SelectedValue.ToString() + "</td></tr>");
                        Response.Write("<tr><td colspan='3'></td></tr>");
                        Response.Write("</table>");

                        //gvSalary.RenderControl(htextw);
                        //Response.Write("Contract Details");

                        //Employee Addition
                        Response.Write("<table>");
                        Response.Write("<tr><td></td>");
                        Response.Write("<td>" + stw.ToString() + "</td>");
                        Response.Write("</tr></table>");
                        Response.Write("<table>");
                        Response.Write("<tr><td></td></tr>");
                        Response.Write("</table>");


                        //Employee Deletion Det
                        StringWriter stw_EmpDeletion = new StringWriter();
                        HtmlTextWriter htextw_EmpDeletion = new HtmlTextWriter(stw_EmpDeletion);
                        GVEmpDeletion.RenderControl(htextw_EmpDeletion);

                        Response.Write("<table>");
                        Response.Write("<tr align='Left'><td></td><td colspan='4'style='font-weight:bold;text-decoration:underline;'>Employee Deletion For the Month Of " + txtPFMonth.SelectedValue.ToString() + " - " + txtYear.SelectedValue.ToString() + "</td></tr>");
                        Response.Write("<tr><td colspan='3'></td></tr>");
                        Response.Write("</table>");

                        Response.Write("<table>");
                        Response.Write("<tr><td></td>");
                        Response.Write("<td>" + stw_EmpDeletion.ToString() + "</td>");
                        Response.Write("</tr></table>");

                        Response.Write("<table>");
                        Response.Write("<tr><td></td></tr><tr><td></td></tr>");
                        Response.Write("</table>");

                        Response.End();
                        Response.Clear();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Success Fully');", true);

                    }






                    //Result End

                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void txtLeftDate_TextChanged(object sender, EventArgs e)
    {

    }

    protected void txt3AFromDate_TextChanged(object sender, EventArgs e)
    {

    }

    protected void txt3AToDate_TextChanged(object sender, EventArgs e)
    {

    }

    protected void btnPFForm3A_Click(object sender, EventArgs e)
    {

        bool ErrFlag = false;
        string query = "";
        DataTable NetPay_DT = new DataTable();

        if (ddlcategory.SelectedValue == "0" || ddlcategory.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
            ErrFlag = true;
        }
        else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
            ErrFlag = true;
        }
        else if (txt3AFromDate.Text == "" || txt3AToDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the FROM DATE And TO DATE');", true);
            ErrFlag = true;
        }


        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "1") { Stafflabour = "S"; }
            else if (ddlcategory.SelectedValue == "2") { Stafflabour = "L"; }


            string Str_ChkLeft = "";
            if (ChkLeft.Checked == true)
            {
                Str_ChkLeft = "1";
            }
            else { Str_ChkLeft = "0"; }

            ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + "" + "&Months=" + "" + "&yr=" + "2014" + "&fromdate=" + txt3AFromDate.Text + "&ToDate=" + txt3AToDate.Text + "&Salary=" + "1" + "&CashBank=" + "1" + "&ESICode=" + "" + "&EmpType=" + txtEmployeeType.SelectedValue.ToString() + "&PayslipType=" + "1" + "&PFTypePost=" + "" + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Leftdate=" + txtLeftDate.Text + "&Report_Type=PF_Form3A_REPORT", "_blank", "");

        }

    }

    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
