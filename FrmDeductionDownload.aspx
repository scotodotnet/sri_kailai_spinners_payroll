﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FrmDeductionDownload.aspx.cs" Inherits="FrmDeductionDownload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Deduction Download</li></h4> 
                    </ol>
             </div>
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
    <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Deduction Download</h4>
				</div>
				</div>
				<form class="form-horizontal">
				   <div align="center">
					       <asp:Label ID="Label1" for="input-Default" class="col-sm-12 control-label" 
                                Text="WAGES TYPE" runat="server" Font-Bold="True" Font-Underline="True"></asp:Label>
				        
				   </div>
				   
				   <div class="panel-body">
				           <div class="row">
					    <div class="form-group col-md-12">
					      
					    <div align="center">
					    
					        
					    
					    
					    
					     <div class="form-group col-md-2"></div>
					     <div class="form-group col-md-9">
					         <asp:RadioButtonList ID="rbsalary" runat="server" AutoPostBack="true" 
                              RepeatColumns="3" onselectedindexchanged="rbsalary_SelectedIndexChanged">
                              <asp:ListItem Text ="Weekly Wages" Value="1"></asp:ListItem>
                              <asp:ListItem Text="Monthly Wages" Value="2" ></asp:ListItem>                                                                                                                                       
                              <asp:ListItem Text="Bi-Monthly Wages" Value="3"></asp:ListItem>
                            </asp:RadioButtonList>
                            
					        <%--<div class="form-group col-md-3">
                            <asp:RadioButton ID="RadioButton1" runat="server" Text="WeeklyWages"/>
                            </div>
                            <div class="form-group col-md-3">
                            <asp:RadioButton ID="RadioButton2" runat="server" Text="MonthlyWages" />
                            </div>
                            <div class="form-group col-md-3">
                            <asp:RadioButton ID="RadioButton3" runat="server" Text="Bi-Monthly Wages"/>
                         </div>   --%>
                         </div>
				            </div>
                     </div>
                </div>

                           
			<div class="form-group row">
					  <asp:Label ID="Label3" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
						<div class="col-sm-3">
                            <asp:DropDownList ID="ddlcategory" class="form-control" runat="server" AutoPostBack="True"  
                                                                        onselectedindexchanged="ddlcategory_SelectedIndexChanged">
                            </asp:DropDownList>                          
                        </div>
					
				    <div class="col-sm-1"></div>
				       				    
				     <label for="input-Default" class="col-sm-2 control-label">Employee Type</label>
					 <div class="col-sm-3">
                    <asp:DropDownList ID="txtEmployeeType" class="form-control" runat="server">
                            </asp:DropDownList>   
					  </div>
			       </div>
				   </div>
			                        
            
			
			
			
			
			<div class="form-group row">
			<table class="full">
			<tbody>
			<tr ID="Panelload" runat="server" Visible="false">
                                                                <td colspan="5">
                                                                    <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="false" >
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>DeptName</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="DeptName" runat="server" Text='<%# Eval("DepartmentNm") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <%--<asp:TemplateField>
                                                                            <HeaderTemplate>MachineID</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="MachineID" runat="server" Text='<%# Eval("MachineNo") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>EmpNo</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>ExistingCode</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="ExistingCode" runat="server" Text='<%# Eval("ExisistingCode") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>FirstName</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="FirstName" runat="server" Text='<%# Eval("EmpName") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Allowance 3</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="Days" runat="server" Text="0.00" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Allowance 4</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="CL" runat="server" Text="0.00" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Allowance 5</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblHomeTown" runat="server" Text="0.00" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Fancy</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblAbsent" runat="server" Text="0.00" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Uni</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblweekoff" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Main</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDed5" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Advance</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblAdvance" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>EB</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDedOthers1" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Mess</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDedOthers2" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                         <asp:TemplateField>
                                                                            <HeaderTemplate>Atm</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDedOthers2" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                         <asp:TemplateField>
                                                                            <HeaderTemplate>Fine</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFine" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Gift</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblGift" runat="server" Text="0.00" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                       
                                                                    </Columns>
                                                                </asp:GridView>
                                                                </td>
                                                            </tr>
			
		  			
			<tr ID="WagesPanel" runat="server" Visible="false">
                                                                <td colspan="5">
                                                                    <asp:GridView ID="GVWages" runat="server" AutoGenerateColumns="false" >
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>EmpNo</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>MachineID</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblMachineID" runat="server" Text='<%# Eval("BiometricID") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>TokenNo</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="btnTokenNo" runat="server" Text='<%# Eval("ExisistingCode") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>                                                                        
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Department</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Name</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="FirstName" runat="server" Text='<%# Eval("EmpName") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>OLD Wages</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblWages" runat="server" Text='<%# Eval("Base") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>New Wages</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblNewWages" runat="server" Text='<%# Eval("Base") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>                                                                        
                                                                    </Columns>
                                                                </asp:GridView>
                                                                </td>
                                                            </tr>
			
		
			
			<tr ID="Tr1" runat="server" Visible="false">
                                                                <td colspan="5">
                                                                    <asp:GridView ID="GVHostelWages" runat="server" AutoGenerateColumns="false" >
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>EmpNo</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>MachineID</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblMachineID" runat="server" Text='<%# Eval("BiometricID") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>TokenNo</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="btnTokenNo" runat="server" Text='<%# Eval("ExisistingCode") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>                                                                        
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Department</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Name</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="FirstName" runat="server" Text='<%# Eval("EmpName") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>OLD Wages</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblWages" runat="server" Text='<%# Eval("OLD_Wages") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>New Wages</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblNewWages" runat="server" Text='<%# Eval("New_Wages") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Total Worked Days</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblTotal_Days" runat="server" Text='<%# Eval("Total_Days") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>                                                                         
                                                                    </Columns>
                                                                </asp:GridView>
                                                                </td>
                                                            </tr>
			
			</tbody>
			</table>
			</div>
			
			 
				<div class="form-group row">
				<div class="col-sm-2"></div>
					 <div class="col-sm-3">
                         <asp:Button ID="btnDownload" runat="server" class="btn btn-success" Text="Download" onclick="btnDownload_Click"/>
					 </div>
				  <div class="col-sm-1">  </div>
				  <div class="col-sm-3">
				  <asp:Button ID="btnWagesDownload" runat="server" class="btn btn-success" Text="Wages Download" onclick="btnWagesDownload_Click"/>
					 </div>   
			   </div>
           
			
			
		                        
			</form>
			
			</div>
			</div>
	  
		    
		
 	 <!-- Dashboard start -->
 	
 	<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                           
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                          
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
 
    <!-- Dashboard End -->
 
  
  </div> <!-- col 12 end -->
 </div><!-- row end -->
      
  </div>
</ContentTemplate>

<Triggers>
      <asp:PostBackTrigger ControlID="btnDownload"  />
 </Triggers>
<Triggers>
      <asp:PostBackTrigger ControlID="btnWagesDownload"  />
 </Triggers>

</asp:UpdatePanel>


</asp:Content>

