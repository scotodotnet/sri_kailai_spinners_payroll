﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RptEmployeeSalaryHistory.aspx.cs" Inherits="RptEmployeeSalaryHistory" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Salary History</li></h4> 
                    </ol>
              </div>
<div id="main-wrapper" class="container">
<div class="row">
<div class="col-md-12">
              
 <div class="col-md-9">
<div class="panel panel-white">
<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">EMPLOYEE SALARY HISTORY DETAILS</h4>
				</div>
				</div>

<form class="form-horizontal">
<div class="panel-body">
<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblcategory" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
				     
					   <div class="col-sm-3">
					      <asp:DropDownList ID="ddlcategory" class="form-control" runat="server" 
					      AutoPostBack="True" onselectedindexchanged="ddlcategory_SelectedIndexChanged" >
                            </asp:DropDownList>  
					   </div>
				       <div class="col-md-1"></div>
				       <asp:Label ID="lbldept" runat="server" class="col-sm-2 control-label" 
                                                             Text="Department"></asp:Label>
                	  <div class="col-sm-3">
						<asp:DropDownList ID="ddldept" class="form-control" runat="server" 
						AutoPostBack="true" onselectedindexchanged="ddldept_SelectedIndexchanged">
                            </asp:DropDownList> 
					  </div>
						
					   </div>
				        
</div>

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblEmployee_No" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee No"></asp:Label>
				     
					   <div class="col-sm-3">
					      <asp:DropDownList ID="txtEmployeeNo" class="form-control" runat="server" 
					      AutoPostBack="True" onselectedindexchanged="txtEmployeeNo_SelectedIndexChanged">
                            </asp:DropDownList>  
					   </div>
				       <div class="col-md-1"></div>
				       <asp:Label ID="lblEmployee_Name" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee Name"></asp:Label>
                	  <div class="col-sm-3">
						<asp:DropDownList ID="txtEmployeeName" class="form-control" runat="server" 
						AutoPostBack="true" onselectedindexchanged="txtEmployeeName_SelectedIndexChanged" >
                            </asp:DropDownList> 
					  </div>
						
					   </div>
				        
</div>

<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblTokenNo" runat="server" class="col-sm-2 control-label" 
                                                             Text="Existing No"></asp:Label>
  
   <div class="col-sm-3">
      <asp:DropDownList ID="txtTokenNo" class="form-control" runat="server" AutoPostBack="true" 
           onselectedindexchanged="txtTokenNo_SelectedIndexChanged">
      </asp:DropDownList>  
     </div>
    <div class="col-md-1"></div>
    <asp:Label ID="lblFinancialYear" runat="server" class="col-sm-2 control-label" 
                                                             Text="Token No Search"></asp:Label>                       
  
    <div class="col-sm-3">
        <asp:TextBox ID="txtTokenSearch" class="form-control" runat="server" AutoPostBack="true" onclick="btnTokenSearch_Click"></asp:TextBox>
        
	</div>
						
    <div class="col-sm-1"></div>
     
	</div>
		        
 </div> 
 
<div class="col-md-12">     
 <div class="form-group row">
 
   <div class="col-sm-5"></div>
    <div class="col-md-1"></div>
    <div class="col-sm-2"></div>
    <div class="col-sm-2">
   <asp:Button ID="btnTokenSearch" class="btn btn-info"  runat="server" Text="Search" onclick="btnTokenSearch_Click"/>
	</div>
						
    
     
	</div>
		        
 </div> 

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblFromDate" runat="server" class="col-sm-2 control-label" 
                                                             Text="From Date"></asp:Label>
				     
					   <div class="col-sm-3">
					     <asp:TextBox ID="txtFromDate" class="form-control" runat="server" AutoPostBack="true" ontextchanged="txtFromDate_TextChanged" ></asp:TextBox> 
					     <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtFromDate" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
                         <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR1" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtFromDate" ValidChars="0123456789/- "></cc1:FilteredTextBoxExtender>
                                                       
					   </div>
				       <div class="col-md-1"></div>
				       <asp:Label ID="lblToDate" runat="server" class="col-sm-2 control-label" 
                                                             Text="To Date"></asp:Label>
                	  <div class="col-sm-3">
						<asp:TextBox ID="txtToDate" class="form-control" runat="server" AutoPostBack="true" ontextchanged="txtToDate_TextChanged"></asp:TextBox> 
					    <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtToDate" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR2" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtToDate" ValidChars="0123456789/- "></cc1:FilteredTextBoxExtender>
                           
					  </div>
						
					   </div>
				        
</div>

<asp:Panel ID="Result_Panel" Visible="false" runat="server">
<div class="col-md-12">     
<div class="form-group row">
<table class="full">
				                                <tbody>
				                                    <tr>
                                                        <td colspan="4">
                                                            <asp:GridView ID="griddept" runat="server" AutoGenerateColumns="false" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Month</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblMonth" runat="server" Text='<%# Eval("[Month]") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Salary Date</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblSalaryDate" runat="server" Text= '<%# Eval("SalaryDate") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Working Days</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblWorkedDays" runat="server" Text='<%#Eval("WorkedDays") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>OT Hours</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblNoHrs" runat="server" Text='<%#Eval("NoHrs") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                         <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField>
                                                                    <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFixedSal" runat="server" Text='<%#Eval("FixedSal") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                     <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                    <HeaderTemplate>Fixed OT Salary</HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFixedOT" runat="server" Text='<%#Eval("FixedOT") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                     <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                    <HeaderTemplate>Base</HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblBase" runat="server" Text='<%#Eval("Base")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                     <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                    <HeaderTemplate>ESI</HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblESI" runat="server" Text='<%#Eval("ESI")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                     <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                     </asp:TemplateField>
                                                                     <asp:TemplateField>
                                                                     <HeaderTemplate>PF</HeaderTemplate>
                                                                     <ItemTemplate>
                                                                         <asp:Label ID="lblPfSalary" runat="server" Text='<%#Eval("PfSalary") %>'></asp:Label>
                                                                     </ItemTemplate>
                                                                      <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                     </asp:TemplateField>
                                                                     <asp:TemplateField>
                                                                     <HeaderTemplate>Any Other Deduction</HeaderTemplate>
                                                                     <ItemTemplate>
                                                                         <asp:Label ID="lblOtherDet" runat="server" Text='<%#Eval("OtherDet") %>'></asp:Label>
                                                                     </ItemTemplate>
                                                                      <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                                                                                     
                                                                     </asp:TemplateField>
                                                                     <asp:TemplateField>
                                                                     <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                                     <ItemTemplate>
                                                                         <asp:Label ID="lblTotalDeductions" runat="server" Text='<%#Eval("TotalDeductions") %>'></asp:Label>
                                                                     </ItemTemplate>
                                                                      <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                     </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Net Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("NetSalary") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>OT Amount</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblOvetTime" runat="server" Text='<%# Eval("OvetTime") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Net Pay</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblNetPay" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <asp:GridView ID="GridView1" runat="server">
                                                        </asp:GridView>
                                                    </td>
                                                    </tr>
				                                </tbody>
				                            </table>
</div>
</div>
</asp:Panel>

<div class="col-md-12">     
				       <div class="form-group row">
				       <div align="center">
                           <asp:Button ID="btnReportView" runat="server" class="btn btn-success" Text="Wages View" onclick="btnReportView_Click"/>
				       </div>
						
					   </div>
				        
</div>

</div>
</form>
</div>
</div>

<!-- Dashboard start -->
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>  
 <!-- Dashboard End -->

</div>  <!-- col-md-12 End -->
</div>  <!-- row End -->
</div>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

