﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;



public partial class RptDepartmentWiseEmployee : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string Emp_ESI_Code;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        if (SessionAdmin == "1")
        {
           // PanelOnrole.Visible = true;
        }
        else
        {
           // PanelOnrole.Visible = false;
        }
        if (!IsPostBack)
        {
            DropDwonCategory();
            alldropdownadd();
            //ESICode_load();
        }
        //lblComany.Text = SessionCcode + " - " + SessionLcode;
        //lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }

    public void ESICode_load()
    {

        //DataTable dt_Empty = new DataTable();
        //ddESI.DataSource = dt_Empty;
        //ddESI.DataBind();
        //DataTable dt = new DataTable();
        //dt = objdata.ESICode_GRID();
        //DataRow dr = dt.NewRow();
        //dr["ESIno"] = "--Select--";
        //dt.Rows.InsertAt(dr, 0);
        //ddESI.DataSource = dt;
        //ddESI.DataValueField = "ESIno";
        //ddESI.DataTextField = "ESIno";
        //ddESI.DataBind();
    }

    //saba
    public void alldropdownadd()
    {

        //Year Add
        int currentYear = Utility.GetFinancialYear;
        txtFromYear.Items.Add("----Select----");
        txtToYear.Items.Add("----Select----");
        txtMonth.Items.Add("----Select----");
        txtSortby.Items.Add("----Select----");
        for (int i = 1950; i <= currentYear; i++)
        {
            txtFromYear.Items.Add(i.ToString());
            txtToYear.Items.Add(i.ToString());
            //currentYear = currentYear - 1;
        }
        //Month Add
        System.Globalization.DateTimeFormatInfo mfi = new
        System.Globalization.DateTimeFormatInfo();
        string strMonthName = "";
        for (int i = 0; i <= 11; i++)
        {
            strMonthName = mfi.GetAbbreviatedMonthName(i + 1).ToString();
            txtMonth.Items.Add(strMonthName);
        }

        //Qualification
        DataTable dtQuali = new DataTable();
        dtQuali = objdata.Qualification();
        txtQualification.DataSource = dtQuali;
        txtQualification.DataTextField = "QualificationNm";
        txtQualification.DataValueField = "QualificationCd";
        txtQualification.DataBind();

        //Designation
        DataTable dtDesign = new DataTable();
        dtDesign = objdata.Emp_Designation();
        for (int i = 0; i <= dtDesign.Rows.Count - 1; i++)
        {
            dtDesign.Rows[i][1] = i;
        }
        txtDesignation.DataSource = dtDesign;
        txtDesignation.DataTextField = "Designation";
        txtDesignation.DataValueField = "DesignationNo";
        txtDesignation.DataBind();

        //Sortby Add
        txtSortby.Items.Add("Employee No");
        txtSortby.Items.Add("Employee Name");
        txtSortby.Items.Add("Machine No");
        txtSortby.Items.Add("Exisisting Code");
        txtSortby.Items.Add("Date of Joining");
        txtSortby.Items.Add("De-Activate Date");
        txtSortby.Items.Add("Date of Birth");
        txtSortby.Items.Add("Qualification");
        txtSortby.Items.Add("Department");
        txtSortby.Items.Add("Designation");
        txtSortby.Items.Add("Employee Type");


    }
    //saba end

    protected void ddldept_SelectedIndexchanged(object sender, EventArgs e)
    {

        //Clear Form
        Result_Panel.Visible = false;
        //rbtngender.SelectedIndex = 0;

        //Clear All DropDown Box
        if (ddldept.SelectedIndex != 0)
        {
            //ddldept.SelectedIndex = 0;
            //txtFromYear.SelectedIndex = 0;
            //txtToYear.SelectedIndex = 0;
            //txtMonth.SelectedIndex = 0;
            //txtQualification.SelectedIndex = 0;
            //txtDesignation.SelectedIndex = 0;
            //txtEmployeeType.SelectedIndex = 0;
            //txtSortby.SelectedIndex = 0;
        }
    }



    protected void btnexport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        //string tbl_From_Year = "";
        //string tbl_To_Year = "";
        string Query_Val = "";

        //if (DeActiveReports.Checked == true)
        //{

        //    DataTable dd = new DataTable();
        //    dd = objdata.deptdeactive(SessionCcode,ddldept.SelectedValue.ToString());
        //    griddept.DataSource = dd;
        //    griddept.DataBind();
        //    //DataTable dd = new DataTable();
        //    //dd = objdata.sal_his_all_report(SessionCcode, SessionLcode, txtFinancial_Year.SelectedValue.ToString());

        //    //GridView1.DataSource = dd;
        //    //GridView1.DataBind();
        //}
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Employee Category');", true);
            //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        //Sabapathy
        //if (ddldept.SelectedIndex == 0 && txtFromYear.SelectedIndex == 0 && txtToYear.SelectedIndex == 0 && txtMonth.SelectedIndex == 0 && txtQualification.SelectedIndex == 0 && txtDesignation.SelectedIndex == 0 && txtEmployeeType.SelectedIndex == 0)
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Any One Serach Type');", true);
        //    //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    ErrFlag = true;
        //}
        if (txtSortby.SelectedIndex == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Sort By');", true);
            //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }

        if (ddlrptReport.SelectedIndex == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Export Type');", true);
            //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        //if (DeActiveReports.Checked = true)
        //{
        //    string Category = "0";
        //    string Deactive_From_date = txtdeactivefrom.Text.ToString();
        //    string Deactive_To_Date = txtdeactiveto.Text.ToString();
        //    DateTime Deactive_From_Date_DB_Format;
        //    DateTime Deactive_To_Date_DB_Format;
        //    Query_Val = "Select ED.EmpNo,ED.OldID,ED.ExisistingCode,ED.EmpName,convert(varchar,OP.Dateofjoining,106) as DOJ,";
        //    Query_Val = Query_Val + " convert(varchar,ED.DOB,106) as DOB,MD.DepartmentNm,ED.Designation,ME.EmpType,";
        //    Query_Val = Query_Val + " Case isnull(OP.ProfileType,1) When  1 then 'Temporary Period' When  2 then 'Confirmation'";
        //    Query_Val = Query_Val + " When  3 then 'General Period' Else '' End as ProfileType,Case isnull(OP.Salarythrough,1)";
        //    Query_Val = Query_Val + " When  1 then 'Cash' When  2 then 'Bank' Else '' End as Salarythrough,ESICnumber,PFnumber,";
        //    Query_Val = Query_Val + " Case isnull(OP.Wagestype,1) When  1 then 'Weekly Wages' When  2 then 'Monthly Wages'";
        //    Query_Val = Query_Val + " When  3 then 'Bi-Monthly Wages' Else '' End as Wagestype,CONVERT(DECIMAL(10,2),SM.Base) as Base_Salary,CONVERT(DECIMAL(10,2),SM.PFS) as OTSalary from";
        //    Query_Val = Query_Val + " EmployeeDetails ED,MstEmployeeType ME,MstDepartment MD,";
        //    Query_Val = Query_Val + " Officialprofile OP,SalaryMaster SM,MstQualification MQ where";
        //    Query_Val = Query_Val + " ME.EmpTypeCd=ED.EmployeeType and MD.DepartmentCd=ED.Department";
        //    Query_Val = Query_Val + " and OP.EmpNo=ED.EmpNo and SM.EmpNo=ED.EmpNo and MQ.QualificationCd=ED.Qualification";
        //    Query_Val = Query_Val + " and ED.stafforlabor = '" + Category + "' and ED.Ccode='" + SessionCcode + "'";
        //    Query_Val = Query_Val + " and ED.Lcode='" + SessionLcode + "'";
        //    if (Deactive_From_date.ToString() != "" && Deactive_To_Date.ToString() != "")
        //    {
        //          Deactive_From_Date_DB_Format = DateTime.ParseExact(txtdeactivefrom.Text, "dd-MM-yyyy", null);
        //            Deactive_To_Date_DB_Format = DateTime.ParseExact(txtdeactiveto.Text, "dd-MM-yyyy", null);
        //        Query_Val = Query_Val + " and deactivedate >= convert(datetime, deactivedate, 105) and deactivedate <= convert(datetime,deactivedate, 105)";
        //    }


        //    DataTable dd = new DataTable();
        //    dd = objdata.deptdeactive(SessionCcode, ddldept.SelectedItem.Text);
        //    griddept.DataSource = dd;
        //    griddept.DataBind();



        //}








        if (!ErrFlag)
        {
            btnClick_Click(sender, e);
            if (ddlrptReport.SelectedValue == "1")
            {

                string attachment = "attachment; filename=EmployeeDetailsReport.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                if (chkDeActive.Checked == true)
                {
                    GridViewdeactiveded.RenderControl(htextw);
                }
                else
                {

                    griddept.RenderControl(htextw);
                }
                Response.Write("<table><tr Font-Bold='true'><td colspan='13' align='center'>EMPLOYEE DETAILS</td></tr></table>");
                Response.Write(stw.ToString());

                Response.End();
            }
            else if (ddlrptReport.SelectedValue == "2")
            {

                string attachment = "attachment; filename=DepartmentwiseEmployee.pdf";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/pdf";
                StringWriter stw = new StringWriter();

                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                if (chkDeActive.Checked == true)
                {
                    GridViewdeactiveded.RenderControl(htextw);
                }
                else
                {
                    griddept.RenderControl(htextw);
                }
                Document document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);


                PdfWriter.GetInstance(document, Response.OutputStream);
                document.Open();
                StringReader str = new StringReader(stw.ToString());
                HTMLWorker htmlworker = new HTMLWorker(document);
                htmlworker.Parse(str);
                document.Close();
                Response.Write(document);
                Response.End();
            }
            else if (ddlrptReport.SelectedValue == "3")
            {

                string attachment = "attachment; filename=DepartmentwiseEmployee.doc";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                if (chkDeActive.Checked == true)
                {
                    GridViewdeactiveded.RenderControl(htextw);
                }
                else
                {
                    griddept.RenderControl(htextw);
                }
                Response.Write("<table><tr Font-Bold='true'><td colspan='13' align='center'>EMPLOYEE DETAILS</td></tr></table>");
                Response.Write(stw.ToString());
                Response.End();
            }
        }

    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddldept.DataSource = dtempty;
        ddldept.DataBind();
        griddept.DataSource = dtempty;
        griddept.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(Stafflabour, SessionCcode, SessionLcode);
        if (dtDip.Rows.Count > 1)
        {
            ddldept.DataSource = dtDip;
            ddldept.DataTextField = "DepartmentNm";
            ddldept.DataValueField = "DepartmentCd";
            ddldept.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldept.DataSource = dtempty;
            ddldept.DataBind();
        }
        //Employee Type
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        txtEmployeeType.DataSource = dtemp;
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();

        //Clear All DropDown Box
        ddldept.SelectedIndex = 0;
        txtFromYear.SelectedIndex = 0;
        txtToYear.SelectedIndex = 0;
        txtMonth.SelectedIndex = 0;
        txtQualification.SelectedIndex = 0;
        txtDesignation.SelectedIndex = 0;
        txtEmployeeType.SelectedIndex = 0;
        txtSortby.SelectedIndex = 0;
        Result_Panel.Visible = false;
    }
    protected void btnClick_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string Query_Val = "";
        string Category = "0";
        string DOBandDOJ = "";
        string SortFieldName = "";
        string tbl_From_Year = "";
        string tbl_To_Year = "";
        int tbl_Month = 0;
        try
        {
            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Employee Category');", true);
                //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            //Sabapathy
            //if (ddldept.SelectedIndex == 0 && txtFromYear.SelectedIndex == 0 && txtToYear.SelectedIndex == 0 && txtMonth.SelectedIndex == 0 && txtQualification.SelectedIndex == 0 && txtDesignation.SelectedIndex == 0 && txtEmployeeType.SelectedIndex == 0)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Any One Serach Type');", true);
            //    //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            if (txtSortby.SelectedIndex == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Sort By');", true);
                //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                //Category
                if (ddlcategory.SelectedValue == "1") { Category = "S"; }
                if (ddlcategory.SelectedValue == "2") { Category = "L"; }

                Query_Val = "";
                Query_Val = "Select ED.EmpNo,ED.OldID,ED.ExisistingCode,ED.EmpName,convert(varchar,OP.Dateofjoining,106) as DOJ,convert(varchar,ED.deactivedate,106) as deactivedate";
                Query_Val = Query_Val + " ,convert(varchar,ED.DOB,106) as DOB,MD.DepartmentNm,ED.Designation,ME.EmpType,";
                Query_Val = Query_Val + " Case isnull(OP.ProfileType,1) When  1 then 'Temporary Period' When  2 then 'Confirmation'";
                Query_Val = Query_Val + " When  3 then 'General Period' Else '' End as ProfileType,Case isnull(OP.Salarythrough,1)";
                Query_Val = Query_Val + " When  1 then 'Cash' When  2 then 'Bank' Else '' End as Salarythrough,ESICnumber,PFnumber,";
                Query_Val = Query_Val + " Case isnull(OP.Wagestype,1) When  1 then 'Weekly Wages' When  2 then 'Monthly Wages'";
                Query_Val = Query_Val + " When  3 then 'Bi-Monthly Wages' Else '' End as Wagestype,CONVERT(DECIMAL(10,2),SM.Base) as Base_Salary,CONVERT(DECIMAL(10,2),SM.PFS) as OTSalary from";
                Query_Val = Query_Val + " EmployeeDetails ED,MstEmployeeType ME,MstDepartment MD,";
                Query_Val = Query_Val + " Officialprofile OP,SalaryMaster SM,MstQualification MQ where";
                Query_Val = Query_Val + " ME.EmpTypeCd=ED.EmployeeType and MD.DepartmentCd=ED.Department";
                Query_Val = Query_Val + " and OP.EmpNo=ED.EmpNo and SM.EmpNo=ED.EmpNo and MQ.QualificationCd=ED.Qualification";
                Query_Val = Query_Val + " and ED.stafforlabor = '" + Category + "' and ED.Ccode='" + SessionCcode + "'";
                Query_Val = Query_Val + " and ED.Lcode='" + SessionLcode + "'";

                //Query_Val = "";
                //Query_Val = "Select ED.EmpNo,ED.EmpName,convert(varchar,ED.DOB,106) as DOB,ME.EmpType,MD.DepartmentNm,";
                //Query_Val = Query_Val + " convert(varchar,OP.Dateofjoining,106) as DOJ,CONVERT(DECIMAL(10,2),SM.Base) as Base,";
                //Query_Val = Query_Val + " ED.ExisistingCode,ED.OldID,MQ.QualificationNm,ED.Designation from";
                //Query_Val = Query_Val + " EmployeeDetails ED,MstEmployeeType ME,MstDepartment MD,";
                //Query_Val = Query_Val + " Officialprofile OP,SalaryMaster SM,MstQualification MQ where";
                //Query_Val = Query_Val + " ME.EmpTypeCd=ED.EmployeeType and MD.DepartmentCd=ED.Department";
                //Query_Val = Query_Val + " and OP.EmpNo=ED.EmpNo and SM.EmpNo=ED.EmpNo and MQ.QualificationCd=ED.Qualification";
                //Query_Val = Query_Val + " and ED.stafforlabor = '" + Category + "' and ED.Ccode='" + SessionCcode + "'";
                //Query_Val = Query_Val + " and ED.Lcode='" + SessionLcode + "'";

                //Gender
                if (rbtngender.SelectedIndex == 1) { Query_Val = Query_Val + " and ED.Gender='1'"; }
                if (rbtngender.SelectedIndex == 2) { Query_Val = Query_Val + " and ED.Gender='2'"; }
                //PF GRADE
                if (rppfgrade.SelectedIndex == 1) { Query_Val = Query_Val + " and ED.NonPFGrade='2'"; }
                if (rppfgrade.SelectedIndex == 2) { Query_Val = Query_Val + " and ED.NonPFGrade='1'"; }
                //Department
                if (ddldept.SelectedIndex != 0) { Query_Val = Query_Val + " and MD.DepartmentNm = '" + ddldept.SelectedItem.Text.ToString() + "'"; }
                //Date of Joining and Date of Birth
                tbl_From_Year = txtFromYear.SelectedItem.Text.ToString();
                tbl_To_Year = txtToYear.SelectedItem.Text.ToString();
                tbl_Month = txtMonth.SelectedIndex;
                if (RdbDOJDOB.SelectedIndex == 0) { DOBandDOJ = "OP.Dateofjoining"; }
                if (RdbDOJDOB.SelectedIndex == 1) { DOBandDOJ = "ED.DOB"; }
                if (DOBandDOJ != "")
                {
                    if (txtFromYear.SelectedIndex != 0 && txtToYear.SelectedIndex != 0) { Query_Val = Query_Val + " and Year(" + DOBandDOJ + ") >= '" + tbl_From_Year + "' And Year(" + DOBandDOJ + ") <= '" + tbl_To_Year + "'"; }
                    if (txtFromYear.SelectedIndex != 0 && txtMonth.SelectedIndex != 0) { Query_Val = Query_Val + " and Year(" + DOBandDOJ + ") = '" + tbl_From_Year + "' And Month(" + DOBandDOJ + ") = '" + tbl_Month + "'"; }
                    if (txtFromYear.SelectedIndex != 0 && txtMonth.SelectedIndex == 0 && txtToYear.SelectedIndex == 0) { Query_Val = Query_Val + " and Year(" + DOBandDOJ + ") = '" + tbl_From_Year + "'"; }
                    if (txtFromYear.SelectedIndex == 0 && txtMonth.SelectedIndex != 0 && txtToYear.SelectedIndex == 0) { Query_Val = Query_Val + " and Month(" + DOBandDOJ + ") = '" + tbl_Month + "'"; }
                }
                //Qualification
                if (txtQualification.SelectedIndex != 0) { Query_Val = Query_Val + " and MQ.QualificationNm = '" + txtQualification.SelectedItem.Text.ToString() + "'"; }
                //Designation
                if (txtDesignation.SelectedIndex != 0) { Query_Val = Query_Val + " and ED.Designation = '" + txtDesignation.SelectedItem.Text.ToString() + "'"; }
                //Employee Type
                if (txtEmployeeType.SelectedIndex != 0) { Query_Val = Query_Val + " and ME.EmpType = '" + txtEmployeeType.SelectedItem.Text.ToString() + "'"; }

                //Active Check
                if (chkDeActive.Checked == false) { Query_Val = Query_Val + " and ED.ActivateMode = 'Y'"; }

                //Deactive Check
                if (chkDeActive.Checked == true) { Query_Val = Query_Val + " and ED.ActivateMode = 'N'"; }

                //Deactive Date Check
                if (chkDeActive.Checked == true)
                {
                    //griddept.Columns[6].Visible = true;
                    //mygrid.Columns[2].Visible = false;
                    //lblDeActiveCheckBox.Visible=
                    //lblDeActivateDate.Visible = true;
                    string Deactive_From_date = txtdeactivefrom.Text.ToString();
                    string Deactive_To_Date = txtdeactiveto.Text.ToString();
                    DateTime Deactive_From_Date_DB_Format;
                    DateTime Deactive_To_Date_DB_Format;
                    if (Deactive_From_date.ToString() != "" && Deactive_To_Date.ToString() != "")
                    {
                        Deactive_From_Date_DB_Format = DateTime.ParseExact(txtdeactivefrom.Text, "dd-MM-yyyy", null);
                        Deactive_To_Date_DB_Format = DateTime.ParseExact(txtdeactiveto.Text, "dd-MM-yyyy", null);
                        Query_Val = Query_Val + " and ED.deactivedate >= convert(datetime, '" + Deactive_From_Date_DB_Format + "', 105)" +
                        " and ED.deactivedate <= convert(datetime, '" + Deactive_To_Date_DB_Format + "', 105)";
                    }
                }

                ////ESI Code Check
                //Emp_ESI_Code = ddESI.SelectedValue.ToString();

                //if (Emp_ESI_Code == "--Select--") { Emp_ESI_Code = ""; }
                //if (Emp_ESI_Code != "") { Query_Val = Query_Val + " and OP.ESICode='" + Emp_ESI_Code + "'"; }

                //Set Order by
                SortFieldName = "";
                if (txtSortby.SelectedValue == "Employee No") { SortFieldName = "ED.EmpNo"; }
                if (txtSortby.SelectedValue == "Employee Name") { SortFieldName = "ED.EmpName"; }
                if (txtSortby.SelectedValue == "Machine No") { SortFieldName = "ED.MachineNo"; }
                if (txtSortby.SelectedValue == "Exisisting Code") { SortFieldName = "ED.ExisistingCode"; }
                if (txtSortby.SelectedValue == "Date of Joining") { SortFieldName = "OP.Dateofjoining"; }
                if (txtSortby.SelectedValue == "De-Activate Date") { SortFieldName = "ED.deactivedate"; }
                if (txtSortby.SelectedValue == "Date of Birth") { SortFieldName = "ED.DOB"; }
                if (txtSortby.SelectedValue == "Qualification") { SortFieldName = "MQ.QualificationNm"; }
                if (txtSortby.SelectedValue == "Department") { SortFieldName = "MD.DepartmentNm"; }
                if (txtSortby.SelectedValue == "Designation") { SortFieldName = "ED.Designation"; }
                if (txtSortby.SelectedValue == "Employee Type") { SortFieldName = "ME.EmpType"; }
                //Order by
                if (SortFieldName != "") { Query_Val = Query_Val + " Order by " + SortFieldName + " Asc"; }


                //Final Result
                //Result_Panel.Visible = true;
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(Query_Val);
                griddept.DataSource = dt;
                griddept.DataBind();
                GridViewdeactiveded.DataSource = dt;
                GridViewdeactiveded.DataBind();
            }
            //Sabapathy END



            //if (ddldept.SelectedValue == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department Name');", true);
            //    //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            //if (!ErrFlag)
            //{
            //    Result_Panel.Visible = true;
            //    DataTable dt = new DataTable();
            //    if (ddlcategory.SelectedValue == "1")
            //    {
            //        string Cate = "S";
            //        if (SessionAdmin == "1")
            //        {
            //            dt = objdata.DepartwiseEmployee(ddldept.SelectedValue, rbtngender.SelectedValue, "Yes", Cate, SessionCcode, SessionLcode, rppfgrade.SelectedValue);
            //        }
            //        else
            //        {
            //            dt = objdata.DepartwiseEmployee(ddldept.SelectedValue, rbtngender.SelectedValue, "No", Cate, SessionCcode, SessionLcode, rppfgrade.SelectedValue);
            //        }
            //        griddept.DataSource = dt;
            //        griddept.DataBind();
            //    }
            //    else if (ddlcategory.SelectedValue == "2")
            //    {
            //        string Cate = "L";
            //        if (SessionAdmin == "1")
            //        {
            //            dt = objdata.DepartwiseEmployee(ddldept.SelectedValue, rbtngender.SelectedValue, "Yes", Cate, SessionCcode, SessionLcode, rppfgrade.SelectedValue);
            //        }
            //        else
            //        {
            //            dt = objdata.DepartwiseEmployee(ddldept.SelectedValue, rbtngender.SelectedValue, "No", Cate, SessionCcode, SessionLcode, rppfgrade.SelectedValue);
            //        }
            //        griddept.DataSource = dt;
            //        griddept.DataBind();
            //    }


            //}
        }
        catch (Exception ex)
        { }
    }
    protected void rbtngender_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }

    protected void txtFromYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtFromYear.SelectedIndex != 0)
        {
            //Clear All DropDown Box
            //ddldept.SelectedIndex = 0;
            ////txtFromYear.SelectedIndex = 0;
            //txtToYear.SelectedIndex = 0;
            //txtMonth.SelectedIndex = 0;
            //txtQualification.SelectedIndex = 0;
            //txtDesignation.SelectedIndex = 0;
            //txtEmployeeType.SelectedIndex = 0;
            //txtSortby.SelectedIndex = 0;
            Result_Panel.Visible = false;
        }
    }
    protected void txtToYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtToYear.SelectedIndex != 0)
        {
            ////Clear All DropDown Box
            //ddldept.SelectedIndex = 0;
            ////txtFromYear.SelectedIndex = 0;
            ////txtToYear.SelectedIndex = 0;
            //txtMonth.SelectedIndex = 0;
            //txtQualification.SelectedIndex = 0;
            //txtDesignation.SelectedIndex = 0;
            //txtEmployeeType.SelectedIndex = 0;
            ////txtSortby.SelectedIndex = 0;
            Result_Panel.Visible = false;
        }
    }
    protected void txtMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtMonth.SelectedIndex != 0)
        {
            ////Clear All DropDown Box
            //ddldept.SelectedIndex = 0;
            ////txtFromYear.SelectedIndex = 0;
            //txtToYear.SelectedIndex = 0;
            ////txtMonth.SelectedIndex = 0;
            //txtQualification.SelectedIndex = 0;
            //txtDesignation.SelectedIndex = 0;
            //txtEmployeeType.SelectedIndex = 0;
            ////txtSortby.SelectedIndex = 0;
            Result_Panel.Visible = false;
        }
    }
    protected void txtQualification_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtQualification.SelectedIndex != 0)
        {
            ////Clear All DropDown Box
            //ddldept.SelectedIndex = 0;
            //txtFromYear.SelectedIndex = 0;
            //txtToYear.SelectedIndex = 0;
            //txtMonth.SelectedIndex = 0;
            ////txtQualification.SelectedIndex = 0;
            //txtDesignation.SelectedIndex = 0;
            //txtEmployeeType.SelectedIndex = 0;
            ////txtSortby.SelectedIndex = 0;
            Result_Panel.Visible = false;
        }
    }
    protected void txtDesignation_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtDesignation.SelectedIndex != 0)
        {
            ////Clear All DropDown Box
            //ddldept.SelectedIndex = 0;
            //txtFromYear.SelectedIndex = 0;
            //txtToYear.SelectedIndex = 0;
            //txtMonth.SelectedIndex = 0;
            //txtQualification.SelectedIndex = 0;
            ////txtDesignation.SelectedIndex = 0;
            //txtEmployeeType.SelectedIndex = 0;
            ////txtSortby.SelectedIndex = 0;
            Result_Panel.Visible = false;
        }
    }
    protected void txtEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtEmployeeType.SelectedIndex != 0)
        {
            ////Clear All DropDown Box
            //ddldept.SelectedIndex = 0;
            //txtFromYear.SelectedIndex = 0;
            //txtToYear.SelectedIndex = 0;
            //txtMonth.SelectedIndex = 0;
            //txtQualification.SelectedIndex = 0;
            //txtDesignation.SelectedIndex = 0;
            ////txtEmployeeType.SelectedIndex = 0;
            ////txtSortby.SelectedIndex = 0;
            Result_Panel.Visible = false;
        }
    }
    protected void RdbDOJDOB_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void txtSortby_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
