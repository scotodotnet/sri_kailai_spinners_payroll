﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SalaryAdvance.aspx.cs" Inherits="SalaryAdvance" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Salary Advance</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
<div class="row">
<div class="col-md-12">
              
<div class="col-md-9">
<div class="panel panel-white">
<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Salary Advance</h4>
				</div>
				</div>

<form class="form-horizontal">
    <div class="panel-body"> 

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="Label7" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
				       
					   <div class="col-sm-3">
					      <asp:DropDownList ID="ddlcategory" class="form-control" runat="server" 
					      AutoPostBack="True" onselectedindexchanged="ddlcategory_SelectedIndexChanged">
                            </asp:DropDownList>  
					   </div>
				       <div class="col-md-1"></div>
                	   <label for="input-Default" class="col-sm-2 control-label">Department</label>
					   <div class="col-sm-3">
						<asp:DropDownList ID="ddlDepartment" class="form-control" runat="server"
						AutoPostBack="true" onselectedindexchanged="ddlDepartment_SelectedIndexChanged">
                            </asp:DropDownList> 
					  </div>
						
					   </div>
				        
                       </div>
 
<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblEmpNo" runat="server" class="col-sm-2 control-label" 
                                                             Text="EmployeeNo"></asp:Label>
  
   <div class="col-sm-2">
      <asp:DropDownList ID="ddlempno" class="form-control" runat="server" 
      AutoPostBack="true" onselectedindexchanged="ddlempno_SelectedIndexChanged">
      </asp:DropDownList>  
     </div>
    <div class="col-md-1">    
    </div>
<asp:Label ID="lblExisting" runat="server" class="col-sm-2 control-label" 
                                                             Text="Existing No"></asp:Label>                       
  
    <div class="col-sm-2">
        <asp:TextBox ID="txtexist" class="form-control" runat="server" ontextchanged="txtexist_TextChanged"></asp:TextBox>
	</div>
						
     <div class="col-sm-3">
          <asp:Button ID="btnsearch" class="btn btn-info"  runat="server" Text="Search" onclick="btnsearch_Click"/>
     </div>
	</div>
		        
 </div> 

<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblname" runat="server" class="col-sm-2 control-label" 
                Text="EmployeeName"></asp:Label>                       
   <div class="col-sm-3">
      <asp:DropDownList ID="ddlEmpname" class="form-control" runat="server" 
      AutoPostBack="true" onselectedindexchanged="ddlEmpname_SelectedIndexChanged">
      </asp:DropDownList>  
     </div>
  
 <asp:Label ID="lbldoj" runat="server" class="col-sm-2 control-label" 
                Text="Date of Join"></asp:Label>                       
  
    <div class="col-sm-3">
        <asp:Label ID="lbldoj1" runat="server" Text=""></asp:Label>
    </div>
		
   
	</div>
		        
 </div> 

<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblDesignation" runat="server" class="col-sm-2 control-label" 
                Text="Designation"></asp:Label> 
 
   <div class="col-sm-3">
      <asp:Label ID="lblDesignation1" runat="server" Text=""></asp:Label>
     </div>
  
 <asp:Label ID="lbldepartment" runat="server" class="col-sm-2 control-label" 
                Text="Department"></asp:Label>                       
   <div class="col-sm-3">
        <asp:Label ID="lbldepartment1" runat="server" Text=""></asp:Label>
     </div>
		
   
	</div>
		        
 </div>
 
<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblProfile" runat="server" class="col-sm-2 control-label" 
                Text="Profile Type"></asp:Label>

   <div class="col-sm-3">
      <asp:Label ID="lblprofile1" runat="server" Text=""></asp:Label>
     </div>
  
   <asp:Label ID="lblwages" runat="server" class="col-sm-2 control-label" 
                Text="Wages Type"></asp:Label>                    
 
    <div class="col-sm-3">
        <asp:Label ID="lblwages1" runat="server" Text=""></asp:Label>
     </div>
		
   
	</div>
		        
 </div>

<div class="col-md-12">     
 <div class="form-group row">
  <asp:Label ID="lblBasic" runat="server" class="col-sm-2 control-label" 
                Text="Basic Salary"></asp:Label> 
 
   <div class="col-sm-3">
      <asp:Label ID="lblBasic11" runat="server" Text=""></asp:Label>
     </div>
  
   <asp:Label ID="lblDate" runat="server" class="col-sm-2 control-label" 
                Text="Date"></asp:Label>                    
   
    <div class="col-sm-3">
        <asp:TextBox ID="txtDate" class="form-control" runat="server"></asp:TextBox>
      <cc1:CalendarExtender ID="Caleder_txtdob" runat="server" TargetControlID="txtDate"
        Format="dd-MM-yyyy" CssClass="orange">
        </cc1:CalendarExtender>
        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server"
          FilterMode="ValidChars" FilterType="Custom,Numbers"
          TargetControlID="txtDate" ValidChars="0123456789/-">
        </cc1:FilteredTextBoxExtender>
     </div>
		
   
	</div>
		        
 </div>
 
<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblAdvance" runat="server" class="col-sm-2 control-label" 
                Text="Advance Amount"></asp:Label>

   <div class="col-sm-3">
       <asp:TextBox ID="txtadvance" class="form-control" runat="server" ontextchanged="txtadvance_TextChanged"></asp:TextBox>
     </div>
  
 <asp:Label ID="lblBalane" runat="server" class="col-sm-2 control-label" 
                Text="Balance Amount"></asp:Label>                      
 
    <div class="col-sm-3">
        <asp:TextBox ID="txtBalance" class="form-control" runat="server" Enabled="false"></asp:TextBox>
      
     </div>
		
   
	</div>
		        
 </div>
 
<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="NoMonths" runat="server" class="col-sm-2 control-label" 
                Text="No of Months"></asp:Label>
 
   <div class="col-sm-3">
       <asp:TextBox ID="txtmonths" class="form-control" runat="server" ontextchanged="txtmonths_TextChanged"></asp:TextBox>
     </div>
  <div class="col-sm-3">
   <asp:Button ID="btncal" class="btn btn-success" runat="server" onclick="btncal_Click" Text="Calculate" />
  </div>
  <asp:Label ID="lblmontly" runat="server" class="col-sm-2 control-label" 
                Text="Monthly Deduction"></asp:Label>  
 
  <div class="col-sm-2">
     <asp:TextBox ID="txtmonthly" class="form-control" runat="server" Enabled="false"></asp:TextBox>
  </div>
		
   
	</div>
		        
 </div>
 
 
 <div class="col-md-12">     
				<div class="form-group row">
					
			
				 <div class="col-sm-3">  
				     <asp:Button ID="btnSave" class="btn btn-success" runat="server" Text="Save" onclick="btnSave_Click"/>    
				 </div>
		
				 <div class="col-sm-3">
                     <asp:Button ID="btnClear" class="btn btn-success" runat="server" Text="Clear" onclick="btnClear_Click"/>
  				 </div>
  				
  				
  				 				
			    </div>
				       
        </div>
 
</div>
</form>

</div>
</div>


    <!-- Dashboard start -->
 	
 <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    
 <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                            
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                          
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
  
    <!-- Dashboard End --> 


</div>
</div>
</div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

