﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SalaryMaster.aspx.cs" Inherits="SalaryMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Salary Master</li></h4> 
                    </ol>
             </div>

<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Salary Master</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body"> 

                    <div class="col-md-12">     
				        <div class="form-group row">
				           <asp:Label ID="lblCategory" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
					      <div class="col-sm-2">
					      <asp:DropDownList ID="ddlcategory" class="form-control" runat="server" 
					      AutoPostBack="True" onselectedindexchanged="ddlcategory_SelectedIndexChanged">
                            </asp:DropDownList>  
					      </div>
					    
					
						<div class="col-md-1">
                          
                        </div>
                        <asp:Label ID="lblDept" runat="server" class="col-sm-2 control-label" 
                                                             Text="Department"></asp:Label> 
					 
					   <div class="col-sm-2">
						<asp:DropDownList ID="ddlDepartment" class="form-control" runat="server" 
						onselectedindexchanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList> 
					  </div>
						
					  <div class="col-sm-3">
                         <asp:Button ID="btnclick" class="btn btn-info"  runat="server" Text="Click" onclick="btnclick_Click"/>
                      </div>
						</div>
				        
                       </div>
                     
                                     
                      <div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="lblFinance" runat="server" class="col-sm-2 control-label" 
                                                             Text="Financial Year"></asp:Label> 
				          <div class="col-sm-2">
					      <asp:DropDownList ID="ddFinance" class="form-control" runat="server" 
					      AutoPostBack="true" onselectedindexchanged="ddFinance_SelectedIndexChanged" >
                            </asp:DropDownList>  
					      </div>
					    
					
						<div class="col-md-1"></div>
                        <asp:Label ID="Label1" runat="server" class="col-sm-2 control-label" 
                                                             Text="Token No"></asp:Label>
                       
					  <div class="col-sm-2">
                           <asp:TextBox ID="txtTokenNo" class="form-control" runat="server"></asp:TextBox>    
					  </div>
						
					  <div class="col-sm-2">
					   <asp:CheckBox ID="ChkAllDept" runat="server" Text="AllDept" 
					   oncheckedchanged="ChkAllDept_CheckedChanged" AutoPostBack="true" />  
					  </div>
						</div>
				        
                       </div>
                       
                       <div class="col-md-12">     
				        <div class="form-group row">
				         
					      <div class="col-sm-2">
					    
					      </div>
					    
					
						<div class="col-md-1"> </div>
                        
					
					   <div class="col-sm-2">
                           <asp:Button ID="btnTokenNoSearch" class="btn btn-info"  runat="server" Text="Search" 
                           onclick="btnTokenNoSearch_Click"/>
					  </div>
						
					  <div class="col-sm-3"> </div>
						</div>
				        
                     </div>
                       
                       <div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="lblexisitingno" runat="server" class="col-sm-2 control-label" 
                                                             Text="Existing Number"></asp:Label>
                        <asp:TextBox ID="txtexistingNo" runat="server" Visible="false" ></asp:TextBox>
				         <div class="col-sm-3">
                              <asp:DropDownList ID="ddToken" class="form-control" runat="server"
                               AutoPostBack="true" onselectedindexchanged="ddToken_SelectedIndexChanged">
                              </asp:DropDownList>
					      </div>
					    	
						                      
					 
				
						</div>
				        
                       </div>
                       
                       
                       
                        <div class="col-md-12">     
				        <div class="form-group row">
				            <label for="input-Default" class="col-sm-2 control-label">Employee No</label>
					      <div class="col-sm-3">
                              <asp:DropDownList ID="ddlempNo" class="form-control" runat="server" AutoPostBack="true"
                               onselectedindexchanged="ddlempNo_SelectedIndexChanged">
                              </asp:DropDownList>
					      </div>
					    	
						    <div class="col-sm-1"></div>
				            <label for="input-Default" class="col-sm-2 control-label">Employee Name</label>
						    
						      <div class="col-sm-3">                    
						       <asp:DropDownList ID="ddlEmpName" class="form-control" runat="server" AutoPostBack="true"
						        onselectedindexchanged="ddlEmpName_SelectedIndexChanged">
                              </asp:DropDownList>
					 </div>
				
						</div>
				        
                       </div>
                       
                       
                        <div class="col-md-12"> 
                       <div class="form-group row">
					   <label for="input-Default" class="col-sm-2 control-label">SalaryType</label>
						<div class="col-sm-4">
                             <asp:RadioButtonList ID="rbsalary" runat="server" RepeatColumns="3" Enabled="false">
                             <asp:ListItem Text="Weekly Wages" Value="1"></asp:ListItem>
                             <asp:ListItem Text="Monthly Wages" Value="2"></asp:ListItem> 
	                         <asp:ListItem Text="Bi-Monthly Wages" Value="3"></asp:ListItem>                                            
	                         </asp:RadioButtonList>                               
                        </div>
			       </div>
			       </div>
			       <asp:Panel ID="PanelStaff" runat="server" Visible="false" >
			       <div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="lblStaffBasic" runat="server" class="col-sm-2 control-label" 
                                                             Text="Basic Salary"></asp:Label>
				         <div class="col-sm-4">
                           <asp:TextBox ID="txtSBasic" class="form-control" runat="server">
                                  </asp:TextBox>    
                        </div>
								
						</div>
				        
                       </div>
           
           <div class="col-md-12">     
				        <div class="form-group row">
				        <asp:Label ID="lblSall1" runat="server" class="col-sm-2 control-label" 
                                                             Text="Allowance 1"></asp:Label>
				           
					      <div class="col-sm-4">
                           <asp:TextBox ID="txtSall1" class="form-control" runat="server">
                                  </asp:TextBox>    
                           </div>
                          <asp:Label ID="lblSAded1" runat="server" class="col-sm-2 control-label" 
                                                             Text="Deduction 1"></asp:Label>  
                          <div class="col-sm-4">
                           <asp:TextBox ID="txtSded1" class="form-control" runat="server">
                             </asp:TextBox>    
                           </div>
								
						</div>
				        
                       </div>
            
            <div class="col-md-12">     
				        <div class="form-group row">
				         <asp:Label ID="lblSAll2" runat="server" class="col-sm-2 control-label" 
                                                             Text="Allowance 2"></asp:Label>
				          
					      <div class="col-sm-4">
                           <asp:TextBox ID="txtSall2" class="form-control" runat="server">
                                  </asp:TextBox>    
                           </div>
                      <asp:Label ID="lblSded2" runat="server" class="col-sm-2 control-label" 
                                                             Text="Deduction 2"></asp:Label>     
                        <div class="col-sm-4">
                           <asp:TextBox ID="txtSded2" class="form-control" runat="server">
                             </asp:TextBox>    
                           </div>
								
						</div>
				        
                       </div>
             
             <div class="col-md-12">     
				        <div class="form-group row">
				            <label for="input-Default" class="col-sm-2 control-label">PF Amount</label>
					      <div class="col-sm-4">
                           <asp:TextBox ID="txtPF" class="form-control" runat="server">
                                  </asp:TextBox>    
                           </div>
								
						</div>
				        
                       </div>
            </asp:Panel>  
              <div class="col-md-12">     
				 <div class="form-group row">
					
				 <div class="col-sm-2"></div>
				 <div class="col-sm-2">
                     <asp:Button ID="btnSave" class="btn btn-success" runat="server" Text="Save" onclick="btnSave_Click" />                             
                 </div>
                 <div class="col-sm-1"></div>
				 <div class="col-sm-2">
                     <asp:Button ID="btnreset" class="btn btn-success" runat="server" Text="Reset" onclick="btnreset_Click" />
  				 </div>
  				<div class="col-sm-1"></div>
  				 <div class="col-sm-2">
                     <asp:Button ID="btncancel" class="btn btn-danger" onclick="btncancel_Click" runat="server" Text="Clear" />
  				 </div>
  				 
  				
			     </div>
               </div>
              
              
         
                       
                      </div>
                      </form>
                      
                       </div>
                      </div>
                      
                      <!-- Dashboard start -->
 <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
 <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>  
                        <!-- Dashboard End --> 
                      
                     
                      </div>
                      </div>
                      </div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

