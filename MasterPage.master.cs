﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using System.Security.Cryptography;
using System.IO;
using System.Text;

public partial class MasterPage : System.Web.UI.MasterPage
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Data;
    string SSQL;
    string SessionUserType;
    BALDataAccess objdata = new BALDataAccess();
    string ssql;
    DataTable dtd = new DataTable();
    string DashBoards;
    string masterpages;
    string UserCreations;
    string AdministrationRight;
    string EmployeeTypes;
    string PFandESImasters;
    string BankDetailss;
    string DepartmentDetails;
    string IncentiveMasters;
    string BasicDetailss;
    string OTPaymentDetails;
    string Employees;
    string EmployeeRegistrations;
    string Profiless;
    string SalaryMasters;
    string SalaryDetailss;
    string Advances;
    string Settlements;
    string EmployeeReactives;
    string Reportss;
    string BonusProcess;
    string Uploads;
    string AttendanceUploads;
    string EmployeeUploads;
    string OTUploads;
    string NewWagesUploads;
    string SalaryMasterUploads;
    string SchemeUploads;
    string ProfileUploads;
    string ESIandPfs;
    string PFDownloads;
    //string SessionCompanyName;

    string SessionUserID;




    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionCompanyName = Session["CompanyName"].ToString();

        if (!IsPostBack)
        {
            if (SessionUserID.ToUpper().ToString() == "scoto".ToUpper().ToString())
            {
                //Skip
                btnPayroll_Link.Visible = true;
            }
            else
            {
                //Payroll_ModuleLink_Check();
                if (SessionUserType == "1")
                {
                    Admin_User_Rights_Check();
                }
                else
                {
                    //NonAdmin_User_Rights_Check();
                    Admin_User_Rights_Check();
                }
            }
        }

        lblCompanyName.Text = SessionCompanyName;
        lblUserNameDisp.Text = SessionCcode + "-" + SessionLcode;
        lbladmin.Text = ss.ToString();


        //AdministrationRights();

     }
    private void Admin_User_Rights_Check()
    {
        ALL_Menu_Header_Disable();
        ALL_Menu_Header_Forms_Disable();
        string ModuleID = Encrypt("2").ToString();
        string MenuID = "";
        DataTable DT_Head = new DataTable();
        DataTable DT_Form = new DataTable();
        //Check with Header Menu
        string query = "Select Distinct Menu_LI_ID,MenuID from [HR_Rights]..Company_Module_MenuHead_Rights where ModuleID='" + ModuleID + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT_Head = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;

            MenuID = DT_Head.Rows[i]["MenuID"].ToString();
            query = "Select Distinct Form_LI_ID from [HR_Rights]..Company_Module_Menu_Form_Rights where ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT_Form = objdata.RptEmployeeMultipleDetails(query);
            for (int J = 0; J < DT_Form.Rows.Count; J++)
            {
                this.FindControl(DT_Form.Rows[J]["Form_LI_ID"].ToString()).Visible = true;
            }
        }

    }

    private void NonAdmin_User_Rights_Check()
    {
        ALL_Menu_Header_Disable();
        ALL_Menu_Header_Forms_Disable();
        string ModuleID = Encrypt("2").ToString();
        string MenuID = "";
        DataTable DT_Head = new DataTable();
        DataTable DT_Form = new DataTable();
        //Check with Header Menu
        string query = "Select Distinct Menu_LI_ID,MenuID from [HR_Rights]..Company_Module_User_Rights where ModuleID='" + ModuleID + "' And UserName='" + SessionAdmin + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT_Head = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;

            MenuID = DT_Head.Rows[i]["MenuID"].ToString();
            query = "Select Distinct Form_LI_ID from [HR_Rights]..Company_Module_User_Rights where ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "' And UserName='" + SessionAdmin + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT_Form = objdata.RptEmployeeMultipleDetails(query);
            for (int J = 0; J < DT_Form.Rows.Count; J++)
            {
                this.FindControl(DT_Form.Rows[J]["Form_LI_ID"].ToString()).Visible = true;
            }
        }

    }

    private void ALL_Menu_Header_Disable()
    {
        this.FindControl("Dashboard").Visible = false;
        this.FindControl("masterpage").Visible = false;
        this.FindControl("Employee").Visible = false;
        this.FindControl("Reports").Visible = false;
        this.FindControl("BonusProces").Visible = false;
        this.FindControl("Upload").Visible = false;
        this.FindControl("ESIandPFupload").Visible = false;
    }
    private void ALL_Menu_Header_Forms_Disable()
    {
        this.FindControl("Dashboard").Visible = false;
        //Master
        this.FindControl("UserCreation").Visible = false;
        this.FindControl("AdminRights").Visible = false;
        //this.FindControl("UserRights").Visible = false;
        //this.FindControl("Company_Module_Rights").Visible = false;
        //this.FindControl("Company_Module_MenuHead_Rights").Visible = false;
        //this.FindControl("Module_Form_Rights").Visible = false;
        this.FindControl("EmployeeType").Visible = false;
        this.FindControl("PFandESImaster").Visible = false;
        this.FindControl("BankDetails").Visible = false;
        this.FindControl("Department").Visible = false;
        this.FindControl("IncentiveMaster").Visible = false;
        this.FindControl("BasicDetails").Visible = false;
        this.FindControl("OvertimePayment").Visible = false;
        //Employee
        this.FindControl("EmployeeRegistration").Visible = false;
        this.FindControl("Profiles").Visible = false;
        this.FindControl("SalaryMaster").Visible = false;
        this.FindControl("SalaryDetails").Visible = false;
        this.FindControl("Advance").Visible = false;
        this.FindControl("Settelment").Visible = false;
        this.FindControl("EmployeeReactive").Visible = false;
        //Reports
        this.FindControl("MnuRptEmpDet").Visible = false;
        this.FindControl("MnuRptResgination").Visible = false;
        this.FindControl("MnuRptPayslip").Visible = false;
        this.FindControl("MnuRptOTReport").Visible = false;
        this.FindControl("MnuRptSalaryHistory").Visible = false;
        this.FindControl("MnuRptELCL").Visible = false;
        this.FindControl("MnuRptAttnDet").Visible = false;
        this.FindControl("MnuRptPFESIDet").Visible = false;
        //BonusProces
        this.FindControl("BonusProces").Visible = false;
        //Upload
        this.FindControl("AttendanceUpload").Visible = false;
        this.FindControl("EmployeeUpload").Visible = false;
        this.FindControl("OTUpload").Visible = false;
        this.FindControl("NewWagesUpload").Visible = false;
        this.FindControl("SalaryMasterUpload").Visible = false;
        this.FindControl("SchemeUpload").Visible = false;
        this.FindControl("ProfileUpload").Visible = false;
        //ESIandPFupload
        this.FindControl("PFDownload").Visible = false;
    }

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    private string Decrypt(string cipherText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }


    public void AdministrationRights()
    {

        ssql = "select *from Rights where CompCode='" + SessionCcode.ToString() + "' and LocCode= '" + SessionLcode.ToString() + "' and Username='" + lbladmin.Text + "'";
        dtd = objdata.ReturnMultipleValue(ssql);
        if (dtd.Rows.Count > 0)
        {
            DashBoards = dtd.Rows[0]["DashBoard"].ToString();
            if (DashBoards == "1")
            {
                this.FindControl("Dashboard").Visible = true;
            }
            else
            {
                this.FindControl("Dashboard").Visible = false;
            }


            //if (masterpageM != "1" && UserCreationM != "1" && AdministrationRightM != "1" && DepartmentDetailsM != "1"
            //        && DesginationDetailsM != "1" && EmployeeTypeDetailsM != "1" && WagesTypeDetailsM != "1"
            //        && EmployeeDetailsM != "1" && LeaveMasterM != "1" && PermissionMasterM != "1" && GraceTimeM != "1"
            //        && EarlyOutMasterM != "1" && LateINMasterM != "1")






            masterpages = dtd.Rows[0]["masterpage"].ToString();
            EmployeeTypes = dtd.Rows[0]["EmployeeType"].ToString();
            UserCreations = dtd.Rows[0]["UserCreation"].ToString();
            PFandESImasters = dtd.Rows[0]["PFandESImaster"].ToString();
            BankDetailss = dtd.Rows[0]["BankDetails"].ToString();
            DepartmentDetails = dtd.Rows[0]["Department"].ToString();
            IncentiveMasters = dtd.Rows[0]["IncentiveMaster"].ToString();
            BasicDetailss = dtd.Rows[0]["BasicDetails"].ToString();
            OTPaymentDetails = dtd.Rows[0]["OvertimePayment"].ToString();
            Employees = dtd.Rows[0]["PermissionMaster"].ToString();
            EmployeeRegistrations = dtd.Rows[0]["EmployeeRegistration"].ToString();
            Profiless = dtd.Rows[0]["Profile"].ToString();
            SalaryMasters = dtd.Rows[0]["SalaryMaster"].ToString();
            SalaryDetailss = dtd.Rows[0]["SalaryDetails"].ToString();
            Advances = dtd.Rows[0]["Advance"].ToString();
            Settlements = dtd.Rows[0]["Settelment"].ToString();
            EmployeeReactives = dtd.Rows[0]["EmployeeReactive"].ToString();
            Reportss = dtd.Rows[0]["Reports"].ToString();
            BonusProcess = dtd.Rows[0]["BonusProcess"].ToString();
            Uploads = dtd.Rows[0]["Upload"].ToString();
            AttendanceUploads = dtd.Rows[0]["AttendanceUpload"].ToString();
            EmployeeUploads = dtd.Rows[0]["EmployeeUpload"].ToString();
            OTUploads = dtd.Rows[0]["OTUpload"].ToString();
            NewWagesUploads = dtd.Rows[0]["NewWagesUpload"].ToString();
            SalaryMasterUploads = dtd.Rows[0]["SalaryMasterUpload"].ToString();
            SchemeUploads = dtd.Rows[0]["SchemeUpload"].ToString();
            ProfileUploads = dtd.Rows[0]["ProfileUpload"].ToString();
            ESIandPfs = dtd.Rows[0]["ESIandPFupload"].ToString();
            PFDownloads = dtd.Rows[0]["PFDownload"].ToString();
            AdministrationRight = dtd.Rows[0]["AdminRights"].ToString();


            if (masterpages != "1" && UserCreations != "1" && AdministrationRight != "1" && EmployeeTypes != "1" && PFandESImasters != "1"
                && BankDetailss != "1" && DepartmentDetails != "1" && IncentiveMasters != "1" && BasicDetailss != "1" && OTPaymentDetails != "1")
            {
                this.FindControl("masterpage").Visible = false;
            }
            else
            {
                this.FindControl("masterpage").Visible = true;
            }

            

          
            if (EmployeeTypes == "1")
            {
                this.FindControl("EmployeeType").Visible = true;
            }
            else
            {
                this.FindControl("EmployeeType").Visible = false;
            }



          
            if (UserCreations == "1")
            {
                this.FindControl("UserCreation").Visible = true;
            }
            else
            {
                this.FindControl("UserCreation").Visible = false;
            }


           
            if (PFandESImasters == "1")
            {
                this.FindControl("PFandESImaster").Visible = true;
            }
            else
            {
                this.FindControl("PFandESImaster").Visible = false;
            }


           
            if (BankDetailss == "1")
            {
                this.FindControl("BankDetails").Visible = true;
            }
            else
            {
                this.FindControl("BankDetails").Visible = false;
            }


           
            if (DepartmentDetails == "1")
            {
                this.FindControl("Department").Visible = true;
            }
            else
            {
                this.FindControl("Department").Visible = false;
            }



            
            if (IncentiveMasters == "1")
            {
                this.FindControl("IncentiveMaster").Visible = true;
            }
            else
            {
                this.FindControl("IncentiveMaster").Visible = false;
            }

            
            if (BasicDetailss == "1")
            {
                this.FindControl("BasicDetails").Visible = true;
            }
            else
            {
                this.FindControl("BasicDetails").Visible = false;
            }

            
            if (OTPaymentDetails == "1")
            {
                this.FindControl("OvertimePayment").Visible = true;
            }
            else
            {
                this.FindControl("OvertimePayment").Visible = false;
            }


            
            if (Employees != "1" && EmployeeRegistrations != "" && Profiless != "" && SalaryMasters != "" && SalaryDetailss != ""
                && Advances != "1" && Settlements != "1" && EmployeeReactives != "1")
            {
                this.FindControl("Employee").Visible = false;
            }
            else
            {
                this.FindControl("Employee").Visible = true;
            }


            
            if (EmployeeRegistrations == "1")
            {
                this.FindControl("EmployeeRegistration").Visible = true;
            }
            else
            {
                this.FindControl("EmployeeRegistration").Visible = false;
            }

           
            if (Profiless == "1")
            {
                this.FindControl("Profiles").Visible = true;
            }
            else
            {
                this.FindControl("Profiles").Visible = false;
            }


           
            if (SalaryMasters == "1")
            {
                this.FindControl("SalaryMaster").Visible = true;
            }
            else
            {
                this.FindControl("SalaryMaster").Visible = false;
            }


          
            if (SalaryDetailss == "1")
            {
                this.FindControl("SalaryDetails").Visible = true;
            }
            else
            {
                this.FindControl("SalaryDetails").Visible = false;
            }

           
            if (Advances == "1")
            {
                this.FindControl("Advance").Visible = true;
            }
            else
            {
                this.FindControl("Advance").Visible = false;
            }


          
            if (Settlements == "1")
            {
                this.FindControl("Settelment").Visible = true;
            }
            else
            {
                this.FindControl("Settelment").Visible = false;
            }

            
            if (EmployeeReactives == "1")
            {
                this.FindControl("EmployeeReactive").Visible = true;
            }
            else
            {
                this.FindControl("EmployeeReactive").Visible = false;
            }


            if (Reportss == "1")
            {
                this.FindControl("Reports").Visible = true;
            }
            else
            {
                this.FindControl("Reports").Visible = false;
            }

          
            if (BonusProcess == "1")
            {
                this.FindControl("BonusProces").Visible = true;
            }
            else
            {
                this.FindControl("BonusProces").Visible = false;
            }

           
            if (Uploads != "1" && AttendanceUploads != "1" && EmployeeUploads != "1" && OTUploads != "1" && NewWagesUploads != "1" && SalaryMasterUploads != "1"
                && SchemeUploads != "1" && ProfileUploads != "1")
            {
                this.FindControl("Upload").Visible = false;
            }
            else
            {
                this.FindControl("Upload").Visible = true;
            }

           
            if (AttendanceUploads == "1")
            {
                this.FindControl("AttendanceUpload").Visible = true;
            }
            else
            {
                this.FindControl("AttendanceUpload").Visible = false;
            }

           
            if (EmployeeUploads == "1")
            {
                this.FindControl("EmployeeUpload").Visible = true;
            }
            else
            {
                this.FindControl("EmployeeUpload").Visible = false;
            }

            
            if (OTUploads == "1")
            {
                this.FindControl("OTUpload").Visible = true;
            }
            else
            {
                this.FindControl("OTUpload").Visible = false;
            }
           
            if (NewWagesUploads == "1")
            {
                this.FindControl("NewWagesUpload").Visible = true;
            }
            else
            {
                this.FindControl("NewWagesUpload").Visible = false;
            }
           
            if (SalaryMasterUploads == "1")
            {
                this.FindControl("SalaryMasterUpload").Visible = true;
            }
            else
            {
                this.FindControl("SalaryMasterUpload").Visible = false;
            }
           
            if (SchemeUploads == "1")
            {
                this.FindControl("SchemeUpload").Visible = true;
            }
            else
            {
                this.FindControl("SchemeUpload").Visible = false;
            }
            
            if (ProfileUploads == "1")
            {
                this.FindControl("ProfileUpload").Visible = true;
            }
            else
            {
                this.FindControl("ProfileUpload").Visible = false;
            }
           
            if (ESIandPfs != "1" && PFDownloads != "1")
            {
                this.FindControl("ESIandPFupload").Visible = false;
            }
            else
            {
                this.FindControl("ESIandPFupload").Visible = true;
            }
            
            if (PFDownloads == "1")
            {
                this.FindControl("PFDownload").Visible = true;
            }
            else
            {
                this.FindControl("PFDownload").Visible = false;
            }

           
            if (AdministrationRight == "1")
            {
                this.FindControl("AdminRights").Visible = true;
            }
            else
            {
                this.FindControl("AdminRights").Visible = false;
            }


        }


        







    }

    protected void btnPayroll_Link_Click(object sender, ImageClickEventArgs e)
    {
        //Get Attndance Link
        DataTable DT_Link = new DataTable();
        string query = "Select * from [HR_Rights]..Module_List where ModuleID='1'";
        DT_Link = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Link.Rows.Count != 0)
        {
            string Link_Open = DT_Link.Rows[0]["ModuleLink"].ToString();
            query = "Delete from [HR_Rights]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);
            //Get user Password
            query = "Select * from [HR_Rights]..MstUsers where UserCode='" + SessionUserID + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
            DT_Link = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Link.Rows.Count != 0)
            {
                string Password_Str = DT_Link.Rows[0]["Password"].ToString();
                //Insert User Details
                query = "Insert Into [HR_Rights]..Module_Open_User(CompCode,LocCode,UserName,Password) Values";
                query = query + " ('SSP','" + SessionLcode + "','" + SessionUserID + "','" + Password_Str + "')";
                objdata.RptEmployeeMultipleDetails(query);
                Response.Redirect(Link_Open);
            }

        }
    }



    
}
