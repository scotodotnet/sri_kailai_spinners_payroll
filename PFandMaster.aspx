﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PFandMaster.aspx.cs" Inherits="PFandMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


 <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tablePFandESI').dataTable();               
            }
        }); 
    };
    </script>
      
             
     <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
        $('#tablePFandESI').dataTable();
        });
    </script>






    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate>

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">PF & ESI Master</li>
                   </ol>
              </div>
 
 
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">PF & ESI Master</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
					
					    <div class="form-group row">
					    
					       <label for="input-Default" class="col-sm-2 control-label" for="input-Default">PF%</label>
						<div class="col-sm-4">
                            <asp:TextBox ID="txtpf" class="form-control" runat="server" required></asp:TextBox>
                        </div>
					    
					   
						<label for="input-Default" class="col-sm-2 control-label" for="input-Default">ESI%</label>
						<div class="col-sm-4">
						 <asp:TextBox ID="txtESI" class="form-control" runat="server" required></asp:TextBox>
                         
						</div>
				        </div>  
				       
				        <div class="form-group row">
					    
					       <label for="input-Default" class="col-sm-2 control-label" for="input-Default">PF Salary</label>
						<div class="col-sm-4">
                            <asp:TextBox ID="txtstaff" class="form-control" runat="server" required></asp:TextBox>
                        </div>
					    
					   	
						<label for="input-Default" class="col-sm-2 control-label" for="input-Default">Emp PF% A/c 1 </label>
						<div class="col-sm-4">
						 <asp:TextBox ID="txtEmployeerPF1" class="form-control" runat="server" required></asp:TextBox>
                         
						</div>
				        </div>
				        
				        <div class="form-group row">
					    
					     <label for="input-Default" class="col-sm-2 control-label" for="input-Default">Bonus </label>
						<div class="col-sm-4">
                            <asp:TextBox ID="txtEmployeerPF2" class="form-control" runat="server" required></asp:TextBox>

                        </div>
					    
					  
						<label for="input-Default" class="col-sm-2 control-label" for="input-Default">Employee ESI%</label>
						<div class="col-sm-4">
						 <asp:TextBox ID="txtEmployeerESI" class="form-control"  runat="server" required></asp:TextBox>
                         
						</div>
				        </div>
				       
						<!-- Button start -->
						<div class="form-group row">
						</div>
                        <div class="txtcenter">
                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" onclick="btnSave_Click"/>
                                     
                                                       
                   <asp:LinkButton ID="btnclr" class="btn btn-danger" runat="server" onclick="btnclr_Click">Cancel</asp:LinkButton>      
                    </div>
                    <!-- Button End -->	
						
						
							
						<div class="form-group row">
					      <asp:Label ID="Label1" for="input-Default" class="col-sm-10 control-label" 
                                Text="ESI Company Code" runat="server" Font-Bold="True" Font-Size="X-Large"></asp:Label>
				       </div> 
                           
                        <div class="form-group row">
					      <label for="input-Default" class="col-sm-2 control-label">ESI Code</label>
						 <div class="col-sm-4">
						  <asp:TextBox ID="txtESI_mst" class="form-control" runat="server" required></asp:TextBox>
                         
						</div>
					       
					     	
					    
						<div class="col-sm-4">
                              <asp:Button ID="btnSave1" class="btn btn-success" onclick="btnSave1_Click" runat="server" Text="Save"/>
                        </div>
					    
					 
						
				        </div>
				        
				        
				        <asp:Panel ID="pnlvisible" runat="server" Visible="false" >
                                                              <div class="form-group row">
                                                                    <asp:Label ID="Label2" runat="server" Text="Day Incentive Amount" Font-Bold="true" Visible="false" ></asp:Label>
                                                                  <div class="col-sm-4">
                                                                    <asp:TextBox ID="txtDayIncentive" runat="server" TabIndex="3" MaxLength="10" Visible="false" Text="0.00" ></asp:TextBox>
                                                                   
                                                                  </div>
                                                            
                                                               
                                                                    <asp:Label ID="Label3" runat="server" Text="O/N Shift Amount" Font-Bold="true" Visible="false" ></asp:Label>
                                                                <div class="col-sm-4">
                                                                    <asp:TextBox ID="txtHalf" runat="server" TabIndex="3" MaxLength="10" Visible="false"  Text="0.00" ></asp:TextBox>
                                                                   
                                                                 </div>
                                                             </div>
                                                             
                                                             
                                                              <div class="form-group row">
                                                                    <asp:Label ID="Label4" runat="server" Text="F/N Shift Amount" Font-Bold="true" Visible="false" ></asp:Label>
                                                                 <div class="col-sm-4">
                                                                    <asp:TextBox ID="txtFull" runat="server" TabIndex="3" MaxLength="10" Visible="false"  Text="0.00" ></asp:TextBox>
                                                                    
                                                               </div>
                                                               
                                                                    <asp:Label ID="Label6" runat="server" Text="3 Sided Amount" Font-Bold="true" Visible="false" ></asp:Label>
                                                                <div class="col-sm-4">
                                                                    <asp:TextBox ID="txtThree" runat="server" TabIndex="3" MaxLength="10" Visible="false"  Text="0.00" ></asp:TextBox>
                                                                   
                                                              </div>
                                                             </div>
                                                             
                                                              <div class="form-group row">
                                                                    <asp:Label ID="Label5" runat="server" Text="Spinning Departmnt Amount" Font-Bold="true" Visible="false" ></asp:Label>
                                                                 <div class="col-sm-4">
                                                                    <asp:TextBox ID="txtSpinning" runat="server" TabIndex="3" MaxLength="10" Visible="false"  Text="0.00" ></asp:TextBox>
                                                                    
                                                                </div>
                                                            
                                                           
                                                                    <asp:Label ID="lblstamp" runat="server" Text="Stamp Charge" Font-Bold="true" ></asp:Label>
                                                                <div class="col-sm-4">
                                                                    <asp:TextBox ID="txtstamp" runat="server" TabIndex="4" MaxLength="10" Text="0" ></asp:TextBox>
                                                                   
                                                                </div>
                                                             </div>
                                                             
                                                             
                                                              <div class="form-group row">
                                                                    <asp:Label ID="lblVDAPoint" runat="server" Text="VDA Point 1" Font-Bold="true"></asp:Label>
                                                                <div class="col-sm-4">
                                                                    <asp:TextBox ID="txtVDAPoint1" runat="server" Text="0" ></asp:TextBox>
                                                                    
                                                                  </div>
                                                           </div>
                                                           
                                                           
                                                              <asp:Panel ID="paneldiasable" runat="server" Visible="false" >
                                                            <div class="form-group row">
                                                                    <asp:Label ID="Label12" runat="server" Text="VDA Point 2" Font-Bold="true"></asp:Label>
                                                                 <div class="col-sm-4">
                                                                    <asp:TextBox ID="txtVDAPoint2" runat="server" Text="0" ></asp:TextBox>
                                                               </div>
                                                           </div>
                                                            </asp:Panel>
                                                            
                                    </asp:Panel>
                                    
                                    
                        <tr visible="false">
                                                                <td colspan="2">
                                                                    <asp:CheckBox ID="chkUnion" runat="server" Text="Add Union Charges" 
                                                                        Font-Bold="true" oncheckedchanged="chkUnion_CheckedChanged"  Visible="false" />
                                                                </td>
                                                            </tr>
				        
				        
				        
				        <div class="form-group row">
						</div>
							<!-- Table start -->
				        
				            <div class="table-responsive">
    
                         <asp:Repeater ID="rptrPFandMaster" runat="server" onitemcommand="Repeater1_ItemCommand">
                         <HeaderTemplate>
                
                         <table id="tablePFandESI" class="display table" style="width: 100%;">
                         <thead>
                         <tr>
                            <th>ESI COMPANY CODE</th>
                        </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                         <tr>
                           <td>
                              <%# DataBinder.Eval(Container.DataItem, "ESINo")%>
                           </td>
                         </tr>
                         </ItemTemplate>
                        <FooterTemplate>
              
                       </table>
                       </FooterTemplate>
                       </asp:Repeater>
                    
                            </div>
				               
          
					         <!-- Table End -->
						<div class="form-group row">
						</div>
                           
                        
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 <!-- Dashboard start -->
 <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                            
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
 
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->

 </ContentTemplate>
 </asp:UpdatePanel>

</asp:Content>

