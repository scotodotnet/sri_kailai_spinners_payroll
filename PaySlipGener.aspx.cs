﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;


public partial class PaySlipGener : System.Web.UI.Page
{

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string NetBase;
    string NetFDA;
    string NetVDA;
    string NetHRA;
    string Nettotal;
    string NetPFEarnings;
    string NetPF;
    string NetESI;
    string NetUnion;
    string NetAdvance;
    string NetAll1;
    string NetAll2;
    string NetAll3;
    string NetAll4;
    string NetAll5;
    string NetDed1;
    string NetDed2;
    string NetDed3;
    string NetDed4;
    string NetDed5;
    string HomeDays;
    string Halfnight;
    string FullNight;
    string DayIncentive;
    string Spinning;
    string ThreeSided;
    string NetLOP;
    string NetStamp;
    string NetTotalDeduction;
    string NetOT;
    string NetAmt;
    string Network;
    string totNFh;
    string totweekoff;
    string totCL;
    string totwork;
    string Roundoff;
    DateTime MyDate;
    DateTime MyDate1;
    DateTime MyDate2;
    static decimal AdvAmt;
    static string ID;
    static string Adv_id = "";
    static string Dec_mont = "0";
    static string Adv_BalanceAmt = "0";
    static string Adv_due = "0";
    static string Increment_mont = "0";
    static decimal val;
    static string EmployeeDays = "0";
    string MyMonth;
    static string cl = "0";
    string TempDate;
    static string Fixedsal = "0";
    static string FixedOT = "0";
    static string Tot_OThr = "0";
    static string Emp_ESI_Code = "";
    static string NetPay_Grand_Total = "0";
    static string NetPay_Grand_Total_Words = "";
    static bool isUK = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(constr);
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        //lblComany.Text = SessionCcode + " - " + SessionLcode;
        //lblusername.Text = Session["Usernmdisplay"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            DropDwonCategory();
            Months_load();
            //ESICode_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                //txtLCYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            //Master.Visible = false;
        }
    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.months_load();
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }
    public void EmployeeType()
    {
        //DataTable dtemp = new DataTable();
        //dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        //ddldept.DataSource = dtemp;
        //ddldept.DataTextField = "EmpType";
        //ddldept.DataValueField = "EmpTypeCd";
        //ddldept.DataBind();
    }

    public void ESICode_load()
    {

        //DataTable dt_Empty = new DataTable();
        //ddESI.DataSource = dt_Empty;
        //ddESI.DataBind();
        //DataTable dt = new DataTable();
        //dt = objdata.ESICode_GRID();
        //DataRow dr = dt.NewRow();
        //dr["ESIno"] = "--Select--";
        //dt.Rows.InsertAt(dr, 0);
        //ddESI.DataSource = dt;
        //ddESI.DataValueField = "ESIno";
        //ddESI.DataTextField = "ESIno";
        //ddESI.DataBind();
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        //ddldept.DataSource = dtempty;
        //ddldept.DataBind();

        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        //EmployeeType();
        //Department
        //ddldept.Items.Clear();
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(Stafflabour, SessionCcode, SessionLcode);
        //ddldept.DataSource = dtDip;
        //ddldept.DataTextField = "DepartmentNm";
        //ddldept.DataValueField = "DepartmentCd";
        //ddldept.DataBind();

        EmployeeType_Load();
    }

    public void EmployeeType_Load()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        ddlEmployeeType.DataSource = dtemp;
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
    }

    protected void ddldept_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //try
        //{
        //    bool ErrFlag = false;
        //    string FixedDays = "0";
        //    string CmpName = "";
        //    string Cmpaddress = "";
        //    int YR = 0;
        //    if (ddlMonths.SelectedValue == "January")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else if (ddlMonths.SelectedValue == "February")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else if (ddlMonths.SelectedValue == "March")
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //        YR = YR + 1;
        //    }
        //    else
        //    {
        //        YR = Convert.ToInt32(ddlFinance.SelectedValue);
        //    }

        //    if (ddlcategory.SelectedValue == "0")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
        //        ErrFlag = true;
        //    }
        //    else if ((ddlEmployeeType.SelectedValue == "") || (ddlEmployeeType.SelectedValue == "0"))
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
        //        ErrFlag = true;
        //    }

        //    else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Months');", true);
        //        ErrFlag = true;
        //    }
              
        //        if (!ErrFlag)
        //        {
        //            if (ddlcategory.SelectedValue == "1")
        //            {
        //                Stafflabour = "S";
        //            }
        //            else if (ddlcategory.SelectedValue == "2")
        //            {
        //                Stafflabour = "L";
        //            }
        //            string constr = ConfigurationManager.AppSettings["ConnectionString"];
        //            SqlConnection con = new SqlConnection(constr);
        //            string Query = "";


        //            if (rbsalary.SelectedValue == "2" && ddlEmployeeType.SelectedValue == "1") // Staff Salary
        //            {
        //                //Query = " Select ''as Remaining_Days,SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName + '.' + EmpDet.Initial) as Name,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,convert(varchar(20),OP.Dateofjoining,105) as DOJ, " +
        //                //  " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.BasicHRA,	 SalDet.ConvAllow, " +
        //                //  " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,AD.halfNight,(SalDet.GrossEarnings + SalDet.ProvidentFund) as GrossAmt, " +
        //                //  " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2,SalDet.Fixed_Work_Days,SalDet.HomeDays,AD.ThreeSided ,SalDet.OTHoursAmtNew,SalDet.Fbasic, " +
        //                //  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings, " +
        //                //  " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
        //                //  " SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
        //                //  " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where " +
        //                //  " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and AD.Months='" + ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
        //                //  " EmpDet.EmployeeType='" + ddlEmployeeType.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.Lcode='" + SessionLcode + "' group by SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
        //                //  " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,SalDet.BasicHRA,AD.ThreeSided ,SalDet.OTHoursAmtNew,SalDet.Fbasic, " +
        //                //  " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.HomeDays,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
        //                //  " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SalDet.HalfNightAmt,SM.Base,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.Initial,AD.halfNight,OP.Dateofjoining,SalDet.Fixed_Work_Days,SalDet.HomeDays order by EmpDet.ExisistingCode asc";


        //                //Query = " Select SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName)  as Name,OP.PFnumber as PFNo,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.BasicandDA,SalDet.BasicHRA,SalDet.ConvAllow,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.SalaryDate, SalDet.Stamp," +
        //                //        " (SalDet.BasicandDA +SalDet.BasicHRA+SalDet.ConvAllow+SalDet.MediAllow) as GrossEarnings,(SalDet.ESI+SalDet.ProvidentFund) as TotalDeductions,SalDet.DedOthers1,SalDet.DedOthers2," +
        //                //      "  SalDet.Fixed_Work_Days,SalDet.Fbasic,MstDpt.DepartmentNm,SalDet.ProvidentFund, SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,SalDet.MediAllow,SalDet.EmployeerPFone,SalDet.EmployeerESI, " +
        //                //      "  ((SalDet.BasicandDA +SalDet.BasicHRA+SalDet.ConvAllow+SalDet.MediAllow) -(SalDet.ESI+SalDet.ProvidentFund)) as NetPay,SalDet.Gift,SalDet.Fine" +
        //                //      "   from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo  inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo" +
        //                //      " where SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and AD.Months='" + ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
        //                //      " EmpDet.EmployeeType='" + ddlEmployeeType.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.Lcode='" + SessionLcode + "' group by OP.PFnumber" +
        //                //      " SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,  SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.EmployeerPFone,SalDet.EmployeerESI,  " +
        //                //      "SalDet.Year,SalDet.SalaryDate,SalDet.BasicHRA,SalDet.Fbasic,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,SalDet.DedOthers1,SalDet.DedOthers2, SalDet.PfSalary,SM.Base,SalDet.Fixed_Work_Days,SalDet.Stamp,SalDet.MediAllow order by EmpDet.ExisistingCode asc";

        //                Query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName)  as Name,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.BasicandDA,SalDet.BasicHRA,SalDet.ConvAllow,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.SalaryDate, SalDet.Stamp, (SalDet.BasicandDA +SalDet.BasicHRA+SalDet.ConvAllow+SalDet.MediAllow) as GrossEarnings,(SalDet.ESI+SalDet.ProvidentFund) as TotalDeductions,SalDet.DedOthers1,SalDet.DedOthers2,  SalDet.Fixed_Work_Days,SalDet.Fbasic,MstDpt.DepartmentNm,SalDet.ProvidentFund, SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,SalDet.MediAllow,SalDet.EmployeerPFone,SalDet.EmployeerESI,   ((SalDet.BasicandDA +SalDet.BasicHRA+SalDet.ConvAllow+SalDet.MediAllow) -(SalDet.ESI+SalDet.ProvidentFund)) as NetPay   from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo  inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where SalDet.Month='" + this.ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + this.ddlFinance.SelectedValue + "' and AD.Months='" + this.ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + this.ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + this.Stafflabour + "' and  EmpDet.EmployeeType='" + this.ddlEmployeeType.SelectedValue + "' and EmpDet.Ccode='" + this.SessionCcode + "' and OP.WagesType='" + this.rbsalary.SelectedValue + "' and EmpDet.Lcode='" + this.SessionLcode + "' group by  SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,  SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.EmployeerPFone,SalDet.EmployeerESI,  SalDet.Year,SalDet.SalaryDate,SalDet.BasicHRA,SalDet.Fbasic,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,SalDet.DedOthers1,SalDet.DedOthers2, SalDet.PfSalary,SM.Base,SalDet.Fixed_Work_Days,SalDet.Stamp,SalDet.MediAllow order by EmpDet.ExisistingCode asc";

        //            }
        //            else if (rbsalary.SelectedValue == "2" && ddlEmployeeType.SelectedValue == "2") // Labour
        //            {

        //                //Query = " Select ''as Remaining_Days,SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName + '.' + EmpDet.Initial) as Name,OP.PFnumber as PFNo,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,convert(varchar(20),OP.Dateofjoining,105) as DOJ, " +
        //                //  " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.BasicHRA,	 SalDet.ConvAllow, " +
        //                //  " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,AD.halfNight,(SalDet.GrossEarnings + SalDet.ProvidentFund) as GrossAmt, " +
        //                //  " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2,SalDet.Fixed_Work_Days,SalDet.HomeDays,AD.ThreeSided ,SalDet.OTHoursAmtNew,SalDet.Fbasic,AD.OTHoursNew, " +
        //                //  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Fine,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew, " +
        //                //  " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +

        //                //  //" SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
        //                //  " SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base,SalDet.Gift,SalDet.Fine from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
        //                //  " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where " +
        //                //  "  SalDet.Month='" + ddlMonths.SelectedValue + "'   AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and AD.Months='" + ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
        //                //  " EmpDet.EmployeeType='" + ddlEmployeeType.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.Lcode='" + SessionLcode + "' group by OP.PFnumber,SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
        //                //  " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,SalDet.BasicHRA,AD.ThreeSided ,SalDet.Fbasic,AD.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.Fbasic, " +
        //                //  " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.HomeDays,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Fine,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew," +
        //                //  " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SalDet.HalfNightAmt,SalDet.DedOthers1,SalDet.DedOthers2,SM.Base,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.Initial,AD.halfNight,OP.Dateofjoining,SalDet.Fixed_Work_Days,SalDet.HomeDays,SalDet.Gift,SalDet.Fine order by EmpDet.ExisistingCode asc";

        //                Query = " Select ''as Remaining_Days,SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName + '.' + EmpDet.Initial) as Name,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,convert(varchar(20),OP.Dateofjoining,105) as DOJ,  SalDet.CL,SalDet.BasicHRA,SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.BasicHRA,\t SalDet.ConvAllow,  SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,AD.halfNight,(SalDet.GrossEarnings + SalDet.ProvidentFund) as GrossAmt,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2,SalDet.Fixed_Work_Days,SalDet.HomeDays,AD.ThreeSided ,SalDet.OTHoursAmtNew,SalDet.Fbasic,AD.OTHoursNew,  SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew,  SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff, SalDet.BasicRAI,SalDet.WashingAllow,SalDet.PfSalary as PFEarnings,Saldet.HRA, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base,SalDet.Messdeduction from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo  inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where   SalDet.Month='" + this.ddlMonths.SelectedValue + "'   AND SalDet.FinancialYear='" + this.ddlFinance.SelectedValue + "' and AD.Months='" + this.ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + this.ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + this.Stafflabour + "' and  EmpDet.EmployeeType='" + this.ddlEmployeeType.SelectedValue + "' and EmpDet.Ccode='" + this.SessionCcode + "' and OP.WagesType='" + this.rbsalary.SelectedValue + "' and EmpDet.Lcode='" + this.SessionLcode + "' group by SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,  SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA,SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,SalDet.BasicHRA,AD.ThreeSided ,SalDet.Fbasic,AD.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.HomeDays,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew,SalDet.CL,SalDet.BasicHRA, SalDet.BasicRAI,SalDet.WashingAllow,SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SalDet.HalfNightAmt,SalDet.Messdeduction,SalDet.DedOthers1,SalDet.DedOthers2,SM.Base,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.Initial,AD.halfNight,OP.Dateofjoining,SalDet.Fixed_Work_Days,SalDet.HomeDays order by EmpDet.ExisistingCode asc";

        //            }
        //            else if ((this.rbsalary.SelectedValue == "2") && (this.ddlEmployeeType.SelectedValue == "3"))
        //            {
        //                Query = " Select ''as Remaining_Days,SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName + '.' + EmpDet.Initial) as Name,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,convert(varchar(20),OP.Dateofjoining,105) as DOJ,  SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.BasicHRA,\t SalDet.ConvAllow,SalDet.Advance,SalDet.WorkedDays,  SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,(SalDet.GrossEarnings + SalDet.ProvidentFund) as GrossAmt,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2,SalDet.Fixed_Work_Days,SalDet.HomeDays ,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,  SalDet.BasicRAI,SalDet.WashingAllow,SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff, SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo  inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo  where   SalDet.Month='" + this.ddlMonths.SelectedValue + "'   AND SalDet.FinancialYear='" + this.ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + this.Stafflabour + "' and  EmpDet.EmployeeType='" + this.ddlEmployeeType.SelectedValue + "'  and OP.WagesType='" + this.rbsalary.SelectedValue + "'  group by SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,  SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,SalDet.BasicHRA ,SalDet.Fbasic,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.HomeDays,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings, SalDet.BasicRAI,SalDet.WashingAllow,SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SalDet.HalfNightAmt,SalDet.Advance,SalDet.WorkedDays,SalDet.DedOthers1,SalDet.DedOthers2,SM.Base,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.Initial,OP.Dateofjoining,SalDet.Fixed_Work_Days,SalDet.HomeDays order by EmpDet.ExisistingCode asc";
        //            }
        //            //else if (rbsalary.SelectedValue == "3" && ddlEmployeeType.SelectedValue == "4")  // Labour Salary
        //            //{
        //            //    MyDate1 = DateTime.ParseExact(txtFromDate .Text, "dd-MM-yyyy", null);
        //            //    MyDate2 = DateTime.ParseExact(txtToDate .Text, "dd-MM-yyyy", null);

        //            //    Query = " Select ''as Remaining_Days,SalDet.EmpNo,EmpDet.ExisistingCode,OP.PFnumber as PFNo,(EmpDet.EmpName + '.' + EmpDet.Initial) as Name,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,convert(varchar(20),OP.Dateofjoining,105) as DOJ, " +
        //            //      " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.BasicHRA,	 SalDet.ConvAllow, " +
        //            //      " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,AD.halfNight,(SalDet.GrossEarnings + SalDet.ProvidentFund) as GrossAmt, " +
        //            //      " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2,SalDet.Fixed_Work_Days,SalDet.HomeDays,AD.ThreeSided ,SalDet.OTHoursAmtNew,SalDet.Fbasic, " +
        //            //      " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Fine,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew, " +
        //            //      " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +

        //            //      " SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base,SalDet.Gift,SalDet.Fine from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
        //            //     // " SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
        //            //      " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where " +
        //            //      " SalDet.FromDate= convert(datetime,'" + MyDate1 + "', 105) and  SalDet.ToDate= convert(datetime,'" + MyDate2 + "', 105) AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and AD.FromDate=convert(datetime,'" + MyDate1 + "', 105) and  AD.ToDate=convert(datetime,'" + MyDate2 + "', 105) AND AD.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
        //            //      " EmpDet.EmployeeType='" + ddlEmployeeType.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.Lcode='" + SessionLcode + "' group by OP.PFnumber,SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
        //            //      " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,SalDet.BasicHRA,AD.ThreeSided ,SalDet.OTHoursAmtNew,SalDet.Fbasic, " +
        //            //      " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.HomeDays,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Fine,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew," +
        //            //      " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SalDet.HalfNightAmt,SM.Base,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.Initial,AD.halfNight,OP.Dateofjoining,SalDet.Fixed_Work_Days,SalDet.HomeDays,SalDet.Gift,SalDet.Fine order by EmpDet.ExisistingCode asc";
                    

        //            //}
        //            else if ((this.rbsalary.SelectedValue == "1") && (this.ddlEmployeeType.SelectedValue == "3"))
        //            {
        //                this.MyDate1 = DateTime.ParseExact(this.txtFromDate.Text, "dd-MM-yyyy", null);
        //                this.MyDate2 = DateTime.ParseExact(this.txtToDate.Text, "dd-MM-yyyy", null);
        //                Query = string.Concat(new object[] { 
        //                " Select ''as Remaining_Days,SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName + '.' + EmpDet.Initial) as Name,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,convert(varchar(20),OP.Dateofjoining,105) as DOJ,  SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.BasicHRA,\t SalDet.ConvAllow,  SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,AD.halfNight,(SalDet.GrossEarnings + SalDet.ProvidentFund) as GrossAmt,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2,SalDet.Fixed_Work_Days,SalDet.HomeDays,AD.ThreeSided ,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew,  SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff, SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo  inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where  SalDet.FromDate= convert(datetime,'", this.MyDate1, "', 105) and  SalDet.ToDate= convert(datetime,'", this.MyDate2, "', 105) AND SalDet.FinancialYear='", this.ddlFinance.SelectedValue, "' and AD.FromDate=convert(datetime,'", this.MyDate1, "', 105) and  AD.ToDate=convert(datetime,'", this.MyDate2, "', 105) AND AD.FinancialYear='", this.ddlFinance.SelectedValue, "' and EmpDet.StafforLabor='", this.Stafflabour, "' and  EmpDet.EmployeeType='", this.ddlEmployeeType.SelectedValue, 
        //                "' and EmpDet.Ccode='", this.SessionCcode, "' and OP.WagesType='", this.rbsalary.SelectedValue, "' and EmpDet.Lcode='", this.SessionLcode, "' group by SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,  SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,SalDet.BasicHRA,AD.ThreeSided ,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.HomeDays,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew, SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SalDet.HalfNightAmt,SM.Base,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.Initial,AD.halfNight,OP.Dateofjoining,SalDet.Fixed_Work_Days,SalDet.HomeDays order by EmpDet.ExisistingCode asc"
        //             });
        //            }
        //            SqlCommand cmd = new SqlCommand(Query, con);
        //            DataTable dt_1 = new DataTable();
        //            SqlDataAdapter sda = new SqlDataAdapter(cmd);
        //            con.Open();
        //            sda.Fill(dt_1);
        //            con.Close();
        //            if (dt_1.Rows.Count != 0)
        //            {
        //                FixedDays = dt_1.Rows[0]["Totalworkingdays"].ToString(); 
        //            }

        //            if (ddlEmployeeType.SelectedValue == "1")  // Staff
        //            {
        //                StaffGridView.DataSource = dt_1;
        //                StaffGridView.DataBind();
        //            }
        //            else if (ddlEmployeeType.SelectedValue == "2") // Labour
        //            {
        //                Grid_Labour.DataSource = dt_1;
        //                Grid_Labour.DataBind();
        //            }

        //            string attachment = "attachment;filename=Payslip.xls";
        //            Response.ClearContent();
        //            Response.AddHeader("content-disposition", attachment);
        //            Response.ContentType = "application/ms-excel";
        //            DataTable dt = new DataTable();
        //            dt = objdata.Company_retrive(SessionCcode, SessionLcode);
        //            if (dt.Rows.Count > 0)
        //            {
        //                CmpName = dt.Rows[0]["Cname"].ToString();
        //                Cmpaddress = (dt.Rows[0]["Address1"].ToString() + " - " + dt.Rows[0]["Location"].ToString() + " - " + dt.Rows[0]["Pincode"].ToString());
        //            }

        //            StringWriter stw = new StringWriter();
        //            HtmlTextWriter htextw = new HtmlTextWriter(stw);
        //            if (ddlEmployeeType.SelectedValue == "1")
        //            {
        //                StaffGridView.RenderControl(htextw);
        //            }
                   
        //            if (ddlEmployeeType.SelectedValue == "2")
        //            {
        //                Grid_Labour.RenderControl(htextw);
        //            }

        //            Response.Write("<table style='font-weight:bold;'>");
        //            Response.Write("<tr align='Center' style='font-size:18.0pt; text-decoration:underline;'>");
        //            Response.Write("<td colspan='20'>");
        //            Response.Write("" + CmpName + " - PAYSLIP REPORT");
        //            //Response.Write("GCS TEXTILE");
        //            Response.Write("</td>");
        //            Response.Write("</tr>");
        //            Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
        //            Response.Write("<td colspan='20'>");
        //            Response.Write("" + SessionLcode + "");
        //            Response.Write("</td>");
        //            Response.Write("</tr>");
        //            Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
        //            Response.Write("<td colspan='20'>");
        //            Response.Write("" + Cmpaddress + "");
        //            Response.Write("</td>");
        //            Response.Write("</tr>");
        //            if (rbsalary.SelectedValue == "2")
        //            {
        //                Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
        //                Response.Write("<td colspan='20'>");
        //                Response.Write("Salary Month of -" + ddlMonths.SelectedValue + " - " + FixedDays);
        //                Response.Write("</td>");
        //                Response.Write("</tr>");
        //            }
        //            else
        //            {
        //                Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
        //                Response.Write("<td colspan='20'>");
        //                Response.Write("Salary Month of -" + txtFromDate.Text + "   " + txtToDate.Text);
        //                Response.Write("</td>");
        //                Response.Write("</tr>");
        //            }
        //            Response.Write("</table>");
        //            Response.Write(stw.ToString());
        //            Response.Write("<table border='1'>");
        //            Response.Write("<tr Font-Bold='true' align='right'>");
        //            Response.Write("</td>");

        //            if (ddlEmployeeType.SelectedValue == "1")
        //            {
        //                Response.Write("<td font-Bold='true' colspan='4'>");
        //                Response.Write("Grand Total");
        //                Response.Write("</td>");
        //            }
        //            if (ddlEmployeeType.SelectedValue == "2")
        //            {
        //                Response.Write("<td font-Bold='true' colspan='4'>");
        //                Response.Write("Grand Total");
        //                Response.Write("</td>");
        //            }
                  
                   

        //            //Get Count
        //            string Pay_RowCount = "0";


        //            if (ddlEmployeeType.SelectedValue == "1")
        //            {
        //                Pay_RowCount = Convert.ToDecimal(StaffGridView.Rows.Count + 5).ToString();
        //            }
                   
        //            if (ddlEmployeeType.SelectedValue == "2")
        //            {
        //                Pay_RowCount = Convert.ToDecimal(Grid_Labour.Rows.Count + 5).ToString();
        //            }
                   
        //            if (ddlEmployeeType.SelectedValue == "1")
        //            {

        //                Response.Write("<td>=sum(E6:E" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(P6:P" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(Q6:Q" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(R6:R" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(S6:S" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(T6:T" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(U6:U" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(V6:V" + Pay_RowCount + ")</td>");
        //               Response.Write("<td>=sum(W6:W" + Pay_RowCount + ")</td>");

        //        }
        //            if (ddlEmployeeType.SelectedValue == "2")
        //            {

        //                Response.Write("<td>=sum(E6:E" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(P6:P" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(Q6:Q" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(R6:R" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(S6:S" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(T6:T" + Pay_RowCount + ")</td>");
        //                Response.Write("<td>=sum(U6:U" + Pay_RowCount + ")</td>");
                   
        //            Response.Write("<td>=sum(V6:V" + Pay_RowCount + ")</td>");
        //            Response.Write("<td>=sum(W6:W" + Pay_RowCount + ")</td>");


        //        }
       
        //            Response.Write("</tr>");
        //            Response.Write("</table>");
        //            Response.Write("<table>");
        //            Response.End();
        //            Response.Clear();
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);


                  

        //        }
           
        //}
        //catch (Exception ex)
        //{
        //}



        try
        {
            bool flag = false;
            string str = "";
            string str2 = "";
            int num = 0;
            if (this.ddlMonths.SelectedValue == "January")
            {
                num = Convert.ToInt32(this.ddlFinance.SelectedValue) + 1;
            }
            else if (this.ddlMonths.SelectedValue == "February")
            {
                num = Convert.ToInt32(this.ddlFinance.SelectedValue) + 1;
            }
            else if (this.ddlMonths.SelectedValue == "March")
            {
                num = Convert.ToInt32(this.ddlFinance.SelectedValue) + 1;
            }
            else
            {
                num = Convert.ToInt32(this.ddlFinance.SelectedValue);
            }
            if (this.ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "Window", "alert('Select the Category');", true);
                flag = true;
            }
            else if ((this.ddlEmployeeType.SelectedValue == "") || (this.ddlEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "Window", "alert('Select the Employee Type');", true);
                flag = true;
            }
            else if ((this.ddlMonths.SelectedValue == "0") || (this.ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "Window", "alert('Select the Months');", true);
                flag = true;
            }
            if (!flag)
            {
                if (this.ddlcategory.SelectedValue == "1")
                {
                    this.Stafflabour = "S";
                }
                else if (this.ddlcategory.SelectedValue == "2")
                {
                    this.Stafflabour = "L";
                }
                string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection connection = new SqlConnection(connectionString);
                string cmdText = "";
                if ((this.rbsalary.SelectedValue == "2") && (this.ddlEmployeeType.SelectedValue == "1"))
                {
                    cmdText = " Select SalDet.EmpNo,OP.PFnumber as PFNo,SalDet.Fine,SalDet.Gift,EmpDet.ExisistingCode,(EmpDet.EmpName)  as Name,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.BasicandDA,SalDet.BasicHRA,SalDet.ConvAllow,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.SalaryDate, SalDet.Stamp, (SalDet.BasicandDA +SalDet.BasicHRA+SalDet.ConvAllow+SalDet.MediAllow) as GrossEarnings,(SalDet.ESI+SalDet.ProvidentFund) as TotalDeductions,SalDet.DedOthers1,SalDet.DedOthers2,  SalDet.Fixed_Work_Days,SalDet.Fbasic,MstDpt.DepartmentNm,SalDet.ProvidentFund, SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,SalDet.MediAllow,SalDet.EmployeerPFone,SalDet.EmployeerESI,   ((SalDet.BasicandDA +SalDet.BasicHRA+SalDet.ConvAllow+SalDet.MediAllow) -(SalDet.ESI+SalDet.ProvidentFund)) as NetPay   from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo  inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where SalDet.Month='" + this.ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + this.ddlFinance.SelectedValue + "' and AD.Months='" + this.ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + this.ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + this.Stafflabour + "' and  EmpDet.EmployeeType='" + this.ddlEmployeeType.SelectedValue + "' and EmpDet.Ccode='" + this.SessionCcode + "' and OP.WagesType='" + this.rbsalary.SelectedValue + "' and EmpDet.Lcode='" + this.SessionLcode + "' group by  SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,  SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.EmployeerPFone,SalDet.EmployeerESI,  SalDet.Year,SalDet.SalaryDate,SalDet.BasicHRA,SalDet.Fbasic,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,SalDet.DedOthers1,SalDet.DedOthers2, SalDet.PfSalary,SM.Base,SalDet.Fixed_Work_Days,SalDet.Stamp,SalDet.MediAllow,OP.PFnumbe,SalDet.Fine,SalDet.Gift order by EmpDet.ExisistingCode asc";
                }
                else if ((this.rbsalary.SelectedValue == "2") && (this.ddlEmployeeType.SelectedValue == "2"))
                {
                    cmdText = " Select ''as Remaining_Days,OP.PFnumber as PFNo,SalDet.Fine,SalDet.Gift,SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName + '.' + EmpDet.Initial) as Name,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,convert(varchar(20),OP.Dateofjoining,105) as DOJ,  SalDet.CL,SalDet.BasicHRA,SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.BasicHRA,\t SalDet.ConvAllow,  SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,AD.halfNight,(SalDet.GrossEarnings + SalDet.ProvidentFund) as GrossAmt,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2,SalDet.Fixed_Work_Days,SalDet.HomeDays,AD.ThreeSided ,SalDet.OTHoursAmtNew,SalDet.Fbasic,AD.OTHoursNew,  SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew,  SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff, SalDet.BasicRAI,SalDet.WashingAllow,SalDet.PfSalary as PFEarnings,Saldet.HRA, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base,SalDet.Messdeduction from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo  inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where   SalDet.Month='" + this.ddlMonths.SelectedValue + "'   AND SalDet.FinancialYear='" + this.ddlFinance.SelectedValue + "' and AD.Months='" + this.ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + this.ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + this.Stafflabour + "' and  EmpDet.EmployeeType='" + this.ddlEmployeeType.SelectedValue + "' and EmpDet.Ccode='" + this.SessionCcode + "' and OP.WagesType='" + this.rbsalary.SelectedValue + "' and EmpDet.Lcode='" + this.SessionLcode + "' group by SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,  SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA,SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,SalDet.BasicHRA,AD.ThreeSided ,SalDet.Fbasic,AD.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.HomeDays,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew,SalDet.CL,SalDet.BasicHRA, SalDet.BasicRAI,SalDet.WashingAllow,SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SalDet.HalfNightAmt,SalDet.Messdeduction,SalDet.DedOthers1,SalDet.DedOthers2,SM.Base,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.Initial,AD.halfNight,OP.Dateofjoining,SalDet.Fixed_Work_Days,SalDet.HomeDays,OP.PFnumber,SalDet.Fine,SalDet.Gift order by EmpDet.ExisistingCode asc";
                }
                else if ((this.rbsalary.SelectedValue == "2") && (this.ddlEmployeeType.SelectedValue == "3"))
                {
                    cmdText = " Select ''as Remaining_Days,OP.PFnumber as PFNo,SalDet.Fine,SalDet.Gift,SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName + '.' + EmpDet.Initial) as Name,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,convert(varchar(20),OP.Dateofjoining,105) as DOJ,  SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.BasicHRA,\t SalDet.ConvAllow,SalDet.Advance,SalDet.WorkedDays,  SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,(SalDet.GrossEarnings + SalDet.ProvidentFund) as GrossAmt,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2,SalDet.Fixed_Work_Days,SalDet.HomeDays ,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,  SalDet.BasicRAI,SalDet.WashingAllow,SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff, SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo  inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo  where   SalDet.Month='" + this.ddlMonths.SelectedValue + "'   AND SalDet.FinancialYear='" + this.ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + this.Stafflabour + "' and  EmpDet.EmployeeType='" + this.ddlEmployeeType.SelectedValue + "'  and OP.WagesType='" + this.rbsalary.SelectedValue + "'  group by SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,  SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,SalDet.BasicHRA ,SalDet.Fbasic,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.HomeDays,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings, SalDet.BasicRAI,SalDet.WashingAllow,SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SalDet.HalfNightAmt,SalDet.Advance,SalDet.WorkedDays,SalDet.DedOthers1,SalDet.DedOthers2,SM.Base,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.Initial,OP.Dateofjoining,SalDet.Fixed_Work_Days,SalDet.HomeDays,OP.PFnumber,SalDet.Fine,SalDet.Gift order by EmpDet.ExisistingCode asc";
                }
                else if ((this.rbsalary.SelectedValue == "1") && (this.ddlEmployeeType.SelectedValue == "3"))
                {
                    this.MyDate1 = DateTime.ParseExact(this.txtFromDate.Text, "dd-MM-yyyy", null);
                    this.MyDate2 = DateTime.ParseExact(this.txtToDate.Text, "dd-MM-yyyy", null);
                    cmdText = string.Concat(new object[] { 
                        " Select ''as Remaining_Days,OP.PFnumber as PF,SalDet.Fine,SalDet.Gift,SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName + '.' + EmpDet.Initial) as Name,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,convert(varchar(20),OP.Dateofjoining,105) as DOJ,  SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.BasicHRA,\t SalDet.ConvAllow,  SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,AD.halfNight,(SalDet.GrossEarnings + SalDet.ProvidentFund) as GrossAmt,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2,SalDet.Fixed_Work_Days,SalDet.HomeDays,AD.ThreeSided ,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew,  SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff, SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo  inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where  SalDet.FromDate= convert(datetime,'", this.MyDate1, "', 105) and  SalDet.ToDate= convert(datetime,'", this.MyDate2, "', 105) AND SalDet.FinancialYear='", this.ddlFinance.SelectedValue, "' and AD.FromDate=convert(datetime,'", this.MyDate1, "', 105) and  AD.ToDate=convert(datetime,'", this.MyDate2, "', 105) AND AD.FinancialYear='", this.ddlFinance.SelectedValue, "' and EmpDet.StafforLabor='", this.Stafflabour, "' and  EmpDet.EmployeeType='", this.ddlEmployeeType.SelectedValue, 
                        "' and EmpDet.Ccode='", this.SessionCcode, "' and OP.WagesType='", this.rbsalary.SelectedValue, "' and EmpDet.Lcode='", this.SessionLcode, "' group by SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,  SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,SalDet.BasicHRA,AD.ThreeSided ,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.HomeDays,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew, SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SalDet.HalfNightAmt,SM.Base,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.Initial,AD.halfNight,OP.Dateofjoining,SalDet.Fixed_Work_Days,SalDet.HomeDays,OP.PFnumber,SalDet.Fine,SalDet.Gift order by EmpDet.ExisistingCode asc"
                     });
                }
                SqlCommand selectCommand = new SqlCommand(cmdText, connection);
                DataTable dataTable = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(selectCommand);
                connection.Open();
                adapter.Fill(dataTable);
                connection.Close();
                if (dataTable.Rows.Count != 0)
                {
                    dataTable.Rows[0]["Totalworkingdays"].ToString();
                }
                if (this.ddlEmployeeType.SelectedValue == "1")
                {
                    this.StaffGridView.DataSource = dataTable;
                    this.StaffGridView.DataBind();
                }
                else if (this.ddlEmployeeType.SelectedValue == "2")
                {
                    this.Grid_Labour.DataSource = dataTable;
                    this.Grid_Labour.DataBind();
                }
                else if (this.ddlEmployeeType.SelectedValue == "3")
                {
                    this.GridPayslip.DataSource = dataTable;
                    this.GridPayslip.DataBind();
                }
                string str5 = "attachment;filename=Payslip.xls";
                base.Response.ClearContent();
                base.Response.AddHeader("content-disposition", str5);
                base.Response.ContentType = "application/ms-excel";
                DataTable table2 = new DataTable();
                table2 = this.objdata.Company_retrive(this.SessionCcode, this.SessionLcode);
                if (table2.Rows.Count > 0)
                {
                    str = table2.Rows[0]["Cname"].ToString();
                    str2 = table2.Rows[0]["Address1"].ToString() + " - " + table2.Rows[0]["Location"].ToString() + " - " + table2.Rows[0]["Pincode"].ToString();
                }
                StringWriter writer = new StringWriter();
                HtmlTextWriter writer2 = new HtmlTextWriter(writer);
                if (this.ddlEmployeeType.SelectedValue == "1")
                {
                    this.StaffGridView.RenderControl(writer2);
                }
                if (this.ddlEmployeeType.SelectedValue == "2")
                {
                    this.Grid_Labour.RenderControl(writer2);
                }
                if (this.ddlEmployeeType.SelectedValue == "3")
                {
                    this.GridPayslip.RenderControl(writer2);
                }
                base.Response.Write("<table style='font-weight:bold;'>");
                base.Response.Write("<tr align='Center' style='font-size:18.0pt; text-decoration:underline;'>");
                base.Response.Write("<td colspan='20'>");
                base.Response.Write(str + " - PaySlip Report");
                base.Response.Write("</td>");
                base.Response.Write("</tr>");
                base.Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
                base.Response.Write("<td colspan='20'>");
                base.Response.Write(this.SessionLcode ?? "");
                base.Response.Write("</td>");
                base.Response.Write("</tr>");
                base.Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
                base.Response.Write("<td colspan='20'>");
                base.Response.Write(str2 ?? "");
                base.Response.Write("</td>");
                base.Response.Write("</tr>");
                if (this.rbsalary.SelectedValue == "2")
                {
                    base.Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
                    base.Response.Write("<td colspan='20'>");
                    base.Response.Write(string.Concat(new object[] { "Salary Month of -", this.ddlMonths.SelectedValue, " - ", num }));
                    base.Response.Write("</td>");
                    base.Response.Write("</tr>");
                }
                else
                {
                    base.Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
                    base.Response.Write("<td colspan='20'>");
                    base.Response.Write("Salary Month of -" + this.txtFromDate.Text + "   " + this.txtToDate.Text);
                    base.Response.Write("</td>");
                    base.Response.Write("</tr>");
                }
                base.Response.Write("</table>");
                base.Response.Write(writer.ToString());
                base.Response.Write("<table border='1'>");
                base.Response.Write("<tr Font-Bold='true' align='right'>");
                base.Response.Write("</td>");
                if (this.ddlEmployeeType.SelectedValue == "1")
                {
                    base.Response.Write("<td font-Bold='true' colspan='4'>");
                    base.Response.Write("Grand Total");
                    base.Response.Write("</td>");
                }
                if (this.ddlEmployeeType.SelectedValue == "2")
                {
                    base.Response.Write("<td font-Bold='true' colspan='4'>");
                    base.Response.Write("Grand Total");
                    base.Response.Write("</td>");
                }
                if (this.ddlEmployeeType.SelectedValue == "3")
                {
                    base.Response.Write("<td font-Bold='true' colspan='4'>");
                    base.Response.Write("Grand Total");
                    base.Response.Write("</td>");
                }
                string str6 = "0";
                if (this.ddlEmployeeType.SelectedValue == "1")
                {
                    str6 = Convert.ToDecimal((int)(this.StaffGridView.Rows.Count + 5)).ToString();
                }
                if (this.ddlEmployeeType.SelectedValue == "2")
                {
                    str6 = Convert.ToDecimal((int)(this.Grid_Labour.Rows.Count + 5)).ToString();
                }
                if (this.ddlEmployeeType.SelectedValue == "3")
                {
                    str6 = Convert.ToDecimal((int)(this.GridPayslip.Rows.Count + 5)).ToString();
                }
                if (this.ddlEmployeeType.SelectedValue == "1")
                {
                    base.Response.Write("<td>=sum(E6:E" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(F6:F" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(G6:G" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(H6:H" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(I6:I" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(J6:J" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(K6:K" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(L6:L" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(M6:M" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(N6:N" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(O6:O" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(P6:P" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(Q6:Q" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(R6:R" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(S6:S" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(T6:T" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(U6:U" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(V6:V" + str6 + ")</td>");
                }
                if (this.ddlEmployeeType.SelectedValue == "2")
                {
                    base.Response.Write("<td>=sum(E6:E" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(F6:F" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(G6:G" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(H6:H" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(I6:I" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(J6:J" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(K6:K" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(L6:L" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(M6:M" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(N6:N" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(O6:O" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(P6:P" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(Q6:Q" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(R6:R" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(S6:S" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(T6:T" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(U6:U" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(V6:V" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(W6:W" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(X6:X" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(Y6:Y" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(Z6:Z" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(AA6:AA" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(AB6:AB" + str6 + ")</td>");

                }
                if (this.ddlEmployeeType.SelectedValue == "3")
                {
                    base.Response.Write("<td>=sum(E6:E" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(F6:F" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(G6:G" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(H6:H" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(I6:I" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(J6:J" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(K6:K" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(L6:L" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(M6:M" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(N6:N" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(O6:O" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(P6:P" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(Q6:Q" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(R6:R" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(S6:S" + str6 + ")</td>");
                    base.Response.Write("<td>=sum(T6:T" + str6 + ")</td>");
                }
                base.Response.Write("</tr>");
                base.Response.Write("</table>");
                base.Response.End();
                base.Response.Clear();
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Downloaded Successfully');", true);
            }
        }
        catch (Exception)
        {
        }
    }

    
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
   

    


    protected void gvSalary_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void rbsalary_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbsalary.SelectedValue == "2")
        {
            pnlMonth.Visible = false;
        }
        else if (rbsalary.SelectedValue == "1")
        {
            pnlMonth.Visible = true;
        }
        else if (rbsalary.SelectedValue == "3")
        {
            pnlMonth.Visible = true;
        }
        else
        {
            pnlMonth.Visible = false;
        }
    }

 
    public string NumerictoNumber(int Number, bool isUK)
    {
        if (Number == 0)
            return "Zero";

        string and = isUK ? "and " : "";
        if (Number == -2147483648)
            return "Minus Two Billion One Hundred " + and +
        "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
        "Six Hundred " + and + "Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t = 0;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (Number < 0)
        {
            sb.Append("Minus");
            Number = -Number;
        }

        string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
        num[0] = Number % 1000;
        num[1] = Number / 1000;
        num[2] = Number / 1000000;
        num[1] = num[1] - 1000 * num[2];  // thousands
        num[2] = num[2] - 100 * num[3];//laks
        num[3] = Number / 10000000;     // billions
        num[2] = num[2] - 1000 * num[3];  // millions

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10;
            t = num[i] / 10;
            h = num[i] / 100;
            t = t - 10 * h;

            if (h > 0)
                sb.Append(words0[h] + " Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i < first)
                    sb.Append(and);

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);

            }

            if (i != 0)
                sb.Append(words3[i - 1]);

        }

        return sb.ToString().TrimEnd();



    }

   

 

    protected void txtLeftDate_TextChanged(object sender, EventArgs e)
    {

    }

    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    protected void btnSalary_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string FixedDays = "0";
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            if (ddlMonths.SelectedValue == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedValue == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedValue == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((ddlEmployeeType.SelectedValue == "") || (ddlEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Months');", true);
                ErrFlag = true;
            }





            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "1")
                {
                    Stafflabour = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    Stafflabour = "L";
                }
                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);
                string Query = "";



                //Query = "SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName + '.' + EmpDet.Initial) as Name,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,convert(varchar(20),OP.Dateofjoining,105) as DOJ, " +
                //  " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.BasicHRA,	 SalDet.ConvAllow, " +
                //  " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,AD.halfNight,(SalDet.GrossEarnings + SalDet.ProvidentFund) as GrossAmt, " +
                //  " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2,SalDet.Fixed_Work_Days,SalDet.HomeDays,AD.ThreeSided ,SalDet.OTHoursAmtNew,SalDet.Fbasic, " +
                //  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew, " +
                //  " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
                //  " SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                //  " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where " +
                //  "  SalDet.Month='" + ddlMonths.SelectedValue + "'   AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and AD.Months='" + ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                //  " EmpDet.EmployeeType='" + ddlEmployeeType.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.Lcode='" + SessionLcode + "' group by SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
                //  " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1,SalDet.BasicHRA,AD.ThreeSided ,SalDet.OTHoursAmtNew,SalDet.Fbasic, " +
                //  " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.HomeDays,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew," +
                //  " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary,SalDet.HalfNightAmt,SM.Base,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.Initial,AD.halfNight,OP.Dateofjoining,SalDet.Fixed_Work_Days,SalDet.HomeDays order by EmpDet.ExisistingCode asc";

                //Query = "select SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName) as Name,EmpDet.Designation,EmpDet.Department," +
                //        " SalDet.Totalworkingdays,SalDet.NFh,SalDet.Advance,SalDet.Stamp,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.WorkedDays,SalDet.Month," +
                //        " SalDet.Year,SalDet.SalaryDate,SalDet.allowances1,(SalDet.GrossEarnings) as GrossAmt," +
                //        " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.Deduction2," +
                //        " SalDet.Fixed_Work_Days,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2," +
                //        " SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew,SalDet.OverTime,MstDpt.DepartmentNm," +
                //        " SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
                //        " SalDet.DayIncentive,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                //        " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo" +
                //        "  inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where" +
                //        "  SalDet.Month='" + ddlMonths.SelectedValue + "'   AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and AD.Months='" + ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                //        " EmpDet.EmployeeType='" + ddlEmployeeType.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.Lcode='" + SessionLcode + "' group by  " +
                //        " SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,  SalDet.NFh,SalDet.Advance,SalDet.Stamp,SalDet.WorkedDays,SalDet.Month," +
                //        " SalDet.Year,SalDet.SalaryDate,SalDet.allowances1,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
                //        " SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay," +
                //        " SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew, SalDet.OverTime,MstDpt.DepartmentNm,SalDet.WorkedDays,SM.Base,SalDet.DedOthers1,SalDet.DedOthers2," +
                //        " SalDet.DayIncentive,EmpDet.Initial,OP.Dateofjoining,SalDet.Fixed_Work_Days order by EmpDet.ExisistingCode asc";

                if (rbsalary.SelectedValue == "2" && ddlEmployeeType.SelectedValue == "1") // Staff Salary
                {
                    Query = "select SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName) as Name,EmpDet.Designation,EmpDet.Department,SalDet.CL,SalDet.BasicHRA," +
                           " SalDet.Totalworkingdays,SalDet.NFh,SalDet.Advance,SalDet.Stamp,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.WorkedDays,SalDet.Month," +
                           " SalDet.Year,SalDet.SalaryDate,SalDet.allowances1,(SalDet.GrossEarnings) as GrossAmt,SalDet.ProvidentFund,SalDet.ESI ,SalDet.DayIncentive," +
                           " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.Deduction2," +
                           " SalDet.Fixed_Work_Days,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2," +
                           " SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew,SalDet.OverTime,MstDpt.DepartmentNm," +
                           " SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
                           " SalDet.DayIncentive,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                           " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo" +
                           "  inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where" +
                           "  SalDet.Month='" + ddlMonths.SelectedValue + "'   AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and AD.Months='" + ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                           " EmpDet.EmployeeType='" + ddlEmployeeType.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.Lcode='" + SessionLcode + "' group by  " +
                           " SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.DayIncentive,SalDet.NFh,SalDet.Advance,SalDet.Stamp,SalDet.WorkedDays,SalDet.Month," +
                           " SalDet.Year,SalDet.SalaryDate,SalDet.allowances1,SalDet.OTHoursAmtNew,SalDet.Fbasic,SalDet.CL,SalDet.BasicHRA,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
                           " SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.ProvidentFund,SalDet.ESI ," +
                           " SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew, SalDet.OverTime,MstDpt.DepartmentNm,SalDet.WorkedDays,SM.Base,SalDet.DedOthers1,SalDet.DedOthers2," +
                           " SalDet.DayIncentive,EmpDet.Initial,OP.Dateofjoining,SalDet.Fixed_Work_Days order by EmpDet.ExisistingCode asc";

                }
                if (rbsalary.SelectedValue == "2" && ddlEmployeeType.SelectedValue == "2") // Staff Salary
                {
                    Query = "select SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName) as Name,EmpDet.Designation,EmpDet.Department,SalDet.CL,SalDet.BasicHRA," +
                           " SalDet.Totalworkingdays,SalDet.NFh,SalDet.Advance,SalDet.Stamp,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.WorkedDays,SalDet.Month," +
                           " SalDet.Year,SalDet.SalaryDate,SalDet.allowances1,(SalDet.GrossEarnings) as GrossAmt,SalDet.ProvidentFund,SalDet.ESI ,SalDet.DayIncentive,SalDet.BasicandDA," +
                           " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.Deduction2," +
                           " SalDet.Fixed_Work_Days,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2," +
                           " SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew,SalDet.OverTime,MstDpt.DepartmentNm," +
                           " SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
                           " SalDet.DayIncentive,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                           " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo" +
                           "  inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where" +
                           "  SalDet.Month='" + ddlMonths.SelectedValue + "'   AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and AD.Months='" + ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                           " EmpDet.EmployeeType='" + ddlEmployeeType.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.Lcode='" + SessionLcode + "' group by  " +
                           " SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.DayIncentive,SalDet.NFh,SalDet.Advance,SalDet.Stamp,SalDet.WorkedDays,SalDet.Month," +
                           " SalDet.Year,SalDet.SalaryDate,SalDet.allowances1,SalDet.OTHoursAmtNew,SalDet.Fbasic,SalDet.CL,SalDet.BasicHRA,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
                           " SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.BasicandDA,SalDet.Deduction5,SalDet.Losspay,SalDet.ProvidentFund,SalDet.ESI ," +
                           " SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew, SalDet.OverTime,MstDpt.DepartmentNm,SalDet.WorkedDays,SM.Base,SalDet.DedOthers1,SalDet.DedOthers2," +
                           " SalDet.DayIncentive,EmpDet.Initial,OP.Dateofjoining,SalDet.Fixed_Work_Days order by EmpDet.ExisistingCode asc";
                }



                if (rbsalary.SelectedValue == "1" && ddlEmployeeType.SelectedValue == "3") // Weekly Salary
                {
                    Query = "select SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName) as Name,EmpDet.Designation,EmpDet.Department,SalDet.CL,SalDet.BasicHRA," +
                           " SalDet.Totalworkingdays,SalDet.NFh,SalDet.Advance,SalDet.Stamp,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.WorkedDays,SalDet.Month," +
                           " SalDet.Year,SalDet.SalaryDate,SalDet.allowances1,(SalDet.GrossEarnings) as GrossAmt,SalDet.ProvidentFund,SalDet.ESI ,SalDet.DayIncentive,SalDet.BasicandDA," +
                           " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.Deduction2," +
                           " SalDet.Fixed_Work_Days,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2," +
                           " SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew,SalDet.OverTime,MstDpt.DepartmentNm," +
                           " SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
                           " SalDet.DayIncentive,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                           " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo" +
                           "  inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where" +
                           "  SalDet.Month='" + ddlMonths.SelectedValue + "'   AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and AD.Months='" + ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                           " EmpDet.EmployeeType='" + ddlEmployeeType.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.Lcode='" + SessionLcode + "' group by  " +
                           " SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.DayIncentive,SalDet.NFh,SalDet.Advance,SalDet.Stamp,SalDet.WorkedDays,SalDet.Month," +
                           " SalDet.Year,SalDet.SalaryDate,SalDet.allowances1,SalDet.OTHoursAmtNew,SalDet.Fbasic,SalDet.CL,SalDet.BasicHRA,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
                           " SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.BasicandDA,SalDet.Deduction5,SalDet.Losspay,SalDet.ProvidentFund,SalDet.ESI ," +
                           " SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew, SalDet.OverTime,MstDpt.DepartmentNm,SalDet.WorkedDays,SM.Base,SalDet.DedOthers1,SalDet.DedOthers2," +
                           " SalDet.DayIncentive,EmpDet.Initial,OP.Dateofjoining,SalDet.Fixed_Work_Days order by EmpDet.ExisistingCode asc";
                }

                SqlCommand cmd = new SqlCommand(Query, con);
                DataTable dt_1 = new DataTable();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                con.Open();
                sda.Fill(dt_1);
                con.Close();
                if (dt_1.Rows.Count != 0)
                {
                    FixedDays = dt_1.Rows[0]["Totalworkingdays"].ToString();
                }

                if (ddlEmployeeType.SelectedValue == "3")
                {
                    Grid_Labour.DataSource = dt_1;
                    Grid_Labour.DataBind();
                }
                else
                {
                    GridPayslip.DataSource = dt_1;
                    GridPayslip.DataBind();
                }
               



                string attachment = "attachment;filename=Payslip.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                DataTable dt = new DataTable();
                dt = objdata.Company_retrive(SessionCcode, SessionLcode);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + " - " + dt.Rows[0]["Location"].ToString() + " - " + dt.Rows[0]["Pincode"].ToString());
                }

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);

                if (ddlEmployeeType.SelectedValue == "3")
                {
                    Grid_Labour.RenderControl(htextw);
                }
                else
                {
                    GridPayslip.RenderControl(htextw);
                }


                Response.Write("<table style='font-weight:bold;'>");
                Response.Write("<tr align='Center' style='font-size:18.0pt; text-decoration:underline;'>");
                Response.Write("<td colspan='20'>");
                Response.Write("" + CmpName + " - PAYSLIP REPORT");
                //Response.Write("GCS TEXTILE");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
                Response.Write("<td colspan='20'>");
                Response.Write("" + SessionLcode + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
                Response.Write("<td colspan='20'>");
                Response.Write("" + Cmpaddress + "");
                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
                Response.Write("<td colspan='20'>");
                Response.Write("Salary Month of -" + ddlMonths.SelectedValue + " - " + FixedDays);
                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.Write("<table border='1'>");
                Response.Write("<tr Font-Bold='true' align='right'>");
                Response.Write("</td>");


                Response.Write("<td font-Bold='true' colspan='4'>");
                Response.Write("Grand Total");
                Response.Write("</td>");



                //Get Count
                string Pay_RowCount = "0";

                if (ddlEmployeeType.SelectedValue == "3")
                {
                    Pay_RowCount = Convert.ToDecimal(Grid_Labour.Rows.Count + 5).ToString();
                    Response.Write("<td>=sum(E6:E" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(P6:P" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(Q6:Q" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(R6:R" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(S6:S" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(T6:T" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(U6:U" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(V6:V" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(W6:W" + Pay_RowCount + ")</td>");
                }
                else
                {
                    Pay_RowCount = Convert.ToDecimal(GridPayslip.Rows.Count + 5).ToString();
                    Response.Write("<td>=sum(E6:E" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(P6:P" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(Q6:Q" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(R6:R" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(S6:S" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(T6:T" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(U6:U" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(V6:V" + Pay_RowCount + ")</td>");
                    Response.Write("<td>=sum(W6:W" + Pay_RowCount + ")</td>");
                }
                

               

                
               
              




                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write("<table>");
                Response.End();
                Response.Clear();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);




            }

        }
        catch (Exception ex)
        {
        }
    }
    protected void btnStaffPayslip_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string FixedDays = "0";
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            if (ddlMonths.SelectedValue == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedValue == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedValue == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((ddlEmployeeType.SelectedValue == "") || (ddlEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Months');", true);
                ErrFlag = true;
            }





            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "1")
                {
                    Stafflabour = "S";
                }
                else if (ddlcategory.SelectedValue == "2")
                {
                    Stafflabour = "L";
                }


                ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedValue + "&yr=" + ddlFinance.SelectedValue + "&Salary=" + rbsalary.SelectedValue +  "&EmpType=" + ddlEmployeeType.SelectedValue.ToString(), "_blank", "");




                //string constr = ConfigurationManager.AppSettings["ConnectionString"];
                //SqlConnection con = new SqlConnection(constr);
                //string Query = "";

                //Query = "select SalDet.EmpNo,EmpDet.ExisistingCode,(EmpDet.EmpName) as Name,EmpDet.Designation,EmpDet.Department," +
                //        " SalDet.Totalworkingdays,SalDet.NFh,SalDet.Advance,SalDet.Stamp,(SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.WorkedDays,SalDet.Month," +
                //        " SalDet.Year,SalDet.SalaryDate,SalDet.allowances1,(SalDet.GrossEarnings) as GrossAmt," +
                //        " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.Deduction2," +
                //        " SalDet.Fixed_Work_Days,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                //        " SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew,SalDet.OverTime,MstDpt.DepartmentNm," +
                //        " SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
                //        " SalDet.DayIncentive,SM.Base from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                //        " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo" +
                //        "  inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo inner join AttenanceDetails AD on AD.EmpNo=SalDet.EmpNo where" +
                //        "  SalDet.Month='" + ddlMonths.SelectedValue + "'   AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and AD.Months='" + ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
                //        " EmpDet.EmployeeType='" + ddlEmployeeType.SelectedValue + "' and EmpDet.Ccode='" + SessionCcode + "' and OP.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.Lcode='" + SessionLcode + "' group by  " +
                //        " SalDet.EmpNo,SalDet.ConvAllow,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,  SalDet.NFh,SalDet.Advance,SalDet.Stamp,SalDet.WorkedDays,SalDet.Month," +
                //        " SalDet.Year,SalDet.SalaryDate,SalDet.allowances1,SalDet.OTHoursAmtNew,SalDet.Fbasic,  SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
                //        " SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay," +
                //        " SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,AD.OTHoursNew, SalDet.OverTime,MstDpt.DepartmentNm,SalDet.WorkedDays,SM.Base," +
                //        " SalDet.DayIncentive,EmpDet.Initial,OP.Dateofjoining,SalDet.Fixed_Work_Days order by EmpDet.ExisistingCode asc";
 
                //SqlCommand cmd = new SqlCommand(Query, con);
                //DataTable dt_1 = new DataTable();
                //SqlDataAdapter sda = new SqlDataAdapter(cmd);
                //con.Open();
                //sda.Fill(dt_1);
                //con.Close();
                //if (dt_1.Rows.Count != 0)
                //{
                //    FixedDays = dt_1.Rows[0]["Totalworkingdays"].ToString();
                //}


                //GridPayslip.DataSource = dt_1;
                //GridPayslip.DataBind();
                //string attachment = "attachment;filename=Payslip.xls";
                //Response.ClearContent();
                //Response.AddHeader("content-disposition", attachment);
                //Response.ContentType = "application/ms-excel";
                //DataTable dt = new DataTable();
                //dt = objdata.Company_retrive(SessionCcode, SessionLcode);
                //if (dt.Rows.Count > 0)
                //{
                //    CmpName = dt.Rows[0]["Cname"].ToString();
                //    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + " - " + dt.Rows[0]["Location"].ToString() + " - " + dt.Rows[0]["Pincode"].ToString());
                //}

                //StringWriter stw = new StringWriter();
                //HtmlTextWriter htextw = new HtmlTextWriter(stw);

                //GridPayslip.RenderControl(htextw);


                //Response.Write("<table style='font-weight:bold;'>");
                //Response.Write("<tr align='Center' style='font-size:18.0pt; text-decoration:underline;'>");
                //Response.Write("<td colspan='20'>");
                //Response.Write("" + CmpName + " - PAYSLIP REPORT");
                ////Response.Write("GCS TEXTILE");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                //Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
                //Response.Write("<td colspan='20'>");
                //Response.Write("" + SessionLcode + "");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                //Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
                //Response.Write("<td colspan='20'>");
                //Response.Write("" + Cmpaddress + "");
                //Response.Write("</td>");
                //Response.Write("</tr>");

                //Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
                //Response.Write("<td colspan='20'>");
                //Response.Write("Salary Month of -" + ddlMonths.SelectedValue + " - " + FixedDays);
                //Response.Write("</td>");
                //Response.Write("</tr>");

                //Response.Write("</table>");
                //Response.Write(stw.ToString());
                //Response.Write("<table border='1'>");
                //Response.Write("<tr Font-Bold='true' align='right'>");
                //Response.Write("</td>");


                //Response.Write("<td font-Bold='true' colspan='4'>");
                //Response.Write("Grand Total");
                //Response.Write("</td>");



                ////Get Count
                //string Pay_RowCount = "0";


                //Pay_RowCount = Convert.ToDecimal(GridPayslip.Rows.Count + 5).ToString();


                //Response.Write("<td>=sum(E6:E" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(F6:F" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(G6:G" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(H6:H" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(I6:I" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(J6:J" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(K6:K" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(L6:L" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(M6:M" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(N6:N" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(O6:O" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(P6:P" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(Q6:Q" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(R6:R" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(S6:S" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(T6:T" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(U6:U" + Pay_RowCount + ")</td>");
                //Response.Write("<td>=sum(V6:V" + Pay_RowCount + ")</td>");




                //Response.Write("</tr>");
                //Response.Write("</table>");
                //Response.Write("<table>");
                //Response.End();
                //Response.Clear();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);




            }

        }
        catch (Exception ex)
        {
        }
    }
}
