﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;



public partial class SalaryMasterUpload : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    AttenanceClass objatt = new AttenanceClass();
    string SessionCcode;
    string SessionLcode;
    string WagesType;
    SalaryMasterClass objStaffSal = new SalaryMasterClass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        if (SessionAdmin == "2")
        {
            Response.Redirect("EmployeeRegistration.aspx");

        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
       
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            //Skip
        }
       
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        string MachineID = "";
        string BasicSalary = "";
        string Allowance1 = "";
        string Allowance2 = "";
        string Deduction1 = "";
        string Deduction2 = "";
        string PFSalary = "";
        string EmpNo = "";
        string MachineNo = "";
        bool ErrFlag = false;

        int months = 0;
        int year = 0;

        months = Convert.ToInt32(DateTime.Now.ToString("MM"));
        year = Convert.ToInt32(DateTime.Now.ToString("yyyy"));


        if (months >= 4)
        {
            year = year + 1;

        }
        else
        {
            year = year;
        }


        try
        {
            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

                string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                DataTable dts = new DataTable();

                using (sSourceConnection)
                {

                    sSourceConnection.Open();
                    OleDbCommand command = new OleDbCommand("select * FROM [Sheet1$];", sSourceConnection);
                    sSourceConnection.Close();

                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        command.CommandText = "select * FROM [Sheet1$];";
                        sSourceConnection.Open();
                    }
                    using (OleDbDataReader dr = command.ExecuteReader())
                    {

                        if (dr.HasRows)
                        {

                        }

                    }
                    OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                    objDataAdapter.SelectCommand = command;
                    DataSet ds = new DataSet();

                    objDataAdapter.Fill(ds);
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                    }
                    string constr = ConfigurationManager.AppSettings["ConnectionString"];
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        SqlConnection cn = new SqlConnection(constr);
                        MachineID = dt.Rows[j][0].ToString();
                        BasicSalary = dt.Rows[j][4].ToString();
                        Allowance1 = dt.Rows[j][5].ToString();
                        Allowance2 = dt.Rows[j][6].ToString();
                        Deduction1 = dt.Rows[j][7].ToString();
                        Deduction2 = dt.Rows[j][8].ToString();
                        PFSalary = dt.Rows[j][9].ToString();


                        if (MachineID == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter MachineID. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (BasicSalary == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter MachineID. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (Allowance1 == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter MachineID. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (Allowance2 == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter MachineID. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (Deduction1 == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter MachineID. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (Deduction2 == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter MachineID. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (PFSalary == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter MachineID. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }

                    }
                    if (!ErrFlag)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            SqlConnection cn = new SqlConnection(constr);

                            cn.Open();
                            string MID = dt.Rows[i][0].ToString();
                            DataTable dtmachine = new DataTable();
                            dtmachine = objdata.GetMachineNo(MID, SessionCcode, SessionLcode);
                            if (dtmachine.Rows.Count > 0)
                            {
                                EmpNo = dtmachine.Rows[0]["EmpNo"].ToString();
                                MachineNo = dtmachine.Rows[0]["MachineNo"].ToString();

                                objStaffSal.EmpNo = EmpNo;
                                objStaffSal.Ccode = SessionCcode;
                                objStaffSal.Lcode = SessionLcode;
                                objStaffSal.Base = dt.Rows[i][4].ToString();
                                objStaffSal.HRA = "0";
                                objStaffSal.FDA = "0";
                                objStaffSal.VDA = "0";
                                objStaffSal.union = "0";
                                objStaffSal.total = "0";
                                objStaffSal.PFsalary = dt.Rows[i][9].ToString();
                                objStaffSal.Allowance1Amt = dt.Rows[i][5].ToString();
                                objStaffSal.Allowance2Amt = dt.Rows[i][6].ToString();
                                objStaffSal.deduction1 = dt.Rows[i][7].ToString();
                                objStaffSal.deduction2 = dt.Rows[i][8].ToString();

                                string StaffEmpCodeVerify = objdata.SalaryMasterUploadVerify(EmpNo, SessionCcode, SessionLcode);
                                if (StaffEmpCodeVerify == EmpNo)
                                {
                                    //objdata.SalaryMaster_Update(objStaffSal);

                                    //Update = true;

                                    // DataTable dt_del= new DataTable();

                                    objdata.DeleteSM_Sp(EmpNo, SessionCcode, SessionLcode);

                                    objdata.SalaryMasterUpload_Insert(objStaffSal);
                                    // Insert = true;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);

                                }
                                else
                                {
                                    objdata.SalaryMasterUpload_Insert(objStaffSal);
                                    // Insert = true;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);
                                }

                                cn.Close();
                            }


                        }


                    }
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your File Not Upload...');", true);
                }

            }
        }
        catch
        {

        }
    }
}
