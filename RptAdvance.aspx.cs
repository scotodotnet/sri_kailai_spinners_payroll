﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class RptAdvance : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    bool searchFlag = false;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        
        if (!IsPostBack)
        {
            category();
            //DropDownDepart();
        }
      
    }

    public void category()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public void DropDownDepart()
    {
        DataTable dt = new DataTable();
        dt = objdata.Depart_Labour();
        ddldepartment.DataSource = dt;
        ddldepartment.DataTextField = "DepartmentNm";
        ddldepartment.DataValueField = "DepartmentCd";
        ddldepartment.DataBind();
    }
    private void clr()
    {
        //lblEmpName1.Text = "";
        lbldesignation1.Text = "";
        lblDepartment1.Text = "";
        lbldop1.Text = "";
        DataTable dtempty = new DataTable();
        rptrEmployee.DataSource = dtempty;
        rptrEmployee.DataBind();
        panelAdvance.Visible = false;
        ddlEmpNo.SelectedIndex = 0;
        txtExist.Text = "";
           

    }
    protected void txtExist_TextChanged(object sender, EventArgs e)
    {
        if (searchFlag == false)
        {
            clr();

        }
    }

    
    protected void btnclr_Click(object sender, EventArgs e)
    {
        clr();
        //txtemp.Text = "";
        txtExist.Text = "";
        ddlcategory.SelectedIndex = 0;
        ddlEmpNo.SelectedIndex = 0;
        DataTable dtempty = new DataTable();
        ddlEmpName.DataSource = dtempty;
        ddlEmpName.DataBind();
        ddldepartment.SelectedIndex = 0;
        ddlEmpNo.SelectedIndex = 0;
        txtEmployee_Type.SelectedIndex = 0;
    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable dtempty = new DataTable();
        ddlEmpNo.DataSource = dtempty;
        ddlEmpNo.DataBind();
        ddlEmpName.DataSource = dtempty;
        ddlEmpName.DataBind();
        txtExist.Text = "";
        string stafforLabour = "";
        if (ddldepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {

            DataTable dtempcode = new DataTable();
            if (ddlcategory.SelectedValue == "1")
            {
                stafforLabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                stafforLabour = "L";
            }

            if (SessionAdmin == "1")
            {
                dtempcode = objdata.LoadEmployeeForBonus(stafforLabour, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
            }
            else
            {
                dtempcode = objdata.EmpLoadFORBonus_USER(stafforLabour, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
            }
            ddlEmpNo.DataSource = dtempcode;
            DataRow dr = dtempcode.NewRow();
            dr["EmpNo"] = ddldepartment.SelectedItem.Text;
            dr["EmpName"] = ddldepartment.SelectedItem.Text;
            dtempcode.Rows.InsertAt(dr, 0);

            ddlEmpNo.DataTextField = "EmpNo";
            ddlEmpNo.DataValueField = "EmpNo";
            ddlEmpNo.DataBind();
            ddlEmpName.DataSource = dtempcode;
            ddlEmpName.DataTextField = "EmpName";
            ddlEmpName.DataValueField = "EmpNo";
            ddlEmpName.DataBind();
        }
        clr();
    }
    protected void ddlEmpNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        
       
        if (ddlEmpNo.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            panelAdvance.Visible = true;
            DataTable dt = new DataTable();
            dt = objdata.AdvanceSalary_EmpNo(ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                ddlEmpName.DataSource = dt;
                ddlEmpName.DataTextField = "EmpName";
                ddlEmpName.DataValueField = "EmpNo";
                ddlEmpName.DataBind();
                txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                //txtemp.Text = dt.Rows[0]["EmpNo"].ToString();
                //lblEmpName1.Text = dt.Rows[0]["EmpName"].ToString();
                lbldop1.Text = dt.Rows[0]["Dateofjoining"].ToString();
                lblDepartment1.Text = dt.Rows[0]["DepartmentNm"].ToString();
                lbldesignation1.Text = dt.Rows[0]["Designation"].ToString();
                DataTable dt1 = new DataTable();
                dt1 = objdata.ADV_ALL_REPORT(ddlEmpNo.SelectedValue);
                rptrEmployee.DataSource = dt1;
                rptrEmployee.DataBind();
            }
        }
        else
        {
            panelAdvance.Visible = false;
        }
    }
    protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
       
        clr();
        if (ddlEmpName.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            panelAdvance.Visible = true;
            DataTable dt = new DataTable();
            dt = objdata.AdvanceSalary_EmpNo(ddlEmpName.SelectedValue, SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                ddlEmpNo.DataSource = dt;
                ddlEmpNo.DataTextField = "EmpNo";
                ddlEmpNo.DataValueField = "EmpNo";
                ddlEmpNo.DataBind();
                txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                //txtemp.Text = dt.Rows[0]["EmpNo"].ToString();
                //lblEmpName1.Text = dt.Rows[0]["EmpName"].ToString();
                lbldop1.Text = dt.Rows[0]["Dateofjoining"].ToString();
                lblDepartment1.Text = dt.Rows[0]["DepartmentNm"].ToString();
                lbldesignation1.Text = dt.Rows[0]["Designation"].ToString();
                DataTable dt1 = new DataTable();
                dt1 = objdata.ADV_ALL_REPORT(ddlEmpNo.SelectedValue);
                rptrEmployee.DataSource = dt1;
                rptrEmployee.DataBind();
            }
        }
        else
        {
            panelAdvance.Visible = false;
        }
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string staffLabour_temp;
        DataTable dtempty = new DataTable();
        ddldepartment.DataSource = dtempty;
        ddldepartment.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            staffLabour_temp = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            staffLabour_temp = "L";
        }
        else
        {
            staffLabour_temp = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(staffLabour_temp, SessionCcode, SessionLcode);
        if (dtDip.Rows.Count > 1)
        {
            ddldepartment.DataSource = dtDip;
            ddldepartment.DataTextField = "DepartmentNm";
            ddldepartment.DataValueField = "DepartmentCd";
            ddldepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldepartment.DataSource = dt;
            ddldepartment.DataBind();
        }
        EmployeeType_Load();

    }
    public void EmployeeType_Load()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        txtEmployee_Type.DataSource = dtemp;
        txtEmployee_Type.DataTextField = "EmpType";
        txtEmployee_Type.DataValueField = "EmpTypeCd";
        txtEmployee_Type.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
       
        string dpt = "";
        DataTable dt = new DataTable();
        clr();

        if (((txtExist.Text == "")))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Employee Number or Existing Number...!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Employee Number or Existing Number...!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            //dt = objdata.AdvanceSalary_EmpNo(txtemp.Text);
            if (txtExist.Text != "")
            {
                //if (SessionAdmin == "1")
                //{
                //    dt = objdata.AdvanceSalary_exist(txtExist.Text, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                //}
                //else
                //{
                //    dt = objdata.AdvanceSalary_exist_user(txtExist.Text, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
                //}
                dt = objdata.AdvanceSalary_Token_No_Search(txtExist.Text, SessionCcode, SessionLcode);
            }
            if (dt.Rows.Count > 0)
            {

                if (dt.Rows[0]["StafforLabor"].ToString().ToUpper() == "S".ToString().ToUpper())
                {
                    ddlcategory.SelectedValue = "1";
                }
                else if (dt.Rows[0]["StafforLabor"].ToString().ToUpper() == "L".ToString().ToUpper())
                {
                    ddlcategory.SelectedValue = "2";
                }
                ddlcategory_SelectedIndexChanged(sender, e);
                ddldepartment.SelectedValue = dt.Rows[0]["Department"].ToString();
                ddldepartment_SelectedIndexChanged(sender, e);

                panelAdvance.Visible = true;
                searchFlag = true;


                txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                ddlEmpNo.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                ddlEmpName.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                lbldop1.Text = dt.Rows[0]["Dateofjoining"].ToString();

                lblDepartment1.Text = dt.Rows[0]["DepartmentNm"].ToString();


                lbldesignation1.Text = dt.Rows[0]["Designation"].ToString();
                DataTable dt1 = new DataTable();
                dt1 = objdata.ADV_ALL_REPORT(dt.Rows[0]["EmpNo"].ToString());
                rptrEmployee.DataSource = dt1;
                rptrEmployee.DataBind();

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Employee Number Not Found....!');", true);
                //System.Windows.Forms.MessageBox.Show("Employee Number Not Found....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

            }
        }
        searchFlag = false;
    }
    protected void btnAll_Emp_Advance_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (ddlcategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
            ErrFlag = true;
        }
        if ((txtEmployee_Type.SelectedValue == "") || (txtEmployee_Type.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            string Stafflabour = "";
            if (ddlcategory.SelectedValue == "1")
            {
                Stafflabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                Stafflabour = "L";
            }

            ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + "" + "&Months=" + "" + "&yr=" + "2014" + "&fromdate=" + "" + "&ToDate=" + "" + "&Salary=" + "1" + "&CashBank=" + "1" + "&ESICode=" + "" + "&EmpType=" + txtEmployee_Type.SelectedValue.ToString() + "&PayslipType=" + "1" + "&PFTypePost=" + "1" + "&Left_Emp=" + "1" + "&Leftdate=" + "" + "&Report_Type=ALL_Emp_Advance_History", "_blank", "");
        }

    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {

            case ("Edit"):
                EditRepeaterData(id);
                break;
        }
    }
    private void EditRepeaterData(string id)
    {

        ResponseHelper.Redirect("ViewAdvance.aspx?Cate=&Depat=" + ddldepartment.SelectedValue + "&EmpType=" + txtEmployee_Type.SelectedValue + "&EmpNo=" + txtExist.Text, "_blank", "");


    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
  
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
