﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;



public partial class Bank_Master : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    MastersClass objMas = new MastersClass();
    string SessionAdmin;
    static string lblprobation_Common;
    string SessionCcode;
    string SessionLcode;
    string SSQL;
    static string bankid;
    bool ErrFlag = false;

    protected void Page_Load(object sender, EventArgs e)
    {

        //if (Session["UserId"] == null)
        //{
        //    Response.Redirect("Default.aspx");
        //    Response.Write("Your session expired");
        //}
        //string ss = Session["UserId"].ToString();
        //SessionAdmin = Session["Isadmin"].ToString();
        //SessionCcode = Session["Ccode"].ToString();
        //SessionLcode = Session["Lcode"].ToString();
       
        if (!IsPostBack)
        {
            BankDisplay();
        }
      
    }

  
    public void BankDisplay()
    {
        DataTable dt = new DataTable();
        dt = objdata.BankDisplay();
        rptrBank.DataSource = dt;
        rptrBank.DataBind();
        rptrBank.DataSource = dt;


    }



 

    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            string EmpNo = "";
            MastersClass objEmp = new MastersClass();
            bool isRegistered = false; 
            bool ErrFlag = false;

            if(txtBanknm.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Bankname');", true);
                ErrFlag = true;
            }
            DataTable dtdexist = new DataTable();
            SSQL = "select Bankcd from Bank where BankName='"+ txtBanknm.Text +"'";

            dtdexist = objdata.ReturnMultipleValue(SSQL);
            if (dtdexist.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Bank Name Already Exist');", true);
                ErrFlag = true;
            }
            else
            {
               if (btnsave.Text == "Update")
                {
                    objMas.Bankcd = bankid.ToString();
                    objMas.Bankname = txtBanknm.Text;
                    objdata.UpdateBank(objMas);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Bank Name Update Successfully..');", true);
                    BankDisplay();
                    btnsave.Text = "Save";
                    Clear();
                }
                else
                {
                    if (!ErrFlag)
                    {
                        string bank_name = objdata.BankName_verify_value(txtBanknm.Text);
                        if (bank_name.ToLower() == txtBanknm.Text.ToLower())
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Bank Name already Exist');", true);
                            ErrFlag = true;
                        }
                        if (!ErrFlag)
                        {
                            objMas.Bankname = txtBanknm.Text;
                            try
                            {
                                objdata.Bank(objMas);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Bank Save Successfully..!');", true);

                                txtBanknm.Text = "";
                                BankDisplay();
                                Clear();
                            }
                            catch (Exception ex)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Server Error Contact Admin....!');", true);
                            }
                        }

                    }
                }
            }
        }
        catch (Exception ex)
        {
           ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Contact Admin....!');", true);
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear();
    }

    public void Clear()
    {
        txtBanknm.Text = "";
        btnsave.Text = "Save";
        BankDisplay();
    }


    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        bankid = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(bankid);
                break;
            case ("Edit"):
                EditRepeaterData(bankid);
                break;
        }
    }

    private void EditRepeaterData(string BankID)
    {
        DataTable dtd = new DataTable();
        SSQL = "select BankName from Bank where Bankcd = '" + BankID.ToString() + "'";
        dtd = objdata.ReturnMultipleValue(SSQL);
        if (dtd.Rows.Count > 0)
        {
            txtBanknm.Text = dtd.Rows[0]["BankName"].ToString();
            btnsave.Text = "Update";
        }
    }

    private void DeleteRepeaterData(string BankID)
    {
        DataTable dtable = new DataTable();
        SSQL = "select BankName from Bank where Bankcd = '" + BankID.ToString() + "'";
        dtable = objdata.ReturnMultipleValue(SSQL);
        if (dtable.Rows.Count > 0)
        {
            string bankname = dtable.Rows[0]["BankName"].ToString();
            string bank_del = objdata.Bank_Delete(BankID.ToString());

            if (bank_del.Trim() == BankID.Trim())
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Cannot be Deleted....!');", true);
                ErrFlag = true;
            }
            else
            {
                DataTable dtd = new DataTable();
                SSQL = "Delete Bank where Bankcd = '" + BankID.ToString() + "'";
                objdata.ReturnMultipleValue(SSQL);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Delete Successfully..!');", true);
                BankDisplay();
                Clear();
            }

        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
   
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}