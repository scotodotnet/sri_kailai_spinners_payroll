﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Administration_Rights.aspx.cs" Inherits="AdministrationRights" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate>

 <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">Administration Rights</li></h4> 
                    </ol>
                </div>
   
 <div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
     
        
    
                        
   
                                       
            
            <div class="col-md-9">
			<div class="panel panel-white">
			         <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Administration Rights</h4>
				</div>
				</div>
				     <form class="form-horizontal">
				       <div class="panel-body">
					
					<%--<div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Company<span class="mandatory">*</span></label>
						<div class="col-sm-4">
						       <asp:DropDownList ID="ddlCompany" runat="server" class="form-control" AutoPostBack="true">
                               </asp:DropDownList> 
				        </div>
                      <div class="col-md-1"></div> 
						
				        <label for="input-Default" class="col-sm-2 control-label">Location<span class="mandatory">*</span></label>
						<div class="col-sm-4">
                              <asp:DropDownList ID="ddlLocation" runat="server" class="form-control" AutoPostBack="true">
                              </asp:DropDownList>
					   </div>
                   </div>--%>
                                   
                   <%-- <div class="col-md-1"></div> --%>                   
					<div class="form-group row">
						 <label for="input-Default" class="col-sm-2 control-label">User Name</label>
							<div class="col-sm-4">
                               <asp:DropDownList ID="ddlUserName" runat="server" class="form-control" 
                                    AutoPostBack="True" onselectedindexchanged="ddlUserName_SelectedIndexChanged">
                               </asp:DropDownList>
							</div>
					  </div>
					  
					  
					   <%--User Rights start--%> 
					   
					  <div class="col-sm-12"  style="padding: 0 1.4em 1.4em 1.4em !important;   border: 1px solid #e9e9e9 !important;">
					     <h4>User Rights</h4>
					     <br />
                         <div class="form-group row">
    					 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 <asp:CheckBox ID="ChkDashBoard" runat="server" /> DashBoard
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkEmployee" runat="server" />  Employee
					
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 <asp:CheckBox ID="ChkUpload" runat="server" />  Upload 
					
						 </div>
						 </div>  
                         
                         <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
					      <asp:CheckBox ID="ChkMasterPage" runat="server" />  Master Page
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkEmployeeRegistration" runat="server" /> Employee Registration 
					     </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 
						 <asp:CheckBox ID="ChkAttendanceUpload" runat="server" />   Attendance Upload
						 </div>
						 </div>
						 
						 <div class="form-group row">
					   
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 <asp:CheckBox ID="ChkUserCreation" runat="server" />  User Creation 	
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 <asp:CheckBox ID="ChkProfile" runat="server" />   Profile 
						 </div>
						<div class="col-sm-1"></div>
					     <div class="col-sm-3">
					     <asp:CheckBox ID="ChkEmployeeUpload" runat="server" />  Employee Upload
						 
						
						 </div>
						 </div>
						 
						 
						 <div class="form-group row">
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
					       <asp:CheckBox ID="ChkAdministrationRights" runat="server" /> Administration Rights 
						 </div>
						 
						 <div class="col-sm-1"></div>
					     <div class="col-sm-3">
					      <asp:CheckBox ID="ChkSalaryMaster" runat="server" />    Salary Master
						
						 </div>
						
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 <asp:CheckBox ID="ChkOTUpload" runat="server" />   OT Upload
						  
						 
						 </div>
						 </div>
						
						 <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
						 <asp:CheckBox ID="ChkEmployeeType" runat="server" />  Employee Type 
						 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkSalaryDetails" runat="server" />  Salary Details 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 <asp:CheckBox ID="ChkNewWagesUpload" runat="server" /> NewWages Upload 
						 
						
						 </div>
						 </div>
						 
						 
						 
						 <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
						 <asp:CheckBox ID="ChkPFandESIMaster" runat="server" />  PF & ESI Master
						 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkAdvance" runat="server" />   Advance
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 <asp:CheckBox ID="ChkSalaryMasterUpload" runat="server" />  SalaryMaster Upload
						
						
						 </div>
						 </div>
						 
						 <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
						 <asp:CheckBox ID="ChkBankDetails" runat="server" />  Bank Details
						 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkSettelment" runat="server" />  Settelment
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkSchemeUpload" runat="server" />   Scheme Upload
						
						
						 </div>
						 </div>
						 
					     <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
						 <asp:CheckBox ID="ChkDepartmentDetails" runat="server" />  Department Details
						 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkEmployeeReactive" runat="server" />  Employee Reactive
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkProfileUpload" runat="server" />  Profile Upload 
						
						
						 </div>
						 </div>
					
						 <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
						 <asp:CheckBox ID="ChkIncentiveMaster" runat="server" />  Incentive Master
						 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkReports" runat="server" />  Reports
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						 <asp:CheckBox ID="ChkESIandPF" runat="server" />  ESI & PF
						 </div>
						 </div>
						 
						 <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
						 <asp:CheckBox ID="ChkBasicDetails" runat="server" />  Basic Details
						 
						 </div>
						 <div class="col-sm-1"></div>
						 <div class="col-sm-3">
						  <asp:CheckBox ID="ChkBonusProcess" runat="server" />   Bonus Process
						 </div>
						 <div class="col-sm-1"></div>
						 
						  <div class="col-sm-3">
						  <asp:CheckBox ID="ChkPFDownload" runat="server" />  PF Download
						 </div>
						 
						 
						 
						 
						
						 
						 </div>
						 
					     <div class="form-group row">
					     <div class="col-sm-1"></div>
					     <div class="col-sm-3">
						 <asp:CheckBox ID="ChkOTPaymentDetails" runat="server" /> OT Payment Details
						 
						 </div>
						 
						 
						 
						 
						 
						
						 
						 </div>
					 
					       
						
						<!-- Button start -->
						<div class="form-group row">
						</div>
                            <div class="txtcenter">
                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                    onclick="btnSave_Click"/>
                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                    <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" 
                                    onclick="btnCancel_Click" />
                    </div>
                    <!-- Button End -->	
                         </div>
        
        </div>

				</form>
			
			</div><!-- panel white end -->
		   
		    
		    <div class="col-md-2"></div>
		    </div> 
            
       
       
       
      

                         <!-- Dashboard start -->
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- Dashboard End -->   
                       
  </div><!-- col 12 end -->
      
  </div><!-- row end -->
  
 </div>
 
 </ContentTemplate>
 </asp:UpdatePanel>
 
 
</asp:Content>

