﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Incentive_Master.aspx.cs" Inherits="Incentive_Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate>

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Incentive Master</li>
                   </ol>
               </div>
 
 
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Incentive Master</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
				
					    <div class="form-group row">
					     <div class="txtcenter">
					           <asp:Label ID="Label1" for="input-Default" class="col-sm-12 control-label" 
                                Text="REGULAR WORK DAYS INCENTIVE" runat="server" Font-Bold="True" Font-Underline="True"></asp:Label>
						  
						   </div>
						   </div>
						   
						
					
					    <div id="Div1" class="form-group row" visible="true" runat="server"  >
					    
					       <label for="input-Default" class="col-sm-2 control-label">Days Of Month</label>
						<div class="col-sm-4">
                            <asp:TextBox ID="txtMonthDays" class="form-control" runat="server" required></asp:TextBox>
                        </div>
					    
					   
						
				        </div>  
				       
				       	<div class="form-group row">
				       	<label for="input-Default" class="col-sm-2 control-label">Minimum Days Worked</label>
						<div class="col-sm-4">
						 <asp:TextBox ID="txtWorkerDays" class="form-control" runat="server" required></asp:TextBox>
                         
						</div>
					       <label for="input-Default" class="col-sm-2 control-label">Incentive Amount</label>
						   <div class="col-sm-4">
                              <asp:TextBox ID="txtWorkerIncentive" class="form-control" runat="server" required></asp:TextBox>
                           </div>
				        </div> 
				        
				      
				        <div class="form-group row">
				        <label for="input-Default" class="col-sm-2 control-label">OT</label>
				      
				        <div class="col-sm-4">
				        <asp:TextBox ID="txtOt" class="form-control" AutoPostBack="true" Text="8"  runat="server" required></asp:TextBox>
				        </div> 
				        </div>
				        	       
						<!-- Button start -->
						<div class="form-group row">
						</div>
                            <div class="txtcenter">
                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" onclick="btnSave1_Click"/>
                                     
                                                       
                   <asp:LinkButton ID="btnClear" class="btn btn-danger" runat="server" onclick="btnClear1_Click">Cancel</asp:LinkButton>      
                    </div>
                    <!-- Button End -->	
						
						<div class="form-group row">
						</div>
							
							<!-- Table start -->
							
							 <div class="table-responsive">
    
                              <asp:Repeater ID="rptrIncentiveMaster" runat="server" onitemcommand="Repeater1_ItemCommand">
                            <HeaderTemplate>
                
                            <table id="tableIncentiveMaster" class="display table" style="width: 100%;">
                          <thead>
                           <tr>
                                                 
                                                   
                                                    <th>Month Days</th>
                                                    <th>Days Worked</th> 
                                                    <th>Amount</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                                      
                    
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "MonthDays")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "WorkerDays")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "WorkerAmt")%>
                    </td>
                    <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("MonthDays")+","+ Eval("WorkerDays")+","+ Eval("WorkerAmt") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                     </td>
                    <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("MonthDays")+","+ Eval("WorkerDays")+","+ Eval("WorkerAmt") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
			
					         <!-- Table End -->
					         
					    
					    <div id="Div2" class="form-group row" visible="false" runat="server">
					      <div class="txtcenter">
					       <asp:Label ID="Label2" for="input-Default" class="col-sm-12 control-label" 
                                Text="DAYS OF THE MONTH INCENTIVE" runat="server" Font-Bold="True" Font-Underline="True"></asp:Label>
						  
						   </div>
					     </div>
					    
					  
					         
					    <div id="Div3" class="form-group row" visible="false" runat="server"  >
					    <label for="input-Default" class="col-sm-2 control-label">Incentive Amount</label>
						   <div class="col-sm-3">
                           <asp:TextBox ID="txtHostelAmt" class="form-control" runat="server" required></asp:TextBox>
                           </div>
					    </div>
				
					    <div id="Div4" class="form-group row" visible="false" runat="server">
						 <label for="input-Default" class="col-sm-2 control-label">Eligible Check</label>
						<div class="col-sm-16">
						   <div class="col-sm-2">
                           <asp:CheckBox ID="chkWorkDays" runat="server" Text="Worked Days" />
                           </div>
                           <div class="col-sm-2">
                           <asp:CheckBox ID="chkHAllowed" runat="server" Text="H.Allowed" />
                             </div>
                           <div class="col-sm-2">
                           <asp:CheckBox ID="chkOTDays" runat="server" Text="OTDays"/>
                             </div>
                           <div class="col-sm-2">
                           <asp:CheckBox ID="chkNFH" runat="server" Text="NFH Days"/>
                             </div>
                           <div class="col-sm-2">
                           <asp:CheckBox ID="chkNFHWorked" runat="server" Text="NFH Worked" Font-Bold="true"/>   
                        </div>
						
						</div>
						</div>
						
											
					    <div id="Div5" class="form-group row" visible="false" runat="server">
						 <label for="input-Default" class="col-sm-2 control-label">Day Attn.Calculation</label>
						<div class="col-sm-16">
						   <div class="col-sm-2">
                           <asp:CheckBox ID="chkAttnCalWDays" runat="server" Text="Worked Days" />
                           </div>
                           <div class="col-sm-2">
                           <asp:CheckBox ID="chkAttnCalHAllowed" runat="server" Text="H.Allowed" />
                             </div>
                           <div class="col-sm-2">
                           <asp:CheckBox ID="chkAttnCalOTDays" runat="server" Text="OTDays"/>
                             </div>
                           <div class="col-sm-2">
                           <asp:CheckBox ID="chkAttnCalNFH" runat="server" Text="NFH Days"/>
                             </div>
                           <div class="col-sm-2">
                           <asp:CheckBox ID="chkAttnCalNFHWorked" runat="server" Text="NFH Worked" Font-Bold="true"/>   
                        </div>
						
						</div>
						</div>
						
						<div id="Div6" class="form-group row" visible="false" runat="server">
						 <div class="txtcenter">
                            <asp:Button ID="btnSave2" class="btn btn-success"  runat="server" Text="Save" onclick="btnSave2_Click"/>
						</div>
						</div>
						
					    <div id="Div7" class="form-group row" visible="false" runat="server">
					    
					       <label for="input-Default" class="col-sm-2 control-label">Eligible Days</label>
						<div class="col-sm-3">
                            <asp:TextBox ID="txtCivilEligibleDays" class="form-control" runat="server" required></asp:TextBox>
                        </div>
					    
					   
						<label for="input-Default" class="col-sm-2 control-label">Incentive Amount</label>
						<div class="col-sm-3">
						 <asp:TextBox ID="txtCivilIncenAmount" class="form-control" runat="server" required></asp:TextBox>
                         
						</div>
				        </div>  
				        
				        
				        
				        <div id="Div8" class="txtcenter" visible="false" runat="server"> 
					       <asp:Button ID="BtnCivilIncenSave" class="btn btn-success"  runat="server" Text="Save"  onclick="BtnCivilIncenSave_Click"/>
					   </div>
                           
                        <!-- Table start -->
							
							 <div class="table-responsive">
    
                              <asp:Repeater ID="Repeater1" runat="server">
                            <HeaderTemplate>
                
                            <table id="tableIncentiveMaster" class="display table" style="width: 100%;">
                          <thead>
                           <tr>
                                                 
                                                   
                                                    <th>Amount</th>
                                                    <th>Delete</th>
                                                    
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "Amt")%>
                    </td>
                    <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("Amt") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
			
					     <!-- Table End -->
                        
                        
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 <!-- Dashboard start -->
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>  
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->

 </ContentTemplate>
 </asp:UpdatePanel>



</asp:Content>

