﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RptOT.aspx.cs" Inherits="RptOT" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Over Time</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
<div class="row">
<div class="col-md-12">
              
 <div class="col-md-9">
<div class="panel panel-white">
<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">OverTime Report</h4>
				</div>
				</div>

<form class="form-horizontal">

<div align="center">
<asp:Label ID="Label1" for="input-Default" class="col-sm-12 control-label" 
 Text="WAGES TYPE" runat="server" Font-Bold="True" Font-Underline="True"></asp:Label>
</div>

<div class="panel-body">
<div class="row">
<div class="form-group col-md-12">
					      
<div align="center">
<div class="form-group col-md-2"></div>
<div class="form-group col-md-9">
<asp:RadioButtonList ID="rbsalary" runat="server" AutoPostBack="true" 
    RepeatColumns="3" onselectedindexchanged="rbsalary_SelectedIndexChanged">
 <asp:ListItem Text ="Weekly Wages" Value="1"></asp:ListItem>
 <asp:ListItem Text="Monthly Wages" Value="2" ></asp:ListItem>                                                                                                                                       
 <asp:ListItem Text="Bi-Monthly Wages" Value="3"></asp:ListItem>
</asp:RadioButtonList>
</div>
</div>
</div>
</div>

<asp:Panel ID="pnlMonth" runat="server" visible="false">
<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblfrom" runat="server" class="col-sm-2 control-label" 
                                                             Text="From Date"></asp:Label>
					   <div class="col-sm-3">
                           <asp:TextBox ID="txtfrom" class="form-control" runat="server" AutoPostBack="true" ontextchanged="txtfrom_TextChanged"></asp:TextBox>
                           <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtfrom" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
                           <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR38" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtfrom" ValidChars="0123456789/- "></cc1:FilteredTextBoxExtender>
                                                              
					   </div>
				      <div class="col-md-1"></div>
				      <asp:Label ID="lblToDate" runat="server" class="col-sm-2 control-label" 
                                                             Text="To Date"></asp:Label>
				       <div class="col-sm-3">
				       	<asp:TextBox ID="txtTo" class="form-control" runat="server" AutoPostBack="true" ontextchanged="txtTo_TextChanged"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true"></cc1:CalendarExtender>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR1" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtTo" ValidChars="0123456789/- "></cc1:FilteredTextBoxExtender>
                       </div>
						
</div>
				        
</div>
</asp:Panel>

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblFinancialYr" runat="server" class="col-sm-2 control-label" 
                                                             Text="Financial Year"></asp:Label>
					   <div class="col-sm-3">
                       <asp:DropDownList ID="ddlfinance" class="form-control" runat="server" AutoPostBack="true">
                       </asp:DropDownList>                                
					   </div>
				      <div class="col-md-1"></div>
				      <asp:Label ID="lblMonths" runat="server" class="col-sm-2 control-label" 
                                                             Text="Month"></asp:Label>
				       <div class="col-sm-3">
				       <asp:DropDownList ID="ddlMonths" class="form-control" runat="server" AutoPostBack="true">
                       </asp:DropDownList>
				       </div>
						
</div>
				        
</div>

<asp:Panel ID="Panelload" runat="server" visible="false">
<div class="col-md-12">     
<div class="form-group row">
<table class="full">
<tbody>
<tr>
                                                            <td>
                                                                <asp:GridView ID="gvot" runat="server" AutoGenerateColumns="false" >
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Employee No</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Employee Name</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("EmpName") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Department</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Month</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblmonth" runat="server" Text='<%# Eval("Month") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>No of Hrs</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblHrs" runat="server" Text='<%# Eval("NoHrs") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Rate</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="Rate" runat="server" Text='<%# Eval("HrSalary") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="NetAmt" runat="server" Text='<%# Eval("netAmount") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                </td>
                                                            </tr>
</tbody>
</table>
</div>
</div>
</asp:Panel>

<div class="col-md-12">     
 <div class="form-group row">
    <div class="col-sm-5"></div>
	<div class="col-md-2">
    <asp:Button ID="btnExport" class="btn btn-success" runat="server" Text="Export" onclick="btnExport_Click" />
	</div>
 </div>				        
</div>

</div>
</form>
</div>
</div>

<!-- Dashboard start -->
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>  
 <!-- Dashboard End -->

</div>  <!-- col-md-12 End -->
</div>  <!-- row End -->
</div>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

