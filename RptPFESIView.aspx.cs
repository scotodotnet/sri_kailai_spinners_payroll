﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class RptPFESIView : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string str_cate;
    string str_dept;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate;
    string ToDate;
    string ReportType;
    string Emp_ESI_Code;

    string EmployeeType;
    string PayslipType;

    string PFTypeGet;
    string Left_Employee = "0";
    string Left_Date = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        //ReportDocument rd = new ReportDocument();
        SqlConnection con = new SqlConnection(constr);
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();

        
        str_month = Request.QueryString["Months"].ToString();
        str_yr = Request.QueryString["yr"].ToString();
        ReportType = Request.QueryString["ReportType"].ToString();

        Left_Employee = Request.QueryString["Left_Emp"].ToString();
        Left_Date = Request.QueryString["Leftdate"].ToString();

        int YR = 0;
        string report_head = "";
        if (str_month == "January")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "February")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "March")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(str_yr);
        }

        DataTable dt = new DataTable();
        dt = objdata.Company_retrive(SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {
            CmpName = dt.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        }

        query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.FatherName,MstDpt.DepartmentNm," +
            " EmpDet.Department,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL," +
            " SalDet.WH_Work_Days,'' as Leave_Credit,SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew," +
            " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
            " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
            " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
            " SalDet.Advance,SalDet.ProvidentFund,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
            " SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
            " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2," +
            " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
            " SalDet.SalaryDate,convert(varchar,OP.Dateofjoining,105) as Dateofjoining,OP.PFnumber,OP.ESICnumber," +
            " SalDet.EmployeerPFone,SalDet.EmployeerPFTwo,SalDet.EmployeerESI,EmpDet.EmployeeType" +
            " from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
            " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department" +
            " inner Join AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo And AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "'" +
            " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
            " SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.Ccode='" + SessionCcode + "'" +
            " And AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "'" +
            " and EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='2' ";

        //PF Check
        if (ReportType.ToString() == "1" || ReportType.ToString() == "2")
        {
            //ESI
            query = query + " and (OP.ElgibleESI='1') And SalDet.ESI >= 1";
        }
        else
        {
            //PF
            query = query + " and (OP.EligiblePF='1') And SalDet.ProvidentFund >= 1";
        }

        //Activate Employee Check
        if (Left_Employee == "1") { query = query + " and EmpDet.ActivateMode='N' and Month(EmpDet.deactivedate)=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(EmpDet.deactivedate)=Year(convert(datetime,'" + Left_Date + "', 105))"; }
        //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
        else
        {
            //if (Left_Date != "") { query = query + " and EmpDet.deactivedate > convert(datetime,'" + Left_Date + "', 105)"; }
            if (Left_Date != "") { query = query + " and (EmpDet.deactivedate > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.ActivateMode='Y')"; }
        }


        query = query + "group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.FatherName,MstDpt.DepartmentNm," +
        " EmpDet.Department,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided,SalDet.CL," +
        " SalDet.WH_Work_Days,SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew," +
        " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
        " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
        " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
        " SalDet.Advance,SalDet.ProvidentFund,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
        " SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
        " SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
        " SalDet.SalaryDate,OP.Dateofjoining,OP.PFnumber,OP.ESICnumber,SalDet.EmployeerPFone,SalDet.EmployeerPFTwo,SalDet.EmployeerESI,EmpDet.EmployeeType";
        if (ReportType.ToString() == "2")
        {
            query = query + " Order by OP.ESICnumber Asc";
        }
        else if (ReportType.ToString() == "4")
        {
            query = query + " Order by OP.PFnumber Asc";
        }
        else
        {
            query = query + " Order by EmpDet.ExisistingCode Asc";
        }

        SqlCommand cmd = new SqlCommand(query, con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataSet ds1 = new DataSet();
        con.Open();
        sda.Fill(ds1);
        DataTable dt_1 = new DataTable();
        sda.Fill(dt_1);
        con.Close();

        if (ds1.Tables[0].Rows.Count > 0)
        {
            if (ReportType == "1")
            {
                rd.Load(Server.MapPath("Payslip/ESIReport.rpt"));
            }
            else if (ReportType == "2")
            {
                rd.Load(Server.MapPath("Payslip/ESIOnline.rpt"));
            }

            else if (ReportType == "3")
            {
                rd.Load(Server.MapPath("Payslip/PFReport.rpt"));
            }

            else if (ReportType == "4")
            {
                rd.Load(Server.MapPath("Payslip/PFOnline.rpt"));
            }
            else
            {
                //rd.Load(Server.MapPath("PaySlip1.rpt"));
            }

            //Get PF And ESI Percentage
            query = "Select * from MstESIPF where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            DataTable PF_ESI_Percnt_ds = new DataTable();
            PF_ESI_Percnt_ds = objdata.RptEmployeeMultipleDetails(query);
            

            rd.SetDataSource(ds1.Tables[0]);

            rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
            rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

            if (ReportType == "3" || ReportType == "1")
            {
                if (PF_ESI_Percnt_ds.Rows.Count != 0)
                {
                    rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                    rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                    rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                }
                else
                {
                    rd.DataDefinition.FormulaFields["PF_Percent_One"].Text = "'" + "3.67" + "'";
                    rd.DataDefinition.FormulaFields["PF_Percent_Two"].Text = "'" + "8.33" + "'";
                    rd.DataDefinition.FormulaFields["ESI_Percent_One"].Text = "'" + "4.75" + "'";
                }
            }
            
            if (ReportType == "1")
            {
                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "ESI MONTHLY STATEMENT FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
            }
            else if (ReportType == "2")
            {
                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "ESI MONTHLY RETURN FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
            }
            else if (ReportType == "3")
            {
                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "PF MONTHLY STATEMENT FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
            }
            else if (ReportType == "4")
            {
                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + " " + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + " " + "'";
            }

            rd.DataDefinition.FormulaFields["PayMonth"].Text = "'" + str_month.ToUpper() + " - " + YR.ToString() + "'";
            

            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);

        }

    }
}
