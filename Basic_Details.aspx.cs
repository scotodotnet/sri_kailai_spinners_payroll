﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using System.Configuration;



public partial class Basic_Details : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        //string ss = Session["UserId"].ToString();
        //SessionAdmin = Session["Isadmin"].ToString();
        ////string ss = Session["UserId"].ToString();
        //SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        //if (SessionAdmin == "2")
        //{
        //    Response.Redirect("EmployeeRegistration.aspx");
        //}

        //SessionCcode = "ESM";
        //SessionLcode = "UNIT I";
        if (!IsPostBack)
        {
            DropDwonCategory();
        }

    }
    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }

    private void Load_data1()
    {
        DataTable dt = new DataTable();

        clr1();
        if (txtEmployeeType.SelectedValue != "" && txtEmployeeType.SelectedValue != "0")
        {
            dt = objdata.BsaicDetails_Load(SessionCcode, SessionLcode, ddlcategory.SelectedValue, txtEmployeeType.SelectedValue);
            if (dt.Rows.Count != 0)
            {
                txtBasicAndDA.Text = dt.Rows[0]["BasicDA"].ToString();
                txtHRA.Text = dt.Rows[0]["HRA"].ToString();
                txtConv_Allow.Text = dt.Rows[0]["ConvAllow"].ToString();
                txtEdu_Allow.Text = dt.Rows[0]["EduAllow"].ToString();
                txtMedi_Allow.Text = dt.Rows[0]["MediAllow"].ToString();
                txtRAI.Text = dt.Rows[0]["RAI"].ToString();
                txtWashing_Allow.Text = dt.Rows[0]["WashingAllow"].ToString();
            }

        }


    }
    private void clr1()
    {
        this.txtBasicAndDA.Text = "0.00";
        this.txtHRA.Text = "0.00";
        this.txtConv_Allow.Text = "0.00";
        this.txtEdu_Allow.Text = "0.00";
        this.txtMedi_Allow.Text = "0.00";
        this.txtRAI.Text = "0.00";
        this.txtWashing_Allow.Text = "0.00";
        
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        clr1();
        ddlcategory.SelectedValue = "1";
        txtEmployeeType.SelectedValue = "";

        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //try
        //{
        //    bool ErrFlag = false;
        //    bool SaveFlag = false;

        //    if (ddlcategory.SelectedValue == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
        //        ErrFlag = true;
        //    }
        //    else if (ddlcategory.SelectedValue == "0")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
        //        ErrFlag = true;
        //    }
        //    else if (txtEmployeeType.SelectedValue == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
        //    }
        //    else if (txtEmployeeType.SelectedValue == "0")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
        //    }


        //    if (!ErrFlag)
        //    {
        //        string WkDays = objdata.BsaicDetails_Verify(SessionCcode, SessionLcode, ddlcategory.SelectedValue, txtEmployeeType.Text);
        //        if (WkDays.Trim() == "")
        //        {
        //            objdata.BsaicDetails_Insert_New(SessionCcode, SessionLcode, ddlcategory.SelectedValue.ToString(), txtEmployeeType.SelectedValue.ToString(), txtBasicAndDA.Text.ToString(), txtHRA.Text.ToString(), txtConv_Allow.Text.ToString(), txtEdu_Allow.Text.ToString(), txtMedi_Allow.Text.ToString(), txtRAI.Text.ToString(), txtWashing_Allow.Text.ToString());
        //            //objdata.BsaicDetails_Insert_New();
        //            SaveFlag = true;
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
        //        }
        //        else
        //        {
        //            //Update
        //            objdata.BsaicDetails_Update(SessionCcode, SessionLcode, ddlcategory.SelectedValue.ToString(), txtEmployeeType.SelectedValue.ToString(), txtBasicAndDA.Text.ToString(), txtHRA.Text.ToString(), txtConv_Allow.Text.ToString(), txtEdu_Allow.Text.ToString(), txtMedi_Allow.Text.ToString(), txtRAI.Text.ToString(), txtWashing_Allow.Text.ToString());
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
        //            ErrFlag = true;
        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //}

        try
        {
            bool flag = false;
            if (this.ddlcategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Select the Category');", true);
                flag = true;
            }
            else if (this.ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Select the Category');", true);
                flag = true;
            }
            else if (this.txtEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else if (this.txtEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            if (!flag)
            {
                if (this.objdata.BsaicDetails_Verify(this.SessionCcode, this.SessionLcode, this.ddlcategory.SelectedValue, this.txtEmployeeType.Text).Trim() == "")
                {
                    this.objdata.BsaicDetails_Insert_New(this.SessionCcode, this.SessionLcode, this.ddlcategory.SelectedValue.ToString(), this.txtEmployeeType.SelectedValue.ToString(), this.txtBasicAndDA.Text.ToString(), this.txtHRA.Text.ToString(), this.txtConv_Allow.Text.ToString(), this.txtEdu_Allow.Text.ToString(), this.txtMedi_Allow.Text.ToString(), this.txtRAI.Text.ToString(), this.txtWashing_Allow.Text.ToString());
                    ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    this.objdata.BsaicDetails_Update(this.SessionCcode, this.SessionLcode, this.ddlcategory.SelectedValue.ToString(), this.txtEmployeeType.SelectedValue.ToString(), this.txtBasicAndDA.Text.ToString(), this.txtHRA.Text.ToString(), this.txtConv_Allow.Text.ToString(), this.txtEdu_Allow.Text.ToString(), this.txtMedi_Allow.Text.ToString(), this.txtRAI.Text.ToString(), this.txtWashing_Allow.Text.ToString());
                    ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Update Successfully');", true);
                    flag = true;
                }
            }
        }
        catch (Exception)
        {
        }
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        EmployeeType();
    }

    public void EmployeeType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        txtEmployeeType.DataSource = dtemp;
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }
    protected void txtEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_data1();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
