﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EmployeeRegistration2.aspx.cs" Inherits="EmployeeRegistration2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Employee Registration</li>
                       <h4>
                       </h4>
                        </h4>
                      
                      </ol>
                </div>
                <div id="main-wrapper" class="container">
<div class="row">
<div class="col-md-12">
<div class="col-md-9">
<div class="panel panel-white">
		<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Employee Registration</h4>
				</div>
				</div>
		<form class="form-horizontal">
				<div class="panel-body">
 <div class="col-md-12">     
      
				      <div class="form-group row">
					  <asp:Label ID="Label3" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
						<div class="col-sm-3">
                            <asp:DropDownList ID="ddlcategory" class="form-control" runat="server" AutoPostBack="True" onselectedindexchanged="ddlcategory_SelectedIndexChanged">
                            </asp:DropDownList>                          
                        </div>
					
				    <div class="col-sm-3">
				    <asp:Button ID="btnclick" class="btn btn-success" runat="server" Text="Click" 
                            onclick="btnclick_Click" />
				    </div>
				
			       </div>
			    </div>
			 
			<div class="col-md-12"><hr></div>
			 
		
<div class="col-md-12">
		
		<div class="form-group row">
					  <div align="center">
                          <asp:Label ID="lblPrsnDet" runat="server" Text="PERSONAL DETAILS" Font-Bold="True" 
                              Font-Underline="True" ></asp:Label>  
					  </div>
			       </div>
		
		</div>	 
		
<div class="col-md-12">     
				        <div class="form-group row">
				         <asp:Label ID="lblEmpNo" class="col-sm-2 control-label" runat="server" Text="Employee No"></asp:Label> 
					  
					      <div class="col-sm-2">
					       <asp:TextBox ID="txtEmpNo" class="form-control" runat="server"></asp:TextBox>
					      </div>
					    
					  <div class="col-md-1"></div>
                     
						</div>
				        
                       </div>
		
		      
<div class="col-md-12">     
 <div class="form-group row">
  <asp:Label ID="lblempname" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Employee Name"></asp:Label>
  <%--<label for="input-Default" class="col-sm-2 control-label">Employee Name</label>--%>
   <div class="col-sm-3">
       <asp:TextBox ID="txtempname" class="form-control" runat="server"></asp:TextBox>
     </div>
  
     <div class="col-sm-1"></div>    
     <asp:Label ID="lblempfname" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Father / Husband Name"></asp:Label>              
    <%--<label for="input-Default" class="col-sm-2 control-label">Father / Husband Name</label>--%>
    <div class="col-sm-3">
        <asp:TextBox ID="txtempfname" class="form-control" runat="server"></asp:TextBox>
     </div>
		
   
	</div>
		        
 </div>  
			       
<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblgender" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Gender"></asp:Label>              
 <%-- <label for="input-Default" class="col-sm-2 control-label">Gender</label>--%>
   <div class="col-sm-3">
   <asp:RadioButtonList ID="rbtngender" runat="server" RepeatColumns="2" 
     TabIndex="3" Width="210" onselectedindexchanged="rbtngender_SelectedIndexChanged">
      <asp:ListItem Selected="True" Text="Male" Value="1"></asp:ListItem>
      <asp:ListItem Selected="False" Text="Female" Value="2"></asp:ListItem>
     </asp:RadioButtonList>
     </div>
  
     <div class="col-sm-1"></div> 
    <asp:Label ID="lblDOB" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Date of Birth"></asp:Label>                  
    <%--<label for="input-Default" class="col-sm-2 control-label">Date of Birth</label>--%>
    <div class="col-sm-3">
        <asp:TextBox ID="txtdob" class="form-control" runat="server"></asp:TextBox>
        <cc1:CalendarExtender ID="Caleder_txtdob" runat="server" TargetControlID="txtdob"
        Format="dd-MM-yyyy" CssClass="orange">
        </cc1:CalendarExtender>
        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server"
          FilterMode="ValidChars" FilterType="Custom,Numbers"
          TargetControlID="txtdob" ValidChars="0123456789/-">
        </cc1:FilteredTextBoxExtender>
        
     </div>
		
   
	</div>
		        
 </div>  
 
<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblMartial" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Martial Status"></asp:Label>
  <%--<label for="input-Default" class="col-sm-2 control-label">Martial Status</label>--%>
   <div class="col-sm-3">
     <asp:RadioButtonList ID="Rbmartial" runat="server" RepeatColumns="2">
	  <asp:ListItem Selected="True" Text="Unmarried" Value="U"></asp:ListItem>
	  <asp:ListItem Selected="False" Text="Married" Value="M" ></asp:ListItem>
	 </asp:RadioButtonList>
     </div>
  
     <div class="col-sm-1"></div>    
   <asp:Label ID="lblinitial" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Father/Husband"></asp:Label>                
   <%-- <label for="input-Default" class="col-sm-2 control-label">Father/Husband</label>--%>
    <div class="col-sm-3">
        <asp:RadioButtonList ID="Rbfather" runat="server" RepeatColumns="2" >
		 <asp:ListItem Selected="True" Text="Father" Value="F" ></asp:ListItem>
		 <asp:ListItem Selected="False" Text="Husband" Value="S"></asp:ListItem>
		</asp:RadioButtonList>
     </div>
		
   
	</div>
		        
 </div>  

<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblPSadd1" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Address1"></asp:Label>
  <%--<label for="input-Default" class="col-sm-2 control-label">Address1</label>--%>
   <div class="col-sm-3">
       <asp:TextBox ID="txtPSAdd1" class="form-control" runat="server"></asp:TextBox>
     </div>
  
     <div class="col-sm-1"></div>  
     <asp:Label ID="lblDist" for="input-Default" class="col-sm-2 control-label" runat="server" Text="District"></asp:Label>                
   <%-- <label for="input-Default" class="col-sm-2 control-label">District</label>--%>
    <div class="col-sm-3">
        <asp:TextBox ID="txtDistrict" class="form-control" runat="server"></asp:TextBox>
     </div>
		
   
	</div>
		        
 </div>  

<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblPSAdd2" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Address2"></asp:Label> 
  <%--<label for="input-Default" class="col-sm-2 control-label">Address2</label>--%>
   <div class="col-sm-3">
       <asp:TextBox ID="txtPSAdd2" class="form-control" runat="server"></asp:TextBox>
     </div>
  
     <div class="col-sm-1"></div>    
     <asp:Label ID="lblTaluk" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Taluk"></asp:Label>               
    <%--<label for="input-Default" class="col-sm-2 control-label">Taluk</label>--%>
    <div class="col-sm-3">
        <asp:TextBox ID="txtTaluk" class="form-control" runat="server"></asp:TextBox>
     </div>
		
   
	</div>
		        
 </div>  

<div class="col-md-12">     
 <div class="form-group row">
  <div class="col-sm-2"></div>
   <div class="col-sm-3"></div>
  
     <div class="col-sm-1"></div>       
   <asp:Label ID="lblState" for="input-Default" class="col-sm-2 control-label" runat="server" Text="State"></asp:Label>            
    <%--<label for="input-Default" class="col-sm-2 control-label">State</label>--%>
    <div class="col-sm-3">
        <asp:TextBox ID="txtpsstate" class="form-control" runat="server" Text="TamilNadu"></asp:TextBox>
     </div>
		
   
	</div>
		        
 </div>
 
<div class="col-md-12"> 
						 <div class="form-group row">	
						  <asp:Label ID="lblPhn" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Phone"></asp:Label>
                         <%--<label for="input-Default" class="col-sm-2 control-label">Phone</label>--%>
						 <div class="col-sm-1">
                           <asp:TextBox ID="txtphone" runat="server" class="form-control" MaxLength="5" required Text="0"></asp:TextBox> 
                         </div>
                        <div class="col-sm-2">
                        <asp:TextBox ID="txtphone1" runat="server" class="form-control" MaxLength="7" Text="0"></asp:TextBox>
                            
                       </div>
                      
                       <div class="col-md-1"></div>
                        	
                    <asp:Label ID="lblMob" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Mobile"></asp:Label>
                          
                        <%-- <label for="input-Default" class="col-sm-2 control-label">Mobile</label>--%>
                      	 <div class="col-sm-1">
                      	 
                      	   <asp:TextBox ID="txtMobileCode" runat="server" class="form-control" MaxLength="3" required Text="0"></asp:TextBox>
                         </div>
                         <div class="col-sm-2">
                                                   
                           <asp:TextBox ID="txtmobile" runat="server" class="form-control" MaxLength="10" required Text="0"></asp:TextBox>
                                                   
                                            
                         </div>
						</div>
						</div> 
		
<div class="col-md-12">     
 <div class="form-group row">
  <asp:Label ID="lblppno" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Passport No"></asp:Label>
                    
 <%-- <label for="input-Default" class="col-sm-2 control-label">Passport No</label>--%>
   <div class="col-sm-3">
       <asp:TextBox ID="txtppno" class="form-control" runat="server"></asp:TextBox>
     </div>
  
     <div class="col-sm-1"></div>   
   <asp:Label ID="lblDirNo" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Driving License No"></asp:Label>               
    <%--<label for="input-Default" class="col-sm-2 control-label">Driving License No</label>--%>
    <div class="col-sm-3">
        <asp:TextBox ID="txtdrivingno" class="form-control" runat="server"></asp:TextBox>
     </div>
		
   
	</div>
		        
 </div>

<div class="col-md-12">
		
		<div class="form-group row">
					  <div align="center">
                          <asp:Label ID="lblEdu" runat="server" Text="EDUCATION DETAILS" Font-Bold="True" 
                              Font-Underline="True"></asp:Label>  
					  </div>
			       </div>
		
		</div>	
		 
<div class="col-md-12">     
 <div class="form-group row">
  <div class="col-sm-3"></div>
   <div class="col-sm-2">
       <%--<asp:RadioButton ID="RadioButton1" runat="server" Text="Educated" 
        oncheckedchanged="RadioButton1_CheckedChanged" AutoPostBack="true"/>--%>
   </div>   
   <div class="col-sm-3">
  <%--<asp:RadioButton ID="RadioButton2" runat="server" Text="Uneducated" 
        oncheckedchanged="RadioButton2_CheckedChanged" AutoPostBack="true"/>--%>
   
   <asp:RadioButtonList ID="rbtneducateduneducated" runat="server" RepeatColumns="2" AutoPostBack="true" 
           onselectedindexchanged="rbtneducateduneducated_SelectedIndexChanged">
          <asp:ListItem Text="Educated" Value="1" Selected="False"></asp:ListItem>
          <asp:ListItem Text="UnEducated" Value="2" Selected="True"></asp:ListItem>
       </asp:RadioButtonList>
      
   </div>
  </div>
		        
 </div>
		
<div class="col-md-12">     
 <div class="form-group row">
  <asp:Label ID="lblqualif" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Qualification"></asp:Label>               
  <%--<label for="input-Default" class="col-sm-2 control-label">Qualification</label>--%>
   <div class="col-sm-3">
       <asp:DropDownList ID="ddlqualification" class="form-control" runat="server">
       </asp:DropDownList>
    </div>
  
     <div class="col-sm-1"></div>   
      <asp:Label ID="lblunive" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Qualification"></asp:Label>                
   <%-- <label for="input-Default" class="col-sm-2 control-label">Qualification</label>--%>
    <div class="col-sm-3">
        <asp:TextBox ID="txtuniversity" class="form-control" runat="server"></asp:TextBox>
     </div>
	<div>
	<asp:Label ID="Label1" for="input-Default" class="col-sm-2 control-label" runat="server" Visible="false" Text="School/College"></asp:Label>
        <asp:TextBox ID="txtscholl" Visible="false" runat="server"></asp:TextBox>
	</div>	
   
	</div>
		        
 </div>  
 
 
 <div class="col-md-12">
		
		<div class="form-group row">
					  <div align="center">
                          <asp:Label ID="lblCmpDet" runat="server" Text="COMPANY DETAILS" Font-Bold="True" 
                              Font-Underline="True"></asp:Label>  
					  </div>
			       </div>
		
		</div>	 
<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblDept" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Department"></asp:Label>               

  <%--<label for="input-Default" class="col-sm-2 control-label">Department</label>--%>
   <div class="col-sm-3">
       <asp:DropDownList ID="ddldepartment" class="form-control" runat="server">
       </asp:DropDownList>
    </div>
  
     <div class="col-sm-1"></div>   
    <asp:Label ID="lblDesgn" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Designation"></asp:Label>               
    <%--<label for="input-Default" class="col-sm-2 control-label">Designation</label>--%>
    <div class="col-sm-3">
        <asp:TextBox ID="txtdesignation" class="form-control" runat="server"></asp:TextBox>
     </div>
		
   
	</div>
		        
 </div>  

<div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblemptype" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Employee Type"></asp:Label>
  <%--<label for="input-Default" class="col-sm-2 control-label">Employee Type</label>--%>
   <div class="col-sm-3">
       <asp:DropDownList ID="ddemployeetype" class="form-control" runat="server" 
           onselectedindexchanged="ddemployeetype_SelectedIndexChanged1">
       </asp:DropDownList>
    </div>
  
     <div class="col-sm-1"></div>   
     <asp:Label ID="lblexisiting" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Exisiting No"></asp:Label>               
    <%--<label for="input-Default" class="col-sm-2 control-label">Exisiting No</label>--%>
    <div class="col-sm-3">
        <asp:TextBox ID="txtexistiongno" class="form-control" runat="server"></asp:TextBox>
     </div>
		
   
	</div>
		        
 </div>
 
 <div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblMachID" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Machine ID"></asp:Label>
  <%--<label for="input-Default" class="col-sm-2 control-label">Machine ID</label>--%>
   <div class="col-sm-3">
    <asp:TextBox ID="txtOld" class="form-control" runat="server"></asp:TextBox>
    </div>
  	
   
	</div>
		        
 </div>
 
 <div class="col-md-12">     
 <div class="form-group row">
 <asp:Label ID="lblContract" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Contract Type"></asp:Label>
  <%--<label for="input-Default" class="col-sm-2 control-label">Machine ID</label>--%>
   <div class="col-sm-3">
   <asp:DropDownList ID="ddlContract" class="form-control" runat="server">
       </asp:DropDownList>
    </div>
  	
   
	</div>
		        
 </div>
 
  <div class="col-md-12">     
 <div class="form-group row">
  <asp:Label ID="lblnonpfgrade" for="input-Default" class="col-sm-2 control-label" runat="server" Text="On Role"></asp:Label>
  <%--<label for="input-Default" class="col-sm-2 control-label">On Role</label>--%>
   <div class="col-sm-3">
     <asp:CheckBox ID="chkboxnonpfgrade" runat="server" />
    </div>
   <div class="col-sm-1"></div>   
    <asp:Label ID="lblHostel" for="input-Default" class="col-sm-2 control-label" runat="server" Text="Hostel"></asp:Label>               
    <%--<label for="input-Default" class="col-sm-2 control-label">Hostel</label>--%>
    <div class="col-sm-3">
         <asp:CheckBox ID="chkHostel" runat="server" />
     </div>
	</div>
		        
 </div>
 
 <div class="col-md-12">     
				<div class="form-group row">
			<div class="col-sm-3">  
			</div>
				 <div class="col-sm-3">  
				     <asp:Button ID="btnsave" class="btn btn-success" runat="server" Text="Save" 
                         onclick="btnsave_Click" />    
				 </div>
	 				
  				 <div class="col-sm-3">
                     <asp:Button ID="btnclear" class="btn btn-success" runat="server" Text="Clear" 
                         onclick="btnclear_Click" />                             
                 </div>
  				 				
			    </div>
				       
        </div>
  </div>
  </form>

<%--<asp:RadioButton ID="RadioButton1" runat="server" Text="Educated" 
        oncheckedchanged="RadioButton1_CheckedChanged" AutoPostBack="true"/>
<asp:RadioButton ID="RadioButton2" runat="server" Text="Uneducated" 
        oncheckedchanged="RadioButton2_CheckedChanged" AutoPostBack="true"/>--%>
    </div>
    </div>
   
  <!-- Dashboard start -->
 	
 	<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                              <asp:Image ID="EmpPhoto" runat="server" />
                                <asp:FileUpload ID="fileUpload" runat="server" />
                                           
                                <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                                  
                                            </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
 
    <!-- Dashboard End -->
 
 
  </div>
  </div>
 </div>
    </ContentTemplate>
     <Triggers>
      <asp:PostBackTrigger ControlID="btnsave"  />
                                                   
      </Triggers>
    </asp:UpdatePanel>
</asp:Content>

