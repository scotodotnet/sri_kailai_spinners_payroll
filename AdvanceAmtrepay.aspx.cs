﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using System.Globalization;
using Payroll.Configuration;
using Payroll.Data;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class AdvanceAmtrepay : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    AdvanceAmount objsal = new AdvanceAmount();
    //bool searchFlag = false;
    string Datetime_ser;
    DateTime mydate;
    //string Mont;
    //int Monthid;
    string SessionAdmin;
    static string id = "";
    static string inc_Month;
    static string dec_month;
    //DateTime MyDate;
    string MyMonth;
    string IncrementMonths;
    string decrementMonths;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        //lblComany.Text = SessionCcode + " - " + SessionLcode;
       
        if (!IsPostBack)
        {
            SettingServerDate();
            //DropDownDepart();
            category();

        }
        //lblusername.Text = Session["Usernmdisplay"].ToString();
    }
    public void category()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public void SettingServerDate()
    {
        Datetime_ser = objdata.ServerDate();
        txtdate.Text = Datetime_ser;
    }
    public void DropDownDepart()
    {
        DataTable dt = new DataTable();
        dt = objdata.Depart_Labour();
        ddldepartment.DataSource = dt;
        ddldepartment.DataTextField = "DepartmentNm";
        ddldepartment.DataValueField = "DepartmentCd";
        ddldepartment.DataBind();
    }
    public void clear()
    {
        txtamt.Text = "";
        txtdate.Text = "";
        lblBalanceAmt1.Text = "";
        lbldesignation1.Text = "";
        id = "";
        SettingServerDate();
    }

    protected void txtamt_TextChanged(object sender, EventArgs e)
    {

    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        txtExist.Text = "";
        DataTable dtempty = new DataTable();
        ddlEmpNo.DataSource = dtempty;
        ddlEmpNo.DataBind();
        ddlEmpName.DataSource = dtempty;
        ddlEmpName.DataBind();
        clear();
        string stafforLabour = "";
        if (ddldepartment.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dtempcode = new DataTable();
            if (ddlcategory.SelectedValue == "1")
            {
                stafforLabour = "S";
            }
            else
            {
                stafforLabour = "L";
            }
            if (SessionAdmin == "1")
            {
                dtempcode = objdata.LoadEmployeeForBonus(stafforLabour, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
            }
            else
            {
                dtempcode = objdata.EmpLoadFORBonus_USER(stafforLabour, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
            }
            ddlEmpNo.DataSource = dtempcode;
            DataRow dr = dtempcode.NewRow();
            dr["EmpNo"] = ddldepartment.SelectedItem.Text;
            dr["EmpName"] = ddldepartment.SelectedItem.Text;
            dtempcode.Rows.InsertAt(dr, 0);

            ddlEmpNo.DataTextField = "EmpNo";
            ddlEmpNo.DataValueField = "EmpNo";
            ddlEmpNo.DataBind();
            ddlEmpName.DataSource = dtempcode;
            ddlEmpName.DataTextField = "EmpName";
            ddlEmpName.DataValueField = "EmpNo";
            ddlEmpName.DataBind();
        }
    }
    protected void ddlEmpNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtExist.Text = "";
        bool ErrFlag = false;
        clear();
        if (ddlEmpNo.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            dt = objdata.AdvanceSalary_EmpNo(ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                ddlEmpName.DataSource = dt;
                ddlEmpName.DataTextField = "EmpName";
                ddlEmpName.DataValueField = "EmpNo";
                ddlEmpName.DataBind();
                txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                lbldesignation1.Text = dt.Rows[0]["Designation"].ToString();
                DataTable dtrepay = new DataTable();
                dtrepay = objdata.repay_load(ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
                {
                    if (dtrepay.Rows.Count > 0)
                    {
                        lblBalanceAmt1.Text = dtrepay.Rows[0]["BalanceAmount"].ToString();
                        id = dtrepay.Rows[0]["ID"].ToString();
                        inc_Month = dtrepay.Rows[0]["IncreaseMonth"].ToString();
                        dec_month = dtrepay.Rows[0]["ReductionMonth"].ToString();
                        if (Convert.ToDecimal(lblBalanceAmt1.Text) < Convert.ToDecimal(dtrepay.Rows[0]["MonthlyDeduction"].ToString()))
                        {
                            txtamt.Text = lblBalanceAmt1.Text;
                        }
                        else
                        {
                            txtamt.Text = dtrepay.Rows[0]["MonthlyDeduction"].ToString();
                        }


                    }
                    else
                    {
                        txtExist.Text = "";
                        lbldesignation1.Text = "";
                        txtamt.Text = "";
                        id = "";
                        //lblAmt.Text = "";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Not Found....!');", true);
                        //System.Windows.Forms.MessageBox.Show("Employee Not Found....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }
                }
            }
        }
    }
    protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        txtExist.Text = "";
        clear();
        if (ddlEmpName.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Name....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Name....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            dt = objdata.AdvanceSalary_EmpNo(ddlEmpName.SelectedValue, SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                ddlEmpNo.DataSource = dt;
                ddlEmpNo.DataTextField = "EmpNo";
                ddlEmpNo.DataValueField = "EmpNo";
                ddlEmpNo.DataBind();
                txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                lbldesignation1.Text = dt.Rows[0]["Designation"].ToString();
                DataTable dtrepay = new DataTable();
                dtrepay = objdata.repay_load(ddlEmpName.SelectedValue, SessionCcode, SessionLcode);
                {
                    if (dtrepay.Rows.Count > 0)
                    {
                        lblBalanceAmt1.Text = dtrepay.Rows[0]["BalanceAmount"].ToString();
                        id = dtrepay.Rows[0]["ID"].ToString();
                        inc_Month = dtrepay.Rows[0]["IncreaseMonth"].ToString();
                        dec_month = dtrepay.Rows[0]["ReductionMonth"].ToString();
                        if (Convert.ToDecimal(lblBalanceAmt1.Text) < Convert.ToDecimal(dtrepay.Rows[0]["MonthlyDeduction"].ToString()))
                        {
                            txtamt.Text = lblBalanceAmt1.Text;
                        }
                        else
                        {
                            txtamt.Text = dtrepay.Rows[0]["MonthlyDeduction"].ToString();
                        }


                    }
                    else
                    {
                        txtExist.Text = "";
                        lbldesignation1.Text = "";
                        txtamt.Text = "";
                        id = "";
                        //lblAmt.Text = "";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Not Found....!');", true);
                        //System.Windows.Forms.MessageBox.Show("Employee Not Found....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }
                }
            }
        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        clear();

        if (txtExist.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Exist No....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Exist No....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            if (SessionAdmin == "1")
            {
                dt = objdata.AdvanceSalary_exist(txtExist.Text, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
            }
            else
            {
                dt = objdata.AdvanceSalary_exist_user(txtExist.Text, ddldepartment.SelectedValue, SessionCcode, SessionLcode);
            }
            if (dt.Rows.Count > 0)
            {
                txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                ddlEmpNo.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                ddlEmpName.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                lbldesignation1.Text = dt.Rows[0]["Designation"].ToString();
                DataTable dtrepay = new DataTable();
                dtrepay = objdata.repay_load(ddlEmpNo.SelectedValue, SessionCcode, SessionLcode);
                {
                    if (dtrepay.Rows.Count > 0)
                    {
                        lblBalanceAmt1.Text = dtrepay.Rows[0]["BalanceAmount"].ToString();
                        id = dtrepay.Rows[0]["ID"].ToString();
                        inc_Month = dtrepay.Rows[0]["IncreaseMonth"].ToString();
                        dec_month = dtrepay.Rows[0]["ReductionMonth"].ToString();
                        if (Convert.ToDecimal(lblBalanceAmt1.Text) < Convert.ToDecimal(dtrepay.Rows[0]["MonthlyDeduction"].ToString()))
                        {
                            txtamt.Text = lblBalanceAmt1.Text;
                        }
                        else
                        {
                            txtamt.Text = dtrepay.Rows[0]["MonthlyDeduction"].ToString();
                        }


                    }
                    else
                    {
                        txtExist.Text = "";
                        lbldesignation1.Text = "";
                        txtamt.Text = "";
                        id = "";
                        //lblAmt.Text = "";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Not Found....!');", true);
                        //System.Windows.Forms.MessageBox.Show("Employee Not Found....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Not Found....!');", true);
                //System.Windows.Forms.MessageBox.Show("Employee Not Found....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool SaveFlag = false;
            if (ddlEmpNo.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee No....!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee No....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (lblBalanceAmt1.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee No....!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee No....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtamt.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Amount Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Amount Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if (Convert.ToDecimal(lblBalanceAmt1.Text) < Convert.ToDecimal(txtamt.Text))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Amount Properly....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Amount Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                else if (Convert.ToDecimal(txtamt.Text) == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Amount Properly....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Amount Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                else if (dec_month == "1")
                {
                    if ((Convert.ToDecimal(txtamt.Text) != Convert.ToDecimal(lblBalanceAmt1.Text)))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Pay the full Amount....!');", true);
                        //System.Windows.Forms.MessageBox.Show("Pay the full Amount....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag = true;
                    }
                }
                if (!ErrFlag)
                {
                    string Completed = "";
                    mydate = DateTime.ParseExact(txtdate.Text, "dd-MM-yyyy", null);

                    int Mont = Convert.ToInt32(mydate.Month);
                    #region MonthName
                    DateTime Md = new DateTime();


                    switch (Mont)
                    {
                        case 1:
                            MyMonth = "January";
                            break;

                        case 2:
                            MyMonth = "February";
                            break;
                        case 3:
                            MyMonth = "March";
                            break;
                        case 4:
                            MyMonth = "April";
                            break;
                        case 5:
                            MyMonth = "May";
                            break;
                        case 6:
                            MyMonth = "June";
                            break;
                        case 7:
                            MyMonth = "July";
                            break;
                        case 8:
                            MyMonth = "August";
                            break;
                        case 9:
                            MyMonth = "September";
                            break;
                        case 10:
                            MyMonth = "October";
                            break;
                        case 11:
                            MyMonth = "November";
                            break;
                        case 12:
                            MyMonth = "December";
                            break;
                        default:
                            break;
                    }
                    #endregion


                    IncrementMonths = (Convert.ToInt32(inc_Month) + 1).ToString();
                    decrementMonths = (Convert.ToDecimal(dec_month) - 1).ToString();
                    string bal = (Convert.ToDecimal(lblBalanceAmt1.Text) - Convert.ToDecimal(txtamt.Text)).ToString();
                    if (Convert.ToDecimal(txtamt.Text) == Convert.ToDecimal(lblBalanceAmt1.Text))
                    {
                        Completed = "Y";
                        objdata.Advance_repayInsert(ddlEmpNo.SelectedValue, mydate, MyMonth, txtamt.Text, bal, id, SessionCcode, SessionLcode);
                        objdata.Advance_update(ddlEmpNo.SelectedValue, bal, IncrementMonths, decrementMonths, id, Completed, mydate, SessionCcode, SessionLcode);
                        SaveFlag = true;
                    }
                    else
                    {
                        Completed = "N";
                        objdata.Advance_repayInsert(ddlEmpNo.SelectedValue, mydate, MyMonth, txtamt.Text, bal, id, SessionCcode, SessionLcode);
                        objdata.Advance_update(ddlEmpNo.SelectedValue, bal, IncrementMonths, decrementMonths, id, Completed, mydate, SessionCcode, SessionLcode);
                        SaveFlag = true;
                    }
                }
                if (SaveFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Saved Successfully....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    clear();
                    DataTable dtempty = new DataTable();
                    ddlEmpNo.DataSource = dtempty;
                    ddlEmpNo.DataBind();
                    ddlEmpName.DataSource = dtempty;
                    ddlEmpName.DataBind();
                    txtExist.Text = "";
                    ddldepartment.SelectedValue = "0";
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Contact Admin....!');", true);
            //System.Windows.Forms.MessageBox.Show("Contact Admin....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        clear();
        txtExist.Text = "";
        DataTable dtempty = new DataTable();
        ddlEmpNo.DataSource = dtempty;
        ddlEmpNo.DataBind();
        ddlEmpName.DataSource = dtempty;
        ddlEmpName.DataBind();
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string staffLabour_temp;
        DataTable dtempty = new DataTable();
        ddldepartment.DataSource = dtempty;
        ddldepartment.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            staffLabour_temp = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            staffLabour_temp = "L";
        }
        else
        {
            staffLabour_temp = "0";
        }
        DataTable dtDip = new DataTable();
        dtDip = objdata.DropDownDepartment_category(staffLabour_temp, SessionCcode, SessionLcode);
        if (dtDip.Rows.Count > 1)
        {
            ddldepartment.DataSource = dtDip;
            ddldepartment.DataTextField = "DepartmentNm";
            ddldepartment.DataValueField = "DepartmentCd";
            ddldepartment.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldepartment.DataSource = dt;
            ddldepartment.DataBind();
        }
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
