﻿using Altius.BusinessAccessLayer.BALDataAccess;
using System;
using System.Data;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reeling : System.Web.UI.Page
{
  
    private Altius.BusinessAccessLayer.BALDataAccess.BALDataAccess objdata = new Altius.BusinessAccessLayer.BALDataAccess.BALDataAccess();
    private string SessionAdmin;
    private string SessionCcode;
    private string SessionLcode;
    private string SSQL = "";
   

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();



        if (!IsPostBack)
        {
            bin();
        }
    }
    public void bin()
    {
        DataTable table = new DataTable();
        this.SSQL = "select * from Reeling_Mst where compcode='" + this.SessionCcode + "' and Loccode='" + this.SessionLcode + "'";
        table = this.objdata.RptEmployeeMultipleDetails(this.SSQL);
        if (table.Rows.Count != 0)
        {
            this.txtSmall.Text = table.Rows[0]["Small"].ToString();
            this.txtMedium.Text = table.Rows[0]["Medium"].ToString();
            this.txtLarge.Text = table.Rows[0]["Large"].ToString();
            this.txtXL.Text = table.Rows[0]["XL"].ToString();
            this.txtXXL.Text = table.Rows[0]["XXL"].ToString();
        }
        else
        {
            this.txtLarge.Text = "0.0";
            this.txtMedium.Text = "0.0";
            this.txtSmall.Text = "0.0";
            this.txtXL.Text = "0.0";
            this.txtXXL.Text = "0.0";
        }

    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        base.Response.Redirect("AttenanceDownload.aspx");
    }

    protected void btncontract_Click(object sender, EventArgs e)
    {
        base.Response.Redirect("ContractRPT.aspx");
    }

    protected void btnEmp_Click(object sender, EventArgs e)
    {
        base.Response.Redirect("RptEmpDownload.aspx");
    }

    protected void btnLeave_Click(object sender, EventArgs e)
    {
        base.Response.Redirect("RptLeaveSample.aspx");
    }

    protected void btnOT_Click(object sender, EventArgs e)
    {
        base.Response.Redirect("OTDownload.aspx");
    }

    protected void btnSal_Click(object sender, EventArgs e)
    {
        base.Response.Redirect("FrmDeductionDownload.aspx");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool flag = false;
        if (this.txtLarge.Text == " ")
        {
            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Large %');", true);
            flag = true;
        }
        if (this.txtMedium.Text == " ")
        {
            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Medium %');", true);
            flag = true;
        }
        if (this.txtSmall.Text == " ")
        {
            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Small %');", true);
            flag = true;
        }
        if (this.txtXL.Text == " ")
        {
            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the XL %');", true);
            flag = true;
        }
        if (this.txtXXL.Text == " ")
        {
            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the XXL %');", true);
            flag = true;
        }
        if (!flag)
        {
            new DataTable();
            this.SSQL = "update Reeling_Mst set Small='" + this.txtSmall.Text + "',Medium='" + this.txtMedium.Text + "',Large='" + this.txtLarge.Text + "',";
            this.SSQL = this.SSQL + " XL='" + this.txtXL.Text + "',XXL='" + this.txtXXL.Text + "'";
            this.objdata.RptEmployeeMultipleDetails(this.SSQL);
            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Rate Saved');", true);
        }
    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
    }
}

