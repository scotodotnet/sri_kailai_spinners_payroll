﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data;

public partial class _Default : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    BALDataAccess objdata = new BALDataAccess();
    string SessionSpay;
    string SessionEpay;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Remove("UserId");
        Session["SessionSpay"] = "SKS_Spay";
        Session["SessionEpay"] = "SKS_Epay";
        if (!IsPostBack)
        {
            Load_company();
            if (ddlCode.Items.Count != 0)
            {
                ddlCode.SelectedIndex = 1;
                ddlCode_SelectedIndexChanged(sender, e);
                //ddlLocation.SelectedValue = "UNIT I".ToString();
                Module_Login_Table(sender, e);
            }
        }
    }

    public void Load_company()
    {
        DataTable dt = new DataTable();
        string Query = "Select Distinct CompCode,CompCode + ' - ' + CompName as Cname from [SKS_Spay]..Company_Mst";

        dt = objdata.RptEmployeeMultipleDetails(Query);
        ddlCode.DataSource = dt;
        //DataRow dr = dt.NewRow();
        //dr["Ccode"] = "Company Name";
        //dr["Cname"] = "Company Name";
        //dt.Rows.InsertAt(dr, 0);
        ddlCode.DataTextField = "Cname";
        ddlCode.DataValueField = "CompCode";
        ddlCode.DataBind();
          
    }
    public void Load_Location()
    {
        DataTable dt = new DataTable();
        string Query = "select * from [SKS_Spay]..Location_Mst";

        dt = objdata.RptEmployeeMultipleDetails(Query);
        ddlLocation.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["LocCode"] = "Location Name";
        dr["LocName"] = "Location Name";
        dt.Rows.InsertAt(dr, 0);

        ddlLocation.DataTextField = "LocCode";
        ddlLocation.DataValueField = "LocCode";
        ddlLocation.DataBind();

    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        Session["SessionSpay"] = "SKS_Spay";
        Session["SessionEpay"] = "SKS_Epay";
        bool ErrFlag = false;
        string Company_Code = ddlCode.SelectedItem.Text;
        string[] Company = Company_Code.Split('-');

        string Verification = objdata.Verification_verify();
        UserRegistrationClass objuser = new UserRegistrationClass();
        if (txtusername.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the User Name ');", true);
            ErrFlag = true;
        }
        else if (txtpassword.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Password ');", true);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            DataTable dt_v = new DataTable();
            if (txtusername.Text.Trim().ToUpper().ToString() == "Scoto".ToUpper().ToString())
            {
                Session["UserId"] = txtusername.Text.Trim();
                objuser.UserCode = txtusername.Text.Trim();
                objuser.Password = UTF8Encryption(txtpassword.Text.Trim()).ToString();
                string pwd = UTF8Encryption(txtpassword.Text.Trim()).ToString();
                DataTable dt = new DataTable();
                string query = "Select UserCode,UserName,Password,IsAdmin,LocationCode,CompCode,IsAdmin from [HR_Rights]..MstUsers where UserCode='" + txtusername.Text + "' and Password='" + pwd + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                //dt = objdata.AltiusLogin(objuser);
                if (dt.Rows.Count > 0)
                {
                    if ((dt.Rows[0]["UserCode"].ToString().Trim().ToUpper().ToString() == txtusername.Text.Trim().ToUpper().ToString()) && (dt.Rows[0]["Password"].ToString().Trim() == pwd))
                    {
                        Session["Isadmin"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                        Session["Usernmdisplay"] = txtusername.Text.Trim();
                        Session["Ccode"] = ddlCode.SelectedValue;
                        Session["Lcode"] = ddlLocation.SelectedValue;
                        Session["CompanyName"] = Company[1];
                        if (Session["Isadmin"].ToString() == "1")
                        {
                            Session["RoleCode"] = "1";
                        }
                        else
                        {
                            Session["RoleCode"] = "2";
                        }
                        Response.Redirect("Dash_Board.aspx");
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                    }
                }
            }
            else
            {
                string date_1 = "";
                dt_v = objdata.Value_Verify();
                //if (dt_v.Rows.Count > 0)
                //{
                //    date_1 = (dt_v.Rows[0]["No_dVal"].ToString() + "-" + dt_v.Rows[0]["No_MVal"].ToString() + "-" + dt_v.Rows[0]["No_YVal"].ToString()).ToString();
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Update your Licence Key');", true);
                //    return;
                //}
                date_1 = ("22".ToString() + "-" + "10".ToString() + "-" + "2020".ToString()).ToString();
                if (Verification.Trim() != "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Login Faild Contact Scoto..');", true);
                    return;
                }
                string dtserver = objdata.ServerDate();
                if (Convert.ToDateTime(dtserver) < Convert.ToDateTime(date_1))
                {
                    if (txtusername.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the User Name ');", true);
                        ErrFlag = true;
                    }
                    else if (txtpassword.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Password ');", true);
                        ErrFlag = true;
                    }
                    else if (ddlLocation.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Location ');", true);
                        ErrFlag = true;
                    }
                    if (!ErrFlag)
                    {
                        Session["UserId"] = txtusername.Text.Trim();
                        objuser.UserCode = txtusername.Text.Trim();
                        objuser.Password = UTF8Encryption(txtpassword.Text.Trim()).ToString();
                        string pwd = UTF8Encryption(txtpassword.Text.Trim()).ToString();
                        objuser.Ccode = ddlCode.SelectedValue;
                        objuser.Lcode = ddlLocation.SelectedValue;
                        DataTable dt = new DataTable();
                        string ssql = "Select UserCode,UserName,Password,IsAdmin,LocationCode,CompCode,IsAdmin from [HR_Rights]..MstUsers where";
                        ssql = ssql + " UserCode='" + objuser.UserCode + "' and Password='" + objuser.Password + "' and CompCode='" + objuser.Ccode + "'";
                        ssql = ssql + " and LocationCode='" + objuser.Lcode + "'";
                        dt = objdata.RptEmployeeMultipleDetails(ssql);
                        //dt = objdata.UserLogin(objuser);
                        if (dt.Rows.Count > 0)
                        {
                            if ((dt.Rows[0]["UserCode"].ToString().Trim() == txtusername.Text.Trim()) && (dt.Rows[0]["Password"].ToString().Trim() == pwd) && (dt.Rows[0]["CompCode"].ToString().Trim() == ddlCode.SelectedValue) && (dt.Rows[0]["LocationCode"].ToString().Trim() == ddlLocation.SelectedValue))
                            {

                                Session["Isadmin"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                                Session["Usernmdisplay"] = txtusername.Text.Trim();
                                Session["Ccode"] = dt.Rows[0]["CompCode"].ToString().Trim();
                                Session["Lcode"] = dt.Rows[0]["LocationCode"].ToString().Trim();
                                Session["CompanyName"] = Company[1];
                                if (Session["Isadmin"].ToString() == "1")
                                {
                                    Session["RoleCode"] = "1";
                                }
                                else
                                {
                                    Session["RoleCode"] = "2";
                                }
                                Response.Redirect("Dash_Board.aspx");
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                        }
                    }
                }
                else
                {
                    objdata.Insert_verificationValue();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Login Faild Contact Scoto..');", true);
                }

                //Session["UserId"] = txtusername.Text;
                //objuser.UserCode = txtusername.Text;
                //objuser.Password = s_hex_md5(txtpassword.Text);
                //objuser.Ccode = ddlCode.SelectedValue.Trim();
                //objuser.Lcode = ddlLocation.SelectedValue.Trim();
                //string pwd = s_hex_md5(txtpassword.Text);
                //string UserName = objdata.username(objuser);
                //string password = objdata.UserPassword(objuser);

                //if (UserName == txtusername.Text && password == pwd)
                //{

                //    string UserNameDis = objdata.usernameDisplay(objuser);
                //    string isadmin = objdata.isAdmin(txtusername.Text);
                //    string see = isadmin;
                //    int dd = Convert.ToInt32(see);
                //    Session["Isadmin"] = Convert.ToString(dd);
                //    Session["Usernmdisplay"] = UserNameDis;
                //    if (see.Trim() == "3")
                //    {
                //        Response.Redirect("Administrator.aspx");
                //    }
                //    else
                //    {
                //        if (UserName == "Admin")
                //        {
                //            Session["RoleCode"] = "1";
                //        }
                //        else
                //        {
                //            Session["RoleCode"] = "2";
                //        }
                //        Response.Redirect("EmployeeRegistration.aspx");
                //    }
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                //}
            }
        }

        //if (isadmin = "1")
        //{

        //}



    }

    public static String s_hex_md5(String originalPassword)
    {
        UTF8Encoding encoder = new UTF8Encoding();
        MD5 md5 = new MD5CryptoServiceProvider();

        Byte[] hashedbytes = md5.ComputeHash(encoder.GetBytes(originalPassword));
        return BitConverter.ToString(hashedbytes).Replace("-", "").ToLower();
    }
    protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
    {


    }
    protected void ddlCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Location();
    }
    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    private void Module_Login_Table(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Module = new DataTable();
        query = "Select * from [HR_Rights]..Module_Open_User";
        DT_Module = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Module.Rows.Count != 0)
        {
            ddlCode.SelectedValue = DT_Module.Rows[0]["CompCode"].ToString();
            string Loc_Code_str = DT_Module.Rows[0]["LocCode"].ToString().Trim();
            ddlLocation.SelectedValue = Loc_Code_str;
            txtusername.Text = DT_Module.Rows[0]["UserName"].ToString();
            txtpassword.Text = UTF8Decryption(DT_Module.Rows[0]["Password"].ToString()).ToString();

            query = "Delete from [HR_Rights]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);

            btnsave_Click(sender, e);

        }
    }


}
