﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RptDepartmentSalaryStrength.aspx.cs" Inherits="RptDepartmentSalaryStrength" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


 <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>

          <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">EL/CL REPORT DETAILS</li></h4> 
                    </ol>
        </div>
          
          
          <div id="main-wrapper" class="container">
            <div class="row">
               <div class="col-md-12">
             <div class="col-md-9">
			  <div class="panel panel-white">
			
			
			    <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">EL/CL REPORT DETAILS</h4>
				</div>
			   </div>
				<form class="form-horizontal">
				   <div align="center">
				        <div class="form-group>
					       <asp:Label ID="Label1" for="input-Default" class="col-sm-12 control-label" 
                                Text="EL/CL REPORT DETAILS" runat="server" Font-Bold="True" Font-Underline="True" Font-Size=Large></asp:Label>
				        
				        </div>
				   </div>
				
				   <div class="panel-body">
                        
                   <div class="row">
					    <div class="form-group col-md-12">
					      
					      <div class="form-group row">
					       <asp:Label ID="Label3" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
					   	<div class="col-sm-3">
				           <asp:DropDownList ID="ddlcategory" runat="server" AutoPostBack="true" class="form-control"
                            onselectedindexchanged="ddlcategory_SelectedIndexChanged">
                           </asp:DropDownList>
                                                  
                        </div>
					
					     <div class="col-sm-1"></div>
				       <asp:Label ID="lblEmployeeType" runat="server" Text="Employee Type" class="col-sm-2 control-label"></asp:Label>
				      			     
				        <div class="col-sm-3">
						       <asp:DropDownList ID="txtEmployeeType" runat="server" AutoPostBack="true" class="form-control">
                              </asp:DropDownList>
                          </div>
			       </div>
					    
					      <div class="form-group row">
					 
					          <asp:Label ID="lblFinancialYear" runat="server" Text="Financial Year" class="col-sm-2 control-label"></asp:Label>
					 
                              <div class="col-sm-3">
                                   <asp:DropDownList ID="txtFinancial_Year" runat="server"  AutoPostBack="true" class="form-control">
                                   </asp:DropDownList>                            
				               </div>
				               
				              <div class="col-sm-1"></div>
				               <asp:Label ID="lblEligibleDays" runat="server" Text="Eligibility Worked Days" class="col-sm-2 control-label"></asp:Label>
					          <div class="col-sm-3">
					          
					           <asp:TextBox ID="txtEligbleDays" runat="server" Text="0" class="form-control"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR4" runat="server" FilterMode="ValidChars"
                                 FilterType="Numbers,Custom" TargetControlID="txtEligbleDays" ValidChars=""></cc1:FilteredTextBoxExtender>                        
                              </div>
                             
                              
					      </div>
					      
					      <div class="form-group row">
						      <div class="col-sm-2">
						        <asp:CheckBox ID="ChkLeft" runat="server" Text="Left Employee"/>
                             </div>
                             
                             <div class="col-sm-4">
                             <asp:Label ID="lblLeftDate" runat="server" Text="LeftDate" class="col-sm-2 control-label" ></asp:Label>
                             </div>
                             
                           
                              <div class="col-sm-2">
                             
                               <asp:TextBox ID="txtLeftDate" runat="server" AutoPostBack="True" class="form-control"
                                  ontextchanged="txtLeftDate_TextChanged"></asp:TextBox>
                               </div>   
                                  <div class="col-sm-2">
                                     <asp:CheckBox ID="ChkBelow" runat="server" Text="Below Employee"/>   
                                  </div>
                             
                          </div>
                             
                            
                            <div class="form-group row">
                             <asp:Label ID="lblpftype" runat="server" Text="PF / Non PF" class="col-sm-2 control-label" ></asp:Label>                
						      <div class="col-sm-3">
                               <asp:RadioButtonList ID="RdbPFNonPF" runat="server" RepeatColumns="3" >
                                       <asp:ListItem Selected="true" Text="ALL" Value="0"></asp:ListItem>                          
                                       <asp:ListItem Selected="False" Text="PF" Value="1"></asp:ListItem>                         
                                        <asp:ListItem Selected="False" Text="Non PF" Value="2"></asp:ListItem>                         
                               </asp:RadioButtonList>
                              </div>
                              <div class="col-sm-1"></div>
                             
				         
                              
                                
					             <asp:Label ID="lblPayslipFormat" runat="server" Text="Report Type" class="col-sm-2 control-label" ></asp:Label>            
                                   <div class="col-sm-3">       
                                      <asp:RadioButtonList ID="rdbPayslipFormat" runat="server" RepeatColumns="3"> 
                                        <asp:ListItem Selected="true" Text="Consolidate" Value="1"></asp:ListItem>                               
                                        <asp:ListItem Selected="False" Text="Cover" Value="2"></asp:ListItem>                               
                                        <asp:ListItem Selected="False" Text="Signlist" Value="3"></asp:ListItem>                               
                                     </asp:RadioButtonList>                                         
                                                                           
					              </div> 
					              
					               
					               
					              
					              
                              </div>
                              
                              
                                <div class="form-group row">
                                <div align="center">
                                  <asp:Button ID="btnELView" runat="server" Text="EL / CL View"  
                                                                onclick="btnELView_Click" class="btn btn-success"/>
                                   </div>
                                </div>
                              
                              
                        
			              <div align="center">
			                     <asp:Label ID="lblFrom_Month" runat="server" Text="Month" class="col-sm-2 control-label" visible="false"></asp:Label>
			                      <div class="col-sm-3">
			                         <asp:DropDownList ID="txtFrom_Month" runat="server" class="form-control"  
                                        AutoPostBack="true" visible="false">
                                     </asp:DropDownList>
			                     </div>
			                     
			                       <div class="col-sm-1"></div>
			                       <div class="col-sm-3">
			                      <asp:Button ID="btnClick" runat="server" Text="Search" onclick="btnClick_Click"  visible="false"/>
			               </div>
			               </div>
			               
			               
			                
			               
			              <div class="form-group row">
			                 <asp:Label ID="lblformat" runat="server" Text="Format" class="col-sm-2 control-label"  visible="false"></asp:Label>
			               
			               <div class="col-sm-3">
			                 <asp:DropDownList ID="ddlrptReport" runat="server" visible="false">
                               <asp:ListItem Text="--Select---" Value="0"></asp:ListItem>
                               <asp:ListItem Text="Excel" Value="1"></asp:ListItem>
                               <asp:ListItem Text="PDF" Value ="2"></asp:ListItem>
                               <asp:ListItem Text="Word" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                            </div>
			                <div class="col-sm-1"></div>
			                
			               <div class="col-sm-3">
			                    <asp:Button ID="btnexport" runat="server" Text="Export" onclick="btnexport_Click" visible="false"/>
 			               </div>
			               
			               
			               
			               </div>
					     
                     </div>
                  </div>
   
                </div>
			  
			
			
	  
		    </form>
		</div>
	</div>
 	           
 	            <!-- Dashboard start -->
 	
             	<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
              <!-- Dashboard End --> 
 
  
          
   
     
            <div class="row">
               <asp:Panel ID="Result_Panel" Visible="false" runat="server">
				                                <table class="full">
				                                <tbody>
				                                    <tr>
                                                        <td colspan="4">
                                                            <asp:GridView ID="griddept" runat="server" AutoGenerateColumns="false" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblempno" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>OLD ID</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbloldid" runat="server" Text='<%# Eval("OldID")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Exist.Code</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblExisistingCode" runat="server" Text='<%# Eval("ExisistingCode")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp. Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblempname" runat="server" Text= '<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Date of Joining</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldoj" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <%--<asp:TemplateField>
                                                                        <HeaderTemplate>De-Activate Date</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDeActivateDate" runat="server" Text='<%# Eval("deactivedate") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>--%>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Date of Birth</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldob" runat="server" Text='<%# Eval("DOB") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Designation</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField> 
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp. Type</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpType" runat="server" Text='<%# Eval("EmpType") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ProfileType</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblProfileType" runat="server" Text='<%# Eval("ProfileType")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SalaryThrough</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSalaryThrough" runat="server" Text='<%# Eval("Salarythrough")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESINumber</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESICnumber" runat="server" Text='<%# Eval("ESICnumber")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PFNumber</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFnumber" runat="server" Text='<%# Eval("PFnumber")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>WagesType</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblWagestype" runat="server" Text='<%# Eval("Wagestype")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblBase_Salary" runat="server" Text='<%# Eval("Base_Salary")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>OT Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblOT" runat="server" Text='<%# Eval("OTSalary")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                </Columns>
                                                            </asp:GridView>
                                                            <asp:GridView ID="GridViewdeactiveded" runat="server" AutoGenerateColumns="false" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblempno" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>OLD ID</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbloldid" runat="server" Text='<%# Eval("OldID")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Exist.Code</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblExisistingCode" runat="server" Text='<%# Eval("ExisistingCode")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp. Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblempname" runat="server" Text= '<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Date of Joining</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldoj" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>De-Activate Date</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDeActivateDate" runat="server" Text='<%# Eval("deactivedate") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Date of Birth</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldob" runat="server" Text='<%# Eval("DOB") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Designation</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField> 
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp. Type</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpType" runat="server" Text='<%# Eval("EmpType") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ProfileType</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblProfileType" runat="server" Text='<%# Eval("ProfileType")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SalaryThrough</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSalaryThrough" runat="server" Text='<%# Eval("Salarythrough")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESINumber</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESICnumber" runat="server" Text='<%# Eval("ESICnumber")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PFNumber</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFnumber" runat="server" Text='<%# Eval("PFnumber")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>WagesType</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblWagestype" runat="server" Text='<%# Eval("Wagestype")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblBase_Salary" runat="server" Text='<%# Eval("Base_Salary")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>OT Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblOT" runat="server" Text='<%# Eval("OTSalary")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
				                                </tbody>
				                            </table>
				                            </asp:Panel>
            </div>
     
        </div><!-- col 12 end -->
         </div><!-- row end -->
        </div>
      
        
          </ContentTemplate>
          
       <Triggers>
          <asp:PostBackTrigger ControlID="btnexport"/>
      </Triggers>
      
       </asp:UpdatePanel>


</asp:Content>

