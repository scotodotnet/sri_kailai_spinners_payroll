﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Reeling_Calculation.aspx.cs" Inherits="Reeling_Calculation" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tablePFandESI').dataTable();               
            }
        }); 
    };
    </script>
      
             
     <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
        $('#tablePFandESI').dataTable();
        });
    </script>






    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                         <ContentTemplate>

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Reeling Calculation</li>
                   </ol>
              </div>
 
 
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Reeling Calculation</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					  <div class="form-group row">
					    
					       <label for="input-Default" class="col-sm-2 control-label" for="input-Default">Deptment</label>
						<div class="col-sm-4">
                            <asp:TextBox ID="txtDept" class="form-control" runat="server" Enabled="false"></asp:TextBox>
                        </div>
					    
					   
						<label for="input-Default" class="col-sm-2 control-label" for="input-Default">MachineID</label>
						<div class="col-sm-4">
						<asp:DropDownList ID="ddlempno" class="form-control" onselectedindexchanged="ddlempno_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            </asp:DropDownList> 
					  </div>
				        </div>  
				          <div class="form-group row">
					    
					       <label for="input-Default" class="col-sm-2 control-label" for="input-Default">Name</label>
						<div class="col-sm-4">
                            <asp:TextBox ID="txtname" class="form-control" runat="server" Enabled="false"></asp:TextBox>
                        </div>
					    
					   
						<label for="input-Default" class="col-sm-2 control-label" for="input-Default">ExistingCode</label>
						<div class="col-sm-4">
						 <asp:TextBox ID="txtexistingcode" class="form-control" runat="server" Enabled="false"></asp:TextBox>
                         
						</div>
				        </div>  
				          <div class="form-group row">
					    
					       <label for="input-Default" class="col-sm-2 control-label" for="input-Default">Month</label>
						<div class="col-sm-4">
                             <asp:DropDownList ID="ddlMonths" runat="server" class="form-control">
                           </asp:DropDownList>
                        </div>
					   
						 
                          <asp:Label ID="lblfinance" runat="server" class="col-sm-2 control-label"  Text="Financial Year" ></asp:Label>
                   
                          <div class="col-sm-4">
                           <asp:DropDownList ID="ddlFinance" runat="server" AutoPostBack="true" class="form-control">
                           </asp:DropDownList>
                        </div>
                        </div>
				          
                        
                           <div class="form-group row">
					  <asp:Label ID="Label2" runat="server" class="col-sm-2 control-label" 
                                                             Text="From Date"></asp:Label>
						<div class="col-sm-4">
						
                            <asp:TextBox ID="txtFromDate" runat="server"  class="form-control"></asp:TextBox>
						   <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromDate" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true">
                        </cc1:CalendarExtender>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR1" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtFromDate" ValidChars="0123456789/- ">
                        </cc1:FilteredTextBoxExtender>
				                                            
                        </div>
					
					
				    <asp:Label ID="Label4" runat="server" class="col-sm-2 control-label"  Text="To Date" ></asp:Label>		    
				    <div class="col-sm-4">   				    
				   
                        <asp:TextBox ID="txtToDate" runat="server"  class="form-control" 
                            ontextchanged="txtToDate_TextChanged"></asp:TextBox>
                        
                           <cc1:CalendarExtender ID="Caleder_txtdob" runat="server" TargetControlID="txtToDate" Format ="dd-MM-yyyy" CssClass ="orange" Enabled="true">
                        </cc1:CalendarExtender>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR2" runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtToDate" ValidChars="0123456789/- ">
                        </cc1:FilteredTextBoxExtender>
                    <asp:TextBox ID="txtdays" Visible="false"  runat="server"  class="form-control" ></asp:TextBox>
                            
					  </div>
			       </div>
                      
					
					    <div class="form-group row">
					    
					       <label for="input-Default" class="col-sm-2 control-label" for="input-Default">Small</label>
						<div class="col-sm-4">
                            <asp:TextBox ID="txtSmall"  Text="0.00" class="form-control" runat="server" required></asp:TextBox>
                        </div>
					    
					   
						<label for="input-Default" class="col-sm-2 control-label" for="input-Default">Medium</label>
						<div class="col-sm-4">
						 <asp:TextBox ID="txtMedium" Text="0.00" class="form-control" runat="server" required></asp:TextBox>
                         
						</div>
				        </div>  
				       
				        <div class="form-group row">
					    
					       <label for="input-Default" class="col-sm-2 control-label" for="input-Default">Large</label>
						<div class="col-sm-4">
                            <asp:TextBox ID="txtLarge" Text="0.00" class="form-control" runat="server" required></asp:TextBox>
                        </div>
					    
					   	
						<label for="input-Default" class="col-sm-2 control-label" for="input-Default">XL</label>
						<div class="col-sm-4">
						 <asp:TextBox ID="txtXL"  Text="0.00" class="form-control" runat="server" required></asp:TextBox>
                         
						</div>
				        </div>
				        
				        <div class="form-group row">
					    
					     <label for="input-Default" class="col-sm-2 control-label" for="input-Default">XXL </label>
						<div class="col-sm-4">
                            <asp:TextBox ID="txtXXL" Text="0.00" class="form-control" runat="server" required></asp:TextBox>

                        </div>
					    
					    <label for="input-Default" class="col-sm-2 control-label" for="input-Default">Days </label>
						<div class="col-sm-4">
                            <asp:TextBox ID="txtWdays" class="form-control" Text="26" runat="server" required></asp:TextBox>

                        </div>
					  
						
				        </div>
				        <div class="form-group row">
				          <label for="input-Default" class="col-sm-2 control-label" for="input-Default">Advance</label>
						<div class="col-sm-4">
                            <asp:TextBox ID="txtAdvance" Text="0.00" class="form-control" runat="server" required></asp:TextBox>

                        </div>
				        </div>
				       
						<!-- Button start -->
						<div class="form-group row">
						</div>
                        <div class="txtcenter">
                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                onclick="btnSave_Click" />
                                     
                                                       
                   <asp:LinkButton ID="btnclr" class="btn btn-danger" runat="server" 
                                onclick="btnclr_Click" >Cancel</asp:LinkButton>      
                    </div>
                    <!-- Button End -->	
						
						
				        
				        <div class="form-group row">
						</div>
			
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 <!-- Dashboard start -->
 <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                            
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
 
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

