﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BonusProcess.aspx.cs" Inherits="BonusProcess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Bonus Process</li></h4> 
                    </ol>
             </div>

<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
<div class="col-md-9">
<div class="panel panel-white">
<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Advance Repayment</h4>
				</div>
				</div>
<form class="form-horizontal">
<div class="panel-body">
<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblStaff" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
				   
					   <div class="col-sm-3">
					      <asp:DropDownList ID="ddlcategory" class="form-control" runat="server" AutoPostBack="True" 
					      onselectedindexchanged="ddlcategory_SelectedIndexChanged">
                            </asp:DropDownList>  
					   </div>
				       <div class="col-md-1"></div>
				       <asp:Label ID="lblEmployeeType" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee Type"></asp:Label>
                	  <div class="col-sm-3">
						<asp:DropDownList ID="txtEmployeeType" class="form-control" runat="server" AutoPostBack="True" 
						onselectedindexchanged="txtEmployeeType_SelectedIndexChanged">
                        </asp:DropDownList> 
					  </div>
						
					   </div>
				        
                       </div>

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblFinYear" runat="server" class="col-sm-2 control-label" 
                                                             Text="Bonus Year"></asp:Label>
				   
					   <div class="col-sm-3">
					      <asp:DropDownList ID="ddlFinance" class="form-control" runat="server" AutoPostBack="True"> 
					      </asp:DropDownList>  
					   </div>
				       <div class="col-md-1"></div>
				     
                	  <div class="col-sm-3">
                          <asp:Button ID="btnBonusProcess" class="btn btn-success" runat="server" Text="Bonus Process" 
                          onclick="btnBonusProcess_Click"/>
					  </div>
						
					   </div>
				        
                       </div>
 
 <div class="col-md-12">     
				       <div class="form-group row">
				     <div align="center">
                         <asp:Label ID="Label1" runat="server" Text="BONUS REPORT" Font-Bold="true" Font-Underline="true"></asp:Label>
				     </div>
					   </div>
				        
                       </div>

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblcategory" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
				   
					   <div class="col-sm-3">
					      <asp:DropDownList ID="ddlcategory_Rpt" class="form-control" runat="server" AutoPostBack="True" 
					      onselectedindexchanged="ddlcategory_Rpt_SelectedIndexChanged">
                            </asp:DropDownList>  
					   </div>
				       <div class="col-md-1"></div>
				       <asp:Label ID="Label3" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee Type"></asp:Label>
                	  <div class="col-sm-3">
						<asp:DropDownList ID="txtEmployeeType_Rpt" class="form-control" runat="server" AutoPostBack="true">
                        </asp:DropDownList> 
					  </div>
						
					   </div>
				        
                       </div>

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="Label2" runat="server" class="col-sm-2 control-label" 
                                                             Text="Bonus Year"></asp:Label>
				   
					   <div class="col-sm-3">
					      <asp:DropDownList ID="txtBonusYear" class="form-control" runat="server" AutoPostBack="True">
                          </asp:DropDownList>  
					   </div>
				       <div class="col-md-1"></div>
				       <asp:Label ID="lblSalaryThrough" runat="server" class="col-sm-2 control-label" 
                                                             Text="SalaryThrough"></asp:Label>
                	  <div class="col-sm-3">
						<asp:RadioButtonList ID="RdbCashBank" runat="server" RepeatColumns="3" 
                                                TabIndex="3" Width="180">
                         <asp:ListItem Selected="true" Text="ALL" Value="0"></asp:ListItem>
                         <asp:ListItem Selected="False" Text="Cash" Value="1"></asp:ListItem>
                         <asp:ListItem Selected="False" Text="Bank" Value="2"></asp:ListItem>
                       </asp:RadioButtonList>
					  </div>
						
					   </div>
				        
                       </div>

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblpftype" runat="server" class="col-sm-2 control-label" 
                                                             Text="PF / Non PF"></asp:Label>
				   
					   <div class="col-sm-3">
					    <asp:RadioButtonList ID="RdbPFNonPF" runat="server" RepeatColumns="3" 
                                                TabIndex="3" Width="180">
                           <asp:ListItem Selected="true" Text="ALL" Value="0"></asp:ListItem>
                           <asp:ListItem Selected="False" Text="PF" Value="1"></asp:ListItem>
                           <asp:ListItem Selected="False" Text="Non PF" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
					   </div>
				       <div class="col-md-1"></div>
				       <asp:Label ID="Label5" runat="server" class="col-sm-2 control-label" 
                                                             Text="Active Mode"></asp:Label>
                	  <div class="col-sm-3">
						<asp:RadioButtonList ID="RdbLeftType" runat="server" RepeatColumns="3" 
                                       TabIndex="3" Width="180">
                        <asp:ListItem Selected="true" Text="ALL" Value="1"></asp:ListItem>
                        <asp:ListItem Selected="False" Text="OnRoll" Value="2"></asp:ListItem>
                        <asp:ListItem Selected="False" Text="Left" Value="3"></asp:ListItem>
                        </asp:RadioButtonList>
					  </div>
						
					   </div>
				        
                       </div>

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblPayslipFormat" runat="server" class="col-sm-2 control-label" 
                                                             Text="Report Type"></asp:Label>
				   
					   <div class="col-sm-10">
					   <asp:RadioButtonList ID="rdbPayslipFormat" runat="server" RepeatColumns="6" 
                           TabIndex="3">
                        <asp:ListItem Selected="true" Text="Individual" Value="1"></asp:ListItem>
                        <asp:ListItem Selected="False" Text="Recociliation" Value="2"></asp:ListItem>
                        <asp:ListItem Selected="False" Text="Checklist" Value="3"></asp:ListItem>
                        <asp:ListItem Selected="False" Text="Signlist" Value="4"></asp:ListItem>
                        <asp:ListItem Selected="False" Text="Blank Signlist" Value="5"></asp:ListItem>
                        <asp:ListItem Selected="False" Text="Cover" Value="6"></asp:ListItem>
                       </asp:RadioButtonList>
					  </div>
						
					   </div>
				        
                       </div>

<div class="col-md-12">     
<div class="form-group row">
				       <div class="col-sm-2"></div>
				   
					   <div class="col-sm-3">
                           <asp:Button ID="btnBonusView" class="btn btn-success" runat="server" Text="Report View" OnClick="btnBonusView_Click" />
					   </div>
				       <div class="col-md-1"></div>
				       
                	  <div class="col-sm-3">
						<asp:Button ID="btnBonusExcelView" class="btn btn-success" runat="server" Text="Excel View" OnClick="btnBonusExcelView_Click" />
					   </div>
						
					   </div>
				        
                       </div>

</div>
</form>
</div>
</div>

<!-- Dashboard start -->
 	
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
    <!-- Dashboard End --> 
<div class="col-md-12">
<table>
<tbody>
<tr id="GridView" runat="server" visible="false">
 <td colspan="5">
 <asp:GridView ID="GridExcelView" runat="server" AutoGenerateColumns="true">
 </asp:GridView>
 </td>
 </tr>    
</tbody>
</table>
</div>
</div> <!-- col-md-12 End --> 
</div> <!-- Row End --> 
</div> 




</ContentTemplate>
<Triggers>
      <asp:PostBackTrigger ControlID="btnBonusView"  />
 </Triggers>
<Triggers>
      <asp:PostBackTrigger ControlID="btnBonusExcelView"  />
 </Triggers>


</asp:UpdatePanel>
</asp:Content>

