﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RptResign.aspx.cs" Inherits="RptResign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Resignation</li></h4> 
                    </ol>
               </div>
<div id="main-wrapper" class="container">
<div class="row">
<div class="col-md-12">
              
 <div class="col-md-9">
<div class="panel panel-white">
<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">EMPLOYEE RELIVED DETAILS</h4>
				</div>
				</div>
<form class="form-horizontal">
<div class="panel-body">
<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblReport" runat="server" class="col-sm-2 control-label" 
                                                             Text="Report Type"></asp:Label>
					   <div class="col-sm-3">
					   <asp:DropDownList ID="ddlreport" class="form-control" runat="server" AutoPostBack="true" 
					   onselectedindexchanged="ddlreport_SelectedIndexChanged">
                        <asp:ListItem Text="----Select----" Value="0" ></asp:ListItem>
                        <asp:ListItem Text="Yearwise Report" Value="1" ></asp:ListItem>
                        <asp:ListItem Text="Monthwise Report" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Employeewise report" Value="3" ></asp:ListItem>
                       </asp:DropDownList>
					      
					   </div>
				      
						
</div>
				        
</div>

<asp:Panel ID="panelyearwise" runat="server" visible="false">
<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblfinance" runat="server" class="col-sm-2 control-label" 
                                                             Text="Financial Year"></asp:Label>
					   <div class="col-sm-3">
					   <asp:DropDownList ID="ddlfinance" class="form-control" runat="server" AutoPostBack="true" 
					   onselectedindexchanged="ddlfinance_SelectedIndexChanged">
                       </asp:DropDownList>
					   </div>
				      <div class="col-md-1"></div>
				       <div class="col-sm-3">
                           <asp:Button ID="btnyr" class="btn btn-success" runat="server" Text="Search" onclick="btnyr_Click"/>
				       </div>
						
</div>
				        
</div>
</asp:Panel>

<asp:Panel ID="panelmont" runat="server" visible="false">
<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblfinance1" runat="server" class="col-sm-2 control-label" 
                                                             Text="Financial Year"></asp:Label>
					   <div class="col-sm-3">
					   <asp:DropDownList ID="ddlFinance1" class="form-control" runat="server" AutoPostBack="true" 
					   onselectedindexchanged="ddlFinance1_SelectedIndexChanged">
                       </asp:DropDownList>
					   </div>
				      
						
</div>
				        
</div>

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblmonth" runat="server" class="col-sm-2 control-label" 
                                                             Text="Months"></asp:Label>
					   <div class="col-sm-3">
					   <asp:DropDownList ID="ddlMonths" class="form-control" runat="server" AutoPostBack="true" 
					   onselectedindexchanged="ddlMonths_SelectedIndexChanged">
                       </asp:DropDownList>
					   </div>
				      <div class="col-md-1"></div>
				       <div class="col-sm-3">
                           <asp:Button ID="btnmonths" class="btn btn-success" runat="server" Text="Search" onclick="btnmonths_Click"/>
				       </div>
						
</div>
				        
</div>

</asp:Panel>

<asp:Panel ID="panelemployee" runat="server" visible="false">
<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblEmpNo" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee No"></asp:Label>
					   <div class="col-sm-3">
                           <asp:TextBox ID="txtEmpno" class="form-control" runat="server" ontextchanged="txtEmpno_TextChanged"></asp:TextBox>
					   </div>
				      
						
</div>
				        
</div>

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblexist" runat="server" class="col-sm-2 control-label" 
                                                             Text="Existing Code"></asp:Label>
					   <div class="col-sm-3">
					  <asp:TextBox ID="txtexist" class="form-control" runat="server" ontextchanged="txtexist_TextChanged" ></asp:TextBox>
					   </div>
				      <div class="col-md-1"></div>
				       <div class="col-sm-3">
                           <asp:Button ID="btnemployee" class="btn btn-success" runat="server" Text="Search" onclick="btnemployee_Click" />
				       </div>
						
</div>
				        
</div>

</asp:Panel>

<div class="col-md-12">     
				       <div class="form-group row">
				       <asp:Label ID="lblexport" runat="server" class="col-sm-2 control-label" 
                                                             Text="Export"></asp:Label>
					   <div class="col-sm-3">
					   <asp:DropDownList ID="ddlexport" class="form-control" runat="server" AutoPostBack="true">
                        <asp:ListItem Text="----Select----" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Excel" Value="1"></asp:ListItem>
                        <asp:ListItem Text="PDF" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Doc" Value="3" ></asp:ListItem>
                       </asp:DropDownList>
					      
					   </div>
				       <div class="col-md-1"></div>
				       <div class="col-sm-3">
				       <asp:Button ID="btnExport" class="btn btn-success" runat="server" Text="Export" onclick="btnExport_Click" />
				       </div>
						
</div>
				        
</div>

<asp:Panel ID="panelgrid" runat="server" visible="false">
<div class="col-md-12">     
<div class="form-group row">

<table class="full">
				                                <tbody>
				                                    <tr>
                                                        <td colspan="4">
                                                            <asp:GridView ID="gvreport" runat="server" AutoGenerateColumns="false" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SL. No.</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp. No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmployeeNo" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp. Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblempName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Category</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblcategory" runat="server" Text='<%# Eval("Category") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Designation</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Date of Resign</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldor" runat="server" Text='<%# Eval("dor") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PF Amount</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblpf" runat="server" Text='<%# Eval("PFAmt") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESIC Amount</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblesic" runat="server" Text='<%# Eval("ESIAmt") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>AdvanceAmount</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("AdvanceAmount") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Reason</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblreason" runat="server" Text='<%# Eval("Reason") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>                                                    
				                                </tbody>
				                            </table>

</div>
</div>				       
</asp:Panel>


</div>
</form>
</div>
</div>


<!-- Dashboard start -->
 <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    
<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>  
 <!-- Dashboard End -->
</div>  <!-- col-md-12 End -->
</div>  <!-- row End -->
</div>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

