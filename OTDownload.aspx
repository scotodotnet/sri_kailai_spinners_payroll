﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="OTDownload.aspx.cs" Inherits="OTDownload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>

          <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">OT Download</li></h4> 
                    </ol>
         </div>
          
          
       <div id="main-wrapper" class="container">
         <div class="row">
           <div class="col-md-12">
             <div class="col-md-9">
			  <div class="panel panel-white">
			
			
			    <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">OT Download</h4>
				</div>
			   </div>
			 
				<form class="form-horizontal">
				  <div align="center">
					       <asp:Label ID="lblWagesType" for="input-Default" class="col-sm-12 control-label" 
                                Text="WAGES TYPE" runat="server" Font-Bold="True" Font-Underline="True"></asp:Label>
				        
				   </div>
				   <div class="panel-body">
                        
            <div class="row">
            <div class="form-group col-md-12">
					      
					    <div align="center">
				   
				   <div class="form-group col-md-2"></div>
					     <div class="form-group col-md-9">
					         <asp:RadioButtonList ID="rbsalary" runat="server" AutoPostBack="true" 
                                               RepeatColumns="3" onselectedindexchanged="rbsalary_SelectedIndexChanged">
                              <asp:ListItem Text ="Weekly Wages" Value="1"></asp:ListItem>
                              <asp:ListItem Text="Monthly Wages" Value="2" ></asp:ListItem>                                                                                                                                       
                              <asp:ListItem Text="Bi-Monthly Wages" Value="3"></asp:ListItem>
                             </asp:RadioButtonList>
                            
					        
                         </div>
				   </div>
				   </div>
				</div>   
				
                        
         
				  
                        
                   <div class="row">
					    <div class="form-group col-md-12">
					      
					     <div class="form-group row">
					       
					
					     <div class="col-sm-1"></div>
				       <asp:Label ID="lblEmployeeType" runat="server" Text="Employee Type" class="col-sm-2 control-label"></asp:Label>
				      			     
				        <div class="col-sm-3">
						       <asp:DropDownList ID="txtEmployeeType" runat="server" AutoPostBack="true" class="form-control">
                              </asp:DropDownList>
                          </div>
			       </div>
					    
					    
                              
                              
                               <div class="form-group row">
                                <div align="center">
                                  <asp:Button ID="btnDownload" runat="server" Text="Download"  
                                                                onclick="btnDownload_Click" class="btn btn-success"/>
                                   </div>
                                </div>
                              
                              
                        
			                    <div class="form-group">
			                 
			                 <tr id="Panelload" runat="server" visible="false">
                                                                <td>
                                                                    <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="false" >
                                                                    <Columns>
                                                                    <asp:TemplateField>
                                                                            <HeaderTemplate>Department</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="Department" runat="server" Text='<%# Eval("DepartmentNm") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <%--<asp:TemplateField>
                                                                            <HeaderTemplate>MachineID</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="MachineID" runat="server" Text='<%# Eval("MachineNo") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>EmpNo</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="EmpNo" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>ExistingCode</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="ExistingCode" runat="server" Text='<%# Eval("ExisistingCode") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>FirstName</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="EmpName" runat="server" Text='<%# Eval("EmpName") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>HrSalary</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="HrSalary" runat="server" Text='<%# Eval("Base") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>NoHrs</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="Days" runat="server" Text="" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>                                                                        
                                                                    </Columns>
                                                                </asp:GridView>
                                                                </td>
                                                            </tr>
			                 
			                 </div>
			               
			               
			                
			               
			             
					     
                     </div>
                  </div>
   
               </div>
			  
			
			
	  
		      </form>
		   
		  
		</div>
	</div>
 	            <!-- Dashboard start -->
 	
             	<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
              <!-- Dashboard End --> 
 
  
          
   
     
                 <div class="row">
               <asp:Panel ID="Result_Panel" Visible="false" runat="server">
				                                <table class="full">
				                                <tbody>
				                                    <tr>
                                                        <td colspan="4">
                                                            <asp:GridView ID="griddept" runat="server" AutoGenerateColumns="false" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblempno" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>OLD ID</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbloldid" runat="server" Text='<%# Eval("OldID")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Exist.Code</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblExisistingCode" runat="server" Text='<%# Eval("ExisistingCode")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp. Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblempname" runat="server" Text= '<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Date of Joining</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldoj" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <%--<asp:TemplateField>
                                                                        <HeaderTemplate>De-Activate Date</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDeActivateDate" runat="server" Text='<%# Eval("deactivedate") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>--%>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Date of Birth</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldob" runat="server" Text='<%# Eval("DOB") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Designation</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField> 
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp. Type</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpType" runat="server" Text='<%# Eval("EmpType") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ProfileType</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblProfileType" runat="server" Text='<%# Eval("ProfileType")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SalaryThrough</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSalaryThrough" runat="server" Text='<%# Eval("Salarythrough")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESINumber</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESICnumber" runat="server" Text='<%# Eval("ESICnumber")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PFNumber</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFnumber" runat="server" Text='<%# Eval("PFnumber")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>WagesType</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblWagestype" runat="server" Text='<%# Eval("Wagestype")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblBase_Salary" runat="server" Text='<%# Eval("Base_Salary")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>OT Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblOT" runat="server" Text='<%# Eval("OTSalary")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                </Columns>
                                                            </asp:GridView>
                                                            <asp:GridView ID="GridViewdeactiveded" runat="server" AutoGenerateColumns="false" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblempno" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>OLD ID</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbloldid" runat="server" Text='<%# Eval("OldID")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Exist.Code</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblExisistingCode" runat="server" Text='<%# Eval("ExisistingCode")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp. Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblempname" runat="server" Text= '<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Date of Joining</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldoj" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>De-Activate Date</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDeActivateDate" runat="server" Text='<%# Eval("deactivedate") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Date of Birth</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldob" runat="server" Text='<%# Eval("DOB") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Designation</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField> 
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp. Type</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpType" runat="server" Text='<%# Eval("EmpType") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ProfileType</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblProfileType" runat="server" Text='<%# Eval("ProfileType")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SalaryThrough</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSalaryThrough" runat="server" Text='<%# Eval("Salarythrough")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESINumber</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESICnumber" runat="server" Text='<%# Eval("ESICnumber")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PFNumber</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFnumber" runat="server" Text='<%# Eval("PFnumber")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>WagesType</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblWagestype" runat="server" Text='<%# Eval("Wagestype")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblBase_Salary" runat="server" Text='<%# Eval("Base_Salary")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>OT Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblOT" runat="server" Text='<%# Eval("OTSalary")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
				                                </tbody>
				                            </table>
				                            </asp:Panel>
            </div>
     
        </div><!-- col 12 end -->
        
          </div><!-- row end -->
         </div>
        
      
        
          </ContentTemplate>
          
      <Triggers>
      <asp:PostBackTrigger ControlID="btnDownload"  />
 </Triggers>
      
       </asp:UpdatePanel>



</asp:Content>

