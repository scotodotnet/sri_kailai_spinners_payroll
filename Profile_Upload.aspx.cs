﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Profile_Upload : System.Web.UI.Page
{

    string SessionAdmin;
    BALDataAccess objdata = new BALDataAccess();
    AttenanceClass objatt = new AttenanceClass();
    string SessionCcode;
    string SessionLcode;

    string ProbationPeriod = "0";
    //string Confirmationdate = "";
    string Pannumber = "";
    string Grade = "";
    string ReportingAuthorityname = "";
    string Insurancecompanyname = "0";
    string Insurancenumber = "";
    string Modepaycheque = "";
    string Banknamecheque = "";
    string Enteramountforcheque = "";
    string Enteramountfortransaction = "";
    string BankACCNo = "";
    string Accountholdername = "";
    string Labourtype = "";
    string Contracttype = "";
    //string ExpiryDate = "";
    //string CreatedDate = "";
    string CreatedBy = "";
    string Modifiedby = "";
    string ModifiedOn = "";
    string ContractPeriod = "0";
    string BasicSalary = "0.00";
    string Status_val = "";
    string PF_Type = "";

    //string ContractStartDate = "";
    // string ContractEndDate = "";
    //string Status = "";
    string ProfileType = "0";
    // string PF_Type = "";
    string chkMonths = "0";
    string chkYears = "0";
    string Duration = "0";

    OfficialprofileClass objOff = new OfficialprofileClass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        if (SessionAdmin == "2")
        {
            Response.Redirect("Dash_Board.aspx");

        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
     
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            //Skip
        }
      
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string EmpNo = "";
        string MachineNo = "";
        string E_PF = "";
        string E_ESI = "";
        string Ccode = "";
        string Lcode = "";
        string E_OT = "";
        string Sal_Tho = "";
        string Fin_Year = "";
        int months = 0;
        int year = 0;

        string MachineID = "";
        string TokenNo = "";
        string EmpName = "";
        string DOJ = "";
        string ElgiblePF = "";
        string PFNo = "";
        string ElgibleESI = "";
        string ESINO = "";
        string ElgibleOT = "";
        string Wages = "";
        string Salarythrough = "";
        string Bank = "";
        string Branch = "";
        string BankACNo = "";
        string Bank_Name = "";
        string ESICode = "";

        DateTime Dofjoin, ExpiryDate, ExpiredDate1 = Convert.ToDateTime("01-01-1999"), ContractStartingDate, ContractStartingDate1 = Convert.ToDateTime("01-01-1999"), ContractEndingDate, ContractEndingDate1 = Convert.ToDateTime("01-01-1999");
        DateTime DofConfir = Convert.ToDateTime("01-01-1999");
        //string CreateDate = "";
        DateTime CreateDate = Convert.ToDateTime(DateTime.Now.ToString());
        //CreateDate = DateTime.Now.ToString("dd/MM/yyy 00:00");

        //DateTime date = new DateTime();
        months = Convert.ToInt32(DateTime.Now.ToString("MM"));
        year = Convert.ToInt32(DateTime.Now.ToString("yyyy"));


        if (months >= 4)
        {
            year = year;

        }
        else
        {
            year = year;
        }



        try
        {
            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

                string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                DataTable dts = new DataTable();
                using (sSourceConnection)
                {
                    sSourceConnection.Open();
                    OleDbCommand command = new OleDbCommand("select * FROM [Sheet1$];", sSourceConnection);
                    sSourceConnection.Close();

                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        command.CommandText = "select * FROM [Sheet1$];";
                        sSourceConnection.Open();
                    }
                    using (OleDbDataReader dr = command.ExecuteReader())
                    {

                        if (dr.HasRows)
                        {

                        }

                    }
                    OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                    objDataAdapter.SelectCommand = command;
                    DataSet ds = new DataSet();

                    objDataAdapter.Fill(ds);
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                    }
                    string constr = ConfigurationManager.AppSettings["ConnectionString"];

                    for (int j = 0; j < dt.Rows.Count - 1; j++)
                    {
                        SqlConnection cn = new SqlConnection(constr);
                        MachineID = dt.Rows[j][0].ToString();
                        TokenNo = dt.Rows[j][1].ToString();
                        EmpName = dt.Rows[j][2].ToString();
                        DOJ = dt.Rows[j][3].ToString();
                        ElgiblePF = dt.Rows[j][4].ToString();
                        PFNo = dt.Rows[j][5].ToString();
                        ElgibleESI = dt.Rows[j][6].ToString();
                        ESINO = dt.Rows[j][7].ToString();
                        ElgibleOT = dt.Rows[j][8].ToString();
                        Wages = dt.Rows[j][9].ToString();
                        Salarythrough = dt.Rows[j][10].ToString();
                        Bank = dt.Rows[j][11].ToString();
                        Branch = dt.Rows[j][12].ToString();
                        BankACNo = dt.Rows[j][13].ToString();
                        ESICode = dt.Rows[j][14].ToString();

                        if (MachineID == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter MachineID. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (TokenNo == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Token No. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (EmpName == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Employee Name. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (DOJ == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter DOJ. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (ElgiblePF == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ElgiblePF. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (PFNo == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter PFNo. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (ElgibleESI == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ElgibleESI. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (ESINO == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ESINO. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (ElgibleOT == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ElgibleOT. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (Wages == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (Salarythrough == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Salarythrough. The Row Number is " + j + "');", true);
                            ErrFlag = true;
                        }
                        else if (ESICode == "")
                        {
                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Company Code. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }
                        else if (Bank == "")
                        {


                            j = j + 2;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Bank Name. The Row Number is " + j + "');", true);
                            //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            ErrFlag = true;
                        }



                        if (Salarythrough.ToLower().ToString() == "no".ToLower().ToString())
                        {
                            if (Branch == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Branch Name. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            if (BankACNo == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Bank AC No. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Open();
                            string qry = "Select BankName from Bank where BankName = '" + Bank + "'";
                            SqlCommand cmd = new SqlCommand(qry, cn);
                            SqlDataReader sdr = cmd.ExecuteReader();
                            if (sdr.HasRows)
                            {


                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This Bank Name Not Availabe in Bank Master.The Row Number is " + j + " ');", true);
                                ErrFlag = true;

                            }
                            cn.Close();
                        }
                    }
                    if (!ErrFlag)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            SqlConnection cn = new SqlConnection(constr);

                            cn.Open();
                            string MID = dt.Rows[i][0].ToString();
                            DataTable dtmachine = new DataTable();
                            dtmachine = objdata.GetMachineNo(MID, SessionCcode, SessionLcode);
                            if (dtmachine.Rows.Count != 0)
                            {
                                EmpNo = dtmachine.Rows[0]["EmpNo"].ToString();
                                MachineNo = dtmachine.Rows[0]["MachineNo"].ToString();

                                TokenNo = dt.Rows[i][1].ToString();
                                EmpName = dt.Rows[i][2].ToString();
                                DOJ = dt.Rows[i][3].ToString();
                                PFNo = dt.Rows[i][5].ToString();
                                ESINO = dt.Rows[i][7].ToString();
                                Wages = dt.Rows[i][9].ToString();
                                Branch = dt.Rows[i][12].ToString();
                                BankACNo = dt.Rows[i][13].ToString();
                                ESICode = dt.Rows[i][14].ToString();


                                if (dt.Rows[i][4].ToString().ToLower().Trim() == "yes".ToString().ToLower().Trim())
                                {
                                    E_PF = "1";
                                }
                                if (dt.Rows[i][4].ToString().ToLower().Trim() == "no".ToString().ToLower().Trim())
                                {
                                    E_PF = "2";
                                }
                                else
                                {
                                    E_PF = "2";
                                }

                                if (dt.Rows[i][6].ToString().ToLower().Trim() == "yes".ToString().ToLower().Trim())
                                {
                                    E_ESI = "1";
                                }
                                if (dt.Rows[i][6].ToString().ToLower().Trim() == "no".ToString().ToLower().Trim())
                                {
                                    E_ESI = "2";
                                }
                                else
                                {
                                    E_ESI = "2";
                                }


                                if (dt.Rows[i][8].ToString().ToLower().Trim() == "yes".ToString().ToLower().Trim())
                                {
                                    E_OT = "1";
                                }
                                if (dt.Rows[i][8].ToString().ToLower().Trim() == "no".ToString().ToLower().Trim())
                                {
                                    E_OT = "2";
                                }
                                else
                                {
                                    E_OT = "2";

                                }
                                if (dt.Rows[i][10].ToString().ToLower().Trim() == "yes".ToString().ToLower().Trim())
                                {
                                    Sal_Tho = "1";
                                }
                                if (dt.Rows[i][10].ToString().ToLower().Trim() == "no".ToString().ToLower().Trim())
                                {
                                    Sal_Tho = "2";
                                }
                                else
                                {
                                    Sal_Tho = "1";

                                }

                                if (dt.Rows[i][14].ToString() == "0")
                                {
                                    ESICode = "--Select--";
                                }
                                else { ESICode = dt.Rows[i][14].ToString(); }

                                BankACNo = dt.Rows[i][13].ToString();

                                if (!ErrFlag)
                                {
                                    objOff.EmpNo = EmpNo;
                                    objOff.Machine = MachineNo;
                                    objOff.Dateofjoining = DOJ;
                                    objOff.Probationperiod = Convert.ToInt32(ProbationPeriod).ToString();
                                    objOff.Confirmationdate = DofConfir.ToString();
                                    objOff.Pannumber = Pannumber;
                                    objOff.ESICnumber = ESINO;
                                    objOff.PFnumber = PFNo;
                                    objOff.Grade = Grade;
                                    //objOff.EmployeeType = "0";
                                    objOff.ReportingAuthorityName = ReportingAuthorityname;
                                    objOff.InsurancecompanyName = Insurancecompanyname;
                                    objOff.Insurancenumber = Insurancenumber;
                                    objOff.Eligibleforovertime = E_OT;
                                    objOff.Salarythrough = Sal_Tho;
                                    objOff.modepaycheque = Modepaycheque;
                                    objOff.Banknamecheque = Banknamecheque;
                                    objOff.EnterAmountforcheque = Enteramountforcheque;
                                    objOff.BankACCno = BankACNo;
                                    objOff.Accountholdername = Accountholdername;
                                    string qry_bankname = "Select Bankcd from Bank where BankName='" + dt.Rows[i]["Bank"].ToString() + "'";
                                    SqlCommand cmd_bank = new SqlCommand(qry_bankname, cn);
                                    Bank_Name = Convert.ToString(cmd_bank.ExecuteScalar());

                                    if (Bank_Name.ToString() == "")
                                    {
                                        Bank_Name = "0";
                                        BankACNo = "0";
                                        Branch = "";
                                    }
                                    else
                                    {

                                    }

                                    objOff.BankName = Convert.ToInt32(Bank_Name).ToString();
                                    objOff.Branchname = Branch;
                                    objOff.BankACno = BankACNo;
                                    objOff.Enteramountfortransaction = Enteramountfortransaction;
                                    objOff.LabourType = Labourtype;
                                    objOff.ContractType = Contracttype;
                                    objOff.ExpiryDate = ExpiredDate1.ToString();
                                    objOff.WagesType = Wages;
                                    objOff.Financialperiod = year.ToString();
                                    objOff.CreatedDate = CreateDate.ToString();
                                    objOff.CreatedBy = CreatedBy;
                                    objOff.Modifiedby = Modifiedby;
                                    objOff.ModifiedOn = ModifiedOn;
                                    objOff.contractperiod = ContractPeriod;
                                    objOff.BasicSalary = BasicSalary;
                                    objOff.Status = Status_val;
                                    objOff.EligibleESI = E_ESI;
                                    objOff.EligblePF = E_PF;
                                    objOff.ContractStartDate = ContractStartingDate1.ToString();
                                    objOff.ContractEndDate = ContractEndingDate1.ToString();
                                    objOff.ProfileType = ProfileType;
                                    objOff.Ccode = SessionCcode;
                                    objOff.Lcode = SessionLcode;
                                    objOff.PF_Type = PF_Type;
                                    objOff.ChkMonths = chkMonths;
                                    objOff.ChkYears = chkYears;
                                    objOff.Duration = Duration;
                                    objOff.ESICode = ESICode;

                                    string qry_Exist = "select EmpNo from EmployeeDetails where BiometricID='" + MID + "' and Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "'";
                                    SqlCommand cmd_Exist = new SqlCommand(qry_Exist, cn);
                                    string EmpNo_code = Convert.ToString(cmd_Exist.ExecuteScalar());
                                    //Delete previus Empno to official profile
                                    if (EmpNo_code == EmpNo)
                                    {
                                        objdata.DeleOff_SP(EmpNo_code, SessionCcode, SessionLcode);
                                        objdata.OfficalProfile_Upload(objOff);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                    }
                                    else
                                    {
                                        objdata.OfficalProfile_Upload(objOff);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);

                                    }

                                    cn.Close();

                                }
                            }
                        }

                    }
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your File Not Upload...');", true);
                }
            }
        }
        catch
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }

}
