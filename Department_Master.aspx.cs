﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using System.Configuration;


public partial class Department_Master : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    MastersClass objDep = new MastersClass();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string lblprobation_Common;
    static string Deptid;
    string SSQL;
     bool ErrFlag = false; 

    protected void Page_Load(object sender, EventArgs e)
    {

        //if (Session["UserId"] == null)
        //{
        //    Response.Redirect("Default.aspx");
        //    Response.Write("Your session expired");
        //}
        //string ss = Session["UserId"].ToString();
        //SessionAdmin = Session["Isadmin"].ToString();
        //if (!IsPostBack)
        //{

           DepartmentGriddisplay();
        //}
        ////string ss = Session["UserId"].ToString();
        //SessionAdmin = Session["Isadmin"].ToString();
        //SessionCcode = Session["Ccode"].ToString();
        //SessionLcode = Session["Lcode"].ToString();
       
    }
    public void DepartmentGriddisplay()
    {
        DataTable dtgv = new DataTable();
        dtgv = objdata.MstDepartmentGridDisplay();
        rptrDepartment.DataSource = dtgv;
        rptrDepartment.DataBind();
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        Clear();
    }

    public void Clear()
    {
        // btnsave.Text = "Save";
        
        txtdepartmentnm.Text = "";
        DepartmentGriddisplay();

    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtdepartmentnm.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Department Name');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                string dptname = objdata.DepartmentName_check(txtdepartmentnm.Text);
               
                if (dptname.Trim().ToLower() == txtdepartmentnm.Text.Trim().ToLower())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department Name already Exist');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    if (btnSave.Text == "Update")
                    {
                        objDep.DepartmentCd = Deptid.ToString();
                        objDep.DepartmentNm = txtdepartmentnm.Text;
                        objdata.UpdateMstDepartment(objDep);
                        DepartmentGriddisplay();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department Name Update Successfully');", true);
                        txtdepartmentnm.Text = "";
                        DepartmentGriddisplay();
                        Clear();
                        btnSave.Text = "Save";
                    }
                    else
                    {
                        objDep.DepartmentNm = txtdepartmentnm.Text;
                        objdata.MstDepartment(objDep);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department Save Successfully');", true);
                        txtdepartmentnm.Text = "";
                        DepartmentGriddisplay();
                        Clear();
                    }
                }
           }
        }
        catch (Exception ex)
        {
        }
    }




    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
  
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }


    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        Deptid = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(Deptid);
                break;
            case ("Edit"):
                EditRepeaterData(Deptid);
                break;
        }
    }

    private void EditRepeaterData(string Deptid)
    {
        DataTable dtd = new DataTable();
        SSQL = "select DepartmentNm from MstDepartment where DepartmentCd = '" + Deptid.ToString() + "'";
        dtd = objdata.ReturnMultipleValue(SSQL);
        if (dtd.Rows.Count > 0)
        {
            txtdepartmentnm.Text = dtd.Rows[0]["DepartmentNm"].ToString();
            btnSave.Text = "Update";
        }
    }

    private void DeleteRepeaterData(string DepartmentID)
    {
       


        DataTable dtable = new DataTable();
        SSQL = "select DepartmentNm from MstDepartment where DepartmentCd = '" + DepartmentID.ToString() + "'";
        dtable = objdata.ReturnMultipleValue(SSQL);
        if (dtable.Rows.Count > 0)
        {
            string DeptName = dtable.Rows[0]["DepartmentNm"].ToString();

            string dptcode = objdata.Departmentcode_delete(DepartmentID);
            if (dptcode.Trim() == DepartmentID.Trim())
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department cannot be deleted');", true);
                ErrFlag = true;
            }
            else
            {
              
                SSQL = "Delete MstDepartment where DepartmentCd = '" + DepartmentID.ToString() + "'";
                objdata.ReturnMultipleValue(SSQL);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Delete Successfully..!');", true);
                DepartmentGriddisplay();
                Clear();

            }
        }

    }

}

