﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using Altius.BusinessAccessLayer.BALDataAccess;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;



public partial class OTDownload : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Name;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["UserId"] == null)
            {
                Response.Redirect("Default.aspx");
                Response.Write("Your session expired");
            }
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            string ss = Session["UserId"].ToString();
            Name = Session["Usernmdisplay"].ToString();
            if (!IsPostBack)
            {
                EmployeeType();
            };


        }
        catch (Exception ex)
        {
        }

    }
    public void EmployeeType()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no("2");
        txtEmployeeType.DataSource = dtemp;
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }

    protected void rbsalary_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (rbsalary.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Salary Type');", true);
                ErrFlag = true;
            }
            if (txtEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }
            if (txtEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                string Employee_Type = "";
                Employee_Type = txtEmployeeType.SelectedValue.ToString();

                DataTable dt = new DataTable();
                dt = objdata.OT_Download_EmpTypeWise(SessionCcode, SessionLcode, rbsalary.SelectedValue, Employee_Type);
                gvEmp.DataSource = dt;
                gvEmp.DataBind();
                if (dt.Rows.Count > 0)
                {
                    string attachment = "attachment;filename=OTDownload.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";

                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    gvEmp.RenderControl(htextw);
                    //gvDownload.RenderControl(htextw);
                    //Response.Write("Contract Details");
                    Response.Write(stw.ToString());
                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                }
            }
            //Response.Redirect("EmployeeRegistration.aspx");
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('" + ex.Message + " SS');", true);
        }
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
