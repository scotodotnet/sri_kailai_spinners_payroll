﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class RptEmployeeSalaryHistory : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;

    protected void Page_Load(object sender, EventArgs e)
    {
      
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            DropDwonCategory();
            alldropdownadd();

        }
        
    }

    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }
    public void alldropdownadd()
    {

        ////Financial Year Add
        //int currentYear = Utility.GetFinancialYear;
        //txtFinancial_Year.Items.Add("----Select----");
        //for (int i = 0; i < 11; i++)
        //{
        //    txtFinancial_Year.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
        //    currentYear = currentYear - 1;
        //}
    }
    protected void ddldept_SelectedIndexchanged(object sender, EventArgs e)
    {

        //Clear Form
        Result_Panel.Visible = false;

        bool ErrFlag = false;
        DataTable dtempty = new DataTable();
        txtEmployeeNo.DataSource = dtempty;
        txtEmployeeNo.DataBind();
        txtEmployeeName.DataSource = dtempty;
        txtEmployeeName.DataBind();
        string stafforLabour = "";
        string activemode = "N";
        //if (CheckBox1.Checked == true)
        //{
        //    activemode = "N";
        //}
        //else
        //{
        //    activemode = "Y";
        //}
        if (ddldept.SelectedValue == "0")
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (ddlcategory.SelectedValue == "1")
        {
            stafforLabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            stafforLabour = "L";
        }
        else if (ddlcategory.SelectedValue == "0")
        {
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            DataTable dtempcode = new DataTable();
            dtempcode = objdata.LoadEmployeeForBonus_Salary_History(stafforLabour, ddldept.SelectedValue, SessionCcode, SessionLcode, activemode);


            txtEmployeeNo.DataSource = dtempcode;
            DataRow dr = dtempcode.NewRow();
            dr["EmpNo"] = ddldept.SelectedItem.Text;
            dr["EmpName"] = ddldept.SelectedItem.Text;
            dtempcode.Rows.InsertAt(dr, 0);

            txtEmployeeNo.DataTextField = "EmpNo";
            txtEmployeeNo.DataValueField = "EmpNo";
            txtEmployeeNo.DataBind();
            txtEmployeeName.DataSource = dtempcode;
            txtEmployeeName.DataTextField = "EmpName";
            txtEmployeeName.DataValueField = "EmpNo";
            txtEmployeeName.DataBind();


            DataTable Token_DT = new DataTable();
            string query = "";
            query = "Select ExisistingCode as TokenNo from EmployeeDetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            query = query + " and Department='" + ddldept.SelectedValue + "'";
            query = query + " and StafforLabor='" + stafforLabour + "' Order by ExisistingCode";

            txtTokenNo.Items.Clear();
            txtTokenNo.Items.Add("----Select-----");
            Token_DT = objdata.RptEmployeeMultipleDetails(query);
            for (int i = 0; i < Token_DT.Rows.Count; i++)
            {
                txtTokenNo.Items.Add(Token_DT.Rows[i]["TokenNo"].ToString());
            }
        }
    }
   
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string activemode = "Y";
        DataTable dtempty = new DataTable();
        ddldept.DataSource = dtempty;
        ddldept.DataBind();
        griddept.DataSource = dtempty;
        griddept.DataBind();
        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        //if (CheckBox1.Checked == true)
        //{
        //    activemode = "N";
        //}
        //else
        //{
        //    activemode = "Y";
        //}
        DataTable dtDip = new DataTable();
        dtDip = objdata.salaryhistory_DropDowndept_category(Stafflabour, SessionCcode, SessionLcode, activemode);
        if (dtDip.Rows.Count > 1)
        {
            ddldept.DataSource = dtDip;
            ddldept.DataTextField = "DepartmentNm";
            ddldept.DataValueField = "DepartmentCd";
            ddldept.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddldept.DataSource = dtempty;
            ddldept.DataBind();
        }

        //Clear All DropDown Box
        ddldept.SelectedIndex = 0;
        //txtEmployeeNo.SelectedIndex = 0;
        //txtEmployeeName.SelectedIndex = 0;
        //txtFinancial_Year.SelectedIndex = 0;
        Result_Panel.Visible = false;
    }
    protected void txtEmployeeNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtEmployeeNo.SelectedValue != "0")
        {
            txtEmployeeName.SelectedValue = txtEmployeeNo.SelectedValue;

            string checkTokenNo = txtEmployeeName.SelectedValue.ToString();
            DataTable Token_DT = new DataTable();
            string query = "";
            query = "Select ExisistingCode from EmployeeDetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            query = query + " and Department='" + ddldept.SelectedValue + "' and EmpNo='" + checkTokenNo + "'";
            Token_DT = objdata.RptEmployeeMultipleDetails(query);
            if (Token_DT.Rows.Count.ToString() != "0")
            {
                txtTokenNo.SelectedValue = Token_DT.Rows[0]["ExisistingCode"].ToString();
                //txtTokenSearch.Text = Token_DT.Rows[0]["ExisistingCode"].ToString();
            }
        }
    }
    protected void txtEmployeeName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtEmployeeName.SelectedValue != "0")
        {
            txtEmployeeNo.SelectedValue = txtEmployeeName.SelectedValue;

            string checkTokenNo = txtEmployeeName.SelectedValue.ToString();
            DataTable Token_DT = new DataTable();
            string query = "";
            query = "Select ExisistingCode from EmployeeDetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            query = query + " and Department='" + ddldept.SelectedValue + "' and EmpNo='" + checkTokenNo + "'";
            Token_DT = objdata.RptEmployeeMultipleDetails(query);
            if (Token_DT.Rows.Count.ToString() != "0")
            {
                txtTokenNo.SelectedValue = Token_DT.Rows[0]["ExisistingCode"].ToString();
                //txtTokenSearch.Text = Token_DT.Rows[0]["ExisistingCode"].ToString();
            }
        }
    }
    protected void txtTokenNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtTokenNo.SelectedValue != "----Select-----")
        {
            string checkTokenNo = txtTokenNo.SelectedItem.Value.ToString();

            DataTable Token_DT = new DataTable();
            string query = "";
            query = "Select EmpNo,EmpName from EmployeeDetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            query = query + " and Department='" + ddldept.SelectedValue + "' and ExisistingCode='" + checkTokenNo + "'";
            Token_DT = objdata.RptEmployeeMultipleDetails(query);
            if (Token_DT.Rows.Count.ToString() != "0")
            {
                txtEmployeeNo.SelectedValue = Token_DT.Rows[0]["EmpNo"].ToString();
                txtEmployeeName.SelectedValue = Token_DT.Rows[0]["EmpNo"].ToString();
                //txtTokenSearch.Text = checkTokenNo;
            }
        }
    }
    protected void btnTokenSearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtTokenSearch.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Token No....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable Token_DT = new DataTable();
            string query = "";
            query = "Select ExisistingCode,EmpName,EmpNo,StafforLabor,Department from EmployeeDetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            query = query + " and ExisistingCode='" + txtTokenSearch.Text + "'";
            Token_DT = objdata.RptEmployeeMultipleDetails(query);
            if (Token_DT.Rows.Count != 0)
            {
                if (Token_DT.Rows[0]["StafforLabor"].ToString().ToUpper() == "S".ToString().ToUpper())
                {
                    ddlcategory.SelectedValue = "1";
                }
                else if (Token_DT.Rows[0]["StafforLabor"].ToString().ToUpper() == "L".ToString().ToUpper())
                {
                    ddlcategory.SelectedValue = "2";
                }
                ddlcategory_SelectedIndexChanged(sender, e);
                ddldept.SelectedValue = Token_DT.Rows[0]["Department"].ToString();
                ddldept_SelectedIndexchanged(sender, e);
                txtTokenNo.Text = Token_DT.Rows[0]["ExisistingCode"].ToString();
                txtEmployeeNo.SelectedValue = Token_DT.Rows[0]["EmpNo"].ToString();
                txtEmployeeName.SelectedValue = Token_DT.Rows[0]["EmpNo"].ToString();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Token No Not Fount in Database....!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Department Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
        }
    }
    protected void btnReportView_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if ((txtTokenNo.SelectedValue == "") || (txtTokenNo.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Token No...');", true);
            ErrFlag = true;
        }
        //else if (txtFromDate.Text.Trim() == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
        //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    ErrFlag = true;
        //}
        //else if (txtToDate.Text.Trim() == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
        //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    ErrFlag = true;
        //}

        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "1")
            {
                Stafflabour = "S";
            }
            else if (ddlcategory.SelectedValue == "2")
            {
                Stafflabour = "L";
            }
            string EmpNo_Search = txtEmployeeNo.SelectedValue.ToString();
            string Token_No_Search = txtTokenNo.SelectedValue.ToString();
            string Employee_Type_Search = "";
            //Get Employee Type
            string query = "";
            DataTable Token_DT = new DataTable();
            query = "Select EmployeeType from EmployeeDetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            query = query + " and EmpNo='" + EmpNo_Search + "'";
            Token_DT = objdata.RptEmployeeMultipleDetails(query);
            if (Token_DT.Rows.Count != 0)
            {
                Employee_Type_Search = Token_DT.Rows[0]["EmployeeType"].ToString();
            }
            else
            {
                Employee_Type_Search = "";
            }

            ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&Months=" + "" + "&yr=" + "2014" + "&fromdate=" + txtFromDate.Text.ToString() + "&ToDate=" + txtToDate.Text.ToString() + "&Salary=" + "" + "&CashBank=" + "" + "&ESICode=" + "" + "&EmpType=" + Employee_Type_Search + "&PayslipType=" + "" + "&PFTypePost=" + "" + "&Left_Emp=" + "" + "&Leftdate=" + "" + "&EmpNo_Search=" + EmpNo_Search + "&Report_Type=Employee_Salary_History", "_blank", "");
        }


    }

    protected void txtFromDate_TextChanged(object sender, EventArgs e)
    {

    }

    protected void txtToDate_TextChanged(object sender, EventArgs e)
    {

    }


    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }

}
