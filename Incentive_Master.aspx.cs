﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;

public partial class Incentive_Master : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    MastersClass objDep = new MastersClass();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string lblprobation_Common;
    static string MonthDay;
    static string DaysWorked;
    static string WorkerAmt;
    string SSQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["UserId"] == null)
        //{
        //    Response.Redirect("Default.aspx");
        //    Response.Write("Your session expired");
        //}
        //string ss = Session["UserId"].ToString();
        //SessionAdmin = Session["Isadmin"].ToString();
        ////string ss = Session["UserId"].ToString();
        //SessionAdmin = Session["Isadmin"].ToString();
        //SessionCcode = Session["Ccode"].ToString();
        //SessionLcode = Session["Lcode"].ToString();
        //lblusername.Text = Session["Usernmdisplay"].ToString();
        //lblComany.Text = SessionCcode + " - " + SessionLcode;
        //if (SessionAdmin == "2")
        //{
        //    Response.Redirect("EmployeeRegistration.aspx");
        //}

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();




        if (!IsPostBack)
        {
            Load_data1();
            Load_grid();
            Load_CivilIncentivedata();
        }
    }

    private void Load_data1()
    {
        DataTable dt = new DataTable();
        dt = objdata.Wk_Load(SessionCcode, SessionLcode);
        rptrIncentiveMaster.DataSource = dt;
        rptrIncentiveMaster.DataBind();
    }

    private void Load_CivilIncentivedata()
    {
        DataTable dt = new DataTable();
        dt = objdata.CivilIncentive_Load(SessionCcode, SessionLcode);
        if (dt.Rows.Count != 0)
        {
            txtCivilEligibleDays.Text = dt.Rows[0]["ElbDays"].ToString();
            txtCivilIncenAmount.Text = dt.Rows[0]["Amt"].ToString();
        }
        else
        {
            txtCivilEligibleDays.Text = "0";
            txtCivilIncenAmount.Text = "0.0";
        }
    }

    private void clr1()
    {
        Load_data1();
        txtMonthDays.Text = "0";
        txtWorkerDays.Text = "0";
        txtWorkerIncentive.Text = "0.00";
    }

    protected void btnClear1_Click(object sender, EventArgs e)
    {
        clr1();
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        //MonthDay = commandArgs[0];
        //DaysWorked = commandArgs[1];
        //WorkerAmt = commandArgs[2];

        //switch (e.CommandName)
        //{
        //    case ("Delete"):
        //        DeleteRepeaterData(MonthDay, DaysWorked, WorkerAmt);
        //        break;
        //    case ("Edit"):
        //        EditRepeaterData(MonthDay, DaysWorked, WorkerAmt);
        //        break;
        //}

        string[] strArray = e.CommandArgument.ToString().Split(new char[] { ',' });
        MonthDay = strArray[0];
        DaysWorked = strArray[1];
        WorkerAmt = strArray[2];
        string commandName = e.CommandName;
        if (commandName != null)
        {
            if (!(commandName == "Delete"))
            {
                if (!(commandName == "Edit"))
                {
                    return;
                }
            }
            else
            {
                this.DeleteRepeaterData(MonthDay, DaysWorked, WorkerAmt);
                return;
            }
            this.EditRepeaterData(MonthDay, DaysWorked, WorkerAmt);
        }
    }

    private void DeleteRepeaterData(string MonthDay, string DaysWorked, string EmpTypecode)
    {
        //SSQL = "Delete WorkerIncentive_mst where Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "'";
        //SSQL = SSQL + "and MonthDays='" + MonthDay + "'";
        //objdata.ReturnMultipleValue(SSQL);
        //Load_data1();
        //clr1();
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Delete Data Successfully....! ');", true);


        this.SSQL = "Delete WorkerIncentive_mst where Ccode='" + this.SessionCcode + "'and Lcode='" + this.SessionLcode + "'";
        this.SSQL = this.SSQL + "and MonthDays='" + MonthDay + "'";
        this.objdata.ReturnMultipleValue(this.SSQL);
        this.Load_data1();
        this.clr1();
        ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Delete Data Successfully....! ');", true);

   }


    private void EditRepeaterData(string MonthDay, string DaysWorked, string WorkerAmt)
    {
           // try
           //{
           //     DataTable dtdEdit = new DataTable();

           //    SSQL = "SELECT * FROM WorkerIncentive_mst where Ccode='"+ SessionCcode +"'and Lcode='"+ SessionLcode +"'";
           //    SSQL = SSQL + " and WorkerDays = '"+ DaysWorked +"' and WorkerAmt='"+ WorkerAmt +"' and MonthDays='"+ MonthDay +"'";
           //    dtdEdit = objdata.ReturnMultipleValue(SSQL);

           //     if (dtdEdit.Rows.Count > 0)
           //     {
           //         txtMonthDays.Text = dtdEdit.Rows[0]["MonthDays"].ToString();
           //         txtWorkerDays.Text = dtdEdit.Rows[0]["WorkerDays"].ToString();
           //         txtWorkerIncentive.Text = dtdEdit.Rows[0]["WorkerAmt"].ToString();
           //         btnSave.Text = "Update";
           //     }
               
           //   }
           // catch (Exception ex)
           // {

           // }


        try
        {
            DataTable table = new DataTable();
            this.SSQL = "SELECT * FROM WorkerIncentive_mst where Ccode='" + this.SessionCcode + "'and Lcode='" + this.SessionLcode + "'";
            this.SSQL = this.SSQL + " and WorkerDays = '" + DaysWorked + "' and WorkerAmt='" + WorkerAmt + "' and MonthDays='" + MonthDay + "'";
            table = this.objdata.ReturnMultipleValue(this.SSQL);
            if (table.Rows.Count > 0)
            {
                this.txtMonthDays.Text = table.Rows[0]["MonthDays"].ToString();
                this.txtWorkerDays.Text = table.Rows[0]["WorkerDays"].ToString();
                this.txtWorkerIncentive.Text = table.Rows[0]["WorkerAmt"].ToString();
                this.btnSave.Text = "Update";
            }
        }
        catch (Exception)
        {
        }
    }
    
        


    protected void btnSave1_Click(object sender, EventArgs e)
    {
        //try
        //{
        //    bool ErrFlag = false;
        //    bool SaveFlag = false;

        //    if (txtWorkerDays.Text.Trim() == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Minimum Days Worked');", true);
        //        ErrFlag = true;
        //    }
        //    else if (txtWorkerIncentive.Text.Trim() == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Incentive Amount');", true);
        //        ErrFlag = true;
        //    }
        //    else if (Convert.ToDecimal(txtWorkerDays.Text.Trim()) == 0)
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Minimum Days Worked Properly');", true);
        //        ErrFlag = true;
        //    }
        //    //else if (Convert.ToDecimal(txtMonthDays.Text.Trim()) == 0)
        //    //{
        //    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days of the Month Properly');", true);
        //    //    ErrFlag = true;
        //    //}
        //    //else if (txtMonthDays.Text.Trim() == "")
        //    //{
        //    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days of the Month');", true);
        //    //    ErrFlag = true;
        //    //}
        //    if (!ErrFlag)
        //    {
        //        //txtMonthDays.Text = "0";
        //        string monthday = txtMonthDays.Text;
        //        int Monthd = Convert.ToInt16(monthday);
        //        if (Monthd > 31)
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Sorry, Maximum Day of Month is 31');", true);
        //            ErrFlag = true;
        //        }
        //        else
        //        {

        //            DataTable dtdIncentive = new DataTable();
        //            SSQL = "select MonthDays from WorkerIncentive_mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and WorkerDays='"+txtWorkerDays.Text +"' ";
        //            dtdIncentive = objdata.ReturnMultipleValue(SSQL);
        //            if (dtdIncentive.Rows.Count > 0)
        //            {
        //                if (btnSave.Text == "Update")
        //                {
        //                    objdata.WorkerIncentive_Update(SessionCcode, SessionLcode, txtWorkerDays.Text, txtWorkerIncentive.Text, txtMonthDays.Text);
        //                    clr1();
        //                    Load_data1();
        //                    btnSave.Text = "Save";
        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successufully');", true);
        //                    ErrFlag = true;
        //                }
        //                else
        //                {
        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Data Already Exist For MonthDays');", true);
        //                    ErrFlag = true;
        //                    clr1();

        //                }
        //            }
        //            else
        //            {
        //                string WkDays = objdata.WorkerIncentive_Verify(SessionCcode, SessionLcode, txtWorkerDays.Text, txtMonthDays.Text);
        //                if (WkDays.Trim() == "")
        //                {
        //                    objdata.WorkerIncentive_Insert(SessionCcode, SessionLcode, txtWorkerDays.Text, txtWorkerIncentive.Text, txtMonthDays.Text);
        //                    SaveFlag = true;
        //                }
        //                else
        //                {
        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Data Already Exist');", true);
        //                    ErrFlag = true;
        //                    clr1();
        //                }
        //                if (SaveFlag == true)
        //                {
        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
        //                    clr1();
        //                    Load_data1();
        //                }
        //            }
        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //}


        try
        {
            bool flag = false;
            bool flag2 = false;
            if (this.txtWorkerDays.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Minimum Days Worked');", true);
                flag = true;
            }
            else if (this.txtWorkerIncentive.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Incentive Amount');", true);
                flag = true;
            }
            else if (Convert.ToDecimal(this.txtWorkerDays.Text.Trim()) == 0M)
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Minimum Days Worked Properly');", true);
                flag = true;
            }
            if (!flag)
            {
                if (Convert.ToInt16(this.txtMonthDays.Text) > 0x1f)
                {
                    ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Sorry, Maximum Day of Month is 31');", true);
                    flag = true;
                }
                else
                {
                    DataTable table = new DataTable();
                    this.SSQL = "select MonthDays from WorkerIncentive_mst where Ccode='" + this.SessionCcode + "' and Lcode='" + this.SessionLcode + "' and Monthdays='" + this.txtMonthDays.Text + "' and WorkerDays='" + this.txtWorkerDays.Text + "' ";
                    if (this.objdata.ReturnMultipleValue(this.SSQL).Rows.Count > 0)
                    {
                        if (this.btnSave.Text == "Update")
                        {
                            this.objdata.WorkerIncentive_Update(this.SessionCcode, this.SessionLcode, this.txtWorkerDays.Text, this.txtWorkerIncentive.Text, this.txtMonthDays.Text);
                            this.clr1();
                            this.Load_data1();
                            this.btnSave.Text = "Save";
                            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Updated Successufully');", true);
                            flag = true;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Data Already Exist For MonthDays');", true);
                            flag = true;
                            this.clr1();
                        }
                    }
                    else
                    {
                        if (this.objdata.WorkerIncentive_Verify(this.SessionCcode, this.SessionLcode, this.txtWorkerDays.Text, this.txtMonthDays.Text).Trim() == "")
                        {
                            this.objdata.WorkerIncentive_Insert(this.SessionCcode, this.SessionLcode, this.txtWorkerDays.Text, this.txtWorkerIncentive.Text, this.txtMonthDays.Text);
                            flag2 = true;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Data Already Exist');", true);
                            flag = true;
                            this.clr1();
                        }
                        if (flag2)
                        {
                            ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Saved Successfully');", true);
                            this.clr1();
                            this.Load_data1();
                        }
                    }
                }
            }
        }
        catch (Exception)
        {
        }
    }

   
    //protected void gvIncentive_RowUpdating(object sender, GridViewUpdateEventArgs e)
    //{
    //    bool ErrFlag = false;
    //    bool SaveFlag = false;
    //    TextBox val = (TextBox)gvIncentive.Rows[e.RowIndex].FindControl("txtgvDaystxt");
    //    if (val.Text.Trim() == "")
    //    {
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
    //        ErrFlag = true;
    //    }
    //    if (!ErrFlag)
    //    {
    //        Label lblDays = (Label)gvIncentive.Rows[e.RowIndex].FindControl("gvDays");
    //        Label lblMopnthDaysGet = (Label)gvIncentive.Rows[e.RowIndex].FindControl("gvMonthDays");
    //        objdata.WorkerIncentive_Update(SessionCcode, SessionLcode, lblDays.Text, val.Text, lblMopnthDaysGet.Text);
    //        SaveFlag = true;
    //    }
    //    if (SaveFlag == true)
    //    {
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
    //        gvIncentive.EditIndex = -1;
    //        Load_data1();
    //        clr1();
    //    }
    //}
    

    public void Load_grid()
    {
        //DataTable dt = new DataTable();
        //dt = objdata.HostelIncentive_Load(SessionCcode, SessionLcode);
        //if (dt.Rows.Count != 0)
        //{
        //    txtHostelAmt.Text = dt.Rows[0]["Amt"].ToString();
        //    if (dt.Rows[0]["WDays"].ToString().ToUpper() == "true".ToUpper()) { chkWorkDays.Checked = true; } else { chkWorkDays.Checked = false; }
        //    if (dt.Rows[0]["HAllowed"].ToString().ToUpper() == "true".ToUpper()) { chkHAllowed.Checked = true; } else { chkHAllowed.Checked = false; }
        //    if (dt.Rows[0]["OTDays"].ToString().ToUpper() == "true".ToUpper()) { chkOTDays.Checked = true; } else { chkOTDays.Checked = false; }
        //    if (dt.Rows[0]["NFHDays"].ToString().ToUpper() == "true".ToUpper()) { chkNFH.Checked = true; } else { chkNFH.Checked = false; }
        //    if (dt.Rows[0]["NFHWorkDays"].ToString().ToUpper() == "true".ToUpper()) { chkNFHWorked.Checked = true; } else { chkNFHWorked.Checked = false; }

        //    if (dt.Rows[0]["CalWDays"].ToString().ToUpper() == "true".ToUpper()) { chkAttnCalWDays.Checked = true; } else { chkAttnCalWDays.Checked = false; }
        //    if (dt.Rows[0]["CalHAllowed"].ToString().ToUpper() == "true".ToUpper()) { chkAttnCalHAllowed.Checked = true; } else { chkAttnCalHAllowed.Checked = false; }
        //    if (dt.Rows[0]["CalOTDays"].ToString().ToUpper() == "true".ToUpper()) { chkAttnCalOTDays.Checked = true; } else { chkAttnCalOTDays.Checked = false; }
        //    if (dt.Rows[0]["CalNFHDays"].ToString().ToUpper() == "true".ToUpper()) { chkAttnCalNFH.Checked = true; } else { chkAttnCalNFH.Checked = false; }
        //    if (dt.Rows[0]["CalNFHWorkDays"].ToString().ToUpper() == "true".ToUpper()) { chkAttnCalNFHWorked.Checked = true; } else { chkAttnCalNFHWorked.Checked = false; }
        //}
        //else
        //{
        //    txtHostelAmt.Text = "0.00";
        //    chkWorkDays.Checked = false; chkHAllowed.Checked = false; chkOTDays.Checked = false; chkNFH.Checked = false;
        //    chkAttnCalWDays.Checked = false; chkAttnCalHAllowed.Checked = false; chkAttnCalOTDays.Checked = false; chkAttnCalNFH.Checked = false;
        //}

        DataTable table = new DataTable();
        table = this.objdata.HostelIncentive_Load(this.SessionCcode, this.SessionLcode);
        if (table.Rows.Count != 0)
        {
            this.txtHostelAmt.Text = table.Rows[0]["Amt"].ToString();
            if (table.Rows[0]["WDays"].ToString().ToUpper() == "true".ToUpper())
            {
                this.chkWorkDays.Checked = true;
            }
            else
            {
                this.chkWorkDays.Checked = false;
            }
            if (table.Rows[0]["HAllowed"].ToString().ToUpper() == "true".ToUpper())
            {
                this.chkHAllowed.Checked = true;
            }
            else
            {
                this.chkHAllowed.Checked = false;
            }
            if (table.Rows[0]["OTDays"].ToString().ToUpper() == "true".ToUpper())
            {
                this.chkOTDays.Checked = true;
            }
            else
            {
                this.chkOTDays.Checked = false;
            }
            if (table.Rows[0]["NFHDays"].ToString().ToUpper() == "true".ToUpper())
            {
                this.chkNFH.Checked = true;
            }
            else
            {
                this.chkNFH.Checked = false;
            }
            if (table.Rows[0]["NFHWorkDays"].ToString().ToUpper() == "true".ToUpper())
            {
                this.chkNFHWorked.Checked = true;
            }
            else
            {
                this.chkNFHWorked.Checked = false;
            }
            if (table.Rows[0]["CalWDays"].ToString().ToUpper() == "true".ToUpper())
            {
                this.chkAttnCalWDays.Checked = true;
            }
            else
            {
                this.chkAttnCalWDays.Checked = false;
            }
            if (table.Rows[0]["CalHAllowed"].ToString().ToUpper() == "true".ToUpper())
            {
                this.chkAttnCalHAllowed.Checked = true;
            }
            else
            {
                this.chkAttnCalHAllowed.Checked = false;
            }
            if (table.Rows[0]["CalOTDays"].ToString().ToUpper() == "true".ToUpper())
            {
                this.chkAttnCalOTDays.Checked = true;
            }
            else
            {
                this.chkAttnCalOTDays.Checked = false;
            }
            if (table.Rows[0]["CalNFHDays"].ToString().ToUpper() == "true".ToUpper())
            {
                this.chkAttnCalNFH.Checked = true;
            }
            else
            {
                this.chkAttnCalNFH.Checked = false;
            }
            if (table.Rows[0]["CalNFHWorkDays"].ToString().ToUpper() == "true".ToUpper())
            {
                this.chkAttnCalNFHWorked.Checked = true;
            }
            else
            {
                this.chkAttnCalNFHWorked.Checked = false;
            }
        }
        else
        {
            this.txtHostelAmt.Text = "0.00";
            this.chkWorkDays.Checked = false;
            this.chkHAllowed.Checked = false;
            this.chkOTDays.Checked = false;
            this.chkNFH.Checked = false;
            this.chkAttnCalWDays.Checked = false;
            this.chkAttnCalHAllowed.Checked = false;
            this.chkAttnCalOTDays.Checked = false;
            this.chkAttnCalNFH.Checked = false;
        }
    }
    protected void gvHostel_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //Label lblAmt_value = (Label)gvHostel.Rows[e.RowIndex].FindControl("lblAmt_hostel");
        //objdata.HostelIncentive_Delete(SessionCcode, SessionLcode, lblAmt_value.Text.Trim());
    }
    protected void btnSave2_Click(object sender, EventArgs e)
    {
        //try
        //{
        //    bool ErrFlag = false;
        //    bool SaveFlag = true;
        //    if (txtHostelAmt.Text.Trim() == "")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Hostel Amount....! ');", true);
        //        ErrFlag = true;
        //    }
        //    if (!ErrFlag)
        //    {
        //        string Amt_1 = objdata.HostelIncentive_Verify(SessionCcode, SessionLcode, txtHostelAmt.Text.Trim());
        //        if (Amt_1.Trim() == "")
        //        {
        //            objdata.HostelAmt_Insert(SessionCcode, SessionLcode, txtHostelAmt.Text.Trim(), chkWorkDays.Checked.ToString(), chkHAllowed.Checked.ToString(), chkOTDays.Checked.ToString(), chkNFH.Checked.ToString(), chkAttnCalWDays.Checked.ToString(), chkAttnCalHAllowed.Checked.ToString(), chkAttnCalOTDays.Checked.ToString(), chkAttnCalNFH.Checked.ToString(), chkNFHWorked.Checked.ToString(), chkAttnCalNFHWorked.Checked.ToString());
        //            SaveFlag = true;
        //        }
        //        else
        //        {
        //            objdata.HostelAmt_Update(SessionCcode, SessionLcode, txtHostelAmt.Text.Trim(), chkWorkDays.Checked.ToString(), chkHAllowed.Checked.ToString(), chkOTDays.Checked.ToString(), chkNFH.Checked.ToString(), chkAttnCalWDays.Checked.ToString(), chkAttnCalHAllowed.Checked.ToString(), chkAttnCalOTDays.Checked.ToString(), chkAttnCalNFH.Checked.ToString(), chkNFHWorked.Checked.ToString(), chkAttnCalNFHWorked.Checked.ToString());
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully....! ');", true);
        //            SaveFlag = false;
        //            Load_grid();
        //        }
        //        if (SaveFlag == true)
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....! ');", true);
        //            Load_grid();
        //        }

        //    }
        //}
        //catch (Exception ex)
        //{
        //}


        try
        {
            bool flag = false;
            bool flag2 = true;
            if (this.txtHostelAmt.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Hostel Amount....! ');", true);
                flag = true;
            }
            if (!flag)
            {
                if (this.objdata.HostelIncentive_Verify(this.SessionCcode, this.SessionLcode, this.txtHostelAmt.Text.Trim()).Trim() == "")
                {
                    this.objdata.HostelAmt_Insert(this.SessionCcode, this.SessionLcode, this.txtHostelAmt.Text.Trim(), this.chkWorkDays.Checked.ToString(), this.chkHAllowed.Checked.ToString(), this.chkOTDays.Checked.ToString(), this.chkNFH.Checked.ToString(), this.chkAttnCalWDays.Checked.ToString(), this.chkAttnCalHAllowed.Checked.ToString(), this.chkAttnCalOTDays.Checked.ToString(), this.chkAttnCalNFH.Checked.ToString(), this.chkNFHWorked.Checked.ToString(), this.chkAttnCalNFHWorked.Checked.ToString());
                    flag2 = true;
                }
                else
                {
                    this.objdata.HostelAmt_Update(this.SessionCcode, this.SessionLcode, this.txtHostelAmt.Text.Trim(), this.chkWorkDays.Checked.ToString(), this.chkHAllowed.Checked.ToString(), this.chkOTDays.Checked.ToString(), this.chkNFH.Checked.ToString(), this.chkAttnCalWDays.Checked.ToString(), this.chkAttnCalHAllowed.Checked.ToString(), this.chkAttnCalOTDays.Checked.ToString(), this.chkAttnCalNFH.Checked.ToString(), this.chkNFHWorked.Checked.ToString(), this.chkAttnCalNFHWorked.Checked.ToString());
                    ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Update Successfully....! ');", true);
                    flag2 = false;
                    this.Load_grid();
                }
                if (flag2)
                {
                    ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Saved Successfully....! ');", true);
                    this.Load_grid();
                }
            }
        }
        catch (Exception)
        {
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    protected void BtnCivilIncenSave_Click(object sender, EventArgs e)
    {
        //    try
        //    {
        //        bool ErrFlag = false;
        //        bool SaveFlag = true;
        //        if (txtCivilEligibleDays.Text.Trim() == "")
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Eligible Days....! ');", true);
        //            ErrFlag = true;
        //        }
        //        if (txtCivilIncenAmount.Text.Trim() == "")
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Incentive Amount....! ');", true);
        //            ErrFlag = true;
        //        }
        //        if (!ErrFlag)
        //        {
        //            string Amt_1 = objdata.CivilIncentive_Verify(SessionCcode, SessionLcode, txtCivilEligibleDays.Text.Trim(), txtCivilIncenAmount.Text.Trim());
        //            if (Amt_1.Trim() == "")
        //            {
        //                objdata.CivilAmt_Insert(SessionCcode, SessionLcode, txtCivilEligibleDays.Text.Trim(), txtCivilIncenAmount.Text.Trim());
        //                SaveFlag = true;
        //            }
        //            else
        //            {
        //                objdata.CivilAmt_Update(SessionCcode, SessionLcode, txtCivilEligibleDays.Text.Trim(), txtCivilIncenAmount.Text.Trim());
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully....! ');", true);
        //                SaveFlag = false;
        //                Load_grid();
        //            }
        //            if (SaveFlag == true)
        //            {
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....! ');", true);
        //                Load_grid();
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        try
        {
            bool flag = false;
            bool flag2 = true;
            if (this.txtCivilEligibleDays.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Eligible Days....! ');", true);
                flag = true;
            }
            if (this.txtCivilIncenAmount.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Enter the Incentive Amount....! ');", true);
                flag = true;
            }
            if (!flag)
            {
                if (this.objdata.CivilIncentive_Verify(this.SessionCcode, this.SessionLcode, this.txtCivilEligibleDays.Text.Trim(), this.txtCivilIncenAmount.Text.Trim()).Trim() == "")
                {
                    this.objdata.CivilAmt_Insert(this.SessionCcode, this.SessionLcode, this.txtCivilEligibleDays.Text.Trim(), this.txtCivilIncenAmount.Text.Trim());
                    flag2 = true;
                }
                else
                {
                    this.objdata.CivilAmt_Update(this.SessionCcode, this.SessionLcode, this.txtCivilEligibleDays.Text.Trim(), this.txtCivilIncenAmount.Text.Trim());
                    ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Update Successfully....! ');", true);
                    flag2 = false;
                    this.Load_grid();
                }
                if (flag2)
                {
                    ScriptManager.RegisterStartupScript((Page)this, base.GetType(), "window", "alert('Saved Successfully....! ');", true);
                    this.Load_grid();
                }
            }
        }
        catch (Exception)
        {
        }
    }
}
