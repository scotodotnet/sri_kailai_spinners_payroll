﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;


public partial class PFandMaster : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();


       
        if (!IsPostBack)
        {
            ESIPF_load();
            ESI_load();
        }
    }
    public void ESIPF_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.ESI_PF_Load(SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {
            txtpf.Text = dt.Rows[0]["PF_per"].ToString();
            txtESI.Text = dt.Rows[0]["ESI_per"].ToString();
            txtstaff.Text = dt.Rows[0]["StaffSalary"].ToString();
            txtstamp.Text = dt.Rows[0]["StampCg"].ToString();
            txtVDAPoint1.Text = dt.Rows[0]["VDA1"].ToString();
            txtVDAPoint2.Text = dt.Rows[0]["VDA2"].ToString();
            txtSpinning.Text = dt.Rows[0]["Spinning"].ToString();
            txtDayIncentive.Text = dt.Rows[0]["DayIncentive"].ToString();
            txtHalf.Text = dt.Rows[0]["halfNight"].ToString();
            txtFull.Text = dt.Rows[0]["FullNight"].ToString();
            txtThree.Text = dt.Rows[0]["ThreeSideAmt"].ToString();
            txtEmployeerPF1.Text = dt.Rows[0]["EmployeerPFone"].ToString();
            txtEmployeerPF2.Text = dt.Rows[0]["EmployeerPFTwo"].ToString();
            txtEmployeerESI.Text = dt.Rows[0]["EmployeerESI"].ToString();

            if (dt.Rows[0]["Union_val"].ToString() == "1")
            {
                chkUnion.Checked = true;
            }
            else
            {
                chkUnion.Checked = false;
            }
        }
        else
        {
            txtpf.Text = "0.00";
            txtESI.Text = "0.00";
            txtstaff.Text = "0.00";
            txtSpinning.Text = "0.00";
            txtDayIncentive.Text = "0.00";
            txtHalf.Text = "0.00";
            txtFull.Text = "0.00";
            txtThree.Text = "0.00";
            txtEmployeerPF1.Text = "0.00";
            txtEmployeerPF2.Text = "0.00";
            txtEmployeerESI.Text = "0.00";
        }
    }
    protected void btnclr_Click(object sender, EventArgs e)
    {
        ESIPF_load();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            if (txtpf.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF %');", true);
                ErrFlag = true;
            }
            else if (txtESI.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI %');", true);
                ErrFlag = true;
            }
            else if (txtstaff.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Staff Salary');", true);
                ErrFlag = true;
            }
            else if (txtstamp.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Stamp Charge');", true);
                ErrFlag = true;
            }
            else if (txtVDAPoint1.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA Point 1');", true);
                ErrFlag = true;
            }
            else if (txtVDAPoint2.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA Point 2');", true);
                ErrFlag = true;
            }
            else if (txtEmployeerPF1.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the EMPLOYEER PF A/C ONE %');", true);
                ErrFlag = true;
            }
            //else if (txtEmployeerPF2.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the EMPLOYEER PF A/C TWO %');", true);
            //    ErrFlag = true;
            //}
            //else if (txtEmployeerESI.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the EMPLOYEER ESI %');", true);
            //    ErrFlag = true;
            //}
            //else if (txtSpinning.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Spinning Department Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtDayIncentive.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Day Incentive Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtHalf.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the O/N Shift Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtFull.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the F/N Shift Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (txtThree.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Three Side Amount');", true);
            //    ErrFlag = true;
            //}
            if (!ErrFlag)
            {
                if (Convert.ToDecimal(txtpf.Text.Trim()) == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF Properly');", true);
                    ErrFlag = true;
                }
                //else if (Convert.ToDecimal(txtESI.Text.Trim()) == 0)
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI Properly');", true);
                //    ErrFlag = true;
                //}
                

                else if (Convert.ToDecimal(txtEmployeerPF2.Text.Trim()) == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the EMPLOYEER PF A/C TWO Properly');", true);
                    ErrFlag = true;
                }
               

                //else if (Convert.ToDecimal(txtVDAPoint2.Text) > Convert.ToDecimal(txtVDAPoint1.Text))
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the VDA Points properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    string Union = "";
                    if (chkUnion.Checked == true)
                    {
                        Union = "1";
                    }
                    else
                    {
                        Union = "0";
                    }
                    string Db = objdata.ESIPF_verify(SessionCcode, SessionLcode);
                    if (Db.Trim() == "")
                    {
                        objdata.ESIPF_Insert(txtpf.Text.Trim(), txtESI.Text.Trim(), txtstaff.Text.Trim(), SessionCcode, SessionLcode, txtstamp.Text, txtVDAPoint1.Text, txtVDAPoint2.Text, Union, txtSpinning.Text.Trim(), txtHalf.Text.Trim(), txtFull.Text.Trim(), txtDayIncentive.Text.Trim(), txtThree.Text.Trim(), txtEmployeerPF1.Text.Trim(), txtEmployeerPF2.Text.Trim(), txtEmployeerESI.Text.Trim());
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                    }
                    else
                    {
                        objdata.ESIPF_Update(txtpf.Text.Trim(), txtESI.Text.Trim(), txtstaff.Text.Trim(), SessionCcode, SessionLcode, txtstamp.Text, txtVDAPoint1.Text, txtVDAPoint2.Text, Union, txtSpinning.Text.Trim(), txtHalf.Text.Trim(), txtFull.Text.Trim(), txtDayIncentive.Text.Trim(), txtThree.Text.Trim(), txtEmployeerPF1.Text.Trim(), txtEmployeerPF2.Text.Trim(), txtEmployeerESI.Text.Trim());
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully');", true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    
    protected void chkUnion_CheckedChanged(object sender, EventArgs e)
    {

    }
    
    public void ESI_load()
    {
        DataTable dt = new DataTable();
        dt = objdata.ESICode_GRID();
        rptrPFandMaster.DataSource = dt;
        rptrPFandMaster.DataBind();
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        //EmpType = commandArgs[0];
        //Category = commandArgs[1];
        //EmpTypecode = commandArgs[2];

        //switch (e.CommandName)
        //{
        //    case ("Delete"):
        //        DeleteRepeaterData(EmpTypecode, EmpType, Category);
        //        break;
        //    case ("Edit"):
        //        EditRepeaterData(EmpTypecode, EmpType, Category);
        //        break;
        //}
    }
    protected void btnSave1_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool SaveFlag = false;
            if (txtESI_mst.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ESI Company Code');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                string ESINo = objdata.ESICode_verify(txtESI_mst.Text.Trim());
                if (ESINo.Trim() == "")
                {
                    objdata.ESICode_Insert(txtESI_mst.Text.Trim());
                    SaveFlag = true;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert(' ESI Company Code Already Exisist');", true);
                }
                if (SaveFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                    ESI_load();
                    txtESI_mst.Text = "";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
}
