﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class RptDepartmentSalaryStrength : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    Int32 Staff_Strength_Tot;
    decimal Staff_Salary_Tot;
    Int32 Labour_Strength_Tot;
    decimal Labour_Salary_Tot;
    Int32 Total_Strength_Tot;
    decimal Total_Salary_Tot;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        string ss = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            //DropDwonCategory();
            //alldropdownadd();
            DropDwonCategory();
            //ESICode_load();
            int currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                txtFinancial_Year.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        //lblComany.Text = SessionCcode + " - " + SessionLcode;
        //lblusername.Text = Session["Usernmdisplay"].ToString();
    }

    public void DropDwonCategory()
    {
        DataTable dtcate = new DataTable();
        dtcate = objdata.DropDownCategory();
        ddlcategory.DataSource = dtcate;
        ddlcategory.DataTextField = "CategoryName";
        ddlcategory.DataValueField = "CategoryCd";
        ddlcategory.DataBind();
    }

    //saba
    public void alldropdownadd()
    {

        //Financial Year Add
        int currentYear = Utility.GetFinancialYear;
        txtFinancial_Year.Items.Add("----Select----");
        txtFrom_Month.Items.Add("----Select----");
        for (int i = 0; i <= 11; i++)
        {
            txtFinancial_Year.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
            currentYear = currentYear - 1;
        }

        //Month Add
        System.Globalization.DateTimeFormatInfo mfi = new
        System.Globalization.DateTimeFormatInfo();
        string strMonthName = "";
        for (int i = 3; i <= 11; i++)
        {
            //strMonthName = mfi.GetAbbreviatedMonthName(i + 1).ToString();
            strMonthName = mfi.GetMonthName(i + 1).ToString();
            txtFrom_Month.Items.Add(strMonthName);
        }
        for (int i = 0; i <= 2; i++)
        {
            strMonthName = mfi.GetMonthName(i + 1).ToString();
            txtFrom_Month.Items.Add(strMonthName);
        }
    }
    //saba end

    protected void btnexport_Click(object sender, EventArgs e)
    {
        //All Header Details Get
        string CmpName = "";
        string Cmpaddress = "";
        string Month_Name = "";
        string Fin_Year = "";
        bool ErrFlag = false;
        if (ddlrptReport.SelectedIndex == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Export Report Type');", true);
            //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        int YR = 0;
        if (txtFrom_Month.SelectedValue == "January")
        {
            YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
            YR = YR + 1;
        }
        else if (txtFrom_Month.SelectedValue == "February")
        {
            YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
            YR = YR + 1;
        }
        else if (txtFrom_Month.SelectedValue == "March")
        {
            YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
        }


        if (!ErrFlag)
        {
            DataTable Final_Tot_DT = new DataTable();
            Final_Tot_DT = (DataTable)Session["GridDataset"];
            //Final Total Assign Variable
            Staff_Strength_Tot = Convert.ToInt32(Final_Tot_DT.Compute("Sum(Staff_Strength)", ""));
            Staff_Salary_Tot = Convert.ToDecimal(Final_Tot_DT.Compute("Sum(Staff_Salary)", ""));
            Labour_Strength_Tot = Convert.ToInt32(Final_Tot_DT.Compute("Sum(Labour_Strength)", ""));
            Labour_Salary_Tot = Convert.ToDecimal(Final_Tot_DT.Compute("Sum(Labour_Salary)", ""));
            Total_Strength_Tot = Convert.ToInt32(Final_Tot_DT.Compute("Sum(Total_Strength)", ""));
            Total_Salary_Tot = Convert.ToDecimal(Final_Tot_DT.Compute("Sum(Total_Salary)", ""));

            DataTable dt = new DataTable();
            dt = objdata.Company_retrive(SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
            }
            if (txtFrom_Month.SelectedIndex != 0)
            {
                Month_Name = txtFrom_Month.SelectedItem.Text.ToString();
            }
            else
            {
                Month_Name = "ALL";
            }
            Fin_Year = txtFinancial_Year.SelectedItem.Text.ToString();

            if (ddlrptReport.SelectedValue == "1")
            {
                //Excel Write
                string attachment = "attachment; filename=SALARY_WITH_STRENGTH_REPORT.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                griddept.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr><td colspan='9'> </td></tr>");
                Response.Write("<tr align='Center'><td colspan='9'>" + CmpName + "</td></tr>");
                Response.Write("<tr align='Center'><td colspan='9'>" + SessionLcode + "</td></tr>");
                Response.Write("<tr align='Center'><td colspan='9'>" + Cmpaddress + "</td></tr>");
                Response.Write("<tr align='Center'><td colspan='9'>SALARY WITH STRENGTH REPORT THE MONTH OF " + txtFrom_Month.SelectedValue + " - " + YR + "</td></tr>");
                Response.Write("<tr align='Center'><td colspan='9'> </td></tr>");
                //
                Response.Write("<tr>");
                Response.Write("<td> </td>");
                Response.Write("<td colspan='4'>Month : " + Month_Name + "</td>");
                Response.Write("<td colspan='4' align='right'>Financial Year : " + Fin_Year + "</td>");
                Response.Write("</tr>");
                //
                Response.Write("<tr align='Center'><td colspan='9'> </td></tr>");

                Response.Write("</table>");
                Response.Write("<table><tr><td></td><td>" + stw.ToString() + "</td></tr></table>");

                //Final Total Add
                Response.Write("<table><tr>");
                Response.Write("<td> </td>");
                Response.Write("<td>");
                Response.Write("<table border='1'>");
                Response.Write("<tr>");
                Response.Write("<td colspan='2' align='Center'>TOTAL STRENGTH</td>");
                Response.Write("<td>" + Staff_Strength_Tot + "</td>");
                Response.Write("<td>" + Staff_Salary_Tot + "</td>");
                Response.Write("<td>" + Labour_Strength_Tot + "</td>");
                Response.Write("<td>" + Labour_Salary_Tot + "</td>");
                Response.Write("<td>" + Total_Strength_Tot + "</td>");
                Response.Write("<td>" + Total_Salary_Tot + "</td>");
                Response.Write("</table>");
                Response.Write("</td></tr></table>");

                Response.End();
            }
            else if (ddlrptReport.SelectedValue == "2")
            {

                string attachment = "attachment; filename=SalaryStrength.pdf";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/pdf";
                StringWriter stw = new StringWriter();

                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                griddept.RenderControl(htextw);

                Document document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);
                PdfWriter.GetInstance(document, Response.OutputStream);
                document.Open();
                StringReader str = new StringReader(stw.ToString());
                HTMLWorker htmlworker = new HTMLWorker(document);
                htmlworker.Parse(str);
                document.Close();
                Response.Write(document);
                Response.End();
            }
            else if (ddlrptReport.SelectedValue == "3")
            {

                string attachment = "attachment; filename=SalaryStrength.doc";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);

                griddept.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr><td colspan='9'> </td></tr>");
                Response.Write("<tr align='Center'><td colspan='9'>Department Wise Salary Strength Details</td></tr>");
                Response.Write("<tr align='Center'><td colspan='9'>" + CmpName + "</td></tr>");
                Response.Write("<tr align='Center'><td colspan='9'>" + SessionLcode + "</td></tr>");
                Response.Write("<tr align='Center'><td colspan='9'>" + Cmpaddress + "</td></tr>");
                Response.Write("<tr align='Center'><td colspan='9'> </td></tr>");
                //
                Response.Write("<tr>");
                Response.Write("<td> </td>");
                Response.Write("<td colspan='4'>Month : " + Month_Name + "</td>");
                Response.Write("<td colspan='4' align='right'>Financial Year : " + Fin_Year + "</td>");
                Response.Write("</tr>");
                //
                Response.Write("<tr align='Center'><td colspan='9'> </td></tr>");

                Response.Write("</table>");
                Response.Write("<table><tr><td></td><td>" + stw.ToString() + "</td></tr></table>");

                //Final Total Add
                Response.Write("<table><tr>");
                Response.Write("<td> </td>");
                Response.Write("<td>");
                Response.Write("<table border='1'>");
                Response.Write("<tr>");
                Response.Write("<td colspan='2' align='Center'>TOTAL STRENGTH</td>");
                Response.Write("<td>" + Staff_Strength_Tot + "</td>");
                Response.Write("<td>" + Staff_Salary_Tot + "</td>");
                Response.Write("<td>" + Labour_Strength_Tot + "</td>");
                Response.Write("<td>" + Labour_Salary_Tot + "</td>");
                Response.Write("<td>" + Total_Strength_Tot + "</td>");
                Response.Write("<td>" + Total_Salary_Tot + "</td>");
                Response.Write("</table>");
                Response.Write("</td></tr></table>");
                Response.End();
            }
        }

    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnClick_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string Query_Val = "";
        string From_Month_DB = "";
        string Department_Name_Check = "";
        ErrFlag = false;
        try
        {
            if (txtFrom_Month.SelectedIndex == 0 && txtFinancial_Year.SelectedIndex == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Month and Financial Year');", true);
                //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            //Sabapathy
            if (txtFinancial_Year.SelectedIndex == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Financial Year');", true);
                //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                //DataTable
                DataTable Final_DT = new DataTable();
                DataTable Staff_DT = new DataTable();
                DataTable Labour_DT = new DataTable();
                DataTable Staff_Labour_DT = new DataTable();

                //From Month
                if (txtFrom_Month.SelectedValue != "0") { From_Month_DB = txtFrom_Month.SelectedItem.Text.ToString(); }

                int Fin_Year_DB = 0;
                string[] Fin_Year_Split = txtFinancial_Year.SelectedItem.Text.Split('-');
                Fin_Year_DB = Convert.ToInt32(Fin_Year_Split[0].ToString());
                //All Department Query List
                Query_Val = "";
                Query_Val = "Select distinct MD.DepartmentNm,0 as Staff_Strength,0.00 as Staff_Salary,";
                Query_Val = Query_Val + " 0 as Labour_Strength,0.00 as Labour_Salary,0 as Total_Strength,";
                Query_Val = Query_Val + " 0.00 as Total_Salary from EmployeeDetails ED,MstDepartment MD";
                Query_Val = Query_Val + " where MD.DepartmentCd=ED.Department and ED.Ccode='" + SessionCcode + "'";
                Query_Val = Query_Val + " and ED.Lcode='" + SessionLcode + "' Order by MD.DepartmentNm Asc";
                Final_DT = objdata.RptEmployeeMultipleDetails(Query_Val);

                //Staff_Strength Salary
                Query_Val = "";
                Query_Val = "Select MD.DepartmentNm,Count(MD.DepartmentNm) as Staff_Strength,CONVERT(DECIMAL(10,2),sum(SD.NetPay)) as Staff_Salary";
                Query_Val = Query_Val + " from EmployeeDetails ED,MstDepartment MD,SalaryDetails SD";
                Query_Val = Query_Val + " where MD.DepartmentCd=ED.Department and SD.EmpNo=ED.EmpNo";
                Query_Val = Query_Val + " and ED.stafforlabor = 'S' and ED.Ccode='" + SessionCcode + "'";
                Query_Val = Query_Val + " and ED.Lcode='" + SessionLcode + "'";
                if (txtFrom_Month.SelectedIndex != 0) { Query_Val = Query_Val + " and SD.Month='" + txtFrom_Month.SelectedItem.Text.ToString() + "'"; }
                if (txtFinancial_Year.SelectedIndex != 0) { Query_Val = Query_Val + " and FinancialYear='" + Fin_Year_DB + "'"; }
                Query_Val = Query_Val + " Group by MD.DepartmentNm Order by MD.DepartmentNm Asc";
                Staff_DT = objdata.RptEmployeeMultipleDetails(Query_Val);

                //Labour_Strength Salary
                Query_Val = "";
                Query_Val = "Select MD.DepartmentNm,Count(MD.DepartmentNm) as Labour_Strength,CONVERT(DECIMAL(10,2),sum(SD.NetPay)) as Labour_Salary";
                Query_Val = Query_Val + " from EmployeeDetails ED,MstDepartment MD,SalaryDetails SD";
                Query_Val = Query_Val + " where MD.DepartmentCd=ED.Department and SD.EmpNo=ED.EmpNo";
                Query_Val = Query_Val + " and ED.stafforlabor = 'L' and ED.Ccode='" + SessionCcode + "'";
                Query_Val = Query_Val + " and ED.Lcode='" + SessionLcode + "'";
                if (txtFrom_Month.SelectedIndex != 0) { Query_Val = Query_Val + " and SD.Month='" + txtFrom_Month.SelectedItem.Text.ToString() + "'"; }
                if (txtFinancial_Year.SelectedIndex != 0) { Query_Val = Query_Val + " and FinancialYear='" + Fin_Year_DB + "'"; }
                Query_Val = Query_Val + " Group by MD.DepartmentNm Order by MD.DepartmentNm Asc";
                Labour_DT = objdata.RptEmployeeMultipleDetails(Query_Val);

                //Staff And Labour_Strength Total Salary
                Query_Val = "";
                Query_Val = "Select MD.DepartmentNm,Count(MD.DepartmentNm) as Total_Strength,CONVERT(DECIMAL(10,2),sum(SD.NetPay)) as Total_Salary";
                Query_Val = Query_Val + " from EmployeeDetails ED,MstDepartment MD,SalaryDetails SD";
                Query_Val = Query_Val + " where MD.DepartmentCd=ED.Department and SD.EmpNo=ED.EmpNo";
                Query_Val = Query_Val + " and ED.Ccode='" + SessionCcode + "'";
                Query_Val = Query_Val + " and ED.Lcode='" + SessionLcode + "'";
                if (txtFrom_Month.SelectedIndex != 0) { Query_Val = Query_Val + " and SD.Month='" + txtFrom_Month.SelectedItem.Text.ToString() + "'"; }
                if (txtFinancial_Year.SelectedIndex != 0) { Query_Val = Query_Val + " and FinancialYear='" + Fin_Year_DB + "'"; }
                Query_Val = Query_Val + " Group by MD.DepartmentNm Order by MD.DepartmentNm Asc";
                Staff_Labour_DT = objdata.RptEmployeeMultipleDetails(Query_Val);

                //Merge DataTable
                for (int i = 0; i <= Final_DT.Rows.Count - 1; i++)
                {
                    Department_Name_Check = Final_DT.Rows[i]["DepartmentNm"].ToString();
                    //Staff_Check
                    for (int j = 0; j <= Staff_DT.Rows.Count - 1; j++)
                    {
                        if (Department_Name_Check.ToUpper() == Staff_DT.Rows[j]["DepartmentNm"].ToString().ToUpper())
                        {
                            Final_DT.Rows[i]["Staff_Strength"] = Staff_DT.Rows[j]["Staff_Strength"].ToString();
                            Final_DT.Rows[i]["Staff_Salary"] = Staff_DT.Rows[j]["Staff_Salary"].ToString();
                            break;
                        }
                    }
                    //Labour_Check
                    for (int k = 0; k <= Labour_DT.Rows.Count - 1; k++)
                    {
                        if (Department_Name_Check.ToUpper() == Labour_DT.Rows[k]["DepartmentNm"].ToString().ToUpper())
                        {
                            Final_DT.Rows[i]["Labour_Strength"] = Labour_DT.Rows[k]["Labour_Strength"].ToString();
                            Final_DT.Rows[i]["Labour_Salary"] = Labour_DT.Rows[k]["Labour_Salary"].ToString();
                            break;
                        }
                    }
                    //Staff_Labour Total
                    for (int L = 0; L <= Staff_Labour_DT.Rows.Count - 1; L++)
                    {
                        if (Department_Name_Check.ToUpper() == Staff_Labour_DT.Rows[L]["DepartmentNm"].ToString().ToUpper())
                        {
                            Final_DT.Rows[i]["Total_Strength"] = Staff_Labour_DT.Rows[L]["Total_Strength"].ToString();
                            Final_DT.Rows[i]["Total_Salary"] = Staff_Labour_DT.Rows[L]["Total_Salary"].ToString();
                            break;
                        }
                    }
                }
                Session["GridDataset"] = Final_DT;

                //Final Result
                Result_Panel.Visible = true;
                griddept.DataSource = Final_DT;
                griddept.DataBind();

            }
            //Sabapathy END

        }
        catch (Exception ex)
        { }
    }

    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
    protected void btnLeave_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptLeaveSample.aspx");
    }
    protected void btnOT_Click(object sender, EventArgs e)
    {
        Response.Redirect("OTDownload.aspx");
    }
    protected void btncontract_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContractRPT.aspx");
    }
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();


        if (ddlcategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlcategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        //EmployeeType();
        EmployeeType_Load();
    }
    public void EmployeeType_Load()
    {
        DataTable dtemp = new DataTable();
        dtemp = objdata.EmployeeDropDown_no(ddlcategory.SelectedValue);
        txtEmployeeType.DataSource = dtemp;
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }

    protected void btnELView_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;



            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "1")
                    {
                        Stafflabour = "S";
                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {
                        Stafflabour = "L";
                    }
                    string EligibleWorkDays = txtEligbleDays.Text.ToString();
                    string Str_ChkBelow = "";
                    if (ChkBelow.Checked == true)
                    {
                        Str_ChkBelow = "1";
                    }
                    else { Str_ChkBelow = "0"; }

                    string Str_ChkLeft = "";
                    if (ChkLeft.Checked == true)
                    {
                        Str_ChkLeft = "1";
                    }
                    else { Str_ChkLeft = "0"; }

                    string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();
                    string Str_PFType = RdbPFNonPF.SelectedValue.ToString();

                    ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + "" + "&Months=" + "" + "&yr=" + txtFinancial_Year.SelectedValue + "&fromdate=" + "" + "&ToDate=" + "" + "&Salary=" + "" + "&CashBank=" + "" + "&ESICode=" + "" + "&EmpType=" + txtEmployeeType.SelectedValue.ToString() + "&PayslipType=" + Payslip_Format_Type + "&PFTypePost=" + Str_PFType + "&Left_Emp=" + Str_ChkLeft + "&EligbleWDays=" + EligibleWorkDays + "&Leftdate=" + txtLeftDate.Text + "&Report_Type=ELCL_REPORT" + "&BelowCheck=" + Str_ChkBelow, "_blank", "");

                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void txtLeftDate_TextChanged(object sender, EventArgs e)
    {

    }


}
