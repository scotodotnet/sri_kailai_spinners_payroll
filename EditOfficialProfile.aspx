﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EditOfficialProfile.aspx.cs" Inherits="EditOfficialProfile" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
    
             $(document).ready(function () {
            $('#tableEmployee').dataTable();
        });
       </script>


<asp:UpdatePanel ID="updatepanel1" runat="server" >
<ContentTemplate>
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Edit Official Profile</li>
                       <h4>
                       </h4>
                        <h4>
                       </h4>
                        <h4>
                       </h4>
                        <h4>
                       </h4>
                        </h4> 
                    </ol>
               </div>
 
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Official Profile</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body"> 
                
                    
 <div class="col-md-12">     
				        <div class="form-group row">
				         <asp:Label ID="lblCategory" runat="server" class="col-sm-2 control-label" 
                                                             Text="Category"></asp:Label>
				         <div class="col-sm-2">
					      <asp:DropDownList ID="ddlcategory" class="form-control" runat="server" onselectedindexchanged="ddlcategory_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>  
					      </div>
					    
					
						<div class="col-md-1">
                          
                        </div>
                        <asp:Label ID="lbldepatment" runat="server" class="col-sm-2 control-label" 
                                                             Text="Department"></asp:Label>
					
					   <div class="col-sm-2">
						<asp:DropDownList ID="ddldepartment" class="form-control" onselectedindexchanged="ddldepartment_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            </asp:DropDownList> 
					  </div>
						
					  <div class="col-sm-3">
                         <asp:Button ID="btnclick" class="btn btn-info"  runat="server" onclick="btnclick_Click" Text="Click"/>
                      </div>
						</div>
				        
                       </div> 

 <asp:Panel ID="panelError" runat="server" Visible="false">
    <div class="col-md-12">     
				        <div class="form-group row">
				         <asp:Label ID="lblError" runat="server" class="col-sm-2 control-label"></asp:Label>
			            </div>
	</div>
 </asp:Panel>

 <div class="col-md-12">
              <div class="row">
              
                  <div class="table-responsive">
        <asp:Repeater ID="rptrEmployee" runat="server" onitemcommand="Repeater1_ItemCommand">
                    <HeaderTemplate>
                       <table id="tableEmployee" class="display table">
                    <thead>
                        <tr>
                        <th>TokenNo</th>
                        <th>EMPNo</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>DeptName</th>
                        <th>Designation</th>
                        <th>Category</th>
                        <th>Add</th>
                     </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                 <td>
                    <%# DataBinder.Eval(Container.DataItem, "ExisistingCode")%>
                    </td>
                    
                    <td>
                    <%# DataBinder.Eval(Container.DataItem, "EmpNo")%>
                    </td>
                    <td>
                    <%# DataBinder.Eval(Container.DataItem, "EmpName")%>
                    </td>
                    <td><%# DataBinder.Eval(Container.DataItem, "Gender")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "DepartmentNm")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "Designation")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "Staff")%></td>
                    
                     <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("EmpNo") %>' CommandName="Add"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>
                    
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
                        </div>
 </div>
 <div class="col-md-12"><hr></div>
 
 <div class="form-group row">
					  <asp:Label ID="lblempno" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee No"></asp:Label>
						<div class="col-sm-3">
                            <asp:TextBox ID="txtempno" class="form-control" runat="server"></asp:TextBox>  
                                                                
                        </div>
					
				    <div class="col-sm-1"></div>
				       				    
				   <asp:Label ID="lblempname" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee Name"></asp:Label>
				     <%--<label for="input-Default" class="col-sm-2 control-label">Employee Name</label>--%>
					 <div class="col-sm-3">
                     <asp:Label ID="lblempname1" runat="server" class="control-label" 
                                                             Text=""></asp:Label>   
					  </div>
			       </div>
			       
<%--<div class="form-group row">
					  <asp:Label ID="Label1" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee No"></asp:Label>
						<div class="col-sm-3">
                            <asp:TextBox ID="TextBox1" class="form-control" runat="server"></asp:TextBox>  
                                                                
                        </div>
					
				    <div class="col-sm-1"></div>
				       				    
				   <asp:Label ID="Label2" runat="server" class="col-sm-2 control-label" 
                                                             Text="Employee Name"></asp:Label>
				  
					 <div class="col-sm-3">
                     <asp:Label ID="Label3" runat="server" class="control-label" 
                                                             Text=""></asp:Label>   
					  </div>
			       </div>
--%>			       <div class="form-group row">
			    <asp:Panel ID="PanelLabour" runat="server" Visible="false">   
			        
					  <asp:Label ID="lbllabourty" runat="server" class="col-sm-2 control-label" 
                                                             Text="Labour Type"></asp:Label>
						<div class="col-sm-3">
                            <asp:TextBox ID="txtlabourtype" class="form-control" runat="server"></asp:TextBox>  
                                                                
                        </div>
                         <div class="col-sm-1"></div>
                         <div class="col-sm-3">
                         <asp:Panel id="PanelContarctTypeLabel" runat="server" visible="false" >                                                     
                          <asp:Label ID="lblcontratctyp" runat="server" Text="Contract Type" class="form-control" Font-Bold="true"></asp:Label>
                           </asp:Panel>
                         </div>
                         </asp:Panel>
				       				    
				 <asp:Panel  id="PanelContarctTypeRadio" runat="server" visible="false" >
					 <div class="col-sm-3">
                     <asp:RadioButtonList ID="rbtncontract" runat="server" RepeatColumns="2">
                                                            <asp:ListItem Text="Days" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Month" Value="2"></asp:ListItem>
                                                            </asp:RadioButtonList> 
					  </div>
					   </asp:Panel> 
			       </div>
			     
			       <asp:Panel ID="PanelOthers" runat="server" Visible="false"> 
			        <div class="form-group row">
					   <label for="input-Default" class="col-sm-2 control-label">Wages</label>
						<div class="col-sm-4">
                             <asp:RadioButtonList ID="rbtnotherwages" runat="server" RepeatColumns="3" >
                             <asp:ListItem Text="Weekly Wages" Value="1"></asp:ListItem>
                             <asp:ListItem Text="Monthly Wages" Value="2"></asp:ListItem> 
	                         <asp:ListItem Text="Bi-Monthly Wages" Value="3"></asp:ListItem>                                            
	                         </asp:RadioButtonList>                               
                        </div>
			       
			       </div>
			       </asp:Panel>
			       
			       
				                
				    <div class="form-group row">
					  <asp:Label ID="lbldobjoin" runat="server" class="col-sm-2 control-label" 
                                                             Text="Date Of Joining"></asp:Label>
						<div class="col-sm-3">
                            <asp:TextBox ID="txtdobjoin" class="form-control" runat="server"
                            ontextchanged="txtdobjoin_TextChanged" AutoPostBack="true"></asp:TextBox>  
                            <cc1:CalendarExtender ID="Caleder_txtdob" runat="server" TargetControlID="txtdobjoin"
                              Format="dd-MM-yyyy" CssClass="orange">
                            </cc1:CalendarExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR11" runat="server"
                              FilterMode="ValidChars" FilterType="Custom,Numbers"
                              TargetControlID="txtdobjoin" ValidChars="0123456789/-">
                            </cc1:FilteredTextBoxExtender>                                    
                        </div>
					
				    <div class="col-sm-1"></div>
				       				    
				     <asp:Label ID="lblprobperiod" runat="server" class="col-sm-2 control-label" 
                                                             Text="Probation Period"></asp:Label>
				    
					 <div class="col-sm-3">
                         <asp:DropDownList ID="ddlprobation" class="form-control" runat="server" Enabled="false">
                         </asp:DropDownList>
                      
                                       
					  </div>
			       </div>            
				                   
				    <div class="form-group row">
					  <asp:Label ID="lblDuration" runat="server" class="col-sm-2 control-label" 
                                                             Text="Probationary Period Duration"></asp:Label>
						<div class="col-sm-3">
                            <asp:TextBox ID="txtDuration" class="form-control" runat="server" Enabled="false"></asp:TextBox>  
                                                                
                        </div>
					
				    <div class="col-sm-1"></div>
				       				  
				     
				     <label for="input-Default" class="col-sm-2 control-label">Special Salary for PF Calculation</label>
					 <div class="col-sm-3">
                     <asp:CheckBox ID="chkpf" runat="server" />
                      
                                       
					  </div>
			       </div>     
			       
			        <div class="form-group row">
			        <asp:Label ID="lblprofileType" runat="server" class="col-sm-2 control-label" 
                                                             Text="Profile Type"></asp:Label>
					  
						<div class="col-sm-4">
                             <asp:RadioButtonList ID="rbtnProfie" runat="server" RepeatColumns="3" AutoPostBack="true" 
                                                                    onselectedindexchanged="rbtnProfie_SelectedIndexChanged">
                             <asp:ListItem Text="Temporary" Value="1"></asp:ListItem>
                             <asp:ListItem Text="Confirmation" Value="2"></asp:ListItem> 
	                         <asp:ListItem Text="General" Value="3"></asp:ListItem>                                            
	                         </asp:RadioButtonList>                               
                        </div>
			       
			       </div>
			       
			       <asp:Panel ID="panelContract" runat="server" Visible="false"> 
			        <div class="form-group row">
			        <asp:Label ID="lblContract" runat="server" class="col-sm-2 control-label" 
                                                             Text="Profile Type"></asp:Label>
					  
						<div class="col-sm-4">
						<asp:DropDownList ID="ddlcontract" runat="server" class="form-control" Enabled="false" ></asp:DropDownList></td>
                                                            
                        </div>
			       
			       </div>
			       </asp:Panel>
			       
			        <div class="form-group row">
					  <asp:Label ID="lblElgiblePF" runat="server" class="col-sm-2 control-label" 
                                                             Text="Elgible for PF"></asp:Label>
						<div class="col-sm-3">
                          <asp:RadioButtonList ID="rbPF" runat="server" AutoPostBack="true" RepeatColumns ="2"
                          onselectedindexchanged="rbPF_SelectedIndexChanged">
                                 <asp:ListItem Text="Yes" Value="1" Selected="True" ></asp:ListItem>
                                 <asp:ListItem Text="No" Value="2"></asp:ListItem>
                          </asp:RadioButtonList> 
                                                                
                        </div>
					
				    <div class="col-sm-1"></div>
				       <asp:Label ID="lblESICCode" runat="server" class="col-sm-2 control-label" 
                                                             Text="ESI Company Code"></asp:Label>				    
				  
					 <div class="col-sm-3">
                         <asp:DropDownList ID="ddESI" class="form-control" runat="server" AutoPostBack="true">
                         </asp:DropDownList>
  				  </div>
			       </div>   
         
                    <div class="form-group row">
					  <asp:Label ID="lblconfirmationdate" runat="server" class="col-sm-2 control-label" 
                                                             Text="Confirmation Date"></asp:Label>
						<div class="col-sm-3">
                        <asp:TextBox ID="txtconfirmationdate" class="form-control" runat="server"
                        ontextchanged="txtconfirmationdate_TextChanged"></asp:TextBox>  
                        <cc1:CalendarExtender ID="Caledercondate" runat="server" TargetControlID="txtconfirmationdate"
                              Format="dd-MM-yyyy" CssClass="orange">
                            </cc1:CalendarExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR14" runat="server"
                              FilterMode="ValidChars" FilterType="Custom,Numbers"
                              TargetControlID="txtconfirmationdate" ValidChars="0123456789/-">
                            </cc1:FilteredTextBoxExtender>                                        
                        </div>
					
				    <div class="col-sm-1"></div>
				      <asp:Label ID="pfnumber" runat="server" class="col-sm-2 control-label" 
                                                             Text="PF Number"></asp:Label> 				    
				   	 <div class="col-sm-3">
                      <asp:TextBox ID="txtpfnumber" class="form-control" runat="server"></asp:TextBox>  
                                       
					  </div>
			       </div>
			       
			       
			        <div class="form-group row">
					  <asp:Label ID="lblPFDOJ" runat="server" class="col-sm-2 control-label" 
                                                             Text="PF DOJ"></asp:Label>
						<div class="col-sm-3">
                            <asp:TextBox ID="txtPFDOJ" class="form-control" runat="server"></asp:TextBox>  
                             <cc1:CalendarExtender ID="CalendarExtender12" runat="server" TargetControlID="txtPFDOJ"
                              Format="dd-MM-yyyy" CssClass="orange">
                            </cc1:CalendarExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR15" runat="server"
                              FilterMode="ValidChars" FilterType="Custom,Numbers"
                              TargetControlID="txtPFDOJ" ValidChars="0123456789/-">
                            </cc1:FilteredTextBoxExtender>                                   
                        </div>
					
				    <div class="col-sm-1"></div>
				       				    
				     <label for="input-Default" class="col-sm-2 control-label">ESI DOJ</label>
					 <div class="col-sm-3">
                      <asp:TextBox ID="txtESIDOJ" class="form-control" runat="server" ontextchanged="txtESIDOJ_TextChanged" AutoPostBack="true"></asp:TextBox>  
                       <cc1:CalendarExtender ID="CalendarExtender13" runat="server" TargetControlID="txtESIDOJ"
                              Format="dd-MM-yyyy" CssClass="orange">
                            </cc1:CalendarExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtendeR12" runat="server"
                              FilterMode="ValidChars" FilterType="Custom,Numbers"
                              TargetControlID="txtESIDOJ" ValidChars="0123456789/-">
                            </cc1:FilteredTextBoxExtender>                
					  </div>
			       </div>
			      
			        <div class="form-group row">
					  <asp:Label ID="lblpannumber" runat="server" class="col-sm-2 control-label" 
                                                             Text="PAN Number"></asp:Label>
						<div class="col-sm-3">
                            <asp:TextBox ID="txtpannumber" class="form-control" runat="server"></asp:TextBox>  
                                                                
                        </div>
					
				    <div class="col-sm-1"></div>
				       				  
				     <asp:Label ID="lblFinance" runat="server" class="col-sm-2 control-label" 
                                                             Text="Financial Period"></asp:Label>  
				  
					 <div class="col-sm-3">
                         <asp:DropDownList ID="ddlFinancialPeriod" class="form-control" runat="server" Enabled="false">
                         </asp:DropDownList>
                                       
					  </div>
			       </div>
			       
			       
			        <div class="form-group row">
					  <asp:Label ID="lblESI" runat="server" class="col-sm-2 control-label" 
                                                             Text="Eligible for ESI"></asp:Label>
						<div class="col-sm-3">
                          <asp:RadioButtonList ID="rbtnesi" runat="server" AutoPostBack="true" RepeatColumns ="2"
                          onselectedindexchanged="rbtnesi_SelectedIndexChanged">
                                 <asp:ListItem Text="Yes" Value="1" Selected="True" ></asp:ListItem>
                                 <asp:ListItem Text="No" Value="2"></asp:ListItem>
                          </asp:RadioButtonList> 
                                                                
                        </div>
					
				    <div class="col-sm-1"></div>
				       				 
				       <asp:Label ID="lblesic" runat="server" class="col-sm-2 control-label" 
                                                             Text="ESI Number"></asp:Label> 		   
				  
					 <div class="col-sm-3">
                    <asp:TextBox ID="txtesic" class="form-control" runat="server"></asp:TextBox>  
  				  </div>
			       </div> 
			       
			       
			        <div class="form-group row">
					  <asp:Label ID="lblinsuracmpnm" runat="server" class="col-sm-2 control-label" 
                                                             Text="Insurance Company Name"></asp:Label>
						<div class="col-sm-3">
                         <asp:DropDownList ID="ddlInsurance" class="form-control" runat="server">
                         </asp:DropDownList>
                        </div>
					
				    <div class="col-sm-1"></div>
				       				  
				 <asp:Label ID="lblinsuranceno" runat="server" class="col-sm-2 control-label" 
                                                             Text="Insurance Number"></asp:Label>  
				
					 <div class="col-sm-3">
                       <asp:TextBox ID="txtinsuranceno" class="form-control" runat="server"></asp:TextBox>  
                     </div>
			       </div>
			       
			       
			        <div class="form-group row">
					  <asp:Label ID="lblsalarythroug" runat="server" class="col-sm-2 control-label" 
                                                             Text="Salary Through"></asp:Label>
						<div class="col-sm-3">
                          <asp:RadioButtonList ID="rbtnsalarythroug" runat="server" AutoPostBack="true" RepeatColumns ="2"
                          onselectedindexchanged="rbtnsalarythroug_SelectedIndexChanged">
                                 <asp:ListItem Text="Cash" Value="1" Selected="True" ></asp:ListItem>
                                 <asp:ListItem Text="Bank" Value="2"></asp:ListItem>
                          </asp:RadioButtonList> 
                                                                
                        </div>
					
				    <div class="col-sm-1"></div>
				       		
			<asp:Label ID="lblbankacno" runat="server" class="col-sm-2 control-label" 
                                                             Text="BankAccountNumber"></asp:Label>	       			    
				<div class="col-sm-3">
                    <asp:TextBox ID="txtbankacno" class="form-control" runat="server"></asp:TextBox>  
  				  </div>
			       </div> 
			       
			       
			        <div class="form-group row">
					  <asp:Label ID="lblbankname" runat="server" class="col-sm-2 control-label" 
                                                             Text="Bank Name"></asp:Label>
						<div class="col-sm-3">
                            <asp:DropDownList ID="ddlbankname" class="form-control" runat="server" Enabled="false">
                            </asp:DropDownList>                                
                        </div>
					
				    <div class="col-sm-1"></div>
				      <asp:Label ID="lblbranch" runat="server" class="col-sm-2 control-label" 
                                                             Text="Branch Name"></asp:Label>  				    
				  
				 <div class="col-sm-3">
                    <asp:TextBox ID="txtbranch" class="form-control" runat="server" Enabled="false"></asp:TextBox>  
  				  </div>
			       </div> 
			       
			       
			         <asp:Panel ID="PanelReportAuthrity" runat="server" Visible="false"> 
			       <div class="form-group row">
					  <asp:Label ID="lblreportingauthritynm" runat="server" class="col-sm-2 control-label" 
                                                             Text="Reporting Authority Name"></asp:Label>
						<div class="col-sm-3">
                         <asp:TextBox ID="txtauthoritynm" class="form-control" runat="server" ></asp:TextBox> 
                                                                
                        </div>
					
				    <div class="col-sm-1"></div>
				       		
			<asp:Label ID="lblovertime" runat="server" class="col-sm-2 control-label" 
                                                             Text="Eligible For Over Time"></asp:Label>	       			    
				<div class="col-sm-3">
                   <asp:RadioButtonList ID="rbtnovertime" runat="server" RepeatColumns="2">
                                                            <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="No" Value="2" Selected="False"></asp:ListItem>
                                                            </asp:RadioButtonList>  
  				  </div>
			       </div>
			       </asp:Panel>
			       
			       
			     
			        <div class="form-group row">
					  <asp:Label ID="lblBasicSalary" runat="server" class="col-sm-2 control-label" 
                                                             Text="Basic Salary" Visible="false"></asp:Label>
						<div class="col-sm-3">
						 <asp:TextBox ID="txtbasicSalary" class="form-control" runat="server" Visible="false"></asp:TextBox>  
                                    
                        </div>
					   </div> 			       
					   
					   <div class="form-group row">
					
					<div class="col-sm-1"></div>
						<div class="col-sm-2">
                            <asp:Button ID="btnsave" class="btn btn-success" onclick="btnsave_Click" runat="server" Text="Save" />                             
                        </div>
					
				    <div class="col-sm-2">  
				     <asp:Button ID="btncancel" class="btn btn-success" runat="server" Text="Cancel" 
                            onclick="btncancel_Click" />    
				    </div>
				       				    
				   
				 <div class="col-sm-2">
                     <asp:Button ID="btnback" class="btn btn-success" runat="server" Text="Back" 
                         onclick="btnback_Click" />
  				  </div>
  				
  				 
  				
			       </div>
			       </div>
			       </form>
			       </div>
			       </div> <!-- End Col-md-9 -->
			     
			     <!-- Dashboard start -->
 	
 	<div class="col-lg-3 col-md-3">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
    <div class="col-lg-3 col-md-3">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                 
                                             <div class="form-group row">
                                             
                                             <asp:Button ID="btncontract" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Contract Report" Width="100%" onclick="btncontract_Click" />
                                              </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnEmp" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Employee Download" Width="100%" onclick="btnEmp_Click" />
                                             </div>
                                             <div class="form-group row">
                                             <asp:Button ID="btnatt" class="btn btn-success btn-rounded"  runat="server" 
                                                     Text="Attendance Download" Width="100%" onclick="btnatt_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnLeave" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Leave Download" Width="100%" OnClick="btnLeave_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnOT" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="OT Download" Width="100%" onclick="btnOT_Click" />
                                             </div>
                                              <div class="form-group row">
                                             <asp:Button ID="btnSal" class="btn btn-success btn-rounded"  runat="server" 
                                                      Text="Salary Download" Width="100%" onclick="btnSal_Click" />
                                             </div>
                                     
                                </div>
                            </div>
                            
                        </div>
 
    <!-- Dashboard End -->
                        </div> <!-- End Col-md-12 -->
                        </div> <!-- End row -->
                        </div> <!-- End main -->
			</ContentTemplate>
			</asp:UpdatePanel>       

</asp:Content>

