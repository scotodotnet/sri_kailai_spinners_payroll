﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;




public partial class Employee_Type : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    MastersClass objMas = new MastersClass();
    string SessionAdmin;
    static string lblprobation_Common;
    string SessionCcode;
    string SessionLcode;
    string empname;

    static string EmpType;
    static string Category;
    static string EmpTypecode;

    string SSQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        if (!IsPostBack)
        {
            MstEmployeeTypeDisplay();
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (rbtstaffloaber.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Category ');", true);
            ErrFlag = true;
        }
       

        else if (txtemptyp.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Type Name');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (btnsave.Text == "Update")
            {

                objMas.EmpTypeCd = EmpTypecode;
                objMas.EmpType = txtemptyp.Text;
                objMas.EmpCategory = rbtstaffloaber.SelectedValue;
                objdata.UpdateEmployeeType(objMas);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Type Update Successfully...');", true);
                MstEmployeeTypeDisplay();
                btnsave.Text = "Save";
                clear();

            }
            else
            {
            string empname = objdata.EmpTypeName_check(txtemptyp.Text, rbtstaffloaber.SelectedValue);
            if (empname.Trim().ToLower() == txtemptyp.Text.Trim().ToLower())
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Type Already Exist');", true);
                ErrFlag = true;
            }
            else
            {

                objMas.EmpType = txtemptyp.Text;
                objMas.EmpCategory = rbtstaffloaber.SelectedValue;
                objdata.MstEmployeeType(objMas);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Save Successfully...');", true);
                clear();
                MstEmployeeTypeDisplay();


            }
            }
          }
        }
    
    public void MstEmployeeTypeDisplay()
    {
        DataTable dt = new DataTable();
        dt = objdata.MstEmployeeTypeDisplay();
        rptrEmployeeType.DataSource = dt;
        rptrEmployeeType.DataBind();
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        EmpType = commandArgs[0];
        Category = commandArgs[1];
        EmpTypecode = commandArgs[2];

        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(EmpTypecode, EmpType, Category);
                break;
            case ("Edit"):
                EditRepeaterData(EmpTypecode, EmpType, Category);
                break;
        }
    }

    private void EditRepeaterData(string EmpTypecode, string EmpType , string Category)
    {
        try
        {
            bool ErrFlag = false;
            string staff = "";
            if (Category.ToString() == "Staff")
            {
                staff = "1";
            }
            else if (Category.ToString() == "Labour")
            {
                staff = "2";
            }
           

            if (!ErrFlag)
            {
                DataTable getEmployeeType = new DataTable();

                getEmployeeType = objdata.EmpTypeName_checkDataTable(EmpTypecode,EmpType, staff);
                txtemptyp.Text = getEmployeeType.Rows[0]["EmpType"].ToString();
                string stafflabour = getEmployeeType.Rows[0]["EmpCategory"].ToString();
                int sl = Convert.ToInt32(stafflabour.ToString());
                rbtstaffloaber.SelectedValue = sl.ToString();
                btnsave.Text = "Update";
            }
        }
        catch (Exception ex)
        {

        }
    }

    private void DeleteRepeaterData(string EmpTypecode, string EmpType, string Category)
    {
        bool ErrFlag = false;
        string EmpployeeType = objdata.EmpType_delete(EmpTypecode);
        if (EmpployeeType.Trim() == EmpTypecode.Trim())
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee type cannot be Deleted...');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dtdDelete = new DataTable();
            SSQL = "Delete MstEmployeeType where EmpType ='" + EmpType + "' and EmpTypeCd = '" + EmpTypecode + "'";
            objdata.ReturnMultipleValue(SSQL);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully...');", true);
            MstEmployeeTypeDisplay();
        }
    }

    //private static string UTF8Encryption(string password)
    //{
    //    string strmsg = string.Empty;
    //    byte[] encode = new byte[password.Length];
    //    encode = Encoding.UTF8.GetBytes(password);
    //    strmsg = Convert.ToBase64String(encode);
    //    return strmsg;
    //}

    //private static string UTF8Decryption(string encryptpwd)
    //{
    //    string decryptpwd = string.Empty;
    //    UTF8Encoding encodepwd = new UTF8Encoding();
    //    Decoder Decode = encodepwd.GetDecoder();
    //    byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
    //    int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
    //    char[] decoded_char = new char[charCount];
    //    Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
    //    decryptpwd = new String(decoded_char);
    //    return decryptpwd;
    //}


    protected void btncancel_Click(object sender, EventArgs e)
    {
        clear();
    }

    public void clear()
    {
        txtemptyp.Text = "";
        rbtstaffloaber.SelectedValue = null;
        btnsave.Text = "Save";
    }


    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void btnEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("RptEmpDownload.aspx");
    }
    protected void btnatt_Click(object sender, EventArgs e)
    {
        Response.Redirect("AttenanceDownload.aspx");
    }
   
   
    protected void btnSal_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmDeductionDownload.aspx");
    }
    
}