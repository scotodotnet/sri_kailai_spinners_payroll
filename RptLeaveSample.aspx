﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RptLeaveSample.aspx.cs" Inherits="RptLeaveSample" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr id="Panelload" runat="server" visible="false">
                <td>
                    <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="false" >
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>EmpNo</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="EmpNo" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>LeaveType</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="LeaveType" runat="server" Text='<%# Eval("LeaveType") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>NumberofLeave</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="NumberofLeave" runat="server" Text='<%# Eval("NumberofLeave") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>TotalWorkingDays</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="TotalWorkingDays" runat="server" Text='<%# Eval("TotalWorkingDays") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>FromDate</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="FromDate" runat="server" Text='<%# Eval("FromDate") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>ToDate</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="ToDate" runat="server" Text='<%# Eval("ToDate") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>Month</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="Month" runat="server" Text='<%# Eval("Month") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>Year</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="Year" runat="server" Text='<%# Eval("Year") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>Approvededby</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="Approvededby" runat="server" Text='<%# Eval("Approvededby") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>Approveddate</HeaderTemplate>
                            <ItemTemplate>
                            <asp:Label ID="Approveddate" runat="server" Text='<%# Eval("Approveddate") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
